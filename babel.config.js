module.exports = function(api) {
  api.cache(true)

  const presets = [];
  const plugins = [];

  if (process.env.NODE_ENV === "test") {    
    plugins.push("require-context-hook")    
  }

  return {
    presets,
    plugins
  }
}