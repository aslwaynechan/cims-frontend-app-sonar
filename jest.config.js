// jest.config.js
module.exports = {
	"verbose" : true,
	"modulePaths" : [ "src" ],
	"moduleNameMapper" : {
		"\\.(css|less)$" : "<rootDir>/__mocks__/styleMock.js",
		"\\.(gif|ttf|eot|svg)$" : "<rootDir>/__mocks__/fileMock.js"
	},
	setupFiles: ['<rootDir>/.jest/register-context.js'],
	"testResultsProcessor": "jest-sonar-reporter"
};