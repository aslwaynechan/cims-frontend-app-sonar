const proxy = require('http-proxy-middleware');
const pkg = require('../package.json');
const targetUrl = process.env.REACT_APP_PROXY || pkg.proxy;
console.log('Start custom proxy for dev, the proxy url: ');
console.log(targetUrl);

let proxyOpts = {
  target: targetUrl,
  pathRewrite: {

  },
  router: {
  },
  onProxyReq: function onProxyReq(proxyReq, req, res) {
      // Log outbound request to remote target
      console.log('-->  ', req.method, req.path, '->', proxyReq.baseUrl + proxyReq.path);
  },
  onError: function onError(err, req, res) {
      console.error(err);
      res.status(500);
      res.json({error: 'Error when connecting to remote server.'});
  },
  logLevel: 'debug',
  changeOrigin: true,
  secure: false
};

let cimsProxy = proxy(proxyOpts);

module.exports = function(app) {
  app.use('/common', cimsProxy);
  app.use('/user', cimsProxy);
  app.use('/appointment', cimsProxy);
  app.use('/patient', cimsProxy);
  app.use('/message', cimsProxy);
  app.use('/assessment', cimsProxy);
  app.use('/medical-summary', cimsProxy);
  app.use('/diagnosis', cimsProxy);
  app.use('/clinical-note', cimsProxy);
};


