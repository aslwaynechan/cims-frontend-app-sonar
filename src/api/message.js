import request from 'services/request';

export async function API_getMessageListByAppId(params) {
  let {applicationId} = params;
  return request(`message/message/${applicationId}`, {method:'get'});
}