import moment from 'moment';

export const initUserData = {
    chiFullName: '',
    contactPhone: '',
    email: '',
    engGivenName: '',
    engSurname: '',
    genderCd: '',
    loginName: '',
    password: '',
    position: '',
    salutation: '',
    status: 'active',
    supervisor: '',
    userExpiryDate: moment().add(10,'years'),
    userId: 0,
    version: ''
  };