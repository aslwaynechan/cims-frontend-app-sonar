import moment from 'moment';

export const TimeslotTemplateDTO={
    description: '',
    encounterTypeCd: '',
    newNormal: 0,
    newPublic: 0,
    newForce: 0,
    oldNormal: 0,
    oldPublic: 0,
    oldForce: 0,
    overallQuota: 9999,
    slotProfileCode: '',
    slotTemplateId: 0,
    startTime: moment().format('HH:mm'),
    subEncounterTypeCd: '',
    version: '',
    week: '0000000'
};
