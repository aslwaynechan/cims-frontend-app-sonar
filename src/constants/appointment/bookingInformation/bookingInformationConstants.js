import _ from 'lodash';

export const initEncounterType = {
    encounterTypeCd: '',
    subEncounterTypeCd: '',
    caseTypeCd: 'N',
    appointmentTypeCd: '',
    appointmentDate: null,
    appointmentTime: null,
    elapsedPeriod: '',
    elapsedPeriodUnit: '',
    //session: '',
    searchLogicCd: 'W',
    bookingUnit: '1',
    encounterTypeList: [],
    subEncounterTypeList: [],
    remarkId: '',
    memo: '',
    caseNo: ''
};

export const initBookingData = {
    clinicCd: '',
    encounterTypes: [
        _.cloneDeep(initEncounterType)
    ],
    page: 0,
    pageSize: 0
    // remark: ''
};

export const anonymousDataList = [{ option: 'New Registration' }, { option: 'Anonymous Appoimtment' }];
