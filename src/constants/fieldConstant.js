
const FieldConstant = {
    //country code default value
    COUNTRY_CODE_DEFAULT_VALUE: 'HKG',
    //gender default value
    PATIENT_GENDER_DEFAULT_VALUE: 'F'
};

export default FieldConstant;