import Enum from '../../enums/enum';
import FieldConstant from '../../constants/fieldConstant';

export const contactPersonList = {
    block: '',
    building: '',
    contactId: 0,
    displaySeq: 0,
    districtCd: '',
    emailAddress: '',
    engGivename: '',
    engSurname: '',
    estate: '',
    floor: '',
    nameChi: '',
    contactPhoneList: [],
    relationshipCd: '',
    region: '',
    room: '',
    subDistrictCd: '',
    streetName: '',
    streetNo: ''
};

export const patientBasic = {
    nationality: '',
    birthPlaceCd: '',
    caseList: [],
    ccCode1: '',
    ccCode2: '',
    ccCode3: '',
    ccCode4: '',
    ccCode5: '',
    ccCode6: '',
    communicationMeansCd: '',
    clinicCd: 'A2',
    dob: null,
    docTypeCd: '',
    eduLevelCd: '',
    ehrEnrolmentEndDate: '',
    ehrPatientId: '',
    ehrssInd: '',
    emailAddress: '',
    engGivename: '',
    engSurname: '',
    ethnicityCd: '',
    exactDobCd: Enum.DATE_FORMAT_EDMY_KEY,
    exactDodCd: '',
    genderCd: FieldConstant.PATIENT_GENDER_DEFAULT_VALUE,
    govDptCd: '',
    hkid: '',
    maritalStatusCd: '',
    nameChi: '',
    occupationCd: '',
    otherDocNo: '',
    otherName: '',
    addressList: [],
    contactPersonList: [],
    documentPairList: [],
    patientKey: 0,
    phoneList: [],
    postOfficeBoxNo: '',
    postOfficeName: '',
    postOfficeRegion: '',
    preferredLangCd: '',
    rank: '',
    religionCd: '',
    remarks: '',
    translationLangCd: '',
    userId: 'admin',
    version: '',
    communicateLanguageCd: ''
};

export const patientAddressBasic = {
    addressId: 0,
    addressTypeCd: '',
    addressLanguageCd: Enum.PATIENT_ADDRESS_ENGLISH_LANUAGE,
    block: '',
    building: '',
    buildingCsuId: '',
    districtCd: '',
    estate: '',
    floor: '',
    room: '',
    streetName: '',
    streetNo: '',
    subDistrictCd: ''
};

export const patientContactPhonesBasic = {
    areaCd: '',
    contactPhoneId: 0,
    countryCd: FieldConstant.COUNTRY_CODE_DEFAULT_VALUE,
    extPhoneNo: '',
    phoneNo: '',
    phoneTypeCd: ''
};

export const patientPhonesBasic = {
    areaCd: '',
    countryCd: FieldConstant.COUNTRY_CODE_DEFAULT_VALUE,
    extPhoneNo: '',
    phoneId: 0,
    phoneNo: '',
    phonePriority: 0,
    phoneTypeCd: Enum.PHONE_TYPE_MOBILE_PHONE,
    smsPhoneInd: '0'
};

export const patientSocialDataBasic = {
    ethnicityCd: '',
    maritalStatusCd: '',
    religionCd: '',
    occupationCd: '',
    translationLangCd: '',
    eduLevelCd: '',
    govDptCd: '',
    rank: '',
    remarks: ''
};

export const patientContactInfoBasic = {
    communicationMeansCd: '',
    emailAddress: '',
    communicateLangCd: Enum.PATIENT_ADDRESS_CHINESE_LANGUAGE,
    postOfficeBoxNo: '',
    postOfficeName: '',
    postOfficeRegion: ''
};

export const patientBaseInfoBasic = {
    nationality: '',
    birthPlaceCd: '',
    ccCode1: '',
    ccCode2: '',
    ccCode3: '',
    ccCode4: '',
    ccCode5: '',
    ccCode6: '',
    dob: null,
    docTypeCd: '',
    hkid: '',
    nameChi: '',
    otherDocNo: '',
    otherName: '',
    ehrPatientId: '',
    engGivename: '',
    engSurname: '',
    exactDobCd: Enum.DATE_FORMAT_EDMY_KEY,
    genderCd: '',
    phone: {
        areaCd: '',
        countryCd: FieldConstant.COUNTRY_CODE_DEFAULT_VALUE,
        extPhoneNo: '',
        phoneId: 0,
        phoneNo: '',
        phonePriority: 1,
        phoneTypeCd: Enum.PHONE_TYPE_MOBILE_PHONE,
        smsPhoneInd: '1'
    },
    translationLangCd:'',
    communicateLanguageCd: '',
    preferredLangCd: '',
    documentPairList: []
};

export const patientDocumentPair = {
    docNo: '',
    docTypeCd: '',
    documentPairId: '',
    patientKey: '',
    serviceCd: ''
};