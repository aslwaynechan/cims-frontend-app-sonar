const CommonRegex = {
    VALIDATION_REGEX_ENGLISH: '^[A-Za-z]+$',
    VALIDATION_REGEX_SPECIAL_ENGLISH: '^[A-Za-z][A-Za-z ,\'.`/@:()*-]*$',//Match the beginning of the letter, and contain [letter][,][-]['][.][space][`][/][@][(][)][:][*] character
    VALIDATION_REGEX_SPECIAL_ENGLISH_WARNING: '[@:()*]+',//Match the [@][(][)][:][*] character
    VALIDATION_REGEX_ENGLISH_OR_SPACE: '^[A-Za-z ]+$',
    VALIDATION_REGEX_ENGLISH_OR_NUMBER: '^[A-Za-z0-9]+$',
    VALIDATION_REGEX_ENGLISH_OR_NUMBER_OR_SPACES: '^[A-Za-z0-9 ]+$',
    VALIDATION_REGEX_CHINESE: '^[\u4e00-\u9fa5]+$',
    VALIDATION_REGEX_POSITIVE_INTEGER: '^[1-9]+[0-9]*]*$',
    VALIDATION_REGEX_POSITIVE_INTEGER_EMPTY:  /^\s*$|^[1-9]+[0-9]*]*$/,
    VALIDATION_REGEX_ZERO_INTEGER: '^([0-9]*)$',
    VALIDATION_REGEX_MAXINUM9999_INTEGER: '^([0-9]{0,3})$',
    VALIDATION_REGEX_BLANK_SPACE: /\s+/g,
    VALIDATION_REGEX_EMAIL: '^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])?@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$',
    VALIDATION_REGEX_INCLUDE_CHINESE:'.*[\u4e00-\u9fa5]+.*$',
    VALIDATION_REGEX_NOT_NUMBER : /[^0-9]/ig,
    VALIDATION_REGEX_IS_NUMBER: '^[0-9]*$',
    VALIDATION_REGEX_ENGLISH_NUM:'^[A-Za-z0-9]+$',
    VALIDATION_REGEX_PASSWORD:/^(?=.*\d{1,})(?=.*[A-Z]{1,}).{8,20}$/,
    VALIDATION_REGEX_DECIMAL_15_4:'^(([^0][0-9]{0,10}|0)\\.([0-9]{1,4})$)|^(([^0][0-9]{0,10}|0)$)|^(([1-9]+)\\.([0-9]{1,4})$)|^(([1-9]{0,10})$)',
    VALIDATION_REGEX_PERIOD: '^\\d+(.5)?$',
    VALIDATION_REGEX_CODEVERIFICATION: '[a-zA-Z][a-zA-Z0-9]+$',
    VALIDATION_REGEX_PHONENO: '^[1-9][0-9]*$',
    VALIDATION_REGEX_HKPHONENO: '^[1-9][0-9]{7}$',
    VALIDATION_REGEX_NOCHINESE: '^[\u0000-\u00ff]+$'
};

export default CommonRegex;