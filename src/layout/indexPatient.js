import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PatientPanel from '../views/compontent/patientPanel';
import { Grid, Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { resetAll, getPatientCaseNo } from '../store/actions/patient/patientAction';
import {
    addTabs,
    refreshSubTabs,
    cleanSubTabs,
    changeTabsActive,
    changeSubTabsActive,
    deleteSubTabs,
    deleteTabs,
    cleanTabParams
} from '../store/actions/mainFrame/mainFrameAction';
import { selectCaseTrigger } from '../store/actions/caseNo/caseNoAction';
import CIMSMultiTabs from '../components/Tabs/CIMSMultiTabs';
import CIMSMultiTab from '../components/Tabs/CIMSMultiTab';
import CIMSButton from '../components/Buttons/CIMSButton';
import CIMSeHRButton from '../components/EHR/CIMSeHRButton';
import * as CommonUtilities from '../utilities/commonUtilities';
import PatientList from '../views/patientSpecificFunction/patientList';
import { openCommonMessage } from '../store/actions/message/messageAction';
import { resetCondition, updatePatientListField } from '../store/actions/patient/patientSpecFunc/patientSpecFuncAction';
import Loadable from 'react-loadable';
import Loading from './component/loading';
import NotFound from './component/notfound';
import NotLiveRoute from 'react-live-route';
import MedicalSummaryDialog from '../views/jos/medicalSummary/MedicalSummaryDialog';
import ErrorBoundary from 'components/JErrorBoundary';
import Enum from '../enums/enum';
import accessRightEnum from '../enums/accessRightEnum';

const LiveRoute = withRouter(NotLiveRoute);

const styles = () => ({
    root: {
        height: '100%'
    },
    patientPanelRoot: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexFlow: 'column'
    },
    subTabRoot: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexFlow: 'column',
        // marginTop: 3
        marginTop: 0//Consultation test
    },
    subContentRoot: {
        //padding: '16px 18px',
        borderTop: '1px solid #e6e6e6',
        overflowY: 'auto',
        overflowX: 'hidden',
        flex: 1
    }
});


class IndexPatient extends Component {
    constructor(props) {
        super(props);
        this.componentList = [];
        // const accessRightList =
        //     (this.props.accessRights || []).filter(item => item.accessRightType === 'function' && item.isPatRequired === 'Y');
        // for (let i = 0; i < accessRightList.length; i++) {
        //     this.componentList.push({
        //         name: accessRightList[i].accessRightCd,
        //         path: accessRightList[i].componentPath,
        //         component: Loadable({
        //             // loader: () => import(/* webpackChunkName: "[request]" */`../views${accessRightList[i].componentPath}`),
        //             loader: () => import(`../views${accessRightList[i].componentPath}`),
        //             loading: Loading
        //         })
        //     });
        // }
        const { accessRights } = this.props;
        accessRights && accessRights.filter(item => item.type === 'function').forEach(right => {
            if (right.isPatRequired === 'Y') {
                this.componentList.push({
                    name: right.name,
                    path: right.path,
                    // displayOrder: menu.displayOrder,
                    component: ErrorBoundary(Loadable({
                        // loader: () => import(/* webpackChunkName: "[request]" */`../views${accessRightList[i].componentPath}`),
                        loader: () => import(`../views${right.path}`),
                        loading: Loading
                    }))
                });
            }
        });

        this.eHRButtonInfo = {
            label: 'eHRSS Registered',
            name: accessRightEnum.eHRRegistered,
            path: '/eHR/eHRRegistration',
            isPatRequired: 'Y'
        };
        this.componentList.push({
            name: accessRightEnum.eHRRegistered,
            path: '/eHR/eHRRegistration',
            // displayOrder: menu.displayOrder,
            component: Loadable({
                // loader: () => import(/* webpackChunkName: "[request]" */`../views${accessRightList[i].componentPath}`),
                loader: () => import(`../views${'/eHR/eHRRegistration'}`),
                loading: Loading
            })
        });
        // this.props.userRoleStatus && this.props.userRoleStatus.menuBK.filter(
        //     item => item.accessRightCd === accessRightEnum.eHRRegistered).forEach(right => {
        //     if (right.isPatRequired === 'Y') {
        //         // Add the eHR Registered Data to eHR Button Info
        //         if (right.accessRightCd === accessRightEnum.eHRRegistered) {
        //             this.eHRButtonInfo = {
        //                 label: right.accessRightName ? right.accessRightName : '',
        //                 name: right.accessRightCd ? right.accessRightCd : '',
        //                 // icon: right.icon ? right.icon : '',
        //                 path: right.componentPath ? right.componentPath : '',
        //                 isPatRequired: right.isPatRequired ? right.isPatRequired : ''
        //             };
        //         }
        //         this.componentList.push({
        //             name: right.accessRightCd,
        //             path: right.componentPath,
        //             // displayOrder: menu.displayOrder,
        //             component: Loadable({
        //                 // loader: () => import(/* webpackChunkName: "[request]" */`../views${accessRightList[i].componentPath}`),
        //                 loader: () => import(`../views${right.componentPath}`),
        //                 loading: Loading
        //             })
        //         });
        //     }
        // });

        this.state = {
            messageDialogIndex: 0,
            medicalSummaryOpenFlag: false
        };
    }

    componentDidUpdate() {
        const { patientInfo, caseNoInfo, openSelectCase } = this.props;
        if (patientInfo && !caseNoInfo.caseNo && !openSelectCase) {
            let activeCaseList = patientInfo.caseList.filter(item => item.statusCd === Enum.CASE_STATUS.ACTIVE);
            if(activeCaseList.length === 1){
                this.props.getPatientCaseNo(patientInfo.caseList, activeCaseList[0]['caseNo']);
            } else if (activeCaseList.length > 1) {
                this.props.selectCaseTrigger({ trigger: true });
            }
        }
    }

    componentWillUnmount() {
        this.props.resetAll();
    }

    handleChangeTab = (e, newValue) => {
        this.props.changeSubTabsActive(newValue);
    };

    handleDeleteTab = (e, item) => {
        if (typeof item.doCloseFunc === 'function') {
            item.doCloseFunc(
                (success) => {
                    if (success) {
                        this.props.deleteSubTabs(item.name);
                    }
                });
        }
        else {
            this.props.deleteSubTabs(item.name);
        }
    };

    // recursionDeleteTab = (targetList, index, deleteTabFunc, changeActiveTab) => {
    //     let item = targetList[index];
    //     if (!item) {
    //         if (!this.props.subTabs || (this.props.subTabs && this.props.subTabs.length === 0)) {
    //             this.props.resetAll();
    //             // this.props.resetCondition();
    //             let filterCondition = {
    //                 encounterTypeCd: '',
    //                 subEncounterTypeCd: '',
    //                 patientKey: ''
    //             };
    //             if (this.props.userRoleType === Enum.USER_ROLE_TYPE.COUNTER) {
    //                 filterCondition.attnStatusCd = Enum.ATTENDANCE_STATUS.NOT_ATTEND;
    //             } else if (this.props.userRoleType === Enum.USER_ROLE_TYPE.NURSE || this.props.userRoleType === Enum.USER_ROLE_TYPE.DOCTOR) {
    //                 filterCondition.attnStatusCd = Enum.ATTENDANCE_STATUS.ATTENDED;
    //             }
    //             this.props.updatePatientListField({ filterCondition: filterCondition });
    //             return;
    //         }
    //         this.recursionDeleteTab(this.props.subTabs, 0, this.props.deleteSubTabs, this.props.changeSubTabsActive);
    //         return;
    //     }
    //     else {
    //         changeActiveTab(item.name);
    //         // let editModeTab = targetList.find(tab => tab.name === item.name);
    //         if (typeof item.doCloseFunc === 'function') {
    //             item.doCloseFunc(
    //                 (success) => {
    //                     if (success) {
    //                         new Promise(resolve => {
    //                             deleteTabFunc(item.name);
    //                             resolve();
    //                         }).then(() => {
    //                             index++;
    //                             this.recursionDeleteTab(targetList, index, deleteTabFunc, changeActiveTab);
    //                         });
    //                     }
    //                 });
    //         }
    //         else {
    //             new Promise((resolve) => {
    //                 deleteTabFunc(item.name);
    //                 resolve();
    //             }).then(() => {
    //                 index++;
    //                 this.recursionDeleteTab(targetList, index, deleteTabFunc, changeActiveTab);
    //             });
    //         }
    //     }
    // }


    myDeleteTab = (tabList, tabIndex, deleteTabFunc, activateTabFunc) => {
        let curTab = tabList[tabIndex];
        if (!curTab) {    // if there is no (main) tab,
            // if there is no subTabs, update patient list field with filter condition
            if (!this.props.subTabs || (this.props.subTabs && this.props.subTabs.length === 0)) {
                this.myUpdatePatientListField();
                return;
            }
            this.myDeleteTab(this.props.subTabs, 0, this.props.deleteSubTabs, this.props.changeSubTabsActive);
            return;
        }
        else {  // if current tab exists
            activateTabFunc(curTab.name);
            if (typeof curTab.doCloseFunc === 'function') {   // if doCloseFunc is defined, call the doCloseFunc of the current
                curTab.doCloseFunc(
                    // the callback method
                    (canDeleteCurTab) => {
                        if (canDeleteCurTab) {  // close the current tab
                            new Promise(resolve => {
                                deleteTabFunc(curTab.name);
                                resolve();
                            }).then(() => { // close the next tab
                                tabIndex++;
                                this.myDeleteTab(tabList, tabIndex, deleteTabFunc, activateTabFunc);
                            });
                        }
                    });
            }
            else {  // if no doCloseFunc defined, delete the tab
                new Promise((resolve) => {
                    deleteTabFunc(curTab.name);
                    resolve();
                }).then(() => { // close the next tab
                    tabIndex++;
                    this.myDeleteTab(tabList, tabIndex, deleteTabFunc, activateTabFunc);
                });
            }
        }
    }

    myUpdatePatientListField() {
        this.props.resetAll();
        // this.props.resetCondition();
        let filterCondition = {
            encounterTypeCd: '',
            subEncounterTypeCd: '',
            patientKey: ''
        };
        // if (this.props.userRoleType === Enum.USER_ROLE_TYPE.COUNTER) {
        //     filterCondition.attnStatusCd = Enum.ATTENDANCE_STATUS.NOT_ATTEND;
        // } else if (this.props.userRoleType === Enum.USER_ROLE_TYPE.NURSE || this.props.userRoleType === Enum.USER_ROLE_TYPE.DOCTOR) {
        //     filterCondition.attnStatusCd = Enum.ATTENDANCE_STATUS.ATTENDED;
        // }
        if (this.props.userRoleType === Enum.USER_ROLE_TYPE.COUNTER) {
            filterCondition.attnStatusCd = Enum.ATTENDANCE_STATUS.NOT_ATTEND;
        } else {
            filterCondition.attnStatusCd = Enum.ATTENDANCE_STATUS.ATTENDED;
        }
        this.props.updatePatientListField({ filterCondition: filterCondition });
    }


    handleNextPatient = () => {
        // this.recursionDeleteTab(this.props.tabs, 1, this.props.deleteTabs, this.props.changeTabsActive);
        this.myDeleteTab(this.props.tabs, 1, this.props.deleteTabs, this.props.changeTabsActive);
    };

    handleMedicalSummaryDialogOpen = () => {
        this.setState({
            medicalSummaryOpenFlag: true
        });
    }

    handleMedicalSummaryDialogCancel = () => {
        this.setState({
            medicalSummaryOpenFlag: false
        });
    }

    render() {
        const { classes, serviceCd, patientInfo, caseNoInfo,
                addTabs, refreshSubTabs, subTabs, cleanSubTabs } = this.props;
        let { medicalSummaryOpenFlag } = this.state;
        let dialogProps = {
            isOpen: medicalSummaryOpenFlag,
            patientKey: patientInfo !== null ? patientInfo.patientKey : null,
            serviceCd: serviceCd,
            handleDialogCancel: this.handleMedicalSummaryDialogCancel
        };

        let isOpenPanel = false;
        if (patientInfo && patientInfo.patientKey) {
            if (caseNoInfo.caseNo || patientInfo.caseList.filter(item => item.statusCd === Enum.CASE_STATUS.ACTIVE).length === 0) {
                isOpenPanel = true;
                this.props.updatePatientListField({ isFocusSearchInput: true });
            }
        }

        let isEHRSSRegistered = false;
        if (patientInfo && patientInfo.patientKey && patientInfo.patientEhr && patientInfo.patientEhr.ehrNo) {
            isEHRSSRegistered = true;
        }

        const { accessRights } = this.props;
        let isEHRAccessRight = false;
        // TODO : Add the ehruId check in accessRights
        accessRights && accessRights.filter(item => item.type === 'button')
                .forEach(right => {
            if (right.isPatRequired === 'Y'
                    && right.name === accessRightEnum.eHRRegistered) {
                isEHRAccessRight = true;
            }
        });

        return (
            <Grid className={classes.root}>
                {
                    isOpenPanel ?
                        <Grid className={classes.patientPanelRoot}>
                            <PatientPanel>
                                {// If Check db ACCESS_RIGHT [eHRSS Registered button(B150)] for use userRoleStatus.menuBK
                                    this.eHRButtonInfo && (
                                        <CIMSeHRButton
                                            key={(this.eHRButtonInfo && this.eHRButtonInfo.name ? this.eHRButtonInfo.name : '')}
                                            label={(this.eHRButtonInfo && this.eHRButtonInfo.label ? this.eHRButtonInfo.label : '')}
                                            name={(this.eHRButtonInfo && this.eHRButtonInfo.name ? this.eHRButtonInfo.name : '')}
                                            eHRTabInfo={this.eHRButtonInfo}
                                            subTabs={subTabs}
                                            onClick={addTabs}
                                            refreshSubTabs={refreshSubTabs}
                                            cleanSubTabs={cleanSubTabs}
                                            isEHRSSRegistered={isEHRSSRegistered}
                                            isEHRAccessRight={isEHRAccessRight}
                                        />
                                    )
                                }
                               {
                                    patientInfo && patientInfo.patientKey ? (
                                        <CIMSButton
                                            id="indexPatient_patientPanel_medical_summary"
                                            onClick={this.handleMedicalSummaryDialogOpen}
                                        >
                                            Medical Summary
                                        </CIMSButton>
                                    ) : null
                                }
                                <CIMSButton
                                    id="indexPatient_patientPanel_nextPatient"
                                    onClick={this.handleNextPatient}
                                >{`Next ${CommonUtilities.getPatientCall()}`}</CIMSButton>
                            </PatientPanel>
                            <Paper className={classes.subTabRoot}>
                                <CIMSMultiTabs
                                    value={this.props.subTabsActiveKey}
                                    onChange={this.handleChangeTab}
                                >
                                    {this.props.subTabs.map((item) => (
                                         <CIMSMultiTab disable={this.props.isOpenReview} key={item.name} label={item.label} value={item.name} onClear={e => this.handleDeleteTab(e, item)} />
                                    ))}
                                </CIMSMultiTabs>
                                <Grid className={classes.subContentRoot}>
                                    <LiveRoute path="/index/patientSpecFunction">
                                        <Grid style={{ textAlign: 'center', fontSize: '16pt', padding: 41, display: this.props.subTabs.length > 0 ? 'none' : null }}>
                                            {`Please select a ${CommonUtilities.getPatientCall()}-specific function.`}
                                        </Grid>
                                    </LiveRoute>
                                    {
                                        this.props.subTabs.map(item => {
                                            const asyncComp = this.componentList.find(i => i.name === item.name);
                                            return asyncComp ?
                                                <LiveRoute
                                                    exact
                                                    alwaysLive
                                                    key={asyncComp.name}
                                                    path={`/index/patientSpecFunction/${asyncComp.name}`}
                                                    component={asyncComp.component}
                                                />
                                                :
                                                <LiveRoute
                                                    exact
                                                    alwaysLive
                                                    key={item.name}
                                                    path={`/index/patientSpecFunction/${item.name}`}
                                                    component={NotFound}
                                                />;
                                        })
                                    }
                                </Grid>
                            </Paper>
                        </Grid> : <PatientList />
                }
                <MedicalSummaryDialog key={Math.random()} {...dialogProps} />
                {/* <Prompt message={MessageUtilities.getMessageDescriptionByMsgCode('110018')} when={this.props.editModeTabs && this.props.editModeTabs.length > 0} /> */}
            </Grid >
        );
    }
}

function mapStateToProps(state) {
    return {
        patientInfo: state.patient.patientInfo,
        caseNoInfo: state.patient.caseNoInfo,
        subTabsActiveKey: state.mainFrame.subTabsActiveKey,
        subTabs: state.mainFrame.subTabs,
        editModeTabs: state.mainFrame.editModeTabs,
        accessRights: state.login.accessRights,
        tabs: state.mainFrame.tabs,
        serviceCd: state.login.service.serviceCd,
        openSelectCase: state.caseNo.openSelectCase,
        userRoleType: state.login.loginInfo && state.login.loginInfo.userRoleType,
        isOpenReview: state.registration.isOpenReview
    };
}

const dispatchProps = {
    addTabs,
    refreshSubTabs,
    cleanSubTabs,
    changeTabsActive,
    changeSubTabsActive,
    deleteSubTabs,
    resetAll,
    openCommonMessage,
    resetCondition,
    deleteTabs,
    cleanTabParams,
    updatePatientListField,
    selectCaseTrigger,
    getPatientCaseNo
};

export default withRouter(connect(mapStateToProps, dispatchProps)(withStyles(styles)(IndexPatient)));
