import React, { Component } from 'react';
import {
    Avatar,
    Grid,
    Typography,
    MenuList,
    MenuItem,
    Button,
    Popper,
    Fade,
    Paper,
    IconButton
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { NavigateNext } from '@material-ui/icons';
import * as CommonUtilities from '../../utilities/commonUtilities';
import * as messageType from '../../store/actions/message/messageActionType';
import { openCommonMessage } from '../../store/actions/message/messageAction';
import { updatePatientListField } from '../../store/actions/patient/patientSpecFunc/patientSpecFuncAction';
import { connect } from 'react-redux';

const styles = theme => ({
    gridRoot: {
        marginTop: 8,
        paddingRight: 10,
        width: 'auto',
        flexWrap: 'nowrap',
        whiteSpace: 'nowrap'
    },
    avatarRoot: {
        display: 'flex',
        width: 20,
        height: 20,
        borderRadius: '5%'
    },
    menuAvatarRoot: {
        width: 15,
        height: 15,
        borderRadius: '5%',
        paddingRight: 5
    },
    paperRoot: {
        border: '1px solid #CCC',
        boxShadow: '4px 3px 5px -1px rgba(0,0,0,0.2), 5px 6px 8px 0px rgba(0,0,0,0.14), 4px 2px 14px 0px rgba(0,0,0,0.12)'
    },
    btnRoot: {
        color: theme.palette.primary.main,
        verticalAlign: 'middle',
        textTransform: 'none',
        padding: '2px 3px',
        minWidth: 0,
        borderRadius: '5px',
        '&:hover': {
            backgroundColor: theme.palette.primary.main,
            color: theme.palette.background.default
        }
    },
    btnSelectedRoot: {
        verticalAlign: 'middle',
        textTransform: 'none',
        padding: '2px 3px',
        minWidth: 0,
        borderRadius: '5px',
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.background.default,
        '&:hover': {
            backgroundColor: theme.palette.primary.main,
            color: theme.palette.background.default
        }
    },
    titleRoot: {
        fontWeight: 600,
        color: 'inherit',
        fontSize: 'small'
    },
    menuListRoot: {
        padding: 0
    },
    menuItemRoot: {
        minHeight: '25px',
        padding: '8px 10px',
        paddingRight: 40,
        backgroundColor: theme.palette.background.default,
        color: theme.palette.primary.main,
        '&:hover': {
            padding: '8px 10px',
            paddingRight: 40,
            backgroundColor: theme.palette.primary.main,
            color: theme.palette.background.default
        }
    },
    menuItemFont: {
        color: 'inherit',
        fontSize: 'smaller'
    },
    popperRoot: {
        zIndex: 1200
    },
    iconRoot: {
        color: '#B8BCB9',
        padding: 0,
        paddingRight: 5,
        right: 0,
        position: 'absolute'
    }
});

class MenuBarButton extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mainAnchor: null,
            subAnchor: {}
        };
    }

    handleOpen = (event) => {
        this.setState({ mainAnchor: event.currentTarget });
    }

    handleClose = () => {
        this.setState({ mainAnchor: null, subAnchor: {} });
    }

    handleOnClick = (data) => {
        const { patientInfo } = this.props;
        let patientCall = CommonUtilities.getPatientCall();
        if (data && data.isPatRequired === 'Y' && (!patientInfo || !patientInfo.patientKey)) {
            let regOrSumTabsIndex = this.props.tabs.findIndex((item) => { return item.name == 'F002'; });
            if (regOrSumTabsIndex != -1) {
                this.props.openCommonMessage({
                    msgObj: {
                        description: `Please complete the Registration before proceeding to ${patientCall} related functions.`,
                        header: 'Application Suggestion',
                        btn1Caption:'OK'
                    }
                });
                return;
            }
            this.props.onClick({
                name: 'patientSpecFunction',
                label: `${CommonUtilities.getPatientCall()}-specific Function(s)`,
                disableClose: true,
                path: 'indexPatient'
            });
            this.props.updatePatientListField({ isFocusSearchInput: true });
        } else {
            if (patientInfo && data && data.label == 'Registration') {
                this.props.openCommonMessage({
                    msgObj: {
                        description: `Please complete the tasks for current ${patientCall} before proceeding to Registration.`,
                        header: 'Application Suggestion',
                        btn1Caption:'OK'
                    }
                });
                return;
            }
            this.props.onClick(data);
        }
    }

    handleOpenChild = (event, name) => {
        let { subAnchor } = this.state;
        subAnchor[name] = event.currentTarget;
        this.setState({ subAnchor });
    }

    handleCloseChild = (e, name) => {
        let { subAnchor } = this.state;
        delete subAnchor[name];
        this.setState({ subAnchor });
    }

    //eslint-disable-next-line
    getMenuList = (data, parent) => {
        const { classes, name } = this.props;
        const id = 'menuBarButton' + name + 'MenuPopper';
        return data.map(menu => {
            const childs = menu.child ? menu.child.length : 0;
            if (childs === 0) {
                return (
                    <MenuItem
                        id={id + 'MenuItem' + menu.name}
                        key={menu.name}
                        classes={{ root: classes.menuItemRoot }}
                        disableGutters
                        onClick={(e) => { this.handleOnClick(menu); this.handleClose(); e && e.stopPropagation ? e.stopPropagation() : window.event.cancelBubble = true; }}
                    >
                        <Typography className={classes.menuItemFont}>{menu.label}</Typography>
                    </MenuItem>
                );
            }
            else {
                return (
                    <MenuItem
                        id={`${id}MenuItem${menu.name}`}
                        key={menu.name}
                        classes={{ root: classes.menuItemRoot }}
                        disableGutters
                        onMouseEnter={(e) => { this.handleOpenChild(e, menu.name); }}
                        onMouseLeave={(e) => { this.handleCloseChild(e, menu.name); }}
                    >
                        {menu.icon ? <Avatar src={`data:image/gif;base64,${menu.icon}`} className={classes.menuAvatarRoot} /> : null}
                        <Typography className={classes.menuItemFont}>{menu.label}</Typography>
                        <IconButton
                            justify={'flex-end'}
                            color="primary"
                            className={classes.iconRoot}
                        >
                            <NavigateNext />
                        </IconButton>
                        <Popper
                            open={this.state.subAnchor[menu.name] !== undefined}
                            anchorEl={this.state.subAnchor[menu.name]}
                            placement="right-start"
                            transition
                            className={classes.popperRoot}
                            modifiers={{
                                offset: {
                                    offset: '-1px'
                                }
                            }}
                        >
                            {
                                ({ TransitionProps }) => (
                                    <Fade {...TransitionProps} timeout={{ enter: 350, exit: 0 }}>
                                        <Paper className={classes.paperRoot}>
                                            <MenuList className={classes.menuListRoot}>
                                                {this.getMenuList(menu.child, menu)}
                                            </MenuList>
                                        </Paper>
                                    </Fade>)}
                        </Popper>
                    </MenuItem>
                );
            }
        });
    }

    render() {
        const { classes, icon, label, name, selected, childMenu, menuData } = this.props;
        const id = 'menuBarButton' + name + 'MenuPopper';
        return (
            <Grid id={'menuBarButton' + name} container alignItems="center" justify="center" className={classes.gridRoot}>
                {icon ? <Avatar src={`data:image/gif;base64,${icon}`} className={classes.avatarRoot} /> : null}
                {childMenu && childMenu.length > 0 ?
                    <Button
                        disableFocusRipple
                        aria-describedby={id}
                        classes={{
                            root: selected ? classes.btnSelectedRoot : classes.btnRoot
                        }}
                        onMouseEnter={this.handleOpen}
                        onMouseLeave={this.handleClose}
                        id={'menuBarButton' + name + 'button'}
                        tabIndex={-1}
                    >
                        <Typography variant="subtitle1" className={classes.titleRoot}>{label}</Typography>
                        <Popper
                            id={id}
                            open={this.state.mainAnchor !== null}
                            anchorEl={this.state.mainAnchor}
                            placement="bottom-start"
                            transition
                            className={classes.popperRoot}
                        >
                            {
                                ({ TransitionProps }) => (
                                    <Fade {...TransitionProps} timeout={{ enter: 350, exit: 0 }}>
                                        <Paper className={classes.paperRoot}>
                                            <MenuList className={classes.menuListRoot}>
                                                {this.getMenuList(childMenu, null)}
                                            </MenuList>
                                        </Paper>
                                    </Fade>
                                )
                            }
                        </Popper>
                    </Button>
                    :
                    <Button
                        aria-describedby={id}
                        classes={{
                            root: selected ? classes.btnSelectedRoot : classes.btnRoot
                        }}
                        onClick={() => { this.handleOnClick(menuData); }}
                        id={'menuBarButton' + name + 'button'}
                        tabIndex={-1}
                    >
                        <Typography variant="subtitle1" className={classes.titleRoot}>{label}</Typography>
                    </Button>
                }
            </Grid>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        tabs: state.mainFrame.tabs
    };
};

const dispatchToProps = {
    openCommonMessage,
    updatePatientListField
};

export default withRouter(connect(mapStateToProps, dispatchToProps)(withStyles(styles)(MenuBarButton)));
