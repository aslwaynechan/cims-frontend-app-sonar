import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import {
    AppBar,
    Grid,
    Toolbar,
    IconButton,
    Typography,
    Avatar
} from '@material-ui/core';
import { logout, refreshToken } from '../../store/actions/login/loginAction';
import MenuBarButton from './menuBarButton';
import Logoffgif from '../../images/menu/logoff2.gif';
import CIMSAlertDialog from '../../components/Dialog/CIMSAlertDialog';
import { addTabs, refreshSubTabs, cleanSubTabs } from '../../store/actions/mainFrame/mainFrameAction';
import {
    updatePatientListField
} from '../../store/actions/patient/patientSpecFunc/patientSpecFuncAction';
const styles = theme => ({
    appbarRoot: {
        borderBottom: '1px solid #dcdcdc',
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'space-between',
        zIndex: 1000,
        position: 'unset'
    },
    toolbarRoot: {
        minHeight: 46
    },
    loginMessageRoot: {
        lineHeight: 1.3,
        fontWeight: 400,
        color: theme.palette.primary.main,
        fontSize: 'small',
        width: '100%',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        textAlign: 'end'
    },
    logoutAvatar: {
        width: 20,
        height: 20,
        borderRadius: '5%'
    },
    overFlowText: {
        overflow: 'hidden'
    }
});


class MenuBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            timer: null,
            showDialog: false
        };
    }

    componentDidMount() {
        this.props.refreshToken();
        window.lastMove = new Date().getTime();
        document.onmousemove = function () {
            window.lastMove = new Date().getTime();
        };
        const expiryTime = this.props.expiryTime;
        let timer = setInterval(() => {
            let now = new Date().getTime();
            if (now - window.lastMove >= expiryTime) {
                this.setState({ showDialog: true });
            } else {
                this.props.refreshToken();
            }
        }, expiryTime);
        let timeOut = setInterval(() => {
            let now = new Date().getTime();
            if (now - window.lastMove >= expiryTime) {
                clearInterval(timer);
                clearInterval(timeOut);
                this.setState({ showDialog: true });
            }
        }, 1000);
        this.setState({ timer, timeOut });
    }

    componentWillUnmount() {
        if (this.state.timer) {
            clearInterval(this.state.timer);
        }
        if (this.state.timeOut) {
            clearInterval(this.state.timeOut);
        }
    }

    handleLogout = () => {
        this.props.logout();
        this.props.updatePatientListField({ isFocusSearchInput: true });
    }

    render() {
        const { classes, addTabs, patientInfo, refreshSubTabs, subTabs, cleanSubTabs } = this.props;
        return (
            <Grid container>
                <AppBar className={classes.appbarRoot} elevation={2}>
                    <Grid item container>
                        <Toolbar className={classes.toolbarRoot}>
                            {this.props.menuList && this.props.menuList.map((item, index) => (
                                <MenuBarButton
                                    key={index}
                                    icon={item.icon}
                                    label={item.label}
                                    name={item.name}
                                    url={item.url}
                                    childMenu={item.child}
                                    menuData={item}
                                    onClick={addTabs}
                                    patientInfo={patientInfo}
                                    refreshSubTabs={refreshSubTabs}
                                    subTabs={subTabs}
                                    cleanSubTabs={cleanSubTabs}
                                />
                            ))}
                        </Toolbar>
                    </Grid>
                    <Grid item container alignItems="flex-end" justify="center" direction="column" className={classes.overFlowText}>
                        <Typography className={classes.loginMessageRoot} variant="body1" title={`User: ${this.props.loginName}`}>{this.props.loginName}</Typography>
                        <Typography className={classes.loginMessageRoot} variant="body1" title={`Login Time: ${this.props.loginTime}`}>{this.props.loginTime}</Typography>
                    </Grid>
                    <Grid item>
                        <IconButton
                            id={'menuBarLogoutButton'}
                            color="primary"
                            onClick={this.handleLogout}
                            title="Logout"
                            tabIndex={-1}
                        >
                            <Avatar src={Logoffgif} className={classes.logoutAvatar} />
                        </IconButton>
                    </Grid>
                </AppBar>
                <CIMSAlertDialog
                    id="timeout-dialog"
                    dialogStyle={{ backgroundColor: '#fff' }}
                    open={this.state.showDialog}
                    onClickOK={this.handleLogout}
                    onClose={this.handleLogout}
                    okButtonName={'Close'}
                    dialogTitle="Session Timeout"
                    dialogContentTitle={'Your session expired. Please login again.'}
                />
            </Grid>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loginName: state.login.loginInfo.loginName,
        loginTime: state.login.loginInfo.loginTime,
        expiryTime: state.login.loginInfo.expiryTime,
        menuList: state.login.menuList,
        patientInfo: state.patient.patientInfo,
        subTabs: state.mainFrame.subTabs
    };
};

const dispatchToProps = {
    refreshToken,
    logout,
    addTabs,
    refreshSubTabs,
    cleanSubTabs,
    updatePatientListField
};

export default withRouter(connect(mapStateToProps, dispatchToProps)(withStyles(styles)(MenuBar)));