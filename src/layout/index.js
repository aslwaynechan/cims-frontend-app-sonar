import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, withRouter } from 'react-router-dom';
import MenuBar from './component/menuBar';
import { Grid, withStyles } from '@material-ui/core';
import CIMSSnackbar from '../components/Dialog/CIMSSnackbar';
import CIMSMessageDialog from '../components/Dialog/CIMSMessageDialog';
import MessageSnackbar from '../components/Snackbar/MessageSnackbar/MessageSnackbar';
import CommonErrorDialog from '../views/compontent/commonDialog/commonErrorDialog';
import CommonCircular from '../views/compontent/commonProgress/commonCircular';
import CIMSMultiTabs from '../components/Tabs/CIMSMultiTabs';
import CIMSMultiTab from '../components/Tabs/CIMSMultiTab';
import accessRightEnum from '../enums/accessRightEnum';
import { cleanCommonMessageDetail } from '../store/actions/message/messageAction';
import { updateState as updatePatientState } from '../store/actions/patient/patientAction';
import { addTabs, deleteTabs, changeTabsActive, cleanTabParams } from '../store/actions/mainFrame/mainFrameAction';
import { openCommonMessage } from '../store/actions/message/messageAction';
import { closeWarnSnackbar } from '../store/actions/common/commonAction';
import * as CommonUtilities from '../utilities/commonUtilities';
// import * as MessageUtilities from '../utilities/messageUtilities';
import Loadable from 'react-loadable';
import Loading from './component/loading';
import IndexPatient from './indexPatient';
import NotFound from './component/notfound';
import CommonCircularDialog from '../views/compontent/commonProgress/commonCircularDialog';
import CaseNoDialog from '../views/compontent/caseNoDialog';
import CaseNoSelectDialog from '../views/compontent/caseNoSelectDialog';
import Enum from '../enums/enum';

import NotLiveRoute from 'react-live-route';
import ErrorBoundary from 'components/JErrorBoundary';

import MwecsDialogContainer from 'components/ECS/Mwecs/MwecsDialogContainer';
import EcsDialogContainer from 'components/ECS/Ecs/EcsDialogContainer';

const LiveRoute = withRouter(NotLiveRoute);

const styles = () => ({
  container: {
    padding: '0px 10px'
  }
});

class IndexWarp extends Component {

  constructor(props) {
    super(props);
    this.componentList = [];
    const { accessRights } = this.props;
    accessRights && accessRights.filter(item => item.type === 'function').forEach(right => {
      if (right.isPatRequired !== 'Y') {
        this.componentList.push({
          name: right.name,
          path: right.path,
          // displayOrder: menu.displayOrder,
          component: ErrorBoundary(Loadable({
            // loader: () => import(/* webpackChunkName: "[request]" */`../views${accessRightList[i].componentPath}`),
            loader: () => import(`../views${right.path}`),
            loading: Loading
            // onRef: this.onRef.bind(this)
          }))
        });
      }
    });
  }

  componentDidMount() {
    if (this.props.isLoginSuccess) {
      if (this.props.isTemporaryLogin) {
        this.props.addTabs(this.props.accessRights.find(item => item.name === accessRightEnum.userProfile));
      } else {
        this.props.addTabs({ name: 'patientSpecFunction', label: `${CommonUtilities.getPatientCall()}-specific Function(s)`, disableClose: true, path: 'indexPatient' });
        this.props.history.push('/index/patientSpecFunction');
      }
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.tabsActiveKey !== this.props.tabsActiveKey || prevProps.subTabsActiveKey !== this.props.subTabsActiveKey) {
      if (prevProps.tabsActiveKey !== this.props.tabsActiveKey) {
        if (this.props.tabsActiveKey === 'patientSpecFunction' && this.props.subTabsActiveKey) {
          const subActiveTab = this.props.subTabs.find(item => item.name === this.props.subTabsActiveKey);
          if (subActiveTab) {
            this.props.history.replace({
              pathname: `/index/patientSpecFunction/${this.props.subTabsActiveKey}`,
              state: subActiveTab.params
            });
            if (subActiveTab.params) {
              this.props.cleanTabParams(this.props.subTabsActiveKey);
            }
          }
        } else {
          const activeTab = this.props.tabs.find(item => item.name === this.props.tabsActiveKey);
          if (activeTab) {
            this.props.history.replace({
              pathname: `/index/${this.props.tabsActiveKey}`,
              state: activeTab.params
            });
            if (activeTab.params) {
              this.props.cleanTabParams(this.props.tabsActiveKey);
            }
          }
        }
      } else {
        const subActiveTab = this.props.subTabs.find(item => item.name === this.props.subTabsActiveKey);
        if (subActiveTab) {
          this.props.history.replace({
            pathname: `/index/patientSpecFunction/${this.props.subTabsActiveKey}`,
            state: subActiveTab.params
          });
          if (subActiveTab.params) {
            this.props.cleanTabParams(this.props.subTabsActiveKey);
          }
        }
      }
    }
  }

  componentWillUnmount() {
    this.props.cleanCommonMessageDetail();
    this.props.closeWarnSnackbar();
  }

  handleChangeTab = (e, newValue) => {
    if (newValue == 'patientSpecFunction' && this.props.isOpenReview) {
      return;
    }
    this.props.changeTabsActive(newValue);
  };

  handleDeleteTab = (e, item) => {
    // if (this.props.editModeTabs.indexOf(item.name) > -1) {
    // if (this.props.editModeTabs.find(ele => ele.name === item.name)) {
    //   this.props.openCommonMessage({
    //     msgCode: '110018',
    //     btnActions: {
    //       btn1Click: () => {
    //         this.props.deleteTabs(item.name);
    //       }
    //     }
    //   });
    // } else {
    //   this.props.deleteTabs(item.name);
    // }
    if (typeof item.doCloseFunc === 'function') {
      item.doCloseFunc(
        (success) => {
          if (success) {
            this.props.deleteTabs(item.name);
          }
        });
    }
    else {
      this.props.deleteTabs(item.name);
    }
  };

  getComponentList = () => {
    const { tabs } = this.props;
    let liveTabs = tabs.filter(item => item.name !== 'patientSpecFunction');

    liveTabs.forEach(tab => {
      let depth = tab.displayOrder.length;
      const parentOrders = [];

      for (let i = 0; i <= depth; i + 3) {
        parentOrders.push(tab.displayOrder.subString(i, i + 3));
      }
    });
  }

  getTabLabel = (item) => {
    const { patientInfo, caseNoInfo } = this.props;
    let label = item.label;
    if (item.name === 'patientSpecFunction') {
      label = `${CommonUtilities.getPatientCall()} List`;
      if (patientInfo && patientInfo.patientKey) {
        if (caseNoInfo.caseNo || patientInfo.caseList.filter(item => item.statusCd === Enum.CASE_STATUS.ACTIVE).length === 0) {
          label = `${CommonUtilities.getPatientCall()}-specific Function(s)`;
        }
      }
    }
    return label;
  }

  render() {
    const { classes, patientInfo } = this.props;
    if (!this.props.isLoginSuccess) {
      return <Redirect to={'/'} />;
    }

    return (
      <div className={'main_body'}>
        <MenuBar />
        {/* <CommonHeader /> */}
        <div className={'nephele_main_body'}>
          <CIMSMultiTabs
              value={this.props.tabsActiveKey}
              onChange={this.handleChangeTab}
          >
            {this.props.tabs.map((item) => (
              <CIMSMultiTab
                  disableClose={item.disableClose}
                  key={item.name}
                  label={this.getTabLabel(item)}
                  value={item.name}
                  onClear={e => this.handleDeleteTab(e, item)}
                  tabIndex={-1}
              />
            ))}
          </CIMSMultiTabs>
          <Grid className={'nephele_content_body'}>
            <LiveRoute
                alwaysLive
                path={'/index/patientSpecFunction'}
                component={IndexPatient}
            />
            {
              this.props.tabs.filter(item => item.name !== 'patientSpecFunction').map(item => {
                const asyncComp = this.componentList.find(i => i.name === item.name);
                return asyncComp ?
                  <Grid key={item.name} className={classes.container} style={{ display: this.props.tabsActiveKey === item.name ? '' : 'none' }}>
                    <LiveRoute
                        exact
                        alwaysLive
                        key={asyncComp.name}
                        path={`/index/${asyncComp.name}`}
                        component={asyncComp.component}
                    />
                  </Grid>
                  :
                  <LiveRoute
                      exact
                      alwaysLive
                      key={item.name}
                      path={`/index/${item.name}`}
                      component={NotFound}
                  />;
              })
            }
          </Grid>
        </div>
        <CommonCircular />
        <CommonErrorDialog />
        <CIMSMessageDialog />
        <MessageSnackbar />
        <CommonCircularDialog />
        <MwecsDialogContainer parentPageName={'index'}/>
        <EcsDialogContainer  parentPageName={'index'}/>
        {this.props.openCaseNo ? <CaseNoDialog /> : null}
        {this.props.openSelectCase ? <CaseNoSelectDialog /> : null}
        {
          process.env.NODE_ENV === 'development' ?
            <CIMSSnackbar
                open={this.props.warnSnackbarStatus}
                message={this.props.warnSnackbarMessage}
                close={() => { this.props.closeWarnSnackbar(); }}
            /> : null
        }

      </div >
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoginSuccess: state.login.isLoginSuccess,
    isTemporaryLogin: state.login.isTemporaryLogin === 'Y',
    accessRights: state.login.accessRights,
    tabs: state.mainFrame.tabs,
    tabsActiveKey: state.mainFrame.tabsActiveKey,
    subTabs: state.mainFrame.subTabs,
    subTabsActiveKey: state.mainFrame.subTabsActiveKey,
    editModeTabs: state.mainFrame.editModeTabs,
    patientInfo: state.patient.patientInfo,
    caseNoInfo: state.patient.caseNoInfo,
    warnSnackbarStatus: state.common.warnSnackbarStatus,
    warnSnackbarMessage: state.common.warnSnackbarMessage,
    openCaseNo: state.caseNo.openCaseNo,
    openSelectCase: state.caseNo.openSelectCase,
    isOpenReview: state.registration.isOpenReview
  };
}

const dispatchProps = {
  changeTabsActive,
  addTabs,
  deleteTabs,
  closeWarnSnackbar,
  openCommonMessage,
  cleanTabParams,
  updatePatientState,
  cleanCommonMessageDetail
};
export default withRouter(connect(mapStateToProps, dispatchProps)(withStyles(styles)(IndexWarp)));
