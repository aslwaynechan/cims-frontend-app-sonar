import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { HashRouter as Router } from 'react-router-dom';

import Login from './views/login/login';
import ForgetPassword from './views/forgetPassword/forgetPassword';
import IndexWarp from './layout/index';
// import IndexMoe from './layout/indexMoe';
import ADIWebClientForm from './views/registration/component/adiWebClientForm';
import ADICallback from './utilities/adiCallbackUtilities';

class CIMSRouter extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          {/* <Route path="/login/:serviceCd?/:clinicCd?" component={IndexMoe} strict /> */}
          <Route path="/login/:serviceCd?/:clinicCd?" component={Login} strict />
          <Redirect exact from="/" to="/login" />
          <Route path="/forgetPassword" component={ForgetPassword} />
          <Route path="/index" component={IndexWarp} />
          <Route path="/adi/:url/:json">
            <ADIWebClientForm />
          </Route>
          <Route path="/adiCallback">
            <ADICallback />
          </Route>
        </Switch>
      </Router>
    );
  }
}

export default CIMSRouter;