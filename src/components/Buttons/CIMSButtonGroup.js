
import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import CIMSButton from './CIMSButton';

const style = () => ({
    leftFooter: {
        position: 'fixed',
        left: 10,
        bottom: 30,
        width: 'auto',
        zIndex: 999,
        padding: '5px 10px'
    },
    rightFooter: {
        position: 'fixed',
        right: 10,
        bottom: 30,
        width: 'auto',
        zIndex: 999,
        padding: '5px 10px'
    }
});

class CIMSButtonGroup extends Component {
    render() {
        const { buttonConfig, isLeft, classes, customStyle } = this.props;
        return (
            <Grid container className={isLeft ? classes.leftFooter : classes.rightFooter} style={customStyle ? customStyle : null}>
                {
                    buttonConfig && buttonConfig.map(item => {
                        let { id, name, ...rest } = item;
                        return (
                            <CIMSButton
                                id={id}
                                key={id}
                                children={name}
                                {...rest}
                            />
                        );
                    })
                }
            </Grid>
        );

    }
}

export default withStyles(style)(CIMSButtonGroup);
