import React from 'react';
import {FormControl,InputLabel,Grid} from '@material-ui/core';

export default function Form({onSubmit,children,...rest}){
  const handleSubmit=(e)=>{
    let form=e.target;
    let formData={};
    for (let i=0;i<form.length;i++){
      let name=form[i].name;
      if(name){
        formData[name]=form[name].value;
      }
    }
    e.preventDefault();
    onSubmit(formData,e);
  };

  const isNeedLabel=(item)=>{
    if(item.props){
      return !item.props['no-need-input-label'];
    }
    return false;
  };
  return (
    <form onSubmit={handleSubmit} {...rest} >
      <Grid container spacing={4}>
      {
        children.map((item,index)=>{
          if(!item){return false;}
          return (
            <Grid item lg={2} md={3} xs={4} key={index}>
            <FormControl style={{minWidth:'120px',width:'100%'}}>
              {isNeedLabel(item)&&<InputLabel>{item.props.label}</InputLabel>}
              <item.type {...item.props}/>
            </FormControl>
            </Grid>
          );
        })
      }
      </Grid>
    </form>
  );
}
