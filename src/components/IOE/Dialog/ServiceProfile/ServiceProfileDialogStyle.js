export const styles = () => ({
  paper: {
    minWidth: 1700
  },
  dialogTitle: {
    lineHeight: 1.6,
    fontWeight: 'bold',
    fontSize: '1.5rem',
    fontFamily: 'Arial',
    paddingTop: '5px',
    paddingBottom: '5px',
    backgroundColor: '#b8bcb9'
  },
  dialogContent: {
    padding: '10px 24px',
    backgroundColor: 'white',
    overflowY: 'hidden'
  },
  dialogActions: {
    margin:0,
    padding:10,
    paddingTop: 0,
    backgroundColor:'white'
  },
  remark: {
    margin: 0,
    marginLeft: 14
  },
  itefSign:{
    color: '#0579c8'
  },
  iteoSign:{
    color: '#4caf50'
  }
});