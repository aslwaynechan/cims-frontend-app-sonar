import React, { Component } from 'react';
import { withStyles, Dialog, Paper, DialogTitle, DialogContent, Typography, DialogActions, Grid, Button } from '@material-ui/core';
import { CheckCircle, HighlightOff } from '@material-ui/icons';
import Draggable from 'react-draggable';
import { styles } from './InfoDialogStyle';
import InputBoxField from '../../components/InputBoxField/InputBoxField';
import * as ServiceProfileConstants from '../../../../../../constants/IOE/serviceProfile/serviceProfileConstants';
import * as utils from '../../utils/dialogUtils';

function PaperComponent(props) {
  return (
    <Draggable
        enableUserSelectHack={false}
        onStart={(e)=>{
          return e.target.getAttribute('customdraginfo') === 'allowed';
        }}
    >
      <Paper {...props} />
    </Draggable>
  );
}

class InfoDialog extends Component {
  handleAction = action => {
    let { orderNumber,selectedOrderKey,orderIsEdit,selectedLabId,selectedFormId, middlewareObject, temporaryStorageMap, updateState,handleInfoDialogCancel,handleResetOrderNumber } = this.props;
    if (action === ServiceProfileConstants.INFO_DIALOG_ACTION_TYPE.CANCEL) {
      middlewareObject.infoValMap = middlewareObject.backupInfoValMap;
    }
    if (orderIsEdit) {
      // Edit
      let targetObj = temporaryStorageMap.get(selectedOrderKey);
      let obj = utils.initTemporaryStorageObj(middlewareObject,targetObj.testGroup,selectedLabId);
      temporaryStorageMap.set(selectedOrderKey,obj);
    } else {
      // Add
      for (let i = 0; i < orderNumber; i++) {
        let currentTestGroup = temporaryStorageMap.size+1; //seq
        let obj = utils.initTemporaryStorageObj(middlewareObject,currentTestGroup,selectedLabId);
        let timestamp = new Date().valueOf();
        temporaryStorageMap.set(`${selectedFormId}_${timestamp}_${i}`,obj);
      }
    }
    delete middlewareObject.backupInfoValMap;
    updateState&&updateState({
      selectedOrderKey:null,
      orderIsEdit:false,
      temporaryStorageMap,
      middlewareObject
    });
    handleResetOrderNumber&&handleResetOrderNumber();
    handleInfoDialogCancel&&handleInfoDialogCancel();
  }

  generateFieldByType = (item) => {
    const { classes,middlewareObject,updateState } = this.props;
    let itemType = item.frmItemTypeCd;
    let fieldProps = {
      id:item.codeIoeFormItemId,
      middlewareObject,
      itemValType:ServiceProfileConstants.ITEM_VALUE.TYPE1, //default:value1
      categoryType:ServiceProfileConstants.ITEM_CATEGORY_TYPE.INFO,
      updateState
    };
    let element = null;
    if(itemType==ServiceProfileConstants.FORM_ITEM_TYPE.INPUT_BOX){
      fieldProps.maxLength = item.fieldLength;
      fieldProps.sideEffect = utils.handleInfoOperationType;
      fieldProps.disabledFlag = false;
      element = (
        <div className={classes.itemGrid}>
          <InputBoxField {...fieldProps} />
        </div>
      );
    }
    return element;
  }

  generateContents = () => {
    const { classes,selectedLabId,selectedFormId,frameworkMap } = this.props;
    let infoItemsMap = frameworkMap.get(selectedLabId).formMap.get(selectedFormId).infoItemsMap;
    let gridItems = [];
    let items = infoItemsMap.get(ServiceProfileConstants.ITEM_CATEGORY_TYPE.INFO);
    gridItems = items.map(item=>{
      let element = this.generateFieldByType(item);
      return (
        <div key={`${selectedLabId}_${selectedFormId}_${item.codeIoeFormItemId}`} className={classes.itemWrapper}>
          <Grid item xs={3} classes={{'grid-xs-3':classes.itemNameGrid}}>
            <Typography component="div" variant="subtitle1" classes={{subtitle1:classes.itemNameTypography}}>
              {item.frmItemName}
            </Typography>
          </Grid>
          <Grid item xs={9}>
            {element}
          </Grid>
        </div>
      );
    });
    return (
      <Grid container direction="row" justify="center" alignItems="center">
        {gridItems}
      </Grid>
    );
  }

  render() {
    const {
      classes,
      isOpen=false,
      dialogTitle=''
    } = this.props;
    return (
      <Dialog
          classes={{paper: classes.paper}}
          fullWidth
          maxWidth="md"
          open={isOpen}
          scroll="body"
          PaperComponent={PaperComponent}
          onEscapeKeyDown={()=>{this.handleAction(ServiceProfileConstants.INFO_DIALOG_ACTION_TYPE.CANCEL);}}
      >
        {/* title */}
        <DialogTitle
            className={classes.dialogTitle}
            disableTypography
            customdraginfo="allowed"
        >
          {dialogTitle}
        </DialogTitle>
        {/* content */}
        <DialogContent classes={{'root':classes.dialogContent}}>
          <Typography component="div">
            <Paper elevation={1}>
              {this.generateContents()}
            </Paper>
          </Typography>
        </DialogContent>
        {/* button group */}
        <DialogActions className={classes.dialogActions}>
          <Grid container direction="row" justify="flex-end" alignItems="center">
            <Button
                classes={{
                  'root':classes.btnRoot,
                  'label':classes.btnLabel
                }}
                color="primary"
                id="btn_service_profile_info_dialog_ok"
                onClick={()=>{this.handleAction(ServiceProfileConstants.INFO_DIALOG_ACTION_TYPE.OK);}}
                variant="contained"
            >
              <CheckCircle className={classes.btnIcon}/>
              OK
            </Button>
            <Button
                classes={{
                  'root':classes.btnRoot,
                  'label':classes.btnLabel
                }}
                id="btn_service_profile_info_dialog_cancel"
                onClick={()=>{this.handleAction(ServiceProfileConstants.INFO_DIALOG_ACTION_TYPE.CANCEL);}}
                variant="contained"
            >
              <HighlightOff className={classes.btnIcon}/>
              Cancel
            </Button>
          </Grid>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles)(InfoDialog);
