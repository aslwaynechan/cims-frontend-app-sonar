import React, { Component } from 'react';
import { withStyles, AppBar, Toolbar, IconButton, Typography, Drawer, Divider, List, ListItem, ListItemIcon, ListItemText, Tooltip, Grid, Fab, Avatar } from '@material-ui/core';
import { styles } from './TabContainerStyle';
import classNames from 'classnames';
import { Menu, ChevronLeft, ArrowForward, TextRotationNone } from '@material-ui/icons';
import TestContainer from '../TestContainer/TestContainer';
import SpecimenContainer from '../SpecimenContainer/SpecimenContainer';
import ValidatorForm from '../../../../../FormValidator/ValidatorForm';
import * as utils from '../../utils/dialogUtils';
import { cloneDeep } from 'lodash';
import CustomizedSelectFieldValidator from '../../../../../Select/CustomizedSelect/CustomizedSelectFieldValidator';
import * as serviceProfileConstants from '../../../../../../constants/IOE/serviceProfile/serviceProfileConstants';
import CIMSMultiTabs from '../../../../../Tabs/CIMSMultiTabs';
import CIMSMultiTab from '../../../../../Tabs/CIMSMultiTab';

class TabContainer extends Component {
  constructor(props){
    super(props);
    let { selectedFormId } = props;
    this.state={
      open: true,
      selectedFormId: selectedFormId
    };
  }

  handleDrawerClick = labId => {
    let { lab2FormMap,frameworkMap,updateStateWithoutStatus,updateGroupingContainerState } = this.props;
    let selectedFormId = lab2FormMap.get(labId)[0];
    let formObj = frameworkMap.get(labId).formMap.get(selectedFormId);
    let valObj = utils.initMiddlewareObject(formObj);

    updateStateWithoutStatus&&updateStateWithoutStatus({
      orderIsEdit:false,
      selectedOrderKey: null,
      middlewareObject:valObj
    });
    updateGroupingContainerState&&updateGroupingContainerState({
      selectedFormId,
      labValue:labId
    });
  };

  generateDrawerList = () => {
    const { classes,selectedLabId,frameworkMap } = this.props;
    let list = [];
    for (let labId of frameworkMap.keys()) {
      list.push(
        <Tooltip
            key={labId}
            title={labId}
            classes={{
              tooltip:classes.tooltip
            }}
        >
          <ListItem
              button
              onClick={() => {this.handleDrawerClick(labId);}}
              className={classNames({
                [classes.selectedItem]: labId === selectedLabId
              })}
          >
            <ListItemIcon
                className={
                  classNames({
                    [classes.marginRightNone]: open
                  })
                }
            >
              <Avatar className={classes.avatar}>{labId.substring(0,2)}</Avatar>
              {/* <Assessment /> */}
            </ListItemIcon>
            <ListItemText
                primary={
                  <Typography className={classes.font} noWrap>
                    {labId}
                  </Typography>
                }
            />
          </ListItem>
        </Tooltip>
      );
    }
    return list;
  }

  handleDrawerOpen = () => {
    this.setState({
      open: true
    });
  };

  handleDrawerClose = () => {
    this.setState({
      open: false
    });
  };

  handleAddOrder = () => {
    const { orderNumber,selectedOrderKey,selectedLabId,selectedFormId,orderIsEdit, middlewareObject, temporaryStorageMap, updateState, openCommonMessage,updateGroupingContainerState } = this.props;
    //validate
    let msgCode = utils.handleValidateItems(middlewareObject);
    if (msgCode === '') {
      if (orderIsEdit) {
        // Edit
        let targetObj = temporaryStorageMap.get(selectedOrderKey);
        let obj = utils.initTemporaryStorageObj(middlewareObject,targetObj.testGroup,selectedLabId);
        temporaryStorageMap.set(selectedOrderKey,obj);
      } else {
        // Add
        for (let i = 0; i < orderNumber; i++) {
          let currentTestGroup = temporaryStorageMap.size+1;
          let obj = utils.initTemporaryStorageObj(middlewareObject,currentTestGroup,selectedLabId);
          let timestamp = new Date().valueOf();
          temporaryStorageMap.set(`${selectedFormId}_${timestamp}_${i}`,obj);
        }
      }
      updateState&&updateState({
        selectedOrderKey:null,
        orderIsEdit:false,
        temporaryStorageMap
      });
      updateGroupingContainerState&&updateGroupingContainerState({
        orderNumber:serviceProfileConstants.ORDER_NUMBER_OPTIONS[0].value
      });
    } else {
      let payload = {
        msgCode
      };
      openCommonMessage&&openCommonMessage(payload);
    }
  }

  handleAddOrderWithInfo = () => {
    let { updateGroupingContainerState,openCommonMessage,middlewareObject,selectedLabId,selectedFormId,updateState } = this.props;
    //validate
    let msgCode = utils.handleValidateItems(middlewareObject);
    if (msgCode === '') {
      //backup info
      middlewareObject.backupInfoValMap = cloneDeep(middlewareObject.infoValMap);
      updateState&&updateState({
        middlewareObject
      });
      updateGroupingContainerState&&updateGroupingContainerState({
        isOpen: true,
        infoTargetLabId:selectedLabId,
        infoTargetFormId:selectedFormId
      });
    } else {
      let payload = {
        msgCode
      };
      openCommonMessage&&openCommonMessage(payload);
    }
  }

  handleOrderNumberChange = event => {
    const { handleOrderNumberChange } = this.props;
    handleOrderNumberChange&&handleOrderNumberChange(event);
  }

  changeTabValue = (event, value) => {
    let { frameworkMap,selectedLabId,updateStateWithoutStatus,updateGroupingContainerState } = this.props;
    let formObj = frameworkMap.get(selectedLabId).formMap.get(value);
    let valObj = utils.initMiddlewareObject(formObj);
    updateStateWithoutStatus&&updateStateWithoutStatus({
      orderIsEdit:false,
      selectedOrderKey: null,
      middlewareObject:valObj
    });
    updateGroupingContainerState&&updateGroupingContainerState({
      selectedFormId: value
    });
  }

  generateTab = () => {
    const { classes,selectedLabId,frameworkMap } = this.props;
    let { selectedFormId } = this.state;
    let tabs = [];
    if (selectedLabId!==null&&frameworkMap.size > 0) {
      let formMap = frameworkMap.get(selectedLabId).formMap;
      for (let [formId,formObj] of formMap) {
        tabs.push(
          <CIMSMultiTab
              key={formId}
              id={`form_tab_${formId}`}
              value={formId}
              disableClose
              className={selectedFormId === formId ? 'tabSelected' : 'tabNavigation'}
              label={
                <span className={classes.tabSpan}>
                {/* <span style={{ fontSize: 16, textTransform: 'none' }}> */}
                  {formObj.formShortName}
                </span>
              }
          />
        );
      }
    }
    return tabs;
  }

  judgeContainerType = (testFrameworkMap,specimenFrameworkMap) => {
    let types = [];
    if (testFrameworkMap.size > 0) {
      types.push(serviceProfileConstants.ITEM_CATEGORY_TYPE.TEST);
    }
    if (specimenFrameworkMap.size > 0) {
      types.push(serviceProfileConstants.ITEM_CATEGORY_TYPE.SPECIMEN);
    }
    return types;
  }

  render() {
    const {
      classes,
      frameworkMap,
      selectedLabId,
      selectedFormId,
      dropdownMap,
      middlewareObject,
      updateState,
      orderIsEdit,
      orderNumber
    } = this.props;
    let { open } = this.state;

    let testFrameworkMap = frameworkMap!==undefined&&frameworkMap.size>0?frameworkMap.get(selectedLabId).formMap.get(selectedFormId).testItemsMap:new Map();
    let specimenFrameworkMap = frameworkMap!==undefined&&frameworkMap.size>0?frameworkMap.get(selectedLabId).formMap.get(selectedFormId).specimenItemsMap:new Map();
    let types = this.judgeContainerType(testFrameworkMap,specimenFrameworkMap);

    let testContainerProps = {
      selectedLabId,
      selectedFormId,
      dropdownMap,
      testFrameworkMap,
      middlewareObject,
      updateState
    };
    let specimenContainerProps = {
      selectedLabId,
      selectedFormId,
      dropdownMap,
      middlewareObject,
      specimenFrameworkMap,
      updateState
    };
    return (
      <div>
        <AppBar
            className={classNames(classes.appBar, {
              [classes.appBarShift]: open
            })}
            position="relative"
        >
          <Toolbar
              disableGutters={!open}
              classes={{
                regular:classes.toolBar
              }}
          >
            <IconButton
                aria-label="Open drawer"
                className={classNames(classes.menuButton, {
                  [classes.hide]: open
                })}
                color="inherit"
                onClick={this.handleDrawerOpen}
            >
              <Menu />
            </IconButton>
            <Typography
                className={classNames(classes.font,classes.fontBold)}
                variant="h6"
                color="inherit"
                noWrap
            >
              {selectedLabId}
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer
            classes={{
              root: classes.drawerRoot,
              paper: classNames(classes.drawerPaperRoot, {
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open
              })
            }}
            className={classNames(classes.drawer, {
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open
            })}
            open={open}
            variant="permanent"
        >
          <div>
            <IconButton onClick={this.handleDrawerClose}>
              <ChevronLeft />
            </IconButton>
          </div>
          <Divider />
          <List className={classes.listRoot}>
            {this.generateDrawerList()}
          </List>
        </Drawer>
        {/* list content */}
        <ValidatorForm id="GroupForm" onSubmit={()=>{}} ref="form">
          <div
              className={classNames(classes.content, {
                [classes.contentOpen]: open
              })}
          >
            {/* Top Tab */}
            <CIMSMultiTabs
                value={selectedFormId}
                onChange={this.changeTabValue}
                indicatorColor="primary"
            >
              {this.generateTab()}
            </CIMSMultiTabs>
            {/* Content */}
            <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="flex-start"
            >
              {
                types.length>1?(
                  [
                    (
                      <Grid item xs={6} key={`${selectedLabId}_${selectedFormId}_specimen`} classes={{'grid-xs-6':classes.specimenWrapper}}>
                        <SpecimenContainer {...specimenContainerProps} />
                      </Grid>
                    ),
                    (
                      <Grid item xs={6} key={`${selectedLabId}_${selectedFormId}_test`} classes={{'grid-xs-6':classes.testWrapper}}>
                        <TestContainer {...testContainerProps} />
                      </Grid>
                    )
                  ]
                ):(
                  types.length === 1?(
                    types[0] === serviceProfileConstants.ITEM_CATEGORY_TYPE.TEST?(
                      <Grid item xs={12} classes={{'grid-xs-12':classes.fullWrapper}}>
                        <TestContainer {...testContainerProps} displayHeader />
                      </Grid>
                    ):(
                      types[0] === serviceProfileConstants.ITEM_CATEGORY_TYPE.SPECIMEN?(
                        <Grid item xs={12} classes={{'grid-xs-12':classes.fullWrapper}}>
                          <SpecimenContainer {...specimenContainerProps} displayHeader />
                        </Grid>
                      ):null
                    )
                  ):null
                )
              }
            </Grid>
          </div>
          {/* action */}
          <div className={classes.actionWrapper}>
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                className={classes.fabGird}
            >
              <Grid item style={{width:82,marginBottom:20}}>
                <CustomizedSelectFieldValidator
                    id="service_profile_order_number_dropdown"
                    options={serviceProfileConstants.ORDER_NUMBER_OPTIONS.map(option=>{
                      return {
                        label: option.label,
                        value: option.value
                      };
                    })}
                    isDisabled={orderIsEdit}
                    value={orderNumber}
                    onChange={event => {this.handleOrderNumberChange(event);}}
                    styles={{ menuPortal: base => ({ ...base, zIndex: 1600 }) }}
                    menuPortalTarget={document.body}
                />
              </Grid>
              <Grid item>
                <Tooltip title="Add Order" classes={{tooltip:classes.tooltip}}>
                  <Fab
                      size="small"
                      color="primary"
                      aria-label="Add Order"
                      id="btn_service_profile_dialog_add_order"
                      className={classes.fab}
                      onClick={()=>{this.handleAddOrder();}}
                  >
                    <ArrowForward />
                  </Fab>
                </Tooltip>
              </Grid>
              <Grid item>
                <Tooltip title="Add Order with Info" classes={{tooltip:classes.tooltip}}>
                  <Fab
                      size="small"
                      color="primary"
                      aria-label="Add Order with Info"
                      id="btn_service_profile_dialog_add_order_with_info"
                      className={classes.fab}
                      onClick={()=>{this.handleAddOrderWithInfo();}}
                  >
                    <TextRotationNone />
                  </Fab>
                </Tooltip>
              </Grid>
            </Grid>
          </div>
        </ValidatorForm>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(TabContainer);
