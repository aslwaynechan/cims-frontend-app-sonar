import { COMMON_STYLE } from '../../../../../../constants/commonStyleConstant';

export const styles = () => ({
  paper: {
    minWidth: 1100
  },
  dialogTitle: {
    lineHeight: 1.6,
    fontWeight: 'bold',
    fontSize: '1.5rem',
    fontFamily: 'Arial',
    paddingTop: '5px',
    paddingBottom: '5px',
    backgroundColor: '#b8bcb9'
  },
  dialogContent: {
    padding: '10px 24px',
    backgroundColor: 'white',
    overflowY: 'hidden'
  },
  dialogActions: {
    margin:'0px' ,
    padding:'0 10px' ,
    backgroundColor:'white'
  },
  numberSpan: {
    fontWeight: 'bold',
    paddingLeft: 16
  },
  headRowStyle:{
    height: 50
  },
  headCellStyle:{
    color:'white',
    overflow: 'hidden',
    fontWeight: 'bold',
    fontSize: '1rem',
    fontFamily: 'Arial',
    position:'sticky',
    top:0,
    backgroundColor:COMMON_STYLE.TABLE_BGCKGROUNDCOLOR
  },
  customRowStyle:{
    fontSize: '1rem',
    fontFamily: 'Arial',
    whiteSpace: 'pre-line',
    wordWrap: 'break-word',
    wordBreak: 'break-all'
  },
  flexCenter: {
    display: 'flex',
    alignItems: 'center',
    marginRight: 20
  },
  searchLabel: {
    marginRight: 10
  },
  searchBtn: {
    float:'right'
  },
  actionWrapper:{
    marginRight: 12
  },
  searchDiv: {
    position: 'sticky',
    backgroundColor: '#ffffff'
  },
  tableDiv: {
    maxHeight: 600,
    overflowY: 'auto',
    marginTop: -5
  }
});