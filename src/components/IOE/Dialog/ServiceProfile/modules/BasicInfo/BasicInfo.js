import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles, TextField, Checkbox, FormHelperText } from '@material-ui/core';
import { styles } from './BasicInfoStyle';
import classNames from 'classnames';
import { ErrorOutline } from '@material-ui/icons';
import { trim } from 'lodash';

class BasicInfo extends Component {

  handleTextFieldChanged = event => {
    const { updateState } = this.props;
    updateState&&updateState({
      templateName: event.target.value
    });
  }

  handleTextFieldBlur = event => {
    const { updateState } = this.props;
    updateState&&updateState({
      templateNameErrorFlag: trim(event.target.value)===''?true:false
    });
  }

  handleCheckBoxChanged = event => {
    const { updateState } = this.props;
    updateState&&updateState({
      isActive: event.target.checked
    });
  }

  render() {
    const { classes,templateName,isActive,templateNameErrorFlag } = this.props;
    let inputProps = {
      autoCapitalize:'off',
      variant:'outlined',
      type:'text',
      inputProps: {
        style:{
          fontSize: '1rem',
          fontFamily: 'Arial'
        }
      }
    };
    return (
      <div className={classNames(classes.wrapper,classes.flexCenter)}>
        <div className={classNames(classes.floatLeft)}>
          <div className={classes.flexCenter}>
            <label className={classes.label}>
              Template Name<span className={classes.required}>*</span>
            </label>
            <TextField
                id="input_service_profile_dialog_template_name"
                error={templateNameErrorFlag}
                onChange={this.handleTextFieldChanged}
                // onBlur={this.handleTextFieldBlur}
                value={templateName}
                {...inputProps}
            />
          </div>
          <div className={classes.errorWrapper}>
            {templateNameErrorFlag?(
              <FormHelperText id="error_service_profile_template_name" error classes={{root:classes.helperError}}>
                <ErrorOutline className={classes.errorIcon} />
                This field is required.
              </FormHelperText>
            ):null}
          </div>
        </div>
        <div className={classNames(classes.floatLeft,classes.activeWrapper)}>
          <label className={classes.label}>Active</label>
          <Checkbox
              id="checkbox_service_profile_dialog_template_active"
              onChange={this.handleCheckBoxChanged}
              color="primary"
              checked={isActive}
              className={classes.checkbox}
          />
        </div>
      </div>
    );
  }
}

export default connect()(withStyles(styles)(BasicInfo));
