export const styles = () => ({
  tabSpan: {
    fontSize: '1rem',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: '#404040'
  },
  tabSpanSelected: {
    fontWeight: 'bold',
    color: '#FFFFFF',
    '&:hover':{
      color: '#404040'
    }
  },
  tabSelect: {
    backgroundColor: '#0579c8'
  },
  label: {
    fontWeight: 'bold',
    paddingRight: 10
  },
  flexCenter: {
    display: 'flex',
    alignItems: 'center'
  },
  tabs: {
    width: 500,
    float: 'left'
  },
  searchBar: {
    float: 'right'
  },
  searchBtn: {
    margin: '0 8px'
  }
});