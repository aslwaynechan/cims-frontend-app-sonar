export const styles = () => ({
  wrapper: {
    height: 510,
    overflowY: 'hidden',
    paddingRight: 5
  },
  wrapperHidden: {
    height: 535
  },
  cardContainer: {
    columnCount: '1',
    columnGap: 0,
    height: 494,
    paddingTop: 10
  },
  cardContainerHidden: {
    height: 510
  },
  card: {
    marginBottom: 10,
    marginRight: 10,
    marginLeft: 10,
    breakInside: 'avoid',
    boxShadow: 'none',
    border: '1px solid #0579C8'
  },
  cardContent: {
    padding: '5px 10px',
    '&:last-child':{
      paddingBottom: '5px'
    }
  },
  groupNameTitle: {
    cursor: 'default',
    fontSize: '1.125rem',
    fontWeight: 'bold'
  },
  itemWrapperDiv: {
    display: 'flex',
    flexDirection: 'row'
  },
  itemTypography: {
    fontSize: '1rem',
    fontFamily: 'Arial'
  },
  checkBoxDiv: {
    float: 'left'
  },
  formItemDiv: {
    paddingLeft: 5,
    width: '100%'
  },
  title: {
    fontSize: '1rem',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  hiddenTitle: {
    display: 'none'
  }
});