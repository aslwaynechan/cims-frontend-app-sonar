import {COMMON_ACTION_TYPE} from '../../../../../constants/common/commonConstants';
import * as ServiceProfileConstants from '../../../../../constants/IOE/serviceProfile/serviceProfileConstants';
import { isNull,union,findIndex,find } from 'lodash';
import { SERVICE_PROFILE_MAINTENANCE_CODE } from '../../../../../constants/message/IOECode/serviceProfileMaintenanceCode';

let generateMasterTestMap = (testItemsMap) => {
  let tempMap = new Map();
  if (testItemsMap.size > 0) {
    for (let items of testItemsMap.values()) {
      items.forEach(item => {
        if (item.ioeMasterTest === ServiceProfileConstants.TEST_ITEM_MASTER_TEST_FLAG) {
          tempMap.set(item.codeIoeFormItemId,[]);
        } else if (!isNull(item.ioeMasterId)) {
          if (tempMap.has(item.ioeMasterId)) {
            let tempIds = tempMap.get(item.ioeMasterId);
            tempIds.push(item.codeIoeFormItemId);
            tempMap.set(item.ioeMasterId,tempIds);
          } else {
            tempMap.set(item.ioeMasterId,[item.codeIoeFormItemId]);
          }
        }
      });
    }
  }
  return tempMap;
};

let generateValMap = (itemsMap,type,formId,storageObjMap) => {
  let valMap = new Map();
  if (itemsMap.size>0) {
    for (let items of itemsMap.values()) {
      items.forEach(item => {
        let obj = {
          codeIoeFormItemId:item.codeIoeFormItemId,
          testGroup: 0, //default group
          operationType: null,
          version: null,
          itemVal: null,
          itemVal2: null,
          codeIoeFormId:formId,
          ioeType: item.ioeType,
          isChecked: type === ServiceProfileConstants.ITEM_CATEGORY_TYPE.INFO?true:false, //Click box status
          itemName: item.frmItemName,
          createdBy:null,
          createdDtm:null,
          updatedBy:null,
          updatedDtm:null,
          frmItemTypeCd: item.frmItemTypeCd,
          frmItemTypeCd2: item.frmItemTypeCd2,
          ioeTestTemplateId: null, // template id
          ioeTestTemplateItemId:null  // template item id
        };
        if (storageObjMap.has(item.codeIoeFormItemId)) {
          let valObj = storageObjMap.get(item.codeIoeFormItemId);
          obj.testGroup = valObj.testGroup;
          obj.operationType = valObj.operationType;
          obj.version = valObj.version;
          obj.itemVal = valObj.itemVal;
          obj.itemVal2 = valObj.itemVal2;
          obj.isChecked = true;
          obj.createdBy = valObj.createdBy;
          obj.createdDtm = valObj.createdDtm;
          obj.updatedBy = valObj.updatedBy;
          obj.updatedDtm = valObj.updatedDtm;
          obj.ioeTestTemplateId = valObj.ioeTestTemplateId;
          obj.ioeTestTemplateItemId = valObj.ioeTestTemplateItemId;
        }
        valMap.set(item.codeIoeFormItemId,obj);
      });
    }
  }
  return valMap;
};

export function initMiddlewareObject(formObj,storageObj=null) {
  let valObj = {
    codeIoeFormId:null,
    formShortName:'',
    testValMap:new Map(),
    specimenValMap:new Map(),
    infoValMap:new Map(),
    masterTestMap:new Map()
  };
  if (!isNull(formObj)) {
    let {codeIoeFormId,testItemsMap,specimenItemsMap,infoItemsMap,formShortName} = formObj;
    let testStorageObjMap = new Map();
    let specimenStorageObjMap = new Map();
    let infoStorageObjMap = new Map();
    if (!isNull(storageObj)) {
      testStorageObjMap = storageObj.testItemsMap;
      specimenStorageObjMap = storageObj.specimenItemsMap;
      infoStorageObjMap = storageObj.infoItemsMap;
    }
    valObj.codeIoeFormId = codeIoeFormId;
    valObj.formShortName = formShortName;
    valObj.testValMap = generateValMap(testItemsMap,ServiceProfileConstants.ITEM_CATEGORY_TYPE.TEST,codeIoeFormId,testStorageObjMap);
    valObj.specimenValMap = generateValMap(specimenItemsMap,ServiceProfileConstants.ITEM_CATEGORY_TYPE.SPECIMEN,codeIoeFormId,specimenStorageObjMap);
    valObj.infoValMap = generateValMap(infoItemsMap,ServiceProfileConstants.ITEM_CATEGORY_TYPE.INFO,codeIoeFormId,infoStorageObjMap);
    valObj.masterTestMap = generateMasterTestMap(testItemsMap);
  }
  return valObj;
}

let transformValMap2StorageMap = (itemsMap,testGroup) => {
  let valMap = new Map();
  if (itemsMap.size>0) {
    for (let item of itemsMap.values()) {
      if (item.isChecked||!isNull(item.operationType)) {
        valMap.set(item.codeIoeFormItemId,{
          codeIoeFormItemId:item.codeIoeFormItemId,
          testGroup: testGroup,
          operationType: item.operationType,
          version: item.version,
          itemVal: item.itemVal,
          itemVal2: item.itemVal2,
          codeIoeFormId: item.codeIoeFormId,
          itemName: item.itemName,
          createdBy: item.createdBy,
          createdDtm: item.createdDtm,
          updatedBy: item.updatedBy,
          updatedDtm: item.updatedDtm,
          frmItemTypeCd: item.frmItemTypeCd,
          frmItemTypeCd2: item.frmItemTypeCd2,
          ioeTestTemplateId: item.ioeTestTemplateId, // template id
          ioeTestTemplateItemId: item.ioeTestTemplateItemId // template item id
        });
      }
    }
  }
  return valMap;
};

export function initTemporaryStorageObj(middleObj,testGroup,labId) {
  let obj = {
    codeIoeFormId: middleObj.codeIoeFormId,
    testGroup,
    labId,
    formShortName: middleObj.formShortName,
    testItemsMap: transformValMap2StorageMap(middleObj.testValMap,testGroup),
    specimenItemsMap: transformValMap2StorageMap(middleObj.specimenValMap,testGroup),
    infoItemsMap: transformValMap2StorageMap(middleObj.infoValMap,testGroup)
  };
  return obj;
}

export function handleClickBoxOperationType(valObj) {
  let { version, isChecked } = valObj;
  if (!isNull(version)) {
    if (isChecked) {
      valObj.operationType = COMMON_ACTION_TYPE.UPDATE;
    } else {
      valObj.operationType = COMMON_ACTION_TYPE.DELETE;
    }
  } else if (isNull(version)&&isChecked) {
    valObj.operationType = COMMON_ACTION_TYPE.INSERT;
  } else {
    valObj.operationType = null;
  }
}

export function handleInputBoxOperationType(valObj) {
  let { version, itemVal, itemVal2 } = valObj;
  if (!isNull(version)) {
    valObj.operationType = COMMON_ACTION_TYPE.UPDATE;
  } else if (isNull(version)&&(!!itemVal || !!itemVal2)) {
    valObj.operationType = COMMON_ACTION_TYPE.INSERT;
  } else {
    valObj.operationType = null;
  }
}

export function handleDropdownOperationType(valObj) {
  let { version, itemVal, itemVal2 } = valObj;
  if (!isNull(version)) {
    valObj.operationType = COMMON_ACTION_TYPE.UPDATE;
  } else if (isNull(version)&&(!!itemVal || !!itemVal2)) {
    valObj.operationType = COMMON_ACTION_TYPE.INSERT;
  } else {
    valObj.operationType = null;
  }
}

export function handleInfoOperationType(valObj) {
  let { version, itemVal } = valObj;
  if (!isNull(version)) {
    if (!!itemVal) {
      valObj.operationType = COMMON_ACTION_TYPE.UPDATE;
    } else if (itemVal === '') {
      valObj.operationType = COMMON_ACTION_TYPE.DELETE;
    }
  } else if (isNull(version)&&(!!itemVal)) {
    valObj.operationType = COMMON_ACTION_TYPE.INSERT;
  } else {
    valObj.operationType = null;
  }
}

let sortItemsMap = (itemsMap,testGroup) => {
  if (itemsMap.size > 0) {
    for (let valObj of itemsMap.values()) {
      valObj.testGroup = testGroup;
      if (!isNull(valObj.version)) {
        valObj.operationType = COMMON_ACTION_TYPE.UPDATE;
      }
    }
  }
  return itemsMap;
};

export function handleOrderDeleted(temporaryStorageMap) {
  if (temporaryStorageMap.size>0) {
    let seq = 1;
    for (let valObj of temporaryStorageMap.values()) {
      if (valObj.testGroup !== seq) {
        valObj.testGroup = seq;
        valObj.testItemsMap = sortItemsMap(valObj.testItemsMap,seq);
        valObj.specimenItemsMap = sortItemsMap(valObj.specimenItemsMap,seq);
        valObj.infoItemsMap = sortItemsMap(valObj.infoItemsMap,seq);
      }
      seq++;
    }
  }
}

let setDeletedItemsMap = (itemsMap) => {
  if (itemsMap.size > 0) {
    for (let valObj of itemsMap.values()) {
      valObj.operationType = COMMON_ACTION_TYPE.DELETE;
    }
  }
  return itemsMap;
};

export function handledeletedStorageObj(deletedStorageObj) {
  let { testItemsMap, specimenItemsMap, infoItemsMap } = deletedStorageObj;
  deletedStorageObj.testItemsMap = setDeletedItemsMap(testItemsMap);
  deletedStorageObj.specimenItemsMap = setDeletedItemsMap(specimenItemsMap);
  deletedStorageObj.infoItemsMap = setDeletedItemsMap(infoItemsMap);
  return deletedStorageObj;
}

export function handleValidateItems(middlewareObject) {
  let {testValMap,specimenValMap} = middlewareObject;
  let msgCode = '';
  //1.test
  if (testValMap.size > 0) {
    let checkedIoeTypeArray = [];
    for (let valObj of testValMap.values()) {
      if (valObj.isChecked) {
        checkedIoeTypeArray.push(valObj.ioeType);
      }
    }
    if (checkedIoeTypeArray.length === 0) {
      msgCode = SERVICE_PROFILE_MAINTENANCE_CODE.DIALOG_TEST_NOT_SELECTED;
    } else {
      checkedIoeTypeArray = union(checkedIoeTypeArray);
      if (checkedIoeTypeArray.length === 1) {
        let index = findIndex(checkedIoeTypeArray, type => {
          return type === ServiceProfileConstants.TEST_ITEM_IOE_TYPE.ITEF;
        });
        if (index!==-1) {
          msgCode = SERVICE_PROFILE_MAINTENANCE_CODE.DIALOG_TEST_IOE_TYPE;
        }
      }
    }
  }
  //2.specimen
  if (msgCode === ''&&specimenValMap.size>0) {
    let checkedIoeTypeArray = [];
    for (let valObj of specimenValMap.values()) {
      if (valObj.isChecked) {
        checkedIoeTypeArray.push(valObj.ioeType);
      }
    }
    if (checkedIoeTypeArray.length === 0) {
      msgCode = SERVICE_PROFILE_MAINTENANCE_CODE.DIALOG_SPECIMEN_NOT_SELECTED;
    }
  }
  return msgCode;
}

export function handleSepcimenItem(id,valMap) {
  for (let [itemId, valObj] of valMap) {
    if (itemId!==id) {
      valObj.isChecked = false;
      handleClickBoxOperationType(valObj);
    }
  }
}

export function handleTestItem(id,valMap,masterTestMap,selectedLabId) {
  let currentValObj = valMap.get(id);
  let currentItemIoeType = currentValObj.ioeType;
  let checkedFlag = currentValObj.isChecked;
  // handle comment test
  if (checkedFlag) {
    switch (currentItemIoeType) {
      case ServiceProfileConstants.TEST_ITEM_IOE_TYPE.ITEO:{
        for (let itemValObj of valMap.values()) {
          if (itemValObj.ioeType === ServiceProfileConstants.TEST_ITEM_IOE_TYPE.ITE) {
            itemValObj.isChecked = false;
            handleClickBoxOperationType(itemValObj);
          }
        }
        break;
      }
      case ServiceProfileConstants.TEST_ITEM_IOE_TYPE.ITE:{
        for (let itemValObj of valMap.values()) {
          if (itemValObj.ioeType === ServiceProfileConstants.TEST_ITEM_IOE_TYPE.ITEO) {
            itemValObj.isChecked = false;
            handleClickBoxOperationType(itemValObj);
          }
        }
        break;
      }
      default:
        break;
    }
  }
  // handle master test
  if (selectedLabId === ServiceProfileConstants.LAB_ID.CPLC) {
    // check master item id
    if (masterTestMap.has(id)) {
      let subItemIds = masterTestMap.get(id);
      for (let [itemId, itemValObj] of valMap) {
        let index = findIndex(subItemIds,subItemId => {
          return subItemId === itemId;
        });
        if (index !== -1) {
          itemValObj.isChecked = checkedFlag;
          handleClickBoxOperationType(itemValObj);
        }
      }
    } else {
      // check sub item id
      let targetMasterId = null;
      let targetMasterSubIds = [];
      for (let [masterId, subItemIds] of masterTestMap) {
        let index = findIndex(subItemIds,subItemId => {
          return subItemId === id;
        });
        if (index !== -1) {
          targetMasterId = masterId;
          targetMasterSubIds = subItemIds;
          break;
        }
      }
      if (!isNull(targetMasterId)&&targetMasterSubIds.length > 0) {
        let tempArray = [checkedFlag];
        targetMasterSubIds.forEach(subItemId => {
          tempArray.push(valMap.get(subItemId).isChecked);
        });
        tempArray = union(tempArray);
        if (tempArray.length === 1) {
          valMap.get(targetMasterId).isChecked = tempArray[0];
          handleClickBoxOperationType(valMap.get(targetMasterId));
        } else {
          valMap.get(targetMasterId).isChecked = false;
          handleClickBoxOperationType(valMap.get(targetMasterId));
        }
      }
    }
  }
}

export function generateDropdownValue(dropdownMap,valObj,type) {
  let options = dropdownMap.get(valObj.codeIoeFormItemId).get(type);
  let targetOption = find(options,option=>{
    let tempVal = type === ServiceProfileConstants.ITEM_VALUE.TYPE1?valObj.itemVal:valObj.itemVal2;
    return option.codeIoeFormItemDropId === tempVal;
  });
  return !!targetOption?targetOption.value:'';
}