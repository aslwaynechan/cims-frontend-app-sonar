import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './ClickBoxFieldStyle';
import { isNull,isUndefined } from 'lodash';
import { Checkbox } from '@material-ui/core';
import * as utils from '../../utils/dialogUtils';

class ClickBoxField extends Component {
  constructor(props){
    super(props);
    this.state={
      isChecked: false
    };
  }

  static getDerivedStateFromProps(props, state) {
    const { id,middlewareObject,categoryType } = props;
    let valMap = middlewareObject[`${categoryType}ValMap`];
    let valObj = !isUndefined(valMap)?valMap.get(id):null;
    let isChecked = !isNull(valObj)&&!isUndefined(valObj)?valObj.isChecked:false;
    if (isChecked!==state.isChecked) {
      return {
        isChecked
      };
    }
    return null;
  }

  handleChanged = event => {
    const { id,middlewareObject,categoryType,updateState,sideEffect } = this.props;
    let valMap = middlewareObject[`${categoryType}ValMap`];
    let masterTestMap = middlewareObject.masterTestMap;
    let valObj = valMap.get(id);
    valObj.isChecked = event.target.checked;
    this.setState({
      isChecked:event.target.checked
    });
    sideEffect&&sideEffect(id,valMap,masterTestMap);
    utils.handleClickBoxOperationType(valObj);
    updateState&&updateState({
      middlewareObject
    });
  }

  render() {
    const {classes,id=''} = this.props;
    let {isChecked} = this.state;

    return (
      <div>
        <Checkbox
            id={`form_item_clickbox_${id}`}
            checked={isChecked}
            onChange={this.handleChanged}
            color="primary"
            classes={{
              root:classes.rootCheckbox
            }}
        />
      </div>
    );
  }
}

export default withStyles(styles)(ClickBoxField);
