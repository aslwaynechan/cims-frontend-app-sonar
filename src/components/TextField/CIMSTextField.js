import React, { Component } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import * as commonUtilities from '../../utilities/commonUtilities';

const styles = () => ({
});

class CIMSTextField extends Component {
    handleOnChange = (e) => {
        if (this.props.onlyOneSpace) {
            e.target.value = e.target.value.replace(/(\s)\1+/g, '$1');
        }
        if (this.props.type === 'number') {
            if (e.target.value) {
                e.target.value = e.target.value.replace(/[^0-9]/ig, '');
            }
        }
        if (this.props.type === 'cred') {
            if (e.target.value) {
                e.target.value = e.target.value.replace(/[^0-9a-zA-Z]+$/ig, '');
            }
        }
        //20191025 create new text field type for drug dosage use by Louis Chen
        if (this.props.type === 'decimal') {
            if (e.target.value) {
                // e.target.value = e.target.value.replace(/[^0-9.]+$/ig, '');
                e.target.value = commonUtilities.formatterDecimal(e.target.value);//update by Demi on 20191205
            }
        }
        //20191127 create new action to cal actual length
        if (this.props.calActualLength && e.target.maxLength) {
            // eslint-disable-next-line no-constant-condition
            while (true) {
                const byteSize = commonUtilities.getUTF8StringLength(e.target.value);
                if (byteSize <= e.target.maxLength) {
                    break;
                }
                e.target.value = e.target.value.substr(0, e.target.value.length - 1);
            }
        }

        //upperCase when onchange
        if (this.props.upperCase) {
            e.target.value = (e.target.value || '').toUpperCase();
        }
        if (this.props.onChange) {
            this.props.onChange(e);
        }
    }

    handleOnBlur = (e) => {
        if (this.props.trim) {
            if (this.props.trim.toLowerCase() === 'all') {
                e.target.value = e.target.value.trim();
            }
            else if (this.props.trim.toLowerCase() === 'left') {
                e.target.value = e.target.value.trimLeft();
            }
            else if (this.props.trim.toLowerCase() === 'right') {
                e.target.value = e.target.value.trimRight();
            }
            if (this.props.onChange) {
                this.props.onChange(e);
            }
        }
        if (e.target && e.target.value && e.target.maxLength > 0) {
            if (e.target.value.length > e.target.maxLength) {
                e.target.value = e.target.value.substr(0, e.target.maxLength - 1);
            }
            this.props.onChange && this.props.onChange(e);
        }
        if (this.props.onBlur) {
            this.props.onBlur(e);
        }
    }

    render() {
        /* eslint-disable */
        const {
            classes,
            inputProps,
            onChange,
            onBlur,
            upperCase,
            type,
            value,
            calActualLength,
            onlyOneSpace,
            ...rest } = this.props;
        /* eslint-enable */
        let custom_inputProps = inputProps || {};
        if (upperCase) {
            custom_inputProps.style = Object.assign({}, custom_inputProps.style, { textTransform: 'uppercase', margin:'0' });
        }

        let custom_value = value || '';
        let custom_type = type;
        if (type === 'number') {
            custom_type = '';
        }

        return (
            <TextField
                fullWidth
                variant={this.props.disabled ? 'standard' : 'outlined'}
                autoComplete="off"
                inputProps={{
                    ...custom_inputProps,
                    spellCheck: false
                }}
                type={custom_type}
                value={custom_value}
                onChange={this.handleOnChange}
                onBlur={this.handleOnBlur}
                {...rest}
            />
        );
    }
}

CIMSTextField.defaultProps = {
    type: 'text',
    trim: 'all'
};

CIMSTextField.propTypes = {
    /**
   * `number` ,`cred`,`password`,`email`,`decimal` is available.Default use `text`.
   * `number` number only.
   * `cred` is for credentials,english and number only.
   * `password` passowrd input field.
   * `email` email input field.
   * `decimal` decimal number
   */
    type: PropTypes.oneOf(['number', 'cred', 'text', 'password', 'email', 'decimal']),
    /**
   * `all`, `left` and `right` is available. Default use `all`.
   * `all` is for triming left and right space.
   * `left` is for triming left space.
   * `right` is for triming right space.
   */
    trim: PropTypes.oneOf(['all', 'left', 'right']),
    calActualLength: PropTypes.bool
};

export default withStyles(styles)(CIMSTextField);