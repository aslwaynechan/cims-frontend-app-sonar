import React, { Component } from 'react';
//import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CIMSButton from '../Buttons/CIMSButton';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';

const styles = (theme) => ({
    paper: {
        minWidth: 300,
        maxWidth: '100%',
        borderRadius: 16,
        backgroundColor: 'rgba(249, 249, 249, 0.08)'
    },
    paperScrollPaper: {
        display: 'flex',
        maxHeight: 'calc(100% - 20px)',
        flexDirection: 'column'
    },
    dialogTitle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 16,
        marginBottom: 5
    },
    formControlCss: {
        backgroundColor: theme.palette.dialogBackground,
        padding: '10px 10px 10px 10px'
    },
    formControl2Css: {
        backgroundColor: '#ffffff'
    },
    dialogAction: {
        //justifyContent: 'center'
        padding: '10px 15px'
    }
});

class CIMSPromptDialog extends Component {

    // shouldComponentUpdate(nextP){
    //     return nextP.open!==this.props.open;
    // }

    // componentDidUpdate(){
    //     if(this.props.open===false&&typeof this.props.onClose ==='function'){
    //         this.props.onClose();
    //     }
    // }

    handleDialogExit = () => {
        if (typeof this.props.onExited === 'function') {
            this.props.onExited();
        }
    }

    render() {
        const { classes } = this.props;
        let { buttonConfig, dialogContentTitle } = this.props;
        let titleArr = [];
        if (dialogContentTitle) {
            if (dialogContentTitle.indexOf('<br/>') > -1) {
                titleArr = dialogContentTitle.split('<br/>');
            }
            else {
                titleArr.push(dialogContentTitle);
            }
        }

        return (
            <Dialog
                classes={{
                    paper: this.props.paperStyl || classes.paper,
                    paperScrollPaper: classes.paperScrollPaper
                }}
                id={this.props.id}
                open={this.props.open}
                onClose={() => { }}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                onExited={this.handleDialogExit}
            >
                <FormControl className={classes.formControlCss}>
                    <Typography variant="subtitle2" className={classes.dialogTitle} id={this.props.id + '-title'}>{this.props.dialogTitle}</Typography>
                    <FormControl className={classes.formControl2Css}>
                        <DialogContent id={this.props.id + '-description'}>
                            {/* <Typography variant="subtitle1">{this.props.dialogContentTitle}</Typography> */}
                            {titleArr.length > 0 && titleArr.map((item, index) => {
                                return (
                                    <Typography variant="subtitle1" key={`subtitleLine${index}`}>{item}</Typography>
                                );
                            })}
                            {this.props.dialogContentText}
                        </DialogContent>
                        <DialogActions className={classes.dialogAction}>
                            {/* <CIMSButton onClick={this.props.onClickOK} id={this.props.id + '-ok'} color="primary">{this.props.okButtonName}</CIMSButton>
                            {this.props.btnCancel ? <CIMSButton onClick={this.props.onClickCancel} color="primary" id={this.props.id + '-cancel'} autoFocus>{this.props.cancelButtonName}</CIMSButton> : null} */}
                            {buttonConfig && buttonConfig.map(config => {
                                let { id, name, ...rest } = config;
                                return (
                                    <CIMSButton
                                        id={this.props.id + id}
                                        key={id}
                                        children={name}
                                        {...rest}
                                    />
                                );
                            })}
                        </DialogActions>
                    </FormControl>
                </FormControl>
            </Dialog>
        );
    }
}

// CIMSPromptDialog.propTypes = {
//     open: PropTypes.bool.isRequired,
//     id: PropTypes.string.isRequired,
//     onClose: PropTypes.func,
//     //onClickOK: PropTypes.func.isRequired,
//     dialogTitle: PropTypes.string,
//     onClickCancel: PropTypes.func,
//     btnCancel: PropTypes.bool,
//     okButtonName: PropTypes.string,
//     cancelButtonName: PropTypes.string,
//     dialogContentText: PropTypes.node || PropTypes.string,
//     dialogContentTitle: PropTypes.string
// };

// CIMSPromptDialog.defaultProps = {
//     onClickCancel: () => { },
//     onClose: () => { },
//     okButtonName: 'OK',
//     cancelButtonName: 'Cancel',
//     dialogTitle: '',
//     dialogContentTitle: '',
//     dialogContentText: '',
//     btnCancel: false
// };

export default withStyles(styles)(CIMSPromptDialog);