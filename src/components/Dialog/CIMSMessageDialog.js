import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Dialog,
    withStyles,
    DialogTitle,
    DialogContent,
    DialogActions,
    Divider,
    Typography,
    Grid
} from '@material-ui/core';
import {
    InfoOutlined,
    HelpOutlineOutlined,
    WarningRounded,
    WifiOffOutlined,
    DesktopAccessDisabledOutlined,
    StorageOutlined
} from '@material-ui/icons';
import CIMSButton from '../Buttons/CIMSButton';
import { isNull } from 'lodash';
import { cleanCommonMessageDetail, closeCommonMessage } from '../../store/actions/message/messageAction';

const styles = {
    dialogTitle: {
        fontWeight: 'bold',
        fontSize: '1.5rem',
        textAlign: 'center'
    },
    contentWrapper: {
        padding: 24
    },
    dialogLeftContent: {
        width: 'calc(100% - 20px)',
        padding: '10px 10px',
        backgroundColor: '#FFFFFF',
        border: '1px solid #e6e6e6',
        overflowY: 'auto',
        overflowX: 'hidden',
        fontSize: '14pt',
        whiteSpace: 'normal',
        wordWrap: 'break-word',
        wordBreak: 'break-all'
    },
    iconFont: {
        width: '1em',
        height: '1em',
        fontSize: '40pt',
        color: '#0579C8'
    },
    warningFont: {
        width: '1em',
        height: '1em',
        fontSize: '40pt',
        color: '#FD0000'
    },
    subtitle1: {
        fontSize: '1rem',
        fontWeight: 'bold',
        fontFamily: 'Arial'
    },
    body2: {
        fontSize: '1rem',
        fontFamily: 'Arial',
        whiteSpace: 'inherit',
        wordBreak: 'break-word',
        wordWrap: 'break-word'
    },
    pre: {
        margin: 0,
        fontSize: '1rem',
        fontFamily: 'Arial',
        whiteSpace: 'pre-line',
        wordBreak: 'break-word',
        wordWrap: 'break-word',
        lineHeight: 1.43
    }
};

class CIMSMessageDialog extends Component {
    // constructor(props) {
    //   super(props);
    // }

    handleClickBtn1 = () => {
        let { commonMessageDetail } = this.props;
        commonMessageDetail.btn1Click && commonMessageDetail.btn1Click();
        this.props.closeCommonMessage();
    };

    handleClickBtn2 = () => {
        let { commonMessageDetail } = this.props;
        commonMessageDetail.btn2Click && commonMessageDetail.btn2Click();
        this.props.closeCommonMessage();
    };

    handleClickBtn3 = () => {
        let { commonMessageDetail } = this.props;
        commonMessageDetail.btn3Click && commonMessageDetail.btn3Click();
        this.props.closeCommonMessage();
    };

    handleExited = () => {
        this.props.cleanCommonMessageDetail();
    };

    render() {
        const { classes, commonMessageDetail, openMessageDialog } = this.props;
        let {
            severityCode,
            messageCode,
            description,
            header = '',
            cause = null,
            actionView = null,
            btn1Caption = null,
            btn2Caption = null,
            btn3Caption = null
        } = commonMessageDetail;
        let dialogTitle = messageCode ? `${header} (MSG:${messageCode})` : `${header}`;
        let displayBottomBtnGroup = isNull(btn1Caption) && isNull(btn2Caption) && isNull(btn3Caption) ? false : true;
        return (
            <Dialog
                id="CIMS-Message-Dialog"
                fullWidth
                open={openMessageDialog}
                maxWidth="sm"
                onExited={this.handleExited}
            >
                {/* title */}
                <DialogTitle
                    disableTypography
                    classes={{
                        root: classes.dialogTitle
                    }}
                >
                    {dialogTitle}
                </DialogTitle>
                <Divider />
                {/* content */}
                <DialogContent
                    classes={{
                        root: classes.contentWrapper
                    }}
                >
                    <div className="detail_warp">
                        <Grid
                            container
                            direction="row"
                            justify="center"
                            alignItems="stretch"
                        >
                            <Grid item md={10}>
                                <div className={classes.dialogLeftContent}>
                                    <Typography variant="subtitle1" classes={{subtitle1: classes.subtitle1}}>Description</Typography>
                                    <pre id="cims-message-dialog-description" className={classes.pre} dangerouslySetInnerHTML={{__html:description}}></pre>
                                    {!isNull(cause) ? (
                                        <div>
                                            <Divider />
                                            <Typography variant="subtitle1" classes={{subtitle1: classes.subtitle1}}>Cause</Typography>
                                            <pre id="cims-message-dialog-cause" className={classes.pre} dangerouslySetInnerHTML={{__html:cause}}></pre>
                                        </div>
                                    ) : null}
                                    {!isNull(actionView) ? (
                                        <div>
                                            <Divider />
                                            <Typography variant="subtitle1" classes={{subtitle1: classes.subtitle1}}>Action</Typography>
                                            <pre id="cims-message-dialog-action" className={classes.pre} dangerouslySetInnerHTML={{__html:actionView}}></pre>
                                        </div>
                                    ) : null}
                                </div>
                            </Grid>
                            <Grid item md={2}>
                                <Typography style={{ textAlign: 'center',position:'relative',top:'50%',marginTop:'-30px' }} component="div">
                                    {(() => {
                                        switch (severityCode) {
                                            case 'I': //Information
                                                return (
                                                    <InfoOutlined className={classes.iconFont} />
                                                );
                                            case 'Q': //Question
                                                return (
                                                    <HelpOutlineOutlined className={classes.iconFont} />
                                                );
                                            case 'L':
                                            case 'W': //Warning
                                                return (
                                                    <WarningRounded className={classes.warningFont} />
                                                );
                                            case 'A': //Application Error
                                                return (
                                                    <DesktopAccessDisabledOutlined
                                                        className={classes.warningFont}
                                                    />
                                                );
                                            case 'N': //Network Error
                                                return (
                                                    <WifiOffOutlined className={classes.warningFont} />
                                                );
                                            case 'D': //Database Error
                                                return (
                                                    <StorageOutlined className={classes.warningFont} />
                                                );
                                            default:
                                                break;
                                        }
                                    })()}
                                </Typography>
                            </Grid>
                        </Grid>
                    </div>
                </DialogContent>
                {/* bottom action */}
                {displayBottomBtnGroup ? (
                    <div>
                        <Divider />
                        <DialogActions>
                            <Typography component="div">
                                <Grid
                                    container
                                    direction="row"
                                    justify="flex-end"
                                    alignItems="center"
                                >
                                    {!isNull(btn1Caption) ? (
                                        <CIMSButton
                                            id="cims_message_dialog_btn1"
                                            classes={{
                                                label: classes.body2
                                            }}
                                            onClick={this.handleClickBtn1}
                                        >{btn1Caption}</CIMSButton>
                                    ) : ''}
                                    {!isNull(btn2Caption) ? (
                                        <CIMSButton
                                            id="cims_message_dialog_btn2"
                                            classes={{
                                                label: classes.body2
                                            }}
                                            onClick={this.handleClickBtn2}
                                        >{btn2Caption}</CIMSButton>
                                    ) : ''}
                                    {!isNull(btn3Caption) ? (
                                        <CIMSButton
                                            id="cims_message_dialog_btn3"
                                            classes={{
                                                label: classes.body2
                                            }}
                                            onClick={this.handleClickBtn3}
                                        >{btn3Caption}</CIMSButton>
                                    ) : ''}
                                </Grid>
                            </Typography>
                        </DialogActions>
                    </div>
                ) : null}
            </Dialog>
        );
    }
}

const mapStateToProps = state => {
    return {
        commonMessageList: state.message.commonMessageList,
        openMessageDialog: state.message.openMessageDialog,
        commonMessageDetail: state.message.commonMessageDetail
    };
};

const mapDispatchToProps = {
    cleanCommonMessageDetail,
    closeCommonMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(CIMSMessageDialog));
