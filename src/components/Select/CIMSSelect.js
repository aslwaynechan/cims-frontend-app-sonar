import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import memoize from 'memoize-one';
import Select from 'react-select';
import { emphasize, makeStyles, useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
// import NoSsr from '@material-ui/core/NoSsr';
import TextField from '@material-ui/core/TextField';
// import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
// import Fade from '@material-ui/core/Fade';
import Paper from '@material-ui/core/Paper';
import MenuList from '@material-ui/core/MenuList';
// import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import CancelIcon from '@material-ui/icons/Cancel';

const useStyles = makeStyles(theme => ({
    input: {
        display: 'flex',
        padding: '0 0 0 8px',
        height: 'auto',
        fontSize: '12pt'
    },
    multiBaseInput: {
        height: 'auto',
        minHeight: 26
    },
    valueContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        alignItems: 'center',
        overflow: 'hidden'
    },
    chip: {
        margin: theme.spacing(0.5, 0.25),
        height: 'auto'
    },
    chipLabel: {
        fontSize: 'unset'
    },
    chipDeleteIcon: {
        fontSize: 'unset'
    },
    chipFocused: {
        backgroundColor: emphasize(
            theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
            0.08
        )
    },
    noOptionsMessage: {
        padding: theme.spacing(1, 2)
    },
    singleValue: {
        paddingLeft: 8,
        fontSize: '12pt',
        fontStyle: 'Arial',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        maxWidth: 'calc(100% - 28px)'
    },
    placeholder: {
        // position: 'absolute',
        // top: 2,
        fontSize: '12pt',
        fontStyle: 'Arial',
        paddingLeft: 8
    },
    menuItem: {
        paddingTop: 10,
        paddingBottom: 10,
        //whiteSpace: 'normal',
        whiteSpace: 'nowrap',
        height: 'auto',
        lineHeight: 1,
        minHeight: 'unset',
        fontWeight: 400
    },
    paper: {
        position: 'absolute',
        zIndex: 2,
        marginTop: 0,
        left: 0,
        right: 0,
        background: '#fff'
        //fontSize: props.isSmallSize ? '10pt' : '',
        //width: menuWidth
    },
    smallSize: {
        fontSize: '10pt'
    },
    menuItemSelected: {
        fontWeight: 500
    }
}));

function NoOptionsMessage(props) {
    return (
        <Typography
            color="textSecondary"
            className={
                classNames({
                    [props.selectProps.classes.noOptionsMessage]: true,
                    [props.selectProps.classes.smallSize]: props.selectProps.isSmallSize
                })
            }
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    );
}

function inputComponent({ inputRef, ...props }) {
    return <div ref={inputRef} {...props} />;
}

function Control(props) {
    const {
        children,
        innerProps,
        innerRef,
        selectProps: { classes, TextFieldProps }
    } = props;
    let txtVal = '';
    if (props.isMulti && props.selectProps.value) {
        const valArr = props.selectProps.value.map(item => item.value);
        txtVal = valArr.join(';');
    } else if (!props.isMulti && props.selectProps.value) {
        txtVal = props.selectProps.value.value;
    }

    const handleKeyDown = (e) => {
        if (props.selectProps.isClearable) {
            if (e.keyCode === 8 || e.keyCode === 46) {
                props.clearValue();
            }
        }
    };

    return (
        <TextField
            id={props.selectProps.id + '_control'}
            error={!props.selectProps.isValid}
            value={txtVal}
            fullWidth
            disabled={props.selectProps.isDisabled}
            variant={props.selectProps.isDisabled ? 'standard' : 'outlined'}
            InputProps={{
                inputComponent,
                inputProps: {
                    className: classes.input,
                    ref: innerRef,
                    children,
                    ...innerProps
                },
                classes: {
                    root: classNames({
                        [props.selectProps.classes.multiBaseInput]: props.selectProps.isMulti,
                        [props.selectProps.classes.smallSize]: props.selectProps.isSmallSize
                    })
                }
            }}
            onKeyDown={handleKeyDown}
            {...TextFieldProps}
        />
    );
}

function Option(props) {
    return (
        <MenuItem
            ref={props.innerRef}
            selected={props.isFocused}
            component="div"
            className={
                classNames({
                    [props.selectProps.classes.menuItem]: true,
                    [props.selectProps.classes.menuItemSelected]: props.isSelected,
                    [props.selectProps.classes.smallSize]: props.selectProps.isSmallSize
                })
            }
            {...props.innerProps}
        >
            {props.children}
        </MenuItem>
    );
}

function Placeholder(props) {
    const { selectProps, innerProps = {}, children } = props;
    return (
        <Typography
            color="textSecondary"
            className={
                classNames({
                    [selectProps.classes.placeholder]: true,
                    [props.selectProps.classes.smallSize]: props.selectProps.isSmallSize
                })
            }
            {...innerProps}
        >
            {children}
        </Typography>
    );
}

function SingleValue(props) {
    return (
        <Typography
            className={
                classNames({
                    [props.selectProps.classes.singleValue]: true,
                    [props.selectProps.classes.smallSize]: props.selectProps.isSmallSize
                })
            }
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    );
}

function ValueContainer(props) {
    return (
        <div
            // style={{ backgroundColor: !props.isMulti && props.selectProps.value ? 'white' : '' }}
            className={props.selectProps.classes.valueContainer}
        >{props.children}</div>
    );
}

function MultiValue(props) {
    return (
        <Chip
            tabIndex={-1}
            label={props.children}
            className={clsx(props.selectProps.classes.chip, {
                [props.selectProps.classes.chipFocused]: props.isFocused
            })}
            classes={{
                label: props.selectProps.classes.chipLabel,
                deleteIcon: props.selectProps.classes.chipDeleteIcon
            }}
            onDelete={props.removeProps.onClick}
            deleteIcon={<CancelIcon {...props.removeProps} />}
        />
    );
}

MultiValue.propTypes = {
    children: PropTypes.node,
    isFocused: PropTypes.bool.isRequired,
    removeProps: PropTypes.shape({
        onClick: PropTypes.func.isRequired,
        onMouseDown: PropTypes.func.isRequired,
        onTouchEnd: PropTypes.func.isRequired
    }).isRequired,
    selectProps: PropTypes.object.isRequired
};

function Menu(props) {
    const textFieldControl = document.getElementById(props.selectProps.id + '_control');
    const minWidth = textFieldControl.getBoundingClientRect().width;
    return (
        <Popper
            id={props.selectProps.id + '_menu_container'}
            open
            anchorEl={textFieldControl}
            placement="bottom"
            modifiers={{
                flip: {
                    enabled: true
                }
            }}
            popperOptions={{
                positionFixed: true
            }}
            style={{ minWidth: minWidth, zIndex: 1301 }}
            {...props.innerProps}
        >
            <Paper>
                <MenuList>
                    {props.children}
                </MenuList>
            </Paper>
        </Popper>
    );
}

const myComponents = {
    Control,
    Menu,
    MultiValue,
    NoOptionsMessage,
    Option,
    Placeholder,
    SingleValue,
    ValueContainer
};

const CIMSSelect = React.forwardRef((props, ref) => {
    const classes = useStyles();
    const theme = useTheme();
    const { options, defaultValue, value, onChange, addNullOption, addAllOption, ...rest } = props;

    const selectStyles = {
        input: base => ({
            ...base,
            color: theme.palette.text.primary,
            '& input': {
                font: 'inherit'
            }
        }),
        clearIndicator: base => ({
            ...base,
            cursor: 'pointer'
        }),
        dropdownIndicator: base => ({
            ...base,
            cursor: 'pointer'
        })
    };

    const multiValFilter = memoize((options, value) => {
        let filterList = [];
        for (let i = 0; i < value.length; i++) {
            filterList.push(options.find(item => item.value === value[i]));
        }
        return filterList;
    });

    const handleOnChange = (e) => {
        if (!props.isMulti && !e) {
            onChange && onChange({ value: null });
        } else {
            onChange && onChange(e);
        }
    };

    let opt = options || [];
    let defaultVal = null, val = null;
    if (addNullOption && opt.findIndex(item => item.value === '') < 0) {
        opt.unshift({
            label: <>&nbsp;</>,
            value: ''
        });
    }
    if (addAllOption && opt.findIndex(item => item.value === '') < 0) {
        opt.unshift({
            label: '*All',
            value: '*All'
        });
    }
    if (props.isMulti) {
        defaultVal = defaultValue ? multiValFilter(opt, defaultValue) : null;
        val = value ? multiValFilter(opt, value) : defaultVal;
    } else {
        defaultVal = defaultValue ? opt.find(item => item.value === defaultValue) : null;
        val = value ? opt.find(item => item.value === value) : defaultVal;
    }

    return (
        <Select
            options={opt}
            value={val}
            defaultValue={defaultVal}
            classes={classes}
            components={myComponents}
            placeholder=""
            ref={ref}
            isValid
            onChange={handleOnChange}
            {...rest}
            styles={selectStyles}
        />
    );
});

CIMSSelect.propTypes = {
    id: PropTypes.string.isRequired
};

export default CIMSSelect;