import React, { Component } from 'react';
import { connect } from 'react-redux';
import { cleanCommonMessageSnackbarDetail, closeCommonMessage } from '../../../store/actions/message/messageAction';
import { Snackbar, SnackbarContent, withStyles, IconButton } from '@material-ui/core';
import { styles } from './MessageSnackbarStyle';
import clsx from 'clsx';
import {
    InfoOutlined,
    HelpOutlineOutlined,
    WarningRounded,
    WifiOffOutlined,
    DesktopAccessDisabledOutlined,
    StorageOutlined,
    Close
} from '@material-ui/icons';

const icons={
    I:<InfoOutlined id="icon_message_type_I" className="message_icon" />,
    Q:<HelpOutlineOutlined id="icon_message_type_Q" className="message_icon" />,
    L:<WarningRounded id="icon_message_type_L" className="message_icon" />,
    W:<WarningRounded id="icon_message_type_W" className="message_icon" />,
    A:<DesktopAccessDisabledOutlined id="icon_message_type_A" className="message_icon" />,
    N:<WifiOffOutlined id="icon_message_type_N" className="message_icon" />,
    D:<StorageOutlined id="icon_message_type_D" className="message_icon" />
};

const codeTypeVariant={
    I:'success',
    Q:'info',
    L:'warning',
    W:'warning',
    A:'error',
    N:'error',
    D:'error'
};

class MessageSnackbar extends Component {

    // close => btn1
    handleClose = () => {
        let { commonMessageDetail } = this.props;
        commonMessageDetail.btn1Click && commonMessageDetail.btn1Click();
        this.props.closeCommonMessage();
    }

    handleExited = () => {
        this.props.cleanCommonMessageSnackbarDetail();
    }

    getDescription = () => {
        let { commonMessageDetail } = this.props;
        if (commonMessageDetail.setDescription) {
            return commonMessageDetail.setDescription(commonMessageDetail);
        } else {
            return commonMessageDetail.description;
        }
    };

    render() {
        let { openSnackbar, commonMessageDetail, classes, variant} = this.props;
        let { messageCode, severityCode } = commonMessageDetail;
        let description = this.getDescription();
        variant=variant?variant:codeTypeVariant[severityCode];
        return (
            <Snackbar
                id="message_snackbar"
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left'
                }}
                open={openSnackbar}
                autoHideDuration={3000}
                onClose={this.handleClose}
                onExited={this.handleExited}
            >
                <SnackbarContent
                    className={clsx(classes.content,classes[variant])}
                    message={
                        <span>
                            {icons[severityCode]}
                            {`(MSG:${messageCode}) ${description}`}
                        </span>
                    }
                    action={[
                        <IconButton className={classes.actionClose} key="action_close" onClick={this.handleClose}>
                            <Close />
                        </IconButton>
                    ]}
                />
            </Snackbar>
        );
    }
}

const mapStateToProps = state => {
    return {
        openSnackbar: state.message.openSnackbar,
        commonMessageDetail: state.message.commonMessageSnackbarDetail,
        variant: state.message.variant
    };
};

const mapDispatchToProps = {
    cleanCommonMessageSnackbarDetail,
    closeCommonMessage
};


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(MessageSnackbar));
