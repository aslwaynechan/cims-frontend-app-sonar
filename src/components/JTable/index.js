import React from 'react';
import { MuiThemeProvider, createMuiTheme,withStyles } from '@material-ui/core/styles';
import MaterialTable from 'material-table';
import TableHeader from './TableHeader';


const customStyle={
    root:(props)=>{
      const {options={}}=props;
      if(options.maxBodyHeight==undefined){
        return {
          '& div':{
            overflow:'unset!important'
          }
        };
      }
      else{
        return null;
      }
    }
};

class MyTable extends React.Component{
  constructor(props) {
    super(props);
    const {options={},selected=[]}=props;
    this.state={
      selected:selected,
      selectionKey:typeof(options.selection)=='string'?options.selection:undefined
    };
  }

  componentWillReceiveProps(props){
    const {selected}=props;
    if(selected){
      this.setState({
        selected:selected
      });
    }
  }

  customTheme = (theme) => createMuiTheme({
    ...theme,
    palette:{
      primary:{
        main:'#7BC1D9'
      },
      secondary:{
        main:'#0579c8'
      }
    },
    overrides: {
      // MuiTableHead:{
      //   root:{
      //     backgroundColor:'#7BC1D9'
      //   }
      // },
      MuiTableCell:{
        root:{
          padding:this.props.size=='small'?'8px':'8px',
          fontSize: '1rem',
          fontFamily: 'Arial'
        }
      },
      MuiInputBase: {
        inputSelect: {
            fontSize: '1rem',
            fontFamily: 'Arial'
        },
      input: {
            fontSize: '1rem',
            fontFamily: 'Arial'
        }
      },
      MuiMenuItem: {
        root: {
            fontFamily: 'Arial'
        }
      }
    }
  });

  defaultOptions={
    headerStyle:{
      backgroundColor:'#7BC1D9',
      color:'#fff'
    },
    toolbar:false,
    paging:false,
    sorting:false,
    rowStyle:(rowData)=>({
      backgroundColor:(this.state.selected&&this.state.selected.find(item=>JSON.stringify(item)==JSON.stringify(rowData)))?'#eee':'#fff'
    })
  };

  defaultComponents={
    Header:(props)=>{
      return <TableHeader {...props}/>;
    }
  }

  handleRowClick=(e,rowData)=>{
    const {onSelectionChange,onRowClick}=this.props;
    onRowClick&&onRowClick(e,rowData);
    const {selectionKey}=this.state;
    let dataIndex=null;
    let arr;
    if(selectionKey){
      if(this.state.selected.find((item,index)=>{
          dataIndex=index;
          return item[selectionKey]===rowData[selectionKey];
        })
      ){
        this.state.selected.splice(dataIndex,1);
      }else{
        this.state.selected.push(rowData);
      }
      arr=[...this.state.selected];
    }else{
      if(this.state.selected&&JSON.stringify(rowData)==JSON.stringify(this.state.selected[0])){
        arr=[];
      }else{
        arr=[rowData];
      }
    }

    if(onSelectionChange){
      onSelectionChange(arr);
    }
    this.setState({
      selected:arr
    });
  }

  render(){
	const {options={},classes,components,id='',...others}=this.props;    const {headerStyle={}}=options;
    const rowSelection=typeof(options.selection)=='string';
    options.selection=rowSelection?false:options.selection;
    return (
      <MuiThemeProvider theme={this.customTheme}>
        <div id={id} className={classes.root} stick>
          <MaterialTable {...others} options={{...this.defaultOptions,...options,headerStyle:{...this.defaultOptions.headerStyle,...headerStyle}}} components={{...this.defaultComponents,...components}} onRowClick={this.handleRowClick}/>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default withStyles(customStyle)(MyTable);