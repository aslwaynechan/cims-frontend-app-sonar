import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import IndexPatient from '../../layout/indexPatient';
import Welcome from '../../views/welcome';
import Registration from '../../views/registration/registration';
import UserProfileMaintenance from '../../views/administration/userProfileMaintenance/userProfileMaintenance';
import UserRole from '../../views/administration/userRole/userRole';
import TimeslotTemplate from '../../views/appointment/timeslotTemplate/timeslotTemplate';
import Booking from '../../views/appointment/booking/booking';
import BookingAnonymous from '../../views/appointment/booking/bookingAnonymous';
import CalendarView from '../../views/appointment/calendarView/calendarView';
import Attendance from '../../views/appointment/attendance/attendance';
import ConsultationInfo from '../../views/consultation/consultationInfo';
import Hkic from '../../views/hkic/hkic';
import EditTimeSlot from '../../views/appointment/editTimeSlot/editTimeSlot';
import GenerateTimeSlot from '../../views/appointment/generateTimeSlot/generateTimeSlot';
import AppointmentSlipFooter from '../../views/administration/appointmentSlipFooter/appointmentSlipFooter';
import EncounterTypeManagement from '../../views/administration/encounterTypeManagement/encounterTypeManagement';
import WaitingList from '../../views/appointment/waitingList/waitingList';
import AttendanceCert from '../../views/certificate/attendanceCertificate/attendanceCert';
import SickLeaveCert from '../../views/certificate/sickLeave/sickLeaveCert';
import ReferralLetter from '../../views/certificate/referralLetter/referralLetterCert';
import YellowFeverLetter from '../../views/certificate/yellowFever/yellowFeverLetter';
import PublicHoliday from '../../views/administration/publicHoliday/publicHoliday';
import AssessmentMaintenance from '../../views/jos/assessment/maintenance/maintenance';
import ClinicalNote from '../../views/jos/clinicalNote/clinicalNote';
import Message from '../../views/jos/messageTest/messageTest';
import GeneralAssessment from '../../views/jos/assessment/general/GeneralAssessment';
import ClinicalNoteTemplate from '../../views/jos/clinicalNoteTemplate/manageClinicalNoteTemplate';
import DiagnosisTemplate from '../../views/jos/problemTemplates/diagnosis';
import ProcedureTemplate from '../../views/jos/procedureTemplates/procedure';
import MedicalSummary from '../../views/jos/medicalSummary/medicalSummayTest';
import HistoricalRecord from '../../views/jos/historicalRecord/historicalRecord';
import TokenTemplateManagement from '../../views/jos/IOE/tokenTemplateManagement/tokenTemplateManagement';
import TurnaroundTime from '../../views/jos/IOE/turnaroundTime/turnaroundTime';
import MRAM from '../../views/jos/MRAM/MRAM';
import SpecimenCollection from '../../views/jos/IOE/specimenCollection/specimenCollection';
import LabTestGroupingMaintenance from '../../views/jos/IOE/labTestGroupingMaintenance/labTestGroupingMaintenance';
import ClinicalSummaryReport from '../../views/jos/report/clinicalSummaryReport';
import Enquiry from '../../views/jos/IOE/enquiry/enquiry';
import ixRequest from '../../views/jos/IOE/ixRequest/ixRequest';

const styles = () => ({
    root: {
        // padding: '6px 8px',
        padding: '0px 8px',//Consultation test
        height: '100%',
        boxSizing: 'border-box',
        overflow: 'auto'
    }
});

class CIMSMultiContainer extends React.Component {

    shouldComponentUpdate(nextProps) {
        return nextProps.id !== this.props.id;
    }

    getComponent(componentName) {
        switch (componentName) {
            case 'pmi':
                return Welcome;
            case 'registration':
                return Registration;
            case 'hkicIVRS':
                return Hkic;
            case 'booking':
                return Booking;
            case 'bookingAnonymous':
                return BookingAnonymous;
            case 'attendance':
                return Attendance;
            case 'consultationInfo':
                return ConsultationInfo;
            case 'timeslotTemplate':
                return TimeslotTemplate;
            case 'generateTimeSlot':
                return GenerateTimeSlot;
            case 'userProfile':
                return UserProfileMaintenance;
            case 'userRole':
                return UserRole;
            case 'encounterTypeManagement':
                return EncounterTypeManagement;
            case 'appointmentSlipFooterSetting':
                return AppointmentSlipFooter;
            case 'editTimeSlot':
                return EditTimeSlot;
            case 'waitingList':
                return WaitingList;
            case 'patientSpecFunction':
                return IndexPatient;
            case 'attendanceCert':
                return AttendanceCert;
            case 'sickLeaveCert':
                return SickLeaveCert;
            case 'referralLetterCert':
                return ReferralLetter;
            case 'calendarView':
                return CalendarView;
            case 'yellowFeverLetter':
                return YellowFeverLetter;
            case 'publicHoliday':
                return PublicHoliday;
            case 'assessmentSettingMaintenance':
                return AssessmentMaintenance;
            case 'clinicalNote':
                return ClinicalNote;
            case 'messageTest':
                return Message;
            case 'generalAssessment':
                return GeneralAssessment;
            case 'clinicalNoteTemplateMaintenance':
                return ClinicalNoteTemplate;
            case 'diagnosisTemplateMaintenance':
                return DiagnosisTemplate;
            case 'procedureTemplateMaintenance':
                return ProcedureTemplate;
            case 'medicalSummary':
                return MedicalSummary;
            case 'dxpxHistoricalRecord':
                return HistoricalRecord;
            case 'tokenTemplateMaintenance':
                return TokenTemplateManagement;
            case 'turnaroundTimeMaintenance':
                return TurnaroundTime;
            case 'mramAssessmentInput':
                return MRAM;
            case 'specimenCollection':
                return SpecimenCollection;
            case 'labTestGroupingMaintenance':
                return LabTestGroupingMaintenance;
            case 'clinicalSummaryReport':
                return ClinicalSummaryReport;
            case 'ioeEnquiryPatient':
                return  Enquiry;
            case 'ioeEnquiryClinic':
                return  Enquiry;
            case 'ixRequest':
                return ixRequest;
            default:
                return undefined;
        }
    }

    render() {
        const { classes, component, id, params } = this.props;
        return (
            <Grid className={classes.root} id={id}>
                <Grid component={this.getComponent(component)} params={params} />
            </Grid>
        );
    }
}


export default withStyles(styles)(CIMSMultiContainer);