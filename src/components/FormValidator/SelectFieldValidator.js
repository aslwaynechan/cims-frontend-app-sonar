import React from 'react';
import PropTypes from 'prop-types';
import CIMSSelect from '../Select/CIMSSelect';
import { FormHelperText } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import ValidatorComponent from './ValidatorComponent';

class SelectFieldValidator extends ValidatorComponent {
    handleOnChange = (e) => {
        this.validate(this.props.value);
        if (this.props.onChange) {
            this.props.onChange(e);
        }
    }

    render() {
        /* eslint-disable */
        const {
            errorMessages,
            validators,
            validatorListener,
            withRequiredValidator,
            isRequired,
            labelText,
            labelPosition,
            labelProps,
            msgPosition,
            notShowMsg,
            warning,
            warningMessages,
            validByBlur,
            msgNoWrap,
            ...rest } = this.props;
        /* eslint-enable */

        return (
            <Grid container direction={'column'} alignItems={'flex-start'}>
                {
                    (labelText && labelPosition === 'top') || (msgPosition === 'top' && !notShowMsg) ?
                        <Grid container item alignItems={'baseline'} spacing={labelPosition === 'top' ? 1 : 0} style={{ paddingBottom: 1 }} wrap={msgNoWrap ? 'nowrap' : 'wrap'}>
                            {
                                labelPosition === 'top' ?
                                    <Grid item {...labelProps}>
                                        {this.inputLabel(labelText, isRequired)}
                                    </Grid>
                                    : null
                            }
                            {
                                msgPosition === 'top' && !notShowMsg ?
                                    <Grid item xs>
                                        {this.errorMessage()}
                                    </Grid>
                                    : null
                            }
                        </Grid> : null
                }
                <Grid container item direction={'row'} alignItems={'center'} spacing={labelPosition === 'left' ? 1 : 0} wrap={'nowrap'}>
                    {
                        labelPosition === 'left' ?
                            <Grid component="div" item  {...labelProps}>
                                {this.inputLabel(labelText, isRequired)}
                            </Grid> : null
                    }
                    <Grid item style={{ width: '100%' }} id={'div_' + this.props.id}>
                        {this.selectElement(rest)}
                    </Grid>
                    {
                        msgPosition === 'right' && !notShowMsg ?
                            <Grid container item wrap={'nowrap'} xs={4} style={{ marginLeft: 10 }}>
                                {this.errorMessage()}
                            </Grid>
                            : null
                    }
                </Grid>
                {
                    msgPosition === 'bottom' && !notShowMsg ?
                        <Grid item>
                            {this.errorMessage()}
                        </Grid>
                        : null
                }
            </Grid>
        );
    }

    selectElement(props) {
        // eslint-disable-next-line
        const { onChange, selectClassName, ...rest } = props;
        const { isValid } = this.state;
        return (
            <CIMSSelect
                className={selectClassName}
                innerRef={'input'}
                onChange={this.handleOnChange}
                isValid={isValid}
                {...rest}
            />
        );
    }

    inputLabel(labelText, isRequired) {
        return (
            <label style={{ fontWeight: 'bold' }}>
                {labelText}
                {isRequired ? <span style={{ color: 'red' }}>*</span> : null}
            </label>
        );
    }

    errorMessage() {
        const { isValid } = this.state;
        const id = this.props.id ? this.props.id + '_helperText' : null;
        if (isValid) {
            return null;
        }

        return (
            <FormHelperText error style={{ marginTop: 0 }} id={id}>
                {this.getErrorMessage()}
            </FormHelperText>
        );
    }

}

SelectFieldValidator.propTypes = {
    errorMessages: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.string
    ]),
    validators: PropTypes.array,
    value: PropTypes.any,
    validatorListener: PropTypes.func,
    withRequiredValidator: PropTypes.bool,
    validByBlur: PropTypes.bool
};

SelectFieldValidator.defaultProps = {
    withRequiredValidator: false,
    errorMessages: [],
    validators: [],
    validatorListener: () => { },
    isRequired: false,
    labelText: '',
    notShowMsg: false,
    msgPosition: 'bottom',
    labelPosition: 'top',
    validByBlur: false,
    warning: [],
    warningMessages: '',
    labelProps: {}
};

export default SelectFieldValidator;