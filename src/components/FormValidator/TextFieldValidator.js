import React from 'react';
import PropTypes from 'prop-types';
import CIMSTextField from '../TextField/CIMSTextField';
import Typography from '@material-ui/core/Typography';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import ValidatorComponent from './ValidatorComponent';
import RequiredIcon from '../InputLabel/RequiredIcon';

class TextFieldValidator extends ValidatorComponent {
    onBlur = (e) => {
        if (this.props.onBlur && typeof this.props.onBlur === 'function') {
            this.props.onBlur(e);
        }
        setTimeout(() => {
            if (this.props.validByBlur) {
                this.validateCurrent();
            }
            if (this.props.warning) {
                this.checkWarning();
            }
        }, 100);
    }

    render() {
        /* eslint-disable */
        const {
            errorMessages,
            validators,
            withRequiredValidator,
            validatorListener,
            validByBlur,
            onBlur,
            warning,
            warningMessages,
            isRequired,
            labelText,
            labelProps,
            labelPosition,
            msgPosition,
            notShowMsg,
            error,
            isSmallSize,
            ...rest } = this.props;
        /* eslint-enable */

        return (
            <Grid container direction="column" alignItems="flex-start">
                {
                    (labelText && labelPosition === 'top') || (msgPosition === 'top' && !notShowMsg) ?
                        <Grid container item alignItems="baseline" spacing={labelPosition === 'top' ? 1 : 0} style={{ paddingBottom: 1 }}>
                            {
                                labelPosition === 'top' ?
                                    <Grid item {...labelProps}>
                                        {this.inputLabel(labelText, isRequired, isSmallSize)}
                                    </Grid>
                                    : null
                            }
                            {
                                msgPosition === 'top' && !notShowMsg ?
                                    <Grid item>
                                        {this.errorMessage()}
                                    </Grid>
                                    : null
                            }
                        </Grid> : null
                }
                <Grid container item direction="row" alignItems="center" spacing={labelPosition === 'left' ? 1 : 0} wrap="nowrap">
                    {
                        labelPosition === 'left' ?
                            <Grid item {...labelProps}>
                                {this.inputLabel(labelText, isRequired, isSmallSize)}
                            </Grid> : null
                    }
                    <Grid item style={{ width: '100%' }} id={'div_' + this.props.id}>
                        <CIMSTextField
                            error={!this.state.isValid || error}
                            ref={r => { this.input = r; }}
                            onBlur={this.onBlur}
                            {...rest}
                        />
                    </Grid>
                    {
                        msgPosition === 'right' && !notShowMsg ?
                            <Grid container item wrap="nowrap" xs={4} style={{ marginLeft: 10 }}>
                                {this.errorMessage()}
                            </Grid>
                            : null
                    }
                </Grid>
                {
                    msgPosition === 'bottom' && !notShowMsg ?
                        <Grid item>
                            {this.errorMessage()}
                        </Grid>
                        : null
                }
            </Grid>
        );
    }

    inputLabel(labelText, isRequired, isSmallSize) {
        return (
            isSmallSize ?
                <Typography style={{ fontWeight: 'bold' }}>
                    {labelText}
                    {isRequired ? <RequiredIcon /> : null}
                </Typography>
                :
                <label style={{ fontWeight: 'bold' }}>
                    {labelText}
                    {isRequired ? <RequiredIcon /> : null}
                </label>
        );
    }

    errorMessage() {
        const { isValid, isWarn } = this.state;
        const id = this.props.id ? this.props.id + '_helperText' : null;
        if (isValid && !isWarn) {
            return null;
        } else if (isValid && isWarn) {
            return (
                <FormHelperText style={{ marginTop: 0, color: '#6E6E6E' }} id={id}>
                    {this.getWarningMessage && this.getWarningMessage()}
                </FormHelperText>
            );
        } else {
            return (
                <FormHelperText error style={{ marginTop: 0 }} id={id}>
                    {this.getErrorMessage && this.getErrorMessage()}
                </FormHelperText>
            );
        }
    }
}

TextFieldValidator.propTypes = {
    errorMessages: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.string
    ]),
    validators: PropTypes.array,
    value: PropTypes.any,
    validatorListener: PropTypes.func,
    withRequiredValidator: PropTypes.bool,
    validByBlur: PropTypes.bool
};

TextFieldValidator.defaultProps = {
    withRequiredValidator: false,
    errorMessages: [],
    validators: [],
    validatorListener: () => { },
    isRequired: false,
    labelText: '',
    notShowMsg: false,
    msgPosition: 'bottom',
    labelPosition: 'top',
    validByBlur: true,
    warning: [],
    warningMessages: '',
    labelProps: {}
};


export default TextFieldValidator;