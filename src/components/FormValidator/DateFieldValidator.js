import React from 'react';
import ValidatorComponent from './ValidatorComponent';
import {
    FormHelperText,
    Grid
} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import CIMSDatePicker from '../DatePicker/CIMSDatePicker';
import moment from 'moment';
import memoize from 'memoize-one';
import Enum from '../../enums/enum';
import ValidatorEnum from '../../enums/validatorEnum';
import CommonMessage from '../../constants/commonMessage';
import _ from 'lodash';

class DateComponent extends ValidatorComponent {
    render() {
        /* eslint-disable */
        const {
            errorMessages,
            validators,
            validatorListener,
            withRequiredValidator,
            warning,
            warningMessages,
            validByBlur,
            isRequired,
            labelText,
            labelPosition,
            labelProps,
            msgPosition,
            notShowMsg,
            isSmallSize,
            ...rest } = this.props;
        /* eslint-enable */

        return (
            <Grid container direction="column" alignItems="flex-start">
                {
                    (labelText && labelPosition === 'top') || (msgPosition === 'top' && !notShowMsg) ?
                        <Grid container item alignItems="baseline" spacing={labelPosition === 'top' ? 1 : 0} style={{ paddingBottom: 1 }}>
                            {
                                labelPosition === 'top' ?
                                    <Grid item {...labelProps}>
                                        {this.inputLabel(labelText, isRequired, isSmallSize)}
                                    </Grid>
                                    : null
                            }
                            {
                                msgPosition === 'top' && !notShowMsg ?
                                    <Grid item>
                                        {this.errorMessage()}
                                    </Grid>
                                    : null
                            }
                        </Grid> : null
                }
                <Grid container item direction="row" alignItems="center" spacing={labelPosition === 'left' ? 1 : 0} wrap="nowrap">
                    {
                        labelPosition === 'left' ?
                            <Grid item {...labelProps}>
                                {this.inputLabel(labelText, isRequired, isSmallSize)}
                            </Grid> : null
                    }
                    <Grid item style={{ width: '100%' }}>
                        {this.dateElement(rest)}
                    </Grid>
                    {
                        msgPosition === 'right' && !notShowMsg ?
                            <Grid container item wrap="nowrap" xs={4} style={{ marginLeft: 10 }}>
                                {this.errorMessage()}
                            </Grid>
                            : null
                    }
                </Grid>
                {
                    msgPosition === 'bottom' && !notShowMsg ?
                        <Grid item>
                            {this.errorMessage()}
                        </Grid>
                        : null
                }
            </Grid>
        );
    }

    dateElement(inputProps) {
        // eslint-disable-next-line
        const { error, ...rest } = inputProps;
        return (
            <CIMSDatePicker
                error={!this.state.isValid || error}
                {...rest}
            />
        );
    }

    inputLabel(labelText, isRequired, isSmallSize) {
        return (
            labelText ?
                isSmallSize ?
                    <Typography style={{ fontWeight: 'bold' }}>
                        {labelText}
                        {isRequired ? <span style={{ color: 'red' }}>*</span> : null}
                    </Typography>
                    :
                    <label style={{ fontWeight: 'bold' }}>
                        {labelText}
                        {isRequired ? <span style={{ color: 'red' }}>*</span> : null}
                    </label>
                : null
        );
    }

    errorMessage() {
        const { isValid } = this.state;
        const id = this.props.id ? this.props.id + '_helperText' : null;
        if (isValid) {
            return null;
        }

        return (
            <FormHelperText error style={{ marginTop: 0 }} id={id}>
                {this.getErrorMessage && this.getErrorMessage()}
            </FormHelperText>
        );
    }
}

class DateFieldValidator extends React.Component {
    filterValidator = memoize((
        validators,
        errorMessages,
        maxDate,
        minDate,
        disableFuture,
        disablePast,
        isRequired
    ) => {
        let _validators = _.cloneDeep(validators);
        let _errorMessages = _.cloneDeep(errorMessages);
        if (isRequired) {
            _validators.push(ValidatorEnum.required);
            _errorMessages.push(CommonMessage.VALIDATION_NOTE_REQUIRED());
        }
        _validators.push(ValidatorEnum.isRightMoment);
        _errorMessages.push(CommonMessage.VALIDATION_NOTE_INVALID_MOMENT());
        if (maxDate) {
            _validators.push(ValidatorEnum.maxDate(moment(maxDate).format(Enum.DATE_FORMAT_EYMD_12_HOUR_CLOCK)));
            _errorMessages.push(CommonMessage.VALIDATION_NOTE_MAX_DATE(moment(maxDate).format(Enum.DATE_FORMAT_EDMY_VALUE)));
        }
        if (minDate) {
            _validators.push(ValidatorEnum.minDate(moment(minDate).format(Enum.DATE_FORMAT_EYMD_12_HOUR_CLOCK)));
            _errorMessages.push(CommonMessage.VALIDATION_NOTE_MIN_DATE(moment(minDate).format(Enum.DATE_FORMAT_EDMY_VALUE)));
        }
        if (disableFuture) {
            _validators.push(ValidatorEnum.maxDate(moment().format(Enum.DATE_FORMAT_EYMD_12_HOUR_CLOCK)));
            _errorMessages.push(CommonMessage.VALIDATION_NOTE_DISABLE_FUTURE());
        }
        if (disablePast) {
            _validators.push(ValidatorEnum.minDate(moment().format(Enum.DATE_FORMAT_EYMD_12_HOUR_CLOCK)));
            _errorMessages.push(CommonMessage.VALIDATION_NOTE_DISABLE_PAST());
        }
        return { _validators, _errorMessages };
    });

    validateCurrent = () => {
        this.inputRef.validateCurrent();
    }

    render() {
        const { validators, errorMessages, ...rest } = this.props;
        const { maxDate, minDate, disableFuture, disablePast, isRequired } = this.props;
        let _validator = this.filterValidator(
            validators,
            errorMessages,
            maxDate,
            minDate,
            disableFuture,
            disablePast,
            isRequired
        );
        return (
            <DateComponent
                helperText=""
                validators={_validator._validators}
                errorMessages={_validator._errorMessages}
                ref={ref => this.inputRef = ref}
                {...rest}
            />
        );
    }
}


DateFieldValidator.defaultProps = {
    withRequiredValidator: false,
    errorMessages: [],
    validators: [],
    validatorListener: () => { },
    isRequired: false,
    labelText: '',
    notShowMsg: false,
    msgPosition: 'bottom',
    labelPosition: 'top',
    validByBlur: false,
    warning: [],
    warningMessages: '',
    labelProps: {},
    minDate: '1900-01-01'
};

export default DateFieldValidator;