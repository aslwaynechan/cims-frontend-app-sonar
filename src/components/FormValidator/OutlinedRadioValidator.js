import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Grid from '@material-ui/core/Grid';
import ValidatorComponent from './ValidatorComponent';
import RequiredIcon from '../InputLabel/RequiredIcon';
import * as CommonUtilities from '../../utilities/commonUtilities';


const sysRatio = CommonUtilities.getSystemRatio();
const unit = CommonUtilities.getResizeUnit(sysRatio);

const styles = theme => ({
    errorColor: {},
    disabled: {},
    formLabel: {
        transform: 'translate(14px, -6px) scale(0.75)',
        top: 0,
        left: '-10px',
        position: 'absolute',
        backgroundColor: 'white',
        paddingLeft: '6px',
        paddingRight: '6px',
        '&$disabled': {
            color: theme.palette.text.disabled,
            borderColor: theme.palette.action.disabled
        },
        '&$errorColor': {
            color: theme.palette.error.main,
            borderColor: theme.palette.error.main
        }
    },
    radioGroup: {
        borderStyle: 'solid',
        borderWidth: '1px',
        borderColor: 'rgba(0, 0, 0, 0.23)',
        height: 39 * unit,
        borderRadius: '4px',
        display: 'flex',
        paddingLeft: '14px',
        justifyContent: 'space-around',
        '&$disabled': {
            color: theme.palette.text.disabled,
            borderColor: theme.palette.action.disabled
        },
        '&$errorColor': {
            borderColor: theme.palette.error.main
        }
    },
    radio: {
        paddingTop: 0,
        paddingBottom: 0
    }
});

class OutlinedRadioValidator extends ValidatorComponent {

    constructor(props) {
        super(props);
    }

    tabPreId = '';
    tabCurId = '';

    componentDidMount() {
        super.componentDidMount();
        const radioGroup = document.getElementById(this.props.id + '_radioGroup');
        this.radioList = radioGroup.getElementsByTagName('input') || [];
        window.addEventListener('keydown', this.tabKeyDown);
        window.addEventListener('keyup', this.tabKeyUp);
        window.addEventListener('keypress', this.keypressFn);
    }

    tabKeyDown = (e) => {
        if (e.keyCode !== 9) return;
        this.tabPreId = e.target.id;
    }
    tabKeyUp = (e) => {
        if (e.keyCode !== 9) return;
        let tabCurIndex = -1;
        let tabPreIndex = -1;
        for(let i = 0; i < this.radioList.length; i++){
            if (this.radioList[i].id === e.target.id){
                tabCurIndex = i;
            }
            if (this.radioList[i].id === this.tabPreId){
                tabPreIndex = i;
            }
        }
        if (tabCurIndex > -1 && tabPreIndex === -1) {
            this.tabCurId = this.radioList[0].id;
            this.radioList[0].focus();
        }
        if (tabPreIndex > -1 && tabPreIndex < this.radioList.length - 1){
            this.tabCurId = this.radioList[tabPreIndex + 1].id;
            this.radioList[tabPreIndex + 1].focus();
        }
    }
    keypressFn = (e) => {

        if (e.keyCode === 13 && (e.code === 'Enter' || e.code ==='NumpadEnter')) {

            if (this.tabCurId && this.tabCurId === e.target.id){
                let {onEnter, value} = this.props;
                if(value == e.target.value){
                    if(onEnter){
                        onEnter();
                    }
                }else {
                    e.target.click();
                }
            }
        }
    }

    render() {
        const {
            classes,
            isRequired,
            labelText,
            labelProps,
            notShowMsg,
            RadioGroupProps,
            disabled,
            value,
            name,
            onChange,
            error,
            FormControlLabelProps,
            RadioProps,
            list
        } = this.props;

        const isError = !this.state.isValid || error;
        return (
            <Grid container>
                <FormControl fullWidth>
                    <FormLabel
                        {...labelProps}
                        className={classNames({
                            [classes.errorColor]: isError,
                            [classes.disabled]: disabled,
                            [classes.formLabel]: true
                        }, labelProps && labelProps.className)}
                    >{labelText}{isRequired ? <RequiredIcon /> : null}
                    </FormLabel>
                    <RadioGroup
                        id={this.props.id + '_radioGroup'}
                        row
                        value={value}
                        name={name}
                        onChange={e => onChange(e)}
                        {...RadioGroupProps}
                        className={classNames({
                            [classes.errorColor]: isError,
                            [classes.disabled]: disabled,
                            [classes.radioGroup]: true
                        }, RadioGroupProps && RadioGroupProps.className)}
                    >
                        {
                            list && list.map((item, index) =>
                                <FormControlLabel
                                    id={this.props.id + '_' + item.value + '_radioLabel'}
                                    key={index}
                                    value={item.value}
                                    disabled={disabled}
                                    label={item.label}
                                    labelPlacement="end"
                                    control={
                                        <Radio
                                            id={this.props.id + '_' + item.value + '_radio'}
                                            color="primary"
                                            {...RadioProps}
                                            className={classNames(classes.radio, RadioProps && RadioProps.className)}
                                        />}
                                    {...FormControlLabelProps}
                                />
                            )}
                    </RadioGroup>
                </FormControl>
                {
                    !notShowMsg ? <>{this.errorMessage()}</> : null
                }
            </Grid>
        );
    }

    errorMessage() {
        const { isValid } = this.state;
        const id = this.props.id ? this.props.id + '_helperText' : null;
        if (isValid) {
            return null;
        } else {
            return (
                <FormHelperText error style={{ marginTop: 0 }} id={id}>
                    {this.getErrorMessage && this.getErrorMessage()}
                </FormHelperText>
            );
        }
    }
}

OutlinedRadioValidator.propTypes = {
    labelPosition: PropTypes.oneOf(['top', 'left']),
    msgPosition: PropTypes.oneOf(['top', 'right', 'bottom']),
    id: PropTypes.string.isRequired
};

OutlinedRadioValidator.defaultProps = {
    errorMessages: 'error',
    validators: [],
    validatorListener: () => { },
    isRequired: false,
    labelText: '',
    notShowMsg: false,
    labelProps: {},
    RadioGroupProps: {},
    RadioProps: {},
    FormControlLabelProps: {}
};

export default withStyles(styles)(OutlinedRadioValidator);