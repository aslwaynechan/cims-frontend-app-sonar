import React from 'react';
import ValidatorComponent from './ValidatorComponent';
import { KeyboardTimePicker } from '@material-ui/pickers';
import {
    FormHelperText,
    Grid
} from '@material-ui/core';
import moment from 'moment';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const customTheme = (theme) => createMuiTheme({
    ...theme,
    overrides: {
        ...theme.overrides,
        MuiInputBase: {
            ...theme.overrides.MuiInputBase,
            input: {
                width: '100%',
                textOverflow: 'ellipsis',
                padding: '0px 14px'
            }
        },
        MuiButton: {
            ...theme.overrides.MuiButton,
            root: {
                height: 'auto'
            }
        }
    }
});

class TimeFieldValidator extends ValidatorComponent {
    lastDate = null;
    state = {
        format: 'HH:mm',
        isValid: true,
        isWarn: false
    }

    makeDateInvalid = () => {
        this.makeInvalid();
    }

    makeDateValid = () => {
        this.makeValid();
    }

    // eslint-disable-next-line
    handleChange = (date, value) => {
        if (this.props.onChange) {
            this.props.onChange(date);
        }
    }

    handleError = (error, value) => {
        if (this.props.onError) {
            this.props.onError(error, value);
        }
    }

    render() {
        // const { value } = this.props;
        /* eslint-disable */
        const {
            errorMessages,
            validators,
            withRequiredValidator,
            validatorListener,
            warning,
            warningMessages,
            validByBlur,
            isRequired,
            labelText,
            labelPosition,
            labelProps,
            msgPosition,
            notShowMsg,
            isValid,
            ...rest } = this.props;
        /* eslint-enable */

        return (
            <MuiThemeProvider theme={customTheme}>
                <Grid container direction="column" alignItems="flex-start">
                    {
                        (labelText && labelPosition === 'top') || (msgPosition === 'top' && !notShowMsg) ?
                            <Grid container item alignItems="baseline" spacing={labelPosition === 'top' ? 1 : 0} style={{ paddingBottom: 1 }}>
                                {
                                    labelPosition === 'top' ?
                                        <Grid item {...labelProps}>
                                            {this.inputLabel(labelText, isRequired)}
                                        </Grid>
                                        : null
                                }
                                {
                                    msgPosition === 'top' && !notShowMsg ?
                                        <Grid item>
                                            {this.errorMessage()}
                                        </Grid>
                                        : null
                                }
                            </Grid> : null
                    }
                    <Grid container item direction="row" alignItems="center" spacing={labelPosition === 'left' ? 1 : 0} wrap="nowrap">
                        {
                            labelPosition === 'left' ?
                                <Grid item {...labelProps}>
                                    {this.inputLabel(labelText, isRequired)}
                                </Grid> : null
                        }
                        <Grid item style={{ width: '100%' }}>
                            {this.dateElement(rest)}
                        </Grid>
                        {
                            msgPosition === 'right' && !notShowMsg ?
                                <Grid container item wrap="nowrap" xs={4} style={{ marginLeft: 10 }}>
                                    {this.errorMessage()}
                                </Grid>
                                : null
                        }
                    </Grid>
                    {
                        msgPosition === 'bottom' && !notShowMsg ?
                            <Grid item>
                                {this.errorMessage()}
                            </Grid>
                            : null
                    }
                </Grid>
            </MuiThemeProvider>
        );
    }

    inputOnFocus = (e) => {
        this.lastDate = this.props.value;
        if (this.props.onFocus) {
            this.props.onFocus(e);
        }
    }

    inputOnBlur = (e) => {
        const { isRequired } = this.props;
        const value = this.lastDate;

        if (!e.target.value && value && isRequired) {
            e.target.value = moment(value).format(this.state.format);
        }

        let targetValue = moment(e.target.value, this.state.format);
        if (targetValue.format(this.state.format) === 'Invalid date') {
            e.target.value = isRequired && value ? moment(value).format(this.state.format) : null;
        }

        if (this.props.onChange) {
            const val = e.target.value ? moment(e.target.value, this.state.format) : null;
            this.lastDate = val;
            this.props.onChange(val);
        }
    }

    dateElement(inputProps) {
        const { isValid } = this.state;
        // eslint-disable-next-line
        const { error, onBlur, onFocus, onChange, KeyboardButtonProps, ...rest } = inputProps;
        let restProp = (!isValid || error) ? { error: true, ...rest } : { ...rest };
        return (
            <KeyboardTimePicker
                fullWidth
                ampm={false}
                style={{ width: 'inherit' }}
                onFocus={this.inputOnFocus}
                onBlur={this.inputOnBlur}
                inputRef={r => this.input = r}
                KeyboardButtonProps={{
                    style: { padding: 2, position: 'absolute', right: 0 },
                    ...KeyboardButtonProps
                }}
                inputVariant={inputProps.disabled ? 'standard' : 'outlined'}
                //helperText=""
                placeholder={moment().format(this.state.format)}
                onChange={this.handleChange}
                {...restProp}
            />
        );
    }

    inputLabel(labelText, isRequired) {
        return (
            labelText ?
                <label style={{ fontWeight: 'bold' }}>
                    {labelText}
                    {isRequired ? <span style={{ color: 'red' }}>*</span> : null}
                </label>
                :
                null
        );
    }

    errorMessage() {
        const { isValid } = this.state;
        const id = this.props.id ? this.props.id + '_helperText' : null;
        if (isValid) {
            return null;
        }

        return (
            <FormHelperText error style={{ marginTop: 0 }} id={id}>
                {this.getErrorMessage && this.getErrorMessage()}
            </FormHelperText>
        );
    }
}

TimeFieldValidator.defaultProps = {
    withRequiredValidator: false,
    errorMessages: [],
    validators: [],
    validatorListener: () => { },
    isRequired: false,
    labelText: '',
    notShowMsg: false,
    msgPosition: 'bottom',
    labelPosition: 'top',
    validByBlur: true,
    warning: [],
    warningMessages: '',
    labelProps: {}
};


export default TimeFieldValidator;