import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import ValidatorComponent from './ValidatorComponent';
import RequiredIcon from '../InputLabel/RequiredIcon';

const styles = theme => ({
    errorColor: {
        color: theme.palette.error.main
    }
});

class RadioFieldValidator extends ValidatorComponent {

    render() {
        const {
            isRequired,
            labelText,
            labelProps,
            labelPosition,
            msgPosition,
            notShowMsg,
            RadioGroupProps,
            disabled,
            value,
            name,
            onChange,
            error,
            FormControlLabelProps,
            RadioProps,
            list
        } = this.props;

        const isError = !this.state.isValid || error;
        return (
            <Grid container direction="column" alignItems="flex-start">
                {
                    (labelText && labelPosition === 'top') || (msgPosition === 'top' && !notShowMsg) ?
                        <Grid container item alignItems="baseline" spacing={labelPosition === 'top' ? 1 : 0} style={{ paddingBottom: 1 }}>
                            {
                                labelPosition === 'top' ?
                                    <Grid item {...labelProps}>
                                        {this.inputLabel(labelText, isRequired)}
                                    </Grid>
                                    : null
                            }
                            {
                                msgPosition === 'top' && !notShowMsg ?
                                    <Grid item>
                                        {this.errorMessage()}
                                    </Grid>
                                    : null
                            }
                        </Grid> : null
                }
                <Grid container item direction="row" alignItems="center" spacing={labelPosition === 'left' ? 1 : 0} wrap="nowrap">
                    {
                        labelPosition === 'left' ?
                            <Grid item {...labelProps}>
                                {this.inputLabel(labelText, isRequired)}
                            </Grid> : null
                    }
                    <Grid item style={{ width: '100%' }} id={'div_' + this.props.id}>
                        <RadioGroup
                            id={this.props.id + '_radioGroup'}
                            row
                            value={value}
                            name={name}
                            onChange={e => onChange(e)}
                            {...RadioGroupProps}
                            className={`${RadioGroupProps && RadioGroupProps.className} ${isError ? this.props.classes.errorColor : null}`}
                        >
                            {
                                list && list.map((item, index) =>
                                    <FormControlLabel
                                        id={this.props.id + '_' + item.value + '_radioLabel'}
                                        key={index}
                                        value={item.value}
                                        disabled={disabled}
                                        label={item.label}
                                        labelPlacement="end"
                                        control={
                                            <Radio
                                                id={this.props.id + '_' + item.value + '_radio'}
                                                color="primary"
                                                {...RadioProps}
                                            />}
                                        {...FormControlLabelProps}
                                    />
                                )}
                        </RadioGroup>
                    </Grid>
                    {
                        msgPosition === 'right' && !notShowMsg ?
                            <Grid container item wrap="nowrap" xs={4} style={{ marginLeft: 10 }}>
                                {this.errorMessage()}
                            </Grid>
                            : null
                    }
                </Grid>
                {
                    msgPosition === 'bottom' && !notShowMsg ?
                        <Grid item>
                            {this.errorMessage()}
                        </Grid>
                        : null
                }
            </Grid>
        );
    }

    inputLabel(labelText, isRequired) {
        return (
            <label style={{ fontWeight: 'bold' }}>
                {labelText}
                {isRequired ? <RequiredIcon /> : null}
            </label>
        );
    }

    errorMessage() {
        const { isValid } = this.state;
        const id = this.props.id ? this.props.id + '_helperText' : null;
        if (isValid) {
            return null;
        } else {
            return (
                <FormHelperText error style={{ marginTop: 0 }} id={id}>
                    {this.getErrorMessage && this.getErrorMessage()}
                </FormHelperText>
            );
        }
    }
}

RadioFieldValidator.propTypes = {
    labelPosition: PropTypes.oneOf(['top', 'left']),
    msgPosition: PropTypes.oneOf(['top', 'right', 'bottom'])
};

RadioFieldValidator.defaultProps = {
    errorMessages: 'error',
    validators: [],
    validatorListener: () => { },
    isRequired: false,
    labelText: '',
    labelPosition: 'top',
    msgPosition: 'bottom',
    notShowMsg: false
};

export default withStyles(styles)(RadioFieldValidator);