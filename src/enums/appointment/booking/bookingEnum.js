const BookingEnum = {
    SEARCH_INPUT_DISPLAY_FIELD: {
        NOMAL: ['hkidOrDocno', 'engFullName', 'phoneAndCountry'],
        ANONYMOUS: ['option']
    },
    //ANONYMOUS_DATA_LIST:[{ option: 'New Registration' }, { option: 'Anonymous Appoimtment' }],
    BOOKING_TYPE: {
        NOMAL: 'NOMAL',
        ANONYMOUS: 'ANONYMOUS'
    },
    ANONYMOUS_BANNER_LABEL: {
        DOCUMENT_TYPE: 'Document Type',
        DOCUMENT_NO: 'Document No.',
        SURNAME: 'Surname',
        GIVEN_NAME: 'Given Name',
        MOBILE: 'Mobile Phone'
    }
};

export default BookingEnum;