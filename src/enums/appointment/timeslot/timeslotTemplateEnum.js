const StatusEnum = {
    EDIT:'Edit',
    VIEW:'View',
    NEW:'New'
};

export default StatusEnum;