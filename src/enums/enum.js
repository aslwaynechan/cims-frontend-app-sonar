const Enum = {
    //phone type
    PHONE_TYPE_MOBILE_PHONE: 'M',
    PHONE_TYPE_HOME_PHONE: 'H',
    PHONE_TYPE_OFFICE_PHONE: 'O',
    PHONE_TYPE_FAX_PHONE: 'F',
    PHONE_TYPE_OTHER_PHONE: 'T',

    PHONE_DROPDOWN_LIST: [
        { value: 'M', label: 'Mobile' },
        { value: 'H', label: 'Home' },
        { value: 'O', label: 'Office' },
        { value: 'T', label: 'Other' }
    ],

    //gender
    GENDER_MALE_VALUE: 'M',
    GENDER_FEMALE_VALUE: 'F',
    GENDER_UNKNOWN_VALUE: 'U',

    //address type
    PATIENT_CORRESPONDENCE_ADDRESS_TYPE: 'C',
    PATIENT_RESIDENTIAL_ADDRESS_TYPE: 'R',
    PATIENT_ADDRESS_CHINESE_LANGUAGE: 'C',
    PATIENT_ADDRESS_ENGLISH_LANUAGE: 'E',

    //date format
    DATE_FORMAT_EY_KEY: 'EY',
    DATE_FORMAT_EMY_KEY: 'EMY',
    DATE_FORMAT_EDMY_KEY: 'EDMY',
    DATE_FORMAT_EY_VALUE: 'YYYY',
    DATE_FORMAT_EMY_VALUE: 'MMM-YYYY',
    DATE_FORMAT_EDMY_VALUE: 'DD-MMM-YYYY',
    TIME_FORMAT_24_HOUR_CLOCK: 'HH:mm',
    TIME_FORMAT_12_HOUR_CLOCK: 'hh:mm A',
    DATE_FORMAT_24_HOUR: 'DD-MMM-YYYY HH:mm',
    DATE_FORMAT_12_HOUR: 'DD-MMM-YYYY hh:mm A',
    DATE_FORMAT_EYMD_VALUE:'YYYY-MM-DD',
    DATE_FORMAT_EYMD_12_HOUR_CLOCK:'YYYY-MM-DD HH:mm:ss',
    DATE_FORMAT_FOCUS_DMY_VALUE:'DD-MM-YYYY',
    DATE_FORMAT_FOCUS_MY_VALUE:'MM-YYYY',
    DATE_FORMAT_ECS_EDMY_VALUE:'YYYYMMDD',

    LANGUAGE_LIST: [
        {
            'code': 'E',
            'engDesc': 'English'
        },
        {
            'code': 'C',
            'engDesc': '中文'
        }
    ],

    //status
    COMMON_STATUS: [
        {
            code: 'active',
            engDesc: 'Active'
        },
        {
            code: 'inactive',
            engDesc: 'InActive'
        }
    ],

    COMMON_SALUTATION: [
        {
            code: 'Dr',
            engDesc: 'Doctor'
        },
        {
            code: 'Miss',
            engDesc: 'Miss'
        },
        {
            code: 'Mr',
            engDesc: 'Mr'
        },
        {
            code: 'Mrs',
            engDesc: 'Mrs'
        },
        {
            code: 'Ms',
            engDesc: 'Ms'
        },
        {
            code: 'Pro',
            engDesc: 'Professor'
        }
    ],

    //contact mean
    CONTACT_MEAN_SMS: 'S',
    CONTACT_MEAN_EMAIL: 'E',
    CONTACT_MEAN_POSTALMAIL: 'P',

    CONTACT_MEAN_LIST: [
        {
            'code': 'S',
            'engDesc': 'SMS'
        },
        {
            'code': 'E',
            'engDesc': 'Email'
        },
        {
            'code': 'P',
            'engDesc': 'Postal Mail'
        }
    ],

    //appointment booking elapsed period
    ELAPSED_PERIOD_TYPE: [
        {
            code: 'day',
            engDesc: 'Day'
        },
        {
            code: 'week',
            engDesc: 'Week'
        },
        {
            code: 'month',
            engDesc: 'Month'
        }
    ],

    //appointment booking session
    SESSION: [
        {
            code: 'w',
            engDesc: 'Whole day'
        },
        {
            code: 'am',
            engDesc: 'AM'
        },
        {
            code: 'pm',
            engDesc: 'PM'
        }
    ],

    //appointment booking appointment type
    APPOINTMENT_TYPE_PERFIX: [
        {
            code: 'N',
            engDesc: 'New'
        },
        {
            code: 'O',
            engDesc: 'Old'
        }
    ],
    APPOINTMENT_TYPE_SUFFIX: [
        {
            code: 'N',
            engDesc: 'Normal'
        },
        {
            code: 'F',
            engDesc: 'Force'
        },
        {
            code: 'P',
            engDesc: 'Public'
        },
        {
            code: 'U',
            engDesc: 'Urgent'
        }
    ],

    //next patient logic
    NEXT_PATIENT_LOGIC: {
        THS: 'Next Client',
        FCS: 'Next Patient',
        SocHS: 'Next Patient',
        FHS: 'Next Client',
        CAS: 'Next Client',
        CGS: 'Next Patient',
        EHS: 'Next Client',
        SPP: 'Next Patient',
        'TB&C': 'Next Patient',
        DTS: 'Next Patient'
    },

    LANDING_PAGE: {
        ATTENDANCE: 'AL',
        CONSULTATION: 'PQ'
    },

    //Timer Status
    TIMER_STOPPED: 'STOPPED',
    TIMER_RUNNING: 'RUNNING',

    //Config
    CLINIC_CONFIGNAME: {
        AMPM_CUTOFF_TIME: 'AMPM_CUTOFF_TIME',
        EVENING_CUTOFF_TIME: 'EVENING_CUTOFF_TIME',
        QUOTA_TYPE: 'QUOTA_TYPE',
        QUOTA_TYPE_DESC: 'QUOTA_TYPE_DESC',
        DEFAULT_ENCOUNTER_CD: 'DEFAULT_ENCOUNTER_CD',
        ELAPSED_PERIOD: 'ELAPSED_PERIOD',
        LATE_ATTENDANCE_FLAG: 'LATE_ATTENDANCE_FLAG',
        LATE_ATTENDANCE_TIME: 'LATE_ATTENDANCE_TIME',
        BACK_TAKE_ATTENDANCE_DAY: 'BACK_TAKE_ATTENDANCE_DAY'
    },

    //waiting List status
    WAITING_LIST_STATUS_LIST: [
        { value: '*ALL', label: '*All' },
        { value: '0', label: 'Waiting' },
        { value: '1', label: 'Completed' },
        { value: '2', label: 'Cancelled' }
    ],

    //attendance cert reason
    ATTENDANCE_CERT_FOR_LIST: [
        { label: 'Allied Health', value: 'AH' },
        { label: 'Blood tests', value: 'BT' },
        { label: 'Clinical assessment/study', value: 'CAS' },
        { label: 'Drug injection', value: 'DI' },
        { label: 'Medical Consultation', value: 'MC' },
        { label: 'Radiological examination', value: 'RE' },
        { label: 'Specimen collection', value: 'SC' },
        { label: 'Transfusion', value: 'T' },
        { label: 'Wound care/dressing', value: 'WCD' },
        { label: 'Others', value: 'O' }
    ],

    ATTENDANCE_CERT_SESSION_LIST: [
        { label: 'AM', value: 'A' },
        { label: 'PM', value: 'P' },
        { label: 'Evening', value: 'E' },
        { label: 'Whole day', value: 'W' },
        { label: 'Range', value: 'R' },
        { label: 'Not Specify', value: 'N' }
    ],

    //system message severity code
    SYSTEM_MESSAGE_SEVERITY_CODE: {
        DATABASE: 'D',
        WARNING: 'W',
        APPLICATION: 'A',
        INFORMATION: 'I',
        QUESTION: 'Q',
        NETWORK: 'N'
    },

    //system ratio
    SYSTEM_RATIO_ENUM: {
        /**
         * 4:3 screen
         */
        RATIO1: '4:3 screen',
        /**
         * 5:4 screen
         */
        RATIO2: '5:4 scrren',
        /**
         * 16:9 screen
         */
        RATIO3: '16:9 screen'
    },

    //user role type
    USER_ROLE_TYPE: {
        COUNTER: 'C',
        NURSE: 'N',
        DOCTOR: 'D'
    },

    ATTENDANCE_STATUS: {
        ALL: '',
        ATTENDED: 'Y',
        NOT_ATTEND: 'N'
    },


    //attendance status list
    ATTENDANCE_STATUS_LIST: [
        { value: 'Y', label: 'Attended' },
        { value: 'N', label: 'Not Attend' }
    ],

    //document type
    DOC_TYPE: {
        /**Adoption Certificate */
        ADOPTION_CERTIFICATE: 'AD',
        /**Birth Certificate - HK */
        BIRTH_CERTIFICATE_HK: 'BC',
        /**Exemption Certificate */
        EXEMPTION_CERTIFICATE: 'EC',
        /**HKID Card */
        HKID_ID: 'ID',
        /**Travel documents - PRC */
        TRAVEL_DOCUMENTS_PRC: 'OC',
        /**Travel documents - overseas */
        TRAVEL_DOCUMENTS_OVERSEAS: 'OP',
        /**One-way Permit */
        ONE_WAY_PERMIT: 'OW',
        /**Recognizance */
        RECONGIZANCE: 'RE',
        /**Re-entry Permit */
        RE_ENTRY_PERMIT: 'RP',
        /**Two-way Permit */
        TWO_WAY_PERMIT: 'TW',
        /**eHR document */
        EHR_DOCUMENT: 'ED',
        /**Macao ID card*/
        MACAO_ID_CARD: 'MD'
    },

    CASE_DIALOG_STATUS: {
        CREATE: 'CREATE',
        EDIT: 'EDIT'
    },

    CASE_STATUS: {
        ACTIVE: 'A',
        CLOSE: 'C',
        CANCEL: 'D',
        RECORD_DISPOSAL: 'R'
    },

    CASE_STATUS_LIST: [
        { label: 'Active', value: 'A', promptUpStr: 'Active' },
        { label: 'Close', value: 'C', promptUpStr: 'Closed' },
        { label: 'Cancel', value: 'D', promptUpStr: 'Canceled' },
        { label: 'Record Disposal', value: 'R', promptUpStr: 'Disposaled' }
    ],

    APPOINTMENT_LIST_TYPE: {
        APPOINTMENT_LIST: 'APPOINTMENT_LIST',
        APPOINTMENT_HISTORY: 'APPOINTMENT_HISTORY'
    },

    APPOINTMENT_TYPE: {
        BOOKING: 'Booking',
        TAKE_ATTENDANCE: 'Take Attendance',
        BACK_TAKE_ATTENDANCE: 'Back-take Attendance',
        WALK_IN: 'Walk-in'
    },

    ACCESS_RIGHT_TYPE: {
        FUNCTION: 'function',
        BUTTON: 'button'
    },

    //ecs interface related
    ECS_BENEFIT_TYPE: [
        { label: 'GS', value: 'GS' },
        { label: 'HA', value: 'HA' },
        { label: 'Both', value: 'Both' }
    ],
    ECS_DIALOG_TYPES:{ecs:'ecs', ocsss:'ocsss', mwecs:'mwecs'},
    MWECS_ID_TYPE_KEYS: {hkid:'id', otherDoc:'od'},
    MWECS_ID_TYPES: [
        {
            key: 'id',
            value: 'id',
            desc: 'HKID/HKBC'
        },
        {
            key: 'od',
            value: 'od',
            desc: 'Other Document'
        }
    ],
    MWECS_RESULT_TYPES: [
        {
            key:'O',
            eligible: true,
            desc:'Eligible by Higher OALA'
        },
        {
            key:'C',
            eligible: true,
            desc: 'Eligible by CSSA'
        },
        {
            key:'N',
            eligible: false,
            desc:'Not eligible'
        },
        {
            key:'E',
            eligible: false,
            desc:'System Error'
        }
    ],
    ECS_CLINIC_CONFIG_KEY: {
        ECS_SERVICE_STATUS: 'ECS_SERVICE_STATUS',
        OCSSS_SERVICE_STATUS: 'OCSSS_SERVICE_STATUS',
        MWECS_SERVICE_STATUS: 'MWECS_SERVICE_STATUS',
        SHOW_ECS_BTN_IN_BOOKING: 'BOOKING_SHOW_ECS_BTN'
    }
};

export default Enum;