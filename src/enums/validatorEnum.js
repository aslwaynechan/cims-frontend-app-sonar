import CommonRegex from '../constants/commonRegex';

const ValidatorEnum = {
    required: 'required',
    matchRegexp: (regexp) => { return 'matchRegexp:' + regexp; },
    isEmail: 'matchRegexp:' + CommonRegex.VALIDATION_REGEX_EMAIL,
    isEmpty: 'isEmpty',
    trim: 'trim',
    isEnglish: 'matchRegexp:' + CommonRegex.VALIDATION_REGEX_ENGLISH,
    isEnglishOrSpace: 'matchRegexp:' + CommonRegex.VALIDATION_REGEX_ENGLISH_OR_SPACE,
    isEnglishOrNumber: 'matchRegexp:' + CommonRegex.VALIDATION_REGEX_ENGLISH_OR_NUMBER,
    isEnglishOrNumberOrSpace: 'matchRegexp:' + CommonRegex.VALIDATION_REGEX_ENGLISH_OR_NUMBER_OR_SPACES,
    isChinese: 'isChinese',
    isNoChinese: 'matchRegexp:' + CommonRegex.VALIDATION_REGEX_NOCHINESE,/* Can only enter single byte*/
    isNumber: 'isNumber',
    phoneNo: 'matchRegexp:' + CommonRegex.VALIDATION_REGEX_PHONENO,
    hkPhoneNo: 'matchRegexp:' + CommonRegex.VALIDATION_REGEX_HKPHONENO,
    isFloat: 'isFloat',
    isPositive: 'isPositive',
    isPositiveInteger: 'isPositiveInteger',
    minNumber: (min) => { return 'minNumber:' + min; },
    maxNumber: (max) => { return 'maxNumber:' + max; },
    minFloat: (min) => { return 'minFloat:' + min; },
    maxFloat: (max) => { return 'maxFloat:' + max; },
    minStringLength: (length) => { return 'minStringLength:' + length; },
    maxStringLength: (length) => { return 'maxStringLength:' + length; },
    isString: 'isString',
    maxFileSize: (max) => { return 'maxFileSize:' + max; },
    allowedExtensions: (fileTypes) => { return 'allowedExtensions:' + fileTypes; },
    isDecimal: 'matchRegexp:' + CommonRegex.VALIDATION_REGEX_DECIMAL_15_4,
    codeVerification: 'matchRegexp:' + CommonRegex.VALIDATION_REGEX_CODEVERIFICATION, /*At least two words, the first one is a letter, and the back can also be a letter or number */
    isSpecialEnglish: 'matchRegexp:' + CommonRegex.VALIDATION_REGEX_SPECIAL_ENGLISH,//Match the beginning of the letter, and contain [letter][,][-]['][.][space][`][/][@][(][)][:][*] character
    isEnglishWarningChar: 'matchRegexp:' + CommonRegex.VALIDATION_REGEX_SPECIAL_ENGLISH_WARNING,//Match the [@][(][)][:][*] character
    isHkid: 'isHkid',
    isExpiryDate: 'isExpiryDate',
    period: 'matchRegexp:' + CommonRegex.VALIDATION_REGEX_PERIOD,
    isPositiveIntegerWithoutZero: 'matchRegexp:' + CommonRegex.VALIDATION_REGEX_POSITIVE_INTEGER,
    isRightMoment: 'isRightMoment',
    maxDate: max => `maxDate:${max}`,
    minDate: min => `minDate:${min}`
};

export default ValidatorEnum;