const en_US = {
  // Assessment
  assessment:{
    label_maintenance: 'Assessment Setting',
    label_service: 'Service: {0} ({1})',
    error_assessment_item_not_selected: 'At least one item should be selected.'
  },
  clinicalNote:{
    label_favorite_category:'Medical Record:'
  },
  manageTemplate:{
    label_favorite_category:'Favourite Category:'
  },
  problemTemplate:{
    label_title:'Diagnosis Template Group Maintenance'
  },
  procedureTemplate:{
    label_title:'Procedure Template Group Maintenance'
  },
  templateAdmin:{
    label_group_name:'Group Name'
  },
  tokenTemplateManagement:{
    label_title:'Reminder Template Maintenance'
  },
  labTestGroupingManagement:{
    label_title:'Lab Test Grouping'
  }

};

export default en_US;