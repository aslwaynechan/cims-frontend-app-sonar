import _ from 'lodash';

export function getPaitentStatusOption(patientStatus) {
    if (patientStatus) {
        let dataList = _.cloneDeep(patientStatus);
        dataList.splice(0, 0, { code: 'None', label: 'None' });
        return dataList;
    }
    return null;
}