import moment from 'moment';
import _ from 'lodash';
import Enum from '../enums/enum';
import * as PatientUtilities from './patientUtilities';
import * as CommonUtilities from './commonUtilities';
import * as CaseNoUtilities from './caseNoUtilities';
import { initBookingData, initEncounterType } from '../constants/appointment/bookingInformation/bookingInformationConstants';

export function handleBookingDataToParams(params, defaultCaseTypeCd, openCommonMessage) {
    for (let i = 0; i < params.encounterTypes.length; i++) {
        let encounter = params.encounterTypes[i];
        // elapsedFlag default
        encounter.elapsedFlag = false;
        let aDate = encounter.appointmentDate;
        let period = encounter.elapsedPeriod;
        let periodUnit = encounter.elapsedPeriodUnit;
        if (!aDate && period && periodUnit) {
            console.log(_.toSafeInteger(null));
            console.log(_.toSafeInteger(undefined));
            console.log(_.toSafeInteger(''));
            let newMoment = moment();
            switch (periodUnit) {
                case Enum.ELAPSED_PERIOD_TYPE[0].code:
                    newMoment.add(_.toSafeInteger(period), 'days');
                    break;
                case Enum.ELAPSED_PERIOD_TYPE[1].code:
                    newMoment.add(_.toSafeInteger(period), 'weeks');
                    break;
                case Enum.ELAPSED_PERIOD_TYPE[2].code:
                    newMoment.add(_.toSafeInteger(period), 'months');
                    break;
                default: break;
            }
            encounter.appointmentDate = newMoment;
            encounter.elapsedFlag = true;
        }

        //appointment date
        if (!encounter.appointmentDate) {
            encounter.appointmentDate = moment();
        }
        if (moment(encounter.appointmentDate).isBefore(moment(), 'day')) {
            openCommonMessage({ msgCode: '110207' });
            return null;
        }

        //appointment time
        if (!encounter.appointmentTime) {
            if (moment(encounter.appointmentDate).isSame(moment(), 'day')) {
                encounter.appointmentTime = moment();
            } else {
                encounter.appointmentTime = moment().set({ hours: 0, minute: 0, second: 0 });
            }
        } else {
            if (moment(encounter.appointmentDate).isSame(moment(), 'day')) {
                if (moment(encounter.appointmentTime).isSameOrBefore(moment(), 'millisecond')) {
                    encounter.appointmentTime = moment();
                }
            }
        }

        // encounter.appointmentDate = moment(encounter.appointmentDate).format('YYYY-MM-DD');
        // encounter.appointmentTime = moment(encounter.appointmentTime).format('HH:mm');

        encounter.appointmentDate = moment(encounter.appointmentDate).format(Enum.DATE_FORMAT_EYMD_VALUE);
        encounter.appointmentTime = moment(encounter.appointmentTime).format(Enum.TIME_FORMAT_24_HOUR_CLOCK);

        //booking unit
        if (!encounter.bookingUnit) {
            encounter.bookingUnit = 1;
        }

        encounter.caseTypeCd = encounter.caseTypeCd || defaultCaseTypeCd;

        delete encounter.encounterTypeList;
        delete encounter.subEncounterTypeList;
        delete encounter.elapsedPeriod;
        delete encounter.elapsedPeriodUnit;
        delete encounter.remarkId;
        delete encounter.memo;
    }
    //sprint 5 only get the first
    params.encounterTypes = params.encounterTypes.slice(0, 1);
    return params;
}

export function initBookData(bookData, encounterTypeList, defaultEncounterCd) {
    if (!bookData || bookData.encounterTypes.length <= 0) {
        bookData = _.cloneDeep(initBookingData);
        bookData.encounterTypes[0] = _.cloneDeep(initEncounterType);
    }

    for (let i = 0; i < bookData.encounterTypes.length; i++) {
        let encounterData = _.cloneDeep(bookData.encounterTypes[i]);
        encounterData.appointmentDate = encounterData.appointmentDate ? moment(encounterData.appointmentDate) : null;
        encounterData.appointmentTime = encounterData.appointmentTime ? moment(encounterData.appointmentTime, 'HH:mm') : null;
        encounterData.encounterTypeList = _.cloneDeep(encounterTypeList || []);
        if (encounterData.encounterTypeList && encounterData.encounterTypeList.length > 0) {
            // encounterData.encounterTypeCd = encounterData.encounterTypeCd ? encounterData.encounterTypeCd : encounterData.encounterTypeList[0].encounterTypeCd;
            encounterData.encounterTypeCd = encounterData.encounterTypeCd ? encounterData.encounterTypeCd : defaultEncounterCd;
            let selected = encounterData.encounterTypeList.find(item => item.encounterTypeCd === encounterData.encounterTypeCd);
            if (selected && selected.subEncounterTypeList && selected.subEncounterTypeList.length > 0) {
                encounterData.subEncounterTypeList = selected.subEncounterTypeList;
                encounterData.subEncounterTypeCd = encounterData.subEncounterTypeCd ? encounterData.subEncounterTypeCd : encounterData.subEncounterTypeList[0].subEncounterTypeCd;
            } else {
                encounterData.subEncounterTypeCd = '';
                encounterData.subEncounterTypeList = [];
            }
        } else {
            encounterData.encounterTypeCd = '';
            encounterData.subEncounterTypeCd = '';
            encounterData.encounterTypeList = [];
            encounterData.subEncounterTypeList = [];
        }

        encounterData.caseTypeCd;
        encounterData.appointmentTypeCd = encounterData.appointmentTypeCd || Enum.APPOINTMENT_TYPE_SUFFIX[0].code;
        encounterData.appointmentDate = encounterData.appointmentDate || null;
        encounterData.appointmentTime = encounterData.appointmentTime || null;
        encounterData.elapsedPeriod = encounterData.elapsedPeriod || '';
        encounterData.elapsedPeriodUnit = encounterData.elapsedPeriodUnit || '';
        //encounterData.session = Enum.SESSION[0].code;
        encounterData.bookingUnit = encounterData.bookingUnit || 1;
        bookData.encounterTypes[i] = encounterData;
    }
    return bookData;
}

export function compareMinuteTime(atime, btime) {
    let diffMinute = moment(atime).set('seconds', 0).diff(moment(btime).set('seconds', 0), 'minutes');
    return diffMinute;
}

export function compareDay(atime, btime) {
    atime = moment(atime).set({ 'hours': 0, 'minutes': 0, 'seconds': 0 });
    btime = moment(btime).set({ 'hours': 0, 'minutes': 0, 'seconds': 0 });
    // if (atime.format('YYYY-MM-DD') === btime.format('YYYY-MM-DD')) {
    //     return 0;
    // } else {
    //     return atime.diff(btime, 'days', true);
    // }
    if (atime.format(Enum.DATE_FORMAT_EYMD_VALUE) === btime.format(Enum.DATE_FORMAT_EYMD_VALUE)) {
        return 0;
    } else {
        return atime.diff(btime, 'days', true);
    }
}

export function isExpireTime(currentDate, currentTime) {
    let mo = moment();
    if (compareDay(currentDate, mo) > 0) {
        return false;
    } else if (compareDay(currentDate, mo) === 0) {
        return compareMinuteTime(currentTime, mo) < 0;
    } else {
        return true;
    }
}
export function slotRemark(data = {}) {
    let remark = data.startTime || '';
    remark += data.engGivename ? ' ' + data.engGivename : '';
    remark += data.engSurname ? ' ' + data.engSurname : '';
    remark += data.phoneNo ? ', ' + data.phoneNo : '';
    remark += data.appointmentRemark ? ', ' + data.appointmentRemark : '';
    remark += data.appointmentMemo ? ', ' + data.appointmentMemo : '';
    remark += data.engGivename || data.engSurname ? ' by' : '';
    remark += data.engGivename ? ' ' + data.engGivename : '';
    remark += data.engSurname ? ' ' + data.engSurname : '';
    return remark;
}
export function get_WaitingList_TableRow_ByServiceCd(listConfig, docTypeRender) {
    let rows = [
        { name: 'docTypeCd', label: 'Doc Type', width: 96, split: true, disableOrder: true, customBodyRender: docTypeRender },
        {
            name: 'hkidOrDocNo', label: 'DOC Number/HKID', disableOrder: true, split: true, width: 160, customBodyRender: (value, rowData) => {
                if (rowData && PatientUtilities.isHKIDFormat(rowData.docTypeCd)) {
                    return PatientUtilities.getHkidFormat(value);
                } else {
                    return PatientUtilities.getOtherDocNoFormat(value, rowData.docTypeCd);
                }
            }
        },
        { name: 'englishName', label: 'English Name', disableOrder: true, split: true, width: 120 },
        { name: 'phoneNo', label: 'Phone Number', disableOrder: true, split: true, width: 125 },
        { name: 'clinicCd', label: 'Clinic', width: 70 },
        { name: 'encounterTypeCd', label: 'Encounter Type', width: 150 },
        {
            name: 'statusCd', label: 'Status', width: 80, customBodyRender: (value) => {
                let status = Enum.WAITING_LIST_STATUS_LIST.find(item => { return item.value === value; });
                return status && status.label;
            }
        },
        { name: 'travelDtm', label: 'Departure Date', width: 120 },
        { name: 'createdDtm', label: 'Request Date', width: 160 },
        { name: 'countryList', label: 'Destintaion', disableOrder: true, width: 80 }
    ];

    if (listConfig && listConfig.WAITING_LIST) {
        const list = listConfig.WAITING_LIST.sort(function (a, b) {
            return a.displayOrder - b.displayOrder;
        });
        rows = [];
        for (let i = 0; i < list.length; i++) {
            let newRow = {
                name: list[i]['labelCd'],
                label: list[i]['labelName'],
                width: list[i]['labelLength'],
                split: list[i]['site'] === '1',
                disableOrder: true
            };

            if (newRow.name === 'docTypeCd') {
                newRow.customBodyRender = docTypeRender;
            }
            if (newRow.name === 'statusCd') {
                newRow.customBodyRender = (value) => {
                    let status = Enum.WAITING_LIST_STATUS_LIST.find(item => { return item.value === value; });
                    return status && status.label;
                };
            }
            if (newRow.name === 'hkidOrDocNo') {
                newRow.customBodyRender = (value, rowData) => {
                    if (rowData && PatientUtilities.isHKIDFormat(rowData.docTypeCd)) {
                        return PatientUtilities.getHkidFormat(value);
                    } else {
                        return PatientUtilities.getOtherDocNoFormat(value, rowData.docTypeCd);
                    }
                };
            }
            if (newRow.name === 'clinicCd'
                || newRow.name === 'encounterTypeCd'
                || newRow.name === 'statusCd'
                || newRow.name === 'travelDtm'
                || newRow.name === 'createdDtm') {
                newRow.disableOrder = false;
            }
            rows.push(newRow);
        }
    }
    return rows;
}

/**get default elapsed period from clinic config */
export function getDefaultElapsedPeriodBookData(bookData) {
    if (!bookData.encounterTypes[0].appointmentDate && !bookData.encounterTypes[0].appointmentTime) {
        const loginInfo = JSON.parse(sessionStorage.getItem('loginInfo'));
        const service = JSON.parse(sessionStorage.getItem('service'));
        const clinic = JSON.parse(sessionStorage.getItem('clinic'));
        const clinicConfig = JSON.parse(sessionStorage.getItem('clinicConfig'));
        if (loginInfo && clinicConfig) {
            let where = { serviceCd: service.serviceCd, clinicCd: clinic.clinicCd };
            let config = CommonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.ELAPSED_PERIOD, clinicConfig, where);
            if (config) {
                const configValueArr = config.configValue && config.configValue.split('_');
                if (configValueArr && configValueArr.length === 2) {
                    let newEncounterType = _.cloneDeep(initEncounterType);
                    newEncounterType.appointmentDate = null;
                    newEncounterType.appointmentTime = null;
                    newEncounterType.elapsedPeriod = configValueArr[0];
                    newEncounterType.elapsedPeriodUnit = configValueArr[1];
                    bookData.encounterTypes[0] = newEncounterType;
                }
            }
        }
    }
    return bookData;
}

/**get formated appointment remark and memo */
export function getFormatRemarkAndMemo(apptInfo) {
    return `${apptInfo.remark ? apptInfo.remark : ''}${apptInfo.remark && apptInfo.memo ? ', ' : ''}${apptInfo.memo ? apptInfo.memo : ''}`;
}

/**combine appointment date and time */
export function combineApptDateAndTime(apptInfo, dateFormat = null, timeFormat = null) {
    let tempDateFormat = dateFormat === null ? Enum.DATE_FORMAT_EDMY_VALUE : dateFormat;
    let tempTimeFormat = timeFormat === null ? Enum.TIME_FORMAT_24_HOUR_CLOCK : timeFormat;
    return `${moment(apptInfo.appointmentDate, tempDateFormat).format(tempDateFormat)} ${moment(apptInfo.appointmentTime, tempTimeFormat).format(tempTimeFormat)}`;
}

/**appointment list table header */
export function getAppointmentListTableHeader(type) {
    let caseNoRender = (value) => {
        return CaseNoUtilities.getFormatCaseNo(value);
    };

    let apptDateRender = (value, rowData) => {
        return combineApptDateAndTime(rowData);
    };

    let createdDtmRender = (value, rowData) => {
        return moment(value).format(`${Enum.DATE_FORMAT_EDMY_VALUE} ${Enum.TIME_FORMAT_24_HOUR_CLOCK}`);
    };

    let remarkAndMemoRender = (value, rowData) => {
        return getFormatRemarkAndMemo(rowData);
    };

    let rows = [
        // { name: 'statusCd', label: '', width: 15 },
        { name: 'apptDateAndTime', label: 'Appt. Date/Time', customBodyRender: apptDateRender, width: 150 },
        { name: 'clinicName', label: 'Clinic', width: 200 },
        { name: 'encounterTypeCd', label: 'Encounter', width: 75 },
        { name: 'subEncounterTypeCd', label: 'Sub-encounter', width: 110 },
        { name: 'attnStatusCd', label: 'Attn.', width: 40 },
        { name: 'createdDtm', label: 'Appt. Create Date/Time', customBodyRender: createdDtmRender, width: 175 },
        { name: 'remarkAndMemo', label: 'Remark/Memo', customBodyRender: remarkAndMemoRender, width: 200 },
        { name: 'caseNo', label: 'Case No.', customBodyRender: caseNoRender, width: 130 }
    ];

    if (type === Enum.APPOINTMENT_LIST_TYPE.APPOINTMENT_LIST) {
        rows = rows.filter(item => item.name != 'statusCd');
    }

    return rows;
}

export function getBookedApptType(caseTypeCd, apptTypeCd, clinicConfig, where) {
    let bookedApptType = {
        caseType: '',
        bookedQuotaType: ''
    };
    let bookedQuotaType = null;

    const quotaTypeConfig = CommonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.QUOTA_TYPE_DESC, clinicConfig, where);
    const caseType = Enum.APPOINTMENT_TYPE_PERFIX.find(item => item.code === caseTypeCd);

    let quotaDescArr = quotaTypeConfig.configValue ? quotaTypeConfig.configValue.split('|') : ['N:Normal', 'F:Force', 'P:Public'];          //Irving:should have the default Quota Type
    let newQuotaArr = CommonUtilities.transformToMap(quotaDescArr);

    bookedQuotaType = newQuotaArr.find(item => item.code === apptTypeCd);

    if (caseType) {
        bookedApptType.caseType = caseType.engDesc;
    }

    if (bookedQuotaType) {
        bookedApptType.bookedQuotaType = bookedQuotaType.engDesc;
    }

    return bookedApptType;
}

/**get allow back take attendance day from clinic config */
export function getAllowBackTakeDay(clinicConfig, where) {
    const backTakeDay = CommonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.BACK_TAKE_ATTENDANCE_DAY, clinicConfig, where);
    let allowBackTakeDay = backTakeDay.configValue ? `-${backTakeDay.configValue}` : '-18';

    return allowBackTakeDay;
}


/**get current appointment record */
export function getCurentAppointment(appointmentId, appointmentList) {
    let curAppointment;
    if (!appointmentId) {
        return null;
    }
    if (appointmentList && appointmentList.length > 0) {
        curAppointment = appointmentList.find(item => item.appointmentId === appointmentId);
    }
    return curAppointment ? curAppointment : null;
}

/**get patient status code from current using case prefix*/
export function getPatientStatusCd(curAppointment, patientInfo) {

    if (!curAppointment || curAppointment === null) {
        return '';
    }

    if (!curAppointment.caseNo) {
        return '';
    }


    let caseNoDto = patientInfo.caseList.find(item => item.caseNo === curAppointment.caseNo);
    // return caseNoDto ? caseNoDto.patientStatus : '';
    if (!caseNoDto) {
        return '';
    }
    else {
        return caseNoDto.patientStatus ? caseNoDto.patientStatus : '';
    }
}

/**get patient status flag */
export function getPatientStatusFlag(encounterList, curAppointment) {
    if (!encounterList || encounterList.length === 0) {
        return 'N';
    }

    if (curAppointment === null) {
        return 'N';
    }

    let curSelectedEncounter = curAppointment.encounterTypeCd;
    let curSelectedSubEncounter = curAppointment.subEncounterTypeCd;

    let encounterDto = encounterList.find(item => item.encounterTypeCd === curSelectedEncounter);

    if (!encounterDto) {
        return 'N';
    }

    if (encounterDto.subEncounterTypeList.length === 0) {
        return 'N';
    }

    let subEncounterDto = encounterDto.subEncounterTypeList.find(item => item.subEncounterTypeCd === curSelectedSubEncounter);

    return subEncounterDto ? subEncounterDto.patientStatusFlag : 'N';
}


/**booking appointment list table header */
export function getBookingAppointmentListTableHeader(type, bookingContactHistoryRender) {
    let caseNoRender = (value) => {
        return CaseNoUtilities.getFormatCaseNo(value);
    };

    let apptDateRender = (value, rowData) => {
        return combineApptDateAndTime(rowData);
    };

    let createdDtmRender = (value, rowData) => {
        return moment(value).format(`${Enum.DATE_FORMAT_EDMY_VALUE} ${Enum.TIME_FORMAT_24_HOUR_CLOCK}`);
    };

    let remarkAndMemoRender = (value, rowData) => {
        return getFormatRemarkAndMemo(rowData);
    };

    let rows = [
        // { name: 'statusCd', label: '', width: 15 },
        { name: 'statusDesc', label: '', width: 15 },
        { name: 'apptDateAndTime', label: 'Appt. Date/Time', customBodyRender: apptDateRender, width: 150 },
        { name: 'clinicName', label: 'Clinic', width: 200 },
        { name: 'encounterTypeCd', label: 'Encounter', width: 75 },
        { name: 'subEncounterTypeCd', label: 'Sub-encounter', width: 110 },
        { name: 'attnStatusCd', label: 'Attn.', width: 40 },
        { name: 'contactHistory', label: 'Contact History', align: 'center', customBodyRender: bookingContactHistoryRender, width: 40 },
        { name: 'createdDtm', label: 'Appt. Create Date/Time', customBodyRender: createdDtmRender, width: 175 },
        { name: 'remarkAndMemo', label: 'Remark/Memo', customBodyRender: remarkAndMemoRender, width: 200 },
        { name: 'caseNo', label: 'Case No.', customBodyRender: caseNoRender, width: 130 }
    ];

    if (type === Enum.APPOINTMENT_LIST_TYPE.APPOINTMENT_LIST) {
        rows = rows.filter(item => item.name != 'statusCd');
    }

    return rows;
}

/**calculate how many day(s) is later than given appointment date*/
export function calcDayDiff(bookingDate, slotDate) {
    let dayDiff = 0;
    const dateFormat = Enum.DATE_FORMAT_EDMY_VALUE;
    if (moment(bookingDate, dateFormat).isAfter(moment(slotDate, dateFormat))) {
        return 0;
    }

    dayDiff = moment(bookingDate, dateFormat).diff(moment(slotDate, dateFormat), 'day');

    console.log('zxc', dayDiff);

    return dayDiff;

}
