import * as RegUtil from './registrationUtilities';

test('RegUtil.getHkid', () => {
  expect(RegUtil.getHkid('A1234563')).toBe('A123456');
});

test('RegUtil.hkidFormat', () => {
  expect(RegUtil.hkidFormat('A1234563')).toBe('A123456(3)');
});

test('RegUtil.limitTextFieldTrim', () => {
  expect(RegUtil.limitTextFieldTrim(' Hello Jest ')).toBe('HelloJest');
});

