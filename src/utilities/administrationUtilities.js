import axios from '../services/axiosInstance';

export async function checkChinese(value) {
    let result = false;
    if(!value)
        return true;
    let response = await axios.post('/patient/chiNameValidator', { character: value });
    if (response.status === 200) {
        if (response.data.respCode === 0) {
            result = response.data.data;
        }
    }
    return result;
}