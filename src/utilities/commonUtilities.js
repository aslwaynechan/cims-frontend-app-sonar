// import storeConfig from '../store/storeConfig';
import Enum from '../enums/enum';
import _ from 'lodash';
import moment from 'moment';

export function filterConfig(targetConfigs, where) {
    let topConfig = targetConfigs.filter(
        item => (!where.serviceCd || item.serviceCd === where.serviceCd) &&
            (!where.clinicCd || item.clinicCd === where.clinicCd) &&
            (!where.encounterCd || item.encounterCd === where.encounterCd) &&
            (!where.subEncounterCd || item.subEncounterCd === where.subEncounterCd)
    );
    if (topConfig.length > 0) {
        return topConfig;
    } else if (where.subEncounterCd) {
        delete where.subEncounterCd;
        return filterConfig(targetConfigs, where);
    } else if (where.encounterCd) {
        delete where.encounterCd;
        return filterConfig(targetConfigs, where);
    } else if (where.clinicCd) {
        delete where.clinicCd;
        return filterConfig(targetConfigs, where);
    } else if (where.serviceCd) {
        delete where.serviceCd;
        return filterConfig(targetConfigs, where);
    } else {
        return [];
    }
}
export function getPriorityConfig(configName, configs, where) {
    let config = {};
    let targetConfigs = configs && configs[configName];
    if (!targetConfigs) {
        return config;
    } else {
        let topConfig = filterConfig(targetConfigs, where);
        if (topConfig && topConfig.length > 0) {
            config = topConfig[0];
        }
        return config;
    }
}

export function getPriorityListConfig(configList, name, userRoleType, serviceCd) {
    if (configList && name && userRoleType && serviceCd) {
        let cloneList = _.cloneDeep(configList);
        switch (name) {
            case 'patientlist': {
                if (cloneList.findIndex(item => item.userGroupCd === userRoleType) > -1) {
                    cloneList = cloneList.filter(item => item.userGroupCd === userRoleType);
                } else {
                    cloneList = cloneList.filter(item => !item.userGroupCd);
                }
                break;
            }
            case 'waitinglist': {
                if (cloneList.findIndex(item => item.serviceCd === serviceCd) > -1) {
                    cloneList = cloneList.filter(item => item.serviceCd === serviceCd);
                } else {
                    cloneList = cloneList.filter(item => !item.serviceCd);
                }
                break;
            }
        }
        return cloneList;
    }
    return configList;
}

export function matchSection(currentTime, type, ampmOnly = true) {
    let section = '';
    switch (type) {
        case 'D': {
            return;
        }

        case 'H': {
            let tempHours = currentTime.format('HH');
            //let tempMin=currentTime.format('mm');
            if (ampmOnly) {
                if (tempHours >= 12 && tempHours < 24) {
                    section = 'PM';
                }
                else {
                    section = 'AM';
                }
            } else {
                if (tempHours >= 12 && tempHours < 18) {
                    section = 'PM';
                }
                else if (tempHours >= 18 || tempHours < 6) {
                    section = 'Evening';
                }
                else {
                    section = 'AM';
                }
            }

            return section;
        }


        default: {
            return;
        }

    }
}

export function getPatientCall() {
    const config = JSON.parse(sessionStorage.getItem('clinicConfig'));
    const loginInfo = JSON.parse(sessionStorage.getItem('loginInfo'));
    const service = JSON.parse(sessionStorage.getItem('service'));
    if (loginInfo && config && service) {
        const serviceCd = service['serviceCd'];
        const patientLabel = config['PATIENT_LABEL'];
        const obj = patientLabel && patientLabel.find(item => item.serviceCd === serviceCd);
        return obj ? obj.configValue : 'Patient';
    }
    return 'Patient';
}

export function getNameSearchCall() {
    const accessRights = JSON.parse(sessionStorage.getItem('accessRights'));
    if (accessRights.filter(item => item.type === 'button').findIndex(item => item.name === 'B001') !== -1) {
        return '/ ' + getPatientCall() + ' Name';
    }
    return '';
}

export function containAccessRights(accessRights, accessRightNameTypePairs) {
    let result = true;

    let allAccessRightPairs = accessRights.reduce(
        (obj, item) => {
            obj[item.name] = item;
            return obj;
        }, {}
    );


    for(let pair of accessRightNameTypePairs){
        let typeNamePair = allAccessRightPairs[pair.name];
        if(typeNamePair === undefined || typeNamePair === null){
            result = false;
            break;
        }
        if(pair.type !== typeNamePair.type){
            result = false;
            break;
        }
    }
    return result;
}

// //full screen
// export function fullscreen() {
//     let isFullscreen = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
//     if (!isFullscreen) {
//         let el = document.getElementById('root');
//         (el.requestFullscreen && el.requestFullscreen()) ||
//             (el.mozRequestFullScreen && el.mozRequestFullScreen()) ||
//             (el.webkitRequestFullscreen && el.webkitRequestFullscreen()) || (el.msRequestFullscreen && el.msRequestFullscreen());
//     }
// }
// //quit full screen
// export function exitFullscreen() {
//     let isFullscreen = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
//     if (isFullscreen) {
//         (document.exitFullscreen && document.exitFullscreen()) ||
//             (document.mozCancelFullScreen && document.mozCancelFullScreen()) ||
//                 (document.webkitExitFullscreen && document.webkitExitFullscreen());
//     }
// }


/**Irving: we use this func to get the sys ratio */
export function getSystemRatio() {
    //const header = 25;
    const screenWidth = window.screen.width;
    const screenHeight = window.screen.height;

    if ((screenWidth / 4) === ((screenHeight) / 3)) {
        return Enum.SYSTEM_RATIO_ENUM.RATIO1;
    }

    if ((screenWidth / 5) === ((screenHeight) / 4)) {
        return Enum.SYSTEM_RATIO_ENUM.RATIO2;
    }

    if ((screenWidth / 16) === ((screenHeight) / 9)) {
        return Enum.SYSTEM_RATIO_ENUM.RATIO3;
    }
}

/**Irving :basic resolution is 1280 x 720(16:9 screen).
 *         change basic resolution to 1920 x 1080 in sprint 11.
*/
export function getResizeUnit(ratio) {
    let unit = 1;
    // const screenWidth = window.screen.width;
    const screenHeight = window.screen.height;

    if (ratio === undefined) {
        //do something,need to list all 16:9 screen
        return unit;
    }
    // if (ratio === Enum.SYSTEM_RATIO_ENUM.RATIO3 || ratio === Enum.SYSTEM_RATIO_ENUM.RATIO2) {
    //     const basicWidth = 1920;
    //     unit = screenWidth / basicWidth;
    // }
    const basicHeight = 1080;
    unit = screenHeight / basicHeight;
    return unit;
}


export function findByCd(data, cd, cdName) {
    //     if (!data) {
    //         return;
    //     }
    let result = null;
    let tempResult = null;
    tempResult = data.find(item => item[cdName] === cd);
    if (tempResult !== undefined || result !== null) {
        result = tempResult;
        return result;
    } else {

        data.forEach(right => {
            if (right.childAccessRightDtos.length !== 0) {
                tempResult = findByCd(right.childAccessRightDtos, cd, cdName);
                if (tempResult !== undefined) {
                    result = tempResult;
                }
            }
            // else {
            //     tempResult = right.find(item => item[cdName]);
            // }
        });

    }
    return result;
}

export function groupAllParent(menuList, group) {
    let parentGp = group;
    menuList.forEach(menu => {
        const childs = menu.childAccessRightDtos ? menu.childAccessRightDtos.length : 0;
        const menuName = menu.accessRightName;
        if (childs === 0) {
            return;
        }
        else {
            parentGp[menuName] = menu.childAccessRightDtos;
            groupAllParent(menu.childAccessRightDtos, parentGp);

        }
    });
    return parentGp;
}

export function groupAllChild(menuList, group) {
    let childGp = group;
    menuList.forEach(menu => {
        const childs = menu.childAccessRightDtos ? menu.childAccessRightDtos.length : 0;
        childGp.push(menu);
        if (childs === 0) {
            return childGp;
        }
        else {
            // enu.childAccessRightDtos.forEach(child => {
            //     childGp = groupAllChild(child, childGp);
            // });
            childGp = groupAllChild(menu.childAccessRightDtos, childGp);

        }

    });
    return childGp;

}

export function initSubEncounterList(encounterList) {
    let subEncounterList = [];
    if (encounterList) {
        encounterList.forEach(encounter => {
            // let subEncounter={
            //     ...encounter.subEncounterTypeList
            // };
            encounter.subEncounterTypeList.forEach(subEncounter => {
                let parentGp = [];
                let idx = subEncounterList.findIndex(item => item.subEncounterTypeCd === subEncounter.subEncounterTypeCd);
                let parent = {
                    clinic: encounter.clinic,
                    description: encounter.description,
                    encounterTypeCd: encounter.encounterTypeCd,
                    existCode: encounter.existCode,
                    service: encounter.service,
                    shortName: encounter.shortName,
                    version: encounter.version
                };
                if (idx === -1) {
                    parentGp.push(parent);
                    subEncounter.parentGp = parentGp;
                    subEncounterList.push(subEncounter);
                }
                else {
                    if (subEncounterList[idx].parentGp.findIndex(item => item.encounterTypeCd === parent.encounterCd) === -1) {
                        subEncounterList[idx].parentGp.push(parent);
                    }

                }
            });
        });
    }

    return subEncounterList;
}

export function changeAllRightsOpenStatus(right, status) {
    const childs = right.childAccessRightDtos ? right.childAccessRightDtos.length : 0;
    right.open = status;
    if (childs != 0) {
        right.childAccessRightDtos.forEach(child => {
            changeAllRightsOpenStatus(child, status);
        });
    }
}

export function isObj(object) {
    return object && typeof (object) == 'object' && Object.prototype.toString.call(object).toLowerCase() == '[object object]';
}

export function getLength(object) {
    let count = 0;
    for (let key in object) {
        if (Object.prototype.hasOwnProperty.call(object, key)) {
            count++;
        }
    }
    return count;
}

let CompareObj, CompareJSON;
CompareObj = function CompareObj(objA, objB, flag) {
    for (let key in objA) {
        if (!flag)
            break;
        if (!Object.prototype.hasOwnProperty.call(objB, key)) {
            flag = false;
            break;
        }
        if (!Array.isArray(objA[key])) {
            if (isObj(objA[key])) {
                if (!isObj(objB[key])) {
                    flag = false;
                    break;
                }
                if (!CompareJSON(objA[key], objB[key])) {
                    flag = false;
                    break;
                }
            } else {
                if (objA[key] === 'true' || objA[key] === 'false') {
                    objA[key] = objA[key] === 'true';
                }
                if (objB[key] === 'true' || objB[key] === 'false') {
                    objB[key] = objB[key] === 'true';
                }
                if (objB[key] != objA[key]) {
                    flag = false;
                    break;
                }
            }

            // if(CompareObj(objB[key], objA[key], flag)){
            //     flag = false;
            //     break;
            // }
        } else {
            if (!Array.isArray(objB[key])) {
                flag = false;
                break;
            }
            let oA = objA[key],
                oB = objB[key];
            if (oA.length != oB.length) {
                flag = false;
                break;
            }
            for (let k in oA) {
                if (!flag)
                    break;
                flag = CompareObj(oA[k], oB[k], flag);
            }
        }
    }
    return flag;
};

CompareJSON = function CompareJSON(objA, objB) {
    if (!isObj(objA) || !isObj(objB)) return false;
    if (getLength(objA) != getLength(objB)) return false;
    let tmpObjA = JSON.parse(JSON.stringify(objA));
    let tmpObjB = JSON.parse(JSON.stringify(objB));
    return CompareObj(tmpObjA, tmpObjB, true);
};

export { CompareJSON };

export function getUTF8StringLength(str) {
    if (!str) {
        return 0;
    }
    let byteSize = 0;
    for (let i = 0; i < str.length; i++) {
        const charCode = str.charCodeAt(i);
        if (0 <= charCode && charCode <= 0x7f) {
            byteSize += 1;
        } else if (128 <= charCode && charCode <= 0x7ff) {
            byteSize += 2;
        } else if (2048 <= charCode && charCode <= 0xffff) {
            byteSize += 3;
        } else if (65536 < charCode && charCode <= 0x1fffff) {
            byteSize += 4;
        } else if (0x200000 < charCode && charCode <= 0x3ffffff) {
            byteSize += 5;
        } else if (0x4000000 < charCode && charCode <= 0x7fffffff) {
            byteSize += 6;
        } else {
            return -1;
        }
    }
    return byteSize;
}

export function formatterDecimal(value) {
    value = value.replace(/[^\d.]/g, '');
    value = value.replace(/^\./g, '');
    value = value.replace(/\.{2,}/g, '.');
    value = value.replace('.', '$#$').replace(/\./g, '').replace('$#$', '.');

    let index = value.indexOf('.');
    if (index > -1) {
        let floatValue = value.substring(index);
        if (floatValue.length > 4) {
            value = value.substring(0, index + 5);
        }
    } else {
        value = value.substring(0, 11);
    }
    return value;
}


export function transformToMap(quotaArr) {
    let newQuotaArr = [];
    if (quotaArr && quotaArr[0] && quotaArr[0].indexOf(':') != -1) {
        for (let i = 0; i < quotaArr.length; i++) {
            if (quotaArr[i]) {
                newQuotaArr.push({ code: quotaArr[i].split(':')[0], engDesc: quotaArr[i].split(':')[1] });
            }
        }
    } else {
        if (quotaArr) {
            for (let i = 0; i < quotaArr.length; i++) {
                if (quotaArr[i]) {
                    newQuotaArr.push({ code: quotaArr[i].substring(0, 1), engDesc: quotaArr[i] });
                }
            }
        }
    }
    return newQuotaArr;

}

export function getQuotaDescArray(clinicConfig, where) {
    const defaultQuotaDesc = getPriorityConfig(Enum.CLINIC_CONFIGNAME.QUOTA_TYPE_DESC, clinicConfig, where);
    const quotaArr = transformToMap(defaultQuotaDesc.configValue ? defaultQuotaDesc.configValue.split('|') : null);
    return quotaArr;
}

export function getCaseTypeDesc(caseType) {
    switch (_.toUpper(caseType)) {
        case 'N': return 'New';
        case 'O': return 'Old';
    }
}

export function getQuotaTypeDesc(quotaType, clinicConfig, where) {
    const arr = getQuotaDescArray(clinicConfig, where);
    return arr && arr.find(item => item.code === quotaType);
}

export function getIPs(callback) {
    let ip_dups = {};

    //compatibility for firefox and chrome
    let RTCPeerConnection = window.mozRTCPeerConnection || window.msRTCPeerConnection || window.webkitRTCPeerConnection || window.RTCPeerConnection;
    let mediaConstraints = {
        optional: [{ RtpDataChannels: true }]
    };

    //firefox already has a default stun server in about:config
    // media.peerconnection.default_iceservers =
    // [{"url": "stun:stun.services.mozilla.com"}]
    let servers = undefined;

    //add same stun server for chrome
    if (window.webkitRTCPeerConnection)
        servers = { iceServers: [{ urls: 'stun:stun.services.mozilla.com' }] };

    //construct a new RTCPeerConnection
    let pc = new RTCPeerConnection({ iceServers: [] });

    console.log(pc);
    //listen for candidate events
    pc.onicecandidate = function (ice) {

        //skip non-candidate events
        if (ice.candidate) {

            console.log(ice.candidate);
            //match just the IP address
            let ip_regex = /([0-9]{1,3}(\.[0-9]{1,3}){3})/;
            let ip_addr = ip_regex.exec(ice.candidate.candidate)[1];

            //remove duplicates
            if (ip_dups[ip_addr] === undefined)
                callback(ip_addr);

            ip_dups[ip_addr] = true;
        }
    };

    //create a bogus data channel
    pc.createDataChannel('');

    //create an offer sdp
    pc.createOffer(function (result) {

        //trigger the stun server request
        pc.setLocalDescription(result, function () { }, function () { });

    }, function () { });
}

/**get service name by service code */
export function getServiceNameByServiceCd(serviceCd, serviceList) {
    if (!serviceCd) {
        return '';
    }

    if (!serviceList || serviceList.length === 0) {
        return serviceCd;
    }

    let service = serviceList.find(item => item.serviceCd === serviceCd);
    if (!service) {
        return serviceCd;
    }

    return service ? `${service.serviceCd} - ${service.serviceName}` : serviceCd;
}

// /**get clinic name by clinic code */
// export function getClinicNameByClinicCd(clinicCd, serviceCd, clinicList) {
//     if (!clinicCd) {
//         return '';
//     }

//     if (!serviceCd) {
//         return clinicCd;
//     }

//     if (!clinicList || clinicList.length === 0) {
//         return clinicCd;
//     }

//     let clinicGp = clinicList.filter(item => item.serviceCd === serviceCd);

//     if (!clinicGp) {
//         return clinicCd;
//     }

//     let clinic = clinicGp.find(item => item.clinicCd === clinicCd);

//     if (!clinic) {
//         return clinic;
//     }

//     return clinic.clinicName ? clinic.clinicName : clinicCd;

// }

export function getSubEncounterListByEncounterCd(encounterDtos, encounterTypeCd) {
    if (!encounterDtos || encounterDtos.length === 0) {
        return null;
    }
    let codeList = _.cloneDeep(encounterDtos).find(item => item.encounterTypeCd === encounterTypeCd);
    if (codeList && codeList.subEncounterTypeList && codeList.subEncounterTypeList.length > 0) {
        return codeList.subEncounterTypeList;
    } else {
        return null;
    }
}

export function getSubEncounterBySubEncounterCd(subEncounterDtos, subEncounterTypeCd) {
    return subEncounterDtos && subEncounterDtos.length > 0 ? subEncounterDtos.find(item => item.subEncounterTypeCd === subEncounterTypeCd) : null;
}

export function getServiceByCd(serviceList, serviceCd) {
    if(!serviceList || !Array.isArray(serviceList)){
        return null;
    }
    return serviceList.find(item => item.serviceCd === serviceCd);
}

export function getClinicNameByCd(clinicList, clinicCd) {
    if (!clinicList || !Array.isArray(clinicList)) {
        return null;
    }
    return clinicList.find(item => item.clinicCd === clinicCd);
}

export function getTimeMoment(time) {
    const timeArr = time.split(':');
    if(timeArr.length === 2){
        return moment({ hours: timeArr[0], minutes: timeArr[1] });
    } else if(timeArr.length === 3){
        return moment({ hours: timeArr[0], minutes: timeArr[1], seconds: timeArr[2] });
    }
    return null;
}

export function getHighestPriorityConfig(configName,
    configs,
    where,
    scoreMappings = {
        subEncounterCd: 10000,
        encounterCd: 1000,
        clinicCd: 100,
        serviceCd: 10
    }){

    let config = {};
    let targetConfigs = configs[configName];
    if (!targetConfigs) {
        return config;
    } else {
        let isMatch = (item, where, key) => item[key] && where[key] && item[key] === where[key];
        let isServiceMatch = (item, where) => isMatch(item, where, 'serviceCd');
        let isClinicMatch = (item, where) => isServiceMatch(item, where) && isMatch(item, where, 'clinicCd');
        let isEncounterMatch = (item, where) => isClinicMatch(item, where) && isMatch(item, where, 'encounterCd');
        let isSubEncounterMatch = (item, where) => isEncounterMatch(item, where) && isMatch(item, where, 'subEncounterCd');

        let filterConfigs = targetConfigs
        .filter(
            (item) => {
                return  (!item.clinicCd && !item.serviceCd && !item.encounterCd && !item.subEncounterCd) ||
                (isServiceMatch(item, where)) ||
                (isClinicMatch(item, where)) ||
                (isEncounterMatch(item, where)) ||
                (isSubEncounterMatch(item, where));
            }
        )
        .map(
            (item) => {
                let score = 0;
                for(let scoreMappingKey in scoreMappings){
                    if(item[scoreMappingKey] &&
                        where[scoreMappingKey]
                        ){
                            score += scoreMappings[scoreMappingKey];
                    }
                }
                return {...item, score};
            }
        ).sort((a, b) => b.score - a.score);
        if (filterConfigs && filterConfigs.length > 0) {
            config = filterConfigs[0];
        }
        return config;
    }
}