import accessRightEnum from '../enums/accessRightEnum';
import * as PatientUtil from './patientUtilities';
import * as commonUtilities from './commonUtilities';
import * as RegUtil from './registrationUtilities';
import Enum from '../enums/enum';
import moment from 'moment';

const defaultDisplayNoneCondition = (docTypeCds, allowEmpty = false, hkid = '') => {
    let containAnyHkidFormat = docTypeCds ? docTypeCds.find(item => PatientUtil.isHKIDFormat(item)) ? true : false : false;
    let isDocTypeIsEmpty = docTypeCds? docTypeCds.length > 0: true;
    return !(containAnyHkidFormat ||
    (allowEmpty && isDocTypeIsEmpty) || (allowEmpty || hkid !== ''));
};

export const getEligibleIndexFromEcsResult = (ecsStore) => {
    if(ecsStore){
        if(ecsStore.eligiblePerson1 === 'Y' || ecsStore.eligibleStaff1 === 'Y'){
            return 1;
        }
        if(ecsStore.eligiblePerson2 === 'Y' || ecsStore.eligibleStaff2 === 'Y'){
            return 2;
        }
    }
    return 0;
};

export const purifyHkid = (hkidWithBracket) => {
    return hkidWithBracket && hkidWithBracket.replace('(', '').replace(')','');
};


export const getHkidFromPatient = (patient) => {
    if(patient){
        if(patient.documentPairList){
            let optionalPatientDocPair = patient.documentPairList.find(item => item.docTypeCd === Enum.DOC_TYPE.HKID_ID);
            if(optionalPatientDocPair){
                return optionalPatientDocPair.docNo;
            }
        }
    }
    return null;
};

export const getProperDocTypeCdForEcs = (patient) => {
    if(patient){
        if(patient.documentPairList){
            let optionalPatientDocPair = patient.documentPairList.find(item => item.docTypeCd === Enum.DOC_TYPE.HKID_ID);
            if(!optionalPatientDocPair){
                optionalPatientDocPair = patient.documentPairList.find(item => PatientUtil.isHKIDFormat(item.docTypeCd));
            }
            if(optionalPatientDocPair){
                return optionalPatientDocPair.docTypeCd;
            }
        }
    }
    return null;
};

export const getProperHkicForEcs =  (patient) => {
    let patientDocType = patient.docTypeCd;
    let result = '';
    if(patient){
        if(patient.documentPairList){
            let hkid = getHkidFromPatient(patient);
            if(hkid){
                result = patient.hkid;
            } else if(PatientUtil.isHKIDFormat(patientDocType)){
                let optionalPatientOtherDocPair = patient.documentPairList.find(item => PatientUtil.isHKIDFormat(item.docTypeCd));
                if(optionalPatientOtherDocPair){
                    result = optionalPatientOtherDocPair.docNo;
                }
            }
        }
    }
    return purifyHkid(result);
};

export const isPatientEcsRelatedFieldChanged = (currentPatientInfo, nextPatientInfo) => {
    return !(purifyHkid(getProperHkicForEcs(currentPatientInfo))) === purifyHkid(getProperHkicForEcs(nextPatientInfo) &&
    currentPatientInfo.engSurname === nextPatientInfo.engSurname &&
    currentPatientInfo.engGivename === nextPatientInfo.engGivename &&
    currentPatientInfo.nameChi === nextPatientInfo.nameChi &&
    currentPatientInfo.genderCd === nextPatientInfo.genderCd &&
    moment(currentPatientInfo.dob).format('YYYY-MM-DD') === moment(nextPatientInfo.dob).format('YYYY-MM-DD'));
};

export const isPatientEcsRelatedFieldChangedByStore = (currentEcsStore, nextPatientInfo) => {
    return !(purifyHkid(currentEcsStore.originalHkid) === purifyHkid(getProperHkicForEcs(nextPatientInfo)));
};


export const isEcsButtonDisplayNone = (docTypeCds, allowEmpty, hkid = '') => {
    return defaultDisplayNoneCondition(docTypeCds, allowEmpty, hkid);
};

export const isOcsssDisplayNone = (docTypeCds, allowEmpty, hkid = '') => {
    return defaultDisplayNoneCondition(docTypeCds, allowEmpty, hkid);
};

export const isEcsEnable = (accessRights, docTypeCds, ecsUserId, locationCode, allowEmpty, interfaceStatus, hkid = '') => {
    const requireAccsseRightPairs = [{
        name:[accessRightEnum.ecsChecking],
        type:'button'
    }];

    return interfaceStatus &&
    commonUtilities.containAccessRights(accessRights, requireAccsseRightPairs) &&
    (
        ecsUserId !== '' && ecsUserId !== null && ecsUserId !== undefined &&
        locationCode !== '' && locationCode !== null && locationCode !== undefined
    ) &&
    !isEcsButtonDisplayNone(docTypeCds, allowEmpty, hkid);
};

export const isOcsssEnable = (accessRights, docTypeCds, interfaceStatus, hkid) => {
    const requireAccsseRightPairs = [{
        name:[accessRightEnum.ocsssChecking],
        type:'button'
    }];

    return interfaceStatus &&
    commonUtilities.containAccessRights(accessRights, requireAccsseRightPairs)&&
    !isOcsssDisplayNone(docTypeCds, false, hkid);
};


export const isMwecsEnable = (accessRights, interfaceStatus) => {
    const requireAccsseRightPairs = [{
        name:[accessRightEnum.mwecsChecking],
        type:'button'
    }];
 return interfaceStatus &&
    commonUtilities.containAccessRights(accessRights, requireAccsseRightPairs);
};