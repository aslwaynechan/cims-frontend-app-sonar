import axios from '../services/ccpAxiosInstance';

export function print(params) {
    axios.post('/ccp/check').then((check)=>{
        if (check.data && check.data.status === 'OK') {
            let qs = require('qs');
            let requireParams = {
                tid: params.taskId,
                pt: params.printType || 5,
                url: params.documentUrl || 'http://localhost:17300',
                docParm: params.documentParameters,
                b64: params.base64,
                que: params.printQueue,
                tc: params.printTray,
                fp: params.firstPage,
                lp: params.lastPage,
                ctr: params.isCenter || false,
                fit: params.isFitPage || false,
                shk: params.isShrinkPage || false,
                ps: params.paperSize,
                cps: params.copies,
                ori: params.orientation || -1,
                msz: params.mediaSize || 'N/A',
                os: params.isObjectStream || false,
                cb: params.callback,
                ref: params.referer,
                ver: check.data.agent_version,
                cbm: params.callbackMode || 1,
                pm: params.printMode,
                pdfPw: params.pdfPassword,
                sc: params.sheetCollate || 1
            };
            axios.post('/ccp/prn', qs.stringify(requireParams)).then((respan)=>{
                let data = respan.data;
                let printSuccess = false;
                if (data.success) {
                    printSuccess =true;
                }else{
                    printSuccess =false;
                    console.error(respan);
                }
                if (typeof params.callback === 'function') {
                    params.callback(printSuccess);
                }
            });
        } else {
            console.error(check);
        }
    }).catch( e => {
        params.callback(false);
    });
}
