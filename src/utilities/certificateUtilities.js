import Enum from '../enums/enum';

export function getAttendanceCertSessionValue(label) {
    const obj = Enum.ATTENDANCE_CERT_SESSION_LIST.find(item => item.label === label);
    return obj && obj.value;
}

export function getAttendanceCertSessionLabel(value) {
    const obj = Enum.ATTENDANCE_CERT_SESSION_LIST.find(item => item.value === value);
    return obj && obj.label;
}

export function getAttendanceCertForValue(label) {
    const obj = Enum.ATTENDANCE_CERT_FOR_LIST.find(item => item.label === label);
    return obj && obj.value;
}

export function getAttendanceCertForLabel(value) {
    const obj = Enum.ATTENDANCE_CERT_FOR_LIST.find(item => item.value === value);
    return obj && obj.label;
}