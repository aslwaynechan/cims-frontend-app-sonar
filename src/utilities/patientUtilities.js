import _ from 'lodash';
import moment from 'moment';
import Enum from '../enums/enum';
import storeConfig from '../store/storeConfig';
import * as caseNoUtilities from '../utilities/caseNoUtilities';
import * as patientUtilities from '../utilities/patientUtilities';

export function getPatientSearchResult(data) {
    let result = [];
    if (!data) return result;
    for (let i = 0; i < data.length; i++) {
        const patient = data[i];
        let temp = _.cloneDeep(patient);
        if (patient.hkid) {
            const hkid = patient.hkid.substring(0, patient.hkid.length - 1);
            const hkidNum = patient.hkid.substring(patient.hkid.length - 1);
            temp.hkidOrDocno = hkid + '(' + hkidNum + ')';
        } else {
            temp.hkidOrDocno = patient.otherDocNo + '(' + patient.docTypeCd + ')';
        }
        temp.engFullName = (patient.engSurname || '') + ',' + (patient.engGivename || '');
        temp.phoneAndCountry = (patient.countryCd != 'HKG' && patient.countryCd != '852') ? (patient.countryCd + '-' + patient.phoneNo) : patient.phoneNo;
        result.push(temp);
    }
    return result;
}

export function getHkidOrDocNo(patient) {
    let hkidOrDocNo = '';
    if (patient) {
        if (patient.hkid) {
            const hkid = patient.hkid.substring(0, patient.hkid.length - 1);
            const hkidNum = patient.hkid.substring(patient.hkid.length - 1);
            hkidOrDocNo = hkid + '(' + hkidNum + ')';
        } else if (patient.otherDocNo && patient.docTypeCd) {
            hkidOrDocNo = patient.otherDocNo + '(' + patient.docTypeCd + ')';
        }
    }
    return hkidOrDocNo;
}

export function getHkidFormat(hkid) {
    if (hkid && hkid.indexOf('(') === -1 && hkid.indexOf(')') === -1) {
        const hkidPre = hkid.substring(0, hkid.length - 1);
        const hkidNum = hkid.substring(hkid.length - 1);
        return hkidPre + '(' + hkidNum + ')';
    }
    return hkid;
}

export function getOtherDocNoFormat(otherDocNo, docTypeCd) {
    if (otherDocNo && docTypeCd){
        return `${otherDocNo}(${docTypeCd})`;
    }
    return otherDocNo;
}

export function filterPatientList(inputData, filterCondition) {
    let data = _.cloneDeep(inputData);
    let result = {};
    result.patientQueueDtos = [];
    if (data && data.patientQueueDtos) {
        for (let i = 0; i < data.patientQueueDtos.length; i++) {
            let dto = {};
            const queueDto = data.patientQueueDtos[i];
            if (queueDto.patientDto) {
                const patient = queueDto.patientDto;
                dto.name = `${patient.engSurname || ''}, ${patient.engGivename || ''}`;
                dto.hkic = getHkidOrDocNo(patient);
                dto.discNo = patient.discNo || '';
                dto.age = `${patient.age || ''}${patient.ageUnit ? patient.ageUnit[0] || '' : ''}`;
                dto.genderCd = patient.genderCd || '';
                dto.nameChi = patient.nameChi || '';
                dto.docTypeCd = patient.docTypeCd || '';
                dto.dob = patient.dob ? moment(patient.dob).format(Enum.DATE_FORMAT_EDMY_VALUE) : '';
                dto.engSurname = patient.engSurname || '';
                dto.engGivename = patient.engGivename || '';
                dto.phoneNo = patient.phoneNo || '';
                dto.deadInd = patient.deadInd || '';
            }
            dto.patientKey = queueDto.patientKey || '';
            dto.appointmentId = queueDto.appointmentId || '';
            dto.appointmentTime = `${queueDto.appointmentDate || ''} ${queueDto.appointmentTime || ''}`;
            //dto.arrivalTime = `${queueDto.arrivalDate || ''} ${queueDto.arrivalTime || ''}`;
            dto.arrivalTime = queueDto.attnTime || '';
            dto.encounterType = queueDto.encounterType || '';
            dto.subEncounterType = queueDto.subEncounterType || '';
            dto.status = queueDto.attnStatus || '';
            dto.encounterTypeCd = queueDto.encounterTypeCd || '';
            dto.subEncounterTypeCd = queueDto.subEncounterTypeCd || '';
            dto.statusCd = queueDto.attnStatusCd || '';
            dto.remark = queueDto.remark || '';
            dto.version = queueDto.version || '';
            dto.discNo = queueDto.discNumber || '';
            dto.caseNo = queueDto.caseNo || '';
            dto.encounterDto = queueDto.encounterDto || null;

            //condition filter
            if (
                (!filterCondition.attnStatusCd || dto.statusCd === filterCondition.attnStatusCd)
                && (!filterCondition.encounterTypeCd || dto.encounterTypeCd === filterCondition.encounterTypeCd)
                && (!filterCondition.subEncounterTypeCd || dto.subEncounterTypeCd === filterCondition.subEncounterTypeCd)
                && (!filterCondition.hkic || dto.hkic === filterCondition.hkic)
            ) {
                result.patientQueueDtos.push(dto);
            }
        }
    }
    return result;
}

export function get_PatientList_TableRow_ByServiceCd(actionRender, statusRender) {
    let caseNoRender = (value) => {
        return caseNoUtilities.getFormatCaseNo(value);
    };
    let rows = [
        { name: 'hkic', label: 'HKIC/Doc No', width: 100 },
        { name: 'caseNo', label: 'Case No', width: 40, customBodyRender: caseNoRender },
        { name: 'name', label: 'Name', width: 80 },
        { name: 'encounterTypeCd', label: 'Encounter', width: 80 },
        { name: 'subEncounterTypeCd', label: 'Sub Encounter', width: 80 },
        { name: 'appointmentTime', label: 'Appointment Time', width: 100 },
        { name: 'attnTime', label: 'Arrival Time', width: 100 },
        { name: 'phoneNo', label: 'Phone', width: 60 },
        { name: 'discNo', label: 'Disc No', width: 40 },
        { name: 'attnStatus', label: 'Attn.', width: 80, customBodyRender: statusRender },
        { name: 'action', label: '', width: 110, align: 'center', customBodyRender: actionRender }
    ];

    const store = storeConfig.store.getState();
    const listConfig = store && store['common']['listConfig'];
    if (listConfig && listConfig.PATIENT_LIST) {
        const list = listConfig.PATIENT_LIST.sort(function (a, b) {
            return a.displayOrder - b.displayOrder;
        });
        rows = [];
        for (let i = 0; i < list.length; i++) {
            let newRow = {
                name: list[i]['labelCd'],
                label: list[i]['labelName'],
                width: list[i]['labelLength'],
                split: list[i]['site'] === '1'
            };
            if (list[i].labelCd === 'status') {
                newRow.customBodyRender = statusRender;
            }
            else if (list[i].labelCd === 'caseNo') {
                newRow.customBodyRender = caseNoRender;
            }
            rows.push(newRow);
        }
        rows.push({ name: 'action', label: '', width: 110, align: 'center', customBodyRender: actionRender });
    }
    return rows;
}


export function isHKIDFormat(docTypeCd) {
    const isHKIDFormat = docTypeCd === Enum.DOC_TYPE.HKID_ID ||
        docTypeCd === Enum.DOC_TYPE.BIRTH_CERTIFICATE_HK ||
        docTypeCd === Enum.DOC_TYPE.EXEMPTION_CERTIFICATE ||
        docTypeCd === Enum.DOC_TYPE.RECONGIZANCE;

    return isHKIDFormat;
}

export function transferPatientDocumentPair(patientInfo) {
    if (patientInfo && patientInfo.documentPairList && patientInfo.documentPairList.length > 0) {
        const hkidDto = patientInfo.documentPairList.find(item => item.docTypeCd === Enum.DOC_TYPE.HKID_ID);
        //at present, only get the first other document
        const otherDocDto = patientInfo.documentPairList.find(item => item.docTypeCd !== Enum.DOC_TYPE.HKID_ID);
        patientInfo.hkid = hkidDto && hkidDto.docNo.trim();
        patientInfo.docTypeCd = otherDocDto ? otherDocDto.docTypeCd : '';
        patientInfo.otherDocNo = otherDocDto ? otherDocDto.docNo : '';
    }
    return patientInfo;
}