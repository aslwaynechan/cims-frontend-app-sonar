import Enum from '../enums/enum';

export function getFormatCaseNo(caseNo) {
    if (caseNo) {
        caseNo = caseNo.substr(0, 4) + '-' + caseNo.substr(4, 2) + '-' + caseNo.substr(6, caseNo.length - 7) + '(' + caseNo.substr(caseNo.length - 1, 1) + ')';
    }
    return caseNo;
}

export function getCaseNoStatus(status) {
    const obj = Enum.CASE_STATUS_LIST.find(item => item.value === status);
    return obj && obj.label;
}

export function getCaseNoPromptStr(status) {
    const obj = Enum.CASE_STATUS_LIST.find(item => item.value === status);
    return obj && obj.promptUpStr;
}