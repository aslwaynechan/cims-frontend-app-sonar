import CommonRegex from '../constants/commonRegex';
import RegFieldnameForError from '../enums/registration/regFieldnameForError';
import axios from '../services/axiosInstance';
import Enum from '../enums/enum';
import moment from 'moment';
import {
    patientAddressBasic,
    patientPhonesBasic,
    patientSocialDataBasic,
    patientContactInfoBasic,
    patientBaseInfoBasic
} from '../constants/registration/registrationConstants';
import _ from 'lodash';

export function limitTextFieldMaxLength(value, maxLength) {
    let regex = new RegExp(CommonRegex.VALIDATION_REGEX_POSITIVE_INTEGER);
    if (regex.test(maxLength) && value) {
        let max = parseInt(maxLength);
        if (value.length > max) {
            value = value.slice(0, max);
        }
    }
    return value;
}

export function limitTextFieldTrim(value) {
    if (value) {
        return value.replace(CommonRegex.VALIDATION_REGEX_BLANK_SPACE, '');
    }
    return value;
}

export async function checkChinese(value) {
    let result = false;
    if (!value)
        return true;
    let response = await axios.post('/patient/chiNameValidator', { character: value });
    if (response.status === 200) {
        if (response.data.respCode === 0) {
            result = response.data.data;
        }
    }
    return result;
}

export async function checkHKID(value) {
    let result = false;
    if (!value)
        return true;
    value = value.replace('(', '').replace(')', '');
    let response = await axios.post('/patient/hkidValidator', { hkid: value });
    if (response.status === 200) {
        if (response.data.respCode === 0) {
            result = response.data.data;
        }
    }
    return result;
}

export function IsHKIDbyJS(str) {
    // let strValidChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    // basic check length
    if (str.length < 8)
        return false;

    // handling bracket
    if (str.charAt(str.length - 3) === '(' && str.charAt(str.length - 1) === ')')
        str = str.substring(0, str.length - 3) + str.charAt(str.length - 2);

    // convert to upper case
    str = str.toUpperCase();

    // regular expression to check pattern and split
    let hkidPat = /^([A-Z]{1,2})([0-9]{6})([A0-9])$/;
    let matchArray = str.match(hkidPat);

    // not match, return false
    if (matchArray === null)
        return false;

    return true;
}

export function checkIsDateTime(value, format) {
    let regStr = '';
    if (format === Enum.DATE_FORMAT_EY_VALUE) {
        regStr = /^(\d{4})$/;
    } else if (format === Enum.DATE_FORMAT_EMY_VALUE) {
        regStr = /^([a-zA-Z]{3})-(\d{4})$/;
    } else if (format === Enum.DATE_FORMAT_EDMY_VALUE) {
        regStr = /^(\d{2})-([a-zA-Z]{3})-(\d{4})$/;
    } else {
        return false;
    }

    if (!regStr.test(value)) {
        return false;
    }

    try {
        let mo = moment(value);
        return mo._isValid;
    } catch (err) {
        return false;
    }
}

export function getHkid(value) {
    if (value) {
        value = value.trim();
        value = value.substr(0, value.length - 1);
    }
    return value;
}

export function getHkidNum(value) {
    if (value)
        value = value.substr(value.length - 1);
    return value;
}

export function hkidFormat(value) {
    if (value) {
        value = value.replace('(', '').replace(')', '');
        return _.toUpper(value.slice(0, value.length - 1) + '(' + value.slice(value.length - 1) + ')');
    }
    return value;
}

export function getTitleByFiledName(fieldName) {
    let value = RegFieldnameForError[fieldName];
    return value || fieldName;
}

export function getDateFormat(edob) {
    switch (edob) {
        case Enum.DATE_FORMAT_EY_KEY: {
            return Enum.DATE_FORMAT_EY_VALUE;
        }
        case Enum.DATE_FORMAT_EMY_KEY: {
            return Enum.DATE_FORMAT_EMY_VALUE;
        }
        case Enum.DATE_FORMAT_EDMY_KEY: {
            return Enum.DATE_FORMAT_EDMY_VALUE;
        }
        default: {
            return Enum.DATE_FORMAT_EDMY_VALUE;
        }
    }
}

export function convertBase64UrlToBlob(base64Url) {
    let arr = base64Url.split(',');
    let mime = arr[0].match(/:(.*?);/)[1];
    let bytes = window.atob(base64Url.split(',')[1]);
    let ab = new ArrayBuffer(bytes.length);
    let ia = new Uint8Array(ab);
    for (let i = 0; i < bytes.length; i++) {
        ia[i] = bytes.charCodeAt(i);
    }
    return new Blob([ab], { type: mime });
}

export function getDateByFormat(edob, date) {
    switch (edob) {
        case Enum.DATE_FORMAT_EY_KEY: {
            return moment(date, Enum.DATE_FORMAT_EYMD_VALUE).startOf('year');
        }
        case Enum.DATE_FORMAT_EMY_KEY: {
            return moment(date, Enum.DATE_FORMAT_EYMD_VALUE).startOf('month');
        }
        case Enum.DATE_FORMAT_EDMY_KEY: {
            return moment(date, Enum.DATE_FORMAT_EYMD_VALUE);
        }
        default: {
            return moment(date, Enum.DATE_FORMAT_EYMD_VALUE);
        }
    }
}

function addPatientPhone(type) {
    let ppb = _.cloneDeep(patientPhonesBasic);
    ppb.phoneTypeCd = type;
    if (type === Enum.PHONE_TYPE_MOBILE_PHONE) {
        ppb.phonePriority = 1;
        ppb.smsPhoneInd = '1';
    } else {
        ppb.phonePriority = 0;
        ppb.smsPhoneInd = '0';
    }
    return ppb;
}
function addPatientAddress(type) {
    let pab = _.cloneDeep(patientAddressBasic);
    pab.addressTypeCd = type;
    return pab;
}
export function initPatientPhone() {
    let phoneList = [];
    phoneList.push(addPatientPhone(Enum.PHONE_TYPE_HOME_PHONE));
    return phoneList;
}
export function initPatientAddress() {
    let addressList = [];
    addressList.push(addPatientAddress(Enum.PATIENT_CORRESPONDENCE_ADDRESS_TYPE));
    addressList.push(addPatientAddress(Enum.PATIENT_RESIDENTIAL_ADDRESS_TYPE));
    return addressList;
}
export function initPatientSocialData(patientById) {
    let patientSocialData = _.cloneDeep(patientSocialDataBasic);
    if (patientById) {
        patientSocialData.ethnicityCd = patientById.ethnicityCd || '';
        patientSocialData.maritalStatusCd = patientById.maritalStatusCd || '';
        patientSocialData.religionCd = patientById.religionCd || '';
        patientSocialData.occupationCd = patientById.occupationCd || '';
        patientSocialData.translationLangCd = patientById.translationLangCd || '';
        patientSocialData.eduLevelCd = patientById.eduLevelCd || '';
        patientSocialData.govDptCd = patientById.govDptCd || '';
        patientSocialData.rank = patientById.rank || '';
        patientSocialData.remarks = patientById.remarks || '';
    }
    return patientSocialData;
}
export function initPatientContactInfo(patientById) {
    let patientContactInfo = _.cloneDeep(patientContactInfoBasic);
    if (patientById) {
        patientContactInfo.communicationMeansCd = patientById.communicationMeansCd || patientContactInfo.communicationMeansCd;
        patientContactInfo.emailAddress = patientById.emailAddress;
        // patientContactInfo.preferredLangCd = patientById.preferredLangCd || patientContactInfo.preferredLangCd;
        patientContactInfo.communicateLangCd = patientById.communicateLangCd || patientContactInfo.communicateLangCd;
        patientContactInfo.postOfficeBoxNo = patientById.postOfficeBoxNo;
        patientContactInfo.postOfficeName = patientById.postOfficeName;
        patientContactInfo.postOfficeRegion = patientById.postOfficeRegion;
    }
    return patientContactInfo;
}
export function initPatientBaseInfo(patientById) {
    let patientBaseInfo = _.cloneDeep(patientBaseInfoBasic);
    if (patientById) {
        for (let k in patientBaseInfo) {
            if (k !== 'phone') {
                patientBaseInfo[k] = patientById[k];
            }
        }
        patientBaseInfo.phone = patientById.phoneList && patientById.phoneList.find(item => item.phonePriority === 1);
        if (!patientBaseInfo.phone) {
            patientBaseInfo.phone = addPatientPhone(Enum.PHONE_TYPE_MOBILE_PHONE);
        }
    }
    if (patientBaseInfo.hkid && !patientBaseInfo.otherDocNo) {
        patientBaseInfo.docTypeCd = '';
    }
    patientBaseInfo.documentPairList = patientById.documentPairList || [];
    return patientBaseInfo;
}


function tranformatMonth(month) {
    let mon = _.toUpper(month);
    let num = '';
    switch (mon) {
        case 'JAN':
            num = '01';
            break;
        case 'FEB':
            num = '02';
            break;
        case 'MAR':
            num = '03';
            break;
        case 'APR':
            num = '04';
            break;
        case 'MAY':
            num = '05';
            break;
        case 'JUN':
            num = '06';
            break;
        case 'JUL':
            num = '07';
            break;
        case 'AUG':
            num = '08';
            break;
        case 'SEP':
            num = '09';
            break;
        case 'OCT':
            num = '10';
            break;
        case 'NOV':
            num = '11';
            break;
        case 'DEC':
            num = '12';
            break;
        default: break;
    }
    return num;
}

export function tranformatDate(value) {
    if (value) {
        let arr = value.split('-');
        arr[1] = tranformatMonth(arr[1]);
        value = arr.join('-');
    }
    return value;
}

export function replaceIndexChar(string,index,char) {
    string=string||'';
    index=index||[];
    char = char||'';
    string = string.substring(0,index)+ char;
    return string;
}

export function trimString(params) {
    if (params) {
        params = params.trim();
    } else {
        params = '';
    }
    return params;
}

const WINDOW_ADI_WINDOW_KEY = 'ADI_WINDOW';
export const closeActiveADIWindow = () => {
    const isADIWindowActive = () => window[WINDOW_ADI_WINDOW_KEY] !== undefined;
    const getActiveADIWindow = () => window[WINDOW_ADI_WINDOW_KEY];
    if (isADIWindowActive()) {
        getActiveADIWindow().close();
        window[WINDOW_ADI_WINDOW_KEY] = undefined;
    }
};

export const openADISearchDialog = (adiUrl, callback, closingWindow) => {
    closeActiveADIWindow();
    const h = 600;
    const w = 800;
    const left = (screen.width / 2) - (w / 2);
    const top = (screen.height / 2) - (h / 2);

    let newADIWindow = window.open(adiUrl, '',
        'toolbar=no,location=no' +
        'directories=no,status=no,menubar=no,scrollbars=yes,' +
        'resizable=yes,width=' + w + ',height=' + h + ', top=' + top + ', left=' + left);
    if (newADIWindow) {
        let timer = setInterval(() => {
            if(newADIWindow.closed) {
                clearInterval(timer);
                closingWindow();
                window[WINDOW_ADI_WINDOW_KEY] = undefined;
            }
        }, 500);

        window[WINDOW_ADI_WINDOW_KEY] = newADIWindow;

        window.setADIAddress = (status,
            unit, floor, block, building,
            estate, streetNo, streetName, subDistrict, district, region, buildingCsuId,
            northing, easting,
            latitude, longitude
        ) => {
            if (callback) {
                status = trimString(status);
                unit = trimString(unit);
                floor = trimString(floor);
                block = trimString(block);
                building = trimString(building);
                estate = trimString(estate);
                streetNo = trimString(streetNo);
                streetName = trimString(streetName);
                subDistrict = trimString(subDistrict);
                district = trimString(district);
                region = trimString(region);
                buildingCsuId = trimString(buildingCsuId);
                callback({
                    status,
                    unit,
                    floor,
                    block,
                    building,
                    estate,
                    streetNo,
                    streetName,
                    district,
                    subDistrict,
                    region,
                    buildingCsuId
                });
            }
        };
    }
};