import storeConfig from '../store/storeConfig';

let generatePatientEncounterGroupDto = (caseDto,groups) => {
  if (groups.length > 0) {
    groups.forEach(groupObj => {
      caseDto.encounterGroupDtos.push({
        groupId:groupObj.groupId,
        groupName:groupObj.groupName,
        serviceCd:groupObj.serviceCd,
        clinicCd:groupObj.clinicCd,
        encounterTypeCd:groupObj.encounterTypeCd,
        subEncounterTypeCd:groupObj.subEncounterTypeCd
      });
    });
  }
};

let generatePatientCaseDto = (patientDto,infoObj) => {
    if(infoObj){
      let caseDto={
        caseNo: infoObj.caseNo,
        ownerClinicCd: infoObj.ownerClinicCd,
        regDtm: infoObj.regDtm,
        statusCd: infoObj.statusCd,
        encounterGroupDtos:infoObj.encounterGroupDtos
      };
      // generatePatientEncounterGroupDto(caseDto,infoObj.encounterGroupDtos);
      patientDto.caseNoInfo=caseDto;
    }
  // if (infoObj.caseDtos.length > 0) {
  //   infoObj.caseDtos.forEach(caseObj => {
  //     let caseDto = {
  //       caseNo: caseObj.caseNo,
  //       ownerClinicCd: caseObj.ownerClinicCd,
  //       regDtm: caseObj.regDtm,
  //       encounterTypeCd: null,
  //       subEncounterTypeCd: null,
  //       statusCd: caseObj.statusCd,
  //       patientStatus: null,
  //       caseReference: null,
  //       remark: null,
  //       version: null,
  //       patientKey: null,
  //       caseSeq: null,
  //       serviceCd: null,
  //       casePrefixCd: null,
  //       encounterGroupDtos: []
  //     };
  //     generatePatientEncounterGroupDto(caseDto,caseObj.encounterGroupDtos);
  //     patientDto.caseDtos.push(caseDto);
  //   });
  // }
};

export function generatePatientDto() {
  const store = storeConfig.store.getState();
  const patientInfo = store && store['patient']['patientInfo'];
  const caseNoInfo = store && store['patient']['caseNoInfo'];
  let dto = {
    patientKey: patientInfo.patientKey,
    hkId: patientInfo.hkid,
    engSurname: patientInfo.engSurname,
    engGivename: patientInfo.engGivename,
    nameChi: patientInfo.nameChi,
    genderCd: patientInfo.genderCd,
    otherName: null,
    dob: patientInfo.dob,
    docTypeCd: patientInfo.docTypeCd,
    otherDocNo: patientInfo.otherDocNo,
    birthPlaceCd: patientInfo.birthPlaceCd,
    age: patientInfo.age,
    caseNoInfo: {}
  };
  generatePatientCaseDto(dto,caseNoInfo);

  return dto;
}