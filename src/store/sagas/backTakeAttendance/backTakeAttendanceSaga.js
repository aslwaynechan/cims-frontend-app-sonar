import { take, takeLatest, call, put } from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as commonType from '../../actions/common/commonActionType';
import * as backTakeAttendanceActionTypes from '../../actions/appointment/backTakeAttendance/backTakeAttendanceAcitonType';
import * as messageType from '../../actions/message/messageActionType';



function* getPatientStatus() {
    while (true) {
        try {
            let { params } = yield take(backTakeAttendanceActionTypes.GET_PATIENT_STATUS);
            let { data } = yield call(axios.post, '/common/listCodeList', params);
            if (data.respCode === 0) {
                yield put({
                    type: backTakeAttendanceActionTypes.PATIENT_STATUS_LIST,
                    data: data.data
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* fetchApptList(action) {
    let { data } = yield call(axios.post, '/appointment/listAppointment', action.params);
    try {
        if (data.respCode === 0) {
            yield put({
                type: backTakeAttendanceActionTypes.PUT_APPOINTMENT_LIST,
                appointmentList: data.data
            });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* getApptList() {
    yield takeLatest(backTakeAttendanceActionTypes.LIST_APPOINTMENT, fetchApptList);
}


function* backTakeAttendance() {
    while (true) {
        try {
            let { params, searchParams } = yield take(backTakeAttendanceActionTypes.BACK_TAKE_ATTENDANCE);
            let { data } = yield call(axios.post, '/appointment/takeAttendance', params);
            if (data.respCode === 0) {
                yield put({
                    type: backTakeAttendanceActionTypes.BACK_TAKE_ATTENDANCE_SUCCESS,
                    data: data.data
                });
                yield put({
                    type: backTakeAttendanceActionTypes.LIST_APPOINTMENT,
                    params: searchParams
                });
            // } else if (data.respCode === 11) {
            //     yield put({
            //         type: messageType.OPEN_COMMON_MESSAGE,
            //         payload: {
            //             msgCode: '110250'
            //         }
            //     });
            } else if (data.respCode === 100) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110251'
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}


export const backTakeAttendanceSaga = [
    getPatientStatus(),
    getApptList(),
    backTakeAttendance()
];
