import { take, call, put, select } from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as types from '../../actions/assessment/assessmentActionType';
import * as commonTypes from '../../actions/common/commonActionType';
import * as messageTypes from '../../actions/message/messageActionType';
import { isUndefined,cloneDeep,find,isNull,toNumber } from 'lodash';

// for temporary test
// get service list
function* getTempServiceList() {
  while (true) {
    let {params,callback} = yield take(types.GET_TEMP_SERVICE_LIST);
    try {
      let { data } = yield call(axios.post, '/common/listCodeList', params);
      if (data.respCode === 0) {
        yield put({
          type: types.TEMP_SERVICE_LIST,
          tempServiceList: data.data
        });
        callback&&callback();
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.message ? data.message : 'Service error'
        });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// get assessment setting list(checkbox items)
function* getAssessmentSettingItemList() {
  while (true) {
    let { params,callback } = yield take(types.GET_ASSESSMENT_SETTING_ITEM_LIST);
    try {
      let { data } = yield call(axios.get, '/assessment/listCodeAssessmentList');
      if (data.respCode === 0) {
        yield put({
          type: types.ASSESSMENT_SETTING_ITEM_LIST,
          assessmentSettingList: data.data
        });
        yield put({
          type: types.GET_ASSESSMENT_CHECKED_ITEM_LIST,
          params,
          callback
        });
      } else {
        yield put({
          type: messageTypes.OPEN_COMMON_MESSAGE,
          paylaod:{
            msgCode:data.msgCode
          }
        });
        // yield put({
        //   type: commonTypes.OPEN_ERROR_MESSAGE,
        //   error: data.errMsg ? data.errMsg : 'Service error'
        // });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// get assessment setting value(checked items)
function* getCheckedAssessmentItemList() {
  while (true) {
    let { params,callback } = yield take(types.GET_ASSESSMENT_CHECKED_ITEM_LIST);
    try {
      let { data } = yield call(axios.get, '/assessment/listAssessmentItemByServiceCd',{params:params});
      if (data.respCode === 0) {
        let checkedArray = data.data;
        let tempSet = new Set();
        checkedArray.forEach(item => {
          tempSet.add(item.codeAssessmentCd);
        });
        callback&&callback(tempSet);
      } else {
        yield put({
          type: messageTypes.OPEN_COMMON_MESSAGE,
          paylaod:{
            msgCode:data.msgCode
          }
        });
        // yield put({
        //   type: commonTypes.OPEN_ERROR_MESSAGE,
        //   error: data.errMsg ? data.errMsg : 'Service error'
        // });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// save/update assessment setting value
function* updateAssessmentSettingItemList() {
  while (true) {
    let { params,callback } = yield take(types.UPDATE_ASSESSMENT_SETTING_ITEM_LIST);
    try {
      let { data } = yield call(axios.post, '/assessment/insertAssessmentItems', params);
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      if (data.respCode === 0) {
        callback&&callback(data.msgCode);
      } else {
        yield put({
          type: messageTypes.OPEN_COMMON_MESSAGE,
          paylaod:{
            msgCode:data.msgCode
          }
        });
        // yield put({
        //     type: commonTypes.OPEN_ERROR_MESSAGE,
        //     error: data.errMsg ? data.errMsg : 'Service error'
        // });
      }
    } catch (error) {
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: error.message ? error.message : 'Service error',
          data: error
      });
    }
  }
}

// get general assessment items(exclude value)
function* getPatientAssessmentListByServiceCd() {
  while (true) {
    let { params,callback } = yield take(types.GET_PATIENT_ASSESSMENT_LIST);
    try {
      let { data } = yield call(axios.get, '/assessment/listCodeAssessmentAndFieldByServiceCd', {params:params});
      if (data.respCode === 0) {
        let outputAssesmentFieldMap = new Map();
        if (!isNull(data.data)) {
          data.data.forEach(element=>{
            let fieldStructure = element.fields;
            fieldStructure.forEach(field=>{
              if (field.codeObjectTypeCd === 'OB') {
                outputAssesmentFieldMap.set(element.codeAssessmentCd,field.codeAssessmentFieldId);
              }
            });
          });
          yield put({
            type: types.PATIENT_ASSESSMENT_LIST,
            patientAssessmentList: data.data,
            outputAssesmentFieldMap
          });
          yield put({
            type: types.GET_PATIENT_ASSESSMENT_VAL,
            params,
            callback
          });
        }
        callback&&callback();
      } else {
        yield put({
            type: commonTypes.OPEN_ERROR_MESSAGE,
            error: data.errMsg ? data.errMsg : 'Service error'
        });
      }
    } catch (error) {
      yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: error.message ? error.message : 'Service error',
          data: error
      });
    }
  }
}

// get general assessment items value
function* getPatientAssessmentVal() {
  while (true) {
    let { params,callback } = yield take(types.GET_PATIENT_ASSESSMENT_VAL);
    try {
      let { data } = yield call(axios.get, '/assessment/getAssessmentByEncounterIdAndClinicCdAndServiceCd', {params:params});
      if (data.respCode === 0) {
        let patientAssessmentList = yield select(state => state.assessment.patientAssessmentList);
        let fieldValMap = new Map();
        let resultIdMap = new Map();
        let versionMap = new Map();
        let createdByMap = new Map();
        let createdDtmMap = new Map();
        let { assessmentValueDtos } = data.data;
        //prepare
        patientAssessmentList.forEach(element => {
          let fieldStructure = element.fields[0];
          let tempMap = new Map();
          let tempResultIdMap = new Map();
          let tempVersionMap = new Map();
          let tempCreatedByMap = new Map();
          let tempCreatedDtmMap = new Map();
          fieldStructure.forEach(fieldObj=>{
            tempMap.set(fieldObj.codeAssessmentFieldId,[{val:'',isError:false}]);
            tempResultIdMap.set(fieldObj.codeAssessmentFieldId,[null]); //add new
          });
          fieldValMap.set(element.codeAssessmentCd,tempMap);
          resultIdMap.set(element.codeAssessmentCd,tempResultIdMap);
          versionMap.set(element.codeAssessmentCd,tempVersionMap);
          createdByMap.set(element.codeAssessmentCd,tempCreatedByMap);
          createdDtmMap.set(element.codeAssessmentCd,tempCreatedDtmMap);
        });

        if (assessmentValueDtos.length !== 0) {
          assessmentValueDtos.forEach(assessment => {
            let { codeAssessmentCd,fieldValueDtos } = assessment;
            let tempMap = new Map();
            let tempResultIdMap = new Map();
            let tempVersionMap = new Map();
            let tempCreatedByMap = new Map();
            let tempCreatedDtmMap = new Map();
            fieldValueDtos.forEach(field=>{
              let val = !isUndefined(field.assessmentResults)&&field.assessmentResults.length>0?
                field.assessmentResults.map(result=>{
                  return({
                    val:isNull(result)?'':result,
                    isError:false
                  });
                })
                :[{val:'',isError:false}];
              tempMap.set(field.codeAssessmentFieldId,val);

              // result id
              let tempResultIds = field.resultIds;
              tempResultIdMap.set(field.codeAssessmentFieldId,tempResultIds);

              // version
              let tempVersion = field.version;
              tempVersionMap.set(field.codeAssessmentFieldId,tempVersion);

              // created by
              let tempCreatedBy = field.createdBys;
              tempCreatedByMap.set(field.codeAssessmentFieldId,tempCreatedBy);

              // created dtm
              let tempCreatedDtm = field.createdDtms;
              tempCreatedDtmMap.set(field.codeAssessmentFieldId,tempCreatedDtm);

              if (!isUndefined(field.assessmentResults)&&field.assessmentResults.length>0) {
                if (patientAssessmentList.length>0) {
                  let tempAssessment = find(patientAssessmentList,item=>{
                    return item.codeAssessmentCd === codeAssessmentCd;
                  });
                  if (!isUndefined(tempAssessment)) {
                    if (field.assessmentResults.length!==tempAssessment.fields.length) {
                      tempAssessment.fields.push(cloneDeep(tempAssessment.fields[0]));
                    }
                  }
                }
              }
            });
            fieldValMap.set(codeAssessmentCd,tempMap);
            resultIdMap.set(codeAssessmentCd,tempResultIdMap);
            versionMap.set(codeAssessmentCd,tempVersionMap);
            createdByMap.set(codeAssessmentCd,tempCreatedByMap);
            createdDtmMap.set(codeAssessmentCd,tempCreatedDtmMap);
          });
        }
        yield put({
          type: types.PATIENT_ASSESSMENT_VAL,
          patientAssessmentValMap: fieldValMap,
          resultIdMap,
          versionMap,
          createdByMap,
          createdDtmMap,
          patientAssessmentList
        });
        callback&&callback();
      } else {
        yield put({
            type: commonTypes.OPEN_ERROR_MESSAGE,
            error: data.errMsg ? data.errMsg : 'Service error'
        });
      }
    } catch (error) {
      yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: error.message ? error.message : 'Service error',
          data: error
      });
    }
  }
}

// get field dropdown list
function* getFieldDropList() {
  while (true) {
    let { params,callback } = yield take(types.GET_FIELD_DROP_LIST);
    try {
      let { data } = yield call(axios.get, '/assessment/listCodeAssessmentDrop', params);
      if (data.respCode === 0) {
        let cascadeDropMap = new Map();
        let emptyCascadeFieldMap = new Map();
        data.data.forEach(element=>{
          element.subSet.forEach((optionObj,index)=>{
            if (!isNull(optionObj.dependedFieldId)) {
              cascadeDropMap.set(optionObj.dependedDropId,{
                fieldId:element.fieldId,
                subSetId:index
              });
              emptyCascadeFieldMap.set(optionObj.dependedFieldId,element.fieldId);
            }
          });
        });
        yield put({
          type: types.FIELD_DROP_LIST,
          fieldDropList: data.data,
          cascadeDropMap,
          emptyCascadeFieldMap
        });
        callback&&callback();
      } else {
        yield put({
            type: commonTypes.OPEN_ERROR_MESSAGE,
            error: data.errMsg ? data.errMsg : 'Service error'
        });
      }
    } catch (error) {
      yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: error.message ? error.message : 'Service error',
          data: error
      });
    }
  }
}

// get field normal range list
function* getFieldNormalRangeList() {
  while (true) {
    let { params } = yield take(types.GET_FIELD_NORMAL_RANGE_MAP);
    try {
      let { data } = yield call(axios.get, '/assessment/listAssessmentNormalRange', {params:params});
      if (data.respCode === 0) {
        let fieldNormalRangeMap = new Map();
        data.data.forEach(element=>{
          let fieldMap = new Map();
          element.fields.forEach(field=>{
            if (fieldMap.has(field.fieldId)&&field.lowestVal === field.highestVal) {
              // DL & CB
              let tempSet = fieldMap.get(field.fieldId);
              tempSet.add(field.lowestVal);
              fieldMap.set(field.fieldId,tempSet);
            } else {
              if (field.lowestVal === field.highestVal) {
                // DL & CB
                let tempSet = new Set();
                tempSet.add(field.lowestVal);
                fieldMap.set(field.fieldId,tempSet);
              } else {
                fieldMap.set(field.fieldId,{
                  lowestVal:toNumber(field.lowestVal),
                  highestVal:toNumber(field.highestVal)
                });
              }
            }
          });
          fieldNormalRangeMap.set(element.assessmentCd,fieldMap);
        });
        yield put({
          type: types.FIELD_NORMAL_RANGE_MAP,
          fieldNormalRangeMap
        });
      } else {
        yield put({
            type: commonTypes.OPEN_ERROR_MESSAGE,
            error: data.errMsg ? data.errMsg : 'Service error'
        });
      }
    } catch (error) {
      yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: error.message ? error.message : 'Service error',
          data: error
      });
    }
  }
}

// save/update general assessment value
function* updatePatientAssessment() {
  while (true) {
    let { params,callback } = yield take(types.UPDATE_PATIENT_ASSESSMENT);
    try {
      let { data } = yield call(axios.post, '/assessment/saveAssessment', params);
      // yield put({
      //   type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      // });
      if (data.respCode === 0) {
        yield put({
          type:types.GET_PATIENT_ASSESSMENT_VAL,
          params:{
            encounterId: params.encounterId,
            clinicCd: params.clinicCd,
            serviceCd: params.serviceCd
          }
        });
        yield put({
          type: messageTypes.OPEN_COMMON_MESSAGE,
          payload:{
            msgCode: data.msgCode,
            showSnackbar:true
          }
        });
        callback&&callback();
      } else {
        if (!isNull(data.msgCode)) {
          yield put({
            type: messageTypes.OPEN_COMMON_MESSAGE,
            payload:{
              msgCode: data.msgCode
            }
          });
        } else {
          yield put({
            type: commonTypes.OPEN_ERROR_MESSAGE,
            error: data.message ? data.message : 'Service error'
          });
        }
      }
    } catch (error) {
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: error.message ? error.message : 'Service error',
          data: error
      });
    }
  }
}

function* saveAll() {
  while (true) {
    let { params,callback } = yield take(types.SAVE_ALL);
    try {
      let { data } = yield call(axios.post, '/assessment/saveConsultation', params);
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      if (data.respCode === 0) {
        callback&&callback(data);
      } else {
        yield put({
          type: messageTypes.OPEN_COMMON_MESSAGE,
          paylaod:{
            msgCode:data.msgCode
          }
        });
      }
    } catch (error) {
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: error.message ? error.message : 'Service error',
          data: error
      });
    }
  }
}

export const assessmentSagas = [
  getAssessmentSettingItemList(),
  updateAssessmentSettingItemList(),
  getCheckedAssessmentItemList(),
  getPatientAssessmentListByServiceCd(),
  getPatientAssessmentVal(),
  getFieldDropList(),
  getFieldNormalRangeList(),
  updatePatientAssessment(),
  // for temporary test
  getTempServiceList(),
  saveAll()
];