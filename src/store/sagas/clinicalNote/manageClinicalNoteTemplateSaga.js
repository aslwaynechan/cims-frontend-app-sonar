import { take, call, put } from 'redux-saga/effects';
import * as manageClinicalNoteTemplateActionType from '../../actions/clinicalNoteTemplate/manageClinicalNoteTemplateActionType';
import * as commonType from '../../actions/common/commonActionType';
import {API_requestFavouriteList, API_requestTemplateList, API_deleteTemplateData, API_recordTemplateData, API_addTemplateData, API_editTemplateData} from '../../../api/clinicalNote';
import axios from '../../../services/axiosInstance';

function* requestFavouriteList() {
  while (true) {
    let { params } = yield take(manageClinicalNoteTemplateActionType.REQUEST_DATA);
    try {
            {
                    // let data = yield call(API_requestFavouriteList);
                    let { data } = yield call(axios.get,'clinical-note/codeList/clinicalNoteTmplType',params);
                    if (data.respCode === 0) {
                        yield put({ type: manageClinicalNoteTemplateActionType.FILLING_DATA, fillingData: data});
                    } else {
                        yield put({
                            type: commonType.OPEN_ERROR_MESSAGE,
                            error: data.errMsg ? data.errMsg : 'Service error',
                            data: data.data
                        });
                    }
            }
    } catch (error) {
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.message ? error.message : 'Service error',
            data: error
        });
    }
  }
}


function* requestTemplateList() {
  while (true) {
    let { params,callback } = yield take(manageClinicalNoteTemplateActionType.TEMPLATE_DATA);
    try {
            {
                    // let data = yield call(API_requestTemplateList,params);
                    let {clinicalNoteTmplType} = params;
                    let {data} = yield call(axios.get,`clinical-note/clinicalNoteTemplate/${clinicalNoteTmplType}`,params);
                    if (data.respCode === 0) {
                        yield put({ type: manageClinicalNoteTemplateActionType.PUTTEMPLATELIST_DATA, fillingData: data});
                        callback&&callback(data.data);
                    } else {
                        yield put({
                            type: commonType.OPEN_ERROR_MESSAGE,
                            error: data.errMsg ? data.errMsg : 'Service error',
                            data: data.data
                        });
                    }
            }
    } catch (error) {
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.message ? error.message : 'Service error',
            data: error
        });
    }
  }
}



function* deleteTemplateData() {
  while (true) {
    let { params,callback } = yield take(manageClinicalNoteTemplateActionType.DELETETEMPLATE_DATA);
    try {
            {
                    // let data = yield call(API_deleteTemplateData,params);
                    let { data } = yield call(axios.delete,'clinical-note/clinicalNoteTemplate/',{data:params});
                    yield put({
                        type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
                    });
                    if (data.respCode === 0 ||data.respCode === 1) {
                        yield put({ type: manageClinicalNoteTemplateActionType.DELETETEMPLATE_RESULT, fillingData: data});
                        callback&&callback(data);
                    } else {
                        yield put({
                            type: commonType.OPEN_ERROR_MESSAGE,
                            error: data.errMsg ? data.errMsg : 'Service error',
                            data: data.data
                        });
                    }
            }
    } catch (error) {
        yield put({
            type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.message ? error.message : 'Service error',
            data: error
        });
    }
  }
}



function* recordTemplateData() {
  while (true) {
    let { params ,callback} = yield take(manageClinicalNoteTemplateActionType.RECORDLIST_DATA);
    try {
            {
                    // let data = yield call(API_recordTemplateData,params);
                    let { data } = yield call(axios.patch,'clinical-note/clinicalNoteTemplate/reorder',params);
                    yield put({
                        type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
                    });
                    if (data.respCode === 0) {
                        yield put({ type: manageClinicalNoteTemplateActionType.RECORDLIST_RESULT, fillingData: data});
                        callback&&callback(data);
                    } else {
                        if(data.msgCode!==undefined&&data.msgCode!==null){
                            callback&&callback(data);
                        }else{
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }

                    }
            }
    } catch (error) {
        yield put({
            type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.message ? error.message : 'Service error',
            data: error
        });
    }
  }
}
function* addTemplateData() {
  while (true) {
    let { params,callback} = yield take(manageClinicalNoteTemplateActionType.ADDTEMPLATE_DATA);
    try {
            {
                    // let data = yield call(API_addTemplateData,params);
                    let { data }  = yield call(axios.post,'clinical-note/clinicalNoteTemplate/',params);
                    yield put({
                        type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
                    });
                    if (data.respCode === 0) {
                        yield put({ type: manageClinicalNoteTemplateActionType.ADDTEMPLATE_RESULT, fillingData: data});
                        callback&&callback(data);
                    } else {
                        yield put({
                            type: commonType.OPEN_ERROR_MESSAGE,
                            error: data.errMsg ? data.errMsg : 'Service error',
                            data: data.data
                        });
                    }
            }
    } catch (error) {
        yield put({
            type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.message ? error.message : 'Service error',
            data: error
        });
    }
  }
}

function* editTemplateData() {
  while (true) {
    let { params ,callback} = yield take(manageClinicalNoteTemplateActionType.EDITTEMPLATE_DATA);
    try {
            {
                    // let data  = yield call(API_editTemplateData,params);
                    let { data } = yield call(axios.put,'clinical-note/clinicalNoteTemplate/',params);
                    yield put({
                        type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
                    });
                    if (data.respCode === 0) {
                        yield put({ type: manageClinicalNoteTemplateActionType.EDITTEMPLATE_RESULT, fillingData: data});
                        callback&&callback(data);
                    }
                    else {
                        yield put({
                            type: commonType.OPEN_ERROR_MESSAGE,
                            error: data.errMsg ? data.errMsg : 'Service error',
                            data: data.data
                        });
                    }
            }
    } catch (error) {
        yield put({
            type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.message ? error.message : 'Service error',
            data: error
        });
    }
  }
}



export const manageClinicalNoteTemplateSaga = [
    requestFavouriteList(),
    requestTemplateList(),
    deleteTemplateData(),
    recordTemplateData(),
    addTemplateData(),
    editTemplateData()
];