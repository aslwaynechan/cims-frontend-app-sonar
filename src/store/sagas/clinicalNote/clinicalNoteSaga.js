import { take, call, put } from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as clinicalNoteActionType from '../../actions/clinicalNote/clinicalNoteActionType';
import * as commonType from '../../actions/common/commonActionType';
import { API_requestRecordType, API_getClinicalNote, API_getMedicalRecordNotesList, API_getAmendmentHistoryList, API_getSysConfig
  , API_listClinicalNoteTemplates, API_saveClinicalNote, API_getCopyData, API_deleteTemplateData, API_saveRecordDetail,API_deleteRecordDetail
} from '../../../api/clinicalNote';

//get record type
function* requestRecordType() {
  while (true) {
    let {callback} = yield take(clinicalNoteActionType.GET_RECORDTYPE_DATA_LIST);
    try {
            {
                    // let data = yield call(API_requestRecordType);
                    let { data } = yield call(axios.get, 'clinical-note/codeList/ClinicalNoteType');
                    if (data.respCode === 0) {
                        if(data.data&&data.data.length){
                            data.data=data.data.map(item=>{
                                return {title:item.typeDesc,value:item.codeClinicalnoteTypeCd};
                            });
                        }
                        yield put({ type: clinicalNoteActionType.PUT_RECORDTYPE_DATA_LIST,fillingRecordType:data});
                        callback&&callback();
                    } else {
                        yield put({
                            type: commonType.OPEN_ERROR_MESSAGE,
                            error: data.errMsg ? data.errMsg : 'Service error',
                            data: data.data
                        });
                    }
            }
    } catch (error) {
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.message ? error.message : 'Service error',
            data: error
        });
    }
  }
}

//get template list
function* listClinicalNoteTemplates() {
  while (true) {
  let { templateParams,callback} = yield take(clinicalNoteActionType.GET_TEMPLATE_DATA_LIST);
    let params = templateParams;
    try {
            {
              let {curServiceCd, userLogName} = params;
                let { data } = yield call(axios.get, `clinical-note/clinicalNoteTemplate/${curServiceCd}/${userLogName}`);
                // let data = yield call(API_listClinicalNoteTemplates,params);
                if (data.respCode === 0) {
                    callback&&callback(data.data.myFavoriteList,data.data.serFavoriteList);
                } else {
                    yield put({
                        type: commonType.OPEN_ERROR_MESSAGE,
                        error: data.errMsg ? data.errMsg : 'Service error',
                        data: data.data
                    });
                }
            }
    } catch (error) {
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.message ? error.message : 'Service error',
            data: error
        });
    }
  }
}
//save record detail data
function* saveRecordDetail() {
  while (true) {
    let { params,callback} = yield take(clinicalNoteActionType.SAVE_RECORD_DETAIL_DATA);
    try {
            {
                    // let data = yield call(API_saveRecordDetail,params);
                    let {data} = yield call(axios.put, 'clinical-note/clinicalNote/',params);
                    yield put({
                        type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
                    });
                    // callback&&callback(data);
                    if (data.respCode === 0) {
                        callback&&callback(data);
                    }
                    else {
                        yield put({
                            type: commonType.OPEN_ERROR_MESSAGE,
                            error: data.errMsg ? data.errMsg : 'Service error',
                            data: data.data
                        });
                    }
            }
    } catch (error) {
        yield put({
            type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.message ? error.message : 'Service error',
            data: error
        });
    }
  }
}
//delete record detail data
function* deleteRecordDetail() {
  while (true) {
    let { params,callback} = yield take(clinicalNoteActionType.DELETE_RECORD_DETAIL_DATA);
    try {
            {
                    // let data = yield call(API_deleteRecordDetail,params);
                    let {data} = yield call(axios.delete,'clinical-note/clinicalNote/',{data:params});
                    yield put({
                        type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
                    });
                    if (data.respCode === 0) {
                        callback&&callback(data.msgCode);
                    } else {
                        yield put({
                            type: commonType.OPEN_ERROR_MESSAGE,
                            error: data.errMsg ? data.errMsg : 'Service error',
                            data: data.data
                        });
                    }
            }
    } catch (error) {
        yield put({
            type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.message ? error.message : 'Service error',
            data: error
        });
    }
  }
}

function* getSysConfig() {
  while (true) {
    yield take(clinicalNoteActionType.GET_SYS_CONFIG);
    try {
        // let data = yield call(API_getSysConfig);
        let { data } = yield call(axios.get, 'clinical-note/sysConfig/');
        if (data.respCode === 0) {
            let tempObj = {};
            data.data.forEach(element => {
                Object.assign(tempObj, { [element.key]: element });
            });
            yield put({
                type: clinicalNoteActionType.SYS_CONFIG,
                sysConfig: tempObj
            });
        } else {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: data.errMsg ? data.errMsg : 'Service error'
            });
        }
    } catch (error) {
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.errMsg ? error.errMsg : 'Service error'
        });
    }
  }
}

function* getAmendmentHistoryList() {
  while (true) {
    let {params} = yield take(clinicalNoteActionType.GET_AMENDMENT_HISTORY_LIST);
    try {
        // let data = yield call(API_getAmendmentHistoryList,params);
        let {doctorNoteId,serviceNoteId} = params;
        let { data } = yield call(axios.get, `clinical-note/clinicalNote/history/${doctorNoteId}/${serviceNoteId}`);
        if (data.respCode === 0) {
            yield put({
                type: clinicalNoteActionType.PUT_AMENDMENT_HISTORY_LIST,
                amendmentHistoryList: data.data
            });
        } else {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: data.errMsg ? data.errMsg : 'Service error'
            });
        }
    } catch (error) {
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.errMsg ? error.errMsg : 'Service error'
        });
    }
  }
}

function* getMedicalRecordNotesList() {
  while (true) {
    let {params,callback} = yield take(clinicalNoteActionType.GET_MEDICALRECORD_LIST);
    try {
        // let data = yield call(API_getMedicalRecordNotesList,params);
        console.log('callback======'+callback);
        let {encounterId,patientKey,recordType,selectedServiceCd,userRoleType} = params;
        let { data } = yield call(axios.get, `clinical-note/clinicalNote/medicalRecordNotes/${patientKey}/${encounterId}/${recordType}/${selectedServiceCd}/${userRoleType}`);
        callback&&callback(data);
    } catch (error) {
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.errMsg ? error.errMsg : 'Service error'
        });
    }
  }
}

function* getClinicalNote() {
  while (true) {
    let {params,callback} = yield take(clinicalNoteActionType.GET_CLINICALNOTE);
    try {
        // let data = yield call(API_getClinicalNote,params);
        let {patientKey,encounterId,userRoleType} = params;
        let {data} = yield call(axios.get, `clinical-note/clinicalNote/${patientKey}/${encounterId}/${userRoleType}`);
        callback&&callback(data);
    } catch (error) {
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.errMsg ? error.errMsg : 'Service error'
        });
    }
  }
}

function* saveClinicalNote() {
  while (true) {
    let {params,callback} = yield take(clinicalNoteActionType.SAVE_CLINICALNOTE);
    try {
        // let data = yield call(API_saveClinicalNote,params);
        let {data} = yield call(axios.post, 'clinical-note/clinicalNote/',params);
        callback&&callback(data);
    } catch (error) {
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.errMsg ? error.errMsg : 'Service error'
        });
    }
  }
}

//get copy data
function* getCopyData() {
  while (true) {
    let { params,callback } = yield take(clinicalNoteActionType.GET_COPY_DATA);
    try {
          {
            // let data = yield call(API_getCopyData,params);
            let {copyType, patientKey, encounterId} = params;
            let {data} = yield call(axios.get, `clinical-note/clinicalNote/previousDataCopies/${copyType}/${patientKey}/${encounterId}`);
            if (data.respCode === 0) {
                callback&&callback(data.data);
            } else {
              yield put({
                  type: commonType.OPEN_ERROR_MESSAGE,
                  error: data.errMsg ? data.errMsg : 'Service error',
                  data: data.data
              });
            }
          }
    } catch (error) {
      yield put({
        type: commonType.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

export const clinicalNoteSaga = [
    requestRecordType(),
    listClinicalNoteTemplates(),
    saveRecordDetail(),
    deleteRecordDetail(),
    getSysConfig(),
    getAmendmentHistoryList(),
    getMedicalRecordNotesList(),
    getClinicalNote(),
    saveClinicalNote(),
    getCopyData()
];
