import { takeLatest, call, put } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as type from '../../../actions/certificate/sickLeave/sickLeaveActionType';
import * as commonType from '../../../actions/common/commonActionType';
import {print} from '../../../../utilities/printUtilities';
import * as messageType from '../../../actions/message/messageActionType';

function* fetchSickLeaveCert(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/getLeaveCertificate', action.params);
        if (data.respCode === 0) {
            // console.log('base 64',data.data,action);
            // yield put({ type: commonType.PRINT_START, params: { base64: data.data, callback: action.callback, copies: action.copies } });
            print({base64: data.data, callback: action.callback, copies: action.copies});
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
            yield put({
                type: type.UPDATE_FIELD,
                updateData: { handlingPrint: false }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        yield put({
            type: type.UPDATE_FIELD,
            updateData: { handlingPrint: false }
        });
    }
}

function* getSickLeaveCert() {
    yield takeLatest(type.GET_SICK_LEAVE_CERT, fetchSickLeaveCert);
}

export const sickLeaveSaga = [
    getSickLeaveCert()
];