import { takeLatest, take, call, put } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as type from '../../../actions/certificate/attendanceCertificate/attendanceCertActionType';
import * as commonType from '../../../actions/common/commonActionType';
import {print} from '../../../../utilities/printUtilities';
import * as messageType from '../../../actions/message/messageActionType';


function* fetchGetAttendanceCert(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/getAttendanceCertificate', action.params);
        if (data.respCode === 0) {
            // console.log('base 64', data.data, action);
            // yield put({ type: commonType.PRINT_START, params: { base64: data.data, copies: action.copies, callback: action.callback } });
            print({ base64: data.data, copies: action.copies, callback: action.callback });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
            yield put({
                type: type.UPDATE_FIELD,
                updateData: { handlingPrint: false }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        yield put({
            type: type.UPDATE_FIELD,
            updateData: { handlingPrint: false }
        });
    }
}

function* getAttendanceCert() {
    yield takeLatest(type.GET_ATTENDANCE_CERT, fetchGetAttendanceCert);
}

function* listAttendanceCertificates() {
    while (true) {
        try {
            let { params } = yield take(type.LIST_ATTENDANCE_CERTIFICATES);
            let { data } = yield call(axios.post, '/appointment/listAttendanceCertificates', params);
            if (data.respCode === 0) {
                yield put({
                    type: type.PUT_LIST_ATTENDANCE_CERTIFICATES,
                    data: data.data
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* fetchUpdateAttendanceCertificate(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/updateAttendanceCertificate', action.params);
        if (data.respCode === 0) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110602'
                }
            });
            action.callback && action.callback();
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* updateAttendanceCertificate() {
    yield takeLatest(type.UPDATE_ATTENDANCE_CERTIFICATE, fetchUpdateAttendanceCertificate);
}

export const attendanceCertSaga = [
    getAttendanceCert(),
    listAttendanceCertificates(),
    updateAttendanceCertificate()
];