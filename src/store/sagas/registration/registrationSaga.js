import { take, call, put, takeEvery } from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as RegistrationType from '../../actions/registration/registrationActionType';
import * as commonType from '../../actions/common/commonActionType';
import ButtonStatusEnum from '../../../enums/registration/buttonStatusEnum';
import * as messageType from '../../actions/message/messageActionType';

function* updatePatient() {
    while (true) {
        let { params, callback } = yield take(RegistrationType.UPDATE_PATIENT);
        try {
            let { data } = yield call(axios.post, '/patient/updatePatient', params);
            if (data.respCode === 0) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110102',
                        showSnackbar: true
                    }
                });
                callback && callback();
                yield put({ type: RegistrationType.GET_PATINET_BY_ID, patientKey: data.data });
                yield put({
                    type: RegistrationType.UPDATE_PATIENT_OPERATE_STATUS,
                    status: ButtonStatusEnum.DATA_SELECTED
                });
                yield put({ type: RegistrationType.UPDATE_STATE, updateData: { isOpenReview: true, isSaveSuccess: true } });
            } else if (data.respCode === 1) {
                //todo parameterException
            } else if (data.respCode === 3) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110032'
                    }
                });
            } else if (data.respCode === 101) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110127'
                    }
                });
            } else if (data.respCode === 103) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110128'
                    }
                });
            } else if (data.respCode === 100) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110129'
                    }
                });
            } else if (data.respCode === 104) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110130'
                    }
                });
            } else if (data.respCode === 102) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110138'
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        } finally {
            yield put({ type: commonType.CLOSE_COMMON_CIRCULAR });
        }
    }
}

function* savePatient() {
    while (true) {
        let { params, callback } = yield take(RegistrationType.REGISTER_PATIENT);
        try {
            let { data } = yield call(axios.post, '/patient/insertPatient', params);
            if (data.respCode === 0) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110103',
                        showSnackbar: true
                    }
                });
                callback && callback();
                yield put({ type: RegistrationType.GET_PATINET_BY_ID, patientKey: data.data });
                yield put({ type: RegistrationType.UPDATE_PATIENT_OPERATE_STATUS, status: ButtonStatusEnum.DATA_SELECTED });
                yield put({ type: RegistrationType.UPDATE_STATE, updateData: { isOpenReview: true, isSaveSuccess: true } });
            } else if (data.respCode === 1) {
                //todo parameterException
            } else if (data.respCode === 3) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110032'
                    }
                });
            } else if (data.respCode === 100) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110127'
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        } finally {
            yield put({ type: commonType.CLOSE_COMMON_CIRCULAR });
        }
    }
}

function* getPatientById() {
    while (true) {
        let { patientKey } = yield take(RegistrationType.GET_PATINET_BY_ID);
        try {
            let { data } = yield call(axios.post, '/patient/getPatient', { 'patientKey': patientKey });
            if (data.respCode === 0) {
                yield put({ type: RegistrationType.PUT_PATIENT_BY_ID, data: data.data });
            } else if (data.respCode === 100) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110130'
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* searchPatient() {
    while (true) {
        let { searchString, pageNum, pageSize } = yield take(RegistrationType.SEARCH_PATIENT);
        try {
            let { data } = yield call(axios.post, '/patient/searchPatient', { 'searchString': searchString, 'pageNum': pageNum, 'pageSize': pageSize });
            if (data.respCode === 0) {
                const patientList = data.data;
                let buttonStatus = ButtonStatusEnum.VIEW;
                if (patientList && patientList.patientSearchDtos && patientList.patientSearchDtos.length === 1 && !pageNum) {
                    yield put({ type: RegistrationType.GET_PATINET_BY_ID, patientKey: patientList.patientSearchDtos[0].patientKey || '' });
                    buttonStatus = ButtonStatusEnum.DATA_SELECTED;
                } else if (patientList && patientList.patientSearchDtos && patientList.patientSearchDtos.length >= 1) {
                    buttonStatus = ButtonStatusEnum.SEARCH;
                } else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110111'
                        }
                    });
                    buttonStatus = ButtonStatusEnum.ADD;
                }
                yield put({ type: RegistrationType.PUT_PATIENT, data: patientList, status: buttonStatus });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}
function* findCharByCcCode() {
    yield takeEvery(RegistrationType.FIND_CHAR_BY_CC_CODE, function* (action) {
        let { ccCode, charIndex } = action;
        try {
            let { data } = yield call(axios.get, `/common/findCharByccCd?ccCode=${ccCode}`);
            if (data.respCode === 0) {
                yield put({ type: RegistrationType.UPDATE_CHI_CHAR, charIndex: charIndex, char: data.data });
            } else if (data.respCode === 100) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110131'
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    });
}

export const registrationSaga = [
    updatePatient(),
    savePatient(),
    getPatientById(),
    searchPatient(),
    findCharByCcCode()
];
