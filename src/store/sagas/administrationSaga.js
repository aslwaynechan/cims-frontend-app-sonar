import { take, takeLatest, call, put } from 'redux-saga/effects';
import axios from '../../services/axiosInstance';
import * as administrationType from '../actions/administration/administrationActionType';
import * as messageType from '../actions/message/messageActionType';

function* fetchSearchUser(action) {
    try {
        yield put({ type: administrationType.OPEN_SEARCH });
        let { data } = yield call(axios.post, '/user/searchUser', action.params, { headers: { 'Authorization': 'Bearer ' + window.sessionStorage.getItem('token') } });
        if (data.respCode === 0) {
            yield put({ type: administrationType.PUT_USER_LIST, data: data.data });
            yield put({ type: administrationType.CLOSE_SEARCH });
        } else {
            yield put({ type: administrationType.PUT_USER_LIST, data: [] });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        console.log(error);
    }
}

function* searchUser() {
    yield takeLatest(administrationType.SEARCH_USER, fetchSearchUser);
}

function* getUser() {
    while (true) {
        let { params } = yield take(administrationType.GET_USER_BY_ID);
        try {
            let { data } = yield call(axios.post, '/user/getUser', params, { headers: { 'Authorization': 'Bearer ' + window.sessionStorage.getItem('token') } });
            if (data.respCode === 0) {
                yield put({ type: administrationType.PUT_USER_DATA, data: data.data });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            console.log(error);
        }
    }
}

function* fetchInsertUser(action) {
    try {
        let { data } = yield call(axios.post, '/user/insertUser', action.params, { headers: { 'Authorization': 'Bearer ' + window.sessionStorage.getItem('token') } });
        if (data.respCode === 0) {
            yield put({ type: administrationType.PUT_SAVE_USER_PROFILE_SUCCESS });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110021',
                    btnActions: {
                        btn1Click: typeof action.callback === 'function' ? () => { action.callback(true); } : null
                    }
                }
            });

            let params = { id: data.data };
            yield put({ type: administrationType.GET_USER_BY_ID, params });
        } else if (data.respCode === 1) {
            //todo parameterException
            typeof action.callback === 'function' ? action.callback(false) : null;

        } else if (data.respCode === 3) {
            //Submission failed
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110032',
                    btnActions: {
                        btn1Click: typeof action.callback === 'function' ? () => { action.callback(false); } : null
                    }
                }
            });
        } else if (data.respCode === 100) {
            //do something
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110312',
                    params: [{ name: 'USER_INFO', value: 'Login Name' }],
                    btnActions: {
                        btn1Click: typeof action.callback === 'function' ? () => { action.callback(false); } : null
                    }
                }
            });
        }
        else if (data.respCode === 101) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110312',
                    params: [{ name: 'USER_INFO', value: 'Email' }],
                    btnActions: {
                        btn1Click: typeof action.callback === 'function' ? () => { action.callback(false); } : null
                    }
                }
            });
        }
        else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031',
                    btnActions: {
                        btn1Click: typeof action.callback === 'function' ? () => { action.callback(false); } : null
                    }
                }
            });
        }
    } catch (error) {
        console.log(error);
        typeof action.callback === 'function' ? action.callback(false) : null;
    }
}

function* insertUser() {
    yield takeLatest(administrationType.INSERT_USER_PROFILE, fetchInsertUser);
}

function* fetchUpdateUser(action) {
    try {
        let { data } = yield call(axios.post, '/user/updateUser', action.params, { headers: { 'Authorization': 'Bearer ' + window.sessionStorage.getItem('token') } });
        if (data.respCode === 0) {
            yield put({ type: administrationType.PUT_SAVE_USER_PROFILE_SUCCESS });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110023',
                    btnActions: {
                        btn1Click: typeof action.callback === 'function' ? () => { action.callback(true); } : null
                    }
                }
            });

            let params = { id: data.data };
            yield put({ type: administrationType.GET_USER_BY_ID, params });
        } else if (data.respCode === 1) {
            //todo parameterException
            typeof action.callback === 'function' ? action.callback(false) : null;

        } else if (data.respCode === 3) {
            //Submission failed
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110032',
                    btnActions: {
                        btn1Click: typeof action.callback === 'function' ? () => { action.callback(false); } : null
                    }
                }
            });
        } else if (data.respCode === 100) {
            //do something
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110312',
                    params: [{ name: 'USER_INFO', value: 'Login Name' }],
                    btnActions: {
                        btn1Click: typeof action.callback === 'function' ? () => { action.callback(false); } : null
                    }
                }
            });
        }
        else if (data.respCode === 101) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110312',
                    params: [{ name: 'USER_INFO', value: 'Email' }],
                    btnActions: {
                        btn1Click: typeof action.callback === 'function' ? () => { action.callback(false); } : null
                    }
                }
            });
        } else if (data.respCode === 102) {
            //Records not exist!
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110314',
                    btnActions: {
                        btn1Click: typeof action.callback === 'function' ? () => { action.callback(false); } : null
                    }
                }

            });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031',
                    btnActions: {
                        btn1Click: typeof action.callback === 'function' ? () => { action.callback(false); } : null
                    }
                }

            });
        }
    } catch (error) {
        console.log(error);
        typeof action.callback === 'function' ? action.callback(false) : null;
    }
}

function* updateUser() {
    yield takeLatest(administrationType.UPDATE_USER_PROFILE, fetchUpdateUser);
}

function* getCodeList() {
    while (true) {
        let { params } = yield take(administrationType.GET_CODE_LIST);
        try {
            let { data } = yield call(axios.post, '/common/listCodeList', params, { headers: { 'Authorization': 'Bearer ' + window.sessionStorage.getItem('token') } });
            if (data.respCode === 0) {
                yield put({
                    type: administrationType.PUT_CODE_LIST,
                    codeList: data.data
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            console.log(error);
        }
    }
}

export const administrationSaga = [
    searchUser(),
    getUser(),
    insertUser(),
    updateUser(),
    getCodeList()
];