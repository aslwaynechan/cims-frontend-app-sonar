import { call, put, take, all } from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as type from '../../actions/patient/patientActionType';
import * as commonType from '../../actions/common/commonActionType';
import * as mainFrameActionType from '../../actions/mainFrame/mainFrameActionType';
import * as messageType from '../../actions/message/messageActionType';
import * as PatientUtil from '../../../utilities/patientUtilities';
import accessRightEnum from '../../../enums/accessRightEnum';
import { openCommonMessage } from '../../actions/message/messageAction';

function* getPatientById() {
    while (true) {
        let { patientKey, appointmentId, caseNo, callBack } = yield take(type.GET_PATINET_BY_ID);
        try {
            let { data } = yield call(axios.post, '/patient/getPatient', { 'patientKey': patientKey });
            if (data.respCode === 0) {
                let appointmentInfo = null, caseNoInfo = null;
                //update patient appointment
                if (appointmentId) {
                    let appt = yield call(axios.get, `/appointment/getAppointment/${appointmentId}`);
                    let apptData = appt && appt.data;
                    if (apptData.respCode === 0) {
                        appointmentInfo = apptData.data;
                    } else {
                        yield put({
                            type: messageType.OPEN_COMMON_MESSAGE,
                            payload: {
                                msgCode: '110031'
                            }
                        });
                    }
                }

                //update patient caseNo.
                if (caseNo) {
                    const caseList = data.data.caseList || [];
                    caseNoInfo = caseList.find(item => item.caseNo === caseNo && item.statusCd === 'A');
                }

                let patient = PatientUtil.transferPatientDocumentPair(data.data);
                //update patient
                yield put({ type: type.PUT_PATIENT_INFO, data: patient, appointmentInfo, caseNoInfo });
                //call back
                callBack && callBack(patient);
            } else if (data.respCode === 100) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110130'
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getCodeList() {
    while (true) {
        let { params, putCode } = yield take(type.GET_CODE_LIST);
        try {
            let { data } = yield call(axios.post, '/common/listCodeList', params);
            if (data.respCode === 0) {
                if (Array.isArray(putCode)) {
                    for (let code of putCode) {
                        yield put({
                            type: code,
                            codeList: data.data
                        });
                    }
                } else {
                    yield put({
                        type: putCode,
                        codeList: data.data
                    });
                }
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}


function* listNationalityAndListCountry() {
    while (true) {
        yield take(type.LIST_NATIONALITY_AND_LIST_COUNTRY);
        try {
            const [nationalityCall, countryCall] = yield all([
                // call(axios.get, '/patient/listNationality'),
                // call(axios.get, '/patient/listCountry')
                call(axios.get, '/common/listNationality'),
                call(axios.get, '/common/listCountry')
            ]);
            const nationality = nationalityCall.data;
            const country = countryCall.data;
            if (nationality.respCode === 0 && country.respCode === 0) {
                yield put({
                    type: type.LOAD_NATIONALITY_LIST_AND_COUNTRY_LIST,
                    countryList: country.data,
                    nationalityList: nationality.data
                });
            } else if (nationality.respCode !== 0) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        }
        catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}


function* listPassport() {
    while (true) {
        try {
            yield take(type.LIST_PASSPORT);
            let { data } = yield call(axios.get, '/patient/listPassport');
            if (data.respCode === 0) {
                yield put({
                    type: type.LOAD_PASSPORT_LIST,
                    list: data.data
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        }
        catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getLanguageList() {
    while (true) {
        try {
            yield take(type.GET_LANGUAGE_LIST);
            let { data } = yield call(axios.get, '/common/listPreferredLanguage');
            if (data.respCode === 0) {
                yield put({
                    type: type.PUT_LANGUAGE_LIST,
                    languageData: data.data
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        }
        catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getPatientAppointment() {
    while (true) {
        try {
            const { appointmentId, caseList } = yield take(type.GET_PATIENT_APPOINTMENT);
            let { data } = yield call(axios.get, `/appointment/getAppointment/${appointmentId}`);
            if (data.respCode === 0) {
                yield put({ type: type.UPDATE_PATIENT_APPOINTMENT, appointmentInfo: data.data });
                if (caseList) {
                    let caseDto = caseList.find(item => item.caseNo === data.data.caseNo);
                    yield put({ type: type.UPDATE_PATIENT_CASENO, caseNoInfo: caseDto });
                }
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getPatientCaseNo() {
    while (true) {
        try {
            const { caseList, caseNo } = yield take(type.GET_PATIENT_CASENO);
            if (caseList) {
                let caseDto = caseList.find(item => item.caseNo === caseNo);
                yield put({ type: type.UPDATE_PATIENT_CASENO, caseNoInfo: caseDto });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getEHRUrl() {
    while (true) {
        try {
            let { patientData, isEHRAccessRight, callBack } = yield take(type.GET_EHR_URL);
            if (isEHRAccessRight) {
                    // '/ehris/viewer/patientUrl',
                    // 'https://cims-interface-ehr-viewer-svc-cims-dh-dhp-dev-2.cldpaast71.server.ha.org.hk/viewer/patientUrl',
                    let { data } = yield call(
                        axios.post,
                        'https://cims-interface-ehr-viewer-svc-cims-dh-dhp-dev-2.cldpaast71.server.ha.org.hk/viewer/patientUrl',
                        patientData
                    );
                if (data.code === 0) {
                    if (data.data.url) {
                        yield put({
                            type: type.PUT_EHR_URL,
                            data: data,
                            eHRUrl: data.data.url
                        });
                        callBack && callBack(data);
                    } else {
                        // Invalid JSON Format or API data error
                        yield put({ type: mainFrameActionType.DELETE_SUB_TABS, params: accessRightEnum.eHRRegistered });
                        yield put(openCommonMessage({
                            msgCode: '130102',
                            params: [{ name: 'statusCode', value: data.data.status}, { name: 'statusDesc', value: data.data.statusDescription}]
                        }));
                    }
                } else {
                    // data.code
                    yield put({ type: mainFrameActionType.DELETE_SUB_TABS, params: accessRightEnum.eHRRegistered });
                    yield put(openCommonMessage({
                        msgCode: '130103',
                        params: [{ name: 'dataCode', value: data.code ? data.code : 'Unknown Code'}]
                    }));
                }
            } else {
                // EHRAccess Right Error
                yield put({ type: mainFrameActionType.DELETE_SUB_TABS, params: accessRightEnum.eHRRegistered });
                yield put(openCommonMessage({msgCode: '130101'}));
            }
        } catch (error) {
            // eHR API error
            yield put(openCommonMessage({msgCode: '130100'}));
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
            yield put({ type: mainFrameActionType.DELETE_SUB_TABS, params: accessRightEnum.eHRRegistered });
        }
    }
}

function* getPatientEncounter() {
    while (true) {
        try {
            const { appointmentId } = yield take(type.GET_PATIENT_ENCOUNTER);
            let { data } = yield call(axios.post, '/appointment/listEncounterByAppointmentIds', [appointmentId]);
            if (data.respCode === 0) {
                if (data.data && data.data.length > 0) {
                    yield put({ type: type.LOAD_PATIENT_ENCOUNTER_INFO, encounterInfo: data.data.find(item => item.statusCd === 'N') });
                }
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

export const patientSaga = [
    getPatientById(),
    getCodeList(),
    listNationalityAndListCountry(),
    listPassport(),
    getLanguageList(),
    getPatientAppointment(),
    getPatientCaseNo(),
    getEHRUrl(),
    getPatientEncounter()
];