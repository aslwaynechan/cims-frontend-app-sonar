import { take, takeLatest, put, call } from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as patientSpecFuncActionType from '../../actions/patient/patientSpecFunc/patientSpecFuncActionType';
import * as commonType from '../../actions/common/commonActionType';
import * as messageType from '../../actions/message/messageActionType';
import * as PatientUtil from '../../../utilities/patientUtilities';
import _ from 'lodash';

function* getPatientList() {
    while (true) {
        let { params } = yield take(patientSpecFuncActionType.GET_PATIENT_LIST);
        try {
            let { data } = yield call(axios.post, '/appointment/listPatientQueue', params);
            if (data.respCode === 0) {
                let patientQueueList = data.data;
                let patientQueueDtos = patientQueueList.patientQueueDtos;
                for (let i = 0; i < patientQueueDtos.length; i++) {
                    if(patientQueueDtos[i].patientDto){
                        patientQueueDtos[i].patientDto = PatientUtil.transferPatientDocumentPair(_.cloneDeep(patientQueueDtos[i].patientDto));
                    }
                }
                yield put({
                    type: patientSpecFuncActionType.UPDATE_PATIENT_LIST_FIELD,
                    fields: {
                        patientQueueList: patientQueueList
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* fetchSeachPatientList(action) {
    try {
        let { data } = yield call(axios.post, '/patient/searchPatient', action.params);
        if (data.respCode === 0) {
            yield put({ type: patientSpecFuncActionType.PUT_SEARCH_PATIENT_LIST, data: data.data });
        } else {
            yield put({ type: patientSpecFuncActionType.PUT_SEARCH_PATIENT_LIST, data: null });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* seachPatientList() {
    yield takeLatest(patientSpecFuncActionType.SEARCH_PATIENT_LIST, fetchSeachPatientList);
}

function* fetchSeachPatientPrecisely(action) {
    try {
        let { data } = yield call(axios.post, '/patient/searchPatientPrecisely', action.params);
        if (data.respCode === 0) {
            yield put({ type: patientSpecFuncActionType.PUT_PATIENT_PRECISELY, data: data.data });
        } else {
            yield put({ type: patientSpecFuncActionType.PUT_PATIENT_PRECISELY, data: [] });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* seachPatientPrecisely() {
    yield takeLatest(patientSpecFuncActionType.SEARCH_PATIENT_PRECISELY, fetchSeachPatientPrecisely);
}

function* fetchConfirmAnonymousPatient(action) {
    try {
        let { data } = yield call(axios.post, '/patient/confirmAnonymousPatient', action.params);
        if (data.respCode === 0) {
            yield put({ type: patientSpecFuncActionType.PUT_CONFIRM_ANONYMOUS_PATIENT, data: data.data, status: 'success' });
        } else if (data.respCode === 100) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110279'
                }
            });
        } else {
            yield put({ type: patientSpecFuncActionType.PUT_CONFIRM_ANONYMOUS_PATIENT, data: null, status: 'fail' });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* confirmAnonymousPatient() {
    yield takeLatest(patientSpecFuncActionType.CONFIRM_ANONYMOUS_PATIENT, fetchConfirmAnonymousPatient);
}

function* resetAttendance() {
    while (true) {
        try {
            let { attenPara, searchPara } = yield take(patientSpecFuncActionType.RESET_ATTENDANCE);
            let { data } = yield call(axios.post, '/appointment/resetAttendance', attenPara);
            if (data.respCode === 0) {
                yield put({
                    type: patientSpecFuncActionType.GET_PATIENT_LIST,
                    params: searchPara
                });
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110284'
                    }
                });
            } else if (data.respCode === 1) {
                //todo parameterException
            } else if (data.respCode === 101) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110289'
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}


function* fetchSearchInPatientQueue(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/listPatientQueue', action.params);
        if (data.respCode === 0) {
            const patientQueueList = data.data;
            if (!patientQueueList) {
                throw new Error('Service error');
            }
            let patientQueueDtos = patientQueueList.patientQueueDtos;
            for (let i = 0; i < patientQueueDtos.length; i++) {
                if (patientQueueDtos[i].patientDto) {
                    patientQueueDtos[i].patientDto = PatientUtil.transferPatientDocumentPair(_.cloneDeep(patientQueueDtos[i].patientDto));
                }
            }
            if (patientQueueList.nextActionPage === 'searchPatient') {
                let listData = yield call(axios.post, '/patient/searchPatient', { searchString: action.params.searchStr });
                if (listData.data.respCode === 0) {
                    yield put({ type: patientSpecFuncActionType.PUT_SEARCH_PATIENT_LIST, data: listData.data.data });
                    yield put({ type: patientSpecFuncActionType.PUT_SEARCH_IN_PATIENT_QUEUE, data: patientQueueList });
                } else {
                    yield put({ type: patientSpecFuncActionType.PUT_SEARCH_PATIENT_LIST, data: null });
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
            } else {
                yield put({ type: patientSpecFuncActionType.PUT_SEARCH_IN_PATIENT_QUEUE, data: patientQueueList });
            }
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* searchInPatientQueue() {
    yield takeLatest(patientSpecFuncActionType.SEARCH_IN_PATIENT_QUEUE, fetchSearchInPatientQueue);
}

export const patientSpecFuncSaga = [
    getPatientList(),
    seachPatientList(),
    seachPatientPrecisely(),
    confirmAnonymousPatient(),
    resetAttendance(),
    searchInPatientQueue()
];