import { take, call, put } from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as enquiryActionType from '../../actions/IOE/enquiry/enquiryActionType';
import * as commonType from '../../actions/common/commonActionType';
// for temporary test
//get service list
function* requestClinic() {
    while (true) {
        let { params, callback } = yield take(
            enquiryActionType.REQUEST_ENQUIRY_CLINIC
        );
        try {
            {
                let { data } = yield call(
                    axios.post,
                    '/common/listClinicList',
                    params
                );
                if (data.respCode === 0) {
                    yield put({
                        type: enquiryActionType.FILLING_ENQUIRY_CLINIC,
                        fillingData: data
                    });
                    callback && callback(data);
                } else {
                    yield put({
                        type: commonType.OPEN_ERROR_MESSAGE,
                        error: data.errMsg ? data.errMsg : 'Service error',
                        data: data.data
                    });
                }
            }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* getForms() {
    let { params, callback } = yield take(enquiryActionType.getForms);
    try {
        let { data } = yield call(axios.post, '/ioe/listIoeFormName', params);
        if (data.respCode === 0 && data.data) {
            let forms = data.data
                .filter(item => item.formName !== '---All---')
                .map(item => {
                    return { value: item.codeIoeFormId, title: item.formName };
                });
            yield put({ type: enquiryActionType.setState, payload: { forms } });
            callback && callback(forms);
        } else {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: data.errMsg ? data.errMsg : 'Service error',
                data: data.data
            });
        }
    } catch (error) {
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.message ? error.message : 'Service error',
            data: error
        });
    }
}

function* getPatients() {
    let { params, callback } = yield take(enquiryActionType.getPatients);
    try {
        let { data } = yield call(
            axios.post,
            '/ioe/listPatientByLoginClinicCd',
            params
        );
        if (data.respCode === 0 && data.data) {
            let patients = data.data.map(item => {
                return { value: item.patientKey, title: item.fullName };
            });
            yield put({
                type: enquiryActionType.setState,
                payload: { patients }
            });
            callback && callback(patients);
        } else {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: data.errMsg ? data.errMsg : 'Service error',
                data: data.data
            });
        }
    } catch (error) {
        yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: error.message ? error.message : 'Service error',
            data: error
        });
    }
}

function* getServices() {
    while (true) {
        let { params, callback } = yield take(enquiryActionType.getServices);
        try {
            let { data } = yield call(
                axios.post,
                '/common/listService',
                params
            );
            if (data.respCode === 0 && data.data) {
                let defaultService = [{value:'ALL' , title: 'ALL'}];
                let services = data.data.map(item => {
                    return { value: item.serviceCd, title: item.serviceName };
                });
                services = defaultService.concat(services);
                yield put({
                    type: enquiryActionType.setState,
                    payload: { services}
                });
                callback && callback(data.data);
            } else {
                yield put({
                    type: commonType.OPEN_ERROR_MESSAGE,
                    error: data.errMsg ? data.errMsg : 'Service error',
                    data: data.data
                });
            }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* getClinics() {
    while (true) {
        let { params, callback } = yield take(enquiryActionType.getClinics);
        try {
            {
                let { data } = yield call(
                    axios.post,
                    '/common/listClinic',
                    params
                );
                if (data.respCode === 0 && data.data) {
                    data.data.unshift({clinicCd: ' ', clinicName: '---Please Select---'});
                    let clinics = data.data.map(item => {
                        return { value: item.clinicCd, title: item.clinicName };
                    });
                    yield put({
                        type: enquiryActionType.setState,
                        payload: { clinics }
                    });
                    callback && callback(clinics);
                } else {
                    yield put({
                        type: commonType.OPEN_ERROR_MESSAGE,
                        error: data.errMsg ? data.errMsg : 'Service error',
                        data: data.data
                    });
                }
            }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* getHistoryList(){
    while (true) {
        let { params, callback } = yield take(enquiryActionType.getHistoryList);
        try {
            let { data } = yield call(
                axios.post,
                '/ioe/listLaboratoryRequest',
                params
            );
            if (data.respCode === 0 && data.data) {
                let historyList = data.data.map(item => {
                    return item;
                });
                yield put({
                    type: enquiryActionType.setState,
                    payload: { historyList }
                });
                callback && callback(historyList);
            } else {
                yield put({
                    type: commonType.OPEN_ERROR_MESSAGE,
                    error: data.errMsg ? data.errMsg : 'Service error',
                    data: data.data
                });
            }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* getRequestedByList() {
    while (true) {
        let { params,callback } = yield take(enquiryActionType.UPDATE_IOE_LABORATORY_FOLLOWUP_STATUS);
        try {
                let { data } = yield call(axios.post, '/user/searchUser',params);
                console.log('getRequestedByList data====%o',data);
                if (data.respCode === 0) {
                    let requestedByList = data.data;
                    yield put({
                        type: enquiryActionType.setState,
                        payload: { requestedByList }
                    });
                    callback&&callback(data);
                } else {
                    yield put({
                        type: commonType.OPEN_ERROR_MESSAGE,
                        error: data.errMsg ? data.errMsg : 'Service error',
                        data: data.data
                    });
                }
            } catch (error) {
                yield put({
                    type: commonType.OPEN_ERROR_MESSAGE,
                    error: error.message ? error.message : 'Service error',
                    data: error
                });
            }
    }
}

function* getOrderDetails(){
  while (true) {
      let { params, callback } = yield take(enquiryActionType.REQUEST_ORDER_DETAILS);
      try {
          let { data } = yield call(
              axios.post,
              '/ioe/getLaboratoryRequestDetailById',
              params
          );
          if (data.respCode === 0 && data.data) {
              callback && callback(data.data);
          } else {
              yield put({
                  type: commonType.OPEN_ERROR_MESSAGE,
                  error: data.errMsg ? data.errMsg : 'Service error',
                  data: data.data
              });
          }
      } catch (error) {
          yield put({
              type: commonType.OPEN_ERROR_MESSAGE,
              error: error.message ? error.message : 'Service error',
              data: error
          });
      }
  }
}

export const enquirySaga = [
    requestClinic(),
    getForms(),
    getPatients(),
    getServices(),
    getClinics(),
    getHistoryList(),
    getRequestedByList(),
    getOrderDetails()
];
