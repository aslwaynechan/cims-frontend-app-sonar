import { takeLatest, call, put } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as type from '../../../actions/appointment/apptEnquiry/apptEnquiryActionType';
import * as messageType from '../../../actions/message/messageActionType';
import * as commonType from '../../../actions/common/commonActionType';
import * as PatientUtil from '../../../../utilities/patientUtilities';
import _ from 'lodash';
import { print } from '../../../../utilities/printUtilities';

function* fetchEnquiryResult(action) {
    try {
        let { param } = action;
        let url = '/appointment/appointmentEnquiry';
        let { data } = yield call(axios.post, url, param);

        if (data.respCode === 0) {
            let enquiryResult = data.data;
            for (let i = 0; i < enquiryResult.length; i++) {
                if (enquiryResult[i].patientDto) {
                    enquiryResult[i].patientDto = PatientUtil.transferPatientDocumentPair(_.cloneDeep(enquiryResult[i].patientDto));
                }
            }
            yield put({ type: type.LOAD_ENQUIRY_RESULT, result: enquiryResult });
        } else if (data.respCode === 100) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110320'
                }
            });
        }
        else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    }
    catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* getEnquiryResult() {
    yield takeLatest(type.FETCH_ENQUIRY_RESULT, fetchEnquiryResult);
}

function* printApptReport(action) {
    try {
        let { param } = action;
        let url = '/appointment/getAppointmentsReport';
        let { data } = yield call(axios.post, url, param);
        //let { data } = yield call(axios.post, '/appointment/getYellowFeverExemptionLetter', action.params);
        if (data.respCode === 0) {
            // console.log('base 64',data.data,action);
            // yield put({ type: commonType.PRINT_START, params: { base64: data.data, callback: action.callback, copies: action.copies } });
            print({ base64: data.data, callback: action.callback });
            yield put({ type: commonType.CLOSE_COMMON_CIRCULAR });
        } else if (data.respCode === 100) {
            yield put({ type: commonType.CLOSE_COMMON_CIRCULAR });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110319'
                }
            });
        } else if (data.respCode === 101) {
            yield put({ type: commonType.CLOSE_COMMON_CIRCULAR });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110320'
                }
            });
        } else {
            yield put({ type: commonType.CLOSE_COMMON_CIRCULAR });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
            // yield put({
            //     type: type.UPDATE_FIELD,
            //     updateData: { handlingPrint: false }
            // });
        }
    } catch (error) {
        yield put({ type: commonType.CLOSE_COMMON_CIRCULAR });
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        // yield put({
        //     type: type.UPDATE_FIELD,
        //     updateData: { handlingPrint: false }
        // });
    }
}

function* handleApptReportPrinting() {
    yield takeLatest(type.PRINT_APPT_REPORT, printApptReport);
}

export const apptEnquirySaga = [
    // getHolidayList(),
    // handleHolidayListPrinting()
    handleApptReportPrinting(),
    getEnquiryResult()
];