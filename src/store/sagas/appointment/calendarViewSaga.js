import { take, call, put } from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as calendarViewActionType from '../../actions/appointment/calendarView/calendarViewActionType';
import * as commonType from '../../actions/common/commonActionType';
import * as messageType from '../../actions/message/messageActionType';

function* requestData() {
    while (true) {
        let { dataType, params, fileData } = yield take(calendarViewActionType.REQUEST_DATA);
        fileData = fileData || {};
        try {
            switch (dataType) {
                case 'clinicList':
                    {
                        let { data } = yield call(axios.post, '/common/listClinic', params);
                        if (data.respCode === 0) {
                            fileData['clinicListData'] = data.data;
                            yield put({ type: calendarViewActionType.FILLING_DATA, fillingData: fileData });
                        } else {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110031'
                                }
                            });
                        }
                    }
                    break;
                case 'calendarData':
                    {
                        let { data } = yield call(axios.post, '/appointment/searchTimeSlotForCalendar', params);
                        if (data.respCode === 0) {
                            fileData['calendarData'] = data.data;
                            yield put({ type: calendarViewActionType.FILLING_DATA, fillingData: fileData });
                        } else {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110031'
                                }
                            });
                        }
                    }
                    break;
                default:
                    break;
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* updateField() {
    while (true) {
        let { updateData } = yield take(calendarViewActionType.UPDATE_FIELD);
        try {
            if (updateData.clinicValue) {
                let { data } = yield call(axios.post, '/appointment/getEncounterTypeList', { clinicCd: updateData.clinicValue });
                if (data.respCode === 0) {
                    if (updateData.encounterTypeValue) {
                        let keyAndValue = {};
                        let fillingData = {};
                        fillingData.encounterTypeListData = data.data;
                        let selectEncounterTypes = data.data.filter((item) => { return item.encounterTypeCd === updateData.encounterTypeValue; });
                        if (selectEncounterTypes.length > 0) {
                            fillingData.encounterTypeValue = updateData.encounterTypeValue;
                            fillingData.selectEncounterType = { ...selectEncounterTypes[0] };
                            fillingData.subEncounterTypeListData = selectEncounterTypes[0].subEncounterTypeList.map(item => { keyAndValue[item.subEncounterTypeCd] = item; return item; });
                            fillingData.subEncounterTypeListKeyAndValue = keyAndValue;
                            fillingData.subEncounterTypeValue = [];
                        }
                        yield put({ type: calendarViewActionType.FILLING_DATA, fillingData: fillingData });
                    } else {
                        yield put({ type: calendarViewActionType.FILLING_DATA, fillingData: { encounterTypeListData: data.data } });
                    }
                } else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

export const calendarViewSaga = [
    requestData(),
    updateField()
];