import * as types from '../../actions/appointment/waitingList/waitingListActionType';
import {
    take,
    takeLatest,
    call,
    all,
    put
} from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as commonType from '../../actions/common/commonActionType';
import * as messageType from '../../actions/message/messageActionType';
import * as PatientUtil from '../../../utilities/patientUtilities';

function* initiPage() {
    while (true) {
        let { params } = yield take(types.INITI_PAGE);
        try {
            const [encounterTypeList, codeList, clinicList] = yield all([
                call(axios.post, '/appointment/getEncounterTypeList', {}),
                call(axios.post, '/common/listCodeList', ['doc_type']),
                call(axios.post, '/common/listClinic', params)
            ]);
            if (encounterTypeList.data.respCode !== 0) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            } else if (codeList.data.respCode !== 0) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            } else if (clinicList.data.respCode !== 0) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            } else {
                yield put({
                    type: types.UPDATE_FIELD,
                    fields: {
                        encounterTypeList: encounterTypeList.data.data,
                        docTypeList: codeList.data.data.doc_type,
                        clinicList: clinicList.data.data
                    }
                });
            }

        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* searchWaitingList() {
    while (true) {
        let { params, fields, callback } = yield take(types.SEARCH_WAITING_LIST);
        try {
            let { data } = yield call(axios.post, '/appointment/listWaitingList', params);
            if (data.respCode === 0) {
                fields.waitingList = data.data;
                fields.page = data.data.page;
                fields.pageSize = data.data.pageSize;
                yield put({ type: types.UPDATE_FIELD, fields });
                if (typeof callback === 'function') {
                    callback(data.data.page - 1);
                }
            } else if (data.respCode === 1) {
                //todo parameterException
            } else if (data.respCode === 100) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110275'
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}
function* getWaiting() {
    while (true) {
        let { params } = yield take(types.GET_WAITING);
        try {
            let { data } = yield call(axios.post, '/appointment/getWaitingList', params);
            if (data.respCode === 0) {
                console.log('data', data.data);
                let encounter = yield call(axios.post, '/appointment/getEncounterTypeList', { clinicCd: data.data.clinicCd });
                if (encounter.data.respCode === 0) {
                    yield put({ type: types.EDIT_WAITING, waiting: data.data, waitingEncounterTypeList: encounter.data.data });
                } else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
            } else if (data.respCode === 100) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110270'
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getEncounterList() {
    while (true) {
        let { params, fields = {} } = yield take(types.GET_ENCOUNTER_LIST);
        try {
            let { data } = yield call(axios.post, '/appointment/getEncounterTypeList', params);
            if (data.respCode === 0) {
                let updateFields = {
                    waitingClinicCd: params.clinicCd,
                    waitingEncounterTypeList: data.data,
                    waitingEncounterTypeCd: null,
                    ...fields
                };
                yield put({
                    type: types.UPDATE_FIELD,
                    fields: updateFields
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* fetchSeachPatientList(action) {
    try {
        yield put({ type: commonType.OPEN_SEARCH });
        let { data } = yield call(axios.post, '/patient/searchPatient', action.params);
        yield put({ type: commonType.CLOSE_SEARCH });
        if (data.respCode === 0) {
            yield put({ type: types.PUT_SEARCH_PATIENT_LIST, data: data.data });
        } else {
            yield put({ type: types.PUT_SEARCH_PATIENT_LIST, data: null });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* seachPatientList() {
    yield takeLatest(types.SEARCH_PATIENT_LIST, fetchSeachPatientList);
}


function* getPatient() {
    while (true) {
        let { patientKey } = yield take(types.GET_PATIENT_BY_ID);
        try {
            let { data } = yield call(axios.post, '/patient/getPatient', { patientKey });
            if (data.respCode === 0) {
                let patient = PatientUtil.transferPatientDocumentPair(data.data);
                yield put({ type: types.UPDATE_WAITING_FIELD, patient: patient });
            } else if (data.respCode === 100) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110130'
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}
function* saveWaiting() {
    while (true) {
        let { params, callback } = yield take(types.SAVE_WAITING);
        try {
            if (params.statu === 'E') {
                let { data } = yield call(axios.post, '/appointment/updateWaitingList', params.data);
                if (data.respCode === 0) {
                    yield put({ type: types.SAVE_SUCCESS });
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110247',
                            btnActions: {
                                btn1Click: () => {
                                    callback && callback();
                                }
                            }
                        }
                    });
                } else if (data.respCode === 1) {
                    //todo parameterException
                } else if (data.respCode === 3) {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110032'
                        }
                    });
                } else if (data.respCode === 102) {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110271'
                        }
                    });
                } else if (data.respCode === 101) {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110272'
                        }
                    });
                } else if (data.respCode === 100) {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110273'
                        }
                    });
                } else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
            } else {
                let { data } = yield call(axios.post, '/appointment/insertWaitingList', params.data);
                if (data.respCode === 0) {
                    yield put({ type: types.SAVE_SUCCESS });
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110247',
                            btnActions: {
                                btn1Click: () => {
                                    callback && callback();
                                }
                            }
                        }
                    });
                } else if (data.respCode === 1) {
                    //todo parameterException
                } else if (data.respCode === 2) {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                } else if (data.respCode === 100) {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110273'
                        }
                    });
                } else if (data.respCode === 101) {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110274'
                        }
                    });
                } else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
            }

        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}
function* deleteWaiting() {
    while (true) {
        let { params, callback } = yield take(types.DELETE_WAITING);
        try {
            let { data } = yield call(axios.post, '/appointment/deleteWaitingList', params);
            if (data.respCode === 0) {
                yield put({ type: types.DELETE_SUCCESS });
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110246',
                        btnActions: {
                            btn1Click: () => {
                                callback && callback();
                            }
                        }
                    }
                });
            } else if (data.respCode === 1) {
                //todo parameterException
            } else if (data.respCode === 100) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110273'
                    }
                });
            } else if (data.respCode === 3) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110032'
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}




export const waitingListSaga = [
    initiPage(),
    searchWaitingList(),
    getEncounterList(),
    getWaiting(),
    seachPatientList(),
    getPatient(),
    saveWaiting(),
    deleteWaiting()
];