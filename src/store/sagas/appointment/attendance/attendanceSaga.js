import { take, takeLatest, call, put } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as attendanceActionTypes from '../../../actions/appointment/attendance/attendanceActionType';
import * as commonType from '../../../actions/common/commonActionType';
import * as messageType from '../../../actions/message/messageActionType';
import * as patientType from '../../../actions/patient/patientActionType';
import _ from 'lodash';

function* getPatientStatus() {
    while (true) {
        try {
            let { params } = yield take(attendanceActionTypes.GET_PATIENT_STATUS);
            let { data } = yield call(axios.post, '/common/listCodeList', params);
            if (data.respCode === 0) {
                yield put({
                    type: attendanceActionTypes.PATIENT_STATUS_LIST,
                    data: data.data
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getAppointmentForAttend() {
    while (true) {
        try {
            let { appointmentList, appointmentId } = yield take(attendanceActionTypes.GET_APPOINTMENT_FOR_TAKE_ATTENDANCE);
            let curAppt = null;
            if (appointmentList && appointmentList.length > 0) {
                let index = appointmentList.findIndex(item => item.appointmentId === appointmentId);
                if (index > -1) {
                    curAppt = _.cloneDeep(appointmentList[index]);
                } else {
                    curAppt = _.cloneDeep(appointmentList[0]);
                }
            }
            yield put({ type: attendanceActionTypes.APPOINTMENT_FOR_ATTENDANCE_SUCCESS, currentAppointment: curAppt });
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* markAttendance() {
    while (true) {
        try {
            let { params, searchParams, callback } = yield take(attendanceActionTypes.MARK_ATTENDANCE);
            let { data } = yield call(axios.post, '/appointment/takeAttendance', params);
            if (data.respCode === 0) {
                yield put({
                    type: attendanceActionTypes.MARK_ATTENDANCE_SUCCESS,
                    data: data.data
                });
                yield put({
                    type: attendanceActionTypes.LIST_APPOINTMENT,
                    params: searchParams
                });
                callback && callback();
            } else if (data.respCode === 100) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110251'
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* fetchApptList(action) {
    let { data } = yield call(axios.post, '/appointment/listAppointment', action.params);
    try {
        if (data.respCode === 0) {
            yield put({
                type: attendanceActionTypes.PUT_APPOINTMENT_LIST,
                appointmentList: data.data
                // curAppointmentId: action.curAppointmentId
            });
            if (typeof action.callback === 'function') {
                action.callback(data.data);
            }
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* getApptList() {
    yield takeLatest(attendanceActionTypes.LIST_APPOINTMENT, fetchApptList);
}
export const attendanceSaga = [
    getPatientStatus(),
    markAttendance(),
    getAppointmentForAttend(),
    getApptList()
];