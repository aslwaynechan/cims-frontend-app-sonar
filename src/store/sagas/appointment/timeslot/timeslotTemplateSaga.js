import { take, takeLeading, call, put } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as timeslotTemplateActionTypes from '../../../actions/appointment/timeslotTemplate/timeslotTemplateActionType';
import * as commonType from '../../../actions/common/commonActionType';
import * as messageType from '../../../actions/message/messageActionType';

function* getEncountertypeCodeList() {
    while (true) {
        try {
            yield take(timeslotTemplateActionTypes.GET_ENCOUNTERTYPE_CODE_LIST);
            let { data } = yield call(axios.post, '/appointment/getEncounterTypeList', {});
            if (data.respCode === 0) {
                yield put({
                    type: timeslotTemplateActionTypes.ENCOUNTERTYPE_CODE_LIST,
                    encounterTypeCodeList: data.data
                }
                );
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getTimeslotTempalteList() {
    while (true) {
        try {
            let { params } = yield take(timeslotTemplateActionTypes.SEARCH_TIMESLOT_TEMPLATE);
            let { data } = yield call(axios.post, '/appointment/searchTimeSlotTemplate', params);
            if (data.respCode === 0) {
                yield put({ type: timeslotTemplateActionTypes.TIMESLOT_TEMPLATE_LIST, timeslotTempalteList: data.data });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* fetchSaveTimeslotTemplate(action) {
    try {
        const { params } = action;
        let { data } = yield call(axios.post, '/appointment/insertTimeSlotTemplate', params);
        if (data.respCode === 0) {
            yield put({
                type: timeslotTemplateActionTypes.SAVE_SUCCESS,
                data: data.data,
                encounterTypeCd: params.encounterTypeCd,
                subEncounterTypeCd: params.subEncounterTypeCd
            });
            yield put({
                type: timeslotTemplateActionTypes.SEARCH_TIMESLOT_TEMPLATE,
                params: {
                    encounterTypeCd: params.encounterTypeCd,
                    slotProfileCode: '',
                    subEncounterTypeCd: params.subEncounterTypeCd
                }
            });
        } else if (data.respCode === 100) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110268'
                }
            });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* saveTimeslotTemplate() {
    yield takeLeading(timeslotTemplateActionTypes.SAVE_TEMPLATE, fetchSaveTimeslotTemplate);
}

function* fetchUpdateTimeslotTemplate(action) {
    try {
        const { params } = action;
        let { data } = yield call(axios.post, '/appointment/updateTimeSlotTemplate', params);
        if (data.respCode === 0) {
            yield put({
                type: timeslotTemplateActionTypes.SAVE_SUCCESS,
                data: data.data,
                encounterTypeCd: params.encounterTypeCd,
                subEncounterTypeCd: params.subEncounterTypeCd
            });
            yield put({
                type: timeslotTemplateActionTypes.SEARCH_TIMESLOT_TEMPLATE,
                params: {
                    encounterTypeCd: params.encounterTypeCd,
                    slotProfileCode: '',
                    subEncounterTypeCd: params.subEncounterTypeCd
                }
            });
        } else if (data.respCode === 1) {
            //todo parameterException
        } else if (data.respCode === 100) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110268'
                }
            });
        } else if (data.respCode === 101) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110269'
                }
            });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* updateTimeslotTemplate() {
    yield takeLeading(timeslotTemplateActionTypes.UPDATE_TEMPLATE, fetchUpdateTimeslotTemplate);
}

function* getTimeslotTemplate() {
    while (true) {
        try {
            let { params } = yield take(timeslotTemplateActionTypes.GET_TIMESLOT_TEMPLATE);
            const callParams = { slotTemplateId: params[0] };
            let { data } = yield call(axios.post, '/appointment/getTimeSlotTemplate', callParams);
            if (data.respCode === 0) {
                yield put({ type: timeslotTemplateActionTypes.TIMESLOT_TEMPLATE, data: data.data });
            } else if (data.respCode === 100) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110269'
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

export const timeslotTemplateSagas = [
    getEncountertypeCodeList(),
    getTimeslotTempalteList(),
    saveTimeslotTemplate(),
    updateTimeslotTemplate(),
    getTimeslotTemplate()
];