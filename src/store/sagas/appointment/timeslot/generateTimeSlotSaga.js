import { take, call, put, takeLatest } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import moment from 'moment';
import * as GenerateTimeSlotActionType from '../../../actions/appointment/generateTimeSlot/generateTimeSlotActionType';
import * as commonType from '../../../actions/common/commonActionType';
// import * as messageType from '../../../actions/message/messageActionType';
import * as messageUtilities from '../../../../utilities/messageUtilities';
import * as messageType from '../../../actions/message/messageActionType';

function* getCodeList() {
    while (true) {
        yield take(GenerateTimeSlotActionType.GET_CODE_LIST);
        try {
            let { data } = yield call(axios.post, '/appointment/getEncounterTypeList', {});
            if (data.respCode === 0) {
                yield put({
                    type: GenerateTimeSlotActionType.PUT_CODE_LIST,
                    codeList: data.data
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* fetchSearchTemplate(action) {
    try {
        yield put({ type: GenerateTimeSlotActionType.OPEN_SEARCH });
        let { data } = yield call(axios.post, '/appointment/searchTimeSlotTemplate', action.params);
        if (data.respCode === 0) {
            yield put({ type: GenerateTimeSlotActionType.PUT_TEMPLATE_DATA, data: data.data });
            yield put({ type: GenerateTimeSlotActionType.CLOSE_SEARCH });
        } else {
            yield put({ type: GenerateTimeSlotActionType.PUT_TEMPLATE_DATA, data: [] });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* searchTemplate() {
    yield takeLatest(GenerateTimeSlotActionType.SEARCH_TEMPLATE, fetchSearchTemplate);
}

function* fetchGenerateTimeSlot(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/generateTimeSlot', action.params);
        if (data.respCode === 0) {
            let errorMessage = messageUtilities.getMessageDescriptionByMsgCode('110237');
            if (data.data.totalSuccessNum === 0) {
                errorMessage = messageUtilities.getMessageDescriptionByMsgCode('110214');
            } else {
                errorMessage = messageUtilities.getMessageDescriptionByMsgCode('110216').replace('%NUMBER%', data.data.totalSuccessNum);
            }

            let details = [];
            if (data.data.alreadyExist && data.data.alreadyExist.length > 0) {
                details.push({ detail: messageUtilities.getMessageDescriptionByMsgCode('110217') });
                for (let i = 0; i < data.data.alreadyExist.length; i++) {
                    const existItem = data.data.alreadyExist[i];
                    details.push({ detail: `${moment(existItem).format('DD MMM YYYY HH:mm')}` });
                }
            }
            yield put({ type: GenerateTimeSlotActionType.PUT_GENERATE_SUCCESS, errorMessage: errorMessage, details: details });
            // yield put({
            //     type: messageType.OPEN_COMMON_MESSAGE,
            //     payload: {
            //         msgCode: '110237'
            //     }
            // });
        } else if (data.respCode === 1) {
            //todo parameterException
        } else if (data.respCode === 100) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110267'
                }
            });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* generateTimeSlot() {
    yield takeLatest(GenerateTimeSlotActionType.GENERATE_TIMESLOT, fetchGenerateTimeSlot);
}

export const generateTimeSlotSaga = [
    getCodeList(),
    searchTemplate(),
    generateTimeSlot()
];