import { take, call, put, takeLatest } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as editTimeSlotActionType from '../../../actions/appointment/editTimeSlot/editTimeSlotActionType';
import * as commonType from '../../../actions/common/commonActionType';
import * as messageType from '../../../actions/message/messageActionType';
import _ from 'lodash';
import moment from 'moment';
import * as CommonUtilities from '../../../../utilities/commonUtilities';
import storeConfig from '../../../storeConfig';
import Enum from '../../../../enums/enum';

function* getCodeList() {
    while (true) {
        let { callback } = yield take(editTimeSlotActionType.GET_CODE_LIST);
        try {
            let { data } = yield call(axios.post, '/appointment/getEncounterTypeList', {});
            if (data.respCode === 0) {
                yield put({
                    type: editTimeSlotActionType.PUT_CODE_LIST,
                    codeList: data.data
                });
                callback && callback();
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* fetchListTimeSlot(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/listTimeSlot', action.params);
        if (data.respCode === 0) {
            yield put({ type: editTimeSlotActionType.PUT_LIST_TIME_SLOT, data: data.data });
        } else if (data.respCode === 1) {
            //todo parameterException
        } else {
            yield put({ type: editTimeSlotActionType.PUT_LIST_TIME_SLOT, data: [] });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* listTimeSlot() {
    yield takeLatest(editTimeSlotActionType.LIST_TIME_SLOT, fetchListTimeSlot);
}

function* fetchinsertTimeSlot(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/insertTimeSlot', action.params);
        if (data.respCode === 0) {
            yield put({ type: editTimeSlotActionType.LIST_TIME_SLOT, params: action.searchParams });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110222'
                }
            });
        } else if (data.respCode === 3) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110032'
                }
            });
        } else if (data.respCode === 100) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110261'
                }
            });
        } else if (data.respCode === 101) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110262'
                }
            });
        } else if (data.respCode === 102) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110263'
                }
            });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* insertTimeSlot() {
    yield takeLatest(editTimeSlotActionType.INSERT_TIME_SLOT, fetchinsertTimeSlot);
}

function* fetchUpdateTimeSlot(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/updateTimeSlot', action.params);
        if (data.respCode === 0) {
            yield put({ type: editTimeSlotActionType.LIST_TIME_SLOT, params: action.searchParams });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110220'
                }
            });
        } else if (data.respCode === 1) {
            //todo parameterException
        } else if (data.respCode === 3) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110032'
                }
            });
        } else if (data.respCode === 100) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110262'
                }
            });
        } else if (data.respCode === 101) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110263'
                }
            });
        } else if (data.respCode === 102) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110264'
                }
            });
        } else if (data.respCode === 103) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110265'
                }
            });
        } else if (data.respCode === 104) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110293'
                }
            });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* updateTimeSlot() {
    yield takeLatest(editTimeSlotActionType.UPDATE_TIME_SLOT, fetchUpdateTimeSlot);
}

function* fetchDeleteTimeSlot(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/deleteTimeSlot', action.params);
        if (data.respCode === 0) {
            yield put({ type: editTimeSlotActionType.LIST_TIME_SLOT, params: action.searchParams });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110243'
                }
            });
        } else if (data.respCode === 100) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110266'
                }
            });
        } else if (data.respCode === 101) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110265'
                }
            });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* deleteTimeSlot() {
    yield takeLatest(editTimeSlotActionType.DELETE_TIME_SLOT, fetchDeleteTimeSlot);
}

function* fetchMultipleUpdate(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/multipleUpdateTimeSlot', action.params);
        if (data.respCode === 0) {
            yield put({ type: editTimeSlotActionType.LIST_TIME_SLOT, params: action.searchParams });
            yield put({ type: editTimeSlotActionType.PUT_MULTIPLE_UPDATE, data: data.data, updateParams: action.params });

            let isWarning = false;
            let multipleUpdateForm = _.cloneDeep(action.params);
            let multipleUpdateData = _.cloneDeep(data.data);
            if (multipleUpdateForm.delete) {
                if (multipleUpdateData.booked && multipleUpdateData.booked.length > 0) {
                    let updateDetails = '';
                    for (let i = 0; i < multipleUpdateData.booked.length; i++) {
                        const bookedItem = multipleUpdateData.booked[i];
                        updateDetails += `- ${moment(bookedItem).format(Enum.DATE_FORMAT_24_HOUR)} `;
                    }
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110236',
                            params: [{ name: 'SLOT_STR', value: updateDetails }]
                        }
                    });
                } else {
                    if (parseInt(multipleUpdateData.totalSuccessNum) > 0) {
                        yield put({
                            type: messageType.OPEN_COMMON_MESSAGE,
                            payload: {
                                msgCode: '110226',
                                params: [{ name: 'ACTION', value: 'deleted' }],
                                showSnackbar: true
                            }
                        });
                    } else {
                        yield put({
                            type: messageType.OPEN_COMMON_MESSAGE,
                            payload: {
                                msgCode: '110227',
                                params: [{ name: 'ACTION', value: 'Delete' }],
                                showSnackbar: true
                            }
                        });
                    }
                }
            } else {
                let updateDetails = '', messageCode = '110225';
                // if update on time block
                if (multipleUpdateForm.newStartTime){
                    messageCode = '110290';
                    // when overlapped
                    if (multipleUpdateData.alreadyExist && multipleUpdateData.alreadyExist.length > 0) {
                        updateDetails += 'Below timeslot overlapped! ';
                        for (let i = 0; i < multipleUpdateData.alreadyExist.length; i++) {
                            const existItem = multipleUpdateData.alreadyExist[i];
                            updateDetails += `- ${moment(existItem).format(Enum.DATE_FORMAT_24_HOUR)} `;
                        }
                    }

                    //when booked
                    if (multipleUpdateData.booked && multipleUpdateData.booked.length > 0){
                        updateDetails += ' Update not allowed for the below booked Slot! ';
                        for (let i = 0; i < multipleUpdateData.booked.length; i++) {
                            const bookedItem = multipleUpdateData.booked[i];
                            updateDetails += `- ${moment(bookedItem).format(Enum.DATE_FORMAT_24_HOUR)} `;
                        }
                    }
                } else {
                    if (multipleUpdateData.overBookedQuota && multipleUpdateData.overBookedQuota.length > 0) {
                        updateDetails += 'New quota is less than booked appointment for the below Slot: ';
                        let detailArr = [];
                        let state = storeConfig.store.getState();
                        const clinicConfig = state.common.clinicConfig;
                        const service = state.login.service;
                        const clinic = state.login.clinic;

                        const where = {
                            serviceCd: service.serviceCd,
                            clinicCd: clinic.clinicCd,
                            encounterCd: multipleUpdateForm.encounterTypeCd,
                            subEncounterCd: multipleUpdateForm.subEncounterTypeCd
                        };
                        let quotaDescArray = CommonUtilities.getQuotaDescArray(clinicConfig, where);
                        for (let i = 0; i < multipleUpdateData.overBookedQuota.length; i++) {
                            const bookedItem = multipleUpdateData.overBookedQuota[i];
                            let str = `   ${moment(bookedItem.slotDate).format(Enum.DATE_FORMAT_EDMY_VALUE)} ${bookedItem.startTime} Quota [${bookedItem.newQuota}]/Booked [${bookedItem.bookedQuota}] `;
                            let index = detailArr.findIndex(item => item.name === `${bookedItem.caseType}${bookedItem.quotaType}`);
                            let detailDto = {};
                            if (index > -1) {
                                detailDto = detailArr[index];
                                detailDto.content = detailDto.content + str;
                                detailArr[index] = detailDto;
                            } else {
                                const quotaDesc = quotaDescArray && quotaDescArray.find(item => item.code === bookedItem.quotaType);
                                detailDto = {
                                    name: `${bookedItem.caseType}${bookedItem.quotaType}`,
                                    label: `- ${CommonUtilities.getCaseTypeDesc(bookedItem.caseType)} ${quotaDesc && quotaDesc.engDesc}: `,
                                    content: str
                                };
                                detailArr.push(detailDto);
                            }
                        }
                        detailArr.forEach(item => {
                            updateDetails += `${item.label} ${item.content}`;
                        });
                    }
                }

                if (updateDetails) {
                    if(messageCode === '110290') {
                        isWarning = true;
                    }
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: messageCode,
                            params: [{ name: 'SLOT_STR', value: updateDetails }]
                        }
                    });
                } else {
                    if (parseInt(multipleUpdateData.totalSuccessNum) > 0) {
                        yield put({
                            type: messageType.OPEN_COMMON_MESSAGE,
                            payload: {
                                msgCode: '110226',
                                params: [{ name: 'ACTION', value: 'updated' }],
                                showSnackbar: true
                            }
                        });
                    } else {
                        yield put({
                            type: messageType.OPEN_COMMON_MESSAGE,
                            payload: {
                                msgCode: '110227',
                                params: [{ name: 'ACTION', value: 'Update' }],
                                showSnackbar: true
                            }
                        });
                    }
                }
            }

            action.callback && action.callback(isWarning);
        } else if (data.respCode === 1) {
            //todo parameterException
        } else {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: data.errMsg ? data.errMsg : 'Service error',
                data: data.data
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* multipleUpdate() {
    yield takeLatest(editTimeSlotActionType.MULTIPLE_UPDATE, fetchMultipleUpdate);
}

export const editTimeSlotSaga = [
    getCodeList(),
    listTimeSlot(),
    insertTimeSlot(),
    updateTimeSlot(),
    deleteTimeSlot(),
    multipleUpdate()
];