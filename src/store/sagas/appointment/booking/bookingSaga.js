import { take, takeLatest, call, put } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as bookingActionType from '../../../actions/appointment/booking/bookingActionType';
import * as commonType from '../../../actions/common/commonActionType';
import * as messageType from '../../../actions/message/messageActionType';
import * as bookingAnonymousActionType from '../../../actions/appointment/booking/bookingAnonymousActionType';
// import * as patientActionType from '../../../actions/patient/patientActionType';
// import storeConfig from '../../../storeConfig';

function* requestData() {
    while (true) {
        let { dataType, params, fileData } = yield take(bookingActionType.REQUEST_DATA);
        fileData = fileData || {};
        try {
            switch (dataType) {
                case 'patientInfo':
                    {
                        let { data } = yield call(axios.post, '/patient/getPatient', params);
                        if (data.respCode === 0) {
                            fileData['patientInfo'] = data.data;
                            fileData['patientInfo']['patientKey'] = params.patientKey;
                            yield put({ type: bookingActionType.FILLING_DATA, fillingData: fileData });
                        } else if (data.respCode === 100) {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110134'
                                }
                            });
                            // } else if (data.respCode === 12) {
                            //     yield put({
                            //         type: messageType.OPEN_COMMON_MESSAGE,
                            //         payload: {
                            //             msgCode: '110135'
                            //         }
                            //     });
                        } else {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110031'
                                }
                            });
                        }
                    }
                    break;
                case 'calendarData':
                    {
                        let { data } = yield call(axios.post, '/appointment/searchTimeSlotForCalendar', params);
                        if (data.respCode === 0) {
                            fileData['calendarData'] = data.data;
                            yield put({ type: bookingActionType.FILLING_DATA, fillingData: fileData });
                        } else {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110031'
                                }
                            });
                        }
                    }
                    break;
                default:
                    break;
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* updateField() {
    while (true) {
        let { updateData } = yield take(bookingActionType.UPDATE_FIELD);
        try {
            if (updateData.clinicValue) {
                let { data } = yield call(axios.post, '/appointment/getEncounterTypeList', { clinicCd: updateData.clinicValue });
                if (data.respCode === 0) {
                    if (updateData.encounterTypeValue) {
                        let keyAndValue = {};
                        let fillingData = {};
                        fillingData.encounterTypeListData = data.data;
                        let selectEncounterTypes = data.data.filter((item) => { return item.encounterTypeCd === updateData.encounterTypeValue; });
                        if (selectEncounterTypes.length > 0) {
                            fillingData.encounterTypeValue = updateData.encounterTypeValue;
                            fillingData.selectEncounterType = { ...selectEncounterTypes[0] };
                            fillingData.subEncounterTypeListData = selectEncounterTypes[0].subEncounterTypeList.map(item => { keyAndValue[item.subEncounterTypeCd] = item.shortName; return item; });
                            fillingData.subEncounterTypeListKeyAndValue = keyAndValue;
                            fillingData.subEncounterTypeValue = [];
                        }
                        yield put({ type: bookingActionType.FILLING_DATA, fillingData: fillingData });
                    } else {
                        yield put({ type: bookingActionType.FILLING_DATA, fillingData: { encounterTypeListData: data.data } });
                    }
                } else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getClinicList() {
    while (true) {
        let { serviceCode } = yield take(bookingActionType.GET_CLINIC_LIST);
        try {
            if (serviceCode) {
                let { data } = yield call(axios.post, '/common/listClinic', { serviceCd: serviceCode });
                if (data.respCode === 0) {
                    yield put({
                        type: bookingActionType.PUT_CLINIC_LIST,
                        clinicList: data.data
                    });
                } else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getEncounterTypeList() {
    while (true) {
        let { params, serviceCd, clinicCd, clinicConfig } = yield take(bookingActionType.GET_ENCOUNTER_TYPE_LIST);
        try {
            let { data } = yield call(axios.post, '/appointment/getEncounterTypeList', { clinicCd: params });
            if (data.respCode === 0) {
                yield put({
                    type: bookingActionType.PUT_ENCOUNTER_TYPE_LIST,
                    encounterTypeList: data.data,
                    serviceCd: serviceCd,
                    clinicCd: clinicCd,
                    clinicConfig: clinicConfig
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* fetchappointmentBooking(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/bookAppointment', action.params);
        if (data.respCode === 0) {
            yield put({ type: bookingActionType.PUT_TIMESLOT_DATA, data: data.data });
        } else if (data.respCode === 101) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110277'
                }
            });
        } else if (data.respCode === 100) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110278'
                }
            });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* appointmentBooking() {
    yield takeLatest(bookingActionType.APPOINTMENT_BOOK, fetchappointmentBooking);
}

function* fetchListTimeSlotForAppointmentBook(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/searchTimeSlotForBooking', action.params);
        if (data.respCode === 0) {
            yield put({ type: bookingActionType.PUT_LIST_TIMESLOT_DATA, timeSlotList: data.data });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* listTimeSlotForAppointmentBook() {
    yield takeLatest(bookingActionType.LIST_TIMESLOT, fetchListTimeSlotForAppointmentBook);
}

function* listAppointment() {
    while (true) {
        let { params, callback } = yield take(bookingActionType.LIST_APPOINTMENT);
        try {
            let { data } = yield call(axios.post, '/appointment/listAppointment', params);
            if (data.respCode === 0) {
                yield put({
                    type: bookingActionType.PUT_LIST_APPOINTMENT_DATA,
                    appointmentList: data.data
                });
                callback && callback();
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* fetchLoadAppointmentBooking(action) {
    try {
        let { passData } = action;
        let clinicCd = passData && passData.clinicCd;
        if (clinicCd) {
            let { data } = yield call(axios.post, '/appointment/getEncounterTypeList', { clinicCd: clinicCd });
            if (data.respCode === 0) {
                yield put({ type: bookingActionType.PUT_ENCOUNTER_TYPE_LIST, encounterTypeList: data.data });
                yield put({ type: bookingActionType.PUT_BOOKING_DATA, bookData: passData, encounterTypeList: data.data });
            } else {
                yield put({ type: bookingActionType.PUT_BOOKING_DATA, bookData: passData, encounterTypeList: null });
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* loadAppointmentBooking() {
    yield takeLatest(bookingActionType.LOAD_APPOINTMENT_BOOKING, fetchLoadAppointmentBooking);
}

function* fetchBookingConfirm(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/confirmAppointment', action.params, { retry: 0 });

        if (data.respCode === 0) {
            yield put({ type: bookingActionType.PUT_BOOK_SUCCESS, respData: data.data, bookData: action.params });
            yield put({ type: bookingActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
            // } else if (data.respCode === 100) {
            //     yield put({
            //         type: messageType.OPEN_COMMON_MESSAGE,
            //         payload: {
            //             msgCode: '110277'
            //         }
            //     });
            //     yield put({ type: bookingActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
            // } else if (data.respCode === 101) {
            //     yield put({
            //         type: messageType.OPEN_COMMON_MESSAGE,
            //         payload: {
            //             msgCode: '110257'
            //         }
            //     });
            //     yield put({ type: bookingActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
        } else if (data.respCode === 3) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110032'
                }
            });
            yield put({ type: bookingActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
        } else if (data.respCode === 103) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110278'
                }
            });
            yield put({ type: bookingActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
        } else if (data.respCode === 104) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110282'
                }
            });
            yield put({ type: bookingActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
        } else if (data.respCode === 105) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110286'
                }
            });
            yield put({ type: bookingAnonymousActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
        } else {
            yield put({ type: bookingActionType.PUT_BOOK_FAIL });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
            yield put({ type: bookingActionType.UPDATE_STATE, updateData: { handlingBooking: false } });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        yield put({ type: bookingActionType.UPDATE_STATE, updateData: { handlingBooking: false } });
    }
}

function* bookingConfirm() {
    yield takeLatest(bookingActionType.BOOK_CONFIRM, fetchBookingConfirm);
}

function* fetchBookingConfirmWaiting(action) {
    try {
        let { data } = yield call(axios.post, 'appointment/confirmWaitingAppointment', action.params, { retry: 0 });
        if (data.respCode === 0) {
            // yield put({ type: bookingActionType.PUT_BOOK_SUCCESS, data: data.data });
            yield put({ type: bookingActionType.PUT_BOOK_SUCCESS, respData: data.data, bookData: action.params });
            yield put({ type: bookingActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
            // yield put({
            //     type: messageType.OPEN_COMMON_MESSAGE,
            //     payload: {
            //         msgCode: '110230',
            //         showSnackbar: true
            //     }
            // });
            yield put({ type: bookingActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
        } else if (data.respCode === 100) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110252'
                }
            });
        } else if (data.respCode === 101) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110256'
                }
            });
        } else if (data.respCode === 102) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110253'
                }
            });
        } else if (data.respCode === 103) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110278'
                }
            });
        } else if (data.respCode === 3) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110032'
                }
            });
        } else if (data.respCode === 104) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110282'
                }
            });
        } else if (data.respCode === 105) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110286'
                }
            });
        } else {
            yield put({ type: bookingActionType.PUT_BOOK_FAIL });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
            yield put({ type: bookingActionType.UPDATE_STATE, updateData: { handlingBooking: false } });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        yield put({ type: bookingActionType.UPDATE_STATE, updateData: { handlingBooking: false } });
    }
}

function* bookingConfirmWaiting() {
    yield takeLatest(bookingActionType.BOOK_CONFIRM_WAITING, fetchBookingConfirmWaiting);
}


function* getAppointmentReport() {
    while (true) {
        let { params } = yield take(bookingActionType.GET_APPOINTMENT_REPORT);
        try {
            let url = params.reportType === 'Single' ? '/appointment/reportSingleSlip' : '/appointment/reportMultipleSlip';
            let { data } = yield call(axios.post, url, params.reportParam);
            if (data.respCode === 0) {
                yield put({
                    type: commonType.PRINT_START,
                    params: { base64: data.data }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }

        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* walkInAppointment() {
    while (true) {
        let { params, callback } = yield take(bookingActionType.WALK_IN_ATTENDANCE);
        try {
            let url = 'appointment/walkInAttendance';
            let { data } = yield call(axios.post, url, params);
            if (data.respCode === 0) {
                yield put({ type: bookingActionType.BOOK_AND_ATTEND_SUCCEESS, respData: data.data, walkInData: params });
                callback && callback(data.data.appointmentId);
            } else if (data.respCode === 3) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110032'
                    }
                });
            } else if (data.respCode === 100) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110276'
                    }
                });
            } else if (data.respCode === 104) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110282'
                    }
                });
            } else {
                // let errMsg = '';
                // if (data.data) {
                //     errMsg = `${data.data[0].fieldName}: ` + data.data[0].errMsg;
                // } else {
                //     errMsg = data.errMsg;
                // }
                // yield put({
                //     type: commonType.OPEN_ERROR_MESSAGE,
                //     error: errMsg !== '' ? errMsg : 'Service error'
                // });
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* listSearchLogic() {
    while (true) {
        //let { params } = yield take(bookingActionType.LIST_APPOINTMENT);
        yield take(bookingActionType.LIST_SEARCH_LOGIC);
        try {
            let { data } = yield call(axios.post, '/appointment/listAllSearchLogicByUser');
            if (data.respCode === 0) {
                yield put({
                    type: bookingActionType.PUT_USER_SEARCH_LOGIC,
                    searchLogicList: data.data
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* listRemarkCode() {
    while (true) {
        yield take(bookingActionType.LIST_REMARK_CODE);
        try {
            let { data } = yield call(axios.get, '/appointment/listRemarkCode');
            if (data.respCode === 0) {
                yield put({
                    type: bookingActionType.PUT_LIST_REMARK_CODE,
                    remarkCodeList: data.data
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* cancelAppointment() {
    while (true) {
        let { apptPara, listApptPara } = yield take(bookingActionType.CANCEL_APPOINTMENT);
        try {
            let url = 'appointment/cancelAppointment';
            let { data } = yield call(axios.post, url, apptPara);
            if (data.respCode === 0) {
                //yield put({ type: bookingActionType.BOOK_AND_ATTEND_SUCCEESS });
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110234'
                    }
                });
                yield put({
                    type: bookingActionType.LIST_APPOINTMENT,
                    params: listApptPara
                });
            } else if (data.respCode === 1) {
                //todo parameterException
            } else if (data.respCode === 3) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110032'
                    }
                });
            } else if (data.respCode === 100) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110258'
                    }
                });
            } else if (data.respCode === 102) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110259'
                    }
                });
            } else if (data.respCode === 101) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110260'
                    }
                });
            } else {
                // let errMsg = '';
                // if (data.data) {
                //     errMsg = `${data.data[0].fieldName}: ` + data.data[0].errMsg;
                // } else {
                //     errMsg = data.errMsg;
                // }
                // yield put({
                //     type: commonType.OPEN_ERROR_MESSAGE,
                //     error: errMsg !== '' ? errMsg : 'Service error'
                // });
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}


function* updateAppointment() {
    while (true) {
        let { updateApptPara, listApptPara, updateOrBookNew, bookData, submitParams } = yield take(bookingActionType.SUBMIT_UPDATE_APPOINTMENT);
        try {
            let url = '';
            if (updateOrBookNew === 'update') {
                url = 'appointment/updateAppointment';
            }
            else if (updateOrBookNew === 'bookNew') {
                url = 'appointment/cancelAndConfirmAppointment';
            }
            else {
                url = 'appointment/updateAppointment';
            }
            let { data } = yield call(axios.post, url, updateApptPara);
            if (data.respCode === 0) {
                //yield put({ type: bookingActionType.BOOK_AND_ATTEND_SUCCEESS });
                if (updateOrBookNew === 'update') {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110233'
                        }
                    });
                    yield put({
                        type: bookingActionType.LIST_APPOINTMENT,
                        params: listApptPara
                    });
                    yield put({
                        type: bookingActionType.UPDATE_APPOINTMENT_SUCCESS
                    });

                }
                else if (updateOrBookNew === 'bookNew') {
                    yield put({ type: bookingActionType.PUT_BOOK_SUCCESS, respData: data.data, bookData: submitParams });
                    yield put(
                        {
                            type: bookingActionType.UPDATE_STATE,
                            updateData: { bookingData: bookData, handlingBooking: false, isUpdating: false, currentSelectedApptInfo: null, updateOrBookNew: '' }
                        }
                    );
                }
            }
            else {
                const respCode = data.respCode;
                yield put(
                    {
                        type: bookingActionType.UPDATE_STATE,
                        updateData: { handlingBooking: false }
                    }
                );
                switch (respCode) {
                    case 1: {
                        break;
                    }
                    case 100: {
                        yield put({
                            type: messageType.OPEN_COMMON_MESSAGE,
                            payload: {
                                msgCode: '110258'
                            }
                        });
                        break;
                    }
                    case 3: {
                        yield put({
                            type: messageType.OPEN_COMMON_MESSAGE,
                            payload: {
                                msgCode: '110032'
                            }
                        });
                        break;
                    }
                    case 101: {
                        yield put({
                            type: messageType.OPEN_COMMON_MESSAGE,
                            payload: {
                                msgCode: '110260'
                            }
                        });
                        break;
                    }
                    case 103: {
                        yield put({
                            type: messageType.OPEN_COMMON_MESSAGE,
                            payload: {
                                msgCode: '110278'
                            }
                        });
                        break;
                    }
                    case 102: {
                        yield put({
                            type: messageType.OPEN_COMMON_MESSAGE,
                            payload: {
                                msgCode: '110259'
                            }
                        });
                        break;
                    }
                    case 104: {
                        yield put({
                            type: messageType.OPEN_COMMON_MESSAGE,
                            payload: {
                                msgCode: '110282'
                            }
                        });
                        break;
                    }
                    default: {
                        yield put({ type: bookingActionType.PUT_BOOK_FAIL, data: data.data, errMsg: data.errMsg });
                        break;
                    }
                }
            }
            // else if (data.respCode === 1) {
            //     //todo parameterException
            //     yield put(
            //         {
            //             type: bookingActionType.UPDATE_STATE,
            //             updateData: { handlingBooking: false }
            //         }
            //     );
            // } else if (data.respCode === 9) {
            //     yield put({
            //         type: messageType.OPEN_COMMON_MESSAGE,
            //         payload: {
            //             msgCode: '110032'
            //         }
            //     });
            // } else if (data.respCode === 4) {
            //     yield put({
            //         type: messageType.OPEN_COMMON_MESSAGE,
            //         payload: {
            //             msgCode: '110258'
            //         }
            //     });
            // } else if (data.respCode === 14) {
            //     yield put({
            //         type: messageType.OPEN_COMMON_MESSAGE,
            //         payload: {
            //             msgCode: '110278'
            //         }
            //     });
            // } else if (data.respCode === 15) {
            //     yield put({
            //         type: messageType.OPEN_COMMON_MESSAGE,
            //         payload: {
            //             msgCode: '110259'
            //         }
            //     });
            // } else if (data.respCode === 12) {
            //     yield put({
            //         type: messageType.OPEN_COMMON_MESSAGE,
            //         payload: {
            //             msgCode: '110260'
            //         }
            //     });
            // } else if (data.respCode === 17) {
            //     yield put(
            //         {
            //             type: bookingActionType.UPDATE_STATE,
            //             updateData: { handlingBooking: false }
            //         }
            //     );
            //     yield put({
            //         type: messageType.OPEN_COMMON_MESSAGE,
            //         payload: {
            //             msgCode: '110282'
            //         }
            //     });
            // }
            // else {
            //     yield put({ type: bookingActionType.PUT_BOOK_FAIL, data: data.data, errMsg: data.errMsg });
            //     yield put({ type: bookingActionType.UPDATE_STATE, updateData: { handlingBooking: false } });
            // }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
            yield put({ type: bookingActionType.UPDATE_STATE, updateData: { handlingBooking: false } });
        }
    }
}


function* listContatHistory() {
    while (true) {
        let { apptId, callback } = yield take(bookingActionType.GET_CONTACT_HISTORY);
        try {
            if (apptId) {
                let { data } = yield call(axios.post, '/appointment/listContactHistory', { appointmentId: apptId });
                if (data.respCode === 0) {
                    yield put({
                        type: bookingActionType.PUT_CONTACT_HISTORY,
                        contactList: data.data
                    });
                    callback && callback();
                } else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* insertContatHistory() {
    while (true) {
        let { params, callback } = yield take(bookingActionType.INSERT_CONTACT_HISTORY);
        try {
            if (params) {
                let { data } = yield call(axios.post, '/appointment/insertContactHistory', params);
                if (data.respCode === 0) {

                    callback && callback();
                } else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* updateContatHistory() {
    while (true) {
        let { params, callback } = yield take(bookingActionType.UPDATE_CONTACT_HISTORY);
        try {
            if (params) {
                let { data } = yield call(axios.post, '/appointment/updateContactHistory', params);
                if (data.respCode === 0) {

                    callback && callback();
                } else if (data.respCode === 100) {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110291'
                        }
                    });
                } else if (data.respCode === 101) {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110292'
                        }
                    });
                } else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}
function* listToalAppointment() {
    while (true) {
        let { params } = yield take(bookingActionType.LIST_TOTAL_APPOINTMENT);
        try {
            let { data } = yield call(axios.post, '/appointment/listAppointment', params);
            if (data.respCode === 0) {
                let hasNotAttn = data.data.appointmentDtos.find(item => item.attnStatusCd == 'Y');
                let defaultCaseTypeCd = !hasNotAttn ? 'N' : 'O';
                yield put({
                    type: bookingActionType.PUT_DEFAULT_CASETYPECD,
                    defaultCaseTypeCd: defaultCaseTypeCd
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

export const bookingSaga = [
    requestData(),
    updateField(),
    getClinicList(),
    getEncounterTypeList(),
    appointmentBooking(),
    listTimeSlotForAppointmentBook(),
    listAppointment(),
    loadAppointmentBooking(),
    bookingConfirm(),
    bookingConfirmWaiting(),
    getAppointmentReport(),
    walkInAppointment(),
    listSearchLogic(),
    listRemarkCode(),
    cancelAppointment(),
    updateAppointment(),
    listContatHistory(),
    updateContatHistory(),
    insertContatHistory(),
    listToalAppointment()
];