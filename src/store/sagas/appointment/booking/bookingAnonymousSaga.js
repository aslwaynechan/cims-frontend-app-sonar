import { take, takeLatest, call, put } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as bookingAnonymousActionType from '../../../actions/appointment/booking/bookingAnonymousActionType';
import * as commonType from '../../../actions/common/commonActionType';
import * as messageType from '../../../actions/message/messageActionType';

function* requestData() {
    while (true) {
        let { dataType, params, fileData } = yield take(bookingAnonymousActionType.REQUEST_DATA);
        fileData = fileData || {};
        try {
            switch (dataType) {
                case 'patientInfo':
                    {
                        let { data } = yield call(axios.post, '/patient/getPatient', params);
                        if (data.respCode === 0) {
                            fileData['patientInfo'] = data.data;
                            fileData['patientInfo']['patientKey'] = params.patientKey;
                            yield put({ type: bookingAnonymousActionType.FILLING_DATA, fillingData: fileData });
                        } else if (data.respCode === 100) {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110134'
                                }
                            });
                        // } else if (data.respCode === 12) {
                        //     yield put({
                        //         type: messageType.OPEN_COMMON_MESSAGE,
                        //         payload: {
                        //             msgCode: '110135'
                        //         }
                        //     });
                        } else {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110031'
                                }
                            });
                        }
                    }
                    break;
                case 'calendarData':
                    {
                        let { data } = yield call(axios.post, '/appointment/searchTimeSlotForCalendar', params);
                        if (data.respCode === 0) {
                            fileData['calendarData'] = data.data;
                            yield put({ type: bookingAnonymousActionType.FILLING_DATA, fillingData: fileData });
                        } else {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110031'
                                }
                            });
                        }
                    }
                    break;
                default:
                    break;
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* updateField() {
    while (true) {
        let { updateData } = yield take(bookingAnonymousActionType.UPDATE_FIELD);
        try {
            if (updateData.clinicValue) {
                let { data } = yield call(axios.post, '/appointment/getEncounterTypeList', { clinicCd: updateData.clinicValue });
                if (data.respCode === 0) {
                    if (updateData.encounterTypeValue) {
                        let keyAndValue = {};
                        let fillingData = {};
                        fillingData.encounterTypeListData = data.data;
                        let selectEncounterTypes = data.data.filter((item) => { return item.encounterTypeCd === updateData.encounterTypeValue; });
                        if (selectEncounterTypes.length > 0) {
                            fillingData.encounterTypeValue = updateData.encounterTypeValue;
                            fillingData.selectEncounterType = { ...selectEncounterTypes[0] };
                            fillingData.subEncounterTypeListData = selectEncounterTypes[0].subEncounterTypeList.map(item => { keyAndValue[item.subEncounterTypeCd] = item.shortName; return item; });
                            fillingData.subEncounterTypeListKeyAndValue = keyAndValue;
                            fillingData.subEncounterTypeValue = [];
                        }
                        yield put({ type: bookingAnonymousActionType.FILLING_DATA, fillingData: fillingData });
                    } else {
                        yield put({ type: bookingAnonymousActionType.FILLING_DATA, fillingData: { encounterTypeListData: data.data } });
                    }
                } else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getClinicList() {
    while (true) {
        let { serviceCode } = yield take(bookingAnonymousActionType.GET_CLINIC_LIST);
        try {
            if (serviceCode) {
                let { data } = yield call(axios.post, '/common/listClinic', { serviceCd: serviceCode });
                if (data.respCode === 0) {
                    yield put({
                        type: bookingAnonymousActionType.PUT_CLINIC_LIST,
                        clinicList: data.data
                    });
                } else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getEncounterTypeList() {
    while (true) {
        let { params, serviceCd, clinicCd,clinicConfig } = yield take(bookingAnonymousActionType.GET_ENCOUNTER_TYPE_LIST);
        try {
            let { data } = yield call(axios.post, '/appointment/getEncounterTypeList', { clinicCd: params });
            if (data.respCode === 0) {
                yield put({
                    type: bookingAnonymousActionType.PUT_ENCOUNTER_TYPE_LIST,
                    encounterTypeList: data.data,
                    serviceCd:serviceCd,
                    clinicCd:clinicCd,
                    clinicConfig:clinicConfig
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* fetchappointmentBooking(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/bookAppointment', action.params);
        if (data.respCode === 0) {
            yield put({ type: bookingAnonymousActionType.PUT_TIMESLOT_DATA, data: data.data });
        } else if (data.respCode === 101) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110277'
                }
            });
        } else if (data.respCode === 100) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110278'
                }
            });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* appointmentBooking() {
    yield takeLatest(bookingAnonymousActionType.APPOINTMENT_BOOK, fetchappointmentBooking);
}

function* fetchListTimeSlotForAppointmentBook(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/searchTimeSlotForBooking', action.params);
        if (data.respCode === 0) {
            yield put({ type: bookingAnonymousActionType.PUT_LIST_TIMESLOT_DATA, timeSlotList: data.data });
        } else if (data.respCode === 1) {
            //todo parameterException
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* listTimeSlotForAppointmentBook() {
    yield takeLatest(bookingAnonymousActionType.LIST_TIMESLOT, fetchListTimeSlotForAppointmentBook);
}

function* listAppointment() {
    while (true) {
        let { params } = yield take(bookingAnonymousActionType.LIST_APPOINTMENT);
        try {
            let { data } = yield call(axios.post, '/appointment/listAppointment', params);
            if (data.respCode === 0) {
                yield put({
                    type: bookingAnonymousActionType.PUT_LIST_APPOINTMENT_DATA,
                    appointmentList: data.data
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}



function* fetchLoadAppointmentBooking(action) {
    try {
        let { passData } = action;
        let clinicCd = passData && passData.clinicCd;
        if (clinicCd) {
            let { data } = yield call(axios.post, '/appointment/getEncounterTypeList', { clinicCd: clinicCd });
            if (data.respCode === 0) {
                yield put({ type: bookingAnonymousActionType.PUT_ENCOUNTER_TYPE_LIST, encounterTypeList: data.data });
                yield put({ type: bookingAnonymousActionType.PUT_BOOKING_DATA, bookData: passData, encounterTypeList: data.data });
            } else {
                yield put({ type: bookingAnonymousActionType.PUT_BOOKING_DATA, bookData: passData, encounterTypeList: null });
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* loadAppointmentBooking() {
    yield takeLatest(bookingAnonymousActionType.LOAD_APPOINTMENT_BOOKING, fetchLoadAppointmentBooking);
}

function* fetchBookingConfirm(action) {
    try {
        let requestUrl = '';
        if (action.params.anonymousPatientDto) {
            requestUrl = '/appointment/confirmAnonymousAppointment';
        }
        else {
            requestUrl = '/appointment/confirmAppointment';
        }
        let { data } = yield call(axios.post, requestUrl, action.params, { retry: 0 });
        if (data.respCode === 0) {
            yield put({ type: bookingAnonymousActionType.PUT_BOOK_SUCCESS, data: data.data });
            const appointmentDateStr = data.data[0].appointmentDate + ' ' + data.data[0].appointmentTime;
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110230',
                    params: [{ name: 'DETAIL', value: appointmentDateStr }],
                    btnActions: {
                        btn1Click: action.callback
                    }
                }
            });
            yield put({ type: bookingAnonymousActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
        // } else if (data.respCode === 100) {
        //     yield put({
        //         type: messageType.OPEN_COMMON_MESSAGE,
        //         payload: {
        //             msgCode: '110277'
        //         }
        //     });
        //     yield put({ type: bookingAnonymousActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
        // } else if (data.respCode === 101) {
        //     yield put({
        //         type: messageType.OPEN_COMMON_MESSAGE,
        //         payload: {
        //             msgCode: '110257'
        //         }
        //     });
        //     yield put({ type: bookingAnonymousActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
        } else if (data.respCode === 3) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110032'
                }
            });
            yield put({ type: bookingAnonymousActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
        } else if (data.respCode === 103) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110278'
                }
            });
            yield put({ type: bookingAnonymousActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
        } else if (data.respCode === 104) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110282'
                }
            });
            yield put({ type: bookingAnonymousActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
        } else if (data.respCode === 105) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110286'
                }
            });
            yield put({ type: bookingAnonymousActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
            yield put({ type: bookingAnonymousActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
            yield put({
                type: bookingAnonymousActionType.UPDATE_STATE,
                updateData: { handlingBooking: false, openConfirmDialog: false }
            });
            yield put({ type: bookingAnonymousActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        yield put({ type: bookingAnonymousActionType.UPDATE_STATE, updateData: { handlingBooking: false } });
    }
}

function* bookingConfirm() {
    yield takeLatest(bookingAnonymousActionType.BOOK_CONFIRM, fetchBookingConfirm);
}

function* fetchBookingConfirmWaiting(action) {
    try {
        let { data } = yield call(axios.post, 'appointment/confirmWaitingAppointment', action.params, { retry: 0 });
        if (data.respCode === 0) {
            yield put({ type: bookingAnonymousActionType.PUT_BOOK_SUCCESS, data: data.data });
            const appointmentDateStr = data.data[0].appointmentDate + ' ' + data.data[0].appointmentTime;
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110230',
                    params: [{ name: 'DETAIL', value: appointmentDateStr }],
                    btnActions: {
                        btn1Click: action.callback
                    }
                }
            });
            yield put({ type: bookingAnonymousActionType.UPDATE_STATE, updateData: { bookingData: action.bookData, handlingBooking: false } });
        } else if (data.respCode === 100) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110252'
                }
            });
        } else if (data.respCode === 101) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110256'
                }
            });
        } else if (data.respCode === 102) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110253'
                }
            });
        } else if (data.respCode === 103) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110278'
                }
            });
        } else if (data.respCode === 3) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110032'
                }
            });
        } else if (data.respCode === 104) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110282'
                }
            });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
            yield put({ type: bookingAnonymousActionType.UPDATE_STATE, updateData: { handlingBooking: false } });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        yield put({ type: bookingAnonymousActionType.UPDATE_STATE, updateData: { handlingBooking: false } });
    }
}

function* bookingConfirmWaiting() {
    yield takeLatest(bookingAnonymousActionType.BOOK_CONFIRM_WAITING, fetchBookingConfirmWaiting);
}


function* getAppointmentReport() {
    while (true) {
        let { params } = yield take(bookingAnonymousActionType.GET_APPOINTMENT_REPORT);
        try {
            let url = params.reportType === 'Single' ? '/appointment/reportSingleSlip' : '/appointment/reportMultipleSlip';
            let { data } = yield call(axios.post, url, params.reportParam);
            if (data.respCode === 0) {
                yield put({
                    type: commonType.PRINT_START,
                    params: { base64: data.data }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }

        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* listRemarkCode() {
    while (true) {
        yield take(bookingAnonymousActionType.LIST_REMARK_CODE);
        try {
            let { data } = yield call(axios.get, '/appointment/listRemarkCode');
            if (data.respCode === 0) {
                yield put({
                    type: bookingAnonymousActionType.PUT_LIST_REMARK_CODE,
                    remarkCodeList: data.data
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

export const bookingAnonymousSaga = [
    requestData(),
    updateField(),
    getClinicList(),
    getEncounterTypeList(),
    appointmentBooking(),
    listTimeSlotForAppointmentBook(),
    listAppointment(),
    loadAppointmentBooking(),
    bookingConfirm(),
    bookingConfirmWaiting(),
    getAppointmentReport(),
    listRemarkCode()
];