import { take, call, put } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as formNameActionType from '../../../actions/IOE/formName/formNameActionType';
import * as commonType from '../../../actions/common/commonActionType';
// for temporary test
//get service list
function* requestFormName() {
    while (true) {
        let { params,callback } = yield take(formNameActionType.IOE_FORM_NAME_LIST);
        try {
                {
                        let { data } = yield call(axios.post, '/ioe/listIoeFormName',params);
                        if (data.respCode === 0) {
                            yield put({ type: formNameActionType.FILLING_FORM_NAME_LIST, fillingData: data});
                            callback&&callback(data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
                }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* requestRequestedBy() {

    while (true) {
        let { params,callback } = yield take(formNameActionType.IOE_REQUESTED_BY);
        try {
                {
                        let { data } = yield call(axios.post, '/ioe/listPatientByLoginClinicCd',params);
                        if (data.respCode === 0) {
                            yield put({ type: formNameActionType.FILLING_REQUESTED_BY, fillingData: data});
                            callback&&callback(data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
                }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}


export const formNameSaga = [
    requestFormName(),
    requestRequestedBy()
];
