import { take,call,put,select } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as types from '../../../actions/IOE/serviceProfile/serviceProfileActionType';
import * as commonTypes from '../../../actions/common/commonActionType';
import * as messageTypes from '../../../actions/message/messageActionType';
import {isNull,isEmpty,remove,keys,findIndex,toInteger} from 'lodash';
import * as ServiceProfileConstants from '../../../../constants/IOE/serviceProfile/serviceProfileConstants';

let generateTestItemsMap = (itemObj) => {
  let tempMap = new Map();
  if (Object.keys(itemObj).length > 0) {
    for (const groupName in itemObj) {
      if (Object.prototype.hasOwnProperty.call(itemObj,groupName)) {
        const items = itemObj[groupName];
        remove(items,item=>{
          return isNull(item);
        });
        tempMap.set(groupName,items);
      }
    }
  }
  return tempMap;
};

let transformItemsMap = (valObjs) => {
  let testItemsMap = new Map();
  let specimenItemsMap = new Map();
  let infoItemsMap = new Map();

  let valMap = new Map();
  valObjs.forEach(valObj => {
    if (ServiceProfileConstants.ITEM_CATEGORY_IOE_TYPE_SET.TEST.has(valObj.itemIoeType)) {
      valMap = testItemsMap;
    } else if (ServiceProfileConstants.ITEM_CATEGORY_IOE_TYPE_SET.SPECIMEN.has(valObj.itemIoeType)) {
      valMap = specimenItemsMap;
    } else if (ServiceProfileConstants.ITEM_CATEGORY_IOE_TYPE_SET.INFO.has(valObj.itemIoeType)) {
      valMap = infoItemsMap;
    }

    valMap.set(valObj.codeIoeFormItemId,{
      codeIoeFormItemId:valObj.codeIoeFormItemId,
      testGroup: valObj.testGroup,
      operationType: valObj.operationType,
      version: valObj.version,
      itemVal: valObj.frmItemTypeCd === ServiceProfileConstants.FORM_ITEM_TYPE.DROP_DOWN_LIST&&!!valObj.itemVal?toInteger(valObj.itemVal):valObj.itemVal,
      itemVal2: valObj.frmItemTypeCd2 === ServiceProfileConstants.FORM_ITEM_TYPE.DROP_DOWN_LIST&&!!valObj.itemVal2?toInteger(valObj.itemVal2):valObj.itemVal2,
      codeIoeFormId: valObj.codeIoeFormId,
      itemName: valObj.itemName,
      createdBy: valObj.createdBy,
      createdDtm: valObj.createdDtm,
      updatedBy: valObj.updatedBy,
      updatedDtm: valObj.updatedDtm,
      frmItemTypeCd: valObj.frmItemTypeCd,
      frmItemTypeCd2: valObj.frmItemTypeCd2,
      ioeTestTemplateId: valObj.ioeTestTemplateId, // template id
      ioeTestTemplateItemId: valObj.ioeTestTemplateItemId // template item id
    });
  });
  return {
    testItemsMap,
    specimenItemsMap,
    infoItemsMap
  };
};

let generateTemporaryStorageMap = (ioeFormMap,lab2FormMap,ioeTestTmplItemMap) => {
  let tempMap = new Map();
  if (!isEmpty(ioeTestTmplItemMap)) {
    for (const testGroup in ioeTestTmplItemMap) {
      if (Object.prototype.hasOwnProperty.call(ioeTestTmplItemMap,testGroup)) {
        let currentFormObj = ioeTestTmplItemMap[testGroup];
        let currentFormIds = keys(currentFormObj);
        let formId = parseInt(currentFormIds[0]);
        let targetLabId = null;
        for (let [labId, formIds] of lab2FormMap) {
          let index = findIndex(formIds,id => {
            return id === formId;
          });
          if (index!==-1) {
            targetLabId = labId;
          }
        }
        let tempMapObj = transformItemsMap(currentFormObj[formId]);
        let obj = {
          codeIoeFormId: formId,
          testGroup: parseInt(testGroup),
          labId:targetLabId,
          formShortName: ioeFormMap.get(formId).formShortName,
          testItemsMap: tempMapObj.testItemsMap,
          specimenItemsMap: tempMapObj.specimenItemsMap,
          infoItemsMap: tempMapObj.infoItemsMap
        };
        let timestamp = new Date().valueOf();
        tempMap.set(`${formId}_${timestamp}_${testGroup}`,obj);
      }
    }
  }

  return tempMap;
};

// get dialog framework list
function* getServiceProfileFrameworkList() {
  while (true) {
    let { params,callback } = yield take(types.GET_SERVICE_PROFILE_FRAMEWORK_LIST);
    try {
      let { data } = yield call(axios.get, '/ioe/loadServiceProfileDatas', params);
      if (data.respCode === 0) {
        let frameworkMap = new Map();
        let lab2FormMap = new Map();
        let ioeFormMap = new Map();
        let labOptions = [{label:'All',value:'All'}];  // default
        // lab
        data.data.forEach(lab => {
          let { codeIoeCd,codeLabValue,codeIoeFormList=[] } = lab;
          let labObj = {
            codeIoeCd,
            codeLabValue,
            formMap: new Map()
          };
          let tempFormIds = [];
          // form
          codeIoeFormList.forEach(form => {
            let { codeIoeFormId,formName,formShortName,testItems=null,specimenItems=null,infoItems=null } = form;
            let formObj = {
              codeIoeFormId,
              formName,
              formShortName,
              testItemsMap:!isNull(testItems)?generateTestItemsMap(testItems):new Map(),
              specimenItemsMap:!isNull(specimenItems)?generateTestItemsMap(specimenItems):new Map(),
              infoItemsMap:!isNull(infoItems)?generateTestItemsMap(infoItems):new Map()
            };
            tempFormIds.push(codeIoeFormId);
            labObj.formMap.set(codeIoeFormId,formObj);
            ioeFormMap.set(codeIoeFormId,{
              id:codeIoeFormId,
              formName,
              formShortName
            });
          });
          labOptions.push({label:codeIoeCd,value:codeIoeCd});
          lab2FormMap.set(codeIoeCd,tempFormIds);
          frameworkMap.set(codeIoeCd,labObj);
        });

        yield put({
          type: types.SERVICE_PROFILE_FRAMEWORK_LIST,
          frameworkMap: frameworkMap,
          lab2FormMap: lab2FormMap,
          ioeFormMap: ioeFormMap,
          labOptions: labOptions
        });
        callback&&callback(frameworkMap);
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.errMsg ? data.errMsg : 'Service error',
          data: data.data
        });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// get dialog dropdown list
function* getServiceProfildItemDropdownList() {
  while (true) {
    let { params,callback} = yield take(types.GET_SERVICE_PROFILE_DROPDOWN_LIST);
    try {
      let { data } = yield call(axios.get,'/ioe/loadCodeIoeFormItemDrops',params);
      if (data.respCode === 0) {
        let dropdownMap = new Map();
        if (isNull(data.data) !== null && data.data.length > 0) {
          data.data.forEach(form => {
            let optionMap = new Map();
            for (const subSetId in form.codeIoeFormItemDropMap) {
              if (Object.prototype.hasOwnProperty.call(form.codeIoeFormItemDropMap,subSetId)) {
                optionMap.set(subSetId,form.codeIoeFormItemDropMap[subSetId]);
              }
            }
            dropdownMap.set(form.codeIoeFormItemId,optionMap);
          });
        }
        yield put({
          type: types.SERVICE_PROFILE_DROPDOWN_LIST,
          dropdownMap: dropdownMap
        });
        callback&&callback(dropdownMap);
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.errMsg ? data.errMsg : 'Service error',
          data: data.data
        });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// save/update lab test grouping
function* saveServiceProfileTemplate() {
  while (true) {
    let { params,callback } = yield take(types.SAVE_SERVICE_PROFILE);
    let { favoriteType, innerEditTemplateDto } = params;
    try {
      let { data } = yield call(axios.post, '/ioe/saveServiceProfile', innerEditTemplateDto );
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      if (data.respCode === 0) {
        yield put({
          type:types.GET_SERVICE_PROFILE_LIST,
          params:{
            favoriteType
          }
        });
        callback&&callback(data);
      } else {
        if (!isNull(data.msgCode)) {
          yield put({
            type: messageTypes.OPEN_COMMON_MESSAGE,
            payload:{
              msgCode: data.msgCode
            }
          });
        } else {
          yield put({
            type: commonTypes.OPEN_ERROR_MESSAGE,
            error: data.message ? data.message : 'Service error'
          });
        }
      }
    } catch (error) {
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: error.message ? error.message : 'Service error',
          data: error
      });
    }
  }
}

// get lab test grouping template
function* getServiceProfileTemplate() {
  while (true) {
    let { params,callback} = yield take(types.GET_SERVICE_PROFILE_TEMPLATE);
    try {
      let { data } = yield call(axios.post,'/ioe/getServiceProfileById',params);
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      if (data.respCode === 0) {
        let { ioeTestTmplItemMap } = data.data;
        let lab2FormMap = yield select(state => state.serviceProfile.lab2FormMap);
        let ioeFormMap = yield select(state => state.serviceProfile.ioeFormMap);
        let storageMap = generateTemporaryStorageMap(ioeFormMap,lab2FormMap,ioeTestTmplItemMap);
        data.data.storageMap= storageMap;

        yield put({
          type: types.SERVICE_PROFILE_TEMPLATE,
          serviceProfileTemplate: data.data
        });
        callback&&callback(data.data);
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.errMsg ? data.errMsg : 'Service error',
          data: data.data
        });
      }
    } catch (error) {
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// get all items (text & specimen) for search
function* getTemplateAllItemsForSearch() {
  while (true) {
    let { params,callback} = yield take(types.GET_TEMPLATE_ALL_ITEMS_FOR_SEARCH);
    try {
      let { data } = yield call(axios.get,'/ioe/loadAllItemsForSearch',params);
      if (data.respCode === 0) {
        yield put({
          type: types.TEMPLATE_ALL_ITEMS_FOR_SEARCH,
          searchItemList: data.data
        });
        callback&&callback(data.data);
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.errMsg ? data.errMsg : 'Service error',
          data: data.data
        });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

//get LabTestGrouping list
function* getServiceProfileList() {
  while (true) {
    let { params,callback} = yield take(types.GET_SERVICE_PROFILE_LIST);
    try {
      let { data } = yield call(axios.post,'/ioe/listServiceProfileByType',params);
      if (data.respCode === 0) {
        yield put({
          type: types.SET_SERVICE_PROFILE_LIST,
          serviceProfileTemplateList: data.data
          });
        callback&&callback(data.data);
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.errMsg ? data.errMsg : 'Service error',
          data: data.data
        });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

//save LabTestGrouping list
function* saveServiceProfileList() {
  while (true) {
    let { params,callback} = yield take(types.SAVE_SERVICE_PROFILE_LIST);
    let { dtos } = params;
    try {
      let { data } = yield call(axios.post,'/ioe/saveServiceProfileList',dtos);
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      if (data.respCode === 0) {
        callback&&callback(data);
      } else {
        if (!isNull(data.msgCode)) {
          yield put({
            type: messageTypes.OPEN_COMMON_MESSAGE,
            payload:{
              msgCode: data.msgCode
            }
          });
        } else {
          yield put({
            type: commonTypes.OPEN_ERROR_MESSAGE,
            error: data.message ? data.message : 'Service error'
          });
        }
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

export const serviceProfileSaga = [
  getServiceProfileFrameworkList(),
  getServiceProfildItemDropdownList(),
  saveServiceProfileTemplate(),
  getServiceProfileTemplate(),
  getTemplateAllItemsForSearch(),
  getServiceProfileList(),
  saveServiceProfileList()
];
