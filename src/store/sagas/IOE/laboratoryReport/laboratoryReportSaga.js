import { take, call, put } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as laboratoryReportActionType from '../../../actions/IOE/laboratoryReport/laboratoryReportActionType';
import * as commonType from '../../../actions/common/commonActionType';
import * as messageAction from '../../../actions/message/messageActionType';

function* getIoeLaboratoryReportList() {
    while (true) {
        let { params,callback} = yield take(laboratoryReportActionType.GET_IOE_LABORATORY_REPORT_LIST);
        try {
                let { data } = yield call(axios.post, '/ioe/listIoeLaboratoryReportList',params);
                // console.log('data====%o',data);
                if (data.respCode === 0) {
                    callback&&callback(data);
                    // console.log('callback====%o',data);
                } else {
                    yield put({
                        type: commonType.OPEN_ERROR_MESSAGE,
                        error: data.errMsg ? data.errMsg : 'Service error',
                        data: data.data
                    });
                }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* getIoeLaboratoryReportVersionList() {
    while (true) {
        let { params,callback } = yield take(laboratoryReportActionType.GET_IOE_LABORATORY_REPORT_VERSION_LIST);
        try {
                        let { data } = yield call(axios.post, '/ioe/listIoeReportVersion',params);
                        // console.log('getIoeLaboratoryReportVersionList data====%o',data);
                        if (data.respCode === 0) {
                            // yield put({ type: laboratoryReportActionType.FILLING_FORM_NAME_LIST, fillingData: data});
                            callback&&callback(data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* getIoeLaboratoryReportPdf() {
    while (true) {
        let { params,callback } = yield take(laboratoryReportActionType.GET_IOE_LABORATORY_REPORT_PDF);
        try {
                        let { data } = yield call(axios.post, '/ioe/reportIoeEnquiry',params);
                        // console.log('getIoeLaboratoryReportPdf data====%o',data);
                        if (data.respCode === 0) {
                            // yield put({ type: laboratoryReportActionType.FILLING_FORM_NAME_LIST, fillingData: data});
                            callback&&callback(data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* saveIoeLaboratoryReportComment() {
    while (true) {
        let { params,callback } = yield take(laboratoryReportActionType.SAVE_IOE_LABORATORY_REPORT_COMMENT);
        try {
                        let { data } = yield call(axios.post, '/ioe/saveIoeReportComment',params);
                        if (data.respCode === 0) {
                            // yield put({ type: laboratoryReportActionType.FILLING_FORM_NAME_LIST, fillingData: data});
                            callback&&callback(data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* getIoeLaboratoryReportCommentList() {
    while (true) {
        let { params,callback } = yield take(laboratoryReportActionType.GET_IOE_LABORATORY_REPORT_COMMMENT_LIST);
        try {
                        let { data } = yield call(axios.post, '/ioe/listLaboratoryReportCommentList',params);
                        // console.log('getIoeLaboratoryReportCommentList data====%o',data);
                        if (data.respCode === 0) {
                            // yield put({ type: laboratoryReportActionType.FILLING_FORM_NAME_LIST, fillingData: data});
                            callback&&callback(data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* getPatientById() {
    while (true) {
        let { params } = yield take(laboratoryReportActionType.GET_PATINET_INFOMATION);
        try {
            let { data } = yield call(axios.post, '/patient/getPatient', { 'patientKey': params.patientKey });
            if (data.respCode === 0) {
                // console.log('getPatientById data====%o',data);
                yield put({ type: laboratoryReportActionType.PUT_PATINET_INFOMATION, fillingData: data.data});
            } else if (data.respCode === 9) {
                yield put({
                    type: messageAction.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110134'
                    }
                });
            } else if (data.respCode === 12) {
                yield put({
                    type: messageAction.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110135'
                    }
                });
            } else {
                yield put({ type: laboratoryReportActionType.PUT_PATINET_INFOMATION, fillingData: null});
                yield put({
                    type: messageAction.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getRequestDetailById() {
    while (true) {
        let { params,callback } = yield take(laboratoryReportActionType.GET_IOE_LABORATORY_REPORT_DETAIL);
        try {
                        let { data } = yield call(axios.post, '/ioe/getRequestDetailById',params);
                        // console.log('getRequestDetailById data====%o',data);
                        if (data.respCode === 0) {
                            callback&&callback(data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* updateIoeReportFollowUpStatus() {
    while (true) {
        let { params,callback } = yield take(laboratoryReportActionType.UPDATE_IOE_LABORATORY_FOLLOWUP_STATUS);
        try {
                        // console.log('updateIoeReportFollowUpStatus params====%o',params);
                        let { data } = yield call(axios.post, '/ioe/updateIoeReportFollowUpStatus',params);
                        // console.log('updateIoeReportFollowUpStatus data====%o',data);
                        if (data.respCode === 0) {
                            callback&&callback(data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}


export const laboratoryReportSaga = [
    getIoeLaboratoryReportList(),
    getIoeLaboratoryReportVersionList(),
    getIoeLaboratoryReportPdf(),
    saveIoeLaboratoryReportComment(),
    getIoeLaboratoryReportCommentList(),
    getPatientById(),
    getRequestDetailById(),
    updateIoeReportFollowUpStatus()
];