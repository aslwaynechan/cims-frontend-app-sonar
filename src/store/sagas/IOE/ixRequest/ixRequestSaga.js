import { take,call,put,select } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as types from '../../../actions/IOE/ixRequest/ixRequestActionType';
import * as commonTypes from '../../../actions/common/commonActionType';
import * as messageTypes from '../../../actions/message/messageActionType';
import {isNull,remove,toNumber} from 'lodash';
import * as utils from '../../../../views/jos/IOE/ixRequest/utils/ixUtils';

let generateItemsMap = (itemObj) => {
  let tempMap = new Map();
  if (Object.keys(itemObj).length > 0) {
    for (const groupName in itemObj) {
      if (Object.prototype.hasOwnProperty.call(itemObj,groupName)) {
        const items = itemObj[groupName];
        remove(items,item=>{
          return isNull(item);
        });
        tempMap.set(groupName,items);
      }
    }
  }
  return tempMap;
};

//get clinic list
function* getIxClinicList() {
  while (true) {
    let { params,callback } = yield take(types.GET_IX_CLINIC_LIST);
    try {
      let { data } = yield call(axios.post, '/common/listClinic', params);
      if (data.respCode === 0) {
        yield put({
          type: types.IX_CLINIC_LIST,
          clinicList: data.data
        });
        callback && callback(data.data);
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.errMsg ? data.errMsg : 'Service error',
          data: data.data
        });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// get framework list
function* getIxRequestFrameworkList() {
  while (true) {
    let { params,callback } = yield take(types.GET_IX_REQUEST_FRAMEWORK_LIST);
    try {
      let { data } = yield call(axios.get, '/ioe/ixRequest/forms', params);
      if (data.respCode === 0) {
        let frameworkMap = new Map();
        let lab2FormMap = new Map();
        let ioeFormMap = new Map();
        let labOptions = [{label:'All',value:'All'}];  // default
        // lab
        data.data.forEach(lab => {
          let { codeIoeCd,codeLabValue,codeIoeFormList=[] } = lab;
          let labObj = {
            codeIoeCd,
            codeLabValue,
            formMap: new Map()
          };
          let tempFormIds = [];
          // form
          codeIoeFormList.forEach(form => {
            let { codeIoeFormId,formName,formShortName,testItems=null,specimenItems=null,otherItems=null,questionItems=null } = form;
            let formObj = {
              codeIoeFormId,
              formName,
              formShortName,
              testItemsMap:!isNull(testItems)?generateItemsMap(testItems):new Map(),
              specimenItemsMap:!isNull(specimenItems)?generateItemsMap(specimenItems):new Map(),
              otherItemsMap:!isNull(otherItems)?generateItemsMap(otherItems):new Map(),
              questionItemsMap:!isNull(questionItems)?generateItemsMap(questionItems):new Map()
            };
            tempFormIds.push(codeIoeFormId);
            labObj.formMap.set(codeIoeFormId,formObj);
            ioeFormMap.set(codeIoeFormId,{
              id:codeIoeFormId,
              formName,
              formShortName,
              labId:codeIoeCd
            });
          });
          labOptions.push({label:codeIoeCd,value:codeIoeCd});
          lab2FormMap.set(codeIoeCd,tempFormIds);
          frameworkMap.set(codeIoeCd,labObj);
        });

        yield put({
          type: types.IX_REQUEST_FRAMEWORK_LIST,
          frameworkMap: frameworkMap,
          lab2FormMap: lab2FormMap,
          ioeFormMap: ioeFormMap,
          labOptions: labOptions
        });
        callback&&callback({
          frameworkMap: frameworkMap,
          lab2FormMap: lab2FormMap,
          ioeFormMap: ioeFormMap,
          labOptions: labOptions
        });
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.errMsg ? data.errMsg : 'Service error',
          data: data.data
        });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// get dropdown list
function* getIxRequestItemDropdownList() {
  while (true) {
    let { params,callback} = yield take(types.GET_IX_REQUEST_ITEM_DROPDOWN_LIST);
    try {
      let { data } = yield call(axios.get,'/ioe/loadCodeIoeFormItemDrops',params);
      if (data.respCode === 0) {
        let dropdownMap = new Map();
        if (isNull(data.data) !== null && data.data.length > 0) {
          data.data.forEach(form => {
            let optionMap = new Map();
            for (const subSetId in form.codeIoeFormItemDropMap) {
              if (Object.prototype.hasOwnProperty.call(form.codeIoeFormItemDropMap,subSetId)) {
                optionMap.set(subSetId,form.codeIoeFormItemDropMap[subSetId]);
              }
            }
            dropdownMap.set(form.codeIoeFormItemId,optionMap);
          });
        }
        yield put({
          type: types.IX_REQUEST_ITEM_DROPDOWN_LIST,
          dropdownMap: dropdownMap
        });
        callback&&callback(dropdownMap);
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.errMsg ? data.errMsg : 'Service error',
          data: data.data
        });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// get order list
function* getIxRequestOrderList() {
  while (true) {
    let { params,callback} = yield take(types.GET_IX_REQUEST_ORDER_LIST);
    let { patientKey,encounterId } = params;
    try {
      let { data } = yield call(axios.get,`/ioe/ixRequest/orders/${patientKey}/${encounterId}`);
      if (data.respCode === 0) {
        let tempStorageMap = new Map();
        if (data.data.length>0) {
          let ioeFormMap = yield select(state => state.ixRequest.ioeFormMap);
          data.data.forEach(orderObj => {
            let tempObj = {
              clinicCd: orderObj.clinicCd,
              codeIoeFormId: orderObj.codeIoeFormId,
              codeIoeRequestTypeCd: orderObj.codeIoeRequestTypeCd,
              requestUser: orderObj.requestUser,
              encounterId: orderObj.encounterId,
              patientKey: orderObj.patientKey,
              serviceCd: orderObj.serviceCd,
              createdBy: orderObj.createdBy,
              createdDtm: orderObj.createdDtm,
              updatedBy: orderObj.updatedBy,
              updatedDtm: orderObj.updatedDtm,
              ioeRequestId: orderObj.ioeRequestId,
              ioeRequestNumber: orderObj.ioeRequestNumber,
              requestDatetime: orderObj.requestDatetime,
              version: orderObj.version,
              operationType: orderObj.operationType,

              invldReason:orderObj.invldReason, // invalid
              isInvld: orderObj.isInvld, // invalid
              ivgRqstSeqNum: orderObj.ivgRqstSeqNum, // invalid
              outputFormPrinted: orderObj.outputFormPrinted, // invalid
              outputFormPrintedBy: orderObj.outputFormPrintedBy, // invalid
              outputFormPrintedDatetime: orderObj.outputFormPrintedDatetime, // invalid
              specimenCollectDatetime: orderObj.specimenCollectDatetime, // invalid
              specimenCollected: orderObj.specimenCollected, // invalid
              specimenCollectedBy: orderObj.specimenCollectedBy, // invalid
              specimenLabelPrinted: orderObj.specimenLabelPrinted, // invalid
              specimenLabelPrintedBy: orderObj.specimenLabelPrintedBy, // invalid
              specimenLabelPrintedDatetime: orderObj.specimenLabelPrintedDatetime, // invalid

              labId:ioeFormMap.get(orderObj.codeIoeFormId).labId,
              formShortName: ioeFormMap.get(orderObj.codeIoeFormId).formShortName
            };
            let valObj = utils.transformItemsObj(orderObj.ixRequestItemMap,orderObj.codeIoeFormId);
            tempObj.testItemsMap = valObj.testItemsMap;
            tempObj.specimenItemsMap = valObj.specimenItemsMap;
            tempObj.otherItemsMap = valObj.otherItemsMap;
            tempObj.questionItemsMap = valObj.questionItemsMap;
            tempStorageMap.set(`${orderObj.codeIoeFormId}_${Math.random()}`,tempObj);
          });
        }
        callback&&callback(tempStorageMap);
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.errMsg ? data.errMsg : 'Service error',
          data: data.data
        });
      }
    } catch (error) {
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// get item mapping
function* getIxRequestSpecificMapping() {
  while (true) {
    let { params,callback} = yield take(types.GET_IX_REQUEST_SPECIFIC_ITEM_MAPPING);
    try {
      let { data } = yield call(axios.get,'/ioe/ixRequest/specificItemMapping',params);
      if (data.respCode === 0) {
        let tempMap = new Map();
        if (Object.keys(data.data).length > 0) {
          for (const formId in data.data) {
            if (Object.prototype.hasOwnProperty.call(data.data,formId)) {
              const mappingObj = data.data[formId];
              if (Object.keys(mappingObj).length > 0) {
                let mappingMap = new Map();
                for (const targetQuestionItemId in mappingObj) {
                  mappingMap.set(toNumber(targetQuestionItemId),new Set(mappingObj[targetQuestionItemId]));
                }
                tempMap.set(toNumber(formId),mappingMap);
              }
            }
          }
        }

        yield put({
          type: types.IX_REQUEST_SPECIFIC_ITEM_MAPPING,
          itemMapping: tempMap
        });
        callback&&callback(tempMap);
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.errMsg ? data.errMsg : 'Service error',
          data: data.data
        });
      }
    } catch (error) {
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// save/update ix request order
function* saveIxRequestOrder() {
  while (true) {
    let { params,callback } = yield take(types.SAVE_IX_REQUEST_ORDER);
    let { operationType, innerSaveIxRequestDtos } = params;
    try {
      let { data } = yield call(axios.post, `/ioe/ixRequest/operation/${operationType}`, innerSaveIxRequestDtos );
      if (data.respCode === 0) {
        callback&&callback(data);
      } else {
        yield put({
          type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        if (!isNull(data.msgCode)) {
          yield put({
            type: messageTypes.OPEN_COMMON_MESSAGE,
            payload:{
              msgCode: data.msgCode
            }
          });
        } else {
          yield put({
            type: commonTypes.OPEN_ERROR_MESSAGE,
            error: data.message ? data.message : 'Service error'
          });
        }
      }
    } catch (error) {
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: error.message ? error.message : 'Service error',
          data: error
      });
    }
  }
}

// get all items (text & specimen) for search
function* getIxAllItemsForSearch() {
  while (true) {
    let { params,callback} = yield take(types.GET_IX_ALL_ITEMS_FOR_SEARCH);
    try {
      let { data } = yield call(axios.get,'/ioe/loadAllItemsForSearch',params);
      if (data.respCode === 0) {
        yield put({
          type: types.IX_ALL_ITEMS_FOR_SEARCH,
          searchItemList: data.data
        });
        callback&&callback(data.data);
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.errMsg ? data.errMsg : 'Service error',
          data: data.data
        });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// get all ix profile template
function* getAllIxProfileTemplate() {
  while (true) {
    let { params,callback} = yield take(types.GET_ALL_IX_PROFILE_TEMPLATE);
    try {
      let { data } = yield call(axios.get,'/ioe/ixRequest/templates',params);
      if (data.respCode === 0) {
        let ioeFormMap = yield select(state => state.ixRequest.ioeFormMap);
        let categoryMap = new Map();
        if (data.data.length>0) {
          data.data.forEach(category => {
            let categoryObj = {
              tmplTypeCd: category.tmplTypeCd,
              templateMap: new Map()
            };

            if (category.tmplList.length > 0) {
              category.tmplList.forEach(template => {
                let templateObj = {
                  ioeTestTemplateId:template.ioeTestTemplateId,
                  codeIoeTestTmplTypeCd:template.codeIoeTestTmplTypeCd,
                  templateName:template.templateName,
                  storageMap: new Map()
                };
                templateObj.storageMap = utils.transformTemplateOrderItemsObj(template.ioeTestTmplItemMap,ioeFormMap);
                categoryObj.templateMap.set(template.ioeTestTemplateId,templateObj);
              });
            }
            categoryMap.set(category.tmplTypeCd,categoryObj);
          });
        }
        yield put({
          type: types.ALL_IX_PROFILE_TEMPLATE,
          categoryMap: categoryMap
        });
        callback&&callback(categoryMap);
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.errMsg ? data.errMsg : 'Service error',
          data: data.data
        });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

export const ixRequestSaga = [
  getIxClinicList(),
  getIxRequestFrameworkList(),
  getIxRequestItemDropdownList(),
  getIxRequestOrderList(),
  getIxRequestSpecificMapping(),
  getIxAllItemsForSearch(),
  saveIxRequestOrder(),
  getAllIxProfileTemplate()
];