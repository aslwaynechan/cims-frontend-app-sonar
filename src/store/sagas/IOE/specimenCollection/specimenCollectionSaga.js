import { take,call,put } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as types from '../../../actions/IOE/specimenCollection/specimenCollectionActionType';
import * as commonTypes from '../../../actions/common/commonActionType';
// import * as messageTypes from '../../../actions/message/messageActionType';

// get IOE specimen collection list
function* getIoeSpecimenCollectionList() {
  while (true) {
    let {params,callback} = yield take(types.GET_IOE_SPECIMEN_COLLECTION_LIST);
    try {
      let { data } = yield call(axios.post, '/ioe/loadIoeRequestRecords', params);
      if (data.respCode === 0) {
        callback&&callback(data.data);
        yield put({
                  type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
                });
      } else {
        callback&&callback(data);
        yield put({
          type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
        });
      }
    } catch (error) {
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// save/update IOE specimen collection list
function* saveIoeSpecimenCollectionList() {
  while (true) {
    let {params,callback} = yield take(types.SAVE_IOE_SPECIMEN_COLLECTION_LIST);
    try {
      let { data } = yield call(axios.post, '/ioe/operateIoeRequests', params);
      if (data.respCode === 0) {
        // callback&&callback(data);
        if(params.operationType==='PL'){
          yield put({
            type: commonTypes.PRINT_START,
            params: { base64: data.data.reportData,callback:callback}
          });
        }
        else if(params.operationType==='POF'||params.operationType==='SPOF'){
          for (let index = 0; index <data.data.reportData.length; index++) {
            let reportData = data.data.reportData[index];
            yield put({
              type: commonTypes.PRINT_START,
              params: { base64: reportData,callback:callback}
            });
          }

        }
        else{
          callback&&callback(data);
        }

        yield put({
                  type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
                });
      } else {
        callback&&callback(data);
        yield put({
          type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
        });
      }
    } catch (error) {
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}



export const specimenCollectionSaga = [
  getIoeSpecimenCollectionList(),
  saveIoeSpecimenCollectionList()
];