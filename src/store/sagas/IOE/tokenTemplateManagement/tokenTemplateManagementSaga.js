import { take,call,put } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as types from '../../../actions/IOE/tokenTemplateManagement/tokenTemplateManagementActionType';
import * as commonTypes from '../../../actions/common/commonActionType';
import * as messageTypes from '../../../actions/message/messageActionType';
import {isNull} from 'lodash';


//get template list
function* getTokenTemplateList() {
  while (true) {
    let { params,callback} = yield take(types.GET_TOKEN_TMPL_LIST);
    try {
          {
            let { data } = yield call(axios.post,'/ioe/listTokenTemplate',params);
            if (data.respCode === 0) {
              yield put({
                type: types.TOKEN_TMPL_LIST,
                tokenTemplateList: data.data
                });
              callback&&callback(data.data);
            } else {
              yield put({
                type: commonTypes.OPEN_ERROR_MESSAGE,
                error: data.errMsg ? data.errMsg : 'Service error',
                data: data.data
              });
            }
          }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

function* saveTokenTemplateList() {
  while (true) {
    let { params,callback} = yield take(types.SAVE_TOKEN_TMPL_LIST);
    try {
          {
            let { data } = yield call(axios.post,'/ioe/saveTokenTmplateList',params);
            yield put({
              type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
            });
            if (data.respCode === 0) {
              callback&&callback(data);
            } else {
              if (!isNull(data.msgCode)) {
                yield put({
                  type: messageTypes.OPEN_COMMON_MESSAGE,
                  payload:{
                    msgCode: data.msgCode
                  }
                });
              } else {
                yield put({
                  type: commonTypes.OPEN_ERROR_MESSAGE,
                  error: data.message ? data.message : 'Service error'
                });
              }
            }
          }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

//get  instruction list
function* getInstructionList() {
  while (true) {
    let { params,callback} = yield take(types.GET_INSTRUCTION_LIST);
    try {
          {
            let { data } = yield call(axios.post,'/ioe/getTokenInsturcts',params);
            if (data.respCode === 0) {
              yield put({
                type: types.INSTRUCTION_LIST_RESULT,
                instructionListData: data.data
                });
              callback&&callback(data.data);
            } else {
              yield put({
                type: commonTypes.OPEN_ERROR_MESSAGE,
                error: data.errMsg ? data.errMsg : 'Service error',
                data: data.data
              });
            }
          }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

//save  instruction list
function* saveInstructionList() {
  while (true) {
    let { params,callback} = yield take(types.SAVE_INSTRUCTION_LIST);
    try {
          {
            let { data } = yield call(axios.post,'/ioe/saveTokenInsturcts',params);
            if (data.respCode === 0) {
              yield put({
                type: types.SAVE_INSTRUCTION_LIST_RESULT,
                instructionListData: data.data
                });
               callback&&callback(data);
            } else {
              callback&&callback(data);
            }
          }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

function* getTokenTmplById() {
  while (true) {
    let { params,callback} = yield take(types.GET_TOKEN_TMPL_OBJECT);
    try {
          {
            let { data } = yield call(axios.post,'/ioe/getTokenTmplById',params);
            if (data.respCode === 0) {
              callback&&callback(data);
            }else {
              yield put({
                type: commonTypes.OPEN_ERROR_MESSAGE,
                error: data.errMsg ? data.errMsg : 'Service error',
                data: data.data
              });
            }
          }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

function* getTokenInsturctsByName() {
  while (true) {
    let { params,callback} = yield take(types.GET_TOKEN_INSTRUCT_LIST);
    try {
          {
            let { data } = yield call(axios.post,'/ioe/getTokenInsturctsByName',params);
            if (data.respCode === 0) {
              callback&&callback(data);
            }else {
              yield put({
                type: commonTypes.OPEN_ERROR_MESSAGE,
                error: data.errMsg ? data.errMsg : 'Service error',
                data: data.data
              });
            }
          }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

function* saveTokenTmplate() {
  while (true) {
    let { params,callback} = yield take(types.SAVE_TOKEN_TEMPLAT);
    try {
      let { data } = yield call(axios.post,'/ioe/saveTokenTmplate',params);
      callback&&callback(data);
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

function* getCodeIoeFormItems() {
  while (true) {
    let { params,callback} = yield take(types.GET_TOKEN_FORM_ITEMS);
    try {
          {
            let { data } = yield call(axios.get,'/ioe/getCodeIoeFormItems',params);
            if (data.respCode === 0) {
              callback&&callback(data);
            }else {
              yield put({
                type: commonTypes.OPEN_ERROR_MESSAGE,
                error: data.errMsg ? data.errMsg : 'Service error',
                data: data.data
              });
            }
          }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}



export const tokenTemplateManagementSaga = [
  getTokenTemplateList(),
  saveTokenTemplateList(),
  getInstructionList(),
  saveInstructionList(),
  getTokenTmplById(),
  getTokenInsturctsByName(),
  saveTokenTmplate(),
  getCodeIoeFormItems()
];