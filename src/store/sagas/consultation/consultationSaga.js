import { takeLatest, call, put } from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as type from '../../actions/consultation/consultationActionType';
import * as commonType from '../../actions/common/commonActionType';
import * as messageType from '../../actions/message/messageActionType';


function* fetchGetPatientQueue(action) {
    try {
        let { data } = yield call(axios.post, '/appointment/listPatientQueue', action.params);
        if (data.respCode === 0) {
            yield put({ type: type.PUT_PATIENT_QUEUE, data: data.data });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* getPatientQueue() {
    yield takeLatest(type.GET_PATIENT_QUEUE, fetchGetPatientQueue);
}

export const consultationSaga = [
    getPatientQueue()
];