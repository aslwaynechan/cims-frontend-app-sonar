import { take,call,put } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as types from '../../../actions/MRAM/backgroundInformation/backgroundInformationActionType';
import * as commonTypes from '../../../actions/common/commonActionType';
import * as backgroundInformationConstant from '../../../../constants/MRAM/backgroundInformation/backgroundInformationConstant';

function generateFieldDefaultVal(mramId) {
  let val = '';
  switch (mramId) {
    case '2':
      val = backgroundInformationConstant.ALCOHOL[0].value;
      break;
    case '13':
      val = backgroundInformationConstant.ALCOHOL[0].value;
      break;
    case '15':
      val = backgroundInformationConstant.ALCOHOL[0].value;
      break;
    case '18':
      val = backgroundInformationConstant.ALCOHOL[0].value;
      break;
    case '20':
      val =backgroundInformationConstant.ALCOHOL[0].value;
      break;
    case '22':
      val = backgroundInformationConstant.ALCOHOL[0].value;
      break;
      case '23':
        val = backgroundInformationConstant.ALCOHOL[0].value;
      break;
      case '28':
        val = backgroundInformationConstant.ALCOHOL[0].value;
      break;
      case '34':
        val = backgroundInformationConstant.ALCOHOL[0].value;
      break;
    default:
      break;
  }
  return val;
}

function generateFieldValMap() {
  let backgroundInfoFieldValMap = new Map();
  //prepare
  for (let [key, value] of backgroundInformationConstant.MRAM_BACKGROUNDINFOMATION_ID_MAP) {
    Object.values(value).forEach(element => {
      // handle default value
      let val = generateFieldDefaultVal(element);
      let fieldId = `${key}_${element}`;
      backgroundInfoFieldValMap.set(fieldId,{
        operationType:null,
        version:null,
        value:val
      });
    });
  }
  return backgroundInfoFieldValMap;
}

function* saveBackgroundInformation() {
  while (true) {
    let {params,callback} = yield take(types.SAVE_BACKGROUND_INFO_DATA);
    try {
        let { data } = yield call(axios.post, '/mram/SaveMramBkgdInfo', params);
        if (data.respCode === 0) {
          yield put({ type: types.SAVE_BACKGROUND_INFO_RESULT, fillingData: data});
          callback&&callback(data.data);
        } else {
          yield put({
            type: commonTypes.OPEN_ERROR_MESSAGE,
            error: data.message ? data.message : 'Service error'
          });
        }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

function* saveDraftCarePlanInfo() {
  while (true) {
    let {params,callback} = yield take(types.SAVE_CAREPLAN_INFO_DATA);
    try {
      {
        console.log('params=========',params);
        let { data } = yield call(axios.post, '/mram/saveMramCarePlan', params);
        if (data.respCode === 0) {
          console.log(data);
          yield put({ type: types.SAVE_CAREPLAN_INFO_RESULT, fillingData: data});
          callback&&callback(data.data);
        } else {
          console.log(data);
          yield put({
            type: commonTypes.OPEN_ERROR_MESSAGE,
            error: data.message ? data.message : 'Service error'
          });
        }
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}


export const backgroundInformationSaga = [
  saveBackgroundInformation(),
  saveDraftCarePlanInfo()
];