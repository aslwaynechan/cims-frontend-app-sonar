import { take,call,put } from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as types from '../../actions/MRAM/mramActionType';
import * as eyesTypes from '../../actions/MRAM/eyes/eyesActionType';
import * as feetTypes from '../../actions/MRAM/feet/feetActionType';
import * as measurementAndLabTest from '../../actions/MRAM/measurementAndLabTest/measurementAndLabTestActionType';
import * as otherComplications from '../../actions/MRAM/otherComplications/otherComplicationsActionType';
import * as commonTypes from '../../actions/common/commonActionType';
import * as backgroundInformationType from '../../actions/MRAM/backgroundInformation/backgroundInformationActionType';
import * as carePlanActionType from '../../actions/MRAM/carePlan/carePlanActionType';import * as dietAssessmentType from '../../actions/MRAM/dietAssessment/dietAssessmentType';
import * as riskProfileActionType from '../../actions/MRAM/riskProfile/riskProfileActionType';
// init MRAM field value list (New record)
function* initMramFieldValueList() {
  while (true) {
    let {params,callback} = yield take(types.INIT_MRAM_FIELD_VALUE_LIST);
    try {
      // eyes init
      yield put({
        type:eyesTypes.INIT_MRAM_EYES_FIELD_VALUE,
        valDto:null
      });
      // feet init
      yield put({
        type:feetTypes.INIT_MRAM_FEET_FIELD_VALUE,
        valDto:null
      });
	   // measurement/LabTest init
       yield put({
        type:measurementAndLabTest.INIT_MRAM_MEASRUEMENTANDLABTEST_FIELD_VALUE,
        valDto:null
      });
       // otherComplications init
       yield put({
        type:otherComplications.INIT_MRAM_OTHERCOMPLICATIONS_FIELD_VALUE,
        valDto:null
      });
      // backgroundInformation init
      yield put({
        type:backgroundInformationType.INIT_MRAM_BACKGROUND_INFORMATION_FIELD_VALUE,
        valDto:null
      });
      yield put({
        type:types.MRAM_FIELD_VALUE_LIST,
        mramOriginObj:null
      });
      // care plan init
      yield put({
        type:carePlanActionType.INIT_MRAM_CARE_PLAN_FIELD_VALUE,
        valDto:null
      });
      // dietAssessment init
      yield put({
        type:dietAssessmentType.SET_DIETASSESSMENT_MAP,
        valDto:null
      });
      // riskProfile init
      yield put({
        type:riskProfileActionType.SET_RISKPRIFILE_MAP,
        valDto:null
      });
      callback&&callback();
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// get MRAM field value list
function* getMramFieldValueList() {
  while (true) {
    let {params,callback} = yield take(types.GET_MRAM_FIELD_VALUE_LIST);
    try {
      console.log('getMramFieldValueList params======= %o',params);
      let { data } = yield call(axios.post, '/mram/loadMramDetails', params);
      console.log('getMramFieldValueList data======= %o',data);
      if (data.respCode === 0) {
        let {
          bkgdInfoDto=null,
          measurementDto=null,
          eyesAssessmentDto=null,
          feetAssessmentDto=null,
          otherComplicationsDto=null,
          dietAssessmentDto=null,
          riskProfileDto=null,
          carePlanDto=null
        } = data.data;
        // eyes init
        yield put({
          type:eyesTypes.INIT_MRAM_EYES_FIELD_VALUE,
          valDto:eyesAssessmentDto
        });
        // feet init
        yield put({
          type:feetTypes.INIT_MRAM_FEET_FIELD_VALUE,
          valDto:feetAssessmentDto
        });
        // backgroundInformation init
        yield put({
          type:backgroundInformationType.INIT_MRAM_BACKGROUND_INFORMATION_FIELD_VALUE,
          valDto:bkgdInfoDto
        });
        // care plan init
        yield put({
 		      type:carePlanActionType.INIT_MRAM_CARE_PLAN_FIELD_VALUE,
          valDto:carePlanDto
        });
		    // dietAssessment init
        yield put({
          type:dietAssessmentType.SET_DIETASSESSMENT_MAP,
          valDto:dietAssessmentDto
        });
        // riskProfile init
        yield put({
          type:riskProfileActionType.SET_RISKPRIFILE_MAP,
          valDto:riskProfileDto
        });
        // measurement/LabTest init
        yield put({
          type:measurementAndLabTest.INIT_MRAM_MEASRUEMENTANDLABTEST_FIELD_VALUE,
          valDto:measurementDto
        });
        // otherComplications init
        yield put({
          type:otherComplications.INIT_MRAM_OTHERCOMPLICATIONS_FIELD_VALUE,
          valDto:otherComplicationsDto
        });
        yield put({
          type:types.MRAM_FIELD_VALUE_LIST,
          mramOriginObj:data.data
        });
        yield put({
          type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        callback&&callback(data.data);
        // yield put({
        //   type:types.MRAM_FIELD_VALUE_LIST,
        //   mramOriginObj:data.data
        // });


      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.message ? data.message : 'Service error'
        });
        yield put({
          type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
        });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
    }
  }
}

// get MRAM field value list
function* saveMramFieldValueList() {
  while (true) {
    let {params,callback} = yield take(types.SAVE_MRAM_FIELD_VALUE_LIST);
    try {
      console.log('params======= %o',params);
      let { data } = yield call(axios.post, '/mram/saveAll', params);
      console.log('data======= %o',data);
      if (data.respCode === 0) {
        yield put({
          type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
        });

        callback&&callback(data);
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
    }
  }
}

function* previewReportPatient() {
  while (true) {
    let {params,callback} = yield take(types.GET_PREVIEW_REPORT_PATIENT_DATA);
    try {
      console.log('params======= %o',params);
      let { data } = yield call(axios.post, '/mram/reportMramPatientReport', params);
      console.log('data======= %o',data);
      if (data.respCode === 0) {
        yield put({
          type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        callback&&callback(data.data.reportData);
      } else {
        yield put({
          type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        callback&&callback(data);

      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
    }
  }
}

function* previewReportDoctor() {
  while (true) {
    let {params,callback} = yield take(types.GET_PREVIEW_REPORT_DOCTOR_DATA);
    try {
      console.log('params======= %o',params);
      let { data } = yield call(axios.post, '/mram/reportMramReport', params);
      console.log('previewReportDoctor data======= %o',data);
      if (data.respCode === 0) {
        yield put({
          type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        callback&&callback(data.data.reportData);
      } else {
        yield put({
          type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        callback&&callback(data);

      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
    }
  }
}



export const mramSaga = [
  initMramFieldValueList(),
  getMramFieldValueList(),
  saveMramFieldValueList(),
  previewReportPatient(),
  previewReportDoctor()
];