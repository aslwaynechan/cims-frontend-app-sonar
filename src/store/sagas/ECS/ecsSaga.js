import { select, take, call, put, race, fork, debounce, delay  } from 'redux-saga/effects';
import * as ecsActionType from '../../actions/ECS/ecsActionType';
import * as registrationType from '../../actions/registration/registrationActionType';
import * as patientActionType from '../../actions/patient/patientActionType';
import * as ecsAction from '../../actions/ECS/ecsAction';
import * as ecsService from '../../../services/ECS/ecsService';
import * as commonAction from '../../actions/common/commonAction';
import { openCommonMessage } from '../../actions/message/messageAction';
import * as EcsUtilities from '../../../utilities/ecsUtilities';
import _ from 'lodash';
import * as ecsReducer from '../../reducers/ECS/ecsReducer';
import * as commonUtilities from '../../../utilities/commonUtilities';
import Enum from '../../../enums/enum';

const getSelectedEcsState = state => state.ecs.selectedPatientEcsStatus;
const getSelectedOcsssState = state => state.ecs.selectedPatientMwecsStatus;
const getSelectedMwecsState = state => state.ecs.selectedPatientMwecsStatus;

const getRegSummaryEcsStatus = state => state.ecs.regSummaryEcsStatus;
const getRegSummaryOcsssStatus = state => state.ecs.regSummaryOcsssStatus;
const getRegSummaryMwecsStatus = state => state.ecs.regSummaryMwecsStatus;

const getPatientSummaryEcsStatus = state => state.ecs.patientSummaryEcsStatus;
const getPatientSummaryOcsssStatus = state => state.ecs.patientSummaryOcsssStatus;
const getPatientSummaryMwecsStatus = state => state.ecs.patientSummaryMwecsStatus;

const getClinicConfig = state => state.common.clinicConfig;
const getClinic = state => state.login.clinic;
const getService = state => state.login.service;

const getRegistrationState = state => state.registration.patientBaseInfo;
const getPateintSummaryState = state => state.registration.patientById;
const getPatientState = state => state.patient.patientInfo;
const ECS_SUCCESS_MSG_CD = '130000';
const ECS_UNAVAILABLE_MSG_CD = '130001';
const ECS_UNKNOWN_MSG_CD = '130099';
const OCSSS_OPEN_MSG_CD = '130010';
const OCSSS_VALID_MSG_CD = '130011';
const OCSSS_INVALID_MSG_CD = '130012';
const OCSSS_UNAVAILABLE_MSG_CD = '130013';
const MWECS_SUCCESS_MSG_CD = '130030';
const MWECS_INVALID_MSG_CD = '130031';
const MWECS_UNAVAILABLE_MSG_CD = '130032';

const TIMEOUT_RESPONSE_CD = 199;

function* syncRegPatientToPatientSummary(){
    let patientEcsInReg = yield select(getRegSummaryEcsStatus);
    let patientOcsssInReg = yield select(getRegSummaryOcsssStatus);
    let patientMwecsInReg = yield select(getRegSummaryMwecsStatus);

    yield put(ecsAction.setEcsPatientStatusInPatientSummary(patientEcsInReg));
    yield put(ecsAction.setOcsssPatientStatusInPatientSummary(patientOcsssInReg));
    yield put(ecsAction.setMwecsPatientStatusInPatientSummary(patientMwecsInReg));
}


function* syncPatientSummaryToRegPatient(){
    let ecsInSum = yield select(getPatientSummaryEcsStatus);
    let ocsssInSum = yield select(getPatientSummaryOcsssStatus);

    yield put(ecsAction.setEcsPatientStatusInRegPage(ecsInSum));
    yield put(ecsAction.setOcsssPatientStatusInRegPage(ocsssInSum));
}

function* syncPatientSummaryToSelectedPateint(){
    let ecsInSum = yield select(getPatientSummaryEcsStatus);
    let ocsssInSum = yield select(getPatientSummaryOcsssStatus);
    let mwecsInSum = yield select(getPatientSummaryMwecsStatus);
    yield put(ecsAction.setEcsPatientStatus(ecsInSum));
    yield put(ecsAction.setOcsssPatientStatus(ocsssInSum));
    yield put(ecsAction.setMwecsPatientStatus(mwecsInSum));
}

function* syncSelectedPateintToPatientSummary(){
    let ecsInSelected = yield select(getSelectedEcsState);
    let ocsssInSelected = yield select(getSelectedOcsssState);
    let mwecsInSelected = yield select(getSelectedMwecsState);

    yield put(ecsAction.setEcsPatientStatusInPatientSummary(ecsInSelected));
    yield put(ecsAction.setOcsssPatientStatusInPatientSummary(ocsssInSelected));
    yield put(ecsAction.setMwecsPatientStatusInPatientSummary(mwecsInSelected));
}

function* resetRegPage(){
    while(true){
        yield take(registrationType.RESET_ALL);
        yield put(ecsAction.resetEcsPatientStatus(ecsActionType.SET_REG_ECS_PATIENT_STATUS));
        yield put(ecsAction.resetOcsssPatientStatus(ecsActionType.SET_REG_OCSSS_PATIENT_STATUS));
        yield put(ecsAction.resetMwecsPatientStatus(ecsActionType.SET_REG_MWECS_PATIENT_STATUS));
    }
}

function* regPageEnterPatientSummarySyncControl(){
    while(true){
        //only run if insert or update patient
        let {updateData} = yield take(registrationType.UPDATE_STATE);
        if(updateData.isOpenReview){
            yield syncRegPatientToPatientSummary();
        }
    }
}

function* patientSummaryEnterPatientRelatedPageSyncControl(){
    while(true){
        yield take(patientActionType.PUT_PATIENT_INFO);

        let patientSummaryPatient = yield select(getPateintSummaryState);
        let selectedPatient = yield select(getPatientState);

        if(patientSummaryPatient.patientKey === selectedPatient.patientKey){
            yield syncPatientSummaryToSelectedPateint();
        }
    }
}

function* singleSyncControl(regActionType, patientSummaryActionType, selectedActionType){
    let {reg, sum, selected} = yield race(
            {
                reg: take(regActionType),
                sum: take(patientSummaryActionType),
                selected: take(selectedActionType)
            }
        );

        if(!reg){
            let patientSummaryPatient = yield select(getPateintSummaryState);
            let selectedPatient = yield select(getPatientState);
            const patientSummaryAndSelectedOneIsSame = selectedPatient && patientSummaryPatient && patientSummaryPatient.patientKey === selectedPatient.patientKey;
            if(sum){
                yield syncPatientSummaryToRegPatient();

                if(patientSummaryAndSelectedOneIsSame){
                   yield syncPatientSummaryToSelectedPateint();
                }
            }

            if(selected){
                if(patientSummaryAndSelectedOneIsSame){
                   yield syncSelectedPateintToPatientSummary();
                }
            }
        }

}

function* openOcsssDialog(){
    while(true){
        let {params, callback, callbackAction, checkAction} = yield take(ecsActionType.OPEN_OCSSS_DIALOG);

        let messagePayload = {
            msgCode: OCSSS_OPEN_MSG_CD,
            showSnackbar: false,
            btnActions: {
              btn1Click: () => {
                checkAction(
                    params,
                    callback,
                    callbackAction);
              }
            }
          };
          yield put(openCommonMessage(messagePayload));
    }
}

function* openCloseMwecsDialog(){
    while(true){
        let {open, close} = yield race({
            open: take(ecsActionType.OPEN_MWECS_DIALOG),
            close: take(ecsActionType.CLOSE_MWECS_DIALOG)
        });

        let isOpen = true;
        let callback = null;
        if(close){
            isOpen = false;
            callback = close.callback;
        }else{
            callback = open.callback;
        }

        if(isOpen){
            yield put(ecsAction.setMwecsInput({...open.params, callbackAction: open.callbackAction}));
        }
        yield put(ecsAction.setMwecsActive(isOpen));

        if(callback){
            callback();
        }

        yield put(ecsAction.setMwecsOpenDialog(isOpen));
    }
}

function* openCloseEcsDialog(){
    while(true){
        let {open, close} = yield race({
            open: take(ecsActionType.OPEN_ECS_DIALOG),
            close: take(ecsActionType.CLOSE_ECS_DIALOG)
        });

        let isOpen = true;
        let callback = null;

        if(close){
            isOpen = false;
            callback = close.callback;
        }else{
            callback = open.callback;
        }

        if(isOpen){
            let ecsCurrentInput = yield select(state => state.ecs.ecsDialogInput);
            let inputParams = {...ecsCurrentInput, ...open.inputParams};
            yield put(ecsAction.setEcsInput({ ...inputParams , callbackAction: open.callbackAction}));
        }

        if(isOpen){
            yield put(ecsAction.setEcsActive(isOpen));
            if(callback){
                callback();
            }
            yield put(ecsAction.setEcsOpenDialog(isOpen));
        }else{
            yield put(ecsAction.setEcsOpenDialog(isOpen));
            if(callback){
                callback();
            }
            yield put(ecsAction.setEcsActive(isOpen));
        }

    }
}


function* handleUnknowError(msgCode, desc){
    yield put(openCommonMessage({
        msgCode: msgCode,
        showSnackbar: false,
        params: [{ name: 'errorCode', value: msgCode}, { name: 'errorDesc', value: desc}]
    }));
}

function* handleKnownError(ecsErrorCode, desc){
    yield put(openCommonMessage({
        msgCode: ECS_UNKNOWN_MSG_CD,
        showSnackbar: false,
        params: [{ name: 'errorCode', value: ecsErrorCode}, { name: 'errorDesc', value: desc}]
    }));
}


function* regPageResetControl() {
    let lastEcsResult = yield select(getRegSummaryEcsStatus);
    let lastOcsssResult = yield select(getRegSummaryOcsssStatus);
    let lastPatientInfo = yield select(getRegistrationState);
    while (true) {
        let {resetAll,putPatient,checkEcs,checkOcsss, onBlur} = yield race({
            resetAll: take(registrationType.RESET_ALL),
            putPatient: take(registrationType.PUT_PATIENT_BY_ID),
            checkEcs: take(ecsActionType.SET_REG_ECS_PATIENT_STATUS),
            checkOcsss: take(ecsActionType.SET_REG_OCSSS_PATIENT_STATUS),
            onBlur: take(ecsActionType.REG_PAGE_KEY_FIELD_ON_BLUR)
        });

        if(checkEcs){
            lastEcsResult = yield select(getRegSummaryEcsStatus);
            lastPatientInfo = yield select(getRegistrationState);
        }

        if(checkOcsss){

            lastOcsssResult = yield select(getRegSummaryOcsssStatus);
            lastPatientInfo = yield select(getRegistrationState);
        }

        if(putPatient || resetAll || onBlur){
            lastPatientInfo = yield select(getRegistrationState);
            lastEcsResult = yield select(getRegSummaryEcsStatus);
            lastOcsssResult = yield select(getRegSummaryOcsssStatus);
            if( EcsUtilities.isPatientEcsRelatedFieldChangedByStore(lastEcsResult, lastPatientInfo)){
                yield put(ecsAction.resetEcsPatientStatus(ecsActionType.SET_REG_ECS_PATIENT_STATUS));
            }

            if( EcsUtilities.isPatientEcsRelatedFieldChangedByStore(lastOcsssResult, lastPatientInfo)){
                yield put(ecsAction.resetOcsssPatientStatus(ecsActionType.SET_REG_OCSSS_PATIENT_STATUS));
            }

            lastEcsResult = yield select(getRegSummaryEcsStatus);
            lastOcsssResult = yield select(getRegSummaryOcsssStatus);
        }
    }
}

function* resetForSelectedPatient(){
    yield put(ecsAction.resetEcsPatientStatus(ecsActionType.SET_ECS_PATIENT_STATUS));
    yield put(ecsAction.resetOcsssPatientStatus(ecsActionType.SET_OCSSS_PATIENT_STATUS));
    yield put(ecsAction.resetMwecsPatientStatus(ecsActionType.SET_MWECS_PATIENT_STATUS));
}

function* ecsResetControl(){
    while(true){
        let {resetType} = yield take(ecsActionType.RESET_ECS_PATIENT_STATUS);
        yield put({
            type: resetType,
            ecsPatientStatus: _.cloneDeep(ecsReducer.initSelectedPatientEcsStatus)
        });
        yield put(
            ecsAction.setEcsInput(_.cloneDeep(ecsReducer.initEcsDialogInput))
        );
    }
}

function* ocsssResetControl(){
    while(true){
        let {resetType} = yield take(ecsActionType.RESET_OCSSS_PATIENT_STATUS);
        yield put({
            type: resetType,
            ocsssPatientStatus: _.cloneDeep(ecsReducer.initSelectedPatientOcsssStatus)
        });
        yield put(
            ecsAction.setOcsssInput(_.cloneDeep(ecsReducer.initOcsssDialogInput))
        );
    }
}

function* mwecsResetControl(){
    while(true){
        let {resetType} = yield take(ecsActionType.RESET_MWECS_PATIENT_STATUS);
        yield put({
            type: resetType,
            mwecsPatientStatus: _.cloneDeep(ecsReducer.initSelectedPatientMwecsStatus)
        });
        yield put(
            ecsAction.setMwecsInput(_.cloneDeep(ecsReducer.initMwecsDialogInput))
        );
    }
}

function* patientResetAll(){
    while(true){
        yield take(patientActionType.RESET_ALL);
        yield resetForSelectedPatient();
    }
}

function* checkEcs(){
    while(true){
        let  { params, hkic, callback, callbackAction } = yield take(ecsActionType.CHECK_ECS);

        //change asscoicated hkid in input store
        if(params && params.checkType === 'N'){//is assoicated checking
            let ecsCurrentInput = yield select(state => state.ecs.ecsDialogInput);
            let inputParams = {...ecsCurrentInput, associatedHkic: params.hkid};
            yield put(ecsAction.setEcsInput({ ...inputParams }));
        }

        try {
            yield put(commonAction.openCommonCircular());
            let {data} = yield call(ecsService.checkEcs, params);
            if (data.respCode === 0) {
                let newEcsState = ecsService.transformEcsResponseDataToReduxState(data.data.checkingResult.data, data.data.lastCheckedTime, params.checkType === 'N', hkic);

                if(callbackAction){
                    yield fork(singleSyncControl, ecsActionType.SET_REG_ECS_PATIENT_STATUS, ecsActionType.SET_PATIENT_SUMMARY_ECS_PATIENT_STATUS, ecsActionType.SET_ECS_PATIENT_STATUS);
                    yield put(
                        callbackAction(newEcsState)
                    );
                }
                callback && callback(newEcsState);

                if(data.data.checkingResult && data.data.checkingResult.success){
                    yield put(openCommonMessage({
                        msgCode: ECS_SUCCESS_MSG_CD,
                        showSnackbar: true
                    }));
                } else {
                    yield handleUnknowError(ECS_UNKNOWN_MSG_CD, data.errMsg);
                }

            } else if (data.respCode === TIMEOUT_RESPONSE_CD) {
                yield put(openCommonMessage({
                    msgCode: ECS_UNAVAILABLE_MSG_CD,
                    showSnackbar: true
                }));
            } else {
                yield handleUnknowError(ECS_UNKNOWN_MSG_CD, data.errMsg ? 'Error Message: ' + data.errMsg : 'Service is unavailable.');

            }

        } catch (error) {
            yield handleUnknowError(ECS_UNKNOWN_MSG_CD, 'Service is unavailable.' + ' ' + (error?'Error Message: ' + error.toString():''));
        } finally {
            yield put(commonAction.closeCommonCircular());
        }
    }
}

function* checkOcsss(){
    while(true){
        let  { params, callback, callbackAction } = yield take(ecsActionType.CHECK_OCSSS);

        try {
            yield put(commonAction.openCommonCircular());

            let {data} = yield call(ecsService.checkOcsss, params);

            if (data.respCode === 0) {
            let newOcsssState = ecsService.transformOcsssResponseDataToReduxState(data.data.checkingResult.data, data.data.lastCheckedTime, params.hkid);

                if(callbackAction){
                    yield fork(singleSyncControl, ecsActionType.SET_REG_OCSSS_PATIENT_STATUS, ecsActionType.SET_PATIENT_SUMMARY_OCSSS_PATIENT_STATUS, ecsActionType.SET_OCSSS_PATIENT_STATUS);

                    yield put(
                        callbackAction(newOcsssState)
                    );
                }

                callback && callback(newOcsssState);

                if(newOcsssState.isValid){

                    yield put(openCommonMessage({
                        msgCode: OCSSS_VALID_MSG_CD,
                        showSnackbar: true
                    }));
                }else if (newOcsssState.checkingResult !== 'E'){
                    yield put(openCommonMessage({
                        msgCode: OCSSS_INVALID_MSG_CD,
                        showSnackbar: true
                    }));
                }else { // === 'E'
                    yield handleKnownError( newOcsssState.checkingResult, newOcsssState.errorMessage);
                }
            } else if (data.respCode === TIMEOUT_RESPONSE_CD) {
                yield put(openCommonMessage({
                    msgCode: OCSSS_UNAVAILABLE_MSG_CD,
                    showSnackbar: true
                }));
            } else {
                yield handleUnknowError(ECS_UNKNOWN_MSG_CD, data.errMsg ? data.errMsg : 'Service is unavailable.');
            }
        } catch (error) {
            yield handleUnknowError(ECS_UNKNOWN_MSG_CD, 'Service is unavailable.' + ' ' + (error?'Error Message: ' + error.toString():''));
        } finally {
            yield put(commonAction.closeCommonCircular());
        }
    }
}

function* checkMwecs(){
    while(true){
        let { params, callBack, callbackAction } = yield take(ecsActionType.CHECK_MWECS);

        try {
            yield put(commonAction.openCommonCircular());
            let {data} = yield call(ecsService.checkMwecs, params);
            if (data.respCode === 0) {
            let newMwecsState = ecsService.transformMwecsResponseDataToReduxState(data.data.checkingResult.data, data.data.lastCheckedTime, params.idNumber);


                if(callbackAction){
                    yield fork(singleSyncControl, ecsActionType.SET_REG_MWECS_PATIENT_STATUS, ecsActionType.SET_PATIENT_SUMMARY_MWECS_PATIENT_STATUS, ecsActionType.SET_MWECS_PATIENT_STATUS);

                    yield put(
                        callbackAction(newMwecsState)
                    );
                }

                callBack && callBack(newMwecsState);

                if(newMwecsState.isValid){

                    yield put(openCommonMessage({
                        msgCode: MWECS_SUCCESS_MSG_CD,
                        showSnackbar: true
                    }));
                } else if(newMwecsState.result !== 'E'){
                    yield put(openCommonMessage({
                        msgCode: MWECS_INVALID_MSG_CD,
                        showSnackbar: true
                    }));
                } else { // === 'E'
                    yield handleKnownError( newMwecsState.errorCode, newMwecsState.errorMessage);
                }
            } else if (data.respCode === TIMEOUT_RESPONSE_CD) {
                yield put(openCommonMessage({
                    msgCode: MWECS_UNAVAILABLE_MSG_CD,
                    showSnackbar: true
                }));
            } else {
                yield handleUnknowError(ECS_UNKNOWN_MSG_CD, data.errMsg ? data.errMsg : 'Service is unavailable.');
            }
        } catch (error) {
            yield handleUnknowError(ECS_UNKNOWN_MSG_CD, 'Service is unavailable.' + ' ' + (error?'Error Message: ' + error.toString():''));
        } finally {
            yield put(commonAction.closeCommonCircular());
        }
    }
}

function* refreshEcsServiceStatus() {
    while (true) {
        yield take(ecsActionType.REFRESH_STATUS);
        const clinicConfig = yield select(getClinicConfig);
        const currentClinic = yield select(getClinic);
        const currentService = yield select(getService);

        const clinicCd = currentClinic.clinicCd;
        const serviceCd = currentService.serviceCd;

        let where = { serviceCd: serviceCd, clinicCd: clinicCd};
        const isActive = configObj => configObj && configObj.configValue === 'A';
        const isTrue = configObj => configObj && configObj.configValue === 'Y';

        const ecsState =  isActive(commonUtilities.getHighestPriorityConfig(Enum.ECS_CLINIC_CONFIG_KEY.ECS_SERVICE_STATUS, clinicConfig, where));
        const ocsssState = isActive(commonUtilities.getHighestPriorityConfig(Enum.ECS_CLINIC_CONFIG_KEY.OCSSS_SERVICE_STATUS, clinicConfig, where));
        const mwecsState = isActive(commonUtilities.getHighestPriorityConfig(Enum.ECS_CLINIC_CONFIG_KEY.MWECS_SERVICE_STATUS, clinicConfig, where));

        const showEcsBtnInBooking = isTrue(commonUtilities.getHighestPriorityConfig(Enum.ECS_CLINIC_CONFIG_KEY.SHOW_ECS_BTN_IN_BOOKING, clinicConfig, where));

        yield put({
            type: ecsActionType.SET_STATUS,
            data: {
                ecsServiceStatus: ecsState,
                ocsssServiceStatus: ocsssState,
                mwecsServiceStatus: mwecsState,
                showEcsBtnInBooking: showEcsBtnInBooking
            }
        });

    }
}

export const ecsSaga = [
    resetRegPage(),
    regPageEnterPatientSummarySyncControl(),
    patientSummaryEnterPatientRelatedPageSyncControl(),
    regPageEnterPatientSummarySyncControl(),
    ecsResetControl(),
    ocsssResetControl(),
    mwecsResetControl(),
    openOcsssDialog(),
    openCloseMwecsDialog(),
    openCloseEcsDialog(),
    regPageResetControl(),
    patientResetAll(),
    refreshEcsServiceStatus(),
    checkEcs(),
    checkOcsss(),
    checkMwecs()
];