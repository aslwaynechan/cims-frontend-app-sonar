import { take, call, put } from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as mramHistoryActionType from '../../actions/MRAM/mramHistory/mramHistoryActionType';
import * as commonType from '../../actions/common/commonActionType';
// for temporary test
//get service list
function* requestHistoryService() {
    while (true) {
        let { params,callback } = yield take(mramHistoryActionType.SAVE_MRAM_HISTORY_DATA);
        try {
                {
                        let { data } = yield call(axios.post, '/mram/loadMramData',params);
                        if (data.respCode === 0) {
                            // yield put({ type: mramHistoryActionType.SAVE_MRAM_HISTORY_RESULT});
                            callback&&callback(data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
                }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* deleteHistoryService() {
    while (true) {
        let { params,callback } = yield take(mramHistoryActionType.DELETE_MRAM_HISTORY_DATA);
        try {
                {
                        let { data } = yield call(axios.post, '/mram/deleteMram',params);
                        console.log(data);
                        if (data.respCode === 0) {
                            // yield put({ type: mramHistoryActionType.SAVE_MRAM_HISTORY_RESULT});
                            callback&&callback(data);
                            yield put({
                                type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
                              });
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                            yield put({
                                type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
                              });
                        }
                }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
            yield put({
                type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
              });
        }
    }
}

export const mramHistorySaga = [
    requestHistoryService(),
    deleteHistoryService()
];