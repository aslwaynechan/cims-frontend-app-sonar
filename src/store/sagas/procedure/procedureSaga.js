import { take, call, put,takeLatest} from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as procedureActionType from '../../actions/procedure/procedureActionType';
import * as commonType from '../../actions/common/commonActionType';
import * as messageTypes from '../../actions/message/messageActionType';
import { isNull } from 'lodash';





function* requestTemplateList() {
    while (true) {
        let { params,callback } = yield take(procedureActionType.PROCEDURE_REQUEST_DATA);
        try {
                {
                        let { data } = yield call(axios.get, '/diagnosis/listProcedureTmplGroups',params);

                        if (data.respCode === 0) {
                            yield put({ type: procedureActionType.PROCEDURE_LIST_DATA, fillingData: data});
                            callback&&callback(data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
                }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* saveEditTemplateList() {
    while (true) {
        let { params ,callback} = yield take(procedureActionType.SAVE_PROCEDURE_TEMPLATE_DATA);
        try {
                {
                        let { data } = yield call(axios.post, '/diagnosis/saveProcedureTmpls',params);
                        if (data.respCode === 0) {
                            callback&&callback(data);
                        } else {
                            if(data.msgCode!==undefined&&data.msgCode!==null){
                                callback&&callback(data);
                            }
                            else{
                                 yield put({
                                    type: commonType.OPEN_ERROR_MESSAGE,
                                    error: data.errMsg ? data.errMsg : 'Service error',
                                    data: data.data
                                 });
                                }
                        }
                }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}


function* saveTemplateList() {
    while (true) {
        let { params,callback } = yield take(procedureActionType.SAVE_PROCEDURE_DATA);
        try {
                {
                        let { data } = yield call(axios.post, '/diagnosis/saveProcedureTemplateGroup',params);
                        yield put({
                          type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
                        });
                        if (data.respCode === 0) {
                            yield put({ type: procedureActionType.SAVE_PROCEDURE_RESULT, fillingData: data});
                            callback&&callback(data);
                        } else {
                            callback&&callback(data);
                        }
                }
        } catch (error) {
          yield put({
            type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
          });
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* getTermDisplayList() {
    while (true) {
        let { params,callback} = yield take(procedureActionType.GET_TERMDISPLAYLIST_DATA);
        try {
                {
                        let { data } = yield call(axios.post, '/diagnosis/listTerminologyCodeDataByName',params);
                        if (data.respCode === 0) {
                            // yield put({ type: procedureActionType.PUT_TERMDISPLAYLIST_DATA, fillingData: data.data});
                            callback&&callback(data.data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
                }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* getEditTemplateList() {
    while (true) {
        let { params,callback} = yield take(procedureActionType.GET_EDITTEMPLATELIST_DATA);
        try {
                {
                        let { data } = yield call(axios.post, '/diagnosis/listProcedureTmplsById',params);
                        if (data.respCode === 0) {
                            // yield put({ type: procedureActionType.PUT_EDITTEMPLATELIST_DATA, fillingData: data.data});
                            callback&&callback(data.data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
                }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* getConfig() {
    while (true) {
        let {configParams,callback} = yield take(procedureActionType.GET_CONFIG_DATA);
        let {key} = configParams;
        try {
                {
                        let { data } = yield call(axios.get,`clinical-note/sysConfig/${key}`);
                        if (data.respCode === 0) {
                            callback&&callback(data.data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
                }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

// get patient input procedure list
function* getInputProcedureList() {
  while (true) {
    let {params,callback} = yield take(procedureActionType.GET_INPUT_PROCEDURE_LIST);
    try {
      let { data } = yield call(axios.post, '/diagnosis/listPatientProcedures', params);
      if (data.respCode === 0) {
        yield put({
          type: procedureActionType.INPUT_PROCEDURE_LIST,
          inputProcedureList: data.data
        });
        callback&&callback(data);
        yield put({
          type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
        });
      } else {
 		  yield put({
          type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        if (!isNull(data.msgCode)) {
          yield put({
            type: messageTypes.OPEN_COMMON_MESSAGE,
            payload:{
              msgCode: data.msgCode
            }
          });
        } else {
          yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: data.message ? data.message : 'Service error'
          });
        }      }
    } catch (error) {
      yield put({
        type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
        type: commonType.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}


// save patient input procedure value
function* saveInputProcedure() {
  while (true) {
    let { params,callback } = yield take(procedureActionType.SAVE_INPUT_PROCEDURE);
    let { dtos } = params;
    try {
      let { data } = yield call(axios.post, '/diagnosis/savePatientProcedures', dtos);
      yield put({
        type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      if (data.respCode === 0) {
        callback&&callback(data);
      } else {
        if (!isNull(data.msgCode)) {
          yield put({
            type: messageTypes.OPEN_COMMON_MESSAGE,
            payload:{
              msgCode: data.msgCode
            }
          });
        } else {
          yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: data.message ? data.message : 'Service error'
          });
        }
      }
    } catch (error) {
      yield put({
        type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: error.message ? error.message : 'Service error',
          data: error
      });
    }
  }
}

function* getCodeDiagnosisStatusList() {
  while (true) {
    let {params,callback} = yield take(procedureActionType.GET_PROCEDURE_STATUS);
    try {
      let { data } = yield call(axios.post, '/diagnosis/listCodeDiagnosisStatus', params);
      if (data.respCode === 0) {
        callback&&callback(data.data);
      } else {
        yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: data.message ? data.message : 'Service error'
        });
      }
    } catch (error) {
      yield put({
        type: commonType.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

function* updatePatientProcedure() {
  while (true) {
    let {params,callback} = yield take(procedureActionType.UPDATE_PATIENT_PROCEDURE);
    let { patientKey,encounterId,dtos } = params;
    try {
      let { data } = yield call(axios.post, '/diagnosis/updatePatientProcedure', dtos);
      if (data.respCode === 0) {
        callback&&callback(data);
      } else {
        yield put({
          type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        if (!isNull(data.msgCode)) {
          yield put({
            type: messageTypes.OPEN_COMMON_MESSAGE,
            payload:{
              msgCode: data.msgCode
            }
          });
        } else {
          yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: data.message ? data.message : 'Service error'
          });
        }
      }
    } catch (error) {
      yield put({
        type: commonType.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}
function* deletePatientProcedure() {
  while (true) {
    let {params,callback} = yield take(procedureActionType.DELETE_PATIENT_PROCEDURE);
    let { patientKey,encounterId,dtos } = params;
    try {
      let { data } = yield call(axios.post, '/diagnosis/deletePatientProcedure', dtos);
      if (data.respCode === 0) {
        // yield put({
        //   type:procedureActionType.GET_INPUT_PROCEDURE_LIST,
        //   params:{
        //     patientKey,
        //     encounterId
        //   }
        // });
        callback&&callback(data);
      } else {
        yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: data.message ? data.message : 'Service error'
        });
      }
    } catch (error) {
      yield put({
        type: commonType.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}


function* getProcedureSearchList(action) {
    let {params,callback} = action;
    try {
      let { data } = yield call(axios.post, '/diagnosis/loadTermCodeData', params);
      if (data.respCode === 0) {
        callback&&callback(data.data);
      } else {
        yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: data.message ? data.message : 'Service error'
        });
      }
    } catch (error) {
      yield put({
        type: commonType.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
}

// fuzzy query procedure
function* queryProcedureList() {
  while (true) {
    let {params,callback} = yield take(procedureActionType.QUERY_PROCEDURE_LIST);
    try {
      let { data } = yield call(axios.post, '/diagnosis/listCodeTerminology', params);
      if (data.respCode === 0) {
        callback&&callback(data.data);
      } else {
        yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: data.message ? data.message : 'Service error'
        });
      }
    } catch (error) {
      yield put({
        type: commonType.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

function* searchProcedureList() {
  yield takeLatest(procedureActionType.GET_PROCEDURE_SEARCH_LIST, getProcedureSearchList);
 }

export const procedureSaga = [
    requestTemplateList(),
    saveTemplateList(),
    saveEditTemplateList(),
    getTermDisplayList(),
    getEditTemplateList(),
    getConfig(),
    getInputProcedureList(),
    saveInputProcedure(),
    getCodeDiagnosisStatusList(),
    updatePatientProcedure(),
    deletePatientProcedure(),
    queryProcedureList(),
    searchProcedureList()
];