import { take, takeLatest, call, put } from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as caseNoActionType from '../../actions/caseNo/caseNoActionType';
import * as messageType from '../../actions/message/messageActionType';
import * as commonType from '../../actions/common/commonActionType';
import Enum from '../../../enums/enum';
import * as CaseNoUtil from '../../../utilities/caseNoUtilities';
import _ from 'lodash';

function* fetchSaveCaseNo(action) {
    try {
        const { caseDialogStatus, params, callback, currentUpdateField } = action;
        switch (caseDialogStatus) {
            case Enum.CASE_DIALOG_STATUS.CREATE: {
                let { data } = yield call(axios.post, '/patient/generateCaseNo', params);
                if (data.respCode === 0) {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110038',
                            showSnackbar: true
                        }
                    });
                    callback && callback(data.data);
                }
                else if (data.respCode === 1) {
                    //todo parameterException
                }
                else if (data.respCode === 100) {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110139'
                        }
                    });
                }
                else if (data.respCode === 101) {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110140'
                        }
                    });
                }
                else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
                break;
            }
            case Enum.CASE_DIALOG_STATUS.EDIT: {
                let { data } = yield call(axios.post, '/patient/updateCaseNo', params);
                if (data.respCode === 0) {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110039',
                            params: [
                                { name: 'CASE_NO', value: CaseNoUtil.getFormatCaseNo(data.data && data.data.caseNo) },
                                {
                                    name: 'CASE_STATUS',
                                    value: () => {
                                        // if (data.data && data.data.statusCd === Enum.CASE_STATUS.ACTIVE) {
                                        //     return 'transferred';
                                        // }
                                        // return CaseNoUtil.getCaseNoStatus(data.data && data.data.statusCd);
                                        if (data.data) {
                                            if (currentUpdateField === 'statusCd') {
                                                return CaseNoUtil.getCaseNoStatus(data.data && data.data.statusCd);
                                            }
                                            else {
                                                return 'transferred';
                                            }
                                        }
                                    }
                                }
                            ],
                            showSnackbar: true
                        }
                    });
                    callback && callback(data.data);
                }
                else if (data.respCode === 100) {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110043'
                        }
                    });
                } else if (data.respCode === 101) {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110140'
                        }
                    });
                }
                else if (data.respCode === 102) {
                    let case_status = '';
                    // if (params.statusCd === Enum.CASE_STATUS.ACTIVE) {
                    //     case_status = 'transferred';
                    // } else {
                    //     // case_status = CaseNoUtil.getCaseNoStatus(params.statusCd);
                    //     case_status = CaseNoUtil.getCaseNoPromptStr(params.statusCd);
                    // }
                    if (currentUpdateField === 'statusCd') {
                        case_status = CaseNoUtil.getCaseNoPromptStr(params.statusCd);
                    }
                    else {
                        case_status = 'transferred';
                    }
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110042',
                            params: [{ name: 'CASE_STATUS', value: _.toLower(case_status) }]
                        }
                    });
                }
                else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
                break;
            }
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* saveCaseNo() {
    yield takeLatest(caseNoActionType.SAVE_CASENO, fetchSaveCaseNo);
}

function* listCasePrefix() {
    while (true) {
        try {
            const { serviceCd } = yield take(caseNoActionType.LIST_CASE_PREFIX);
            let { data } = yield call(axios.get, '/patient/listCasePrefix?serviceCd=' + serviceCd);
            if (data.respCode === 0) {
                yield put({
                    type: caseNoActionType.PUT_CASE_PREFIX,
                    casePrefixList: data.data
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getEncounterTypeList() {
    while (true) {
        let { clinicCd } = yield take(caseNoActionType.GET_ENCOUNTER_TYPE_LIST);
        try {
            let { data } = yield call(axios.post, '/appointment/getEncounterTypeList', { clinicCd: clinicCd });
            if (data.respCode === 0) {
                yield put({
                    type: caseNoActionType.PUT_ENCOUNTER_TYPE_LIST,
                    encounterTypeList: data.data
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* listCodeList() {
    while (true) {
        try {
            let { params } = yield take(caseNoActionType.LIST_CODE_LIST);
            let { data } = yield call(axios.post, '/common/listCodeList', params);
            if (data.respCode === 0) {
                yield put({
                    type: caseNoActionType.PUT_LIST_CODE_LIST,
                    data: data.data
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

export const caseNoSaga = [
    saveCaseNo(),
    listCasePrefix(),
    getEncounterTypeList(),
    listCodeList()
];