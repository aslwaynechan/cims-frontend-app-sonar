import { take, call, put } from 'redux-saga/effects';
import * as types from '../../actions/message/messageActionType';
import * as commonTypes from '../../actions/common/commonActionType';
import * as messageType from '../../actions/message/messageActionType';
import { API_getMessageListByAppId } from '../../../api/message';

function* getMessageListByAppId() {
  try {
      let { params } = yield take(messageType.GET_MESSAGE_LIST_BY_APP_ID);
      let data = yield call(API_getMessageListByAppId,params);
      if (data.respCode === 0) {
          yield put({
              type: types.COMMON_MESSAGE_LIST,
              commonMessageList: data.data
          });
      } else {
          yield put({
              type: messageType.OPEN_COMMON_MESSAGE,
              payload: {
                  msgCode: '110031'
              }
          });
      }
  } catch (error) {
      yield put({ type: commonTypes.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
  }
}

export const messageSaga = [
    getMessageListByAppId()
];