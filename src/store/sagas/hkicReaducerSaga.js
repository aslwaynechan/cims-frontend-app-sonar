import * as types from '../actions/hkic/hkicActionType';
import { take, call, put,fork,cancel,cancelled } from 'redux-saga/effects';
import axios from '../../services/axiosInstance';
import * as registrationUtilities from '../../utilities/registrationUtilities';
import * as sysConfig from '../../configs/config';
import * as commonType from '../actions/common/commonActionType';
import CommonMessage from '../../constants/commonMessage';

function* callService(){
    while (true) {
        let { file } = yield take(types.CONCERT);
        try {
            let formData = new FormData();
            formData.append('multipartFile',file);
            // let data=yield call(axios.post,'/patient/retrievalIVRS',formData);
            let {data}=yield call(axios.post,'/patient/retrievalIVRS',formData,{timeout:sysConfig.RequestTimedoutLong});
            if (data.respCode === 0) {
                yield put({ type: types.UPLOAD_SUCCESS,fileName:data.data.fileName,fileBlob: registrationUtilities.convertBase64UrlToBlob(data.data.fileContent)});
            } else if (data.respCode === 100) {
                yield put({type: types.UPLOAD_FAILED, errorMessage: CommonMessage.RETRIEVAL_IVRS_FILE_TYPE()});
            } else if (data.respCode === 101) {
                yield put({type: types.UPLOAD_FAILED, errorMessage: CommonMessage.RETRIEVAL_IVRS_FILE_SIZE()});
            } else {
                yield put({type: types.UPLOAD_FAILED, errorMessage: 'Service error'});
            }
        }catch(error){
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
            yield put({ type: types.UPLOAD_FAILED, errorMessage:null });
        } finally{
            if (yield cancelled())
                yield put({type: types.CANCEL_UPLOAD});
        }
    }
}


function* convert(){
    while (true) {
        const bgSyncTask = yield fork(callService);
        yield take(types.CANCEL_UPLOAD);
        yield cancel(bgSyncTask);
    }

}

export const hkicSagas = [
    convert()
];