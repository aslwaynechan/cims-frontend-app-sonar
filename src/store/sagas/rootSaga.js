import { all } from 'redux-saga/effects';
import { commonSagas } from './commonSaga';
import { registrationSaga } from './registration/registrationSaga';
import { loginSagas } from './loginSaga';
import { hkicSagas } from './hkicReaducerSaga';
import { administrationSaga } from './administrationSaga';
import { userRoleSaga } from './administration/userRole/userRoleSaga';
import { timeslotTemplateSagas } from './appointment/timeslot/timeslotTemplateSaga';
import { generateTimeSlotSaga } from './appointment/timeslot/generateTimeSlotSaga';
import { editTimeSlotSaga } from './appointment/timeslot/editTimeSlotSaga';
import { bookingSaga } from './appointment/booking/bookingSaga';
import { changePasswordSagas } from './administration/changePassword/changePasswordSaga';
import { patientSaga } from './patient/patientSaga';
import { attendanceSaga } from './appointment/attendance/attendanceSaga';
import { consultationSaga } from './consultation/consultationSaga';
import { prescriptionSaga } from './consultation/prescription/prescriptionSaga';
import { encounterTypeManagementSaga } from './administration/encounterTypeManagement/encounterTypeManagementSaga';
import { moeSaga } from './moe/moeSaga';
import { appointmentSlipFooterSaga } from './administration/appointmentSlipFooter/appointmentSlipFooterSaga';
import { forgetPasswordSaga } from './forgetPasswordSaga';
import { myFavouriteSaga } from './moe/myFavourite/myFavouriteSaga';
import { waitingListSaga } from './appointment/waitingListSaga';
import { mainFrameSaga } from './mainFrameSaga';
import { bookingAnonymousSaga } from './appointment/booking/bookingAnonymousSaga';
import { moePrintSaga } from './moe/moePrintSaga';
import { drugHistorySaga } from './moe/drugHistory/drugHistorySaga';
import { clinicalNoteSaga } from './clinicalNote/clinicalNoteSaga';
import { assessmentSagas } from './assessment/assessmentSaga';
import { messageSaga } from './message/messageSaga';
import { manageClinicalNoteTemplateSaga } from './clinicalNote/manageClinicalNoteTemplateSaga';
import { medicalSummarySaga } from './medicalSummary/medicalSummarySaga';
import { procedureSaga } from './procedure/procedureSaga';
import { diagnosisSaga } from './diagnosis/diagnosisSaga';
import { tokenTemplateManagementSaga } from './IOE/tokenTemplateManagement/tokenTemplateManagementSaga';
import { turnaroundTimeSaga } from './IOE/turnaroundTime/turnaroundTimeSaga';
import { specimenCollectionSaga } from './IOE/specimenCollection/specimenCollectionSaga';
import { serviceProfileSaga } from './IOE/serviceProfile/serviceProfileSaga';
import { attendanceCertSaga } from './certificate/attendanceCertificate/attendanceCertSaga';
import { sickLeaveSaga } from './certificate/sickLeave/sickLeaveSaga';
import { referralLetterSaga } from './certificate/referralLetter/referralLetterSaga';
import { calendarViewSaga } from './appointment/calendarViewSaga';
import { yellowFeverSaga } from './certificate/yellowFever/yellowFeverSaga';
import { yellowFeverCertSaga } from './certificate/yellowFeverCert/yellowFeverCertSaga';
import { publicHolidaySaga } from './administration/publicHoliday/publicHolidaySaga';
import { patientSpecFuncSaga } from './patient/patientSpecFuncSaga';
import { backgroundInformationSaga } from './MRAM/backgroundInformation/backgroundInformationSaga';
import { mramSaga } from './MRAM/mramSaga';
import { mramHistorySaga } from './mramHistory/mramHistorySaga';
import { clinicalSummaryReportSaga } from './report/clinicalSummaryReportSaga';
import { apptEnquirySaga } from './appointment/apptEnquiry/apptEnquirySaga';
import { caseNoSaga } from './caseNo/caseNoSaga';
import { enquirySaga } from './enquiry/enquirySaga';
import { formNameSaga } from './IOE/formName/formNameSaga';
import { laboratoryReportSaga } from './IOE/laboratoryReport/laboratoryReportSaga';
import {effects} from 'store/models';
import { ixRequestSaga } from './IOE/ixRequest/ixRequestSaga';
import {backTakeAttendanceSaga} from './backTakeAttendance/backTakeAttendanceSaga';
import { ecsSaga } from './ECS/ecsSaga';

export default function* rootSaga() {
  yield all([
    ...ecsSaga,
    ...commonSagas,
    ...registrationSaga,
    ...loginSagas,
    ...hkicSagas,
    ...administrationSaga,
    ...userRoleSaga,
    ...timeslotTemplateSagas,
    ...generateTimeSlotSaga,
    ...editTimeSlotSaga,
    ...bookingSaga,
    ...bookingAnonymousSaga,
    ...changePasswordSagas,
    ...patientSaga,
    ...attendanceSaga,
    ...consultationSaga,
    ...prescriptionSaga,
    ...encounterTypeManagementSaga,
    ...moeSaga,
    ...appointmentSlipFooterSaga,
    ...forgetPasswordSaga,
    ...myFavouriteSaga,
    ...moePrintSaga,
    ...waitingListSaga,
    ...mainFrameSaga,
    ...drugHistorySaga,
    ...clinicalNoteSaga,
    ...assessmentSagas,
    ...messageSaga,
    ...manageClinicalNoteTemplateSaga,
    ...medicalSummarySaga,
    ...procedureSaga,
    ...diagnosisSaga,
    ...tokenTemplateManagementSaga,
    ...turnaroundTimeSaga,
    ...specimenCollectionSaga,
    ...serviceProfileSaga,
    ...attendanceCertSaga,
    ...sickLeaveSaga,
    ...referralLetterSaga,
    ...calendarViewSaga,
    ...yellowFeverSaga,
    ...publicHolidaySaga,
    ...patientSpecFuncSaga,
    ...backgroundInformationSaga,
    ...mramSaga,
    ...mramHistorySaga,
    ...clinicalSummaryReportSaga,
    ...yellowFeverCertSaga,
    ...apptEnquirySaga,
    ...caseNoSaga,
    ...enquirySaga,
    ...formNameSaga,
    ...laboratoryReportSaga,
    ...effects,
    ...ixRequestSaga,
    ...backTakeAttendanceSaga
  ]);
}

