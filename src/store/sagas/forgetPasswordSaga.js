import * as types from '../actions/forgetPassword/forgetPasswordActionType';
import {
    take,
    call,
    put
} from 'redux-saga/effects';
import axios from '../../services/axiosInstance';
import CommonMessage from '../../constants/commonMessage';
// import * as commonTypes from '../actions/common/commonActionType';

function* send() {
    while (true) {
        let { params } = yield take(types.SEND);
        try {
            let { data } = yield call(axios.post, '/user/getTemporaryPassword', params);
            if (data.respCode === 0) {
                //todo forget password
                yield put({
                    type: types.SEND_SUCCESS,
                    data: data.data
                });
            } else if (data.respCode === 100) {
                yield put({
                    type: types.SEND_FAILURE,
                    data:data.data,
                    errMsg:CommonMessage.FORGET_PASSWORD_NOT_EXISTED()
                });
            } else if (data.respCode === 101) {
                yield put({
                    type: types.SEND_FAILURE,
                    data:data.data,
                    errMsg:CommonMessage.FORGET_PASSWORD_EXPIRED()
                });
            } else if (data.respCode === 102) {
                yield put({
                    type: types.SEND_FAILURE,
                    data:data.data,
                    errMsg:CommonMessage.FORGET_PASSWORD_LOCKED()
                });
            } else if (data.respCode === 103) {
                yield put({
                    type: types.SEND_FAILURE,
                    data:data.data,
                    errMsg:CommonMessage.FORGET_PASSWORD_EMAIL_BLANK()
                });
            } else if (data.respCode === 104) {
                yield put({
                    type: types.SEND_FAILURE,
                    data:data.data,
                    errMsg:CommonMessage.FORGET_PASSWORD_PHONE_BLANK()
                });
            } else {
                yield put({
                    type: types.SEND_FAILURE,
                    data:data.data,
                    errMsg:data.errMsg
                });
            }
        } catch (error) {
            yield put({
                type: types.SEND_FAILURE
            });
            // yield put({
            //     type: messageType.OPEN_COMMON_MESSAGE,
            //     payload: {
            //         msgCode: '110031'
            //     }
            // });
        }
    }
}

export const forgetPasswordSaga = [
    send()
];