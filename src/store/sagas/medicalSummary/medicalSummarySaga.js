import { take,call,put } from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as types from '../../actions/medicalSummary/medicalSummaryActionType';
import * as commonTypes from '../../actions/common/commonActionType';
import * as messageTypes from '../../actions/message/messageActionType';
import { MEDICAL_SUMMARY_TYPE,MEDICAL_SUMMARY_DROP_OPTION_SIGN } from '../../../constants/medicalSummary/medicalSummaryConstants';
import {isArray,isNull} from 'lodash';

// for temporary test
// get service list
function* getTempServiceList() {
  while (true) {
    let {params,callback} = yield take(types.GET_TEMP_SERVICE_LIST);
    try {
      let { data } = yield call(axios.post, '/common/listCodeList', params);
      if (data.respCode === 0) {
        yield put({
          type: types.TEMP_SERVICE_LIST,
          tempServiceList: data.data
        });
        callback&&callback();
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.message ? data.message : 'Service error'
        });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// get medical drop list
function* getMedicalDropList() {
  while (true) {
    let {params,callback} = yield take(types.GET_MEDICAL_DROP_LIST);
    try {
      let { data } = yield call(axios.get, '/medical-summary/listCodeMedicalSummaryDrop', {params:params});
      if (data.respCode === 0) {
        let smokingDropMap=new Map();
        let drinkingDropMap=new Map();
        let substanceAbuseDropMap=new Map();
        if (isArray(data.data)&&data.data.length>0) {
          data.data.forEach(element => {
            let tempMap = new Map();
            tempMap.set(MEDICAL_SUMMARY_DROP_OPTION_SIGN.STATUS,element.codeMedicalSummaryDropDtos[0]); //1st drop list
            tempMap.set(MEDICAL_SUMMARY_DROP_OPTION_SIGN.TYPE,element.codeMedicalSummaryDropDtos[1]); //2nd drop list
            switch (element.codeMedicalSummaryCd) {
              case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SMOKING:
                smokingDropMap = new Map(tempMap);
                break;
              case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_DRINKING:
                drinkingDropMap = new Map(tempMap);
                break;
              case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SUBSTANCE_ABUSE:
                  substanceAbuseDropMap = new Map(tempMap);
                break;
              default:
                break;
            }
          });
        }
        yield put({
          type: types.MEDICAL_DROP_LIST,
          smokingDropMap,
          drinkingDropMap,
          substanceAbuseDropMap
        });
        callback&&callback();
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.message ? data.message : 'Service error'
        });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// get medical summary val
function* getMedicalSummaryVal() {
  while (true) {
    let {params,callback} = yield take(types.GET_MEDICAL_SUMMARY_VAL);
    try {
      let { data } = yield call(axios.get, '/medical-summary/listMedicalSummary', {params:params});
      if (data.respCode === 0) {
        let pastHistoryValDtos = [];
        let smokingHistoryValDtos = [];
        let drinkingHistoryValDtos = [];
        let substanceAbuseHistoryValDtos = [];
        let occupationalHistoryValDtos = [];
        let familyHistoryValDtos = [];
        let remarkValDtos = [];
        if (data.data.medicalSummaryResultDtos.length>0) {
          let dtos = data.data.medicalSummaryResultDtos;
          dtos.forEach(element => {
            switch (element.codeMedicalSummaryCd) {
              case MEDICAL_SUMMARY_TYPE.PAST_MEDICAL_HISTORY:
                pastHistoryValDtos = element.medicalSummaryValueDto;
                break;
              case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SMOKING:
                smokingHistoryValDtos = element.medicalSummaryValueDto;
                break;
              case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_DRINKING:
                drinkingHistoryValDtos = element.medicalSummaryValueDto;
                break;
              case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SUBSTANCE_ABUSE:
                substanceAbuseHistoryValDtos = element.medicalSummaryValueDto;
                break;
              case MEDICAL_SUMMARY_TYPE.OCCUPATIONAL_HISTORY:
                occupationalHistoryValDtos = element.medicalSummaryValueDto;
                break;
              case MEDICAL_SUMMARY_TYPE.FAMILY_HISTORY:
                familyHistoryValDtos = element.medicalSummaryValueDto;
                break;
              case MEDICAL_SUMMARY_TYPE.REMARK:
                remarkValDtos = element.medicalSummaryValueDto;
                break;
              default:
                break;
            }
          });
        }
        yield put({
          type: types.MEDICAL_SUMMARY_VAL,
          pastHistoryValDtos,
          smokingHistoryValDtos,
          drinkingHistoryValDtos,
          substanceAbuseHistoryValDtos,
          occupationalHistoryValDtos,
          familyHistoryValDtos,
          remarkValDtos
        });
        callback&&callback(data.data);
      } else {
        yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: data.message ? data.message : 'Service error'
        });
      }
    } catch (error) {
      yield put({
        type: commonTypes.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

// save/update medical summary value
function* updateMedicalSummary() {
  while (true) {
    let { params,callback } = yield take(types.UPDATE_MEDICAL_SUMMARY);
    try {
      let { data } = yield call(axios.post, '/medical-summary/saveMedicalSummary', params);
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      if (data.respCode === 0) {
        yield put({
          type:types.GET_MEDICAL_SUMMARY_VAL,
          params:{
            patientKey: params.patientKey,
            serviceCd: params.serviceCd
          }
        });
        callback&&callback(data);
      } else {
        if (!isNull(data.msgCode)) {
          yield put({
            type: messageTypes.OPEN_COMMON_MESSAGE,
            payload:{
              msgCode: data.msgCode
            }
          });
        } else {
          yield put({
            type: commonTypes.OPEN_ERROR_MESSAGE,
            error: data.message ? data.message : 'Service error'
          });
        }
      }
    } catch (error) {
      yield put({
        type:commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
          type: commonTypes.OPEN_ERROR_MESSAGE,
          error: error.message ? error.message : 'Service error',
          data: error
      });
    }
  }
}

export const medicalSummarySaga = [
  getMedicalDropList(),
  getMedicalSummaryVal(),
  updateMedicalSummary(),
  // for temporary test
  getTempServiceList()
];