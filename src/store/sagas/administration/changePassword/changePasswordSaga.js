import { take, call, put } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as changePasswordActionTypes from '../../../actions/administration/changePassword/changePasswordActionType';
import * as commonType from '../../../actions/common/commonActionType';
import * as messageType from '../../../actions/message/messageActionType';

function* updatePassword() {
    while (true) {
        try {
            let { params, callback } = yield take(changePasswordActionTypes.UPDATE_PASSWORD);
            let { data } = yield call(axios.post, '/user/changePassword', params);
            if (data.respCode === 0) {
                yield put({
                    type: changePasswordActionTypes.UPDATE_PASSWORD_SUCCESS,
                    data:data.data
                });
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110301',
                        btnActions: {
                            btn1Click: callback
                        }
                    }
                });
            } else if (data.respCode === 100) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110321'
                    }
                });
            } else if (data.respCode === 101) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110322'
                    }
                });
            } else if (data.respCode === 102) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110323'
                    }
                });
            } else if (data.respCode === 3) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110032'
                    }
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}


export const changePasswordSagas = [
    updatePassword()
];