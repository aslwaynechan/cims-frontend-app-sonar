import { call, put, takeLatest, take } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as encounterTypeManagementActionTypes from '../../../actions/administration/encounterTypeManagement/encounterTypeManagementActionType';
import * as commonType from '../../../actions/common/commonActionType';
import * as messageType from '../../../actions/message/messageActionType';

function* getEncounterType(action) {
    try {
        let requsetParam = action.param || {};
        let { data } = yield call(axios.post, '/appointment/getEncounterTypeList', requsetParam);
        if (data.respCode === 0) {
            yield put({ type: encounterTypeManagementActionTypes.INIT_PAGE, field: { encounterType: data.data } });
        } else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* saveData() {
    while (true) {
        let { param } = yield take(encounterTypeManagementActionTypes.SAVE_DATA);
        let isSaveSuccess = false;
        try {
            switch (param.saveType) {
                case 'insertEncounterType':
                    {
                        let { data } = yield call(axios.post, '/appointment/insertEncounterType', param.saveData);
                        if (data.respCode === 0) {
                            isSaveSuccess = true;
                        } else if (data.respCode === 1) {
                            //todo parameterException
                        } else if (data.respCode === 3) {
                            yield put({
                                //Submission failed
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110032'
                                }
                            });
                        } else if (data.respCode === 100) {
                            //Records already exist!
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110316'
                                }
                            });
                        } else {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110031'
                                }
                            });
                        }
                    }
                    break;
                case 'insertSubEncounterType':
                    {
                        let { data } = yield call(axios.post, '/appointment/insertSubEncounterType', param.saveData);
                        if (data.respCode === 0) {
                            isSaveSuccess = true;
                        } else if (data.respCode === 1) {
                            //todo parameterException
                        } else if (data.respCode === 3) {
                            //Submission failed
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110032'
                                }
                            });
                        } else if (data.respCode === 100) {
                            //Records do not exist!
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110316'
                                }
                            });
                        } else {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110031'
                                }
                            });
                        }
                    }
                    break;
                case 'updateSubEncounterTypeDetail':
                    {
                        let { data } = yield call(axios.post, '/appointment/updateSubEncounterType', param.saveData);
                        if (data.respCode === 0) {
                            isSaveSuccess = true;
                        } else if (data.respCode === 1) {
                            //todo parameterException
                        } else if (data.respCode === 100) {
                            //Records do not exist!
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110317'
                                }
                            });
                        } else if (data.respCode === 3) {
                            //Submission failed
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110032'
                                }
                            });
                        } else {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110031'
                                }
                            });
                        }
                    }
                    break;
                case 'updateEncounterTypeDetail':
                    {
                        let { data } = yield call(axios.post, '/appointment/updateEncounterType', param.saveData);
                        if (data.respCode === 0) {
                            isSaveSuccess = true;
                        } else if (data.respCode === 1) {
                            //todo parameterException
                        } else if (data.respCode === 100) {
                            //Records do not exist!
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110317'
                                }
                            });
                        } else if (data.respCode === 3) {
                            //Submission failed
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110032'
                                }
                            });
                        } else {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110031'
                                }
                            });
                        }
                    }
                    break;
                default:
                    yield put({ type: encounterTypeManagementActionTypes.INIT_PAGE, field: { encounterType: [] } });
                    break;
            }
            if (isSaveSuccess) {
                let encounterTypeList = yield call(axios.post, '/appointment/getEncounterTypeList', {});
                if (encounterTypeList.data.respCode === 0) {
                    yield put({ type: encounterTypeManagementActionTypes.SAVE_SUCCESS, encounterType: encounterTypeList.data.data, saveData: param.saveData });
                } else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }

    }
}
function* deleteData() {
    while (true) {
        let { param } = yield take(encounterTypeManagementActionTypes.DELETE_DATA);
        let isSaveSuccess = false;
        try {
            switch (param.deleteType) {
                case 'deleteEncounterTypeDetail':
                    {
                        let { data } = yield call(axios.post, '/appointment/deleteEncounterType', param.deleteData);
                        if (data.respCode === 0) {
                            isSaveSuccess = true;
                        } else if (data.respCode === 1) {
                            //todo parameterException
                        } else if (data.respCode === 100) {
                            //Records do not exist!
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110317'
                                }
                            });
                        } else if (data.respCode === 101) {
                            //This record has time slot, could not delete!
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110318'
                                }
                            });
                        } else if (data.respCode === 3) {
                            //Submission failed
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110032'
                                }
                            });
                        } else {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110031'
                                }
                            });
                        }
                    }
                    break;
                case 'deleteSubEncounterTypeDetail':
                    {
                        let { data } = yield call(axios.post, '/appointment/deleteSubEncounterType', param.deleteData);
                        if (data.respCode === 0) {
                            isSaveSuccess = true;
                        } else if (data.respCode === 1) {
                            //todo parameterException
                        } else if (data.respCode === 100) {
                            //Records do not exist!
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110317'
                                }
                            });
                        } else if (data.respCode === 101) {
                            //This record has time slot, could not delete!
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110318'
                                }
                            });
                        } else if (data.respCode === 3) {
                            //Submission failed
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110032'
                                }
                            });
                        } else {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110031'
                                }
                            });
                        }
                    }
                    break;
                default:
                    yield put({ type: encounterTypeManagementActionTypes.INIT_PAGE, field: { encounterType: [] } });
                    break;
            }
            if (isSaveSuccess) {
                let encounterTypeList = yield call(axios.post, '/appointment/getEncounterTypeList', {});
                if (encounterTypeList.data.respCode === 0) {
                    yield put({ type: encounterTypeManagementActionTypes.SAVE_SUCCESS, encounterType: encounterTypeList.data.data, saveData: param.deleteData });
                } else {
                    yield put({
                        type: messageType.OPEN_COMMON_MESSAGE,
                        payload: {
                            msgCode: '110031'
                        }
                    });
                }
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }

    }
}

function* verifyCodeIsLegal() {
    while (true) {
        let { param } = yield take(encounterTypeManagementActionTypes.VERIFY_CODE_IS_LEGAL);
        try {
            switch (param.verifyType) {
                case 'verifySubEncounterTypeCd':
                    {
                        let { data } = yield call(axios.post, '/appointment/getExistSubEncounterType', param.verifyData);
                        if (data.respCode === 0) {
                            if (data.data.existCode === '0') {
                                yield put({ type: encounterTypeManagementActionTypes.VERIFY_CODE_IS_LEGAL_SUCCESSS, verifyType: param.verifyType });
                            } else if (data.data.existCode === '1') {
                                yield put({
                                    type: encounterTypeManagementActionTypes.CD_VERIFICATION_FAILED,
                                    field: { isLegalCode: false }
                                });
                                yield put({
                                    type: messageType.OPEN_COMMON_MESSAGE,
                                    payload: {
                                        msgCode: '110304'
                                    }
                                });
                            } else {
                                yield put({
                                    type: encounterTypeManagementActionTypes.CD_VERIFICATION_FAILED,
                                    field: { isLegalCode: false }
                                });
                                yield put({
                                    type: messageType.OPEN_COMMON_MESSAGE,
                                    payload: {
                                        msgCode: '110305',
                                        btnActions: {
                                            btn1Click: () => {
                                                param.updateFieldsFunction({
                                                    isLegalCode: true,
                                                    isNewData: false,
                                                    subEncounterTypeDetail: data.data
                                                });
                                            }
                                        }
                                    }
                                });
                            }
                        } else if (data.respCode === 1) {
                            //todo parameterException
                        } else {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110031'
                                }
                            });
                        }
                    }
                    break;
                case 'verifyEncounterTypeCd':
                    {
                        let { data } = yield call(axios.post, '/appointment/getExistEncounterType', param.verifyData);
                        if (data.respCode === 0) {
                            console.log('verifyCodeIsLegal', data);
                            if (data.data.existCode === '0') {
                                yield put({ type: encounterTypeManagementActionTypes.VERIFY_CODE_IS_LEGAL_SUCCESSS, verifyType: param.verifyType });
                            } else if (data.data.existCode === '1') {
                                yield put({
                                    type: encounterTypeManagementActionTypes.CD_VERIFICATION_FAILED,
                                    field: { isLegalCode: false }
                                });
                                yield put({
                                    type: messageType.OPEN_COMMON_MESSAGE,
                                    payload: {
                                        msgCode: '110302'

                                    }
                                });
                            } else {
                                yield put({
                                    type: encounterTypeManagementActionTypes.CD_VERIFICATION_FAILED,
                                    field: { isLegalCode: false }
                                });
                                yield put({
                                    type: messageType.OPEN_COMMON_MESSAGE,
                                    payload: {
                                        msgCode: '110303',
                                        btnActions: {
                                            btn1Click: () => {
                                                param.updateFieldsFunction({
                                                    isLegalCode: true,
                                                    isNewData: false,
                                                    encounterTypeDetail: data.data
                                                });
                                            }
                                        }
                                    }
                                });
                            }
                        } else if (data.respCode === 1) {
                            //todo parameterException
                        }  else {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110031'
                                }
                            });
                        }
                    }
                    break;
                default:
                    yield put({ type: encounterTypeManagementActionTypes.INIT_PAGE, field: { encounterType: [] } });
                    break;
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}


function* requestEncounterType() {
    yield takeLatest(encounterTypeManagementActionTypes.REQUEST_ENCOUNTER_TYPE, getEncounterType);
}




export const encounterTypeManagementSaga = [
    requestEncounterType(),
    saveData(),
    deleteData(),
    verifyCodeIsLegal()
];