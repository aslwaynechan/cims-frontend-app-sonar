import { takeLatest, call, put } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as type from '../../../actions/administration/publicHoliday/publicHolidayActionType';
import * as commonType from '../../../actions/common/commonActionType';
import { print } from '../../../../utilities/printUtilities';
import * as messageType from '../../../actions/message/messageActionType';


function* fetchHolidayList(action) {
    try {
        let { param } = action;
        let url = `/appointment/listHolidays/${param.yearFrom}/${param.yearTo}`;
        let { data } = yield call(axios.get, url);

        if (data.respCode === 0) {
            yield put({ type: type.LOAD_HOLIDAY_LIST, holidayList: data.data });
        } else if (data.respCode === 100) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110319'
                }
            });
        } else if (data.respCode === 101) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110320'
                }
            });
        }else {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    }
    catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* getHolidayList() {
    yield takeLatest(type.LIST_HOLIDAY, fetchHolidayList);
}


function* printHolidayList(action) {
    try {
        let { param } = action;
        let url = `/appointment/getHolidayReport/${param.yearFrom}/${param.yearTo}`;
        let { data } = yield call(axios.get, url);
        //let { data } = yield call(axios.post, '/appointment/getYellowFeverExemptionLetter', action.params);
        if (data.respCode === 0) {
            // console.log('base 64',data.data,action);
            // yield put({ type: commonType.PRINT_START, params: { base64: data.data, callback: action.callback, copies: action.copies } });
            print({ base64: data.data, callback: action.callback });
            yield put({ type: commonType.CLOSE_COMMON_CIRCULAR });
        } else if (data.respCode === 100) {
            yield put({ type: commonType.CLOSE_COMMON_CIRCULAR });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110319'
                }
            });
        } else if (data.respCode === 101) {
            yield put({ type: commonType.CLOSE_COMMON_CIRCULAR });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110320'
                }
            });
        } else {
            yield put({ type: commonType.CLOSE_COMMON_CIRCULAR });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
            // yield put({
            //     type: type.UPDATE_FIELD,
            //     updateData: { handlingPrint: false }
            // });
        }
    } catch (error) {
        yield put({ type: commonType.CLOSE_COMMON_CIRCULAR });
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        // yield put({
        //     type: type.UPDATE_FIELD,
        //     updateData: { handlingPrint: false }
        // });
    }
}

function* handleHolidayListPrinting() {
    yield takeLatest(type.PRINT_HOLIDAY_LIST, printHolidayList);
}

export const publicHolidaySaga = [
    getHolidayList(),
    handleHolidayListPrinting()
];