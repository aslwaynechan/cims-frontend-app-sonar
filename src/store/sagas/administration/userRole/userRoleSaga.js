import { take, takeLatest, call, all, put } from 'redux-saga/effects';
import axios from '../../../../services/axiosInstance';
import * as userRoleActionType from '../../../actions/administration/userRole/userRoleActionType';
import * as commonType from '../../../actions/common/commonActionType';
import * as messageType from '../../../actions/message/messageActionType';

function* searchUserRole() {
    while (true) {
        let { params } = yield take(userRoleActionType.GET_SEARCH_DATA);
        try {
            let { data } = yield call(axios.post, '/user/searchUserRole', params);
            if (data.respCode === 0) {
                yield put({ type: userRoleActionType.SEARCH_USER_ROLE, searchUserRoleList: data.data });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '100101'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getUserRoleById() {
    while (true) {
        let { params } = yield take(userRoleActionType.GET_USER_ROLE_BY_ID);
        try {
            const [userRole, replicableRole] = yield all([
                call(axios.post, '/user/getUserRole', params),
                call(axios.post, '/user/searchUserRole', { excludedId: params.userRoleId })
            ]);
            if (userRole.data.respCode !== 0) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            } else if (replicableRole.data.respCode !== 0) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            } else {
                yield put({
                    type: userRoleActionType.PREVIEW_USER_ROLE_DETAILS,
                    userRoleDetails: userRole.data.data,
                    currentRoleId: params.userRoleId,
                    replicableRole: replicableRole.data.data
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
        yield put({ type: userRoleActionType.SEARCH_USER_ROLE });
    }
}

function* createUserRole() {
    while (true) {
        yield take(userRoleActionType.CREATE_USER_ROLE);
        try {
            let where = {
                parameter: '',
                statusCd: 'A'
            };
            const [searchUser, replicableRole] = yield all([
                call(axios.post, '/user/searchUser', where),
                call(axios.post, '/user/searchUserRole', {})
            ]);
            if (searchUser.data.respCode !== 0) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            } else if (replicableRole.data.respCode !== 0) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            } else {
                yield put({
                    type: userRoleActionType.PUT_CREATE_USER_ROLE_DATA,
                    unassignedUsers: searchUser.data.data,
                    replicableRole: replicableRole.data.data
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* updateField() {
    while (true) {
        let param = yield take(userRoleActionType.UPDATE_FIELD);
        try {
            if (param.name === 'replicableRoleSelect') {
                if (param.value == '') {
                    yield put({
                        type: userRoleActionType.UPDATE_FIELD,
                        name: 'assignedUsers',
                        // value: data.data.users || []
                        value: []
                    });
                    yield put({
                        type: userRoleActionType.UPDATE_FIELD,
                        name: 'accessRights',
                        value: []
                    });
                } else {
                    let { data } = yield call(axios.post, '/user/getUserRole', { userRoleId: param.value });
                    if (data.respCode === 0) {
                        yield put({
                            type: userRoleActionType.UPDATE_FIELD,
                            name: 'unassignedUsers',
                            // value: data.data.excludedUsers || []
                            value: data.data.allUserDtos || []
                        });
                        yield put({
                            type: userRoleActionType.UPDATE_FIELD,
                            name: 'assignedUsers',
                            // value: data.data.users || []
                            value: data.data.userDtos || []
                        });
                        yield put({
                            type: userRoleActionType.UPDATE_FIELD,
                            name: 'accessRights',
                            value: data.data.accessRights
                        });
                    } else {
                        yield put({
                            type: messageType.OPEN_COMMON_MESSAGE,
                            payload: {
                                msgCode: '110031'
                            }
                        });
                    }
                }

            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* fetchSaveUserRole(action) {
    try {
        const param = action;
        let { data } = !param.params.userRoleId ? yield call(axios.post, '/user/insertUserRole', param.params) : yield call(axios.post, '/user/updateUserRole', param.params);
        if (data.respCode === 0) {
            yield put({ type: userRoleActionType.SAVE_SUCCESS });
        } else if (data.respCode === 1) {
            //todo parameterException
        } else if (data.respCode === 3) {
            //Submission failed
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110032'
                }
            });
        } else if (data.respCode === 102) {
            //Records do not exist!
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110314'
                }
            });
        } else if (data.respCode === 101) {
            //Invalid parameter!
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110315'
                }
            });
        } else if (data.respCode === 100) {
            //Records already exist!
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110313'
                }
            });
        } else {
            // yield put({ type: userRoleActionType.SAVE_ERROR, saveErrorMessage: data.errMsg, saveErrorData: data.data || [] });
            // yield put({
            //     type: commonType.OPEN_ERROR_MESSAGE,
            //     error: data.errMsg ? data.errMsg : 'Service error',
            //     data: data
            // });
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    } catch (error) {
        yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
    }
}

function* saveUserRole() {
    yield takeLatest(userRoleActionType.SAVE_USER_ROLE, fetchSaveUserRole);
}

function* searchAccessRight() {
    while (true) {
        let { params } = yield take(userRoleActionType.SEARCH_ACCESS_RIGHT);
        try {
            let { data } = yield call(axios.post, '/user/listAccessRight', params);
            if (data.respCode === 0) {
                yield put({ type: userRoleActionType.GET_ACCESS_RIGHT, data: data.data });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

export const userRoleSaga = [
    createUserRole(),
    searchUserRole(),
    getUserRoleById(),
    updateField(),
    saveUserRole(),
    searchAccessRight()
];