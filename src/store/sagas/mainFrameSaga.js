import { take, put } from 'redux-saga/effects';
import * as mainFrameType from '../actions/mainFrame/mainFrameActionType';
import * as commonType from '../actions/common/commonActionType';
import * as messageType from '../actions/message/messageActionType';
import _ from 'lodash';
import storeConfig from '../storeConfig';
import { deleteSubTabs } from '../actions/mainFrame/mainFrameAction';

function* skipTab() {
    while (true) {
        let { accessRightCd, params, checkExist } = yield take(mainFrameType.SKIP_TAB);
        try {
            if (accessRightCd) {
                const state = storeConfig.store.getState();
                let tabObj = _.cloneDeep(state.login.accessRights.find(item => item.name === accessRightCd));
                if (tabObj) {
                    tabObj.params = params;
                    if (checkExist) {
                        if (state.mainFrame.tabs.findIndex(item => item.name === tabObj.name) > -1 ||
                            state.mainFrame.subTabs.findIndex(item => item.name === tabObj.name) > -1) {
                            yield put({
                                type: messageType.OPEN_COMMON_MESSAGE,
                                payload: {
                                    msgCode: '110040'
                                }
                            });
                        } else {
                            yield put({ type: mainFrameType.ADD_TABS, params: tabObj });
                        }
                    } else {
                        yield put({ type: mainFrameType.ADD_TABS, params: tabObj });
                    }
                }
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* redirectTab() {
    while (true) {
        let { sourceAccessId, destAccessId, params } = yield take(mainFrameType.REDIRECT_TAB);
        try {
            const state = storeConfig.store.getState();
            if (destAccessId) {
                let tabObj = state.login.accessRights.find(item => item.name === destAccessId);
                if (tabObj) {
                    tabObj.params = params;
                    yield put({ type: mainFrameType.ADD_TABS, params: tabObj });
                }
            }
            if (sourceAccessId) {
                let tabObj = state.login.accessRights.find(item => item.name === sourceAccessId);
                if (tabObj) {
                    if (tabObj.isPatRequired === 'Y') {
                        yield put({ type: mainFrameType.DELETE_SUB_TABS, params: tabObj.name });
                    } else {
                        yield put({ type: mainFrameType.DELETE_TABS, params: tabObj.name });
                    }
                }
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* deleteSubTabsByOtherWay() {
    while (true) {
        let { params } = yield take(mainFrameType.DELETE_SUB_TABS_BY_OTHERWAY);
        try {
            const editModeTabs = params.editModeTabs;
            const name = params.name;
            // if (editModeTabs.indexOf(name) > -1) {
            if (editModeTabs.find(item => item.name === name)) {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110018',
                        btnActions: {
                            btn1Click: () => {
                                storeConfig.store.dispatch(deleteSubTabs(name));
                            }
                        }
                    }
                });
            } else {
                yield put({ type: mainFrameType.DELETE_SUB_TABS, params: name });
            }
        } catch (error) {
            yield put({
                type: messageType.OPEN_COMMON_MESSAGE,
                payload: {
                    msgCode: '110031'
                }
            });
        }
    }
}

// function* listenDoClose(){
//     while(true){

//         try{
//           let{tab}=yield take(mainFrameType.DO_CLOSE_TAB);
//         //   let actions=tab.isPatRequired==='Y'?:
//           yield put({
//               type:mainFrameType.DELETE_SUB_TABS()
//           });
//         }catch(error){
//             yield put({
//                 type: messageType.OPEN_COMMON_MESSAGE,
//                 payload: {
//                     msgCode: '110031'
//                 }
//             });
//         }
//     }
// }

export const mainFrameSaga = [
    skipTab(),
    redirectTab(),
    deleteSubTabsByOtherWay()
];