import { take, call, put,takeLatest} from 'redux-saga/effects';
import axios from '../../../services/axiosInstance';
import * as problemActionType from '../../actions/diagnosis/diagnosisActionType';
import * as commonType from '../../actions/common/commonActionType';
import * as messageTypes from '../../actions/message/messageActionType';
import {isNull} from 'lodash';

function* requestTemplateList() {
    while (true) {
        let { params ,callback} = yield take(problemActionType.PROBLEM_REQUEST_DATA);
        try {
                {
                        let { data } = yield call(axios.get, '/diagnosis/listProblemTmplGroups',params);
                        if (data.respCode === 0) {
                            yield put({ type: problemActionType.PROBLEM_LIST_DATA, fillingData: data});
                            callback&&callback(data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
                }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* saveEditTemplateList() {
    while (true) {
        let { params ,callback} = yield take(problemActionType.SAVE_PROBLEM_TEMPLATE_DATA);
        try {
                {
                        let { data } = yield call(axios.post, '/diagnosis/saveDiagnosisTmpls',params);
                        if (data.respCode === 0) {
                            callback&&callback(data);
                        } else {
                            if(data.msgCode!==undefined&&data.msgCode!==null){
                                callback&&callback(data);
                            }
                            else{
                                yield put({
                                  type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
                                });
                                 yield put({
                                    type: commonType.OPEN_ERROR_MESSAGE,
                                    error: data.errMsg ? data.errMsg : 'Service error',
                                    data: data.data
                                 });
                                }
                        }
                }
        } catch (error) {
          yield put({
            type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
          });
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* saveTemplateList() {
    while (true) {
        let { params ,callback} = yield take(problemActionType.SAVE_PROBLE_DATA);
        try {
                {
                        let { data } = yield call(axios.post,'/diagnosis/saveProblemTmplGroups',params);
                        yield put({
                          type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
                        });
                        if (data.respCode === 0) {
                            yield put({ type: problemActionType.SAVE_PROBLEM_RESULT, fillingData: data});
                            callback&&callback(data);
                        } else {
                            callback&&callback(data);
                        }
                }
        } catch (error) {
            yield put({
              type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
            });
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* getTermDisplayList() {
    while (true) {
        let { params,callback} = yield take(problemActionType.GET_TERMDISPLAYLIST_DATA);
        try {
                {
                        let { data } = yield call(axios.post, '/diagnosis/listTerminologyCodeDataByName',params);
                        if (data.respCode === 0) {
                            // yield put({ type: problemActionType.PUT_TERMDISPLAYLIST_DATA, fillingData: data.data});
                            callback&&callback(data.data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
                }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* getEditTemplateList() {
    while (true) {
        let { params,callback} = yield take(problemActionType.GET_EDITTEMPLATELIST_DATA);
        try {
                {
                        let { data } = yield call(axios.post, '/diagnosis/listDiagnosisTmplsById',params);
                        if (data.respCode === 0) {
                            // yield put({ type: problemActionType.PUT_EDITTEMPLATELIST_DATA, fillingData: data.data});
                            callback&&callback(data.data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
                }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

function* getConfig() {
    while (true) {
        let {configParams,callback} = yield take(problemActionType.GET_CONFIG_DATA);
        let {key} = configParams;
        try {
                {
                        let { data } = yield call(axios.get,`clinical-note/sysConfig/${key}`);
                        if (data.respCode === 0) {
                            callback&&callback(data.data);
                        } else {
                            yield put({
                                type: commonType.OPEN_ERROR_MESSAGE,
                                error: data.errMsg ? data.errMsg : 'Service error',
                                data: data.data
                            });
                        }
                }
        } catch (error) {
            yield put({
                type: commonType.OPEN_ERROR_MESSAGE,
                error: error.message ? error.message : 'Service error',
                data: error
            });
        }
    }
}

// get patient input problem list
function* getInputProblemList() {
  while (true) {
    let {params,callback} = yield take(problemActionType.GET_INPUT_PROBLEM_LIST);
    try {
      let { data } = yield call(axios.post, '/diagnosis/listPatientProblems', params);
      yield put({
        type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      if (data.respCode === 0) {
        callback&&callback(data);
      } else {
        yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: data.message ? data.message : 'Service error'
        });

      }
    } catch (error) {
      yield put({
        type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
        type: commonType.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}


// save patient input problem value
function* saveInputProblem() {
  while (true) {
    let { params,callback } = yield take(problemActionType.SAVE_INPUT_PROBLEM);
    let { dtos } = params;
    try {
      let { data } = yield call(axios.post, '/diagnosis/savePatientProblems', dtos);
      yield put({
        type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      if (data.respCode === 0) {
        callback&&callback(data);
      } else {
        if (!isNull(data.msgCode)) {
          yield put({
            type: messageTypes.OPEN_COMMON_MESSAGE,
            payload:{
              msgCode: data.msgCode
            }
          });
        } else {
          yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: data.message ? data.message : 'Service error'
          });
        }
      }
    } catch (error) {
      yield put({
        type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: error.message ? error.message : 'Service error',
          data: error
      });
    }
  }
}


function* getCodeDiagnosisStatusList() {
  while (true) {
    let {params,callback} = yield take(problemActionType.GET_PROBLEM_STATUS);
    try {
      let { data } = yield call(axios.post, '/diagnosis/listCodeDiagnosisStatus', params);
      if (data.respCode === 0) {
        callback&&callback(data.data);
      } else {
        yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: data.message ? data.message : 'Service error'
        });
      }
    } catch (error) {
      yield put({
        type: commonType.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

function* updatePatientProblem() {
  while (true) {
    let {params,callback} = yield take(problemActionType.UPDATE_PATIENT_PROBLEM);
    let { patientKey,encounterId,dtos } = params;
    try {
      let { data } = yield call(axios.post, '/diagnosis/updatePatientProblem', dtos);
      if (data.respCode === 0) {
        callback&&callback(data);
      } else {
        yield put({
          type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        if (!isNull(data.msgCode)) {
          yield put({
            type: messageTypes.OPEN_COMMON_MESSAGE,
            payload:{
              msgCode: data.msgCode
            }
          });
        } else {
          yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: data.message ? data.message : 'Service error'
          });
        }
      }
    } catch (error) {
      yield put({
        type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
      });
      yield put({
        type: commonType.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}
function* deletePatientProblem() {
  while (true) {
    let {params,callback} = yield take(problemActionType.DELETE_PATIENT_PROBLEM);
    let { patientKey,encounterId,dtos } = params;
    try {
      let { data } = yield call(axios.post, '/diagnosis/deletePatientProblem', dtos);
      if (data.respCode === 0) {
        callback&&callback(data);
      } else {
        yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: data.message ? data.message : 'Service error'
        });
      }
    } catch (error) {
      yield put({
        type: commonType.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}


function* getProblemSearchList(action) {
    let {params,callback} = action;
    try {
      let { data } = yield call(axios.post, '/diagnosis/listCodeTerminologyByPageable', params);
      if (data.respCode === 0) {
        callback&&callback(data.data);
      } else {
        yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: data.message ? data.message : 'Service error'
        });
      }
    } catch (error) {
      yield put({
        type: commonType.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
}

function* searchProblemList() {
  yield takeLatest(problemActionType.GET_PROBLEM_SEARCH_LIST, getProblemSearchList);
 }

function* getProblemSearchListNoPagination(action) {
    let {params,callback} = action;
    try {
      let { data } = yield call(axios.post, '/diagnosis/loadTermCodeData', params);
      if (data.respCode === 0) {
        callback&&callback(data.data);
      } else {
        yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: data.message ? data.message : 'Service error'
        });
      }
    } catch (error) {
      yield put({
        type: commonType.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
}

function* searchProblemListNoPagination() {
  yield takeLatest(problemActionType.GET_PROBLEM_SEARCH_LIST_NO_PAGINATION, getProblemSearchListNoPagination);
 }

function* listCodeDiagnosisTypes() {
    while (true) {
      let {params} = yield take(problemActionType.GET_DIAGNOSIS_RECORD_TYPE);
      try {
        let { data } = yield call(axios.get, '/diagnosis/listCodeDiagnosisTypes', params);
        if (data.respCode === 0) {
          yield put({
            type:problemActionType.PUT_DIAGNOSIS_RECORD_TYPE, fillingData:data.data});
        } else {
          yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: data.message ? data.message : 'Service error'
          });
        }
      } catch (error) {
        yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: error.message ? error.message : 'Service error',
          data: error
        });
      }
    }
  }

// fuzzy query problem
function* queryProblemList() {
  while (true) {
    let {params,callback} = yield take(problemActionType.QUERY_PROBLEM_LIST);
    try {
      let { data } = yield call(axios.post, '/diagnosis/listCodeTerminology', params);
      if (data.respCode === 0) {
        callback&&callback(data.data);
      } else {
        yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: data.message ? data.message : 'Service error'
        });
      }
    } catch (error) {
      yield put({
        type: commonType.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

function* getHistoricalRecords() {
    while (true) {
      let {params,callback} = yield take(problemActionType.GET_HISTORICAL_RECORD_LIST);
      try {
        let { data } = yield call(axios.post, '/diagnosis/loadHistoricalRecords', params);
        if (data.respCode === 0) {
            callback&&callback(data.data);
        } else {
          yield put({
            type: commonType.OPEN_ERROR_MESSAGE,
            error: data.message ? data.message : 'Service error'
          });
        }
      } catch (error) {
        yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: error.message ? error.message : 'Service error',
          data: error
        });
      }
    }
  }

  function* savePatient() {
    while (true) {
      let {params,callback} = yield take(problemActionType.SAVE_PATIENT_INFORMATION);
      try {
        let { data } = yield call(axios.post, '/diagnosis/savePatient', params);
        yield put({
          type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        callback&&callback(data);
      } catch (error) {
        yield put({
          type:commonType.CLOSE_COMMON_CIRCULAR_DIALOG
        });
        yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: error.message ? error.message : 'Service error',
          data: error
        });
      }
    }
  }

//get chronic problem list
function* getChronicProblemList() {
  while (true) {
    let { params,callback} = yield take(problemActionType.GET_CHRONIC_PROBLEM_LIST);
    try {
      let { data } = yield call(axios.post,'/diagnosis/listChronicProblems',params);
      if (data.respCode === 0) {
        yield put({
          type: problemActionType.CHRONIC_PROBLEM_LIST,
          chronicProblemList: data.data
        });
        callback&&callback(data.data);
      } else {
        yield put({
          type: commonType.OPEN_ERROR_MESSAGE,
          error: data.errMsg ? data.errMsg : 'Service error',
          data: data.data
        });
      }
    } catch (error) {
      yield put({
        type: commonType.OPEN_ERROR_MESSAGE,
        error: error.message ? error.message : 'Service error',
        data: error
      });
    }
  }
}

export const diagnosisSaga = [
	requestTemplateList(),
    saveTemplateList(),
    saveEditTemplateList(),
    getTermDisplayList(),
    getEditTemplateList(),
    getConfig(),
    getInputProblemList(),
    saveInputProblem(),
    getCodeDiagnosisStatusList(),
    updatePatientProblem(),
  	deletePatientProblem(),
    searchProblemList(),
    searchProblemListNoPagination(),
    listCodeDiagnosisTypes(),
    queryProblemList(),
    getHistoricalRecords(),
    savePatient(),
    getChronicProblemList()
];