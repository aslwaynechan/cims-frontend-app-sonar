import { actionChannel, take, call, put, race } from 'redux-saga/effects';
import axios from '../../services/axiosInstance';
import ccpAxios from '../../services/ccpAxiosInstance';
import * as commonType from '../actions/common/commonActionType';
import * as actions from '../actions/common/commonAction';
import * as messageType from '../actions/message/messageActionType';

function* getEncounterType() {
    while (true) {
        try {
            let { params, callback } = yield take(commonType.GET_ENCOUNTER_TYPE);
            let { data } = yield call(axios.post, '/appointment/getEncounterTypeList', params);
            if (data.respCode === 0) {
                yield put({
                    type: commonType.ENCOUNTER_TYPE,
                    data: data.data
                });
                if (typeof callback === 'function') {
                    callback(data.data);
                }
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* listService() {
    while (true) {
        let { params } = yield take(commonType.LIST_SERVICE);
        try {
            let { data } = yield call(axios.post, '/common/listService', params);
            if (data.respCode === 0) {
                yield put({
                    type: commonType.PUT_LIST_SERVICE,
                    serviceList: data.data
                });
            } else {
                yield put({
                    type: commonType.OPEN_ERROR_MESSAGE,
                    error: data.errMsg ? data.errMsg : 'Service error'
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* listClinic() {
    while (true) {
        let { params } = yield take(commonType.LIST_CLINIC);
        try {
            let { data } = yield call(axios.post, '/common/listClinic', params || {});
            if (data.respCode === 0) {
                yield put({
                    type: commonType.PUT_LIST_CLINIC,
                    clinicList: data.data
                });
            } else {
                yield put({
                    type: commonType.OPEN_ERROR_MESSAGE,
                    error: data.errMsg ? data.errMsg : 'Service error'
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

const wait = ms => (
    new Promise(resolve => {
        setTimeout(() => resolve(), ms);
    })
);

function* runTimer() {
    const channel = yield actionChannel(commonType.TIMER_START);
    while (yield take(channel)) {
        while (true) {
            const winner = yield race({
                stopped: take(commonType.TIMER_STOP),
                tick: call(wait, 1000)
            });

            if (!winner.stopped) {
                yield put(actions.timerTick());
            } else {
                break;
            }
        }
    }
}


function* print() {
    while (true) {
        let { params } = yield take(commonType.PRINT_START);
        let printSuccess = false;
        try {
            let check = yield call(ccpAxios.post, '/ccp/check');
            if (check.data && check.data.status === 'OK') {
                let qs = require('qs');
                let requireParams = {
                    tid: params.taskId,
                    pt: params.printType || 5,
                    url: params.documentUrl || 'http://localhost:17300',
                    docParm: params.documentParameters,
                    b64: params.base64,
                    que: params.printQueue,
                    tc: params.printTray,
                    fp: params.firstPage,
                    lp: params.lastPage,
                    ctr: params.isCenter || false,
                    fit: params.isFitPage || false,
                    shk: params.isShrinkPage || false,
                    ps: params.paperSize,
                    cps: params.copies,
                    ori: params.orientation || -1,
                    msz: params.mediaSize || 'N/A',
                    os: params.isObjectStream || false,
                    cb: params.callback,
                    ref: params.referer,
                    ver: check.data.agent_version,
                    cbm: params.callbackMode || 1,
                    pm: params.printMode,
                    pdfPw: params.pdfPassword,
                    sc: params.sheetCollate || 1
                };
                let { data } = yield call(ccpAxios.post, '/ccp/prn', qs.stringify(requireParams));
                if (data.success) {
                    printSuccess = true;
                    yield put({
                        type: commonType.PRINT_SUCCESS
                    });
                } else {
                    yield put({
                        type: commonType.PRINT_FAILURE
                    });
                }
            } else {
                yield put({
                    type: commonType.PRINT_FAILURE
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
        if (typeof params.callback === 'function') {
            params.callback(printSuccess);
        }
    }
}

function* listHospitalClinic() {
    while (true) {
        try {
            yield take(commonType.LIST_HOSPITAL_AND_CLINIC);
            let { data } = yield call(axios.post, '/common/listHospitalClinic');
            if (data.respCode === 0) {
                yield put({
                    type: commonType.LOAD_HOSPITAL_AND_CLINIC_LIST,
                    list: data.data
                });
            } else {
                yield put({
                    type: messageType.OPEN_COMMON_MESSAGE,
                    payload: {
                        msgCode: '110031'
                    }
                });
            }
        }
        catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

export const commonSagas = [
    getEncounterType(),
    listService(),
    listClinic(),
    runTimer(),
    print(),
    listHospitalClinic()
];