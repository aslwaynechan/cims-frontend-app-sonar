import * as types from '../actions/login/loginActionType';
import { take, call, put, all } from 'redux-saga/effects';
import axios from '../../services/axiosInstance';
import * as commonType from '../actions/common/commonActionType';
import * as patientType from '../actions/patient/patientActionType';
import * as clinicalNoteTypes from '../actions/clinicalNote/clinicalNoteActionType';
import * as caseNoType from '../actions/caseNo/caseNoActionType';
import CommonMessage from '../../constants/commonMessage';
import * as CommonUtilities from '../../utilities/commonUtilities';

function* doLogin() {
    while (true) {
        let { params } = yield take(types.DO_LOGIN);
        const callParams = {
            service: params.serviceCode,
            clinicCode: params.clinicCode,
            loginName: params.loginName,
            password: params.password
        };

        try {
            let { data } = yield call(axios.post, '/user/login', callParams);
            if (data.respCode === 0) {
                //sys config
                yield put({
                    type: clinicalNoteTypes.GET_SYS_CONFIG
                });
                window.sessionStorage.setItem('token', data.data.token);

                //message call first
                // yield put({
                //     type: messageTypes.GET_MESSAGE_LIST_BY_APP_ID,
                //     params: { applicationId: configs.applicationId }
                // });
                data.data['ecsLocCode'] = params.ecsLocCode;
                const userRoleType = data.data.userRoleType || '';
                //config call
                const [clinicConfig, listConfig] = yield all([
                    call(axios.post, '/common/listClinicConfigMap', {}),
                    call(axios.post, '/common/listListConfigMap', { serviceCd: data.data.service_cd, userGroupCd: userRoleType })
                ]);
                const clinicConfigData = clinicConfig.data;
                const listConfigData = listConfig.data;
                if (clinicConfigData && clinicConfigData.respCode === 0) {
                    yield put({ type: commonType.UPDATE_CONFIG, clinicConfig: clinicConfigData.data });
                    let where = { serviceCd: data.data.service_cd, clinicCd: data.data.clinic_cd };
                    window.sessionStorage.setItem('quotaTypeArray', JSON.stringify(CommonUtilities.getQuotaDescArray(clinicConfigData.data, where)));
                } else {
                    yield put({
                        type: commonType.OPEN_ERROR_MESSAGE,
                        error: clinicConfigData.errMsg ? clinicConfigData.errMsg : 'Get Clinic Config Failed.',
                        data: clinicConfigData.data
                    });
                }
                if (listConfigData && listConfigData.respCode === 0) {
                    yield put({ type: commonType.UPDATE_LIST_CONFIG, listConfig: listConfigData.data, loginInfo: data.data });
                } else {
                    yield put({
                        type: commonType.OPEN_ERROR_MESSAGE,
                        error: listConfigData.errMsg ? listConfigData.errMsg : 'Get List Config Failed.',
                        data: listConfigData.data
                    });
                }

                yield put({ type: patientType.LIST_NATIONALITY_AND_LIST_COUNTRY });

                yield put({ type: caseNoType.LIST_CASE_PREFIX, serviceCd: '' });

                yield put({ type: types.LOGIN_SUCCESS, loginInfo: data.data});

            } else if (data.respCode === 100) {
                yield put({
                    type: types.LOGIN_ERROR,
                    data: data.data,
                    errMsg: CommonMessage.LOGIN_INVALID()
                });
            } else if (data.respCode === 102) {
                yield put({
                    type: types.LOGIN_ERROR,
                    data: data.data,
                    errMsg: CommonMessage.LOGIN_EXPIRED()
                });
            } else if (data.respCode === 103) {
                yield put({
                    type: types.LOGIN_ERROR,
                    data: data.data,
                    errMsg: CommonMessage.LOGIN_LOCKED()
                });
            } else if (data.respCode === 101) {
                yield put({
                    type: types.LOGIN_ERROR,
                    data: data.data,
                    errMsg: CommonMessage.LOGIN_TEMPORARY_PASSWORD_EXPIRED()
                });
            } else if (data.respCode === 104) {
                yield put({
                    type: types.LOGIN_ERROR,
                    data: data.data,
                    errMsg: CommonMessage.LOGIN_ROLE_INVALID()
                });
            } else {
                yield put({
                    type: types.LOGIN_ERROR,
                    data: data.data,
                    errMsg: data.errMsg ? data.errMsg : 'Login Failed.'
                });
            }
        } catch (error) {
            console.log('watch login exception', error);
            yield put({ type: types.LOGIN_ERROR, errMsg: 'Service error.' });
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* refreshToken() {
    while (true) {
        yield take(types.REFRESH_TOKEN);
        let params = { oldToken: window.sessionStorage.getItem('token') };
        try {
            let { data } = yield call(axios.post, '/user/refreshToken', params);
            if (data.data != null && data.data.refresh) {
                window.sessionStorage.setItem('token', data.data.data);
            } else {
                yield put({
                    type: commonType.OPEN_ERROR_MESSAGE,
                    error: data.errorMessage ? data.errorMessage : 'Service error'
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getServiceNotice() {
    while (true) {
        let { params } = yield take(types.GET_SERVICE_NOTICE);
        try {
            let { data } = yield call(axios.post, '/common/listNotice', params);
            if (data.respCode === 0) {
                yield put({
                    type: types.PUT_SERVICE_NOTICE,
                    data: data.data
                });
            } else {
                yield put({
                    type: commonType.OPEN_ERROR_MESSAGE,
                    error: data.errorMessage ? data.errorMessage : 'Service error'
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* getContactUs() {
    while (true) {
        try {
            yield take(types.GET_CONTACT_US);
            let { data } = yield call(axios.post, '/common/listContactUsInfo');
            if (data.respCode === 0) {
                yield put({
                    type: types.PUT_CONTACT_US,
                    data: data.data
                });
            } else {
                yield put({
                    type: commonType.OPEN_ERROR_MESSAGE,
                    error: data.errMsg ? data.errMsg : 'Service error'
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}

function* listServiceByClientIp() {
    while(true) {
        try {
            let { uip } = yield take(types.LIST_SERVICE_BY_CLIENT_IP);
            let { data } = yield call(axios.get, '/user/listServiceByClientIp?Uip=' + uip);
            if (data.respCode === 0) {
                yield put({ type: types.PUT_CLIENT_IP, data: data.data });
            } else {
                yield put({
                    type: commonType.OPEN_ERROR_MESSAGE,
                    error: data.errMsg ? data.errMsg : 'Service error'
                });
            }
        } catch (error) {
            yield put({ type: commonType.OPEN_WARN_SNACKBAR, message: error.message ? error.message : 'Service error' });
        }
    }
}


export const loginSagas = [
    doLogin(),
    refreshToken(),
    getServiceNotice(),
    getContactUs(),
    listServiceByClientIp()
];
