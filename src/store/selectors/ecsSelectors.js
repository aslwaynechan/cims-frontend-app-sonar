import * as EcsUtilities from '../../utilities/ecsUtilities';
import Enums from '../../enums/enum';

export const regPatientInfoSelector = (state) => {
    let result = {...state.registration.patientBaseInfo};
    if(!result.documentPairList || (result.documentPairList && result.documentPairList.length == 0)){
        result.documentPairList = [];
        if(result.hkid){
            result.documentPairList.push(
                {
                    docTypeCd: Enums.DOC_TYPE.HKID_ID,
                    docNo: result.hkid
                }
            );
        }

        if(result.otherDocNo && result.docTypeCd){
            result.documentPairList.push(
                {
                    docTypeCd: result.docTypeCd,
                    docNo: result.otherDocNo
                }
            );
        }
    }
    return result;
} ;
export const regPatientKeySelector = (state) =>state.registration.patientById.patientKey;

export const summaryPatientInfoSelector = (state) =>state.registration.patientById ;
export const summaryPatientKeySelector = (state) =>state.registration.patientById.patientKey;

export const patientInfoSelector = (state) =>state.patient.patientInfo;
export const patientKeySelector = (state) =>state.patient.patientInfo?state.patient.patientInfo.patientKey:null;

export const ecsSelector = (state, fnPatientInfo, fnPatientKey) => {
    const patientInfo = fnPatientInfo(state);
    const patientKey = fnPatientKey(state);
    return {
        accessRights: state.login.accessRights?state.login.accessRights:[],
        ecsUserId: state.login.loginInfo.ecsUserId,
        ecsLocCode: state.login.clinic.ecsLocCode,
        ecsServiceStatus: state.ecs.ecsServiceStatus,
        selectedPatientEcsStatus: state.ecs.selectedPatientEcsStatus,
        appointmentId: state.patient.appointmentInfo ? state.patient.appointmentInfo.appointmentId: null,
        docTypeCds: patientInfo && patientInfo.documentPairList? patientInfo.documentPairList.map(item => item.docTypeCd): [],
        docTypeCd: EcsUtilities.getProperDocTypeCdForEcs(patientInfo),
        hkicForEcs: EcsUtilities.getProperHkicForEcs(patientInfo),
        engSurname: patientInfo.engSurname,
        engGivename: patientInfo.engGivename,
        nameChi: patientInfo.nameChi,
        dob: patientInfo.dob,
        exactDobCd: patientInfo.exactDobCd,
        patientKey: patientKey,
        codeList: state.ecs.codeList
    };
};

export const ocsssSelector = (state, fnPatientInfo, fnPatientKey) => {
    const patientInfo = fnPatientInfo(state);
    const patientKey = fnPatientKey(state);
    return {
        accessRights: state.login.accessRights,
        ocsssServiceStatus: state.ecs.ocsssServiceStatus,
        appointmentId: state.patient.appointmentInfo ? state.patient.appointmentInfo.appointmentId: null,
        docTypeCds: patientInfo.documentPairList.map(item => item.docTypeCd),
        docTypeCd: EcsUtilities.getProperDocTypeCdForEcs(patientInfo),
        hkicForEcs: EcsUtilities.getProperHkicForEcs(patientInfo),
        selectedPatientOcsssStatus: state.ecs.selectedPatientOcsssStatus,
        patientKey: patientKey
    };
};

export const mwecsSelector = (state, fnPatientInfo, fnPatientKey) => {
    const patientKey = fnPatientKey(state);
    const patientInfo = fnPatientInfo(state);
    let idNum = EcsUtilities.getHkidFromPatient(patientInfo);
    let idType = Enums.MWECS_ID_TYPE_KEYS.hkid;
    if(!idNum){
        idType = Enums.MWECS_ID_TYPE_KEYS.otherDoc;
        idNum = EcsUtilities.getProperHkicForEcs(patientInfo);
    }
    if(!idNum && patientInfo.documentPairList && patientInfo.documentPairList.length > 0){
        idNum = patientInfo.documentPairList[0].docNo;
    }
    return {
        idType: idType,
        idNum: idNum,
        appointmentId: state.patient.appointmentInfo ? state.patient.appointmentInfo.appointmentId: null,
        serviceStatus: state.ecs.mwecsServiceStatus,
        accessRights: state.login.accessRights,
        selectedPatientMwecsStatus: state.ecs.selectedPatientMwecsStatus,
        openDialog: state.ecs.selectedPatientMwecsStatus.openDialog,
        activeComponent: state.ecs.selectedPatientMwecsStatus.activeComponent,
        patientKey: patientKey
    };
};