import * as type from './yellowFeverActionType';

export const resetAll = () => {
    return {
        type: type.RESET_ALL
    };
};

export const updateField = (updateData) => {
    return {
        type: type.UPDATE_FIELD,
        updateData
    };
};

export const saveAndPrintYellowFeverLetter = (params, callback, copies) => {
    return {
        type: type.SAVE_AND_PRINT_LETTER,
        params,
        callback,
        copies
    };
};