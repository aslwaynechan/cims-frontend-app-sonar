import * as type from './sickLeaveActionType';

export const resetAll = () => {
    return {
        type: type.RESET_ALL
    };
};

export const updateField = (updateData) => {
    return {
        type: type.UPDATE_FIELD,
        updateData
    };
};

export const getSickLeaveCert=(params,callback,copies)=>{
    return {
        type:type.GET_SICK_LEAVE_CERT,
        params,
        callback,
        copies
    };
};