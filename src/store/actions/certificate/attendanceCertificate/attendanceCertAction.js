import * as type from './attendanceCertActionType';

export const getAttendanceCert = (params, copies, callback) => {
    return {
        type: type.GET_ATTENDANCE_CERT,
        params,
        copies,
        callback
    };
};

export const updateField = (updateData) => {
    return {
        type: type.UPDATE_FIELD,
        updateData
    };
};

export const resetAll = () => {
    return {
        type: type.RESET_ALL
    };
};


export const listAttendanceCertificates = (params) => {
    return {
        type: type.LIST_ATTENDANCE_CERTIFICATES,
        params
    };
};

export const updateAttendanceCertificate = (params, callback) => {
    return {
        type: type.UPDATE_ATTENDANCE_CERTIFICATE,
        params,
        callback
    };
};