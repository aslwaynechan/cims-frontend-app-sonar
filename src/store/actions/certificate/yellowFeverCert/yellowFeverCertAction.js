import * as type from './yellowFeverCertActionType';

export const resetAll = () => {
    return {
        type: type.RESET_ALL
    };
};

export const updateField = (updateData) => {
    return {
        type: type.UPDATE_FIELD,
        updateData
    };
};

export const saveAndPrintYellowFeverCert = (params, callback, copies) => {
    return {
        type: type.SAVE_AND_PRINT_CERT,
        params,
        callback,
        copies
    };
};