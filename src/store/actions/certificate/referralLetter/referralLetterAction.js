import * as type from './referralLetterActionType';

export const resetAll = () => {
    return {
        type: type.RESET_ALL
    };
};

export const updateField = (updateData) => {
    return {
        type: type.UPDATE_FIELD,
        updateData
    };
};

export const getReferralLetter = (params, callback, copies) => {
    return {
        type: type.GET_REFERRAL_LETTER_CERT,
        params,
        callback,
        copies
    };
};