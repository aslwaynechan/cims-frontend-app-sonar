import * as problemType from './diagnosisActionType';

export const requestProblemTemplateList = ({params={},callback}) => {
  return {
      type: problemType.PROBLEM_REQUEST_DATA,
      params,
      callback
  };
};

export const saveEditTemplateList = ({params={},callback}) => {
  return {
      type: problemType.SAVE_PROBLEM_TEMPLATE_DATA,
      params,
      callback
  };
};


export const saveTemplateList = ({params={},callback}) => {
  return {
      type: problemType.SAVE_PROBLE_DATA,
      params,
      callback
  };
};

export const getTermDisplayList = ({params={},callback}) => {
  return {
      type: problemType.GET_TERMDISPLAYLIST_DATA,
      params,
      callback
  };
};

export const getEditTemplateList = ({params={},callback}) => {
  return {
      type: problemType.GET_EDITTEMPLATELIST_DATA,
      params,
      callback
  };
};

export const getConfig = ({configParams={},callback}) => {
	return {
		type: problemType.GET_CONFIG_DATA,
		configParams,
		callback
	};
};

export const saveInputProblem = ({params={},callback}) => {
  return {
    type: problemType.SAVE_INPUT_PROBLEM,
    params,
    callback
  };
};

export const getInputProblemList = ({params={},callback}) => {
  return {
    type: problemType.GET_INPUT_PROBLEM_LIST,
    params,
    callback
  };
};

export const getProblemCodeDiagnosisStatusList = ({params={},callback}) => {
  return {
    type: problemType.GET_PROBLEM_STATUS,
    params,
    callback
  };
};

export const updatePatientProblem = ({params={},callback}) => {
  return {
    type: problemType.UPDATE_PATIENT_PROBLEM,
    params,
    callback
  };
};
export const deletePatientProblem = ({params={},callback}) => {
  return {
    type: problemType.DELETE_PATIENT_PROBLEM,
    params,
    callback
  };
};

export const searchProblemList = ({params={},callback}) => {
  return {
    type: problemType.GET_PROBLEM_SEARCH_LIST,
    params,
    callback
  };

};



export const searchProblemListNoPagination = ({params={},callback}) => {
  return {
    type: problemType.GET_PROBLEM_SEARCH_LIST_NO_PAGINATION,
    params,
    callback
  };
};

export const listCodeDiagnosisTypes = ({params={}}) => {
  return {
    type: problemType.GET_DIAGNOSIS_RECORD_TYPE,
    params
  };
};

export const queryProblemList = ({params={},callback}) => {
  return {
    type: problemType.QUERY_PROBLEM_LIST,
    params,
    callback
  };
};

export const getHistoricalRecords = ({params={},callback}) => {
  return {
    type: problemType.GET_HISTORICAL_RECORD_LIST,
    params,
    callback
  };
};

export const savePatient = ({params={},callback}) => {
  return {
    type: problemType.SAVE_PATIENT_INFORMATION,
    params,
    callback
  };
};

export const getChronicProblemList = ({params={},callback}) => {
  return {
    type: problemType.GET_CHRONIC_PROBLEM_LIST,
    params,
    callback
  };
};
