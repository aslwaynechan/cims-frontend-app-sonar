const PRE = 'LOGIN';
export const DO_LOGIN = `${PRE}_DO_LOGIN`;
export const LOGIN_SUCCESS = `${PRE}_LOGIN_SUCCESS`;
export const LOGIN_ERROR = `${PRE}_LOGIN_ERROR`;
export const LOGOUT = `${PRE}_LOGOUT`;
export const CLEAR_INFORMATION = `${PRE}_CLEAR_INFORMATION`;

export const CODE_LIST_BY_PARENT = `${PRE}_CODE_LIST_BY_PARENT`;
export const ERROR_MESSAGE = `${PRE}_ERROR_MESSAGE`;

export const REFRESH_TOKEN = `${PRE}_REFRESH_TOKEN`;
export const RESET_ERROR_MESSAGE = `${PRE}_RESET_ERROR_MESSAGE`;

//Service notice
export const GET_SERVICE_NOTICE = `${PRE}_GET_SERVICE_NOTICE`;
export const PUT_SERVICE_NOTICE = `${PRE}_PUT_SERVICE_NOTICE`;

//Contact us
export const GET_CONTACT_US = `${PRE}_GET_CONTACT_US`;
export const PUT_CONTACT_US = `${PRE}_PUT_CONTACT_US`;

//update login info
export const PUT_LOGIN_INFO = `${PRE}_PUT_LOGIN_INFO`;

//list service by client ip
export const LIST_SERVICE_BY_CLIENT_IP = `${PRE}_LIST_SERVICE_BY_CLIENT_IP`;
export const PUT_CLIENT_IP = `${PRE}_PUT_CLIENT_IP`;
