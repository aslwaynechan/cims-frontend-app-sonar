import * as types from './loginActionType';

export const doLogin = (params) => {
    return {
        type: types.DO_LOGIN,
        params: params
    };
};

export const logout = () => {
    return {
        type: types.LOGOUT
    };
};

export const refreshToken = () => {
    return {
        type: types.REFRESH_TOKEN
    };
};

export const resetErrorMsg = () => {
    return {
        type: types.RESET_ERROR_MESSAGE
    };
};

export const getServiceNotice = (params) => {
    return {
        type: types.GET_SERVICE_NOTICE,
        params
    };
};

export const getContactUs = () => {
    return {
        type: types.GET_CONTACT_US
    };
};

export const putLoginInfo = (curLoginServiceAndClinic) => {
    return {
        type: types.PUT_LOGIN_INFO,
        curLoginServiceAndClinic

    };
};

export const listServiceByClientIp = (uip = '') => {
    return {
        type: types.LIST_SERVICE_BY_CLIENT_IP,
        uip
    };
};