import * as CaseNoActionType from './caseNoActionType';

export const openCaseNoDialog = ({ caseDialogStatus, isNoPopup = false, caseNoForm, caseCallBack = null }) => {
    return {
        type: CaseNoActionType.OPEN_CASENO_DIALOG,
        caseDialogStatus,
        isNoPopup,
        caseNoForm,
        caseCallBack
    };
};

export const closeCaseNoDialog = () => {
    return {
        type: CaseNoActionType.CLOSE_CASENO_DIALOG
    };
};

export const updateCaseNoForm = (form) => {
    return {
        type: CaseNoActionType.UPDATE_CASENO_FORM,
        form
    };
};

export const updateState = (updateData) => {
    return {
        type: CaseNoActionType.UPDATE_STATE,
        updateData
    };
};

export const saveCaseNo = (caseDialogStatus, params, callback, currentUpdateField = '') => {
    return {
        type: CaseNoActionType.SAVE_CASENO,
        caseDialogStatus,
        params,
        callback,
        currentUpdateField
    };
};

export const listCasePrefix = (serviceCd = '') => {
    return {
        type: CaseNoActionType.LIST_CASE_PREFIX,
        serviceCd
    };
};

export const getEncounterTypeList = (clinicCd) => {
    return {
        type: CaseNoActionType.GET_ENCOUNTER_TYPE_LIST,
        clinicCd
    };
};

export const getEncounterGroup = () => {
    return {
        type: CaseNoActionType.GET_ENCOUNTER_GROUP
    };
};

export const listCodeList = (params) => {
    return {
        type: CaseNoActionType.LIST_CODE_LIST,
        params
    };
};

export const selectCaseTrigger = ({ trigger, caseSelectCallBack = null }) => {
    return {
        type: CaseNoActionType.SELECT_CASE_TRIGGER,
        trigger,
        caseSelectCallBack
    };
};