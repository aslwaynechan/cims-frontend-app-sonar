const PRE = 'COMMON';

//Error message dialog
export const OPEN_ERROR_MESSAGE = `${PRE}_OPEN_ERROR_MESSAGE`;
export const CLOSE_ERROR_MESSAGE = `${PRE}_CLOSE_ERROR_MESSAGE`;

//Common circular
export const OPEN_COMMON_CIRCULAR = `${PRE}_OPEN_COMMON_CIRCULAR`;
export const CLOSE_COMMON_CIRCULAR = `${PRE}_CLOSE_COMMON_CIRCULAR`;

//encounter type
export const GET_ENCOUNTER_TYPE = `${PRE}_GET_ENCOUNTER_TYPE`;
export const ENCOUNTER_TYPE = `${PRE}_ENCOUNTER_TYPE`;

//List Service
export const LIST_SERVICE = `${PRE}_LIST_SERVICE`;
export const PUT_LIST_SERVICE = `${PRE}_PUT_LIST_SERVICE`;

//List Clinic
export const LIST_CLINIC = `${PRE}_LIST_CLINIC`;
export const PUT_LIST_CLINIC = `${PRE}_PUT_LIST_CLINIC`;

//search bar
export const OPEN_SEARCH = `${PRE}_OPEN_SEARCH`;
export const CLOSE_SEARCH = `${PRE}_CLOSE_SEARCH`;
export const UPDATE_SEARCHBAR_VALUE = `${PRE}_UPDATE_SEARCHBAR_VALUE`;

//timer
export const TIMER_START = `${PRE}_TIMER_START`;
export const TIMER_TICK = `${PRE}_TIMER_TICK`;
export const TIMER_STOP = `${PRE}_TIMER_STOP`;
export const TIMER_RESET = `${PRE}_TIMER_RESET`;

//Common prompt
export const OPEN_PROMPT_MESSAGE = `${PRE}_OPEN_PROMPT_MESSAGE`;
export const CLOSE_PROMPT_MESSAGE = `${PRE}_CLOSE_PROMPT_MESSAGE`;


//ccp print
export const PRINT_START = `${PRE}_PRINT_START`;
export const PRINT_SUCCESS = `${PRE}_PRINT_SUCCESS`;
export const PRINT_FAILURE = `${PRE}_PRINT_FAILURE`;

//update Config
export const UPDATE_CONFIG = `${PRE}_UPDATE_CONFIG`;
export const UPDATE_LIST_CONFIG = `${PRE}_UPDATE_LIST_CONFIG`;

//warn Snackbar
export const OPEN_WARN_SNACKBAR = `${PRE}_OPEN_WARN_SNACKBAR`;
export const CLOSE_WARN_SNACKBAR = `${PRE}_CLOSE_WARN_SNACKBAR`;

//Common circular dialog
export const OPEN_COMMON_CIRCULAR_DIALOG = `${PRE}_OPEN_COMMON_CIRCULAR_DIALOG`;
export const CLOSE_COMMON_CIRCULAR_DIALOG = `${PRE}_CLOSE_COMMON_CIRCULAR_DIALOG`;

//list hospital and clinic
export const LIST_HOSPITAL_AND_CLINIC = `${PRE}_LIST_HOSPITAL_AND_CLINIC`;
export const LOAD_HOSPITAL_AND_CLINIC_LIST = `${PRE}_HOSPITAL_AND_CLINIC_LIST`;
