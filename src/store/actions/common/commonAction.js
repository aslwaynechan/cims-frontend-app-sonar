import * as CommonActionType from './commonActionType';

export const getEncounterType = (params, callback = null) => {
    return {
        type: CommonActionType.GET_ENCOUNTER_TYPE,
        params,
        callback
    };
};

export const listService = (params = {}) => {
    return {
        type: CommonActionType.LIST_SERVICE,
        params
    };
};

export const listClinic = (params) => {
    return {
        type: CommonActionType.LIST_CLINIC,
        params: params
    };
};

export const openErrorMessage = (error, data = null, errorTitle = '') => {
    return {
        type: CommonActionType.OPEN_ERROR_MESSAGE,
        error: error,
        data: data,
        errorTitle
    };
};

export const closeErrorMessage = () => {
    return {
        type: CommonActionType.CLOSE_ERROR_MESSAGE
    };
};

export const openCommonCircular = () => {
    return {
        type: CommonActionType.OPEN_COMMON_CIRCULAR
    };
};

export const closeCommonCircular = () => {
    return {
        type: CommonActionType.CLOSE_COMMON_CIRCULAR
    };
};

export const openSearch = () => {
    return {
        type: CommonActionType.OPEN_SEARCH
    };
};

export const closeSearch = () => {
    return {
        type: CommonActionType.CLOSE_SEARCH
    };
};

export const updateSearchBarValue = (value, isKeepData = null) => {
    return {
        type: CommonActionType.UPDATE_SEARCHBAR_VALUE,
        value,
        isKeepData
    };
};

export const timerStart = () => {
    return {
        type: CommonActionType.TIMER_START
    };
};

export const timerTick = () => {
    return {
        type: CommonActionType.TIMER_TICK
    };
};

export const timerStop = () => {
    return {
        type: CommonActionType.TIMER_STOP
    };
};

export const timerReset = () => {
    return {
        type: CommonActionType.TIMER_RESET
    };
};

export const openPromptDialog = () => {
    return {
        type: CommonActionType.OPEN_PROMPT_MESSAGE
    };
};

export const closePromptDialog = () => {
    return {
        type: CommonActionType.CLOSE_PROMPT_MESSAGE
    };
};
export const print = (params) => {
    return {
        type: CommonActionType.PRINT_START,
        params
    };
};

export const openWarnSnackbar = (message) => {
    return {
        type: CommonActionType.OPEN_WARN_SNACKBAR,
        message
    };
};

export const closeWarnSnackbar = () => {
    return {
        type: CommonActionType.CLOSE_WARN_SNACKBAR
    };
};

export const openCommonCircularDialog = () => {
    return {
        type: CommonActionType.OPEN_COMMON_CIRCULAR_DIALOG
    };
};

export const closeCommonCircularDialog = () => {
    return {
        type: CommonActionType.CLOSE_COMMON_CIRCULAR_DIALOG
    };
};

export const listHospitalClinic = () => {
    return {
        type: CommonActionType.LIST_HOSPITAL_AND_CLINIC
    };
};

export const loadHospitalClinicList = (list) => {
    return {
        type: CommonActionType.LOAD_HOSPITAL_AND_CLINIC_LIST,
        list
    };
};
