import * as attendanceActionTypes from './attendanceActionType';
export const updateField = (updateData) => {
    return ({
        type: attendanceActionTypes.UPDATE_FIELD,
        updateData
    });
};

export const resetAll = () => {
    return ({
        type: attendanceActionTypes.RESET_ALL
    });
};

//take attendance
export const getPatientStatusList = (params) => {
    return ({
        type: attendanceActionTypes.GET_PATIENT_STATUS,
        params
    });
};

export const markAttendance = (params, searchParams, callback) => {
    return ({
        type: attendanceActionTypes.MARK_ATTENDANCE,
        params,
        searchParams,
        callback
    });
};

export const destroyMarkAttendance = () => {
    return ({
        type: attendanceActionTypes.DESTROY_MARK_ATTENDANCE
    });
};

export const getAppointmentForAttend = (appointmentList, appointmentId) => {
    return ({
        type: attendanceActionTypes.GET_APPOINTMENT_FOR_TAKE_ATTENDANCE,
        appointmentList,
        appointmentId
    });
};

export const listAppointmentList = (params,  callback = null) => {
    return {
        type: attendanceActionTypes.LIST_APPOINTMENT,
        params,
        callback
    };
};
export const putApptList = (appointmentList) => {
    return {
        type: attendanceActionTypes.PUT_APPOINTMENT_LIST,
        appointmentList
    };
};
