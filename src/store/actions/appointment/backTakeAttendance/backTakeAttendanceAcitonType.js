const PREFFIX='BACK_TAKE_ATTENDANCE';
export const UPDATE_FIELD=`${PREFFIX}_UPDATE_FIELD`;
export const RESET_ALL=`${PREFFIX}_RESET_ALL`;

//patient status
export const GET_PATIENT_STATUS=`${PREFFIX}_GET_PATIENT_STATUS`;
export const PATIENT_STATUS_LIST=`${PREFFIX}_PATIENT_STATUS_LIST`;

export const BACK_TAKE_ATTENDANCE=`${PREFFIX}_MARK_ATTENDANCE`;
export const BACK_TAKE_ATTENDANCE_SUCCESS=`${PREFFIX}_MARK_ATTENDANCE_SUCCESS`;
export const DESTROY_MARK_ATTENDANCE=`${PREFFIX}_DESTROY_MARK_ATTENDANCE`;
export const GET_APPOINTMENT_FOR_TAKE_ATTENDANCE=`${PREFFIX}_GET_APPOINTMENT_FOR_TAKE_ATTENDANCE`;
export const APPOINTMENT_FOR_ATTENDANCE_SUCCESS=`${PREFFIX}_APPOINTMENT_FOR_ATTENDANCE_SUCCESS`;
export const RESET_ATTENDANCE=`${PREFFIX}_RESET_ATTENDANCE`;
export const RESET_ATTENDANCE_SUCCESS=`${PREFFIX}_RESET_ATTENDANCE_SUCCESS`;

export const GET_PATIENT_QUEUE = `${PREFFIX}_GET_PATIENT_QUEUE`;
export const PUT_PATIENT_QUEUE = `${PREFFIX}_PUT_PATIENT_QUEUE`;

//list Appt.
export const LIST_APPOINTMENT=`${PREFFIX}_LIST_APPOINTMENT`;
export const PUT_APPOINTMENT_LIST=`${PREFFIX}_PUT_APPOINTMENT_LIST`;