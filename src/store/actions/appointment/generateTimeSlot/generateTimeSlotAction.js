import * as GenerateTimeSlotActionType from './generateTimeSlotActionType';

export const resetAll = () => {
    return {
        type: GenerateTimeSlotActionType.RESET_ALL
    };
};

export const getCodeList = () => {
    return {
        type: GenerateTimeSlotActionType.GET_CODE_LIST
    };
};

export const updateField = (name, value) => {
    return {
        type: GenerateTimeSlotActionType.UPDATE_FIELD,
        name,
        value
    };
};

export const searchTimeSlotTemplate = (params) => {
    return {
        type: GenerateTimeSlotActionType.SEARCH_TEMPLATE,
        params
    };
};

export const generateTimeSlot = (params) => {
    return {
        type: GenerateTimeSlotActionType.GENERATE_TIMESLOT,
        params
    };
};

export const handleSelectTemplate = (data) => {
    return {
        type: GenerateTimeSlotActionType.HANDLE_SELECTED_TEMPLATE,
        data
    };
};

export const closeDialog = () => {
    return {
        type: GenerateTimeSlotActionType.CLOSE_DIALOG
    };
};