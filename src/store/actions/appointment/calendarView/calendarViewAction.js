import * as bookingActionType from './calendarViewActionType';

export const resetAll = () => {
    return {
        type: bookingActionType.RESET_ALL
    };
};

export const requestData = (dataType, params, fileData) => {
    return {
        type: bookingActionType.REQUEST_DATA,
        dataType,
        params,
        fileData
    };
};

export const updateField = (updateData) => {
    return {
        type: bookingActionType.UPDATE_FIELD,
        updateData
    };
};

export const fillingData = (fileData) => {
    return {
        type: bookingActionType.FILLING_DATA,
        ...fileData
    };
};
