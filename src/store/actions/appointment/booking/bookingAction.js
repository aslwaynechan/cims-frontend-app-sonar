import * as bookingActionType from './bookingActionType';

export const resetAll = () => {
    return {
        type: bookingActionType.RESET_ALL
    };
};
export const destroyPage = () => {
    return {
        type: bookingActionType.DESTROY_PAGE
    };
};

export const requestData = (dataType, params, fileData) => {
    return {
        type: bookingActionType.REQUEST_DATA,
        dataType,
        params,
        fileData
    };
};

export const updateField = (updateData) => {
    return {
        type: bookingActionType.UPDATE_FIELD,
        updateData
    };
};

export const fillingData = (fileData) => {
    return {
        type: bookingActionType.FILLING_DATA,
        ...fileData
    };
};


export const resetInfoAll = () => {
    return {
        type: bookingActionType.RESET_INFO_ALL
    };
};

export const getClinicList = (serviceCode) => {
    return {
        type: bookingActionType.GET_CLINIC_LIST,
        serviceCode
    };
};

export const getEncounterTypeList = (params, serviceCd = null, clinicCd = null, clinicConfig = null) => {
    return {
        type: bookingActionType.GET_ENCOUNTER_TYPE_LIST,
        params,
        serviceCd,
        clinicCd,
        clinicConfig
    };
};

export const appointmentBook = (params) => {
    return {
        type: bookingActionType.APPOINTMENT_BOOK,
        params
    };
};

export const listTimeSlot = (params) => {
    return {
        type: bookingActionType.LIST_TIMESLOT,
        params
    };
};

export const listAppointment = (params, callback = null) => {
    return {
        type: bookingActionType.LIST_APPOINTMENT,
        params,
        callback
    };
};

export const loadAppointmentBooking = (passData) => {
    return {
        type: bookingActionType.LOAD_APPOINTMENT_BOOKING,
        passData
    };
};

export const bookConfirm = (params, bookData) => {
    return {
        type: bookingActionType.BOOK_CONFIRM,
        params,
        bookData
    };
};
export const bookConfirmWaiting = (params, bookData) => {
    return {
        type: bookingActionType.BOOK_CONFIRM_WAITING,
        params,
        bookData
    };
};

export const updateState = (updateData,index=null) => {
    return {
        type: bookingActionType.UPDATE_STATE,
        updateData,
        index
    };
};


export const getAppointmentReport = (params) => {
    return {
        type: bookingActionType.GET_APPOINTMENT_REPORT,
        params
    };
};


export const walkInAttendance = (params, callback) => {
    return {
        type: bookingActionType.WALK_IN_ATTENDANCE,
        params,
        callback
    };
};

export const bookAndAttendSuccess = () => {
    return {
        type: bookingActionType.BOOK_AND_ATTEND_SUCCEESS
    };
};

export const listSeachLogic = () => {
    return {
        type: bookingActionType.LIST_SEARCH_LOGIC
    };
};

export const listRemarkCode = () => {
    return {
        type: bookingActionType.LIST_REMARK_CODE
    };
};

export const putUserSearchLogic = (searchLogicList) => {
    return {
        type: bookingActionType.PUT_USER_SEARCH_LOGIC,
        searchLogicList
    };
};

export const cancelAppointment = (apptPara, listApptPara) => {
    return {
        type: bookingActionType.CANCEL_APPOINTMENT,
        apptPara,
        listApptPara
    };
};

export const editAppointment = (apptInfo) => {
    return {
        type: bookingActionType.EDIT_APPOINTMENT,
        apptInfo
    };
};

export const cancelEditAppointment = () => {
    return {
        type: bookingActionType.CANCEL_EDIT_APPOINTMENT
    };
};

export const submitUpdateAppointment = (updateApptPara, listApptPara, updateOrBookNew, bookData = null, submitParams) => {
    return {
        type: bookingActionType.SUBMIT_UPDATE_APPOINTMENT,
        updateApptPara,
        listApptPara,
        updateOrBookNew,
        bookData,
        submitParams
    };
};

export const updateAppointmentSuccess = () => {
    return {
        type: bookingActionType.UPDATE_APPOINTMENT_SUCCESS
    };
};

export const listContatHistory = (apptId, callback = null) => {
    return {
        type: bookingActionType.GET_CONTACT_HISTORY,
        apptId,
        callback
    };
};

export const insertContatHistory = (params, callback = null) => {
    return {
        type: bookingActionType.INSERT_CONTACT_HISTORY,
        params,
        callback
    };
};

export const updateContatHistory = (params, callback = null) => {
    return {
        type: bookingActionType.UPDATE_CONTACT_HISTORY,
        params,
        callback
    };
};

export const clearContactList = () => {
    return {
        type: bookingActionType.CLEAR_CONTACT_HISTORY
    };
};
export const listToalAppointment = (params) => {
    return {
        type: bookingActionType.LIST_TOTAL_APPOINTMENT,
        params
    };
};
