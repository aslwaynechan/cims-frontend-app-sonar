import * as type from './bookingAnonymousActionType';

export const resetAll = () => {
    return {
        type: type.RESET_ALL
    };
};
export const destroyPage = () => {
    return {
        type: type.DESTROY_PAGE
    };
};

export const requestData = (dataType, params, fileData) => {
    return {
        type: type.REQUEST_DATA,
        dataType,
        params,
        fileData
    };
};

export const updateField = (updateData) => {
    return {
        type: type.UPDATE_FIELD,
        updateData
    };
};

export const fillingData = (fileData) => {
    return {
        type: type.FILLING_DATA,
        ...fileData
    };
};


export const resetInfoAll = () => {
    return {
        type: type.RESET_INFO_ALL
    };
};

export const getClinicList = (serviceCode) => {
    return {
        type: type.GET_CLINIC_LIST,
        serviceCode
    };
};

export const getEncounterTypeList = (params, serviceCd = null, clinicCd = null, clinicConfig = null) => {
    return {
        type: type.GET_ENCOUNTER_TYPE_LIST,
        params,
        serviceCd,
        clinicCd,
        clinicConfig
    };
};

export const appointmentBook = (params) => {
    return {
        type: type.APPOINTMENT_BOOK,
        params
    };
};

export const listTimeSlot = (params) => {
    return {
        type: type.LIST_TIMESLOT,
        params
    };
};

export const listAppointment = (params) => {
    return {
        type: type.LIST_APPOINTMENT,
        params
    };
};

export const loadAppointmentBooking = (passData) => {
    return {
        type: type.LOAD_APPOINTMENT_BOOKING,
        passData
    };
};

export const bookConfirm = (params, bookData, callback) => {
    return {
        type: type.BOOK_CONFIRM,
        params,
        bookData,
        callback
    };
};

export const bookConfirmWaiting = (params, bookData, callback) => {
    return {
        type: type.BOOK_CONFIRM_WAITING,
        params,
        bookData,
        callback
    };
};

export const updateState = (updateData,index=null) => {
    return {
        type: type.UPDATE_STATE,
        updateData,
        index
    };
};


export const getAppointmentReport = (params) => {
    return {
        type: type.GET_APPOINTMENT_REPORT,
        params
    };
};

export const listRemarkCode = () => {
    return {
        type: type.LIST_REMARK_CODE
    };
};