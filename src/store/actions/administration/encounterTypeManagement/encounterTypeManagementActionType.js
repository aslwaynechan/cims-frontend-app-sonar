const PREFFIX='encounterTypeManagement';

export const INIT_PAGE=`${PREFFIX}_INIT_PAGE`;
export const RESET_ALL=`${PREFFIX}_RESET_ALL`;
export const UPDATE_FIELD=`${PREFFIX}_UPDATE_FIELD`;
export const REQUEST_ENCOUNTER_TYPE=`${PREFFIX}_REQUEST_ENCOUNTER_TYPE`;
export const VERIFY_CODE_IS_LEGAL=`${PREFFIX}_VERIFY_CODE_IS_LEGAL`;
export const VERIFY_CODE_IS_LEGAL_SUCCESSS=`${PREFFIX}_VERIFY_CODE_IS_LEGAL_SUCCESSS`;
export const SAVE_DATA=`${PREFFIX}_SAVE_DATA`;
export const SAVE_SUCCESS=`${PREFFIX}_SAVE_SUCCESS`;
export const SAVE_FAILURE=`${PREFFIX}_SAVE_FAILURE`;
export const DELETE_DATA=`${PREFFIX}_DELETE_DATA`;
export const DELETE_SUCCESS=`${PREFFIX}_DELETE_SUCCESS`;
export const DELETE_FAILURE=`${PREFFIX}_DELETE_FAILURE`;

export const CD_VERIFICATION_FAILED=`${PREFFIX}_CD_VERIFICATION_FAILED`;