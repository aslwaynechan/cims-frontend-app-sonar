import * as encounterTypeManagementActionTypes from './encounterTypeManagementActionType';

export const updateField=(field)=>{
    return{
        type:encounterTypeManagementActionTypes.UPDATE_FIELD,
        field
    };
};

export const resetAll=()=>{
    return{
        type:encounterTypeManagementActionTypes.RESET_ALL
    };
};

export const requestEncounterType=(param)=>{
    return{
        type:encounterTypeManagementActionTypes.REQUEST_ENCOUNTER_TYPE,
        param
    };
};
export const saveData=(param)=>{
    return{
        type:encounterTypeManagementActionTypes.SAVE_DATA,
        param
    };
};
export const deleteData=(param)=>{
    return{
        type:encounterTypeManagementActionTypes.DELETE_DATA,
        param
    };
};
export const verifyCodeIsLegal=(param)=>{
    return{
        type:encounterTypeManagementActionTypes.VERIFY_CODE_IS_LEGAL,
        param
    };
};

