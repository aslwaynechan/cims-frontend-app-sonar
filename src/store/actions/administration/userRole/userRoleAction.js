import * as userRoleActionType from './userRoleActionType';

export const resetAll = () => {
    return {
        type: userRoleActionType.RESET_ALL
    };
};

export const cancelEdit = () => {
    return {
        type: userRoleActionType.CANCEL_EDIT
    };
};

export const searchUserRole = (params) => {
    return {
        type: userRoleActionType.GET_SEARCH_DATA,
        params
    };
};

export const getUserRoleById = (params) => {
    return {
        type: userRoleActionType.GET_USER_ROLE_BY_ID,
        params
    };
};

export const updateField = (name, value) => {
    return {
        type: userRoleActionType.UPDATE_FIELD,
        name,
        value
    };
};


export const saveUserRole = (params) => {
    return {
        type: userRoleActionType.SAVE_USER_ROLE,
        params
    };
};

export const createUserRole = () => {
    return {
        type: userRoleActionType.CREATE_USER_ROLE
    };
};
export const editUserRole = (address) => {
    return {
        type: userRoleActionType.EDIT_USER_ROLE,
        address: address
    };
};

export const selectAccessRight = (accessRights) => {
    return {
        type: userRoleActionType.SELECT_MENU_LIST,
        accessRights
    };
};
export const searchAccessRight = (params) => {
    return {
        type: userRoleActionType.SEARCH_ACCESS_RIGHT,
        params
    };
};
export const openMenu = (accessRightCd) => {
    return {
        type: userRoleActionType.OPEN_MENU,
        accessRightCd
    };
};



