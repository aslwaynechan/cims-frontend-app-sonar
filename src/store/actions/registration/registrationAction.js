import * as RegistrationType from './registrationActionType';

export const resetAll = () => {
    return {
        type: RegistrationType.RESET_ALL
    };
};

export const updatePatientOperateStatus = (status) => {
    return {
        type: RegistrationType.UPDATE_PATIENT_OPERATE_STATUS,
        status
    };
};

export const updatePatient = (params,callback=null) => {
    return {
        type: RegistrationType.UPDATE_PATIENT,
        params: params,
        callback
    };
};

export const registerPatient = (params,callback=null) => {
    return {
        type: RegistrationType.REGISTER_PATIENT,
        params: params,
        callback
    };
};

export const updateState = (updateData) => {
    return {
        type: RegistrationType.UPDATE_STATE,
        updateData
    };
};

export const getPatientById = (patientKey) => {
    return {
        type: RegistrationType.GET_PATINET_BY_ID,
        patientKey
    };
};

// export const updatePatientField = (name, value) => {
//     return {
//         type: RegistrationType.UPDATE_PATIENT_FIELD,
//         name,
//         value
//     };
// };

export const searchPatient = (searchString, pageNum, pageSize) => {
    return {
        type: RegistrationType.SEARCH_PATIENT,
        searchString,
        pageNum,
        pageSize
    };
};
//find Char By ccCode
export const findCharByCcCode = (ccCode,charIndex) => {
    return {
        type: RegistrationType.FIND_CHAR_BY_CC_CODE,
        charIndex,
        ccCode
    };
};
export const updateChiChar = (charIndex, char ) => {
    return {
        type: RegistrationType.UPDATE_CHI_CHAR,
        charIndex,
        char
    };
};
//update Chinese name Char
export const updateCcCode = (ccCodeList) => {
    return {
        type: RegistrationType.UPDATE_CC_CODE,
        ccCodeList
    };
};
