import * as AssessmentActionType from './assessmentActionType';

// Assessment Setting
export const getAssessmentSettingItemList = ({params={},callback}) => {
  return {
    type: AssessmentActionType.GET_ASSESSMENT_SETTING_ITEM_LIST,
    params,
    callback
  };
};

export const getAssessmentCheckedItemList = ({params={},callback}) => {
  return {
    type: AssessmentActionType.GET_ASSESSMENT_CHECKED_ITEM_LIST,
    params,
    callback
  };
};

export const updateAssessmentSetting = ({params,callback}) => {
  return {
    type: AssessmentActionType.UPDATE_ASSESSMENT_SETTING_ITEM_LIST,
    params,
    callback
  };
};

// General Assessment
export const getPatientAssessmentList = ({params={},callback}) => {
  return {
    type: AssessmentActionType.GET_PATIENT_ASSESSMENT_LIST,
    params,
    callback
  };
};

export const updatePatientAssessment = ({params={},callback}) => {
  return {
    type: AssessmentActionType.UPDATE_PATIENT_ASSESSMENT,
    params,
    callback
  };
};

export const getFieldDropList = ({params={},callback}) => {
  return {
    type: AssessmentActionType.GET_FIELD_DROP_LIST,
    params,
    callback
  };
};

export const getFieldNormalRangeList = ({params={},callback}) => {
  return {
    type: AssessmentActionType.GET_FIELD_NORMAL_RANGE_MAP,
    params,
    callback
  };
};

// for temporary test
export const getTempServiceList = ({params={},callback}) => {
  return {
    type: AssessmentActionType.GET_TEMP_SERVICE_LIST,
    params,
    callback
  };
};

export const saveAll = ({params={},callback}) => {
  return {
    type: AssessmentActionType.SAVE_ALL,
    params,
    callback
  };
};

