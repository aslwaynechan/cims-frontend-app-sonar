import * as type from './patientSpecFuncActionType';

export const searchPatientList = (params) => {
    return {
        type: type.SEARCH_PATIENT_LIST,
        params
    };
};

export const updatePatientListField = (fields) => {
    return {
        type: type.UPDATE_PATIENT_LIST_FIELD,
        fields
    };
};
export const getPatientList = (params) => {
    return {
        type: type.GET_PATIENT_LIST,
        params
    };
};
export const resetPatientListField = () => {
    return {
        type: type.RESET_PATIENT_LIST_FIELD
    };
};
export const resetLinkPatient = () => {
    return {
        type: type.RESET_LINK_PATIENT
    };
};

export const searchPatientPrecisely = (params) => {
    return {
        type: type.SEARCH_PATIENT_PRECISELY,
        params
    };
};

export const confirmAnonymousPatient = (params) => {
    return {
        type: type.CONFIRM_ANONYMOUS_PATIENT,
        params
    };
};

export const resetCondition = () => {
    return {
        type: type.RESET_CONDITION
    };
};


export const resetAttendance = (attenPara, searchPara) => {
    return {
        type: type.RESET_ATTENDANCE,
        attenPara,
        searchPara
    };
};

export const resetAttendanceSuccess = () => {
    return {
        type: type.RESET_ATTENDANCE_SUCCESS
    };
};

export const updatePatientListAttendanceInfo = (attendanceInfo) => {
    return {
        type: type.UPDATE_PATIENT_LIST_ATTENDANCEINFO,
        attendanceInfo
    };
};

export const searchInPatientQueue = (params) => {
    return {
        type: type.SEARCH_IN_PATIENT_QUEUE,
        params
    };
};


