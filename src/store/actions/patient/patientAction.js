import * as type from './patientActionType';

export const getPatientById = (patientKey, appointmentId = null, caseNo = null, callBack = null) => {
    return {
        type: type.GET_PATINET_BY_ID,
        patientKey,
        appointmentId,
        caseNo,
        callBack
    };
};

export const getPatientAppointment = (appointmentId, caseList) => {
    return {
        type: type.GET_PATIENT_APPOINTMENT,
        appointmentId,
        caseList
    };
};

export const getPatientCaseNo = (caseList, caseNo) => {
    return {
        type: type.GET_PATIENT_CASENO,
        caseList,
        caseNo
    };
};

export const getPatientEncounter = (appointmentId) => {
    return {
        type: type.GET_PATIENT_ENCOUNTER,
        appointmentId
    };
};

export const getCodeList = (params, putCode) => {
    return {
        type: type.GET_CODE_LIST,
        params,
        putCode
    };
};

export const resetAll = () => {
    return {
        type: type.RESET_ALL
    };
};

export const updateState = (updateData) => {
    return {
        type: type.UPDATE_STATE,
        updateData
    };
};

export const listNationalityAndListCountry = () => {
    return {
        type: type.LIST_NATIONALITY_AND_LIST_COUNTRY
    };
};

export const listPassport = () => {
    return {
        type: type.LIST_PASSPORT
    };
};

export const loadPassportList = (list) => {
    return {
        type: type.LOAD_PASSPORT_LIST,
        list
    };
};

export const loadEncounterInfo = (encounterInfo) => {
    return {
        type: type.LOAD_PATIENT_ENCOUNTER_INFO,
        encounterInfo
    };
};

export const getLanguageList = () => {
    return {
        type: type.GET_LANGUAGE_LIST
    };
};

export const getEHRUrl = (patientData, isEHRAccessRight, callBack = null) => {
    return {
        type: type.GET_EHR_URL,
        patientData,
        isEHRAccessRight,
        callBack
    };
};