const PRE = 'PATIENT';
export const GET_PATINET_BY_ID = `${PRE}_GET_PATINET_BY_ID`;
export const GET_CODE_LIST = `${PRE}_GET_CODE_LIST`;
export const RESET_ALL = `${PRE}_RESET_ALL`;
export const UPDATE_STATE = `${PRE}_UPDATE_STATE`;
export const GET_LANGUAGE_LIST = `${PRE}_GET_LANGUAGE_LIST`;

//sagas put
export const PUT_PATIENT_INFO = `${PRE}_PUT_PATIENT_INFO`;
export const PUT_GET_CODE_LIST = `${PRE}_PUT_GET_CODE_LIST`;
export const PUT_LANGUAGE_LIST = `${PRE}_PUT_LANGUAGE_LIST`;
export const PUT_EHR_URL = `${PRE}_PUT_EHR_URL`;

//list Nationality And List Country
export const LIST_NATIONALITY_AND_LIST_COUNTRY = `${PRE}_LIST_NATIONALITY_AND_LIST_COUNTRY`;
export const LOAD_NATIONALITY_LIST_AND_COUNTRY_LIST = `${PRE}_LOAD_NATIONALITY_LIST_AND_COUNTRY_LIST`;

//list passport list
export const LIST_PASSPORT = `${PRE}_LIST_PASSPORT`;
export const LOAD_PASSPORT_LIST = `${PRE}_LOAD_PASSPORT_LIST`;

//update patient appointment
export const GET_PATIENT_APPOINTMENT = `${PRE}_GET_PATIENT_APPOINTMENT`;
export const UPDATE_PATIENT_APPOINTMENT = `${PRE}_UPDATE_PATIENT_APPOINTMENT`;

//load patient encounter info
export const GET_PATIENT_ENCOUNTER = `${PRE}_GET_PATIENT_ENCOUNTER`;
export const LOAD_PATIENT_ENCOUNTER_INFO = `${PRE}_LOAD_PATIENT_ENCOUNTER_INFO`;

//update patient caseNo
export const GET_PATIENT_CASENO = `${PRE}_GET_PATIENT_CASENO`;
export const UPDATE_PATIENT_CASENO = `${PRE}_UPDATE_PATIENT_CASENO`;

//get eHR url
export const GET_EHR_URL = `${PRE}_GET_EHR_URL`;