import * as type from './mainFrameActionType';

export const addTabs = (params) => {
    return {
        type: type.ADD_TABS,
        params
    };
};
export const deleteTabs = (params) => {
    return {
        type: type.DELETE_TABS,
        params
    };
};
export const deleteSubTabs = (params) => {
    return {
        type: type.DELETE_SUB_TABS,
        params
    };
};
export const refreshSubTabs = (params) => {
    return {
        type: type.REFRESH_SUB_TABS,
        params
    };
};
export const cleanSubTabs = () => {
    return {
        type: type.CLEAN_SUB_TABS
    };
};
export const deleteSubTabsByOtherWay = (params) => {
    return {
        type: type.DELETE_SUB_TABS_BY_OTHERWAY,
        params
    };
};

export const changeTabsActive = (params) => {
    return {
        type: type.CHANGE_TABS_ACTIVE,
        params
    };
};
export const changeSubTabsActive = (key) => {
    return {
        type: type.CHANGE_SUB_TABS_ACTIVE,
        key
    };
};

export const changeEditMode = (params) => {
    return {
        type: type.CHANGE_EDIT_MODE,
        params
    };
};

export const resetAll = () => {
    return {
        type: type.RESET_ALL
    };
};

export const skipTab = (accessRightCd, params, checkExist = false) => {
    return {
        type: type.SKIP_TAB,
        accessRightCd,
        params,
        checkExist
    };
};

export const redirectTab = (sourceAccessId, destAccessId, params) => {
    return {
        type: type.REDIRECT_TAB,
        sourceAccessId,
        destAccessId,
        params
    };
};

export const updateTabLabel = (accessRightCd, newLabel) => {
    return {
        type: type.UPDATE_TAB_LABEL,
        accessRightCd,
        newLabel
    };
};

export const cleanTabParams = (tabName) => {
    return {
        type: type.CLEAN_TAB_PARAMS,
        tabName
    };
};

export const updateCurTab = (name, doCloseFunc) => {
    return {
        type: type.UPDATE_CURRENT_TAB,
        name,
        doCloseFunc
    };
};
