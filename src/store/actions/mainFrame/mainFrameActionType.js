const PRE = 'MAIN_FRAME';
export const ADD_TABS = `${PRE}_ADD_TABS`;
export const DELETE_TABS = `${PRE}_DELETE_TABS`;
export const CHANGE_TABS_ACTIVE = `${PRE}_CHANGE_TABS_ACTIVE`;
export const DELETE_SUB_TABS = `${PRE}_DELETE_SUB_TABS`;
export const REFRESH_SUB_TABS = `${PRE}_REFRESH_SUB_TABS`;
export const CLEAN_SUB_TABS = `${PRE}_CLEAN_SUB_TABS`;
export const CHANGE_SUB_TABS_ACTIVE = `${PRE}_CHANGE_SUB_TABS_ACTIVE`;
export const CHANGE_EDIT_MODE = `${PRE}_CHANGE_EDIT_MODE`;
export const RESET_ALL = `${PRE}_RESET_ALL`;

// Skip to other tab
export const SKIP_TAB = `${PRE}_SKIP_TAB`;
// Redirect to other tab and delete current tab
export const REDIRECT_TAB = `${PRE}_REDIRECT_TAB`;
//Update tab name
export const UPDATE_TAB_LABEL = `${PRE}_UPDATE_TAB_LABEL`;


//delete sub tabs by other way
export const DELETE_SUB_TABS_BY_OTHERWAY = `${PRE}_DELETE_SUB_TABS_BY_OTHERWAY`;
export const PUT_DELETE_SUB_TABS_BY_OTHERWAY = `${PRE}_PUT_DELETE_SUB_TABS_BY_OTHERWAY`;

//clean tab params
export const CLEAN_TAB_PARAMS = `${PRE}_CLEAN_TAB_PARAMS`;

//next patient new flow demo
export const UPDATE_CURRENT_TAB=`${PRE}_UPDATE_CURRENT_TAB`;

