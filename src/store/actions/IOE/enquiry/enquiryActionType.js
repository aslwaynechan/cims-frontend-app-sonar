const PREFFIX = 'JOS_ENQUIRY';
// for temporary test
export const REQUEST_ENQUIRY_CLINIC=`${PREFFIX}_REQUEST_ENQUIRY_CLINIC`;
export const FILLING_ENQUIRY_CLINIC=`${PREFFIX}_FILLING_ENQUIRY_CLINIC`;
export const getForms=`${PREFFIX}_GET_FORM_LIST`;
export const setState=`${PREFFIX}_SET_STATE`;
export const getPatients=`${PREFFIX}_GET_REQ_BY`;
export const getServices=`${PREFFIX}_GET_SERVICE_LIST`;
export const getClinics=`${PREFFIX}_GET_CLINICS`;
export const getHistoryList=`${PREFFIX}_GET_HISTORY_LIST`;
export const UPDATE_IOE_LABORATORY_FOLLOWUP_STATUS=`${PREFFIX}_UPDATE_IOE_LABORATORY_FOLLOWUP_STATUS`;
export const REQUEST_ORDER_DETAILS=`${PREFFIX}_REQUEST_ORDER_DETAILS`;

