import * as specimenCollectionActionType from './specimenCollectionActionType';

export const getIoeSpecimenCollectionList = ({params={},callback}) => {
  return {
    type: specimenCollectionActionType.GET_IOE_SPECIMEN_COLLECTION_LIST,
    params,
    callback
  };
};

export const saveIoeSpecimenCollectionList = ({params={},callback}) => {
  return {
    type: specimenCollectionActionType.SAVE_IOE_SPECIMEN_COLLECTION_LIST,
    params,
    callback
  };
};
