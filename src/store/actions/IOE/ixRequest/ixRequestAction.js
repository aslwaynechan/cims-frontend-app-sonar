import * as actionType from './ixRequestActionType';

export const getIxClinicList = ({params={},callback}) => {
  return {
    type: actionType.GET_IX_CLINIC_LIST,
    params,
    callback
  };
};

export const getIxRequestFrameworkList = ({params={},callback}) => {
  return {
    type: actionType.GET_IX_REQUEST_FRAMEWORK_LIST,
    params,
    callback
  };
};

export const getIxRequestItemDropdownList = ({params={},callback}) => {
  return {
    type: actionType.GET_IX_REQUEST_ITEM_DROPDOWN_LIST,
    params,
    callback
  };
};

export const getIxRequestOrderList = ({params={},callback}) => {
  return {
    type: actionType.GET_IX_REQUEST_ORDER_LIST,
    params,
    callback
  };
};

export const saveIxRequestOrder = ({params={},callback}) => {
  return {
    type: actionType.SAVE_IX_REQUEST_ORDER,
    params,
    callback
  };
};

export const getIxRequestSpecificMapping = ({params={},callback}) => {
  return {
    type: actionType.GET_IX_REQUEST_SPECIFIC_ITEM_MAPPING,
    params,
    callback
  };
};

export const getIxAllItemsForSearch = ({params={},callback}) => {
  return {
    type: actionType.GET_IX_ALL_ITEMS_FOR_SEARCH,
    params,
    callback
  };
};

export const getAllIxProfileTemplate = ({params={},callback}) => {
  return {
    type: actionType.GET_ALL_IX_PROFILE_TEMPLATE,
    params,
    callback
  };
};