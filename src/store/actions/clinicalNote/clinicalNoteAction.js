import * as clinicalNoteActionType from './clinicalNoteActionType';

export const requestRecordType = ({recordData={},callback}) => {
  return {
      type: clinicalNoteActionType.GET_RECORDTYPE_DATA_LIST,
      recordData,
      callback
  };
};

export const requestMedicalRecord = ({params={},callback}) => {
  return {
      type: clinicalNoteActionType.GET_MEDICAL_RECORD_LIST,
      params,
      callback
  };
};

export const getMedicalRecordNotesList = ({params={},callback}) => {
  return {
      type: clinicalNoteActionType.GET_MEDICALRECORD_LIST,
      params,
      callback
  };
};

export const requestTemplate = ({templateParams={},callback}) => {
  return {
      type: clinicalNoteActionType.GET_TEMPLATE_DATA_LIST,
      templateParams,
      callback
  };
};

export const saveRecordDetail = ({params={},callback}) => {
  return {
      type: clinicalNoteActionType.SAVE_RECORD_DETAIL_DATA,
      params,
      callback
  };
};

export const deleteRecordDetail = ({params={},callback}) => {
  return {
      type: clinicalNoteActionType.DELETE_RECORD_DETAIL_DATA,
      params,
      callback
  };
};

export const getSysConfig = () => {
  return {
      type: clinicalNoteActionType.GET_SYS_CONFIG
  };
};

export const getAmendmentHistoryList = ({params={},callback}) => {
  return {
    type: clinicalNoteActionType.GET_AMENDMENT_HISTORY_LIST,
    params,
    callback
  };
};

export const getCopyData = ({params={},callback}) => {
  return {
    type: clinicalNoteActionType.GET_COPY_DATA,
    params,
    callback
  };
};
