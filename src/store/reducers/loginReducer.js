import * as types from '../actions/login/loginActionType';
import moment from 'moment';
import _ from 'lodash';
import Enum from '../../enums/enum';

const initLoginInfo = {
   expiryTime: 420000,
   loginName: '',
   loginTime: '',
   userRoleType: '',
   token: '',
   ecsUserId:'',
   eHRId:''
};

const initService = {
   serviceCd: '',
   serviceName: '',
   cdFlag: 'Y'
};

const initClinic = {
   clinicCd: '',
   cdFlag: 'Y',
   clinicName: '',
   serviceCd: '',
   ecsLocCode:''
};

const INITAL_STATE = {
   loginInfo: _.cloneDeep(initLoginInfo),
   service: _.cloneDeep(initService),
   clinic: _.cloneDeep(initClinic),
   menuList: [],
   accessRights: [],
   noticeList: [],
   contactConfigs: [],
   isTemporaryLogin: '',
   isLoginSuccess: false,
   errorMessage: '',
   clientIp: null
};

function menuTree(data) {
   if (!data) {
      return [];
   }
   let tempMenu = {
      label: data.accessRightName,
      name: data.accessRightCd,
      icon: data.menuIconStr,
      path: data.componentPath,
      isPatRequired: data.isPatRequired,
      displayOrder: data.displayOrder
   };

   if (data.childAccessRightDtos.length === 0) {
      tempMenu.child = [];
      return tempMenu;
   }
   else {
      let child = [];
      data.childAccessRightDtos.forEach(chilRight => {
         let oneChild = menuTree(chilRight);
         child.push(oneChild);
      });
      tempMenu.child = child;
      return tempMenu;
   }
}

function fetchMenu(data) {
   let menu = [];
   data.forEach(right => {
      if (right.accessRightType === 'function') {
         let topLevel = menuTree(right);
         menu.push(topLevel);
      }
   });
   return menu;
}

function degraded(data, accessRights) {
   data.forEach(right => {
      let tempRight = {
         label: right.accessRightName,
         name: right.accessRightCd,
         icon: right.menuIconStr,
         path: right.componentPath,
         isPatRequired: right.isPatRequired,
         displayOrder: right.displayOrder,
         type: right.accessRightType
      };
      if (right.childAccessRightDtos.length === 0) {
         accessRights.push(tempRight);
      } else {
         accessRights.push(tempRight);
         degraded(right.childAccessRightDtos, accessRights);
      }
   });
}

function fetchAccessRights(data) {
   let accessRights = [];
   degraded(data, accessRights);
   return accessRights;
}

export default (state = INITAL_STATE, action) => {
   switch (action.type) {
      case types.LOGIN_SUCCESS: {
         let { loginInfo, service, clinic, isTemporaryLogin } = state;

         loginInfo.loginTime = moment().format(Enum.DATE_FORMAT_24_HOUR);
         loginInfo.loginName = action.loginInfo.login_name;
         loginInfo.expiryTime = action.loginInfo.expiry_time;
         loginInfo.userRoleType = action.loginInfo.userRoleType;
         loginInfo.token = action.loginInfo.token;
         loginInfo.ecsUserId = action.loginInfo.ecsUserId;

         service.serviceCd = action.loginInfo.service_cd;
         clinic.clinicCd = action.loginInfo.clinic_cd;
         clinic.ecsLocCode = action.loginInfo.ecsLocCode;

         isTemporaryLogin = action.loginInfo.temporary_login;

         const menuList = fetchMenu(action.loginInfo.accessRights);
         const accessRights = fetchAccessRights(action.loginInfo.accessRights);

         window.sessionStorage.setItem('loginInfo', JSON.stringify(loginInfo));
         window.sessionStorage.setItem('service', JSON.stringify(service));
         window.sessionStorage.setItem('clinic', JSON.stringify(clinic));
         window.sessionStorage.setItem('menuList', JSON.stringify(menuList));
         window.sessionStorage.setItem('accessRights', JSON.stringify(accessRights));

         return {
            ...state,
            loginInfo,
            service,
            clinic,
            menuList,
            accessRights,
            isTemporaryLogin,
            isLoginSuccess: true
         };
      }
      case types.LOGIN_ERROR: {
         return {
            ...state,
            errorMessage: action.errMsg,
            isLoginSuccess: false
         };
      }
      case types.LOGOUT: {
         sessionStorage.removeItem('loginInfo');
         sessionStorage.removeItem('service');
         sessionStorage.removeItem('clinic');
         sessionStorage.removeItem('token');
         sessionStorage.removeItem('menuList');
         sessionStorage.removeItem('accessRights');
         sessionStorage.removeItem('clinicConfig');
         sessionStorage.removeItem('listConfig');
         sessionStorage.removeItem('waitingList_tableScale');
         return {
            ...state,
            loginInfo: _.cloneDeep(initLoginInfo),
            service: _.cloneDeep(initService),
            clinic: _.cloneDeep(initClinic),
            errorMessage: '',
            menuList: [],
            accessRights: [],
            isTemporaryLogin: '',
            isLoginSuccess: false
         };
      }
      case types.ERROR_MESSAGE: {
         return {
            ...state,
            errorMessage: action.error
         };
      }
      case types.RESET_ERROR_MESSAGE: {
         return {
            ...state,
            errorMessage: ''
         };
      }
      case types.PUT_SERVICE_NOTICE: {
         return {
            ...state,
            noticeList: action.data
         };
      }
      case types.PUT_CONTACT_US: {
         return {
            ...state,
            contactConfigs: action.data
         };
      }
      case types.PUT_CLIENT_IP: {
         return {
            ...state,
            clientIp: action.data
         };
      }
      case types.PUT_LOGIN_INFO: {
         let { curLoginServiceAndClinic } = action;
         return {
            ...state,
            service: curLoginServiceAndClinic.service,
            clinic: curLoginServiceAndClinic.clinic
         };
      }
      default:
         return { ...state };
   }
};