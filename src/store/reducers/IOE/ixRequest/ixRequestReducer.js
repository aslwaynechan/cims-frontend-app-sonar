import * as type from '../../../actions/IOE/ixRequest/ixRequestActionType';

const INIT_STATE = {
  clinicList: [],
  frameworkMap:new Map(),
  lab2FormMap:new Map(),
  dropdownMap:new Map(),
  ioeFormMap:new Map(),
  itemMapping:new Map(),
  categoryMap:new Map(),
  labOptions:[],
  searchItemList: []
};

export default (state = INIT_STATE, action = {}) => {
  switch (action.type) {
    case type.IX_CLINIC_LIST:{
      return{
        ...state,
        clinicList:action.clinicList
      };
    }
    case type.IX_REQUEST_FRAMEWORK_LIST:{
      return{
        ...state,
        frameworkMap:action.frameworkMap,
        lab2FormMap:action.lab2FormMap,
        ioeFormMap:action.ioeFormMap,
        labOptions:action.labOptions
      };
    }
    case type.IX_REQUEST_ITEM_DROPDOWN_LIST:{
      return{
        ...state,
        dropdownMap:action.dropdownMap
      };
    }
    case type.IX_REQUEST_SPECIFIC_ITEM_MAPPING:{
      return{
        ...state,
        itemMapping:action.itemMapping
      };
    }
    case type.IX_ALL_ITEMS_FOR_SEARCH:{
      return{
        ...state,
        searchItemList:action.searchItemList
      };
    }
    case type.ALL_IX_PROFILE_TEMPLATE:{
      return{
        ...state,
        categoryMap:action.categoryMap
      };
    }
    default:
      return state;
  }
};