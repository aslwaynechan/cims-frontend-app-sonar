import * as types from '../actions/common/commonActionType';
import Enum from '../../enums/enum';
import * as commonUtilities from '../../utilities/commonUtilities';
// import * as LoginActionTypes from '../actions/Login/LoginActionType';

export const initState = {
    encounterTypeList: [],
    serviceList: [],
    clinicList: [],
    //common error dialog
    openErrorDialog: false,
    errorMessage: '',
    errorData: [],
    errorTitle: '',
    //common circular
    openCommonCircular: false,
    //searchBar
    openSearchProgress: false,
    searchBarValue: '',
    keepDataSelected: false,
    //timer
    timerStatus: Enum.TIMER_STOPPED,
    timerSeconds: 0,
    //snackbar
    warnSnackbarStatus: false,
    warnSnackbarMessage: '',
    //clinic config
    clinicConfig: !sessionStorage.getItem('clinicConfig') ? {} : JSON.parse(sessionStorage.getItem('clinicConfig')),
    //list config
    listConfig: !sessionStorage.getItem('listConfig') ? {} : JSON.parse(sessionStorage.getItem('listConfig')),
    //prompt
    openPrompt: false,
    // hospital and clinic list
    hospitalAndClinicList: null,
    openCommonCircularDialog: false
};
export default (state = initState, action = {}) => {
    switch (action.type) {
        case types.ENCOUNTER_TYPE: {
            return { ...state, encounterTypeList: action.data };
        }
        case types.PUT_LIST_SERVICE: {
            return { ...state, serviceList: action.serviceList };
        }
        case types.PUT_LIST_CLINIC: {
            return { ...state, clinicList: action.clinicList };
        }

        /**Common error dialog */
        case types.OPEN_ERROR_MESSAGE: {
            return {
                ...state,
                openErrorDialog: true,
                errorMessage: action.error || '',
                errorData: action.data,
                errorTitle: action.errorTitle
            };
        }
        case types.CLOSE_ERROR_MESSAGE: {
            return {
                ...state,
                openErrorDialog: false
            };
        }
        /**Common error dialog */

        /**Common circular */
        case types.OPEN_COMMON_CIRCULAR: {
            return {
                ...state,
                openCommonCircular: true
            };
        }
        case types.CLOSE_COMMON_CIRCULAR: {
            return {
                ...state,
                openCommonCircular: false
            };
        }
        /**Common circular */

        /**Search bar */
        case types.OPEN_SEARCH: {
            return { ...state, openSearchProgress: true };
        }
        case types.CLOSE_SEARCH: {
            return { ...state, openSearchProgress: false };
        }
        case types.UPDATE_SEARCHBAR_VALUE: {
            let isKeepData = typeof (action.isKeepData) === 'boolean' ? action.isKeepData : state.keepDataSelected;
            return { ...state, searchBarValue: action.value, keepDataSelected: isKeepData };
        }
        /**Search bar */

        /**Timer */
        case types.TIMER_START: {
            return { ...state, timerStatus: Enum.TIMER_RUNNING };
        }
        case types.TIMER_STOP: {
            return { ...state, timerStatus: Enum.TIMER_STOPPED };
        }
        case types.TIMER_TICK: {
            return { ...state, timerSeconds: state.timerSeconds + 1 };
        }
        case types.TIMER_RESET: {
            return { ...state, timerSeconds: 0 };
        }
        /**Timer */

        /**Snack bar */
        case types.OPEN_WARN_SNACKBAR: {
            return { ...state, warnSnackbarStatus: true, warnSnackbarMessage: action.message };
        }
        case types.CLOSE_WARN_SNACKBAR: {
            return { ...state, warnSnackbarStatus: false };
        }
        /**Snack bar */

        case types.UPDATE_CONFIG: {
            let newState = { ...state };
            newState.clinicConfig = { ...state.clinicConfig, ...action.clinicConfig };
            sessionStorage.setItem('clinicConfig', JSON.stringify(newState.clinicConfig));
            return newState;
        }

        case types.UPDATE_LIST_CONFIG: {
            let newState = { ...state };
            let config = { ...action.listConfig };
            config.PATIENT_LIST = commonUtilities.getPriorityListConfig(config.PATIENT_LIST, 'patientlist', action.loginInfo.userRoleType, action.loginInfo.service_cd);
            config.WAITING_LIST = commonUtilities.getPriorityListConfig(config.WAITING_LIST, 'waitinglist', action.loginInfo.userRoleType, action.loginInfo.service_cd);
            newState.listConfig = config;
            sessionStorage.setItem('listConfig', JSON.stringify(newState.listConfig));
            return newState;
        }

        /** Prompt */
        case types.OPEN_PROMPT_MESSAGE: {
            return {
                ...state,
                openPrompt: true
            };
        }
        case types.CLOSE_PROMPT_MESSAGE: {
            return {
                ...state,
                openPrompt: false
            };
        }

        /**Common circular dialog*/
        case types.OPEN_COMMON_CIRCULAR_DIALOG: {
            return {
                ...state,
                openCommonCircularDialog: true
            };
        }
        case types.CLOSE_COMMON_CIRCULAR_DIALOG: {
            return {
                ...state,
                openCommonCircularDialog: false
            };
        }

        /**list hospital start */
        case types.LOAD_HOSPITAL_AND_CLINIC_LIST: {
            let { list } = action;
            return { ...state, hospitalAndClinicList: list };
        }
        /**list hospital end */

        default:
            return state;
    }
};
