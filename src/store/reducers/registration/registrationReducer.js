import _ from 'lodash';
import * as RegistrationType from '../../actions/registration/registrationActionType';
import ButtonStatusEnum from '../../../enums/registration/buttonStatusEnum';
import { patientBasic, patientSocialDataBasic, patientContactInfoBasic, patientBaseInfoBasic } from '../../../constants/registration/registrationConstants';
import * as RegUtil from '../../../utilities/registrationUtilities';
import * as PatientUtil from '../../../utilities/patientUtilities';

const initState = {
    patientOperationStatus: ButtonStatusEnum.VIEW,
    patientList: [],
    patientById: _.cloneDeep(patientBasic),
    patientBaseInfo: _.cloneDeep(patientBaseInfoBasic),
    patientContactInfo: _.cloneDeep(patientContactInfoBasic),
    patientSocialData: _.cloneDeep(patientSocialDataBasic),
    contactPersonList: [],
    addressList: [],
    phoneList: [],
    contactInfoSaveAbove: false,
    codeList: {},
    isOpenReview: false,
    ccCodeChiChar: [],
    isSaveSuccess: false
};

export default (state = initState, action) => {
    switch (action.type) {
        case RegistrationType.RESET_ALL: {
            const { codeList } = state;
            return {
                ...initState,
                phoneList: RegUtil.initPatientPhone(),
                addressList: RegUtil.initPatientAddress(),
                codeList: codeList
            };
        }
        case RegistrationType.UPDATE_PATIENT_OPERATE_STATUS: {
            return {
                ...state,
                patientOperationStatus: action.status
            };
        }
        case RegistrationType.UPDATE_STATE: {
            let lastAction = { ...state };
            for (let p in action.updateData) {
                lastAction[p] = action.updateData[p];
            }
            return lastAction;
        }
        case RegistrationType.PUT_PATIENT_BY_ID: {
            if (action.data) {
                let patientById = _.cloneDeep(action.data);

                //transfer
                patientById = PatientUtil.transferPatientDocumentPair(patientById);
                patientById.hkid = PatientUtil.getHkidFormat(patientById.hkid);

                let phoneList = patientById.phoneList && patientById.phoneList.filter(item => item.phonePriority !== 1);
                if (!phoneList || phoneList.length < 1) {
                    phoneList = RegUtil.initPatientPhone();
                }
                let addressList = patientById.addressList && patientById.addressList.length > 0 ? patientById.addressList : RegUtil.initPatientAddress();
                let patientBaseInfo = RegUtil.initPatientBaseInfo(patientById);
                let ccCodeChiChar = [];
                for (let i = 5; i >= 0; i--) {
                    if (patientBaseInfo[`ccCode${i + 1}`]) {
                        ccCodeChiChar[i] = patientBaseInfo.nameChi[i];
                    }
                }
                return {
                    ...state,
                    patientById: patientById,
                    phoneList: phoneList,
                    addressList: addressList,
                    contactPersonList: patientById.contactPersonList,
                    patientSocialData: RegUtil.initPatientSocialData(patientById),
                    patientContactInfo: RegUtil.initPatientContactInfo(patientById),
                    patientBaseInfo: patientBaseInfo,
                    patientOperationStatus: ButtonStatusEnum.DATA_SELECTED,
                    ccCodeChiChar: ccCodeChiChar
                };
            }
            return { ...state };
        }
        case RegistrationType.PUT_GET_CODE_LIST: {
            return { ...state, codeList: action.codeList };
        }
        case RegistrationType.PUT_PATIENT: {
            let { patientBaseInfo } = state;
            if (action.status === ButtonStatusEnum.ADD) {
                if (!RegUtil.IsHKIDbyJS(patientBaseInfo.hkid)) {
                    patientBaseInfo.hkid = '';
                }
            }
            return { ...state, patientList: action.data, patientOperationStatus: action.status, patientBaseInfo };
        }
        case RegistrationType.UPDATE_CHI_CHAR: {
            let patientBaseInfo = _.cloneDeep(state.patientBaseInfo);
            let ccCodeChiChar = [...state.ccCodeChiChar];
            if (!action.char) {
                //返回字符为null时，清空该ccCode当前和以后的值
                for (let i = 5; i >= action.charIndex; i--) {
                    patientBaseInfo[`ccCode${i + 1}`] = '';
                    ccCodeChiChar[i] = '';
                }
            } else {
                ccCodeChiChar[action.charIndex] = patientBaseInfo[`ccCode${action.charIndex + 1}`] ? action.char : '';
            }
            patientBaseInfo.nameChi = ccCodeChiChar.join('');
            return { ...state, patientBaseInfo, ccCodeChiChar };
        }
        case RegistrationType.UPDATE_CC_CODE: {
            let patientBaseInfo = _.cloneDeep(state.patientBaseInfo);
            let ccCodeList = action.ccCodeList || [];
            let ccCodeChiChar = [...state.ccCodeChiChar];
            ccCodeList.forEach(item => {
                patientBaseInfo[`ccCode${item.index + 1}`] = item.code;
                ccCodeChiChar[item.index] = '';

                //清空该ccCode以后的值
                for (let i = 5; i > item.index; i--) {
                    patientBaseInfo[`ccCode${i + 1}`] = '';
                    ccCodeChiChar[i] = '';
                }
                patientBaseInfo.nameChi = ccCodeChiChar.join('');
            });
            return { ...state, patientBaseInfo, ccCodeChiChar };
        }
        default:
            return { ...state };
    }
};