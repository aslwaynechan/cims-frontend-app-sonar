import * as type from '../../../actions/certificate/sickLeave/sickLeaveActionType';
import moment from 'moment';
import Enum from '../../../../enums/enum';
const INITAL_STATE = {
    newLeaveInfo: {
        to: 'Whom it may concern',
        sufferFrom: '',
        dateRange: {
            leaveFrom: moment().format(Enum.DATE_FORMAT_EYMD_VALUE),
            leaveTo: moment().format(Enum.DATE_FORMAT_EYMD_VALUE),
            period: 1,
            section: '',
            leaveToSection:'PM'
        },
        remark: ''
    },
    allowCopyList: [
        { value: 1, desc: '1' },
        { value: 2, desc: '2' },
        { value: 3, desc: '3' },
        { value: 4, desc: '4' },
        { value: 5, desc: '5' }
    ],
    copyPage: 1,
    handlingPrint: false
};

export default (state = INITAL_STATE, action = {}) => {
    switch (action.type) {
        case type.RESET_ALL: {
            return INITAL_STATE;
        }

        case type.UPDATE_FIELD: {
            let lastAction = { ...state };

            for (let p in action.updateData) {
                lastAction[p] = action.updateData[p];
            }
            return lastAction;
        }

        default:
            return state;
    }
};
