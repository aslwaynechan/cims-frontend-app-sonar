import * as type from '../../../actions/certificate/referralLetter/referralLetterActionType';
import _ from 'lodash';
const INITAL_STATE = {
    newLetterInfo: {
        to: 'The Professor / Doctor',
        problem: '',
        result: '',
        appointmentType: 'Normal Appointment',
        referTo: {
            groupCd: null,
            hosptialClinicName: null,
            specialty: null
        }
    },
    allowCopyList: [
        { value: 1, desc: '1' },
        { value: 2, desc: '2' },
        { value: 3, desc: '3' },
        { value: 4, desc: '4' },
        { value: 5, desc: '5' }
    ],
    copyPage: 1,
    handlingPrint: false,
    referToOpts: [],
    specialtyList: []
};

export default (state = _.cloneDeep(INITAL_STATE), action) => {
    switch (action.type) {
        case type.RESET_ALL: {
            return _.cloneDeep(INITAL_STATE);
        }
        case type.UPDATE_FIELD: {
            let lastAction = { ...state };

            for (let p in action.updateData) {
                lastAction[p] = action.updateData[p];
            }
            return lastAction;
        }
        default: {
            return state;
        }
    }
};