import * as types from '../actions/mainFrame/mainFrameActionType';
import _ from 'lodash';
import * as CommonUtilities from '../../utilities/commonUtilities';

const INITAL_STATE = {
    tabs: [],
    tabsActiveKey: '',
    subTabs: [],
    subTabsActiveKey: '',
    editModeTabs: []
};

function getActivityKeyAfterDelete(currentKey, deleteKey, tabArray, newTabArray) {
    let finalKey = currentKey;
    if (currentKey === deleteKey) {
        let tabIndex = tabArray.findIndex(item => item.name === currentKey);
        if (newTabArray.length - 1 < tabIndex) {
            tabIndex = newTabArray.length - 1;
        }
        return newTabArray[tabIndex].name;
    }
    return finalKey;
}

export default (state = _.cloneDeep(INITAL_STATE), action) => {
    switch (action.type) {
        case types.ADD_TABS: {
            let newState = _.cloneDeep(state);
            if (action.params.isPatRequired === 'Y') {
                let reTabsIndex = newState.tabs.findIndex((item) => { return item.name === 'F002' && item.label === 'Registration'; });
                if (reTabsIndex != -1) {
                    return newState;
                }
                let tabsIndex = newState.tabs.findIndex((item) => { return item.name === 'patientSpecFunction'; });
                let subTabsIndex = newState.subTabs.findIndex(item => { return item.name === action.params.name; });
                if (tabsIndex === -1) {
                    let tab = {
                        name: 'patientSpecFunction',
                        label: `${CommonUtilities.getPatientCall()}-specific Function(s)`,
                        disableClose: true,
                        path: 'indexPatient',
                        doCloseFunc: null
                    };
                    newState.tabs.splice(0, 0, tab);
                    newState.tabsActiveKey = tab.name;
                } else {
                    newState.tabsActiveKey = newState.tabs[tabsIndex].name;
                }

                if (subTabsIndex === -1) {
                    let subTab = {
                        name: action.params.name, label: action.params.label,
                        disableClose: action.params.disableClose,
                        params: action.params.params,
                        path: action.params.path,
                        doCloseFunc: null
                    };
                    newState.subTabs.push(subTab);
                    newState.subTabsActiveKey = subTab.name;
                } else {
                    newState.subTabsActiveKey = newState.subTabs[subTabsIndex].name;
                }
            } else {
                if (action.params.name === 'F002') {
                    if (newState.subTabs.length > 0) {
                        return newState;
                    }
                    let tabsIndex = newState.tabs.findIndex((item) => { return item.name === 'patientSpecFunction'; });
                    if (tabsIndex != -1) {
                        newState.tabs.splice(0, 1);
                    }
                } else if (action.params.name == 'patientSpecFunction') {
                    let regOrSumTabsIndex = newState.tabs.findIndex((item) => { return item.name == 'F002'; });
                    if (regOrSumTabsIndex != -1) {
                        return newState;
                    }
                }

                let tabsIndex = newState.tabs.findIndex((item) => { return item.name === action.params.name; });
                if (tabsIndex === -1) {
                    let tab = {
                        name: action.params.name,
                        label: action.params.label,
                        disableClose: action.params.disableClose,
                        params: action.params.params,
                        path: action.params.path,
                        doCloseFunc: null
                    };
                    newState.tabs.push(tab);
                    newState.tabsActiveKey = tab.name;
                } else {
                    newState.tabsActiveKey = newState.tabs[tabsIndex].name;
                }
            }
            return newState;
        }
        case types.DELETE_TABS: {
            let newState = { ...state };
            newState.tabs = state.tabs.filter(item => item.name !== action.params);
            if (newState.tabs.length === 0) {
                newState.tabsActiveKey = '';
            } else {
                newState.tabsActiveKey = getActivityKeyAfterDelete(state.tabsActiveKey, action.params, state.tabs, newState.tabs);
            }
            newState.editModeTabs = state.editModeTabs.filter(item => item.name !== action.params);
            if (action.params == 'F002') {
                let tabsIndex = newState.tabs.findIndex((item) => { return item.name === 'patientSpecFunction'; });
                if (tabsIndex === -1) {
                    let tab = {
                        name: 'patientSpecFunction',
                        label: `${CommonUtilities.getPatientCall()}-specific Function(s)`,
                        disableClose: true,
                        path: 'indexPatient',
                        doCloseFunc: null
                        //isPatRequired :'Y'
                    };
                    newState.tabs.splice(0, 0, tab);
                    newState.tabsActiveKey = tab.name;
                } else {
                    newState.tabsActiveKey = newState.tabs[tabsIndex].name;
                }
            }
            return newState;
        }
        case types.DELETE_SUB_TABS: {
            let newState = { ...state };
            newState.subTabs = state.subTabs.filter(item => item.name !== action.params);
            if (newState.subTabs.length === 0) {
                newState.tabsActiveKey = 'patientSpecFunction';
                newState.subTabsActiveKey = '';
            } else {
                newState.subTabsActiveKey = getActivityKeyAfterDelete(state.subTabsActiveKey, action.params, state.subTabs, newState.subTabs);
            }
            newState.editModeTabs = state.editModeTabs.filter(item => item.name !== action.params);
            return newState;
        }
        case types.REFRESH_SUB_TABS: {
            let newState = { ...state };
            let subTab = { name: action.params.name, label: action.params.label, component: action.params.name, disableClose: action.params.disableClose, params: action.params.params };
            newState.subTabs = [subTab];
            newState.subTabsActiveKey = subTab.name;
            return newState;
        }
        case types.CLEAN_SUB_TABS: {
            let newState = { ...state };
            newState.subTabs = [];
            newState.subTabsActiveKey = '';
            return newState;
        }
        case types.PUT_DELETE_SUB_TABS_BY_OTHERWAY: {
            return { ...state };
        }
        case types.CHANGE_TABS_ACTIVE: {
            return { ...state, tabsActiveKey: action.params };
        }
        case types.CHANGE_SUB_TABS_ACTIVE: {
            return { ...state, subTabsActiveKey: action.key };
        }
        case types.CHANGE_EDIT_MODE: {
            let newState = { ...state };
            let index = newState.editModeTabs.findIndex(item => item.name === action.params.name);
            if (index === -1 && action.params.isEdit) {
                newState.editModeTabs.push(action.params);
                // newState.editModeTabs.push(action.params.name);
            } else if (index !== -1 && !action.params.isEdit) {
                newState.editModeTabs.splice(index, 1);
            }
            return newState;
        }
        case types.UPDATE_TAB_LABEL: {
            let tabs = _.cloneDeep(state.tabs);
            let subTabs = _.cloneDeep(state.subTabs);
            let tabIndex = tabs.findIndex(item => item.name === action.accessRightCd);
            if (tabIndex > -1) {
                tabs[tabIndex]['label'] = action.newLabel;
            } else {
                let subTabIndex = subTabs.find(item => item.name === action.accessRightCd);
                if (subTabIndex > -1) {
                    subTabs[subTabIndex]['label'] = action.newLabel;
                }
            }
            return {
                ...state,
                tabs: tabs,
                subTabs: subTabs
            };
        }
        case types.CLEAN_TAB_PARAMS: {
            let tabs = _.cloneDeep(state.tabs);
            let tabIndex = tabs.findIndex(item => item.name === action.tabName);
            if (tabIndex > -1) {
                tabs[tabIndex]['params'] = null;
            }

            let subTabs = _.cloneDeep(state.subTabs);
            let subTabIndex = subTabs.findIndex(item => item.name === action.tabName);
            if (subTabIndex > -1) {
                subTabs[subTabIndex]['params'] = null;
            }
            return {
                ...state,
                tabs: tabs,
                subTabs: subTabs
            };
        }
        case types.RESET_ALL: {
            return _.cloneDeep(INITAL_STATE);
        }

        case types.UPDATE_CURRENT_TAB: {
            let { name, doCloseFunc } = action;
            let newState = { ...state };
            let curTab = null;
            if (name === newState.tabsActiveKey) {
                curTab = newState.tabs.find(item => item.name === name);
                // curTab.doCloseFunc=doCloseFunc;
            }
            else if (name === newState.subTabsActiveKey) {
                curTab = newState.subTabs.find(item => item.name === name);
            }
            curTab.doCloseFunc = doCloseFunc;
            return newState;
        }
        default:
            return { ...state };
    }
};
