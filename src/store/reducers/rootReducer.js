import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage/session';
import LoginReducer from './loginReducer';
import hkicReducer from './hkicReducer';
import UserProfileReducer from './administration/userProfile/userProfileReducer';
import RegistrationReducer from './registration/registrationReducer';
import TimeslotTemplateReducer from './appointment/timeSlot/timeslotTemplateReducer';
import GenerateTimeSlotReducer from './appointment/timeSlot/generateTimeSlotReducer';
import EditTimeSlotReducer from './appointment/timeSlot/editTimeSlotReducer';
import { booking, bookingInformation } from './appointment/booking/bookingReducer';
import UserRoleReducer from './administration/userRole/userRoleReducer';
import CommonReducer from './commonReducer';
import ChangePasswordReducer from './administration/changePassword/changePasswordReducer';
import AttendenceReducer from './appointment/attendance/attendanceReducer';
import PatientReducer from './patient/patientReducer';
import ConsultationReducer from './consultation/consultationReducer';
import clinicalNoteReducer from './clinicalNote/clinicalNoteReducer';
import assessmentReducer from './assessment/assessmentReducer';
import PrescriptionReducer from './consultation/prescription/prescriptionReducer';
import AppointmentSlipFooterReducer from './administration/appointmentSlipFooter/appointmentSlipFooterReducer';
import EncounterTypeManagementReducer from './administration/encounterTypeManagement/encounterTypeManagementReducer';
import moeReducer from './moe/moeReducer';
import forgetPasswordReducer from './forgetPasswordReducer';
import mainFrameReducer from './mainFrameReducer';
import myFavouriteReducer from './moe/myFavourite/myFavouriteReducer';
import drugHistoryReducer from './moe/drugHistory/drugHistoryReducer';
import moePrintReducer from './moe/moePrintReducer';
import { bookingAnonymousInformation, bookingAnonymous } from './appointment/booking/bookingAnonymousReducer';
import waitingListReducer from './appointment/waitingListReducer';
import manageClinicalNoteTemplateReducer from './clinicalNote/manageClinicalNoteTemplateReducer';
import medicalSummaryReducer from './medicalSummary/medicalSummaryReducer';
import diagnosisReducer from './diagnosis/diagnosisReducer';
import procedureReducer from './procedure/procedureReducer';
import tokenTemplateManagementReducer from './IOE/tokenTemplateManagement/tokenTemplateManagementReducer';
import turnaroundTimeReducer from './IOE/turnaroundTime/turnaroundTimeReducer';
import specimenCollectionReducer from './IOE/specimenCollection/specimenCollectionReducer';
import serviceProfileReducer from './IOE/serviceProfile/serviceProfileReducer';
import AttendanceCertReducer from './certificate/attendanceCertificate/attendanceCertReducer';
import SickLeaveReducer from './certificate/sickLeave/sickLeaveReducer';
import ReferralLetterReducer from './certificate/referralLetter/referralLetterReducer';
import CalendarViewReducer from './appointment/calendarViewReducer';
import MessageReducer from './message/messageReducer';
import YellowFeverReducer from './certificate/yellowFever/yellowFeverReducer';
import PublicHolidayReducer from './administration/publicHoliday/publicHolidayReducer';
import PatientSpecFuncReducer from './patient/patientSpecFuncReducer';
import DepartmentFavouriteReducer from './moe/departmentFavourite/departmentFavouriteReducer';
import backgroundInformationReducer from './MRAM/backgroundInformation/backgroundInformationReducer';
import eyesReducer from './MRAM/eyes/eyesReducer';
import feetReducer from './MRAM/feet/feetReducer';
import measurementAndLabTestReducer from './MRAM/measurementAndLabTest/measurementAndLabTestReducer';
import otherComplicationsReducer from './MRAM/otherComplications/otherComplicationsReducer';
import riskProfileReducer from './MRAM/riskProfile/riskProfileReducer';
import carePlanReducer from './MRAM/carePlan/carePlanReducer';
import mramReducer from './MRAM/mramReducer';
import dietAssessmentReducer from './MRAM/dietAssessment/dietAssessmentReducer';
import apptEnquiryReducer from './appointment/apptEnquiry/apptEnquiryReducer';
import yellowFeverCertReducer from './certificate/yellowFeverCert/yellowFeverCertReducer';
import enquiryReducer from './IOE/enquiry/enquiryReducer';
import laboratoryReportReducer from './IOE/laboratoryReport/laboratoryReportReducer';
import {reducers} from 'store/models';
import ixRequestReducer from './IOE/ixRequest/ixRequestReducer';
import caseNoReducer from './caseNo/caseNoReducer';
import BackTakeAttendanceReducer from './appointment/backTakeAttendacne/backTakeAttendanceReducer';
import ecsReducer from './ECS/ecsReducer';

const clinicalNotePresistConfig = {
  key: 'clinicalNote',
  storage: storage,
  whitelist: ['sysConfig']
};

const messagePresistConfig = {
  key: 'message',
  storage: storage,
  blacklist: ['openMessageDialog', 'openSnackbar']
};

const loginPressistConfig = {
  key: 'login',
  storage: storage,
  blacklist: ['isLoginSuccess', 'errorMessage']
};

const commonPressistConfig = {
  key: 'common',
  storage: storage,
  whitelist: ['clinicConfig', 'listConfig', 'serviceList', 'clinicList']
};

const patientPressistConfig = {
  key: 'patient',
  storage: storage,
  whitelist: ['nationalityList', 'countryList']
};

const registrationPressistConfig = {
  key: 'registration',
  storage: storage,
  whitelist: ['codeList']
};

const caseNoPressistConfig = {
  key: 'caseNo',
  storage: storage,
  whitelist: ['casePrefixList', 'isAutoGen', 'codeListDtos']
};

const rootReducer = combineReducers({
  config: (state = {}) => state,
  login: persistReducer(loginPressistConfig, LoginReducer),
  hkicReducer,
  common: persistReducer(commonPressistConfig, CommonReducer),
  userProfile: UserProfileReducer,
  userRole: UserRoleReducer,
  registration: persistReducer(registrationPressistConfig, RegistrationReducer),
  timeslotTemplate: TimeslotTemplateReducer,
  generateTimeSlot: GenerateTimeSlotReducer,
  editTimeSlot: EditTimeSlotReducer,
  booking: booking,
  bookingInformation: bookingInformation,
  bookingAnonymous: bookingAnonymous,
  bookingAnonymousInformation: bookingAnonymousInformation,
  changePassword: ChangePasswordReducer,
  attendance: AttendenceReducer,
  patient: persistReducer(patientPressistConfig, PatientReducer),
  consultation: ConsultationReducer,
  prescription: PrescriptionReducer,
  appointmentSlipFooter: AppointmentSlipFooterReducer,
  encounterTypeManagement: EncounterTypeManagementReducer,
  moe: moeReducer,
  forgetPassword: forgetPasswordReducer,
  mainFrame: mainFrameReducer,
  moeMyFavourite: myFavouriteReducer,
  moePrint: moePrintReducer,
  waitingList: waitingListReducer,
  moeDrugHistory: drugHistoryReducer,
  assessment: assessmentReducer,
  clinicalNote: persistReducer(clinicalNotePresistConfig, clinicalNoteReducer),
  manageClinicalNoteTemplate: manageClinicalNoteTemplateReducer,
  medicalSummary: medicalSummaryReducer,
  procedureReducer: procedureReducer,
  diagnosisReducer: diagnosisReducer,
  tokenTemplateManagement: tokenTemplateManagementReducer,
  turnaroundTime: turnaroundTimeReducer,
  specimenCollection: specimenCollectionReducer,
  serviceProfile: serviceProfileReducer,
  attendanceCert: AttendanceCertReducer,
  sickLeave: SickLeaveReducer,
  referralLetter: ReferralLetterReducer,
  calendarView: CalendarViewReducer,
  message: persistReducer(messagePresistConfig, MessageReducer),
  yellowFever: YellowFeverReducer,
  yellowFeverCert: yellowFeverCertReducer,
  publicHoliday: PublicHolidayReducer,
  patientSpecFunc: PatientSpecFuncReducer,
  departmentFavourite: DepartmentFavouriteReducer,
  backgroundInformation: backgroundInformationReducer,
  eyes: eyesReducer,
  feet: feetReducer,
  measurementAndLabTest: measurementAndLabTestReducer,
  otherComplications: otherComplicationsReducer,
  riskProfile: riskProfileReducer,
  mram: mramReducer,
  dietAssessment: dietAssessmentReducer,
  carePlan: carePlanReducer,
  apptEnquiry: apptEnquiryReducer,
  enquiry:enquiryReducer,
  laboratoryReport:laboratoryReportReducer,
  ixRequest:ixRequestReducer,
  ...reducers,
  caseNo: persistReducer(caseNoPressistConfig, caseNoReducer),
  backTakeAttendance:BackTakeAttendanceReducer,
  ecs: ecsReducer
});

export default rootReducer;

