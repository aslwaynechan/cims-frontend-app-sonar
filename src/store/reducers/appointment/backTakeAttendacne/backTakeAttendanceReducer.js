import * as backTakeAttendanceActionTypes from '../../../actions/appointment/backTakeAttendance/backTakeAttendanceAcitonType';
const INITAL_STATE = {
    //patient queue
    // searchParameter: {
    //     dateFrom: moment(),
    //     dateTo: moment(),
    //     attnStatusCd: 'N',
    //     encounterTypeCd: '',
    //     subEncounterTypeCd: '',
    //     patientKey: '',
    //     type: Enum.LANDING_PAGE.ATTENDANCE,
    //     page: 1,
    //     pageSize: 10
    // },
    // showTakeAttendance: false,
    //mark attendance
    patientStatusList: [],
    patientStatus: '',
    patientStatusFlag: 'N',
    discNum: '',
    isAttend: false,
    appointmentForAttendance: null,
    appointmentConfirmInfo: null,
    patientQueueList: [],
    enableWalkIn: false,
    openPromptDialog: false,
    appointmentList: [],
    currentAppointment: null
};

export default (state = INITAL_STATE, action) => {
    switch (action.type) {
        case backTakeAttendanceActionTypes.UPDATE_FIELD: {
            let lastState = { ...state };
            for (let p in action.updateData) {
                lastState[p] = action.updateData[p];
            }
            return lastState;
        }

        case backTakeAttendanceActionTypes.RESET_ALL: {
            return INITAL_STATE;
        }

        case backTakeAttendanceActionTypes.PATIENT_STATUS_LIST: {
            return {
                ...state,
                patientStatusList: action.data ? action.data.patient_status : []
            };
        }

        case backTakeAttendanceActionTypes.PUT_APPOINTMENT_LIST: {
            let tempAppointmentList = { ...action.appointmentList };
            return {
                ...state,
                appointmentList: tempAppointmentList
            };
        }

        case backTakeAttendanceActionTypes.BACK_TAKE_ATTENDANCE_SUCCESS: {
            return {
                ...state,
                isAttend: true,
                appointmentConfirmInfo: action.data,
                currentAppointment: null
            };
        }

        default:
            return state;
    }
};