import * as BookingAnonymousActionType from '../../../actions/appointment/booking/bookingAnonymousActionType';
import moment from 'moment';
import _ from 'lodash';
import FieldConstant from '../../../../constants/fieldConstant';
import * as appointmentUtilities from '../../../../utilities/appointmentUtilities';
import { initBookingData, initEncounterType } from '../../../../constants/appointment/bookingInformation/bookingInformationConstants';
import Enum from '../../../../enums/enum';
import * as CommonUtilities from '../../../../utilities/commonUtilities';

const initState = {
    showMakeAppointmentView: false,
    patientInfo: null,
    calendarViewValue: 'M',
    availableQuotaValue: ['normal', 'force', 'all'],
    showAppointmentRemarkValue: true,
    clinicValue: null,
    encounterTypeValue: null,
    selectEncounterType: {},
    subEncounterTypeValue: [],
    dateFrom: moment().startOf('month'),
    dateTo: moment().endOf('month'),
    encounterTypeListData: [],
    subEncounterTypeListData: [],
    calendarData: [],
    subEncounterTypeListKeyAndValue: {},
    anonyomousBookingActiveInfo: {
        docTypeCd: 'ID',
        docNO: '',
        surname: '',
        givenName: '',
        countryCd: FieldConstant.COUNTRY_CODE_DEFAULT_VALUE,
        mobile: '',
        isHKIDValid: true
    },
    waitingList: null
};

export const bookingAnonymous = (state = initState, action = {}) => {
    switch (action.type) {
        case BookingAnonymousActionType.RESET_ALL: {
            return initState;
        }
        case BookingAnonymousActionType.DESTROY_PAGE: {
            return initState;
        }
        case BookingAnonymousActionType.UPDATE_FIELD: {
            let lastAction = { ...state };
            for (let p in action.updateData) {
                lastAction[p] = action.updateData[p];
            }
            return lastAction;
        }
        case BookingAnonymousActionType.FILLING_DATA: {
            let lastAction = { ...state };
            for (let p in action.fillingData) {
                lastAction[p] = action.fillingData[p];
            }
            return lastAction;
        }
        case BookingAnonymousActionType.PUT_GET_CODE_LIST: {
            return { ...state, codeList: action.codeList };
        }
        default: {
            return state;
        }

    }
};

const initState2 = {
    appointmentList: [],
    timeSlotList: {},
    clinicList: [],
    openTimeSlotModal: false,
    openConfirmDialog: false,
    bookingConfirmed: false,
    bookingData: _.cloneDeep(initBookingData),
    bookTimeSlotData: null,
    changeEncounterIndex: 0,
    showTimeSlotListDialog: false,
    defaultEncounterCd: null,
    handlingBooking: false,
    remarkCodeList: []
};

export const bookingAnonymousInformation = (state = initState2, action = {}) => {
    switch (action.type) {
        case BookingAnonymousActionType.RESET_INFO_ALL: {
            let bookingData = _.cloneDeep(initBookingData);
            let newEncounterType = _.cloneDeep(initEncounterType);
            bookingData.encounterTypes[0] = newEncounterType;
            return {
                ...initState2,
                bookingData: bookingData
            };
        }
        case BookingAnonymousActionType.PUT_BOOKING_DATA: {
            let { bookData, encounterTypeList } = action;
            let defaultEncounterCd = state.defaultEncounterCd;
            let bookingData = appointmentUtilities.initBookData(bookData, encounterTypeList, defaultEncounterCd);
            return {
                ...state,
                bookingData
            };
        }
        case BookingAnonymousActionType.PUT_ENCOUNTER_TYPE_LIST: {
            let bookingData = { ...state.bookingData };
            let where = { serviceCd: action.serviceCd, clinicCd: action.clinicCd };
            let defaultEncounterCd = CommonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.DEFAULT_ENCOUNTER_CD, action.clinicConfig, where);
            for (let i = 0; i < bookingData.encounterTypes.length; i++) {
                bookingData.encounterTypes[i].encounterTypeList = action.encounterTypeList;
                if(bookingData.encounterTypes[i].encounterTypeList.length==0){
                    bookingData.encounterTypes[i].encounterTypeCd='';
                }else{
                    bookingData.encounterTypes[i].encounterTypeCd = defaultEncounterCd.configValue;
                }
                let encounterSelected = bookingData.encounterTypes[i].encounterTypeList.find(item => item.encounterTypeCd === defaultEncounterCd.configValue);
                bookingData.encounterTypes[i].subEncounterTypeList = encounterSelected ? encounterSelected.subEncounterTypeList || [] : [];
                bookingData.encounterTypes[i].subEncounterTypeCd = '';

            }
            return {
                ...state,
                bookingData
            };
        }
        case BookingAnonymousActionType.PUT_CLINIC_LIST: {
            return {
                ...state,
                clinicList: action.clinicList
            };
        }
        case BookingAnonymousActionType.PUT_LIST_APPOINTMENT_DATA: {
            return {
                ...state,
                appointmentList: action.appointmentList
            };
        }
        case BookingAnonymousActionType.PUT_LIST_TIMESLOT_DATA: {
            if (action.timeSlotList && action.timeSlotList.slotForBookingDtos) {
                let { slotForBookingDtos } = action.timeSlotList;
                for (let i = 0; i < slotForBookingDtos.length; i++) {
                    slotForBookingDtos[i].datetime = `${slotForBookingDtos[i].slotDate} ${slotForBookingDtos[i].startTime}`;
                }
            }
            return {
                ...state,
                timeSlotList: action.timeSlotList,
                openTimeSlotModal: true
            };
        }
        case BookingAnonymousActionType.PUT_TIMESLOT_DATA: {
            return {
                ...state,
                bookTimeSlotData: action.data,
                openConfirmDialog: true
            };
        }
        case BookingAnonymousActionType.PUT_BOOK_SUCCESS: {
            return {
                ...state,
                openConfirmDialog: false,
                bookingConfirmed: true
            };
        }
        case BookingAnonymousActionType.UPDATE_STATE: {
            let lastAction = { ...state };
            if (action.index!=null) {
                let encounterSelected = action.updateData.bookingData.encounterTypes[action.index].encounterTypeList.find(item => item.encounterTypeCd === action.updateData.bookingData.encounterTypes[action.index].encounterTypeCd);
                action.updateData.bookingData.encounterTypes[action.index].subEncounterTypeList = encounterSelected ? encounterSelected.subEncounterTypeList || [] : [];
                for (let index = 0; index < action.updateData.bookingData.encounterTypes.length; index++) {
                    if (action.updateData.bookingData.encounterTypes[index].subEncounterTypeList.length == 1) {
                        action.updateData.bookingData.encounterTypes[index].subEncounterTypeCd = action.updateData.bookingData.encounterTypes[index].subEncounterTypeList[0].subEncounterTypeCd;
                    }
                }
            }

            for (let p in action.updateData) {
                lastAction[p] = action.updateData[p];
            }
            return lastAction;
        }
        case BookingAnonymousActionType.PUT_LIST_REMARK_CODE: {
            let { remarkCodeList } = action;
            return {
                ...state,
                remarkCodeList: remarkCodeList
            };
        }
        default: {
            return state;
        }
    }
};