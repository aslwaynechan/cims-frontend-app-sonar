import * as bookingActionType from '../../../actions/appointment/booking/bookingActionType';
import { initBookingData } from '../../../../constants/appointment/bookingInformation/bookingInformationConstants';
import _ from 'lodash';
import moment from 'moment';
import * as appointmentUtilities from '../../../../utilities/appointmentUtilities';
import Enum from '../../../../enums/enum';
import * as CommonUtilities from '../../../../utilities/commonUtilities';

const initState = {
    showMakeAppointmentView: false,
    calendarViewValue: 'M',
    availableQuotaValue: ['normal', 'force', 'all'],
    showAppointmentRemarkValue: true,
    clinicValue: null,
    encounterTypeValue: null,
    selectEncounterType: {},
    subEncounterTypeValue: [],
    dateFrom: moment().startOf('month'),
    dateTo: moment().endOf('month'),
    encounterTypeListData: [],
    subEncounterTypeListData: [],
    calendarData: [],
    subEncounterTypeListKeyAndValue: {},
    isWalkIn: false,
    waitingList: null,
    contactList: []
};

export const booking = (state = initState, action = {}) => {
    switch (action.type) {
        case bookingActionType.RESET_ALL: {
            return initState;
        }
        case bookingActionType.DESTROY_PAGE: {
            return initState;
        }
        case bookingActionType.UPDATE_FIELD: {

            let lastAction = { ...state };
            for (let p in action.updateData) {
                lastAction[p] = action.updateData[p];
            }
            return lastAction;
        }
        case bookingActionType.FILLING_DATA: {
            let lastAction = { ...state };
            for (let p in action.fillingData) {
                lastAction[p] = action.fillingData[p];
            }
            return lastAction;
        }
        default: {
            return state;
        }

    }
};

const initState2 = {
    appointmentList: [],
    timeSlotList: {},
    clinicList: [],
    openTimeSlotModal: false,
    openConfirmDialog: false,
    bookingConfirmed: false,
    bookingData: _.cloneDeep(initBookingData),
    bookTimeSlotData: null,
    changeEncounterIndex: 0,
    showTimeSlotListDialog: false,
    defaultEncounterCd: null,
    handlingBooking: false,
    walkInAttendanceInfo: {
        patientStatus: '',
        paymentMeanCD: 'Cash',
        amount: 100,
        discNumber: '',
        mswWaiver: 0
    },
    searchLogicList: [],
    remarkCodeList: [],
    openApptCancelQueDialog: false,
    currentSelectedApptInfo: null,
    isUpdating: false,
    bookingDataBK: null,
    // openCancelEditApptQueDialog: false,
    confirmData: [],
    updateOrBookNew: '',
    defaultCaseTypeCd: 'N'
};

export const bookingInformation = (state = initState2, action = {}) => {
    switch (action.type) {
        case bookingActionType.RESET_INFO_ALL: {
            let bookingData = _.cloneDeep(initBookingData);
            return {
                ...initState2,
                bookingData: bookingData
            };
        }
        case bookingActionType.PUT_BOOKING_DATA: {
            let { bookData, encounterTypeList } = action;
            let defaultEncounterCd = state.defaultEncounterCd;
            let bookingData = appointmentUtilities.initBookData(bookData, encounterTypeList, defaultEncounterCd);
            return {
                ...state,
                bookingData
            };
        }
        case bookingActionType.PUT_ENCOUNTER_TYPE_LIST: {
            let bookingData = { ...state.bookingData };
            let where = { serviceCd: action.serviceCd, clinicCd: action.clinicCd };
            let defaultEncounterCd = CommonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.DEFAULT_ENCOUNTER_CD, action.clinicConfig, where);

            for (let i = 0; i < bookingData.encounterTypes.length; i++) {
                bookingData.encounterTypes[i].encounterTypeList = action.encounterTypeList;
                if(bookingData.encounterTypes[i].encounterTypeList.length==0){
                    bookingData.encounterTypes[i].encounterTypeCd='';
                }else{
                    bookingData.encounterTypes[i].encounterTypeCd = defaultEncounterCd.configValue;
                }
                let encounterSelected = bookingData.encounterTypes[i].encounterTypeList.find(item => item.encounterTypeCd === defaultEncounterCd.configValue);
                bookingData.encounterTypes[i].subEncounterTypeList = encounterSelected ? encounterSelected.subEncounterTypeList || [] : [];
                bookingData.encounterTypes[i].subEncounterTypeCd = '';

            }
            return {
                ...state,
                bookingData
            };
        }
        case bookingActionType.PUT_CLINIC_LIST: {
            return {
                ...state,
                clinicList: action.clinicList
            };
        }
        case bookingActionType.PUT_LIST_APPOINTMENT_DATA: {
            let tempAppointmentList = { ...action.appointmentList };
            return {
                ...state,
                appointmentList: tempAppointmentList
            };
        }
        case bookingActionType.PUT_LIST_TIMESLOT_DATA: {
            if (action.timeSlotList && action.timeSlotList.slotForBookingDtos) {
                let { slotForBookingDtos } = action.timeSlotList;
                for (let i = 0; i < slotForBookingDtos.length; i++) {
                    slotForBookingDtos[i].datetime = `${slotForBookingDtos[i].slotDate} ${slotForBookingDtos[i].startTime}`;
                }
            }
            return {
                ...state,
                timeSlotList: action.timeSlotList,
                openTimeSlotModal: true
            };
        }
        case bookingActionType.PUT_TIMESLOT_DATA: {
            return {
                ...state,
                bookTimeSlotData: action.data,
                openConfirmDialog: true
            };
        }
        case bookingActionType.PUT_BOOK_FAIL: {
            return {
                ...state,
                openConfirmDialog: false
            };
        }
        case bookingActionType.PUT_BOOK_SUCCESS: {
            let { respData, bookData } = action;
            const appointmentInfo = bookData.appointmentDtos[0];

            let bookingConfirmData = {
                appointmentId: respData[0].appointmentId,
                caseNo: appointmentInfo.caseNo ? appointmentInfo.caseNo : '',
                encounterTypeCd: appointmentInfo.encounterTypeCd,
                subEncounterTypeCd: appointmentInfo.subEncounterTypeCd,
                appointmentDate: respData[0].appointmentDate,
                appointmentTime: respData[0].appointmentTime,
                remarkId: appointmentInfo.remarkId,
                memo: appointmentInfo.memo,
                caseTypeCd: appointmentInfo.caseTypeCd
            };

            return {
                ...state,
                confirmData: bookingConfirmData,
                openConfirmDialog: false,
                bookingConfirmed: true
            };
        }
        case bookingActionType.UPDATE_STATE: {
            let lastAction = { ...state };
            if (action.index != null) {
                let encounterSelected = action.updateData.bookingData.encounterTypes[action.index].encounterTypeList.find(item => item.encounterTypeCd === action.updateData.bookingData.encounterTypes[action.index].encounterTypeCd);
                action.updateData.bookingData.encounterTypes[action.index].subEncounterTypeList = encounterSelected ? encounterSelected.subEncounterTypeList || [] : [];
                for (let index = 0; index < action.updateData.bookingData.encounterTypes.length; index++) {
                    if (action.updateData.bookingData.encounterTypes[index].subEncounterTypeList.length == 1) {
                        action.updateData.bookingData.encounterTypes[index].subEncounterTypeCd = action.updateData.bookingData.encounterTypes[index].subEncounterTypeList[0].subEncounterTypeCd;
                    }
                }
            }
            for (let p in action.updateData) {
                lastAction[p] = action.updateData[p];
            }
            return lastAction;
        }

        case bookingActionType.BOOK_AND_ATTEND_SUCCEESS: {
            let { respData, walkInData } = action;
            let walkInConfirmData = {
                appointmentId: respData.appointmentId,
                caseNo: walkInData.caseNo ? walkInData.caseNo : '',
                encounterTypeCd: walkInData.encounterTypeCd,
                subEncounterTypeCd: walkInData.subEncounterTypeCd,
                appointmentDate: respData.appointmentDate,
                appointmentTime: respData.appointmentTime,
                remarkId: walkInData.remarkId,
                memo: walkInData.memo,
                patientStatusCd: walkInData.patientStatusCd,
                caseTypeCd: walkInData.caseTypeCd,
                amount: walkInData.amount,
                paymentMeanCD: walkInData.paymentMeanCD
            };
            return {
                ...state,
                bookingConfirmed: true,
                confirmData: walkInConfirmData
            };
        }

        case bookingActionType.PUT_USER_SEARCH_LOGIC: {
            let { searchLogicList } = action;
            return {
                ...state,
                searchLogicList: searchLogicList
            };
        }

        case bookingActionType.PUT_LIST_REMARK_CODE: {
            let { remarkCodeList } = action;
            return {
                ...state,
                remarkCodeList: remarkCodeList
            };
        }

        case bookingActionType.EDIT_APPOINTMENT: {                                  //in sprint 6,we only edit one appointment
            let { apptInfo } = action;
            let tempBookingData = _.cloneDeep(state.bookingData);
            let tempEncounterTypes = tempBookingData.encounterTypes;
            let bookingDataBK = _.cloneDeep(state.bookingData);

            for (let i = 0; i < tempEncounterTypes.length; i++) {
                let currentEncounterSelected = tempEncounterTypes[i].encounterTypeList.find(item => item.encounterTypeCd === apptInfo.encounterTypeCd);
                let currentSubEncounterList = currentEncounterSelected ? currentEncounterSelected.subEncounterTypeList || [] : [];
                //bookingData.encounterTypes[index].subEncounterTypeList = encounterSelected ? encounterSelected.subEncounterTypeList || [] : [];
                let newEncounter = _.cloneDeep(tempEncounterTypes[i]);
                newEncounter.encounterTypeCd = apptInfo.encounterTypeCd;
                newEncounter.subEncounterTypeCd = apptInfo.subEncounterTypeCd;
                newEncounter.appointmentDate = apptInfo.appointmentDate;
                //newEncounter.appointmentTime = moment(apptInfo.appointmentTime);
                newEncounter.caseTypeCd = apptInfo.caseTypeCd;
                newEncounter.appointmentTypeCd = apptInfo.appointmentTypeCd;
                newEncounter.subEncounterTypeList = currentSubEncounterList;
                newEncounter.remarkId = apptInfo.remarkId;
                newEncounter.memo = apptInfo.memo;

                //reset the elapsed period
                newEncounter.elapsedPeriod = '';
                newEncounter.elapsedPeriodUnit = '';
                if (apptInfo.appointmentTime) {
                    const aTime = apptInfo.appointmentTime.split(':');
                    if (aTime.length === 2) {
                        newEncounter.appointmentTime = moment(apptInfo.appointmentDate).set({ 'hours': aTime[0], 'minutes': aTime[1] });
                    }
                } else {
                    //let now = moment();
                    newEncounter.appointmentTime = moment(apptInfo.appointmentDate).set({ 'hours': 0, 'minutes': 0, 'second': 0 });
                }
                tempEncounterTypes[i] = newEncounter;
            }
            tempBookingData.encounterTypes = tempEncounterTypes;
            // tempBookingData.remark = apptInfo.remark;
            return {
                ...state,
                isUpdating: true,
                currentSelectedApptInfo: apptInfo,
                bookingData: tempBookingData,
                bookingDataBK
                //bookingConfirmed:true
            };
        }

        case bookingActionType.CANCEL_EDIT_APPOINTMENT: {
            let tempBookingDataBK = _.cloneDeep(state.bookingDataBK);
            return {
                ...state,
                isUpdating: false,
                currentSelectedApptInfo: null,
                bookingData: tempBookingDataBK
            };
        }

        case bookingActionType.UPDATE_APPOINTMENT_SUCCESS: {
            let newBookingData = _.cloneDeep(initBookingData);
            newBookingData = appointmentUtilities.getDefaultElapsedPeriodBookData(newBookingData);
            newBookingData.clinicCd = state.bookingData.clinicCd;
            newBookingData.encounterTypes[0].encounterTypeList = _.cloneDeep(state.bookingData.encounterTypes[0].encounterTypeList);
            // set default value after upadte booing appointment data
            // encounter , sub-encounter
            if (newBookingData.encounterTypes[0].encounterTypeList.length == 0) {
                newBookingData.encounterTypes[0].encounterTypeCd = '';
            } else {
                newBookingData.encounterTypes[0].encounterTypeCd = state.defaultEncounterCd;
            }
            let encounterSelected = newBookingData.encounterTypes[0].encounterTypeList.find(item => item.encounterTypeCd === state.defaultEncounterCd);
            newBookingData.encounterTypes[0].subEncounterTypeList = encounterSelected ? encounterSelected.subEncounterTypeList || [] : [];
            if (newBookingData.encounterTypes[0].subEncounterTypeList.length == 0) {
                newBookingData.encounterTypes[0].subEncounterTypeCd = '';
            } else {
                newBookingData.encounterTypes[0].subEncounterTypeCd = newBookingData.encounterTypes[0].subEncounterTypeList[0].subEncounterTypeCd;
            }
            // appointment type
            newBookingData.encounterTypes[0].appointmentTypeCd = newBookingData.encounterTypes[0].appointmentTypeCd || Enum.APPOINTMENT_TYPE_SUFFIX[0].code;
            return {
                ...state,
                isUpdating: false,
                currentSelectedApptInfo: null,
                bookingData: newBookingData,
                updateOrBookNew: ''
            };
        }

        case bookingActionType.PUT_CONTACT_HISTORY: {
            return {
                ...state,
                contactList: action.contactList
            };
        }

        case bookingActionType.CLEAR_CONTACT_HISTORY: {
            return {
                ...state,
                contactList: []
            };
        }
        case bookingActionType.PUT_DEFAULT_CASETYPECD: {
            return {
                ...state,
                defaultCaseTypeCd: action.defaultCaseTypeCd
            };
        }
        default: {
            return state;
        }

    }
};