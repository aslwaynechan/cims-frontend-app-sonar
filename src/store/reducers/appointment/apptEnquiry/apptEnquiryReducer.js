import *as type from '../../../actions/appointment/apptEnquiry/apptEnquiryActionType';
import moment from 'moment';
import _ from 'lodash';
import ApptEnum from '../../../../views/appointment/appointmentEnquiry/enum/apptEnquiryEnum';

const INITAL_STATE = {
    criteria: {
        serviceCd: null,
        type: ApptEnum.APPT_TYPE.APPT_LIST,
        clinicCd: null,
        encounterCd: 'ALL',
        subEncounterCd: 'ALL',
        dateFrom: moment(),
        dateTo: moment(),
        status: 'ALL',
        remarkCd: null

    },
    enquiryResult: []
};

export default (state = _.cloneDeep(INITAL_STATE), action) => {
    switch (action.type) {
        case type.RESET_ALL: {
            return _.cloneDeep(INITAL_STATE);
        }

        case type.UPDATE_FIELD: {
            let lastAction = { ...state };
            for (let p in action.updateData) {
                lastAction[p] = action.updateData[p];
            }
            return lastAction;
        }

        case type.LOAD_ENQUIRY_RESULT: {
            let { result } = action;
            return {
                ...state,
                enquiryResult: result
            };
        }

        default: {
            return state;
        }
    }
};

