import * as editTimeSlotActionType from '../../../actions/appointment/editTimeSlot/editTimeSlotActionType';
import moment from 'moment';

const initState = {
    dateFrom: moment(),
    dateTo: moment(),
    encounterTypeCd: '',
    subEncounterTypeCd: '',
    page: 1,
    pageSize: 10,
    encounterCodeList: [],
    subEncounterCodeList: [],
    selectedItems: [],
    timeslotList: {},
    dialogOpen: false,
    dialogName: '',
    multipleUpdateFinish: false,
    multipleUpdateData: null,
    multipleUpdateForm: null
};

export default (state = initState, action = {}) => {
    switch (action.type) {
        case editTimeSlotActionType.RESET_ALL: {
            return {
                ...initState,
                dateFrom: moment(),
                dateTo: moment()
            };
        }

        case editTimeSlotActionType.PUT_CODE_LIST: {
            let encounter = '', subEncounter = '', subCodeList = [];
            if (action.codeList && action.codeList.length > 0) {
                encounter = action.codeList[0].encounterTypeCd;
                if (action.codeList[0].subEncounterTypeList && action.codeList[0].subEncounterTypeList.length > 0) {
                    subCodeList = action.codeList[0].subEncounterTypeList;
                    subEncounter = action.codeList[0].subEncounterTypeList[0].subEncounterTypeCd;
                }
            }
            return {
                ...state,
                encounterCodeList: action.codeList || [],
                subEncounterCodeList: subCodeList,
                encounterTypeCd: encounter,
                subEncounterTypeCd: subEncounter
            };
        }

        case editTimeSlotActionType.UPDATE_STATE: {
            let lastAction = {...state};
            for(let p in action.updateData){
                lastAction[p] = action.updateData[p];
            }
            return lastAction;
        }

        case editTimeSlotActionType.PUT_LIST_TIME_SLOT: {
            return{
                ...state,
                timeslotList: action.data
            };
        }

        case editTimeSlotActionType.PUT_MULTIPLE_UPDATE: {
            return {
                ...state,
                multipleUpdateFinish: true,
                multipleUpdateData: action.data,
                multipleUpdateForm: action.updateParams
            };
        }

        default: {
            return state;
        }
    }
};