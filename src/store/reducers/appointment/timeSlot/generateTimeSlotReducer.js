import * as GenerateTimeSlotActionType from '../../../actions/appointment/generateTimeSlot/generateTimeSlotActionType';
import moment from 'moment';


const timeslotState = {
    encounterTypeCd: '',
    subEncounterTypeCd: '',
    fromDate: moment(),
    toDate: moment(),
    slotTemplateDto: [],
    selectTemplate: [],
    openSearchProgress: false,
    encounterCodeList: [],
    subEncounterCodeList: [],
    timeslotOpenDialog: false,
    timeslotErrorMessage: '',
    timeslotErrorData: {}
};

export default (state = timeslotState, action = {}) => {
    switch (action.type) {
        case GenerateTimeSlotActionType.RESET_ALL: {
            let encounter = '', subEncounter = '', subCodeList = [];
            if (state.encounterCodeList && state.encounterCodeList.length > 0) {
                encounter = state.encounterCodeList[0].encounterTypeCd;
                if (state.encounterCodeList[0].subEncounterTypeList && state.encounterCodeList[0].subEncounterTypeList.length > 0) {
                    subCodeList = state.encounterCodeList[0].subEncounterTypeList;
                    subEncounter = subCodeList[0].subEncounterTypeCd;
                }
            }
            return {
                ...state,
                encounterTypeCd: encounter,
                subEncounterTypeCd: subEncounter,
                subEncounterCodeList: subCodeList,
                fromDate: moment(),
                toDate: moment(),
                slotTemplateDto: [],
                selectTemplate: []
            };
        }

        case GenerateTimeSlotActionType.OPEN_SEARCH: {
            return {
                ...state,
                openSearchProgress: true
            };
        }

        case GenerateTimeSlotActionType.CLOSE_SEARCH: {
            return {
                ...state,
                openSearchProgress: false
            };
        }

        case GenerateTimeSlotActionType.UPDATE_FIELD: {
            let { name, value } = action;
            if (name === 'encounterTypeCd') {
                let subEncounter = state.subEncounterTypeCd;
                let subCodeList = state.subEncounterCodeList;
                let codeList = state.encounterCodeList.find(item => item.encounterTypeCd === value);
                if (codeList && codeList.subEncounterTypeList && codeList.subEncounterTypeList.length > 0) {
                    subCodeList = codeList.subEncounterTypeList;
                    subEncounter = subCodeList[0].subEncounterTypeCd;
                } else {
                    subCodeList = [];
                    subEncounter = '';
                }
                return {
                    ...state,
                    subEncounterCodeList: subCodeList,
                    subEncounterTypeCd: subEncounter,
                    [name]: value
                };
            }
            return {
                ...state,
                [name]: value
            };
        }

        case GenerateTimeSlotActionType.PUT_CODE_LIST: {
            let encounter = '', subEncounter = '', subCodeList = [];
            if (action.codeList && action.codeList.length > 0) {
                encounter = action.codeList[0].encounterTypeCd;
                if (action.codeList[0].subEncounterTypeList && action.codeList[0].subEncounterTypeList.length > 0) {
                    subCodeList = action.codeList[0].subEncounterTypeList;
                    subEncounter = action.codeList[0].subEncounterTypeList[0].subEncounterTypeCd;
                }
            }
            return {
                ...state,
                encounterCodeList: action.codeList || [],
                subEncounterCodeList: subCodeList,
                encounterTypeCd: encounter,
                subEncounterTypeCd: subEncounter
            };
        }

        case GenerateTimeSlotActionType.PUT_TEMPLATE_DATA: {
            return {
                ...state,
                selectTemplate: [],
                slotTemplateDto: action.data
            };
        }

        case GenerateTimeSlotActionType.PUT_GENERATE_SUCCESS: {
            return {
                ...state,
                timeslotOpenDialog: true,
                timeslotErrorMessage: action.errorMessage,
                timeslotErrorData: action.details
            };
        }

        case GenerateTimeSlotActionType.PUT_GENERATE_FAIL: {
            return {
                ...state,
                timeslotOpenDialog: true,
                timeslotErrorMessage: action.errMsg,
                timeslotErrorData: action.data
            };
        }

        case GenerateTimeSlotActionType.HANDLE_SELECTED_TEMPLATE: {
            return {
                ...state,
                selectTemplate: action.data
            };
        }

        case GenerateTimeSlotActionType.CLOSE_DIALOG: {
            return {
                ...state,
                timeslotOpenDialog: false
            };
        }

        default: {
            return state;
        }

    }
};