import * as calendarViewActionType from '../../actions/appointment/calendarView/calendarViewActionType';
import moment from 'moment';

const initState = {
    showMakeAppointmentView: false,
    calendarViewValue: 'M',
    availableQuotaValue: ['normal', 'force', 'all'],
    showAppointmentRemarkValue: true,
    clinicValue: null,
    encounterTypeValue: null,
    selectEncounterType: {},
    subEncounterTypeValue: [],
    dateFrom: moment().startOf('month'),
    dateTo: moment().endOf('month'),
    clinicListData: [],
    encounterTypeListData: [],
    subEncounterTypeListData: [],
    calendarData: [],
    bookData: null,
    subEncounterTypeListKeyAndValue: {},
    isWalkIn: false,
    waitingList: null
};

export default (state = initState, action = {}) => {
    switch (action.type) {
        case calendarViewActionType.RESET_ALL: {
            return initState;
        }
        case calendarViewActionType.UPDATE_FIELD: {
            let lastAction = { ...state };
            for (let p in action.updateData) {
                lastAction[p] = action.updateData[p];
            }
            return lastAction;
        }
        case calendarViewActionType.FILLING_DATA: {
            let lastAction = { ...state };
            for (let p in action.fillingData) {
                lastAction[p] = action.fillingData[p];
            }
            return lastAction;
        }
        default: {
            return state;
        }
    }
};

