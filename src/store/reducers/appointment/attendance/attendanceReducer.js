import * as attendanceTypes from './../../../actions/appointment/attendance/attendanceActionType';

const inital_state = {
    patientStatusList: [],
    patientStatus: '',
    discNum: '',
    isAttend: false,
    appointmentForAttendance: null,
    appointmentConfirmInfo: null,
    patientQueueList: [],
    enableWalkIn: false,
    openPromptDialog: false,
    appointmentList: [],
    currentAppointment: null,
    patientStatusFlag:'N'
};

export default (state = inital_state, action) => {
    switch (action.type) {
        case attendanceTypes.PATIENT_STATUS_LIST: {
            return {
                ...state,
                patientStatusList: action.data ? action.data.patient_status : []
            };
        }
        case attendanceTypes.UPDATE_FIELD: {
            let lastState = { ...state };
            for (let p in action.updateData) {
                lastState[p] = action.updateData[p];
            }
            return lastState;
        }
        case attendanceTypes.MARK_ATTENDANCE_SUCCESS: {
            return {
                ...state,
                isAttend: true,
                appointmentConfirmInfo: action.data,
                currentAppointment: null
            };
        }
        case attendanceTypes.APPOINTMENT_FOR_ATTENDANCE_SUCCESS: {
            let currentAppointment = { ...action.currentAppointment };
            let tempDialogFlag = false;
            if (!currentAppointment) {
                tempDialogFlag = true;
            }

            //update patient status
            let patientStatusCd = state.patientStatus;
            if (currentAppointment.patientStatusFlag === 'Y') {
                if (currentAppointment.patientStatusCd) {
                    patientStatusCd = currentAppointment.patientStatusCd;
                }
            }

            return {
                ...state,
                currentAppointment: currentAppointment,
                patientStatus: patientStatusCd,
                openPromptDialog: tempDialogFlag
            };
        }
        case attendanceTypes.DESTROY_MARK_ATTENDANCE: {
            return {
                inital_state,
                patientStatusList: state.patientStatusList
            };
        }
        case attendanceTypes.PUT_APPOINTMENT_LIST: {
            let tempAppointmentList = { ...action.appointmentList };
            return {
                ...state,
                appointmentList: tempAppointmentList
            };
        }
        case attendanceTypes.RESET_ALL: {
            return inital_state;
        }
        default:
            return state;
    }
};