import * as types from '../../actions/appointment/waitingList/waitingListActionType';
import _ from 'lodash';
import moment from 'moment';
import * as patientUtilities from '../../../utilities/patientUtilities';
import Enum from '../../../enums/enum';

const INITAL_STATE = {
   encounterTypeList: [],
   dateFrom: moment().subtract(1, 'months'),
   dateTo: moment(),
   encounterType: '*ALL',
   status: Enum.WAITING_LIST_STATUS_LIST[1].value,
   docTypeList: [],
   docType: '*ALL',
   HKID: '',
   engSurname: '',
   engGivenname: '',
   phone: '',
   openSearchDialog: false,
   waitingList: {},
   page: 1,
   pageSize: 10,
   sortType: 'asc',
   sortBy: '',
   editWaitingStatus: 'I',
   clinicList: [],
   patientList: [],
   waiting: {},
   waitingHkid: '',
   waitingDocTypeCd: '',
   waitingOtherDocNo: '',
   waitingEngSurname: '',
   waitingEngGivenname: '',
   waitingPatientPhones: [{ countryCd: 'HKG', phoneNo: '' }, { countryCd: 'HKG', phoneNo: '' }],
   waitingClinicCd: '',
   waitingEncounterTypeList: [],
   waitingEncounterTypeCd: '',
   waitingTravelDate: null,
   waitingCountryCdList: [],
   waitingRemarks: ''
};

export default (state = _.cloneDeep(INITAL_STATE), action) => {
   // console.log('state', state);
   // console.log('action', action);
   switch (action.type) {
      case types.UPDATE_FIELD: {
         return {
            ...state,
            ...action.fields
         };
      }
      case types.UPDATE_WAITING_FIELD: {
         // let waiting = _.cloneDeep(state.waiting);
         let waiting = {
            patientKey: action.patient.patientKey
         };
         let patientPhones = [{ countryCd: null, phoneNo: '' }, { countryCd: null, phoneNo: '' }];
         action.patient.phoneList.forEach((item) => {
            if (item.phonePriority === 1) {
               patientPhones[0].countryCd = item.countryCd || '';
               patientPhones[0].phoneNo = item.phoneNo || '';
            } else if (item.phoneNo) {
               patientPhones[1].countryCd = item.countryCd || '';
               patientPhones[1].phoneNo = item.phoneNo || '';
            }
         });
         return {
            ...state,
            waiting,
            waitingHkid: action.patient.hkid,
            waitingDocTypeCd: action.patient.docTypeCd,
            waitingOtherDocNo: action.patient.otherDocNo,
            waitingEngSurname: action.patient.engSurname,
            waitingEngGivenname: action.patient.engGivename,
            waitingPatientPhones: patientPhones
         };
      }
      case types.CANCEL_SEARCH: {
         return {
            ...state,
            docType: '*ALL',
            HKID: '',
            engSurname: '',
            engGivenname: '',
            phone: '',
            openSearchDialog: false
         };
      }
      case types.ADD_WAITING: {
         return {
            ...state,
            editWaitingStatus: 'A',
            waiting: {},
            waitingHkid: '',
            waitingDocTypeCd: '',
            waitingOtherDocNo: '',
            waitingEngSurname: '',
            waitingEngGivenname: '',
            waitingPatientPhones: [{ countryCd: 'HKG', phoneNo: '' }, { countryCd: 'HKG', phoneNo: '' }],
            waitingClinicCd: '',
            waitingEncounterTypeList: [],
            waitingEncounterTypeCd: '',
            waitingTravelDate: null,
            waitingCountryCdList: [],
            waitingRemarks: ''
         };
      }
      case types.EDIT_WAITING: {
         let waiting = _.cloneDeep(action.waiting);
         let waitingPatientPhones = [{ countryCd: 'HKG', phoneNo: '' }, { countryCd: 'HKG', phoneNo: '' }];
         waiting.anonymousPatientDto.patientPhones.forEach((item, index) => {
            if (index < 2) {
               waitingPatientPhones[index].countryCd = item.countryCd;
               waitingPatientPhones[index].phoneNo = item.phoneNo;
            }
         });
         return {
            ...state,
            editWaitingStatus: 'E',
            waiting,
            waitingHkid: _.trim(waiting.anonymousPatientDto.hkid || ''),
            waitingDocTypeCd: waiting.anonymousPatientDto.docTypeCd || '',
            waitingOtherDocNo: waiting.anonymousPatientDto.otherDocNo || '',
            waitingEngSurname: waiting.anonymousPatientDto.engSurname || '',
            waitingEngGivenname: waiting.anonymousPatientDto.engGivename || '',
            waitingPatientPhones: waitingPatientPhones || [{ countryCd: null, phoneNo: '' }, { countryCd: null, phoneNo: '' }],
            waitingClinicCd: waiting.clinicCd || '',
            waitingEncounterTypeList: action.waitingEncounterTypeList || [],
            waitingEncounterTypeCd: waiting.encounterTypeCd || '',
            // waitingTravelDate: waiting.travelDtm ? moment(waiting.travelDtm, 'YYYY-MM-DD') : null,
            waitingTravelDate: waiting.travelDtm ? moment(waiting.travelDtm, Enum.DATE_FORMAT_EYMD_VALUE) : null,
            waitingCountryCdList: waiting.countryCdList ? waiting.countryCdList.split('|') : [],
            waitingRemarks: waiting.remarks || ''
         };
      }
      case types.CANCEL_EDIT_WAITING: {
         return {
            ...state,
            editWaitingStatus: 'I',
            waiting: {},
            waitingHkid: '',
            waitingDocTypeCd: '',
            waitingOtherDocNo: '',
            waitingEngSurname: '',
            waitingEngGivenname: '',
            waitingPatientPhones: [{ countryCd: null, phoneNo: '' }, { countryCd: null, phoneNo: '' }],
            waitingClinicCd: '',
            waitingEncounterTypeList: [],
            waitingEncounterTypeCd: '',
            waitingTravelDate: null,
            waitingCountryCdList: [],
            waitingRemarks: ''
         };
      }
      case types.PUT_SEARCH_PATIENT_LIST: {
         const patientResult = patientUtilities.getPatientSearchResult(action.data && action.data.patientSearchDtos);
         return {
            ...state,
            patientList: patientResult
         };
      }
      case types.SAVE_SUCCESS: {
         return {
            ...state,
            promptDialog: {
               dialogType: 'saveSuccess',
               open: true,
               title: 'Save',
               contentText: 'Save success!'
            },
            editWaitingStatus: 'I',
            waiting: {},
            waitingHkid: '',
            waitingDocTypeCd: '',
            waitingOtherDocNo: '',
            waitingEngSurname: '',
            waitingEngGivenname: '',
            waitingPatientPhones: [{ countryCd: null, phoneNo: '' }, { countryCd: null, phoneNo: '' }],
            waitingClinicCd: '',
            waitingEncounterTypeList: [],
            waitingEncounterTypeCd: '',
            waitingTravelDate: null,
            waitingCountryCdList: [],
            waitingRemarks: ''
         };
      }
      case types.DELETE_SUCCESS: {
         return {
            ...state,
            promptDialog: {
               dialogType: 'delectSuccess',
               open: true,
               title: 'Delete',
               contentText: 'Delete success!'
            },
            editWaitingStatus: 'I',
            waiting: {},
            waitingHkid: '',
            waitingDocTypeCd: '',
            waitingOtherDocNo: '',
            waitingEngSurname: '',
            waitingEngGivenname: '',
            waitingPatientPhones: [{ countryCd: null, phoneNo: '' }, { countryCd: null, phoneNo: '' }],
            waitingClinicCd: '',
            waitingEncounterTypeList: [],
            waitingEncounterTypeCd: '',
            waitingTravelDate: null,
            waitingCountryCdList: [],
            waitingRemarks: ''
         };
      }


      case types.RESET_ALL: {
         return _.cloneDeep(INITAL_STATE);
      }
      default:
         return { ...state };
   }
};