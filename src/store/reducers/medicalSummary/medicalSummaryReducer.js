import * as type from '../../actions/medicalSummary/medicalSummaryActionType';

const INIT_STATE = {
  openDialogFlag: false,
  smokingDropMap:new Map(),
  drinkingDropMap:new Map(),
  substanceAbuseDropMap:new Map(),
  pastHistoryValDtos:[],
  smokingHistoryValDtos:[],
  drinkingHistoryValDtos:[],
  substanceAbuseHistoryValDtos:[],
  occupationalHistoryValDtos:[],
  familyHistoryValDtos:[],
  remarkValDtos:[],
  // for temporary test
  tempServiceList:[]
};

export default (state = INIT_STATE, action = {}) => {
  switch (action.type) {
    case type.MEDICAL_DROP_LIST:{
      return{
        ...state,
        smokingDropMap:action.smokingDropMap,
        drinkingDropMap:action.drinkingDropMap,
        substanceAbuseDropMap:action.substanceAbuseDropMap
      };
    }
    case type.MEDICAL_SUMMARY_VAL:{
      return{
        ...state,
        pastHistoryValDtos:action.pastHistoryValDtos,
        smokingHistoryValDtos:action.smokingHistoryValDtos,
        drinkingHistoryValDtos:action.drinkingHistoryValDtos,
        substanceAbuseHistoryValDtos:action.substanceAbuseHistoryValDtos,
        occupationalHistoryValDtos:action.occupationalHistoryValDtos,
        familyHistoryValDtos:action.familyHistoryValDtos,
        remarkValDtos:action.remarkValDtos
      };
    }
    // for temporary test
    case type.TEMP_SERVICE_LIST:{
      return{
        ...state,
        tempServiceList: action.tempServiceList
      };
    }
    default:
      return state;
  }
};