import * as encounterTypeManagementActionTypes from '../../../actions/administration/encounterTypeManagement/encounterTypeManagementActionType';

const inital_state = {
    encounterType:[],
    subEncounterType:[],
    selectEncounterType:{},
    selectSubEncounterType:{},
    selectEncounterTypeIndex:-1,
    selectSubEncounterTypeIndex:-1,
    isSubEncounterTypeDetail:false,
    isEditState:false,
    isNewData:false,
    isLegalCode:false,

    //Detail Field
    encounterTypeDetail:{},
    subEncounterTypeDetail:{}

};

export default (state = inital_state, action = {}) => {
    switch (action.type) {
        case encounterTypeManagementActionTypes.RESET_ALL:
            return inital_state;
        case encounterTypeManagementActionTypes.UPDATE_FIELD: {
            let newState = {...state};
            console.log('oldState',newState);
            for(let key in action.field){
                newState[key] = action.field[key];
                if(key==='selectEncounterType'&&!action.field.isNewData){
                    newState['encounterTypeDetail']={...action.field[key]};
                }
                if(key==='selectSubEncounterType'&&!action.field.isNewData){
                    newState['subEncounterTypeDetail']={...action.field[key]};
                }
            }
            console.log('newState',newState);
            return newState;
        }
        case encounterTypeManagementActionTypes.INIT_PAGE:{
            let newState = {...inital_state};
            for(let key in action.field){
                newState[key] = action.field[key];
            }
            return newState;
        }
        case encounterTypeManagementActionTypes.SAVE_SUCCESS:{
            let newState = {...inital_state};
            let selectEncounterTypeIndex = action.encounterType.findIndex((item)=>(item.encounterTypeCd===action.saveData.encounterTypeCd));
            let selectSubEncounterTypeIndex = -1;
            if(action.saveData.subEncounterTypeCd&&selectEncounterTypeIndex!==-1){
                selectSubEncounterTypeIndex=action.encounterType[selectEncounterTypeIndex].subEncounterTypeList.findIndex((item)=>(item.subEncounterTypeCd===action.saveData.subEncounterTypeCd));
            }
            newState.encounterType = [...action.encounterType];
            newState.subEncounterType =selectEncounterTypeIndex===-1?[]:[...action.encounterType[selectEncounterTypeIndex].subEncounterTypeList];
            newState.selectEncounterType = selectEncounterTypeIndex===-1?{}:{...action.encounterType[selectEncounterTypeIndex]};
            newState.selectSubEncounterType = selectSubEncounterTypeIndex===-1?{}:{...action.encounterType[selectEncounterTypeIndex].subEncounterTypeList[selectSubEncounterTypeIndex]};
            newState.selectEncounterTypeIndex = selectEncounterTypeIndex;
            newState.selectSubEncounterTypeIndex = selectSubEncounterTypeIndex;
            newState.isSubEncounterTypeDetail = selectSubEncounterTypeIndex!==-1;
            newState.encounterTypeDetail={...newState.selectEncounterType};
            newState.subEncounterTypeDetail={...newState.selectSubEncounterType};
            return newState;
        }
        case encounterTypeManagementActionTypes.VERIFY_CODE_IS_LEGAL_SUCCESSS:{
            if(!state.isEditState){
                return state;
            }else{
                let newState = {...state,isLegalCode:true};
                if(action.verifyType==='verifySubEncounterTypeCd'){
                    newState.subEncounterTypeDetail.existCode='0';
                }else{
                    newState.encounterTypeDetail.existCode='0';
                }
                return newState;
            }
        }
        case encounterTypeManagementActionTypes.CD_VERIFICATION_FAILED:{
            if(!state.isEditState){
                return state;
            }else{
                let newState = {...state};
                for(let key in action.field){
                    newState[key] = action.field[key];
                }
                console.log('newState',newState);
                return newState;
            }
        }
        default:
            return state;
    }
};