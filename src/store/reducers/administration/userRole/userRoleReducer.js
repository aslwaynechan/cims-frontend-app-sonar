import * as userRoleActionType from '../../../actions/administration/userRole/userRoleActionType';
import _ from 'lodash';

// import _ from 'lodash';
const INITAL_STATE = {
    //show
    showUserRoleSearch: true,
    showUserRoleForm: false,
    showActiveButton: false,

    disabelUserRoleForm: true,
    disabelEditButton: true,
    disabelSaveButton: true,
    // openDialog:false,

    //select Filed or search data
    currentRoleId: null,
    replicableRole: [],
    searchUserRoleList: [],

    //Filed
    userRoleName: '',
    userRoleDesc: '',
    statusInd: 'Y',
    version: null,
    replicableRoleSelect: null,
    unassignedUsers: [],
    assignedUsers: [],

    //error
    saveErrorMessage: null,
    saveErrorData: [],
    saveSuccess: null,

    //access right
    allMenuList: [],
    accessRights: [],
    landingSuccessPageList: [],
    landingSuccessPage: 'none',
    menuBK: [],

    //user role member
    fullUserSearchUser: '',
    selectedUserSearchUser: ''
};

function filterParent(data, state) {
    if (!data) {
        return [];
    }
    // data.checked = false;
    if (data.childAccessRightDtos.length === 0) {
        return data;
    }
    else {
        let child = [];
        data.open = state;
        data.childAccessRightDtos.forEach(childRight => {
            let oneChild = filterParent(childRight, state);
            child.push(oneChild);
        });
        data.childAccessRightDtos = child;
        //tempMenuList.push(data);

        return data;
    }
}

function findByCd(data, cd, cdName) {

    let result = null;
    let tempResult = null;
    tempResult = data.find(item => item[cdName] === cd);

    if (tempResult !== undefined) {
        result = tempResult;
        return result;
    }

    data.forEach(right => {
        if (right.childAccessRightDtos.length !== 0) {
            tempResult = findByCd(right.childAccessRightDtos, cd, cdName);
            if (tempResult !== undefined) {
                result = tempResult;
            }
        }
    });
    return result;
}

function getAllMenuList(data, state) {
    let allMenuList = [];

    data.forEach(menu => {
        let tempMenu = filterParent(menu, state);
        allMenuList.push(tempMenu);
    });
    return allMenuList;
}

function resetMenuOpenStatus(data) {
    let tempMenu = getAllMenuList(data, false);
    return tempMenu;
}


export default (state = INITAL_STATE, action) => {
    switch (action.type) {
        case userRoleActionType.CANCEL_EDIT: {
            return {
                ...INITAL_STATE,
                allMenuList: state.allMenuList,
                menuBK: state.menuBK,
                landingSuccessPageList: state.landingSuccessPageList
            };
        }
        case userRoleActionType.RESET_ALL: {
            return INITAL_STATE;
        }
        case userRoleActionType.CREATE_USER_ROLE: {
            return {
                ...INITAL_STATE,
                currentRoleId: null,
                showUserRoleSearch: false,
                showUserRoleForm: true,
                disabelSaveButton: false,
                allMenuList: resetMenuOpenStatus(state.allMenuList),
                menuBK: resetMenuOpenStatus(state.allMenuList),
                landingSuccessPageList: state.landingSuccessPageList
            };
        }
        case userRoleActionType.EDIT_USER_ROLE: {
            return {
                ...state,
                showUserRoleSearch: false,
                showUserRoleForm: true,
                disabelUserRoleForm: false,
                disabelSaveButton: false
            };
        }
        case userRoleActionType.PUT_CREATE_USER_ROLE_DATA: {
            return {
                ...state,
                disabelUserRoleForm: false,
                unassignedUsers: action.unassignedUsers || [],
                assignedUsers: action.assignedUsers || [],
                replicableRole: action.replicableRole || []
            };
        }

        case userRoleActionType.SEARCH_USER_ROLE: {
            return {
                ...state,
                searchUserRoleList: action.searchUserRoleList || []
            };
        }
        case userRoleActionType.PREVIEW_USER_ROLE_DETAILS: {
            return {
                ...state,
                showUserRoleForm: true,
                showActiveButton: true,
                disabelEditButton: false,
                disabelUserRoleForm: true,
                disabelSaveButton: true,

                currentRoleId: action.currentRoleId,
                replicableRole: action.replicableRole,

                userRoleName: action.userRoleDetails.userRoleName,
                userRoleDesc: action.userRoleDetails.userRoleDesc,
                statusInd: action.userRoleDetails.statusInd,
                version: action.userRoleDetails.version,
                // assignedUsers: action.userRoleDetails.users || [],
                // unassignedUsers: action.userRoleDetails.excludedUsers || [],
                assignedUsers: action.userRoleDetails.userDtos || [],
                unassignedUsers: action.userRoleDetails.allUserDtos || [],
                accessRights: action.userRoleDetails.accessRights || [],

                allMenuList: resetMenuOpenStatus(state.allMenuList),
                landingSuccessPage: action.userRoleDetails.landingPage || 'none'
            };
        }

        case userRoleActionType.UPDATE_FIELD: {
            let { name, value } = action;
            let updateField = {};
            updateField[name] = value;
            return {
                ...state,
                ...updateField
            };
        }

        case userRoleActionType.SAVE_SUCCESS: {
            return {
                ...state,
                saveSuccess: true,
                saveErrorMessage: '',
                saveErrorData: []
            };
        }

        case userRoleActionType.SAVE_ERROR: {
            return {
                ...state,
                saveSuccess: false,
                saveErrorMessage: action.saveErrorMessage,
                saveErrorData: action.saveErrorData
            };
        }

        //access right
        case userRoleActionType.GET_ACCESS_RIGHT: {
            let landingSuccessPageList = _.cloneDeep(action.data).filter(item => item.isLandingPage === 'Y');
            let allMenu = getAllMenuList(action.data, false);
            let allMenuBK = _.cloneDeep(allMenu);
            return {
                ...state,
                allMenuList: allMenu,
                menuBK: allMenuBK,
                landingSuccessPageList
            };
        }
        case userRoleActionType.OPEN_MENU: {
            let { allMenuList } = state;
            let curOpenMenu = findByCd(allMenuList, action.accessRightCd, 'accessRightCd');
            console.log('5554431', allMenuList, curOpenMenu);
            curOpenMenu.open = !curOpenMenu.open;
            return {
                ...state,
                allMenuList
            };
        }
        case userRoleActionType.SELECT_MENU_LIST: {
            return {
                ...state,
                accessRights: action.accessRights
            };
        }
        default:
            return state;
    }
};