import * as clinicalNoteActionType from '../../actions/clinicalNote/clinicalNoteActionType';



const INITAL_STATE = {
    serviceListData:[],
    userRoleListData:[],
	encounterListData:[],
    recordTypeListData:[],    medicalListData:[],
    todayClinicalNoteListData:[],
    amendmentHistoryList:[],
    templateLength:0,
    encounterData:{},
    //sysConfig
    sysConfig:{}
};

export default (state = INITAL_STATE, action = {}) => {
    switch (action.type) {        case  clinicalNoteActionType.PUT_RECORDTYPE_DATA_LIST:{
            return { ...state,recordTypeListData:action.fillingRecordType.data};
        }
        case  clinicalNoteActionType.PUT_MEDICAL_RECORD_LIST:{
            return { ...state,medicalListData:action.fillingClinicalNoteDtoList,todayClinicalNoteListData:action.fillingTodayClinicalNoteDtoList};
        }
        case clinicalNoteActionType.SYS_CONFIG: {
            return {
                ...state,
                sysConfig: action.sysConfig
            };
        }
        case clinicalNoteActionType.PUT_AMENDMENT_HISTORY_LIST:{
            return {
                ...state,
                amendmentHistoryList: action.amendmentHistoryList
            };
        }
        default:
      return state;
    }
};