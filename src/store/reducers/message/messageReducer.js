import * as types from '../../actions/message/messageActionType';
import moment from 'moment';
import { isNull, find, cloneDeep, replace } from 'lodash';

const INIT_SATE = {
  openMessageDialog: false,
  openSnackbar: false,
  commonMessageList: [],
  commonMessageDetail: {},
  commonMessageSnackbarDetail: {}
};

let handleMessageParams = (msgDetail, params) => {
  let {
    header = null,
    description = null,
    cause = null,
    actionView = null
  } = msgDetail;

  let [tempHeader, tempDesc, tempCause, tempAction] = [header, description, cause, actionView];

  if (params.length > 0) {
    params.forEach(paramObj => {
      let reg = new RegExp(`%${paramObj.name}%`, 'g');
      tempHeader = !isNull(tempHeader) ? replace(tempHeader, reg, paramObj.value) : tempHeader;
      tempDesc = !isNull(tempDesc) ? replace(tempDesc, reg, paramObj.value) : tempDesc;
      tempCause = !isNull(tempCause) ? replace(tempCause, reg, paramObj.value) : tempCause;
      tempAction = !isNull(tempAction) ? replace(tempAction, reg, paramObj.value) : tempAction;
    });
  }

  msgDetail.header = tempHeader;
  msgDetail.description = tempDesc;
  msgDetail.cause = tempCause;
  msgDetail.actionView = tempAction;
};


export default (state = INIT_SATE, action = {}) => {
  switch (action.type) {
    case types.COMMON_MESSAGE_LIST: {
      return { ...state, commonMessageList: action.commonMessageList };
    }
    case types.OPEN_COMMON_MESSAGE: {
      let { msgCode, showSnackbar = false, msgObj = null, params = [], btnActions, setDescription,variant } = action.payload;
      let tempObj = {};
      if (!isNull(msgObj)) {
        tempObj = msgObj;
      } else {
        let tempDetail = find(state.commonMessageList, item => {
          return msgCode === item.messageCode;
        });
        tempObj = cloneDeep(tempDetail);
      }
      let { effective = null, expiry = null } = tempObj;
      let startDate = !isNull(effective) ? moment(effective).valueOf() : null,
        endDate = !isNull(expiry) ? moment(expiry).valueOf() : null,
        currentDate = moment().valueOf();
      let timeCheck = true;
      if (isNull(startDate) && isNull(endDate)) {
        timeCheck = false;
      }
      //handle params
      handleMessageParams(tempObj, params);
      let newObj = {
        ...tempObj,
        ...btnActions,
        setDescription
      };
      if (!timeCheck) {
        if (showSnackbar) {
          return {
            ...state,
            variant,
            openMessageDialog: false,
            openSnackbar: true,
            commonMessageSnackbarDetail: newObj
          };
        } else {
          return {
            ...state,
            openMessageDialog: true,
            openSnackbar: false,
            commonMessageDetail: newObj
          };
        }
      } else {
        const startPass = !isNull(startDate) ? startDate <= currentDate : true;
        const endPass = !isNull(endDate) ? currentDate < endDate : true;
        if (startPass && endPass) {
          if (showSnackbar) {
            return {
              ...state,
              variant,
              openMessageDialog: false,
              openSnackbar: true,
              commonMessageSnackbarDetail: newObj
            };
          } else {
            return {
              ...state,
              openMessageDialog: true,
              openSnackbar: false,
              commonMessageDetail: newObj
            };
          }
        } else {
          return {
            ...state
          };
        }
      }
    }
    case types.CLOSE_COMMON_MESSAGE: {
      return {
        ...state,
        openMessageDialog: false,
        openSnackbar: false
      };
    }
    case types.CLEAN_COMMON_MESSAGE_DETAIL: {
      return {
        ...state,
        openMessageDialog: false,
        commonMessageDetail: {}
      };
    }
    case types.CLEAN_COMMON_MESSAGE_SNACKBAR_DETAIL: {
      return {
        ...state,
        openSnackbar: false,
        commonMessageSnackbarDetail: {}
      };
    }
    default:
      return state;
  }
};
