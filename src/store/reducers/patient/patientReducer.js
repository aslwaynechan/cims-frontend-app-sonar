import * as type from '../../actions/patient/patientActionType';
import _ from 'lodash';

const initState = {
    codeList: [],
    patientInfo: null,
    eHRUrl: {},
    appointmentInfo: {},
    nationalityList: [],
    countryList: [],
    passportList: null,
    encounterInfo: [],
    caseNoInfo: {},
    languageData: {}
};

export default (state = initState, action = {}) => {
    switch (action.type) {
        case type.PUT_PATIENT_INFO: {
            return {
                ...state,
                patientInfo: action.data,
                appointmentInfo: action.appointmentInfo || {},
                caseNoInfo: action.caseNoInfo || {}
            };
        }
        case type.PUT_EHR_URL: {
            return {
                ...state,
                eHRUrl: action.data.url
            };
        }
        case type.PUT_GET_CODE_LIST: {
            return { ...state, codeList: action.codeList };
        }
        case type.PUT_LANGUAGE_LIST: {
            return { ...state, languageData: action.languageData };
        }
        case type.UPDATE_STATE: {
            let lastAction = { ...state };
            for (let p in action.updateData) {
                lastAction[p] = action.updateData[p];
            }
            return lastAction;
        }
        case type.RESET_ALL: {
            return {
                ...initState,
                countryList: state.countryList,
                nationalityList: state.nationalityList,
                appointmentInfo: {},
                caseNoInfo: {},
                languageData:state.languageData
            };
        }

        case type.LOAD_NATIONALITY_LIST_AND_COUNTRY_LIST: {
            return {
                ...state,
                nationalityList: action.nationalityList || [],
                countryList: action.countryList || []
            };
        }

        case type.LOAD_PASSPORT_LIST: {
            return {
                ...state,
                passportList: action.list
            };
        }

        case type.UPDATE_PATIENT_APPOINTMENT: {
            let appointmentInfo = action.appointmentInfo || {};
            return {
                ...state,
                appointmentInfo
            };
        }

        case type.UPDATE_PATIENT_CASENO: {
            let caseNoInfo = action.caseNoInfo || {};
            return {
                ...state,
                caseNoInfo
            };
        }

        case type.LOAD_PATIENT_ENCOUNTER_INFO: {
            let encounterInfo = action.encounterInfo || {};
            return {
                ...state,
                encounterInfo
            };
        }
        default:
            return state;
    }
};