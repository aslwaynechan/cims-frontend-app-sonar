export const CommonStyles={
    /**Gender */
    genderMaleColor: {
        color: '#D1EEFC',
        '&$genderMaleChecked': {
        color: '#D1EEFC'
        }
    },
    genderMaleChecked:{},
    genderFamaleColor:{
        color: '#FEDEED',
        '&$genderFamaleChecked': {
        color: '#FEDEED'
        }
    },
    genderFamaleChecked:{},
    genderUnknownColor:{
        color: '#FBD168',
        '&$genderUnknownChecked': {
        color: '#FBD168'
        }
    },
    genderUnknownChecked:{},

    /**Font */
    italicFont:{
        fontStyle:'italic'
    },

    /**tab */
    tabStyle:{
        border: '1px solid #B8BCB9',
        paddingLeft: 5,
        borderRadius: '20px 20px 0 0',
        backgroundColor:'#FFFFFF',
        color:'#0579C8',
        '&$tabSelected': {
          color: '#000000'
        },
        '&$tabHover': {
          color: '#000000',
          backgroundColor:'#B8BCB9'
        }
    },
    tabSelected:{},
    tabHover:{}

};