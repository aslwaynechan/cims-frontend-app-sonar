import axios from '../axiosInstance';
import * as ecsUtil from '../../utilities/ecsUtilities';
import Enum from '../../enums/enum';

const url = {
  checkEcs: '/patient/ecs/checkEcs',
  checkOcsss: '/patient/ecs/checkOcsss',
  checkMwecs: '/patient/ecs/checkMwecs'
};

export function checkEcs(params) {
  return axios.post(url.checkEcs, params);
}

export function checkOcsss(params) {
  return axios.post(url.checkOcsss, params);
}

export function checkMwecs(params) {
  return axios.post(url.checkMwecs, params);
}

export function transformEcsResponseDataToReduxState(
  responseData,
  lastCheckedTime,
  isAssoicated,
  hkic
) {

  let result = responseData.result[0];

  result.hkId = responseData.hkId;
  result.dob = responseData.dob || responseData.dbo;
  result.dobEstd = responseData.dobEstd;
  result.lastCheckedTime = lastCheckedTime;
  result.isAssoicated = isAssoicated;
  result.patientEcsId = -1;
  result.originalHkid = hkic;
  let resultIndex = ecsUtil.getEligibleIndexFromEcsResult(result);
  result.isValid = resultIndex === 1 || resultIndex === 2;
  result.isInitState = false;
  return result;
}

export function transformOcsssResponseDataToReduxState(
  responseData,
  lastCheckedTime,
  hkic
) {
  return {
    checkingResult: responseData.checkingResult,
    errorMessage: responseData.errorMessage,
    messageId: responseData.messageId,
    replyDateTime: responseData.replyDateTime,
    originalHkid: hkic,
    patientOcsssId: -1,
    isValid: responseData.checkingResult === 'Y',
    lastCheckedTime: lastCheckedTime,
    isInitState: false
  };
}

export function transformMwecsResponseDataToReduxState(
  responseData,
  lastCheckedTime,
  idNumber
) {
  let firstElement = responseData[0];

  const defaultIfUndefined = (item, key, defaultVal) => {return item ? item[key] : defaultVal;};
  const result = defaultIfUndefined(firstElement, 'result', '') ;
  const resultType = Enum.MWECS_RESULT_TYPES.find(item => item.key === result);
  return {
    messageId: defaultIfUndefined(firstElement, 'messageId', ''),
    result: result ,
    recipientName: defaultIfUndefined(firstElement, 'recipientName', '') ,
    eligStartDate: defaultIfUndefined(firstElement, 'eligStartDate', '') ,
    eligEndDate: defaultIfUndefined(firstElement, 'eligEndDate', ''),
    swdResponseDt: defaultIfUndefined(firstElement, 'swdResponseDt', ''),
    responseDt: defaultIfUndefined(firstElement, 'responseDt', ''),
    errorCode: defaultIfUndefined(firstElement, 'errorCode', ''),
    errorMessage: defaultIfUndefined(firstElement, 'errorMessage', ''),
    originalDocNo: idNumber,
    patientMwecsId: -1,
    isValid: resultType && resultType.eligible,
    lastCheckedTime: lastCheckedTime,
    isInitState: false
  };
}