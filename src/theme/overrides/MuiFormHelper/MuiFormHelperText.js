import palette from '../../palette';

export default {
    root: {
        '&$error': {
            color: palette.error.main,
            padding: '5px 14px',
            fontSize: 12
        }
    }
};