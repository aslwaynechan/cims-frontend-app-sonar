
import palette from '../../palette';

export default {
    root: {
        padding: '5px 14px'

    },
    colorPrimary: {
        '&$checked': {
            color: palette.primary.main
        },
        '&$disabled': {
            color: palette.grey.default
        }
    },
    colorSecondary: {
        '&$disabled': {
            color: palette.grey.default
        }
    }
};