export default {
    fontFamily: 'Arial, MingLiU, Helvertica, sans-serif',
    useNextVariants: true,
    h1: {
        fontFamily: 'Arial, MingLiU, Helvertica, sans-serif'
    },
    h2: {
        fontFamily: 'Arial, MingLiU, Helvertica, sans-serif'
    },
    h3: {
        fontFamily: 'Arial, MingLiU, Helvertica, sans-serif'
    },
    h4: {
        fontFamily: 'Arial, MingLiU, Helvertica, sans-serif'
    },
    h5: {
        fontFamily: 'Arial, MingLiU, Helvertica, sans-serif'
    },
    h6: {
        fontFamily: 'Arial, MingLiU, Helvertica, sans-serif'
    },
    subtitle1: {
        fontFamily: 'Arial, MingLiU, Helvertica, sans-serif'
    },
    subtitle2: {
        fontFamily: 'Arial, MingLiU, Helvertica, sans-serif'
    },
    body1: {
        fontFamily: 'Arial, MingLiU, Helvertica, sans-serif'
    },
    body2: {
        fontFamily: 'Arial, MingLiU, Helvertica, sans-serif'
    },
    button: {
        fontFamily: 'Arial, MingLiU, Helvertica, sans-serif'
    },
    caption: {
        fontFamily: 'Arial, MingLiU, Helvertica, sans-serif'
    },
    overline: {
        fontFamily: 'Arial, MingLiU, Helvertica, sans-serif'
    }
};