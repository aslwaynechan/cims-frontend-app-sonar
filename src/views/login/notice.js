import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Typography, Tooltip } from '@material-ui/core';
import SelectFieldValidator from '../../components/FormValidator/SelectFieldValidator';
import ValidatorForm from '../../components/FormValidator/ValidatorForm';
import AutoScrollTable from '../../components/Table/AutoScrollTable';
import { withStyles } from '@material-ui/core/styles';
import { getServiceNotice } from '../../store/actions/login/loginAction';
import _ from 'lodash';
import moment from 'moment';
import memoize from 'memoize-one';
import { DeleteForever, FiberNewRounded, NotificationImportantRounded, LabelImportant } from '@material-ui/icons';
import Enum from '../../enums/enum';

const style = () => ({
    buttonRoot: {
        padding: 0,
        textTransform: 'none'
    },
    gridPadding15: {
        paddingBottom: 15
    },
    detailTypography: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        textAlign: 'justify',
        fontSize: '0.875rem',
        fontWeight: 400
    },
    svgIcon: {
        marginBottom: -5
    }
});

class Notice extends Component {

    state = {
        columns: [
            {
                label: 'To',
                name: 'recipient',
                width: '15%'
            },
            {
                label: 'Date',
                name: 'updatedDtm',
                width: '20%',
                customBodyRender: (value) => {
                    return value ? moment(value).format(Enum.DATE_FORMAT_24_HOUR) : value;
                }
            },
            {
                label: 'Details',
                name: 'content',
                width: '65%',
                customBodyRender: (value, rowData) => {
                    return (
                        <Typography component="div" className={this.props.classes.detailTypography}>
                            {rowData.isDel ? <Tooltip title="Delete"><DeleteForever className={this.props.classes.svgIcon} /></Tooltip> : null}
                            {rowData.isImprtnt ? <Tooltip title="Important"><LabelImportant color="secondary" className={this.props.classes.svgIcon} /></Tooltip> : null}
                            {rowData.isNew ? <Tooltip title="New"><FiberNewRounded htmlColor="blue" className={this.props.classes.svgIcon} /></Tooltip> : null}
                            {rowData.isUrg ? <Tooltip title="Urgent"><NotificationImportantRounded color="error" className={this.props.classes.svgIcon} /></Tooltip> : null}
                            {value}
                        </Typography>
                    );
                }
            }
        ],
        serviceCd: '',
        selectIndex: []
    }

    componentDidMount() {
        this.props.getServiceNotice({ serviceCd: '' });
    }

    filterNotice = memoize((list, serviceCd) => {
        if (serviceCd) {
            return list.filter(item => item.serviceCd === serviceCd);
        } else {
            return list;
        }
    });

    handleChange = (value, name) => {
        this.setState({ [name]: value });
    }

    handleClick = () => {

    }

    handleTableRowClick = (e, row, index) => {
        if (this.state.selectIndex.indexOf(index) > -1) {
            this.setState({ selectIndex: [] });
        } else {
            this.setState({ selectIndex: [index] });
        }
    }

    render() {
        const { classes } = this.props;
        let serviceList = [];
        if (this.props.serviceList) {
            serviceList = _.cloneDeep(this.props.serviceList);
            serviceList.splice(0, 0, { serviceCd: '', serviceName: 'All Service' });
        }
        const noticeFilterList = this.filterNotice(this.props.noticeList, this.state.serviceCd);
        return (
            <ValidatorForm onSubmit={() => { }}>
                <Grid container>
                    <Grid item container xs={12} justify="space-between" className={classes.gridPadding15}>
                        <Grid item xs={6}>
                            <SelectFieldValidator
                                id="notice_serviceCd"
                                options={serviceList.map((item) => (
                                    { value: item.serviceCd, label: item.serviceName }
                                ))}
                                value={this.state.serviceCd}
                                placeholder="All Service"
                                onChange={e => this.handleChange(e.value, 'serviceCd')}
                            />
                        </Grid>
                        {/* <Grid item container xs={6} justify="flex-end">
                            <Button
                                id="notice_pause"
                                variant="contained"
                                color="primary"
                                size="small"
                                className={classes.buttonRoot}
                                onClick={this.handleClick}
                            >Pause</Button>
                        </Grid> */}
                    </Grid>
                    <Grid item container xs={12}>
                        <AutoScrollTable
                            id="login_notice_scrollTable"
                            columns={this.state.columns}
                            store={noticeFilterList}
                            selectIndex={this.state.selectIndex}
                            handleRowClick={this.handleTableRowClick}
                        />
                    </Grid>
                </Grid>
            </ValidatorForm>
        );
    }
}

function mapStateToProps(state) {
    return {
        serviceList: state.common.serviceList,
        noticeList: state.login.noticeList
    };
}
const dispatchProps = {
    getServiceNotice
};

export default connect(mapStateToProps, dispatchProps)(withStyles(style)(Notice));