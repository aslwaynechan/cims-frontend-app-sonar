import React, { Component } from 'react';
import { withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import cover_EHS from '../../images/loginPage/cover_EHS.jpg';
import cover_FHS from '../../images/loginPage/cover_FHS.jpg';
import cover_TBC from '../../images/loginPage/cover_TBC.jpg';
import cover_THS from '../../images/loginPage/cover_THS.jpg';
import cover_CAS from '../../images/loginPage/cover_CAS.png';
import cover_CGS from '../../images/loginPage/cover_CGS.png';
import cover_DH from '../../images/loginPage/cover_DH.jpg';
import cover_LOGIN from '../../images/login.png';
import { withStyles } from '@material-ui/core/styles';
import LoginForm from './loginForm';
import { Grid, Typography, DialogContent, DialogActions, Button, Grow } from '@material-ui/core';
import { SupervisedUserCircleRounded } from '@material-ui/icons';
import Notice from './notice';
import ImportantNotes from './component/importantNotes';
import ContactUs from './component/contactUs';
import { listService, listClinic } from '../../store/actions/common/commonAction';
import CIMSDialog from '../../components/Dialog/CIMSDialog';
import { getMessageListByAppId } from '../../store/actions/message/messageAction';
import { listServiceByClientIp } from '../../store/actions/login/loginAction';

const style = () => ({
  root: {
    alignItems: 'center',
    overflowY: 'auto',
    overflowX: 'hidden'
  },
  inputWidth: {
    width: '100%'
  },
  input: {
    paddingLeft: 10
  },
  container: {
  },
  loginField: {
    paddingRight: 30,
    paddingLeft: 40,
    paddingTop: 10
  },
  noticeField: {
    paddingRight: 40,
    paddingLeft: 30,
    paddingTop: 10
  },
  header: {
    paddingTop: 10
  },
  footer: {
    // position: 'fixed',
    left: 0,
    bottom: 0,
    width: '100%',
    zIndex: 999
  }
});

class Login extends Component {
  constructor(props) {
    super(props);

    const params = this.props.match.params;
    this.state = {
      loginName: '',
      password: '',
      serviceCode: params.serviceCd || 'FCS',
      serviceDesc: 'Family Clinics',
      clinicCode: params.clinicCd || 'KFC',
      clinicDesc: 'Kowloon Families Clinic',
      openDialog: false,
      dialogName: ''
    };
  }

  componentDidMount() {
    this.props.listService();
    this.props.listClinic();
    this.props.getMessageListByAppId({ applicationId: 1 });
    this.props.listServiceByClientIp();

    document.getElementsByTagName('html')[0].style.height = '100%';
    document.getElementsByTagName('body')[0].style.height = '100%';
    document.getElementById('root').style.height = '100%';
    document.getElementById('root').style.display = 'flex';
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps !== this.props || nextState !== this.state;
  }

  handleUpdateLoginForm = (name, value) => {
    this.setState({ [name]: value });
  }

  handleOpenDialog = (e, name) => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({ openDialog: true, dialogName: name });
  }

  handleCloseDialog = () => {
    this.setState({ openDialog: false });
  }

  render() {
    const { classes } = this.props;
    if (this.props.isLoginSuccess) {
      return <Redirect to={'/index'} />;
    }
    // if (window.location.hostname !== 'localhost' && window.name !== 'CMIS') {
    //   window.open(window.location.href, 'CMIS', `width=${window.screen.availWidth},height=${window.screen.availHeight - 60}, left=0,top=0, toolbar =no, menubar=no, scrollbars=yes, resizable=yes, location=no, status=no`, 'true - URL');
    //   return (null);
    // }
    return (
      <Grid container className={classes.root}>
        {/* <Grid container justify="center" className={classes.header}><img src={cover_EHS} alt="" /></Grid> */}
        <Grid container className={classes.container} justify="center">
          <Grid item container xs={6} className={classes.loginField}>
            <Grid item container justify="center" alignItems="center" style={{ marginBottom: 30 }}>
              {
                (() => {
                  let src = '';
                  switch (this.state.serviceCode.toUpperCase()) {
                    case 'THS': src = cover_THS; break;
                    case 'FHS': src = cover_FHS; break;
                    case 'EHS': src = cover_EHS; break;
                    case 'TCS': src = cover_TBC; break;
                    case 'CAS': src = cover_CAS; break;
                    case 'CG': src = cover_CGS; break;
                    case 'SHS': src = cover_LOGIN; break;
                    default: src = cover_DH; break;
                  }
                  if (this.state.serviceCode && this.state.clinicCode) {
                    return <Grow key={this.state.serviceCode} in timeout={1000}><img src={src} alt="" style={{ width: '100%', maxHeight: 300 }} /></Grow>;
                  } else {
                    return (
                      <Grow in timeout={1000}>
                        <Grid container alignItems="center" justify="center" direction="column" style={{ marginBottom: 15 }}>
                          <Grid item><SupervisedUserCircleRounded color="primary" style={{ fontSize: 60 }} /></Grid>
                          <Grid item><Typography variant="h5">Sign in</Typography></Grid>
                        </Grid>
                      </Grow>
                    );
                  }
                })()
              }
            </Grid>
            <Grid item container>
              <LoginForm
                  updateState={this.handleUpdateLoginForm}
                  loginName={this.state.loginName}
                  password={this.state.password}
                  serviceCode={this.state.serviceCode}
                  serviceDesc={this.state.serviceDesc}
                  clinicCode={this.state.clinicCode}
                  clinicDesc={this.state.clinicDesc}
              />
            </Grid>
          </Grid>
          {/* <Grid item container xs={6} className={classes.noticeField}>
            <Grid item container xs={12} justify="center" alignItems="center" style={{ minHeight: 300 }}>
              {
                (() => {
                  let src = '';
                  switch (this.state.serviceCode) {
                    case 'THS': src = cover_THS; break;
                    case 'FHS': src = cover_FHS; break;
                    case 'EHS': src = cover_EHS; break;
                    case 'TCS': src = cover_TBC; break;
                    default: src = ''; break;
                  }
                  if (src) {
                    return <img src={src} alt="" style={{ width: '100%', maxHeight: 300 }} />;
                  } else if (this.state.serviceCode) {
                    return <Typography variant="h4">{this.state.serviceDesc}</Typography>;
                  } else {
                    return <Typography variant="h4">No Service</Typography>;
                  }
                })()
              }

            </Grid>
            <Grid item xs={12}><Notice /></Grid>
          </Grid> */}
        </Grid>
        <Grid container className={classes.footer}>
          <Grid item container justify="center">{this.props.clientIp && this.props.clientIp.length > 0 && this.props.clientIp[0].ipAddr}</Grid>
          <Grid item container justify="center">
            <Grid item><Typography>ASL(TeamA+B) : sprint 14-2020/01/13 + JOS version : sprint 14-2020/01/23</Typography></Grid>
          </Grid>
          <Grid item container justify="center" alignItems="center">
            <Grid item><Typography>© Department of Health, HKSARG. All rights reserved.</Typography></Grid>
          </Grid>
          <Grid item container justify="center" alignItems="center" spacing={1}>
            <Grid item><Typography component="a" href="" onClick={e => this.handleOpenDialog(e, 'Contact Us')}>Contact Us</Typography></Grid>
            <Grid item><Typography>|</Typography></Grid>
            <Grid item><Typography component="a" href="" onClick={e => this.handleOpenDialog(e, 'Important Notes')}>Important Notes</Typography></Grid>
            <Grid item><Typography>|</Typography></Grid>
            <Grid item><Typography component="a" href="" onClick={e => this.handleOpenDialog(e, 'Notice Board')}>Notice Board</Typography></Grid>
          </Grid>
        </Grid>
        <CIMSDialog
            id="login_dialog"
            open={this.state.openDialog}
            onClose={this.handleCloseDialog}
            dialogTitle={this.state.dialogName}
        >
          <DialogContent id={'login_dialog_content'}>
            {
              (() => {
                if (this.state.dialogName === 'Contact Us') {
                  return <ContactUs />;
                } else if (this.state.dialogName === 'Important Notes') {
                  return <ImportantNotes />;
                } else if (this.state.dialogName === 'Notice Board') {
                  return <Notice />;
                }
              })()
            }
          </DialogContent>
          <DialogActions className={classes.dialogAction}>
            <Button
                id="login_dialog_closebtn"
                variant="contained"
                color="primary"
                size="small"
                style={{ textTransform: 'none' }}
                onClick={this.handleCloseDialog}
            >Close</Button>
          </DialogActions>
        </CIMSDialog>
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    clinicList: state.common.clinicList,
    isLoginSuccess: state.login.isLoginSuccess,
    loginInfo: state.login.loginInfo,
    clientIp: state.login.clientIp
  };
}
const dispatchProps = {
  listService,
  listClinic,
  getMessageListByAppId,
  listServiceByClientIp
};
export default withRouter(connect(mapStateToProps, dispatchProps)(withStyles(style)(Login)));
