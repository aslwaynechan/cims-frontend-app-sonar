import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Typography, Button, Grid, InputAdornment, IconButton } from '@material-ui/core';
// import { Business, LocalHospital, AccountBox, AccountCircle, Lock, LockOpen } from '@material-ui/icons';
import { AccountBox, AccountCircle, Lock, LockOpen, Visibility, VisibilityOff } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
import styles from './login.module.scss';
import TextFieldValidator from '../../components/FormValidator/TextFieldValidator';
// import SelectFieldValidator from '../../components/FormValidator/SelectFieldValidator';
import CommonMessage from '../../constants/commonMessage';
import ValidatorForm from '../../components/FormValidator/ValidatorForm';
import { resetAll } from '../../store/actions/mainFrame/mainFrameAction';
import {
  doLogin,
  resetErrorMsg,
  putLoginInfo
} from '../../store/actions/login/loginAction';
import { Link as RouterLink } from 'react-router-dom';
import memoize from 'memoize-one';

// const customTheme = (theme) => createMuiTheme({
//   ...theme,
//   overrides: {
//     ...theme.overrides,
//     MuiInputBase: {
//       root: {
//         height: 40,
//         lineHeight: 'inherit'
//       },
//       input: {
//         padding: '8px 14px'
//       }
//     },
//     MuiOutlinedInput: {
//       input: {
//         padding: '8px 14px'
//       }
//     },
//     MuiInputLabel: {
//       outlined: {
//         zIndex: 0,
//         transform: 'translate(14px, 12px) scale(1)',
//         '&$shrink': {
//           zIndex: 1
//         }
//       }
//     }
//   }
// });

const style = () => ({
  buttonRoot: {
    width: '100%',
    textTransform: 'none'
  },
  gridMargin20: {
    marginBottom: 20
  }
});

class LoginForm extends Component {

  state = {
    showPassword: false
  }

  componentDidMount() {
    this.props.resetAll();
    window.onkeydown = this.handleKeyDown;
  }

  componentWillUnmount() {
    window.onkeydown = null;
  }

  filterClinic = memoize((clinicList, serviceCd) => {
    return clinicList.filter(item => item.serviceCd === serviceCd);
  });

  // handleSubmit=(e)=>{
  //   e.preventDefault();
  //   this.props.resetErrorMsg();
  //   this.refs.form.submit();
  // }

  getCurLoginServiceAndClinic = (serviceCd, clinicCd) => {
    const { serviceList, clinicList } = this.props;
    let curLoginService = serviceList.find(item => item.serviceCd === serviceCd);
    let curLoginClinic = clinicList.find(item => item.clinicCd === clinicCd);
    curLoginService = curLoginService ? curLoginService : { serviceCd: serviceCd };
    curLoginClinic = curLoginClinic ? curLoginClinic : { clinicCd: clinicCd };

    return { service: curLoginService, clinic: curLoginClinic };
  }


  handleLogin = (e) => {
    e.preventDefault();
    const currentClinicDetail = this.props.clinicList.filter((item) => item.clinicCd === this.props.clinicCode)[0];
    const ecsLocCode = currentClinicDetail ? currentClinicDetail.ecsLocCd : '';
    const params = {
      serviceCode: this.props.serviceCode,
      clinicCode: this.props.clinicCode,
      ecsLocCode: ecsLocCode,
      loginName: this.props.loginName,
      password: this.props.password,
      serviceDesc: this.props.serviceDesc,
      clinicDesc: this.props.clinicDesc
    };
    let curLoginServiceAndClinic = this.getCurLoginServiceAndClinic(this.props.serviceCode, this.props.clinicCode);
    this.props.doLogin(params);
    this.props.putLoginInfo(curLoginServiceAndClinic);
  };

  handleChange = (e) => {
    this.props.resetErrorMsg();
    this.props.updateState(e.target.name, e.target.value);
  }
  handleSelectChange = (e, name) => {
    this.props.resetErrorMsg();
    const value = e.value;
    const label = e.label;
    const codeName = name + 'Code';
    if (codeName === 'serviceCode') {
      this.props.updateState('clinicCode', null);
    }
    const descName = name + 'Desc';
    this.props.updateState([descName], label);
    this.props.updateState([codeName], value);
  }
  handleKeyDown = (e) => {
    if (e.keyCode === 13) {
      this.handleLogin(e);
    }
  }
  handleValidationFormError = () => {
    this.props.resetErrorMsg();
  }

  handleMouseDownPassword = () => {
    this.setState({ showPassword: true });
  }

  handleMouseUpPassword = () => {
    this.setState({ showPassword: false });
  }

  handleMouseLeavePassword = () => {
    this.setState({ showPassword: false });
  }

  render() {
    const { classes, loginName, password } = this.props;
    // const { classes, loginName, password, serviceCode, clinicCode, clinicList } = this.props;
    // const fliterClinicList = this.filterClinic(clinicList, serviceCode);
    return (
      // <MuiThemeProvider theme={customTheme}>
      <Grid container>
        <ValidatorForm
            id="loginForm"
            ref={'form'}
            onSubmit={this.handleLogin}
            onError={this.handleValidationFormError}
            autoComplete="false"
            style={{
            width: '100%',
            marginTop: 5
          }}
        >
          <Grid item container>
            {/* <Grid item container alignItems="center">
              <Grid item xs={1}><Business /></Grid>
              <Grid item xs={11}>
                <SelectFieldValidator
                    options={this.props.serviceList &&
                    this.props.serviceList.map((item) => (
                      { value: item.code, label: item.engDesc }
                    ))}
                    value={serviceCode}
                    onChange={(...arg) => this.handleSelectChange(...arg, 'service')}
                    id={'service'}
                    validators={['required']}
                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                    TextFieldProps={{
                    label: 'Service Region'
                  }}
                />
              </Grid>
            </Grid> */}
            {/* <Grid item container alignItems="center">
              <Grid item xs={1}><LocalHospital /></Grid>
              <Grid item xs={11}>
                <SelectFieldValidator
                    options={fliterClinicList && fliterClinicList.map((item) => (
                      { value: item.clinicCd, label: item.clinicName }))}
                    value={clinicCode}
                    onChange={(...arg) => this.handleSelectChange(...arg, 'clinic')}
                    id={'clinicCode'}
                    validators={['required']}
                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                    TextFieldProps={{
                    label: 'Clinic'
                  }}
                />
              </Grid>
            </Grid> */}
            <Grid item container alignItems="center" className={classes.gridMargin20}>
              <Grid item xs={1}><AccountBox /></Grid>
              <Grid item xs={11}>
                <TextFieldValidator
                    fullWidth
                    upperCase
                    trim={false}
                    onChange={this.handleChange}
                    onBlur={this.handleChange}
                    value={loginName}
                    name={'loginName'}
                    id={'loginName'}
                    label="Login ID"
                    msgPosition="bottom"
                    validators={['required']}
                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                />
              </Grid>
            </Grid>
            <Grid item container alignItems="center" className={classes.gridMargin20}>
              <Grid item xs={1}><Lock /></Grid>
              <Grid item xs={11}>
                <TextFieldValidator
                    fullWidth
                    trim={false}
                    onPaste={() => { window.event.returnValue = false; }}
                    onContextMenu={() => { window.event.returnValue = false; }}
                    onCopy={() => { window.event.returnValue = false; }}
                    onCut={() => { window.event.returnValue = false; }}
                    onChange={this.handleChange}
                    type={this.state.showPassword ? 'text' : 'password'}
                    value={password}
                    name={'password'}
                    id={'password'}
                    label="Password"
                    msgPosition="bottom"
                    validators={['required']}
                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                    InputProps={{
                    endAdornment:
                      <InputAdornment position="end">
                        <IconButton
                            aria-label="toggle password visibility"
                            id="passwordVisibility"
                            onMouseDown={this.handleMouseDownPassword}
                            onMouseUp={this.handleMouseUpPassword}
                            onMouseLeave={this.handleMouseLeavePassword}
                        >
                          {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>

                  }}
                />
              </Grid>
            </Grid>
            {this.props.isLoginSuccess === false && this.props.errorMessage ? (
              <Grid item>
                <Typography
                    style={{ color: 'red', fontWeight: 'bold', fontSize: '14px' }}
                    className={styles.float_margintLeft}
                >
                  {this.props.errorMessage}
                </Typography>
              </Grid>
            ) : null}
            <Grid item container justify="flex-end" style={{ marginBottom: 10 }}>
              {/* <Grid item container justify="flex-end" xs={11}>
                <Link
                    id={'forgetPassword'}
                    className={styles.forget_password}
                    style={{ marginBottom: 8, textDecoration: 'none', display: 'inherit', fontSize: '10pt' }}
                    component={RouterLink}
                    to="/forgetPassword"
                >Forget your password</Link>
              </Grid> */}
              <Grid item xs={12}>
                <Button
                    id="btn_signIn"
                    variant="contained"
                    color="primary"
                    size="medium"
                    // onKeyDown={this.handleKeyDown}
                    type="submit"
                    className={classes.buttonRoot}
                    style={{ marginBottom: 8 }}
                >
                  <AccountCircle style={{ marginRight: 8 }} />
                  Sign in
                </Button>
              </Grid>
              <Grid item xs={12}>
                <Button
                    id="btn_forgetPassword"
                    variant="contained"
                    color="primary"
                    size="medium"
                    type="submit"
                    className={classes.buttonRoot}
                    component={RouterLink}
                    to="/forgetPassword"
                >
                  <LockOpen style={{ marginRight: 8 }} />
                  Forgot Password
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </ValidatorForm>
      </Grid>
      // </MuiThemeProvider>
    );
  }
}
function mapStateToProps(state) {
  return {
    serviceList: state.common.serviceList,
    clinicList: state.common.clinicList,
    isLoginSuccess: state.login.isLoginSuccess,
    errorMessage: state.login.errorMessage
  };
}
const dispatchProps = {
  doLogin,
  resetErrorMsg,
  resetAll,
  putLoginInfo
};
export default withRouter(connect(mapStateToProps, dispatchProps)(withStyles(style)(LoginForm)));