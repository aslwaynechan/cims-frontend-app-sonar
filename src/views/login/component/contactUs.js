import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Grid, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { getContactUs } from '../../../store/actions/login/loginAction';

const styles = {
    gridPadding10: {
        paddingLeft: 10
    }
};

function ContactUs(props) {
    /* eslint-disable */
    useEffect(() => {
        props.getContactUs();
    }, []);
    /* eslint-enable */
    const { classes, contactConfigs } = props;
    const address = contactConfigs.find(item => item.configName === 'CONTACT_ADDRESS');
    const fax = contactConfigs.find(item => item.configName === 'CONTACT_FAX');
    const phone = contactConfigs.find(item => item.configName === 'CONTACT_PHONE');
    const email = contactConfigs.find(item => item.configName === 'CONTACT_EMAIL');
    const note = contactConfigs.find(item => item.configName === 'CONTACT_LOTUS_NOTES');
    return (
        <>
            <Grid container direction="column" justify="center" alignItems="center">
                <Typography component="p" variant="subtitle1" style={{ textDecoration: 'underline' }}>Helpdesk support information:</Typography>
                <Typography component="p" variant="subtitle2">Electronic Health Record Management Team</Typography>
                <Grid item container justify="center">
                    <Grid item xs={2} container justify="flex-end">Address:</Grid>
                    <Grid item xs={4} className={classes.gridPadding10}>{address && address.configValue}</Grid>
                </Grid>
                <Grid item container justify="center">
                    <Grid item xs={2} container justify="flex-end">Fax:</Grid>
                    <Grid item xs={4} className={classes.gridPadding10}>{fax && fax.configValue}</Grid>
                </Grid>
                <Grid item container justify="center">
                    <Grid item xs={2} container justify="flex-end">Phone:</Grid>
                    <Grid item xs={4} className={classes.gridPadding10}>{phone && phone.configValue}</Grid>
                </Grid>
                <Grid item container justify="center">
                    <Grid item xs={2} container justify="flex-end">Email Address:</Grid>
                    <Grid item xs={4} className={classes.gridPadding10}>{email && email.configValue}</Grid>
                </Grid>
                <Grid item container justify="center">
                    <Grid item xs={2} container justify="flex-end">Lotus Notes:</Grid>
                    <Grid item xs={4} className={classes.gridPadding10}>{note && note.configValue}</Grid>
                </Grid>
            </Grid>
        </>
    );
}

function mapStateToProps(state) {
    return {
        contactConfigs: state.login.contactConfigs
    };
}

const mapDispatchToProps = {
    getContactUs
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ContactUs));