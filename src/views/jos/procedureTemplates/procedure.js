/* Save Action: [procedureTemplate.js] Save -> handleCheckSave -> [procedureTemplateAction.js] saveTemplateList
-> [procedureTemplateSaga.js] saveTemplateList ->Backend API = /procedure/saveProcedureTemplateGroup  */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Card,
  CardContent,
  Grid,
  Button,
  Typography,
  CardHeader
} from '@material-ui/core';
import en_US from '../../../locales/en_US';
import { withStyles } from '@material-ui/core/styles';
import { AddCircle } from '@material-ui/icons';
import { RemoveCircle } from '@material-ui/icons';
import { Edit } from '@material-ui/icons';
import { ArrowUpwardOutlined } from '@material-ui/icons';
import { ArrowDownward } from '@material-ui/icons';
import moment from 'moment';
import { requestProcedureTemplateList, saveTemplateList, getEditTemplateList, getConfig } from '../../../store/actions/procedure/procedureAction';
import 'react-quill/dist/quill.snow.css';
import { openCommonMessage, closeCommonMessage } from '../../../store/actions/message/messageAction';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import CIMSTable from '../../../components/Table/CimsTableNoPagination';
import { Prompt } from 'react-router-dom';
import FieldConstant from '../../../constants/fieldConstant';
import { style } from './procedureCss';
import EditProcedureTemplate from '../../editTemplate/EditProcedureTemplate';
import { PROCEDURE_CODE } from '../../../constants/message/procedureCode';
import { openCommonCircularDialog } from '../../../store/actions/common/commonAction';
import { SYSCONFIGKEY_CODE } from '../../../constants/sysConfigKey';
import Container from 'components/JContainer';

class ProcedureTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sequence: null,
      isSave: true,
      open: false,  // open dialog
      serviceCd: JSON.parse(sessionStorage.getItem('service')).serviceCd,//
      serviceEngDesc: JSON.parse(sessionStorage.getItem('service')).serviceName,//
      isEdit: false,
      templateList: [],
      editTemplateList: [],
      pageNum: null,
      deleteList: [],
      selectObj: null, //选中的行对象
      tableRows: [
        { name: 'sequence', width: 42, label: 'Seq' },
        { name: 'groupName', width: 'auto', label: 'Group Name' },
        { name: 'updatedByName', width: 180, label: 'Updated By' },
        {
          name: 'updatedDtm', label: 'Updated On', width: 140, customBodyRender: (value) => {
            return value ? moment(value).format('DD-MMM-YYYY') : null;
          }
        }
      ],
      tableOptions: {
        rowHover: true,
        rowsPerPage: 5,
        onSelectIdName: 'sequence',
        tipsListName: 'diagnosisTemplates',
        tipsDisplayListName: null,
        tipsDisplayName: 'diagnosisDisplayName',
        onSelectedRow: (rowId, rowData, selectedData) => {
          this.selectTableItem(selectedData);
        },
        bodyCellStyle: this.props.classes.customRowStyle,
        headRowStyle: this.props.classes.headRowStyle,
        headCellStyle: this.props.classes.headCellStyle
      }
    };
  }

  componentDidMount() {
    this.props.ensureDidMount();
    this.initData();
  }
  //获取tips显示的长度
  getTipsSize = () => {
    let configParams = {
      key: SYSCONFIGKEY_CODE.TMPL_TIPS_SIZE
    };
    this.props.getConfig({
      configParams, callback: (tipsListSize) => {
        this.setState({
          tipsListSize: parseInt(tipsListSize)
        });
      }
    });
  };

  initData = () => {
    const params = {};
    this.getTipsSize();
    this.props.requestProcedureTemplateList({
      params,
      callback: (templateList) => {
        this.setState({
          isSave: true,
          templateList: templateList.data,
          sequence: null,
          selectRow: null,
          selectObj: null
        });
      }
    });
  };

  //获得选中行数据
  getSelectRow = (data) => {
    this.setState({
      sequence: data.sequence,
      selectRow: data.sequence,
      selectObj: data
    });
  }

  checkSelect(msgCode) {
    if (this.state.sequence === null) {
      let payload = {
        msgCode: msgCode,
        btnActions: {
          // Yes
          btn1Click: () => {
          }
        }
      };
      this.props.openCommonMessage(payload);
    }
  }

  handleClickDown = () => {
    let msgCode = PROCEDURE_CODE.IS_SELECTED_TEMPLATE_GROUP_DWON;
    this.checkSelect(msgCode);
    let templateList = this.state.templateList;
    let sequence = this.state.sequence;
    if (sequence) {
      let index = this.state.sequence - 1;
      if (templateList[index + 1]) {
        templateList[index].sequence = sequence + 1;
        templateList[index + 1].sequence = sequence;
        [templateList[index], templateList[index + 1]] = [templateList[index + 1], templateList[index]];
        this.setState({
          sequence: sequence + 1,
          selectRow: sequence + 1,
          templateList: templateList,
          isSave: false
        });
      }
    }
  }

  handleClickUp = () => {
    let msgCode = PROCEDURE_CODE.IS_SELECTED_TEMPLATE_GROUP_UP;
    this.checkSelect(msgCode);
    let templateList = this.state.templateList;
    let sequence = this.state.sequence;
    if (sequence) {
      let index = this.state.sequence - 1;
      if (templateList[index - 1]) {
        templateList[index].sequence = index;
        templateList[index - 1].sequence = sequence;
        [templateList[index], templateList[index - 1]] = [templateList[index - 1], templateList[index]];
        this.setState({
          sequence: index,
          selectRow: index,
          templateList: templateList,
          isSave: false
        });
      }
    }
  }

  handleCheckSave = () => {
    let payload = {
      msgCode: PROCEDURE_CODE.IS_TP_SAVE_COMFIRM,
      btnActions: {
        // Yes
        btn1Click: () => {
        }
      }
    };
    this.props.openCommonMessage(payload);
  }

  handleClickSave = () => {
    let saveList = this.state.templateList.concat(this.state.deleteList);
    this.setState({
      isSave: true,
      sequence: null,
      selectRow: null,
      deleteList: [],
      saveList: saveList
    });
    let params = saveList;
    this.props.openCommonCircularDialog();
    this.props.saveTemplateList({
      params, callback: (data) => {
        this.props.requestProcedureTemplateList({
          params:{},
          callback: (templateList) => {
            this.setState({
              isSave: true,
              templateList: templateList.data,
              sequence: null,
              selectRow: null,
              selectObj: null
            });
          }
        });
        let payload = {
          msgCode: data.msgCode,
          showSnackbar: true,
          btnActions: {
          }
        };
        this.props.openCommonMessage(payload);
      }
    });
  };

  handleClickCancel = () => {
    let isChange = this.state.isSave;
    if (!isChange) {
      let payload = {
        msgCode: PROCEDURE_CODE.IS_CANCEL_CHANGE,
        btnActions: {
          btn1Click: () => {
            this.initData();
          }
        }
      };
      this.props.openCommonMessage(payload);
    } else {
      this.initData();
    }
  }

  handleClickAdd = () => {
    let isSave = this.state.isSave;
    this.setState({
      editTemplateList: []
    });
    if (!isSave) {
      this.handleCheckSave();
    } else {
      this.setState({ open: true });
    }
  };

  handleClose = () => {
    this.setState({ open: false });
  }

  handleDialogClose = () => {
    this.handleClose();
  }
  handleClickEdit = () => {
    let isSave = this.state.isSave;
    if (!isSave) {//提示有其他操作判断未保存
      this.handleCheckSave();
    } else {
      if (this.state.selectObj !== null) {
        this.setState({
          editTemplateList: []
        });
        let groupId = this.state.selectObj.diagnosisTemplateGrpId;
        let params = {
          groupId: groupId
        };
        this.props.getEditTemplateList({
          params,
          callback: (editTemplateList) => {
            this.setState({
              editTemplateList: editTemplateList
            });
          }
        });
        this.setState({ open: true });
      } else {
        let payload = { msgCode: PROCEDURE_CODE.IS_TP_SELECTED_EDIT };
        this.props.openCommonMessage(payload);
      }
    }
  }
  //点击删除方法
  handleClickDelete = () => {
    let delSequence = this.state.sequence;  //获得选中行sequence
    if (delSequence === null) {
      let payload = { msgCode: PROCEDURE_CODE.IS_TP_SELECTED_DELETE };
      this.props.openCommonMessage(payload);
    } else {
      let payload = {
        msgCode: PROCEDURE_CODE.CONFIRM_TEMPLATEGROUP_DELETE,
        btnActions: {
          // Yes
          btn1Click: () => {
            this.delete(); //已有选中行，进入删除方法，进行后台数据交互
          },
          btn2Click: () => {
          }
        }
      };
      this.props.openCommonMessage(payload);
    }
  };

  // 删除指定行后台数据交互方法
  delete = () => {
    let delSequence = this.state.sequence;
    let templateList = this.state.templateList;
    let index = this.state.sequence - 1;
    let deleteList = this.state.deleteList;
    templateList[index].delFlag = 'Y';
    deleteList = deleteList.concat(templateList[index]);

    templateList.splice(index, 1);

    templateList = templateList.map((item) => {
      item.sequence = item.sequence > delSequence ? (item.sequence - 1) : (item.sequence);
      return item;
    });  //页面重新更新templateList
    this.setState({
      deleteList: deleteList,
      templateList: templateList, //页面重新更新templateList
      selectObj: null,
      sequence: null,
      selectRow: null,
      isSave: false
    });
  }

  render() {
    const { classes } = this.props;
    const { open, editTemplateList, templateList, sequence } = this.state;
    let editTemplateProps = {
      open,
      handleClose: this.handleClose,
      editTemplateList: editTemplateList,
      refreshData: this.initData,
      templateList: templateList,
      selectGroupSequence: sequence
    };
    const buttonBar={
      isEdit:!this.state.isSave,
      // height:'64px',
      position:'fixed',
      buttons:[{
        title:'Save',
        id:'btn_procedureTemplate_save',
        onClick:() => this.handleClickSave()
      }]
    };
    return (
      <div className={classes.wrapper}>
        <Card className={classes.bigContainer}>
          <CardHeader
              titleTypographyProps={{
              style: {
                fontSize: '1.5rem',
                fontFamily: 'Arial'
              }
            }}
              title={en_US.procedureTemplate.label_title}
          />

          <CardContent>
            <Container buttonBar={buttonBar}>
              <Typography
                  component="div"
                  style={{ marginBottom: 15, marginLeft: 5, marginRight: 5, marginTop: 5 }}
              >
                <ValidatorForm
                    id="bookingCalendarForm"
                    onSubmit={() => { }}
                    ref="form"
                >
                  <Grid container
                      style={{ marginTop: -10, marginLeft: -8 }}
                  >
                    <label className={classes.left_Label}
                        id="procedureTemplate_serviceLable"
                    >Service: {this.state.serviceEngDesc} ({this.state.serviceCd})</label>
                  </Grid>
                </ValidatorForm>
              </Typography>

              <Typography component="div"
                  style={{ marginTop: 0, marginLeft: -7 }}
              >
                <Button id="btn_procedureTemplate_add"
                    onClick={this.handleClickAdd}
                    style={{ textTransform: 'none' }}
                >
                  <AddCircle color="primary" />
                  <span className={classes.font_color}>Add</span>
                </Button>

                <Button id="btn_procedureTemplate_edit"
                    onClick={this.handleClickEdit}
                    style={{ textTransform: 'none' }}
                >
                  <Edit color="primary" />
                  <span className={classes.font_color}>Edit</span>
                </Button>

                <Button id="btn_procedureTemplate_delete"
                    onClick={this.handleClickDelete}
                    style={{ textTransform: 'none' }}
                >
                  <RemoveCircle color="primary" />
                  <span className={classes.font_color}>Delete</span>
                </Button>

                <Button id="btn_procedureTemplate_up"
                    onClick={this.handleClickUp}
                    style={{ textTransform: 'none' }}
                >
                  <ArrowUpwardOutlined color="primary" />
                  <span className={classes.font_color}>Up</span>
                </Button>

                <Button id="btn_procedureTemplate_down"
                    onClick={this.handleClickDown}
                    style={{ textTransform: 'none' }}
                >
                  <ArrowDownward color="primary" />
                  <span className={classes.font_color}>Down</span>
                </Button>
                <EditProcedureTemplate  {...editTemplateProps} />
              </Typography>

              <CIMSTable id="manage_procedure_template_group_table" data={this.state.templateList}
                  getSelectRow={this.getSelectRow}
                  options={this.state.tableOptions}
                  rows={this.state.tableRows}
                  rowsPerPage={this.state.pageNum}
                  tipsListSize={this.state.tipsListSize}
                  selectRow={this.state.selectRow}
                  style={{ marginTop: 20 }}
              />
              <Prompt message={FieldConstant.LEAVE_CONFIRM_PROMPT_CONTENT}
                  when={!this.state.isSave}
              />
            </Container>
          </CardContent>
        </Card>
        <Typography component="div" className={classes.fixedBottom}>
          {/* <Container
              buttonBar={buttonBar}
              alignItems="center"
              container
              justify="flex-end"
          /> */}
        </Typography>
      </div>
    );
  }
}



const mapDispatchToProps = {
  requestProcedureTemplateList,
  saveTemplateList,
  getEditTemplateList,
  openCommonMessage,
  closeCommonMessage,
  getConfig,
  openCommonCircularDialog
};

function mapStateToProps(state) {
  return {
    templateList: state.procedureReducer.templateList,
    saveTemplateList: state.procedureReducer.saveTemplateList
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(style)(ProcedureTemplate));

