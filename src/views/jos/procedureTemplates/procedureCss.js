import { COMMON_STYLE }from '../../../constants/commonStyleConstant';
export const style = {
  root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      height: 25,
      borderRadius: 0
    },
    favorite_category:
    {
      color: 'rgba(0, 0, 0, 0.7)',
      width: 'calc(100%-20px)',
      padding: 5
    },
    left_Label:
    {
      fontSize: '1rem',
      padding: 6,
      fontWeight:600
    },
    font_color: {
      fontSize: '1rem',
      color: '#0579c8'
    },
    table_itself:{
      marginTop:-20
    },
    table_head: {
      height: 50,
      paddingLeft: '10px' ,
      fontStyle: 'normal',
      fontSize: '13px',
      fontWeight: 'bold'
    },
    table_header: {
      fontSize: '12pt',
      fontWeight: 600,
      fontFamily: 'Arial',
      color: 'rgba(0, 0, 0, 0.7)',
      paddingTop:18,
      paddingLeft:8
    },
    button: {
      Float: 'center',
      backgroundColor: '#fff'
    },
    saveAndeClear: {
      right:0 ,
      Float:'right'
    },
    bigContainer: {
      borderRadius: 5,
      marginLeft:20,
      marginRight:36,
      marginTop:8,
      height: 'calc(100% - 10px)',
      overflowY: 'auto'
      // boxShadow:0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)
    },
    topP: {
      marginLeft: 10,
      paddingTop:15
    },
    table_row: {
      height: 31,
      cursor: 'pointer'
    },
    table_row_selected: {
      height: 31,
      cursor: 'pointer',
      backgroundColor: 'cornflowerblue'
    },
    table_cell:{
      paddingLeft: '10px',
        width: 5,
      whiteSpace:'pre-line',
      wordWrap: 'break-word',
      wordBreak: 'break-all'
    },
    cell_text:{
      paddingLeft: '30px',
      width: 20,
      whiteSpace:'pre-line',
      wordWrap: 'break-word',
      wordBreak: 'break-all'
    },
    table_cell_1:{
      width: 5,
      whiteSpace:'pre-line',
      wordWrap: 'break-word',
      wordBreak: 'break-all'
    },
    customRowStyle: {
      height: '38px',
      fontSize:'1rem',
      fontFamily: 'Arial'
    },
    headRowStyle:{
      backgroundColor:COMMON_STYLE.TABLE_BGCKGROUNDCOLOR,
      color:'white'
    },
    headCellStyle:{
      color:'white',
      overflow: 'hidden',
      fontSize:'1rem',
      fontFamily: 'Arial',
      fontWeight: 'bold'
    },
    wrapper: {
      width: 'calc(100% - 22px)',
      height: 'calc(100% - 167px)',
      position: 'fixed'
    },
    fixedBottom: {
      margin:'10px',
      color: '#6e6e6e',
      position:'fixed',
      bottom: 0,
      width: '100%',
      zIndex: 100,
      backgroundColor: '#FFFFFF',
      right: 30
    },
    fontLabel: {
      fontSize: '1rem',
      fontFamily: 'Arial'
    }
};