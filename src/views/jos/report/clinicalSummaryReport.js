/*
 * Front-end UI for medical summary test page
 * Load smokingDropMap、drinkingDropMap、substanceAbuseDropMap dropdwon list: [medicalSummaryTest.js] componentDidMount
 * -> [medicalSummaryAction.js] getMedicalSummaryDropList
 * -> [medicalSummarySaga.js] getMedicalDropList
 * -> [medicalSummaryReducer.js] smokingDropMap、drinkingDropMap、substanceAbuseDropMap
 * -> Backend API = /medical-summary/listCodeMedicalSummaryDrop
 * Load Service dropdown list: [medicalSummaryTest.js] componentDidMount
 * -> [medicalSummaryAction.js] getTempServiceList
 * -> [medicalSummarySaga.js] getTempServiceList
 * -> [medicalSummaryReducer.js] tempServiceList
 * -> Backend API = /common/listCodeList
 * ->  [medicalSummaryReducer.js] tempEncounterList
 * -> Backend API =/clinical-note/clinicalNote/${encounterId}
 * Load the Encounter medical summary exist data: [medicalSummaryTest.js] componentDidMount
 * -> [medicalSummaryAction.js] getMedicalSummaryVal
 * -> [medicalSummarySaga.js] getMedicalSummaryVal
 * ->  [medicalSummaryReducer.js] MEDICAL_SUMMARY_VAL
 * -> Backend API =/medical-summary/listMedicalSummary
 */
import React, { Component } from 'react';
import { withStyles, Card, CardContent, Grid } from '@material-ui/core';
import { connect } from 'react-redux';
import { openErrorMessage } from '../../../store/actions/common/commonAction';
import { getTempServiceList } from '../../../store/actions/medicalSummary/medicalSummaryAction';
import { previewReportClinicalSummary } from '../../../store/actions/report/clinicalSummaryReportAction';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import { isUndefined, find } from 'lodash';
import PreviewPdfDialog from './components/printDialog/PreviewPdfDialog';
import {openCommonCircularDialog,closeCommonCircularDialog} from '../../../store/actions/common/commonAction';
import { openCommonMessage } from '../../../store/actions/message/messageAction';
import { print} from '../../../store/actions/common/commonAction';
import Container from 'components/JContainer';
import * as commonUtils from '../../../utilities/josCommonUtilties';

const styles = () => ({
  root: {
    width: '100%'
  },
  selectLabel: {
    fontSize: '1rem',
    float: 'left',
    paddingTop: '3px',
    paddingRight: '10px',
    fontFamily: 'Arial',
    fontWeight: 'bold'
  },
  fontLabel: {
    fontFamily: 'Arial',
    fontSize: '1rem'
  }
});

class ClinicalSummaryReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      selectedServiceVal: '',
      selectedEncounterVal: '',
      selectedPatientKey: '',
      previewShow:false,
      previewData:'test'
    };
  }

  componentDidMount(){
    this.props.ensureDidMount();
  }

  closePreviewDialog=()=>{
    this.setState({previewShow:false});
  }

  handlePreview=()=>{
    this.props.openCommonCircularDialog();
    let { loginService,encounterData } = this.props;
    let { encounterId,patientKey,encounterDate } = encounterData;

    const params = {
      serviceCd:loginService.serviceCd,
      encounterId,
      patientKey,
      encounterDate,
      patientDto: commonUtils.generatePatientDto()
    };

    this.props.previewReportClinicalSummary({
      params,
      callback:(data) =>{
        this.setState({
          previewData:data,
          previewShow:true
        });
      }
    });
  }

  print=()=>{
    this.props.openCommonCircularDialog();
    this.props.print({
      base64:this.state.previewData,
      callback:(result)=>{
        if(result){
          let payload = {
            msgCode: '101317',
            showSnackbar:true
          };
          this.props.openCommonMessage(payload);
        }
        else{
          let payload = {
            msgCode: '101318'
          };
            this.props.openCommonMessage(payload);
        }
        this.props.closeCommonCircularDialog();
      }
    });
  }

  handleEncounterChange = (obj) => {
    let { encounterList } = this.props;
    let { selectedEncounterVal } = this.state;
    if (selectedEncounterVal !== obj.value) {
      let tempObj = find(encounterList, item => {
        return item.encounterId === obj.value;
      });
      if (!isUndefined(tempObj)) {
        this.setState({
          selectedServiceCd: tempObj.serviceCd,
          selectedEncounterVal: obj.value,
          selectedPatientKey: tempObj.patient.patientKey,
          selectedEncounterDate: tempObj.createdDtm
        });

      }
    }
  }

  render() {
    const { classes} = this.props;
    const buttonBar={
      isEdit:false,
      // height:'64px',
      // position:'fixed',
      buttons:[{
        title:'Preview',
        onClick:this.handlePreview,
        id:'preview_button'
      }]
    };

    return (
      <div className={classes.root}>
        <Card>
          <CardContent>
            <div>
              <ValidatorForm
                  onSubmit={() => { }}
                  ref="testForm"
              >
                <Grid container style={{ marginBottom: '10px' }}>
                  <Grid item xs={12}>
                    <PreviewPdfDialog
                        open
                        id={'previewPdfDialog'}
                        previewShow={this.state.previewShow}
                        previewData={this.state.previewData}
                        closePreviewDialog={this.closePreviewDialog}
                        print={this.print}
                    />
                  </Grid>
                </Grid>
              </ValidatorForm>
            </div>
            {/* test button */}
            <Container  buttonBar={buttonBar}/>
          </CardContent>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loginService: state.login.service,
    service: state.medicalSummary.tempServiceList.service,
    encounterList: state.medicalSummary.tempEncounterList,
    encounterData: state.patient.encounterInfo
  };
};

const mapDispatchToProps = {
  previewReportClinicalSummary,
  getTempServiceList,
  openErrorMessage,
  openCommonCircularDialog,
  closeCommonCircularDialog,
  print,
  openCommonMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ClinicalSummaryReport));
