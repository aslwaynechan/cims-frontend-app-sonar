/*
 * Front-end UI for load&save specimen collection readings
 * Load EncounterId Field Dropdown List and get specimen collection list Action: [specimenCollection.js] componentDidMount
 * -> [medicalSummaryAction.js] [specimenCollectionAction.js] getIoeSpecimenCollectionList
 * -> [medicalSummarySaga.js] [specimenCollectionSaga.js] getIoeSpecimenCollectionList
 * -> Backend API = clinical-note/clinicalNote/${encounterId},ioe/loadIoeRequestRecords
 * Save Action: [specimenCollection.js]  submit,handlePrintLabel,handlePrintOutPutForm
 * -> [specimenCollectionAction.js] saveIoeSpecimenCollectionList
 * -> [specimenCollectionSaga.js] saveIoeSpecimenCollectionList
 * -> Backend API =ioe/operateIoeRequests
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles, Card, CardContent, Typography, CardHeader, Grid } from '@material-ui/core';
import { styles } from './specimenCollectionStyle';
import { getIoeSpecimenCollectionList, saveIoeSpecimenCollectionList } from '../../../../store/actions/IOE/specimenCollection/specimenCollectionAction';
import { openCommonMessage } from '../../../../store/actions/message/messageAction';
import { openCommonCircularDialog } from '../../../../store/actions/common/commonAction';
import { indexOf, toLower, trim, includes } from 'lodash';
import SpecimenCollectionSearchInput from './components/SpecimenCollectionSearchInput';
import JOSTableNoPagination from './components/JOSTableNoPagination';
import moment from 'moment';
// import ValidatorForm from '../../../../components/FormValidator/ValidatorForm';
import { SPECIMEN_COLLECTION_CODE } from '../../../../constants/message/IOECode/specimenCollectionCode';
import Container from 'components/JContainer';
import * as commonUtils from '../../../../utilities/josCommonUtilties';
// import FieldConstant from '../../../../constants/fieldConstant';
// import {Prompt} from 'react-router-dom';
// import CustomizedSelectFieldValidator from '../../../../components/Select/CustomizedSelect/CustomizedSelectFieldValidator';


class SpecimenCollection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRows: null,
      dataList: [],
      searchProcedureRecordList: [],
      clinicEngDesc: JSON.parse(sessionStorage.getItem('clinic')).clinicCd,
      serviceCd: JSON.parse(sessionStorage.getItem('service')).serviceCd,
      pageNum: null,
      tableRows: [
        {
          name: 'requestDatetime', width: '6%', label: 'Requested Date', customBodyRender: (value) => {
            return value ? moment(value).format('DD-MMM-YYYY') : null;
          }
        },
        { name: 'requestUser', width: '6%', label: 'Requested By' },
        { name: 'test', width: '15%', label: 'Test' },
        { name: 'specimen', width: '10%', label: 'Specimen / Tubes' },
        { name: 'outputFormPrinted', width: '5%', label: 'Output Form' },
        { name: 'formName', label: 'Form Name', width: '15%' }
      ],
      tableOptions: {
        rowHover: true,
        rowsPerPage: 5,
        onSelectIdName: 'seq', //显示tips的列
        tipsListName: 'test', //显示tips的list
        tipsDisplayListName: 'codeIoeFormItem', //显示tips的列
        tipsDisplayName: 'frmItemName', //显示tips的值
        bodyCellStyle: this.props.classes.customRowStyle,
        headRowStyle: this.props.classes.headRowStyle,
        headCellStyle: this.props.classes.headCellStyle
      },
      specimenCollectionList: [],
      oldSpecimenCollectionList: [],
      inputVal: '',
      selected: [],
      oldSelected: [],
      oldSelectTimes: 0,
      isSave: true
    };
  }

  componentDidMount() {
    this.props.ensureDidMount();
    const { encounterData } = this.props;
    let params = {
      patientKey: encounterData.patientKey
    };
    this.props.openCommonCircularDialog();
    this.props.getIoeSpecimenCollectionList({
      params,
      callback: (data) => {
        // console.log('data====== %o',data);
        this.setState({
          specimenCollectionList: data,
          oldSpecimenCollectionList: data
        });
      }
    });
    this.setState({
      selectedEncounterVal: encounterData.encounterId,
      selectedPatientKey: encounterData.patientKey
    });
  }

  getSelectRow = (selectedArray) => {
    if (this.state.oldSelectTimes === 1) {
      //control select update
      let oldSelected = this.state.oldSelected;
      let specimenCollectionList = this.state.specimenCollectionList;
      if (selectedArray.length > 0) {
        for (let a = 0; a < specimenCollectionList.length; a++) {
          for (let b = 0; b < selectedArray.length; b++) {
            if (specimenCollectionList[a].ioeRequestId === selectedArray[b]) {
              if (indexOf(this.state.oldSelected, selectedArray[b]) === -1) {
                oldSelected.push(selectedArray[b]);
                break;
              }
            }
            else {
              if (selectedArray.length - 1 === b) {
                if (indexOf(this.state.oldSelected, selectedArray[b]) !== -1) {
                  let index = indexOf(this.state.oldSelected, selectedArray[b]);
                  oldSelected.splice(index, 1);
                }
              }
            }
          }
        }
      }
      else {
        for (let a = 0; a < specimenCollectionList.length; a++) {
          if (indexOf(this.state.oldSelected, specimenCollectionList[a].ioeRequestId) !== -1) {
            let index = indexOf(this.state.oldSelected, specimenCollectionList[a].ioeRequestId);
            oldSelected.splice(index, 1);
          }
        }
      }
      this.setState({
        selected: selectedArray,
        oldSelected: oldSelected,
        isSave: false
      });
    }
    else {
      this.setState({
        selected: selectedArray,
        oldSelected: selectedArray,
        isSave: false
      });
    }
  }

  handleFuzzySearch = (value) => {
    //filter value
    if (trim(value) !== '') {
      let dataList = this.state.oldSpecimenCollectionList;
      let newSelected = [];
      let filterDataList = [];
      let oldSelected = this.state.oldSelected;
      let deleteSelected = [];

      for (let index = 0; index < dataList.length; index++) {
        if (includes(toLower(moment(dataList[index].requestDatetime).format('DD-MMM-YYYY')), toLower(value)) || includes(toLower(dataList[index].test), toLower(value)) || includes(toLower(dataList[index].formName), toLower(value))) {
          filterDataList[filterDataList.length] = dataList[index];
          if (indexOf(this.state.selected, dataList[index].ioeRequestId) !== -1) {
            newSelected.push(dataList[index].ioeRequestId);
          } else {
            deleteSelected.push(dataList[index].ioeRequestId);
          }
        }
      }

      //add
      for (let a = 0; a < newSelected.length; a++) {
        if (indexOf(oldSelected, newSelected[a]) === -1) {
          oldSelected.push(newSelected[a]);
        }
      }
      //delete
      if (oldSelected.length > 0) {
        for (let a = 0; a < deleteSelected.length; a++) {
          if (indexOf(oldSelected, deleteSelected[a]) !== -1) {
            let index = indexOf(oldSelected, deleteSelected[a]);
            oldSelected.splice(index, 1);
          }
        }
      }

      this.setState({
        specimenCollectionList: filterDataList,
        selected: newSelected,
        oldSelected: oldSelected,
        oldSelectTimes: 1
      });
    }
    else {
      this.setState({
        specimenCollectionList: this.state.oldSpecimenCollectionList,
        selected: this.state.oldSelected,
        oldSelectTimes: 0
      });
    }
  }

  handleSubmit = (type) => {
    let selected = this.state.selected;
    if (selected.length > 0) {
      let payload = {
        msgCode: SPECIMEN_COLLECTION_CODE.IS_SUBMIT_SPECIMEN_COLLECTION,
        btnActions: {
          // Yes
          btn1Click: () => {
            if (type === 'Submit') {
              this.submit('S');
            }
            else if (type === 'Submit and Print') {
              this.submit('SPOF');
            }
          }
        }
      };
      this.props.openCommonMessage(payload);
    }
    else {
      let payload = {
        msgCode: SPECIMEN_COLLECTION_CODE.IS_SELECT_SPECIMEN_COLLECTION
      };
      this.props.openCommonMessage(payload);
    }
  }

  submit = (type) => {
    let dataList = this.state.specimenCollectionList;
    let selected = this.state.selected;
    let requestList = [];
    for (let a = 0; a < selected.length; a++) {
      for (let b = 0; b < dataList.length; b++) {
        if (selected[a] === dataList[b].ioeRequestId) {
          requestList[requestList.length] = dataList[b];
          break;
        }
      }
    }
    this.props.openCommonCircularDialog();
    let params = {
      dtos: requestList,
      operationType: type,
      patientDto: commonUtils.generatePatientDto()
    };
    this.props.saveIoeSpecimenCollectionList({
      params,
      callback: (data) => {
        //fresh data
        if (data.respCode !== undefined && data.respCode !== 0) {
          let payload = {
            msgCode: data.msgCode
          };
          // this.freshSpecimenCollection();
          this.props.openCommonMessage(payload);
        }
        else {
          if (params.operationType === 'S') {
            let payload = {
              msgCode: data.msgCode
            };
            if (data.respCode === 0) {
              payload.showSnackbar = true;
              this.freshSpecimenCollection('A');
              this.props.openCommonMessage(payload);
            }
          }
          else {
            if (data) {
              let payload = {
                msgCode: '101317',
                showSnackbar: true
              };
              this.freshSpecimenCollection(params.operationType);
              this.props.openCommonMessage(payload);
            }
            else {
              let payload = {
                msgCode: '101318'
              };
              this.freshSpecimenCollection('A');
              this.props.openCommonMessage(payload);
            }
          }
        }
      }
    });
  }

  handlePrintLabel = () => {
    let selected = this.state.selected;
    if (selected.length < 1) {
      let payload = {
        msgCode: SPECIMEN_COLLECTION_CODE.IS_SELECT_SPECIMEN_COLLECTION
      };
      this.props.openCommonMessage(payload);
    }
    else {
      if (selected.length !== 1) {
        let payload = {
          msgCode: SPECIMEN_COLLECTION_CODE.IS_ONLY_SELECT_ONE_SPECIMEN_COLLECTION
        };
        this.props.openCommonMessage(payload);
      }
      else {
        this.submit('PL');
      }
    }
  }

  handlePrintOutPutForm = () => {
    let selected = this.state.selected;
    if (selected.length < 1) {
      let payload = {
        msgCode: SPECIMEN_COLLECTION_CODE.IS_SELECT_SPECIMEN_COLLECTION
      };
      this.props.openCommonMessage(payload);
    }
    else {
      this.submit('POF');
    }
  }

  freshSpecimenCollection = (type) => {
    let params = { patientKey: this.state.selectedPatientKey };
    this.props.getIoeSpecimenCollectionList({
      params,
      callback: (data) => {
        if (!(type === 'PL' || type === 'POF')) {
          this.setState({
            oldSpecimenCollectionList: data,
            specimenCollectionList: data,
            selected: [],
            oldSelected: [],
            inputVal: '',
            isSave: true
          });
        }
        else {
          this.setState({
            oldSpecimenCollectionList: data,
            specimenCollectionList: data,
            inputVal: '',
            isSave: true
          });
        }
      }
    });
  }

  render() {
    const { classes, loginInfo } = this.props;
    let { service } = loginInfo;
    let { code } = service;
    const buttonBar={
      isEdit:false,
      // height:'64px',
      position:'fixed',
      buttons:[
        {
          title:'Print Label',
          onClick: this.handlePrintLabel,
          id:'printLabel_button'
        },
        {
          title:'Print Output Form',
          onClick:this.handlePrintOutPutForm,
          id:'printOutputForm_button'
        },
        {
          title:'Submit and Print Output Form',
          onClick:() =>this.handleSubmit('Submit and Print'),
          id:'SubmitAndPrintOutputForm_button'
        },
        {
          title:'Submit',
          onClick:() =>this.handleSubmit('Submit'),
          id:'submit_button'
        }
      ]
    };
    return (
      <div>
        <Card className={classes.cardWrapper}>
          {/* Top Title */}

          <CardHeader
              titleTypographyProps={{
              style: {
                fontSize: '1.5rem',
                fontFamily: 'Arial'
              }
            }}
              title={`Specimen Collections (${code})`}
          />
          <CardContent className={classes.cardContent}>
            <Container buttonBar={buttonBar}>
              <Typography
                  className={classes.divFirst}
                  component="div"
              >
                <Grid className={classes.gridContainer}
                    container
                    justify="flex-end"
                >
                  <Grid item
                      xs={9}
                  >
                    <label className={classes.left_Label}
                        id="tokenTenplateForm_clinic_Lable"
                    >Clinic: {this.state.clinicEngDesc}</label>
                  </Grid>

                  <Grid className={classes.gridFuzzy}
                      item
                      xs={3}
                  >
                    <SpecimenCollectionSearchInput
                        dataList={this.state.searchProcedureRecordList}
                        displayField={['termDesc']}
                        handleSearchBoxLoadMoreRows={this.handleProcedureSearchBoxLoadMoreRows}
                        id={'fuzzySearchBoxId'}
                        inputPlaceHolder={'Search by Requested Date /Test / Form Name'}
                        limitValue={4}
                        onChange={this.handleFuzzySearch}
                        value={this.state.inputVal}
                    >
                    </SpecimenCollectionSearchInput>
                  </Grid>
                </Grid>
                <Typography className={classes.JosTable}
                    component="div"
                >
                  <JOSTableNoPagination
                      data={this.state.specimenCollectionList}
                      getSelectRow={this.getSelectRow}
                      options={this.state.tableOptions}
                      rows={this.state.tableRows}
                      rowsPerPage={this.state.pageNum}
                      selected={this.state.selected}
                  />
                </Typography>
                <Typography
                    className={classes.cimsButtonDiv}
                    component="div"
                >

                </Typography>
              </Typography>
            </Container>
          </CardContent>
        </Card>

      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loginInfo: {
      ...state.login.loginInfo,
      service:{
        code:state.login.service.serviceCd
      }
    },
    encounterData: state.patient.encounterInfo,
    patientPanelInfo: state.patient.patientInfo
  };
};

const mapDispatchToProps = {
  openCommonMessage,
  openCommonCircularDialog,
  getIoeSpecimenCollectionList,
  saveIoeSpecimenCollectionList
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(SpecimenCollection));
