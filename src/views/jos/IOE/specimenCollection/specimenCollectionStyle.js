export const styles = () => ({
  cardWrapper: {
    margin: '8px 0 0 20px',
    marginRight: 36,
    borderRadius: 5
  },
  table: {
    width: '100%'
  },
  titleDiv: {
    padding: '16px',
    fontSize: ' 1.5rem',
    fontFamily:'-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif'
  },
  cardContent: {
    // padding: '0px'
  },
  tableWrapper: {
    padding: '0px 16px 0px 16px',
    backgroundColor: 'white'
  },
  left_Label: {
    fontSize: '1rem',
    padding: 6,
    fontWeight:600
  },
  divFirst:{
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5
  },
  gridContainer:{
    marginTop: -10,
    marginLeft:-8
  },
  gridFuzzy:{

  },
  headRowStyle:{
    backgroundColor:'#7BC1D9'
  },
  headCellStyle:{
    fontSize: '1rem',
    fontFamily: 'Arial',
    color:'white',
    overflow: 'hidden',
    fontWeight: 'bold'
  },
  JosTable:{
    marginTop:20,
    marginLeft:-2
  },
  cimsButton:{
    marginRight:10
  },
  cimsButtonDiv:{
    marginTop:24
  },
  selectLabel: {
    fontSize:'1rem',
    float: 'left',
    paddingTop: '3px',
    paddingRight: '10px',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    marginLeft:6
  },
  fontLabel: {
    fontSize: '1rem',
    fontFamily: 'Arial'

  },
  customRowStyle :{
    fontSize: '1rem',
    fontFamily: 'Arial'
  }
});