export const styles = () => ({
  errorHelper: {
    marginTop: 5,
    fontSize: '1rem',
    fontFamily: 'Arial'
  },
  dropdown: {
    fontSize:'1rem',
    width: 620,
    fontFamily: 'Arial'
  }
});
