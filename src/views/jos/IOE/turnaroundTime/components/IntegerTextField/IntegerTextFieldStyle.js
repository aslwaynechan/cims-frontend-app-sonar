export const styles = () => ({
  errorHelper: {
    marginTop: 5,
    fontSize: '1rem',
    fontFamily: 'Arial'
  },
  input: {
    width: 225,
    fontSize: '1rem',
    fontFamily: 'Arial'
  }
});
