/*
 * Front-end UI for save/update IOE turnaround time
 * Load IOE From List Action: [turnaroundTime.js] componentDidMount
 * -> [turnaroundTimeAction.js] getIoeFormDropList
 * -> [turnaroundTimeSaga.js] getIoeFormDropList
 * -> Backend API = /ioe/listCodeIoeForm
 * Load IOE Turnaround Time List Action: [turnaroundTime.js] componentDidMount
 * -> [turnaroundTimeAction.js] getIoeTurnaroundTimeList
 * -> [turnaroundTimeSaga.js] getIoeTurnaroundTimeList
 * -> Backend API = /ioe/listTurnaroundTime
 * Save Action: [turnaroundTime.js] Save -> handleSave
 * -> [turnaroundTimeAction.js] updateIoeTurnaroundTimeList
 * -> [turnaroundTimeSaga.js] updateIoeTurnaroundTimeList
 * -> Backend API = /ioe/saveTurnaroundTime
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { styles } from './turnaroundTimeStyle';
import { withStyles, Card, CardContent, Typography, Button, IconButton } from '@material-ui/core';
import { AddCircle, Clear, Check, EditRounded, DeleteOutline } from '@material-ui/icons';
import MaterialTable, { MTableAction,MTableToolbar } from 'material-table';
import IntegerTextField from './components/IntegerTextField/IntegerTextField';
import { trim,cloneDeep,find,isEqual } from 'lodash';
import DropdownField from './components/DropdownField/DropdownField';
import { getIoeFormDropList, getIoeTurnaroundTimeList, updateIoeTurnaroundTimeList } from '../../../../store/actions/IOE/turnaroundTime/turnaroundTimeAction';
import { openCommonMessage } from '../../../../store/actions/message/messageAction';
import { openCommonCircularDialog } from '../../../../store/actions/common/commonAction';
import { TURNAROUD_TIME_CODE } from '../../../../constants/message/IOECode/turnaroundTimeCode';
import { COMMON_CODE } from '../../../../constants/message/common/commonCode';
import { COMMON_ACTION_TYPE } from '../../../../constants/common/commonConstants';
import { COMMON_STYLE }from '../../../../constants/commonStyleConstant';
import { createMuiTheme,MuiThemeProvider } from '@material-ui/core/styles';
import Container from 'components/JContainer';

const theme = createMuiTheme({
  overrides: {
    MuiTableCell: {
      paddingNone: {
        borderLeft: '1px  solid rgba(224, 224, 224, 1)'
      },
      body:{
        fontSize:'1rem',
        fontFamily:'Arial'
      }
    },
    MuiMenuItem : {
      gutters : {
        fontSize:'1rem',
        fontFamily:'Arial'
      }
    },
    MuiSelect : {
      root : {
        fontSize:'1rem',
        fontFamily:'Arial'
      }
    },
    MuiInput : {
      root : {
        fontSize: '1rem',
        fontFamily:'Arial'
      }
    },
    MuiTableSortLabel:{
      root: {
        fontSize:'1rem',
        fontFamily:'Arial'
      }
    }
  }
});

class TurnaroundTime extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formRequireFlag: false,
      timeRequireFlag: false,
      timeIllegalFlag: false,
      selectedRow: null,
      columns: [
        {
          title: 'Form Name (Form ID)',
          field: 'codeIoeFormId',
          lookup: {},
          cellStyle: {
            fontSize:'1rem',
            borderRight: '1px  solid rgba(224, 224, 224, 1)'
          },
          headerStyle: {
            width: '50%',
            borderRight: '1px  solid rgba(224, 224, 224, 1)'
          },
          editable: 'onAdd',
          render: rowData => {
            let { originDropList } = this.state;
            let dataObj = find(originDropList, obj=>{
              return obj.codeIoeFormId === rowData.codeIoeFormId;
            });
            return (
              <div style={{fontSize:'1rem'}}>{!!dataObj?dataObj.nameAndNum:''}</div>
            );
          },
          editComponent: props => {
            let {formRequireFlag,originDropList,dataList} = this.state;
            let originChange = props.onChange;
            return (
              <DropdownField
                  id="codeIoeFormId"
                  value={props.value}
                  valueChange={(value)=>{this.handleTableCellChange(value,originChange);}}
                  updateState={this.updateState}
                  formRequireFlag={formRequireFlag}
                  originDropList={originDropList}
                  dataList={dataList}
              />
            );
          }
        },
        {
          title: 'Exp. Turnaround Time (Day)',
          field: 'turnaroundTime',
          cellStyle: {
            fontSize:'1rem',
            color:'#000000',
            borderRight: '1px  solid rgba(224, 224, 224, 1)'
          },
          headerStyle: {
            width: '50%',
            color:'white',
            borderRight: '1px solid rgba(224, 224, 224, 1)'
          },
          render: rowData => {
            return (
              <div style={{fontSize:'1rem'}}>{rowData.turnaroundTime}</div>
            );
          },
          editComponent: props => {
            let {timeRequireFlag,timeIllegalFlag} = this.state;
            let originChange = props.onChange;
            return (
              <IntegerTextField
                  id="turnaroundTime"
                  placeholder="Exp. Turnaround Time (Day)"
                  value={props.value}
                  valueChange={(value)=>{this.handleTableCellChange(value,originChange);}}
                  updateState={this.updateState}
                  timeRequireFlag={timeRequireFlag}
                  timeIllegalFlag={timeIllegalFlag}
              />
            );
          }
        }
      ],
      originDropList: [],
      deleteDataList: [],
      dataList:props.turnaroundTimeList||[],
      noDataTip:'There is no data.'
    };
  }

  componentDidMount(){
    this.props.ensureDidMount();
    const {loginService} = this.props;
    let {columns} = this.state;
    this.props.getIoeTurnaroundTimeList({
      params: {
        serviceCd:loginService.serviceCd
      }
    });
    this.props.getIoeFormDropList({
      params:{},
      callback:(data) => {
        if (data.length > 0) {
          let tempObj = {};
          for (let i = 0; i < data.length; i++) {
            const element = data[i];
            tempObj[element.codeIoeFormId] = element.nameAndNum;
          }
          columns[0].lookup = tempObj;
          this.setState({
            columns,
            originDropList:cloneDeep(data)
          });
        }
      }
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps){
    if (!isEqual(this.props.turnaroundTimeList,nextProps.turnaroundTimeList)) {
      this.setState({
        dataList:cloneDeep(nextProps.turnaroundTimeList)
      });
    }
  }

  handleTableCellChange = (value,originChange) => {
    originChange(value);
  }

  updateState = (obj) => {
    this.setState({
      ...obj
    });
  }

  dataRequiredCheck = (data) => {
    let formRequireFlag = false;
    let timeRequireFlag = false;
    let flag = false;
    if (trim(data.turnaroundTime) === '') {
      timeRequireFlag = true;
      flag = true;
    }
    if (data.codeIoeFormId === undefined) {
      formRequireFlag = true;
      flag = true;
    }
    this.setState({
      formRequireFlag,
      timeRequireFlag
    });
    return flag;
  }

  handleActionAdd = (event,tableProps) => {
    let btnNode = document.getElementById('btn_turnaround_time_row_cancel');

    if(!this.state.dataList.length>0){
      this.setState({
        noDataTip: ''
      });
    }
    if (!!btnNode) {
      let payload = {
        msgCode: COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
      };
      this.props.openCommonMessage(payload);
    } else {
      this.setState({
        formRequireFlag: false,
        timeRequireFlag: false,
        timeIllegalFlag: false
      });
      tableProps.action.onClick(event, tableProps.data);
    }
  }

  handleActionSave = (event,tableProps) => {
    this.setState({
      noDataTip:'There is no data.'
    });
    tableProps.action.onClick(event, tableProps.data);
  }

  handleActionCancel = (event,tableProps) => {
    this.setState({
      formRequireFlag: false,
      timeRequireFlag: false,
      timeIllegalFlag: false,
      noDataTip: 'There is no data.'
    });
    tableProps.action.onClick(event, tableProps.data);
  }

  handleRowAdd = (newData) => {
    let { dataList, formRequireFlag, timeRequireFlag, timeIllegalFlag } = this.state;
    return new Promise((resolve, reject) => {
      if (formRequireFlag||timeRequireFlag||timeIllegalFlag||this.dataRequiredCheck(newData)) {
        reject();
      } else {
        dataList.push({
          ioeTurnaroundTimeId: null,
          codeIoeFormId: newData.codeIoeFormId,
          turnaroundTime: newData.turnaroundTime,
          createdBy: null,
          createdDtm: null,
          version: null,
          operationType: COMMON_ACTION_TYPE.INSERT
        });
        this.setState({dataList},()=>resolve());
      }
    });
  }

  handleRowUpdate = (newData, oldData) => {
    let { dataList, timeRequireFlag, timeIllegalFlag } = this.state;
    return new Promise((resolve,reject) => {
      if (timeRequireFlag||timeIllegalFlag) {
        reject();
      } else {
        let index = dataList.indexOf(oldData);
        if (!!newData.version) {
          newData.operationType = COMMON_ACTION_TYPE.UPDATE;
        }
        dataList[index] = newData;
        this.setState({dataList},()=>resolve());
      }
    });
  }

  handleRowDelete = (oldData) => {
    let { dataList,deleteDataList } = this.state;
    return new Promise((resolve) => {
      let index = dataList.indexOf(oldData);
      if (!!oldData.version) {
        oldData.operationType = COMMON_ACTION_TYPE.DELETE;
        deleteDataList.push(oldData);
      }
      dataList.splice(index, 1);
      this.setState({
        dataList,
        deleteDataList
      },()=>resolve());
    });
  }

  generateResultObj = () => {
    const { loginService } = this.props;
    let { dataList,deleteDataList } = this.state;
    let tempDataList = cloneDeep(dataList);
    let tempDeleteDataList = cloneDeep(deleteDataList);
    tempDataList = tempDataList.concat(tempDeleteDataList);
    let resultObj = {
      serviceCd: loginService.serviceCd,
      dtos:[]
    };
    tempDataList.forEach(element => {
      delete element.tableData;
      element.serviceCd = loginService.serviceCd;
      resultObj.dtos.push(element);
    });

    return resultObj;
  }

  handleSave = () => {
    let btnNode = document.getElementById('btn_turnaround_time_row_cancel');
    if (!!btnNode) {
      let payload = {
        msgCode: COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
      };
      this.props.openCommonMessage(payload);
    } else {
      let resultObj = this.generateResultObj();
      if (resultObj.dtos.length === 0) {
        let payload = {
          msgCode: COMMON_CODE.SUBMITTING_DATA_NULL
        };
        this.props.openCommonMessage(payload);
      } else {
        this.props.openCommonCircularDialog();
        this.props.updateIoeTurnaroundTimeList({
          params: resultObj,
          callback: (data)=>{
            this.setState({
              deleteDataList: []
            });
            let payload = {
              msgCode:data.msgCode,
              showSnackbar: true
            };
            this.props.openCommonMessage(payload);
          }
        });
      }
    }
  }

  dataEditStatusCheck = () => {
    const { turnaroundTimeList } = this.props;
    let { dataList } = this.state;
    let tempDataList = cloneDeep(dataList);
    tempDataList.forEach(element => {
      delete element.tableData;
    });
    let btnNode = document.getElementById('btn_turnaround_time_row_cancel');
    if (!isEqual(tempDataList,turnaroundTimeList)||!!btnNode) {
      return true;
    }
    return false;
  }

  handleCancel = () => {
    const { turnaroundTimeList } = this.props;
    let btnNode = document.getElementById('btn_turnaround_time_row_cancel');
    if (this.dataEditStatusCheck()||!!btnNode) {
      let payload = {
        msgCode: TURNAROUD_TIME_CODE.CANCEL_CHANGE,
        btnActions: {
          // Yes
          btn1Click: () => {
            if (!!btnNode) {
              btnNode.click();
            }
            this.setState({
              formRequireFlag: false,
              timeRequireFlag: false,
              timeIllegalFlag: false,
              dataList: cloneDeep(turnaroundTimeList),
              deleteDataList: []
            });
          }
        }
      };
      this.props.openCommonMessage(payload);
    }
  }


  render() {
    const { classes, loginService } = this.props;
    let { columns, dataList, originDropList } = this.state;
    const buttonBar={
      isEdit:this.dataEditStatusCheck,
      position:'fixed',
      buttons:[{
        title:'Save',
        onClick:this.handleSave,
        id:'save_button'
      }]
    };
    return (
      <Container buttonBar={buttonBar}>
      <Typography component="div"
          id="wrapper"
          className={classes.wrapper}
      >
        <Card className={classes.cardWrapper}>
          <div className={classes.titleDiv} >{`Turnaround Time Maintenance (${loginService.serviceCd})`}</div>
          <CardContent style={{paddingBottom: '0px'}} className={classes.cardContent}>
            {/* table */}
            <Typography component="div" className={classes.tableWrapper}>
              {/* MaterialTable */}
              <MuiThemeProvider theme={theme}>
                <MaterialTable
                    className={classes.table}
                    columns={columns}
                    data={dataList}
                    onRowClick={((evt, selectedRow) => this.setState({ selectedRow }))}
                    options={{
                      sorting: false,
                      search: false,
                      paging: false,
                      showTitle: false,
                      actionsColumnIndex: -1,
                      toolbarButtonAlignment: 'left',
                      draggable:false,
                      // maxBodyHeight: 650,
                      // minBodyHeight: 300,
                      headerStyle: {
                        fontSize: '1rem',
                        fontWeight: 'bold',
                        backgroundColor:COMMON_STYLE.TABLE_BGCKGROUNDCOLOR,
                        color:'white',
                        paddingLeft: 8
                      },
                      rowStyle: rowData => ({
                        wordBreak: 'break-all',
                        height: 53,
                        backgroundColor: (this.state.selectedRow && this.state.selectedRow.tableData.id === rowData.tableData.id) ? 'cornflowerblue' : 'white'
                      }),
                      actionsCellStyle: {
                        direction: 'rtl'
                      }
                    }}
                    actions={[  // add buttons to rows or toolbar by using actions prop.
                      {
                        icon: () => <DeleteOutline id="btn_turnaround_time_row_delete" />,
                        onClick: (event, rowData) => {
                          this.handleRowDelete(rowData);
                        }
                      }
                    ]}
                    components={{ //Action Overriding
                      Action: props => {
                        if (props.action.tooltip === 'Add') {
                          return (
                            <Button
                                style={{padding: '0px'}}
                                id="btn_turnaround_time_add"
                                color="primary"
                                disabled={dataList.length === originDropList.length?true:false}
                                onClick={(event) => {this.handleActionAdd(event,props);}}
                            >
                              <AddCircle />
                              <span className={classes.btnSpan}>Add</span>
                            </Button>
                          );
                        } else if (props.action.tooltip === 'Save') {
                          return (
                            <IconButton
                                id="btn_turnaround_time_row_save"
                                className={classes.iconBtn}
                                onClick={(event) => {this.handleActionSave(event,props);}}
                            >
                              <Check />
                            </IconButton>
                          );
                        } else if (props.action.tooltip === 'Cancel') {
                          return (
                            <IconButton
                                id="btn_turnaround_time_row_cancel"
                                className={classes.iconBtn}
                                onClick={(event) => {this.handleActionCancel(event,props);}}
                            >
                              <Clear />
                            </IconButton>
                          );
                        } else {
                          return (
                            <MTableAction {...props} />
                          );
                        }
                      },
                      Toolbar: props => { //Toolbar Overriding
                        return (
                          <div>
                            <MTableToolbar
                                classes={{
                                  'root':classes.toolBar
                                }}
                                {...props}
                            />
                          </div>
                        );
                      },
                      OverlayLoading: () => null
                      // EditRow:props=>{
                      //   return <MTableEditRow {...props}/>;
                      // }
                    }}
                    editable={{ //具有内联编辑功能，允许用户添加、删除和更新新行
                      onRowAdd: newData => {
                        return this.handleRowAdd(newData);
                      },
                      onRowUpdate: (newData, oldData) => {
                        this.setState({isEdit:true});
                        return this.handleRowUpdate(newData, oldData);
                      }
                    }}
                    icons={{
                      Edit:(props) => {
                        return <EditRounded id="btn_turnaround_time_row_edit" {...props} />;
                      }
                    }}
                    localization={{
                      body:{
                        addTooltip: 'Add',
                        emptyDataSourceMessage: this.state.noDataTip,
                        editRow:{
                          deleteText:'Are you sure delete this record?'
                        }
                      }
                    }}
                />
              </MuiThemeProvider>
            </Typography>
          </CardContent>
        </Card>
        {/*<Typography
                    id="btn_group_turnaround_time"
                    component="div"
                    className={classNames(classes.btnDiv,classes.fixedBottom)}
                >
                  <Grid container alignItems="center" justify="flex-end">
                    <Typography component="div" >
                      <CIMSButton
                          classes={{
                            label:classes.fontLabel
                          }}
                          id="btn_turnaround_time_save"
                          color="primary"
                          size="small"
                          onClick={this.handleSave}
                      >
                        Save
                      </CIMSButton>
                      <CIMSButton
                          classes={{
                            label:classes.fontLabel
                          }}
                          id="btn_turnaround_time_refresh"
                          color="primary"
                          size="small"
                          onClick={this.handleCancel}
                      >
                        Cancel
                      </CIMSButton>
                    </Typography>
                  </Grid>
                </Typography>*/}
      </Typography>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    loginService: state.login.service,
    turnaroundTimeList: state.turnaroundTime.turnaroundTimeList
  };
};

const mapDispatchToProps = {
  openCommonMessage,
  openCommonCircularDialog,
  getIoeFormDropList,
  getIoeTurnaroundTimeList,
  updateIoeTurnaroundTimeList
};

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(TurnaroundTime));
