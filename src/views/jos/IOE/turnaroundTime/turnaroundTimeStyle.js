export const styles = () => ({
  wrapper: {
    width: 'calc(100% - 22px)',
    height: 'calc(100% - 167px)',
    position: 'fixed'
  },
  cardWrapper: {
    margin: '8px 0px 0px 20px',
    marginRight: 20,
    height: 'calc(100% - -8px)',
    overflowY: 'auto'
  },
  selectLabel: {
    float: 'left',
    paddingTop: '3px',
    paddingRight: '10px',
    fontFamily: 'Arial',
    fontWeight: 'bold'
  },
  tableWrapper: {
    padding: '0px 16px 0px 16px',
    backgroundColor: 'white'
  },
  topBtnContainer: {
    marginBottom: 10
  },
  btnSpan: {
    fontSize:'1rem',
    textTransform: 'capitalize'
  },
  table: {
    width: '100%'
  },
  titleDiv: {
    padding: '16px',
    fontSize: ' 1.5rem',
    fontFamily:'Arial'
  },
  toolBar: {
    padding: '0px',
    minHeight: 40
  },
  iconBtn: {
    color: 'inherit'
  },
  cardContent: {
    padding: '0px'
  },
  btnAdd: {
    padding: '0xp'
  },
  btnDiv:{
    margin:'10px'
  },
  fixedBottom: {
    color: '#6e6e6e',
    position:'fixed',
    bottom: 0,
    width: '100%',
    zIndex: 100,
    backgroundColor: '#FFFFFF',
    right: 30
  },
  fontLabel: {
    fontSize: '1rem'
  }
});
