export const styles = () => ({
  wrapper: {
    width: 'calc(100% - 22px)',
    height: 'calc(100% - 167px)',
    position: 'fixed'
  },
  cardWrapper: {
    margin: '8px 0px 0px 20px',
    // marginRight: 20,
    height: 'calc(95% - 30px)',
    overflowY: 'auto',
    overflowX: 'auto'
  },
  cardContent: {
    minWidth: 1700,
    padding: 5,
    '&:last-child':{
      paddingBottom: 5
    }
  },
  btnGroup: {
    color: '#6e6e6e',
    position:'fixed',
    bottom: 0,
    width: '100%',
    zIndex: 100,
    backgroundColor: '#FFFFFF',
    right: 30
    // margin: 10
  },
  remark: {
    margin: 0
  },
  itefSign:{
    color: '#0579c8'
  },
  iteoSign:{
    color: '#4caf50'
  }
});