import React, { Component } from 'react';
import {styles} from './TabContainerStyle';
import { withStyles, AppBar, Toolbar, IconButton, Typography, Drawer, Divider, List, ListItem, ListItemIcon, ListItemText, Tooltip, Grid, Fab, Avatar } from '@material-ui/core';
import { Menu, ChevronLeft, ArrowForward } from '@material-ui/icons';
import classNames from 'classnames';
import { cloneDeep } from 'lodash';
import CIMSMultiTabs from '../../../../../../components/Tabs/CIMSMultiTabs';
import CIMSMultiTab from '../../../../../../components/Tabs/CIMSMultiTab';
// import CustomizedSelectFieldValidator from '../../../../../../components/Select/CustomizedSelect/CustomizedSelectFieldValidator';
import * as constants from '../../../../../../constants/IOE/ixRequest/ixRequestConstants';
import * as utils from '../../utils/ixUtils';
import TestContainer from '../TestContainer/TestContainer';
import SpecimenContainer from '../SpecimenContainer/SpecimenContainer';
import SelectFieldValidator from '../../../../../../components/FormValidator/SelectFieldValidator';

class TabContainer extends Component {
  constructor(props){
    super(props);
    this.state = {
      open: true
    };
  }

  handleDrawerClick = labId => {
    let { lab2FormMap,frameworkMap,updateStateWithoutStatus,contentVals } = this.props;
    let defaultFormId = lab2FormMap.get(labId)[0];
    let formObj = frameworkMap.get(labId).formMap.get(defaultFormId);
    let valObj = utils.initMiddlewareObject(formObj);

    updateStateWithoutStatus&&updateStateWithoutStatus({
      orderIsEdit:false,
      selectedOrderKey: null,
      middlewareObject:valObj,
      contentVals:{
        ...contentVals,
        labId,
        selectedSubTabId: defaultFormId,
        infoTargetLabId: labId,
        infoTargetFormId: defaultFormId
      }
    });
  };

  generateDrawerList = () => {
    const { classes,selectedLabId,frameworkMap } = this.props;
    let list = [];
    if (!!frameworkMap.size&&frameworkMap.size>0) {
      for (let labId of frameworkMap.keys()) {
        list.push(
          <Tooltip
              key={labId}
              title={labId}
              classes={{tooltip:classes.tooltip}}
          >
            <ListItem
                button
                onClick={() => {this.handleDrawerClick(labId);}}
                className={classNames({
                  [classes.selectedItem]: labId === selectedLabId
                })}
            >
              <ListItemIcon
                  className={
                    classNames({
                      [classes.marginRightNone]: open
                    })
                  }
              >
                <Avatar className={classes.avatar}>{labId.substring(0,2)}</Avatar>
              </ListItemIcon>
              <ListItemText
                  primary={
                    <Typography className={classes.font} noWrap>{labId}</Typography>
                  }
              />
            </ListItem>
          </Tooltip>
        );
      }
    }
    return list;
  }

  handleDrawerOpen = () => {
    this.setState({
      open: true
    });
  };

  handleDrawerClose = () => {
    this.setState({
      open: false
    });
  };

  handleOrderNumberChange = event => {
    const { handleOrderNumberChange } = this.props;
    handleOrderNumberChange&&handleOrderNumberChange(event);
  }

  changeTabValue = (event, value) => {
    let { frameworkMap,selectedLabId,updateStateWithoutStatus,contentVals } = this.props;
    let formObj = frameworkMap.get(selectedLabId).formMap.get(value);
    let valObj = utils.initMiddlewareObject(formObj);
    updateStateWithoutStatus&&updateStateWithoutStatus({
      orderIsEdit:false,
      selectedOrderKey: null,
      middlewareObject:valObj,
      contentVals:{
        ...contentVals,
        selectedSubTabId:value,
        infoTargetFormId:value
      }
    });
  }

  generateTab = () => {
    const { classes,selectedLabId,selectedSubTabId,frameworkMap } = this.props;
    let tabs = [];
    if (!!selectedLabId&&frameworkMap.size > 0) {
      let formMap = frameworkMap.get(selectedLabId).formMap;
      for (let [formId,formObj] of formMap) {
        tabs.push(
          <CIMSMultiTab
              key={formId}
              id={`ix_request_sub_tab_${formId}`}
              value={formId}
              disableClose
              className={selectedSubTabId === formId ? 'tabSelected' : 'tabNavigation'}
              label={
                <span className={classes.tabSpan}>
                  {formObj.formShortName}
                </span>
              }
          />
        );
      }
    }
    return tabs;
  }

  judgeContainerType = (testFrameworkMap,specimenFrameworkMap) => {
    let types = [];
    if (testFrameworkMap.size > 0) {
      types.push(constants.ITEM_CATEGORY_TYPE.TEST);
    }
    if (specimenFrameworkMap.size > 0) {
      types.push(constants.ITEM_CATEGORY_TYPE.SPECIMEN);
    }
    return types;
  }

  handleAddOrderWithInfo = () => {
    let {
      itemMapping,
      diagnosisErrorFlag,
      basicInfo,
      orderNumber,
      orderIsEdit,
      selectedOrderKey,
      updateStateWithoutStatus,
      updateGroupingContainerState,
      openCommonMessage,
      middlewareObject,
      selectedLabId,
      selectedSubTabId,
      updateState,
      contentVals,
      temporaryStorageMap,
      handleResetOrderNumber
    } = this.props;
    // validate
    diagnosisErrorFlag = basicInfo.infoDiagnosis === ''?true:false;
    if (!diagnosisErrorFlag) {
      let msgCode = utils.handleValidateItems(middlewareObject);
      if (msgCode === '') {
        let displayDialogFlag = utils.handleOtherInfoDialogDisplay(middlewareObject,itemMapping);
        if (displayDialogFlag) {
          //backup question
          middlewareObject.backupQuestionValMap = cloneDeep(middlewareObject.questionValMap);
          if (!orderIsEdit) {
            utils.resetQuestionStatus(middlewareObject.questionValMap);
          }
          utils.resetQuestionGroupStatus(middlewareObject.questionGroupMap);
          updateState&&updateState({
            middlewareObject,
            contentVals:{
              ...contentVals,
              infoTargetLabId: selectedLabId,
              infoTargetFormId: selectedSubTabId
            }
          });
          // has question
          updateGroupingContainerState&&updateGroupingContainerState({
            infoIsOpen: true
          });
        } else {
          // no question
          if (orderIsEdit) {
            // Edit
            let obj = utils.initTemporaryStorageObj(middlewareObject,basicInfo,selectedLabId);
            let storageObj = temporaryStorageMap.get(selectedOrderKey);
            utils.transformStorageInfo(obj,storageObj);
            temporaryStorageMap.set(selectedOrderKey,obj);
          } else {
            // Add
            for (let i = 0; i < orderNumber; i++) {
              let obj = utils.initTemporaryStorageObj(middlewareObject,basicInfo,selectedLabId);
              let timestamp = new Date().valueOf();
              temporaryStorageMap.set(`${selectedSubTabId}_${timestamp}_${i}`,obj);
            }
          }
          updateState&&updateState({
            selectedOrderKey:null,
            orderIsEdit:false,
            temporaryStorageMap
          });
          handleResetOrderNumber&&handleResetOrderNumber();
        }
      } else {
        openCommonMessage&&openCommonMessage({msgCode});
      }
    }
    updateStateWithoutStatus&&updateStateWithoutStatus({
      diagnosisErrorFlag
    });
  }

  render() {
    const {
      classes,
      frameworkMap,
      selectedLabId,
      selectedSubTabId,
      dropdownMap,
      middlewareObject,
      updateState,
      orderIsEdit,
      orderNumber
    } = this.props;
    let { open } = this.state;

    let testFrameworkMap = !!frameworkMap&&frameworkMap.size>0&&!!selectedLabId?frameworkMap.get(selectedLabId).formMap.get(selectedSubTabId).testItemsMap:new Map();
    let specimenFrameworkMap = !!frameworkMap&&frameworkMap.size>0&&!!selectedLabId?frameworkMap.get(selectedLabId).formMap.get(selectedSubTabId).specimenItemsMap:new Map();
    let types = this.judgeContainerType(testFrameworkMap,specimenFrameworkMap);

    let testContainerProps = {
      selectedLabId,
      selectedFormId:selectedSubTabId,
      dropdownMap,
      testFrameworkMap,
      middlewareObject,
      updateState
    };
    let specimenContainerProps = {
      selectedLabId,
      selectedFormId:selectedSubTabId,
      dropdownMap,
      middlewareObject,
      specimenFrameworkMap,
      updateState
    };

    return (
      <div className={classes.wrapper}>
        <AppBar
            className={classNames(classes.appBar, {
              [classes.appBarShift]: open
            })}
            position="relative"
        >
          <Toolbar
              disableGutters={!open}
              classes={{
                regular:classes.toolBar
              }}
          >
            <IconButton
                aria-label="Open drawer"
                className={classNames(classes.menuButton, {
                  [classes.hide]: open
                })}
                color="inherit"
                onClick={this.handleDrawerOpen}
            >
              <Menu />
            </IconButton>
            <Typography
                className={classNames(classes.font,classes.fontBold)}
                variant="h6"
                color="inherit"
                noWrap
            >
              {selectedLabId}
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer
            classes={{
              root: classes.drawerRoot,
              paper: classNames(classes.drawerPaperRoot, {
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open
              })
            }}
            className={classNames(classes.drawer, {
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open
            })}
            open={open}
            variant="permanent"
        >
          <div>
            <IconButton onClick={this.handleDrawerClose}>
              <ChevronLeft />
            </IconButton>
          </div>
          <Divider />
          <List className={classes.listRoot}>
            {this.generateDrawerList()}
          </List>
        </Drawer>
        {/* list content */}
        <div
            className={classNames(classes.content, {
              [classes.contentOpen]: open
            })}
        >
          {/* sub tab (form name or template name) */}
          <CIMSMultiTabs
              value={selectedSubTabId}
              onChange={this.changeTabValue}
              indicatorColor="primary"
          >
            {this.generateTab()}
          </CIMSMultiTabs>
          {/* content */}
          <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start"
          >
            {
              types.length>1?(
                [
                  (
                    <Grid item xs={6} key={`${selectedLabId}_${selectedSubTabId}_specimen`} classes={{'grid-xs-6':classes.specimenWrapper}}>
                      <SpecimenContainer {...specimenContainerProps} />
                    </Grid>
                  ),
                  (
                    <Grid item xs={6} key={`${selectedLabId}_${selectedSubTabId}_test`} classes={{'grid-xs-6':classes.testWrapper}}>
                      <TestContainer {...testContainerProps} />
                    </Grid>
                  )
                ]
              ):(
                types.length === 1?(
                  types[0] === constants.ITEM_CATEGORY_TYPE.TEST?(
                    <Grid item xs={12} classes={{'grid-xs-12':classes.fullWrapper}}>
                      <TestContainer {...testContainerProps} displayHeader />
                    </Grid>
                  ):(
                    types[0] === constants.ITEM_CATEGORY_TYPE.SPECIMEN?(
                      <Grid item xs={12} classes={{'grid-xs-12':classes.fullWrapper}}>
                        <SpecimenContainer {...specimenContainerProps} displayHeader />
                      </Grid>
                    ):null
                  )
                ):null
              )
            }
          </Grid>
        </div>
        {/* action */}
        <div className={classes.actionWrapper}>
          <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
              className={classes.fabGird}
          >
            <Grid item style={{width:82,marginBottom:20}}>
              <SelectFieldValidator
                  id="ix_request_order_number_dropdown"
                  options={constants.ORDER_NUMBER_OPTIONS.map(option=>{
                    return {
                      label: option.label,
                      value: option.value
                    };
                  })}
                  isDisabled={orderIsEdit}
                  value={orderNumber}
                  onChange={event => {this.handleOrderNumberChange(event);}}
              />
              {/* <CustomizedSelectFieldValidator
                  id="ix_request_order_number_dropdown"
                  options={constants.ORDER_NUMBER_OPTIONS.map(option=>{
                    return {
                      label: option.label,
                      value: option.value
                    };
                  })}
                  isDisabled={orderIsEdit}
                  value={orderNumber}
                  onChange={event => {this.handleOrderNumberChange(event);}}
                  styles={{ menuPortal: base => ({ ...base, zIndex: 1600 }) }}
                  menuPortalTarget={document.body}
              /> */}
            </Grid>
            <Grid item>
              <Tooltip title="Add Order" classes={{tooltip:classes.tooltip}}>
                <Fab
                    size="small"
                    color="primary"
                    aria-label="Add Order"
                    id="btn_ix_request_add_order"
                    className={classes.fab}
                    onClick={()=>{this.handleAddOrderWithInfo();}}
                >
                  <ArrowForward />
                </Fab>
              </Tooltip>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(TabContainer);
