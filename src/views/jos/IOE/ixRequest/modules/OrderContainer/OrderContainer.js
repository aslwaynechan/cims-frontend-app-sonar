import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './OrderContainerStyle';
import { Typography, Card, CardContent, Grid, Fab, Tooltip } from '@material-ui/core';
import { Delete, PriorityHighRounded, Edit } from '@material-ui/icons';
import { isNull,cloneDeep,find } from 'lodash';
import * as utils from '../../utils/ixUtils';
import * as constants from '../../../../../../constants/IOE/ixRequest/ixRequestConstants';
import { COMMON_ACTION_TYPE } from '../../../../../../constants/common/commonConstants';

class OrderContainer extends Component {
  constructor(props){
    super(props);
  }

  generateOrderItems = targetObj => {
    const { classes } = this.props;
    let items = [],displayVals = [],displayTests = [];

    //handle specimen
    let displaySpecimen = '';
    if (targetObj.specimenItemsMap.size>0) {
      for (let valObj of targetObj.specimenItemsMap.values()) {
        if (valObj.operationType !== COMMON_ACTION_TYPE.DELETE) {
          displaySpecimen = this.generateItemDisplayName(valObj);
        }
      }
    }

    //handle test
    if (targetObj.testItemsMap.size > 0) {
      for (let valObj of targetObj.testItemsMap.values()) {
        if (valObj.operationType !== COMMON_ACTION_TYPE.DELETE) {
          let displayVal = '';
          let displayTest = this.generateItemDisplayName(valObj);
          if (displaySpecimen!=='') {
            displayVal = `${displaySpecimen}, ${displayTest}`;
          } else {
            displayVal = `${displayTest}`;
          }
          displayTests.push(displayVal);
        }
      }
    }

    if (displayTests.length>0) {
      displayVals = cloneDeep(displayTests);
    } else {
      displayVals.push(displaySpecimen);
    }

    displayVals.forEach(item => {
      items.push(
        <Tooltip key={`${Math.random()}_order_item`} title={item} classes={{tooltip:classes.tooltip}}>
          <Typography component="div" variant="caption" className={classes.itemTypography} noWrap>
            <span>{item}</span>
          </Typography>
        </Tooltip>
      );
    });

    return (
      <div>{items}</div>
    );
  }

  generateItemDisplayName = valObj => {
    const { dropdownMap } = this.props;
    let displayVal = valObj.itemName||'';
    let val1 = valObj.itemVal,
        val2 = valObj.itemVal2;
    if (valObj.frmItemTypeCd === constants.FORM_ITEM_TYPE.DROP_DOWN_LIST) {
      val1 = utils.generateDropdownValue(dropdownMap,valObj,constants.ITEM_VALUE.TYPE1);
    }
    if (valObj.frmItemTypeCd2 === constants.FORM_ITEM_TYPE.DROP_DOWN_LIST) {
      val2 = utils.generateDropdownValue(dropdownMap,valObj,constants.ITEM_VALUE.TYPE2);
    }
    if (!!val1&&!!val2) {
      displayVal = displayVal!==''?`${displayVal}: ${val1},${val2}`:`${val1},${val2}`;
    } else if (!isNull(val1)&&val1!=='') {
      displayVal = displayVal!==''?`${displayVal}: ${val1}`:`${val1}`;
    } else if (!isNull(val2)&&val2!=='') {
      displayVal = displayVal!==''?`${displayVal}: ${val2}`:`${val2}`;
    }
    return displayVal;
  }

  generateBasicInfoItemValContent = (valObj,labelName) => {
    const { clinicList } = this.props;
    let displayVal = '';
    if (valObj.frmItemTypeCd === constants.FORM_ITEM_TYPE.CLICK_BOX) {
      displayVal = `${valObj.itemName}`;
    } else if (valObj.frmItemTypeCd === constants.FORM_ITEM_TYPE.DROP_DOWN_LIST) {
      let targetOption = find(clinicList,option=>{
        return option.clinicCd === valObj.itemVal;
      });
      if (!!targetOption) {
        displayVal = `${labelName}: ${targetOption.clinicName}`;
      }
    } else if (valObj.frmItemTypeCd === constants.FORM_ITEM_TYPE.INPUT_BOX) {
      displayVal = `${labelName}: ${valObj.itemVal}`;
    }
    return displayVal;
  }

  generateBasicInfoTooltip = valMap => {
    const { classes,basicInfo,clinicList } = this.props;
    let element = '';
    if (valMap.size>0) {
      let requestingUnitDisplayName = '';
      let tempClinic = find(clinicList,clinic=>{
        return basicInfo.requestingUnit === clinic.clinicCd;
      });
      if (!!tempClinic) {
        requestingUnitDisplayName = tempClinic.clinicName;
      }
      let items = [(
        <Typography key={`basic_info_item_request_by_${Math.random()}`} component="div" variant="caption">
          <span className={classes.tooltipItemSpan}>{`Requested By: ${basicInfo.requestedBy}`}</span>
        </Typography>
      ),(
        <Typography key={`basic_info_item_request_by_${Math.random()}`} component="div" variant="caption">
          <span className={classes.tooltipItemSpan}>{`Requested Unit: ${requestingUnitDisplayName}`}</span>
        </Typography>
      )];
      for (let [itemId, valObj] of valMap) {
        let displayVal = '';
        if (valObj.itemIoeType === constants.OTHER_ITEM_FIELD_IOE_TYPE.Urgent && (valObj.operationType === COMMON_ACTION_TYPE.INSERT||valObj.operationType === COMMON_ACTION_TYPE.UPDATE)) {
          displayVal = this.generateBasicInfoItemValContent(valObj);
        } else if (!isNull(valObj.itemVal)&&valObj.itemVal!=='') {
          let labelName = '';
          if (valObj.itemIoeType === constants.OTHER_ITEM_FIELD_IOE_TYPE.RefNo) {
            labelName = 'Clinic Ref. No.';
          } else if (valObj.itemIoeType === constants.OTHER_ITEM_FIELD_IOE_TYPE.Diagnosis) {
            labelName = 'Clinical Summary & Diagnosis';
          } else if (valObj.itemIoeType === constants.OTHER_ITEM_FIELD_IOE_TYPE.ReportTo) {
            labelName = 'Report To';
          } else if (valObj.itemIoeType === constants.OTHER_ITEM_FIELD_IOE_TYPE.Remark&&valObj.itemName === constants.REMARK_ITEM_NAME) {
            labelName = 'Reamrk';
          } else if (valObj.itemIoeType === constants.OTHER_ITEM_FIELD_IOE_TYPE.Instruction&&valObj.itemName === constants.INSTRUCTION_ITEM_NAME) {
            labelName = 'Instruction';
          }
          displayVal = this.generateBasicInfoItemValContent(valObj,labelName);
        }
        items.push(
          <Typography key={`${itemId}_info_item`} component="div" variant="caption">
            <span className={classes.tooltipItemSpan}>{displayVal}</span>
          </Typography>
        );
      }
      if (items.length>0) {
        element = (
          <div>
            {items}
          </div>
        );
      }
    }
    return element;
  }

  generateQuestionItemValContent = (valObj) => {
    const { dropdownMap } = this.props;
    let displayVal = '';
    if (valObj.frmItemTypeCd === constants.FORM_ITEM_TYPE.CLICK_BOX) {
      displayVal = `- ${valObj.itemName}`;
    } else if (valObj.frmItemTypeCd === constants.FORM_ITEM_TYPE.DROP_DOWN_LIST) {
      let options = dropdownMap.get(valObj.codeIoeFormItemId).get(constants.ITEM_VALUE.TYPE1);
      let targetOption = find(options,option=>{
        return option.codeIoeFormItemDropId === valObj.itemVal;
      });
      displayVal = !!valObj.itemName?`- ${valObj.itemName}: ${targetOption.value}`:`- ${targetOption.value}`;
    } else if (valObj.frmItemTypeCd === constants.FORM_ITEM_TYPE.INPUT_BOX) {
      displayVal = !!valObj.itemName?`- ${valObj.itemName}: ${valObj.itemVal}`:`- ${valObj.itemVal}`;
    }
    return displayVal;
  }

  generateQuestionTooltip = (valMap,orderKey) => {
    const { classes,frameworkMap,temporaryStorageMap } = this.props;
    let element = '';
    if (valMap.size>0) {
      let valObj = temporaryStorageMap.get(orderKey);
      let { labId, codeIoeFormId } = valObj;
      let {questionItemsMap} = frameworkMap.get(labId).formMap.get(codeIoeFormId);
      let items = [];
      for (let [groupName, frameworkObjs] of questionItemsMap) {
        let contents = [(
          <Typography key={`${groupName}_info_item`} component="div" variant="caption">
            <span className={classes.tooltipItemSpan}>{groupName}</span>
          </Typography>
        )];
        frameworkObjs.forEach(frameworkObj => {
          let tempValObj = valMap.has(frameworkObj.codeIoeFormItemId)?valMap.get(frameworkObj.codeIoeFormItemId):null;
          if (!!tempValObj) {
            let displayVal = this.generateQuestionItemValContent(tempValObj);
            contents.push(
              <Typography key={`${frameworkObj.codeIoeFormItemId}_info_item`} component="div" variant="caption">
                <span className={classes.tooltipItemSpan}>{displayVal}</span>
              </Typography>
            );
          }
        });
        if (contents.length > 1) {
          items = items.concat(contents);
        }
      }
      if (items.length>0) {
        element = (
          <div>
            {items}
          </div>
        );
      }
    }
    return element;
  }

  handleDeleteOrder = orderKey => {
    const { deletedStorageMap,temporaryStorageMap,updateState } = this.props;
    if (temporaryStorageMap.has(orderKey)) {
      let tempValObj = cloneDeep(temporaryStorageMap.get(orderKey));
      tempValObj = utils.handledeletedStorageObj(tempValObj);
      deletedStorageMap.set(orderKey,tempValObj);
      temporaryStorageMap.delete(orderKey);
      updateState&&updateState({
        orderIsEdit:false,
        selectedOrderKey: null,
        temporaryStorageMap,
        deletedStorageMap
      });
    }
  }

  handleEditOrder = orderKey => {
    const { frameworkMap,temporaryStorageMap,updateState,basicInfo,updateGroupingContainerState } = this.props;
    if (temporaryStorageMap.has(orderKey)) {
      let valObj = temporaryStorageMap.get(orderKey);
      let targetFormId = valObj.codeIoeFormId;
      let targetLabId = valObj.labId;
      let formObj = frameworkMap.get(targetLabId).formMap.get(targetFormId);
      let middlewareObject = utils.initMiddlewareObject(formObj,valObj);
      middlewareObject.backupQuestionValMap = cloneDeep(middlewareObject.questionValMap);
      let tempInfoObj = utils.getBasicInfo(middlewareObject.otherValMap);
      updateState&&updateState({
        orderIsEdit:true,
        selectedOrderKey: orderKey,
        middlewareObject,
        contentVals: {
          labId: targetLabId,
          selectedSubTabId: targetFormId,
          infoTargetLabId: targetLabId,
          infoTargetFormId: targetFormId
        },
        basicInfo: {
          ...basicInfo,
          ...tempInfoObj,
          orderType: constants.NORMAL_TOP_TABS[0].value,
          infoOrderType: constants.NORMAL_TOP_TABS[0].value
        }
      });
      updateGroupingContainerState&&updateGroupingContainerState({
        tabValue: constants.NORMAL_TOP_TABS[0].value
      });
    }
  }

  handleEditQuestion = orderKey => {
    const { frameworkMap,temporaryStorageMap,updateState,updateGroupingContainerState,contentVals,basicInfo } = this.props;
    if (temporaryStorageMap.has(orderKey)) {
      let valObj = temporaryStorageMap.get(orderKey);
      let targetFormId = valObj.codeIoeFormId;
      let targetLabId = valObj.labId;
      let formObj = frameworkMap.get(targetLabId).formMap.get(targetFormId);
      let middlewareObject = utils.initMiddlewareObject(formObj,valObj);
      middlewareObject.backupQuestionValMap = cloneDeep(middlewareObject.questionValMap);
      updateState&&updateState({
        orderIsEdit:true,
        selectedOrderKey: orderKey,
        middlewareObject,
        basicInfo:{
          ...basicInfo,
          infoOrderType:constants.NORMAL_TOP_TABS[0].value
        },
        contentVals:{
          ...contentVals,
          infoTargetLabId:targetLabId,
          infoTargetFormId:targetFormId
        }
      });
      updateGroupingContainerState&&updateGroupingContainerState({
        infoIsOpen: true
      });
    }
  }

  generateOrderTitle = valObj => {
    const { classes } = this.props;
    let { labId, formShortName } = valObj;
    let element = (
      <Typography component="div" noWrap>
        <span className={classes.orderTitle}>{`${labId}, ${formShortName}`}</span>
      </Typography>
    );
    return element;
  }

  generateOrders = () => {
    const { classes,temporaryStorageMap,basicInfo } = this.props;
    let orderCards = [];
    let {codeIoeRequestTypeCd} = basicInfo;
    if (temporaryStorageMap.size>0) {
      for (let [orderKey, valObj] of temporaryStorageMap) {
        let { labId, formShortName } = valObj;
        let content = this.generateOrderItems(valObj);
        let questionTitle = this.generateQuestionTooltip(valObj.questionItemsMap,orderKey);
        let basicInfoTitle = this.generateBasicInfoTooltip(valObj.otherItemsMap);
        orderCards.push(
          <Card key={`${orderKey}_${valObj.codeIoeFormId}`} className={classes.card}>
            <Tooltip title={`${labId}, ${formShortName}`} classes={{tooltip:classes.tooltip}}>
              <Typography component="div" noWrap className={classes.cardHeader}>
                <span className={classes.orderTitle}>{`${labId}, ${formShortName}`}</span>
              </Typography>
            </Tooltip>
            <CardContent classes={{root:classes.cardContent}}>
              <Grid container>
                <Grid item xs={12}>
                  <div>{content}</div>
                </Grid>
                <Grid
                    item
                    xs={12}
                    container
                    direction="row"
                    justify="flex-end"
                    alignItems="center"
                    spacing={1}
                >
                  {!!questionTitle?(
                    <Grid item>
                      <Tooltip title={questionTitle} classes={{tooltip:classes.tooltip}}>
                        <Fab
                            size="small"
                            color="primary"
                            aria-label="Question"
                            id={`btn_ix_request_question_${orderKey}`}
                            className={classes.primaryFab}
                            onClick={()=>{this.handleEditQuestion(orderKey);}}
                        >
                          <PriorityHighRounded style={{transform:'rotate(180deg)'}} />
                        </Fab>
                      </Tooltip>
                    </Grid>
                  ):null}
                  <Grid item>
                    <Tooltip title={basicInfoTitle} classes={{tooltip:classes.tooltip}}>
                      <div>
                        <Fab
                            size="small"
                            color="primary"
                            aria-label="Edit Order"
                            id={`btn_ix_request_edit_order_${orderKey}`}
                            className={classes.primaryFab}
                            disabled={codeIoeRequestTypeCd === constants.IOE_REQUEST_TYPE.NURSE?true:false}
                            onClick={()=>{this.handleEditOrder(orderKey);}}
                        >
                          <Edit />
                        </Fab>
                      </div>
                    </Tooltip>
                  </Grid>
                  <Grid item className={classes.lastGridItem}>
                    <Tooltip title="Delete Order" classes={{tooltip:classes.tooltip}}>
                      <Fab
                          size="small"
                          color="secondary"
                          aria-label="Delete Order"
                          id={`btn_ix_request_delete_order_${orderKey}`}
                          className={classes.deleteFab}
                          onClick={()=>{this.handleDeleteOrder(orderKey);}}
                      >
                        <Delete />
                      </Fab>
                    </Tooltip>
                  </Grid>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        );
      }
    }
    return orderCards;
  }

  render() {
    const { classes,temporaryStorageMap } = this.props;

    return (
      <div>
        <Typography
            component="div"
            variant="h6"
            classes={{h6: classes.title}}
            className={classes.header}
        >
          Added Order(s), Total: {temporaryStorageMap.size>0?temporaryStorageMap.size:0}
        </Typography>
        <div className={classes.orderWrapper}>
          {this.generateOrders()}
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(OrderContainer);
