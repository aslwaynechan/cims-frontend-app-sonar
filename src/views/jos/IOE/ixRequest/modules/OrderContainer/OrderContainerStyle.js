export const styles = () => ({
  title: {
    fontSize: '1rem',
    fontFamily: 'Arial',
    fontWeight: 'bold'
  },
  orderWrapper: {
    padding: 10,
    overflowY: 'auto',
    height: 508
  },
  itemTypography: {
    fontSize: '1rem',
    fontFamily: 'Arial'
  },
  tooltipItemSpan: {
    fontSize: '14px',
    fontFamily: 'Arial'
  },
  tooltip: {
    backgroundColor: '#6E6E6E',
    fontSize: '14px',
    fontFamily: 'Arial'
  },
  primaryFab: {
    '&:hover': {
      backgroundColor: '#0098FF'
    }
  },
  deleteFab: {
    backgroundColor: '#FD0000',
    '&:hover': {
      backgroundColor: '#FD0000'
    }
  },
  card: {
    marginBottom: 10,
    boxShadow: 'none',
    border: '1px solid #0579C8'
  },
  cardHeader: {
    padding: '0 10px',
    paddingLeft: 5,
    backgroundColor: '#0579C8'
  },
  cardContent: {
    padding: '5px 10px',
    paddingLeft: 5,
    '&:last-child':{
      paddingBottom: 5
    }
  },
  header: {
    marginTop: 12,
    paddingLeft: 10
  },
  itemWrapper: {
    minHeight: 65
  },
  lastGridItem: {
    marginRight: -10
  },
  orderTitle: {
    fontWeight: 'bold',
    color: '#ffffff'
  }
});
