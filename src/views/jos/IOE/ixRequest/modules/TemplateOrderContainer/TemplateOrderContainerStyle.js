export const styles = () => ({
  wrapper: {
    width: '100%',
    height: 400,
    overflowY: 'hidden',
    paddingRight: 5
  },
  wrapperHidden: {
    height: 435
  },
  cardContainer: {
    columnCount: '3',
    columnGap: 0,
    height: 384,
    paddingTop: 10
  },
  cardContainerHidden: {
    height: 410
  },
  card: {
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
    breakInside: 'avoid',
    pageBreakInside: 'avoid',
    boxShadow: 'none',
    border: '1px solid #0579C8',
    height: 'max-content'
  },
  cardContent: {
    padding: '5px 10px',
    '&:last-child':{
      paddingBottom: '5px'
    }
  },
  groupNameTitle: {
    fontSize: '1.125rem',
    fontWeight: 'bold'
  },
  itemTypography: {
    fontSize: '1rem',
    fontFamily: 'Arial'
  },
  selectAllBar: {
    paddingTop: 5,
    paddingLeft: 13
  },
  hidden: {
    display: 'none'
  },
  formGroupRow: {
    width: '100%'
  },
  formControlRoot: {
    width: '100%'
  },
  formControlLabel: {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    width: '100%'
  },
  rootCheckbox: {
    padding: '0 5px'
  }
});
