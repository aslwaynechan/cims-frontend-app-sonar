import React, { Component } from 'react';
import { styles } from './TemplateOrderContainerStyle';
import { withStyles } from '@material-ui/core/styles';
import { Card, CardContent, Typography, FormGroup, FormControlLabel, Tooltip } from '@material-ui/core';
import * as constants from '../../../../../../constants/IOE/ixRequest/ixRequestConstants';
import classNames from 'classnames';
import {isNull,cloneDeep} from 'lodash';
import TemplateClickBoxField from '../../components/TemplateClickBoxField/TemplateClickBoxField';
import * as utils from '../../utils/ixUtils';

class TemplateOrderContainer extends Component {

  generateItemDisplayName = valObj => {
    const { dropdownMap } = this.props;
    let displayVal = valObj.itemName||'';
    let val1 = valObj.itemVal,
        val2 = valObj.itemVal2;
    if (valObj.frmItemTypeCd === constants.FORM_ITEM_TYPE.DROP_DOWN_LIST) {
      val1 = utils.generateDropdownValue(dropdownMap,valObj,constants.ITEM_VALUE.TYPE1);
    }
    if (valObj.frmItemTypeCd2 === constants.FORM_ITEM_TYPE.DROP_DOWN_LIST) {
      val2 = utils.generateDropdownValue(dropdownMap,valObj,constants.ITEM_VALUE.TYPE2);
    }
    if (!!val1&&!!val2) {
      displayVal = displayVal!==''?`${displayVal}: ${val1},${val2}`:`${val1},${val2}`;
    } else if (!isNull(val1)&&val1!=='') {
      displayVal = displayVal!==''?`${displayVal}: ${val1}`:`${val1}`;
    } else if (!isNull(val2)&&val2!=='') {
      displayVal = displayVal!==''?`${displayVal}: ${val2}`:`${val2}`;
    }
    return displayVal;
  }

  generateOrderItems = (orderKey,orderObj) => {
    let { updateState,classes,middlewareMapObj } = this.props;
    let { testItemsMap,specimenItemsMap } = orderObj;
    let itemElements = [],displayVals = [],displayTests = [],testIds=[];
    //handle specimen
    let displaySpecimen = '', specimentId = null;
    if (specimenItemsMap.size>0) {
      for (let valObj of specimenItemsMap.values()) {
        displaySpecimen = this.generateItemDisplayName(valObj);
        specimentId = valObj.codeIoeFormItemId;
      }
    }

    //handle test
    if (testItemsMap.size > 0) {
      for (let valObj of testItemsMap.values()) {
        let displayVal = '';
        let displayTest = this.generateItemDisplayName(valObj);
        if (displaySpecimen!=='') {
          displayVal = `${displaySpecimen}, ${displayTest}`;
        } else {
          displayVal = `${displayTest}`;
        }
        displayTests.push(displayVal);
        testIds.push(valObj.codeIoeFormItemId);
      }
    }

    if (displayTests.length>0) {
      displayVals = cloneDeep(displayTests);
    } else {
      displayVals.push(displaySpecimen);
    }
    if (displayVals.length>0) {
      displayVals.forEach((item,index) => {
        let testId = testIds[index]||null;
        let cbFieldProps = {
          id:`${specimentId}_${testId}`,
          specimentId,
          testId,
          middlewareMapObj,
          orderKey,
          level:constants.IX_REQUEST_TEMPLATE_CB.LEVEL_3,
          updateState
        };

        itemElements.push(
          <FormControlLabel
              key={`${Math.random()}_order_item`}
              classes={{
                root: classes.formControlRoot,
                label: classes.formControlLabel
              }}
              control={
                <TemplateClickBoxField {...cbFieldProps}/>
              }
              label={
                <Tooltip title={item} classes={{tooltip:classes.tooltip}}>
                  <Typography component="div" noWrap className={classes.itemTypography}>
                    <span>{item}</span>
                  </Typography>
                </Tooltip>
              }
          />
        );
      });
    }
    return itemElements;
  }

  generateOrderList = () => {
    const { classes,storageMap,selectedLabId,updateState,middlewareMapObj } = this.props;
    let groups = [];
    if (storageMap.size > 0 ) {
      for (let [orderKey, orderObj] of storageMap) {
        let itemElements = this.generateOrderItems(orderKey,orderObj);
        let cbFieldProps = {
          id:orderKey,
          middlewareMapObj,
          orderKey,
          level:constants.IX_REQUEST_TEMPLATE_CB.LEVEL_2,
          updateState
        };
        groups.push(
          <Card key={`${selectedLabId}_${orderKey}`} className={classes.card}>
            <CardContent classes={{root:classes.cardContent}}>
              <FormControlLabel
                  classes={{
                    root: classes.formControlRoot,
                    label: classes.formControlLabel
                  }}
                  control={
                    <TemplateClickBoxField {...cbFieldProps}/>
                  }
                  label={
                    <Tooltip title={`${orderObj.labId}, ${orderObj.formShortName}`} classes={{tooltip:classes.tooltip}}>
                      <Typography component="div" noWrap className={classes.groupNameTitle}>
                        <span>{`${orderObj.labId}, ${orderObj.formShortName}`}</span>
                      </Typography>
                    </Tooltip>
                  }
              />
              <Typography component="div">{itemElements}</Typography>
            </CardContent>
          </Card>
        );
      }
    }
    return groups;
  }

  render() {
    const { classes,templateId,middlewareMapObj,isHasTemplateOrder=true,updateState } = this.props;
    let cbFieldProps = {
      id:templateId,
      middlewareMapObj,
      level:constants.IX_REQUEST_TEMPLATE_CB.LEVEL_1,
      updateState
    };

    return (
      <div>
        <FormGroup row className={classes.formGroupRow}>
          <div className={classNames(classes.selectAllBar,{[classes.hidden]: !isHasTemplateOrder})}>
            <FormControlLabel
                classes={{
                  root: classes.formControlRoot,
                  label: classes.formControlLabel
                }}
                control={
                  <TemplateClickBoxField {...cbFieldProps}/>
                }
                label={<span>All</span>}
            />
          </div>
          <div className={classNames(classes.wrapper)}>
            <div className={classNames(classes.cardContainer)}>
              {this.generateOrderList()}
            </div>
          </div>
        </FormGroup>
      </div>
    );
  }
}

export default withStyles(styles)(TemplateOrderContainer);
