import React, { Component } from 'react';
import { withStyles, Grid, Tabs, Tab, Typography, TextField } from '@material-ui/core';
import { styles } from './ContentContainerStyle';
import * as constants from '../../../../../../constants/IOE/ixRequest/ixRequestConstants';
import * as utils from '../../utils/ixUtils';
import {trim} from 'lodash';
import classNames from 'classnames';
import CIMSButton from '../../../../../../components/Buttons/CIMSButton';
import TabContainer from '../TabContainer/TabContainer';
import OrderContainer from '../OrderContainer/OrderContainer';
import OtherInfoDialog from '../OtherInfoDialog/OtherInfoDialog';
import SearchIxDialog from '../SearchIxDialog/SearchIxDialog';
import TemplateContainer from '../TemplateContainer/TemplateContainer';
import TemplateOrderInfoDialog from '../TemplateOrderInfoDialog/TemplateOrderInfoDialog';

class ContentContainer extends Component {
  constructor(props){
    super(props);
    let { topTabs } = props;
    this.state={
      searchIx: '',
      searchIsOpen: false,
      infoIsOpen: false,
      tabValue: topTabs[0].value,
      orderNumber:constants.ORDER_NUMBER_OPTIONS[0].value //default 1
    };
  }

  handleSearchDialogClose = () => {
    this.setState({
      searchIsOpen: false
    });
  }

  handleSearchIxChanged = event => {
    this.setState({
      searchIx: event.target.value
    });
  }

  handleSearchIxBlur = event => {
    this.setState({
      searchIx: trim(event.target.value)
    });
  }

  handleSearchBtnClick = () => {
    this.setState({
      searchIsOpen: true
    });
  }

  handleOrderNumberChange = event => {
    this.setState({
      orderNumber:event.value
    });
  }

  handleResetOrderNumber = () => {
    this.setState({
      orderNumber:constants.ORDER_NUMBER_OPTIONS[0].value
    });
  }

  generateMiddlewareObject = (defaultObj) => {
    const { frameworkMap, updateStateWithoutStatus} = this.props;
    let formObj = frameworkMap.has(defaultObj.labId)?frameworkMap.get(defaultObj.labId).formMap.get(defaultObj.selectedSubTabId):null;
    let valObj = utils.initMiddlewareObject(formObj);
    updateStateWithoutStatus&&updateStateWithoutStatus({
      middlewareObject:valObj,
      contentVals: {
        ...defaultObj
      }
    });
  }

  generateMiddlewareObjectByTemplate = (orderType) => {
    const { frameworkMap, categoryMap, updateStateWithoutStatus} = this.props;
    let categoryObj = categoryMap.has(orderType)?categoryMap.get(orderType):null;
    let defaultTemplateId = null;
    let defaultMiddlewareMap = new Map();
    if (!!categoryObj&&categoryObj.templateMap.size>0) {
      for (let templateId of categoryObj.templateMap.keys()) {
        defaultTemplateId = templateId;
        break;
      }
      let defaultTemplateObj = categoryObj.templateMap.get(defaultTemplateId);
      // template has order list
      if (defaultTemplateObj.storageMap.size>0) {
        for (let [storageKey,storageObj] of defaultTemplateObj.storageMap) {
          // init middlewareObject
          let formObj = frameworkMap.has(storageObj.labId)?frameworkMap.get(storageObj.labId).formMap.get(storageObj.codeIoeFormId):null;
          let middlewareObject = utils.initMiddlewareObject(formObj,storageObj,true,orderType);
          defaultMiddlewareMap.set(storageKey,middlewareObject);
        }
      }
    }

    updateStateWithoutStatus&&updateStateWithoutStatus({
      middlewareMapObj:{
        templateId:defaultTemplateId,
        templateSelectAll: false,
        middlewareMap:defaultMiddlewareMap
      },
      contentVals:{
        labId:null,
        selectedSubTabId: defaultTemplateId,
        infoTargetLabId: null,
        infoTargetFormId: defaultTemplateId
      }
    });
  }

  generateTab = () => {
    const { classes,topTabs } = this.props;
    let { tabValue } = this.state;
    let tabs = [];
    topTabs.forEach(option => {
      tabs.push(
        <Tab
            key={option.value}
            value={option.value}
            classes={{
              selected:classes.tabSelect
            }}
            className={tabValue===option.value?'tabSelected':'tabNavigation'}
            label={
              <Typography
                  className={classNames(classes.tabSpan,{
                    [classes.tabSpanSelected]: tabValue===option.value
                  })}
              >
                {option.label}
              </Typography>
            }
        />
      );
    });
    return tabs;
  }

  handleTabChange = (event, value) => {
    let { lab2FormMap,updateStateWithoutStatus,basicInfo,resetDiscipline } = this.props;
    if (value === constants.NORMAL_TOP_TABS[0].value) {
      // discipline
      let defaultObj = resetDiscipline&&resetDiscipline(lab2FormMap);
      this.generateMiddlewareObject(defaultObj);
    } else {
      // service / personal / nurse
      this.generateMiddlewareObjectByTemplate(value);
    }
    this.setState({
      tabValue: value
    });
    updateStateWithoutStatus&&updateStateWithoutStatus({
      orderIsEdit:false,
      selectedOrderKey: null,
      basicInfo:{
        ...basicInfo,
        infoOrderType: value,
        orderType: value
      }
    });
  }

  updateGroupingContainerState = (obj) => {
    this.setState({
      ...obj
    });
  }

  handleInfoDialogCancel = () =>{
    this.setState({
      infoIsOpen:false
    });
  }

  render() {
    let {
      classes,
      clinicList,
      ioeFormMap,
      basicInfo,
      contentVals,
      categoryMap,
      itemMapping,
      frameworkMap,
      lab2FormMap,
      dropdownMap,
      temporaryStorageMap,
      middlewareMapObj,
      middlewareObject,
      orderIsEdit,
      deletedStorageMap,
      selectedOrderKey,
      openCommonMessage,
      updateState,
      updateStateWithoutStatus
    } = this.props;
    let {
      searchIx,
      searchIsOpen,
      infoIsOpen,
      tabValue,
      orderNumber
    } = this.state;
    let tabContainerProps = {
      itemMapping,
      basicInfo,
      contentVals,
      orderNumber,
      selectedLabId:contentVals.labId,
      selectedSubTabId:contentVals.selectedSubTabId,
      frameworkMap,
      lab2FormMap,
      dropdownMap,
      temporaryStorageMap,
      middlewareObject,
      orderIsEdit,
      selectedOrderKey,
      openCommonMessage,
      updateState,
      updateStateWithoutStatus,
      updateGroupingContainerState:this.updateGroupingContainerState,
      handleOrderNumberChange:this.handleOrderNumberChange,
      handleResetOrderNumber:this.handleResetOrderNumber
    };

    let templateContainerProps = {
      itemMapping,
      categoryMap,
      basicInfo,
      contentVals,
      orderNumber,
      selectedSubTabId:contentVals.selectedSubTabId,
      dropdownMap,
      frameworkMap,
      temporaryStorageMap,
      middlewareMapObj,
      orderIsEdit,
      openCommonMessage,
      updateState,
      updateStateWithoutStatus,
      updateGroupingContainerState:this.updateGroupingContainerState,
      handleOrderNumberChange:this.handleOrderNumberChange,
      handleResetOrderNumber:this.handleResetOrderNumber
    };

    let orderContainerProps = {
      clinicList,
      basicInfo,
      contentVals,
      dropdownMap,
      deletedStorageMap,
      frameworkMap,
      temporaryStorageMap,
      lab2FormMap,
      updateState,
      updateGroupingContainerState:this.updateGroupingContainerState
    };

    let infoDialogProps = {
      basicInfo,
      contentVals,
      orderNumber,
      isOpen:infoIsOpen,
      orderIsEdit,
      selectedOrderKey,
      dialogTitle: 'Other Order Information',
      selectedLabId:contentVals.infoTargetLabId,
      selectedFormId:contentVals.infoTargetFormId,
      itemMapping,
      dropdownMap,
      frameworkMap,
      middlewareObject,
      temporaryStorageMap,
      updateState,
      updateStateWithoutStatus,
      handleInfoDialogCancel: this.handleInfoDialogCancel,
      handleResetOrderNumber:this.handleResetOrderNumber
    };

    let templateInfoDialogProps = {
      ioeFormMap,
      categoryMap,
      basicInfo,
      orderNumber,
      isOpen:infoIsOpen,
      dialogTitle: 'Other Order Information',
      selectedSubTabId:contentVals.infoTargetFormId,
      middlewareMapObj,
      itemMapping,
      dropdownMap,
      frameworkMap,
      temporaryStorageMap,
      updateState,
      updateStateWithoutStatus,
      handleInfoDialogCancel: this.handleInfoDialogCancel,
      handleResetOrderNumber:this.handleResetOrderNumber
    };

    let searchDialogProps = {
      basicInfo,
      frameworkMap,
      isOpen:searchIsOpen,
      searchIx,
      orderIsEdit,
      middlewareObject,
      updateState,
      handleSearchDialogCancel: this.handleSearchDialogClose,
      updateGroupingContainerState:this.updateGroupingContainerState
    };

    let inputProps = {
      autoCapitalize:'off',
      variant:'outlined',
      type:'text',
      inputProps: {
        style:{
          fontSize: '1rem',
          fontFamily: 'Arial'
        }
      }
    };

    let { orderType,infoOrderType } = basicInfo;
    return (
      <Grid container>
        <Grid item xs={10}>
          {/* top labs */}
          <Tabs
              classes={{root:classes.tabs}}
              indicatorColor="primary"
              value={tabValue}
              onChange={this.handleTabChange}
          >
            {this.generateTab()}
          </Tabs>
          {/* search ix */}
          <div className={
              classNames(classes.searchBar,{
                [classes.divHidden]:orderType === constants.NURSE_TOP_TABS[0].value
              })
            }
          >
            <div className={classes.flexCenter}>
              <label className={classes.label}>Search Ix from Discipline:</label>
              <TextField
                  id="input_ix_request_search"
                  onChange={this.handleSearchIxChanged}
                  onBlur={this.handleSearchIxBlur}
                  value={searchIx}
                  {...inputProps}
              />
              <CIMSButton
                  id="btn_ix_request_search"
                  className={classes.searchBtn}
                  onClick={this.handleSearchBtnClick}
              >
                Search
              </CIMSButton>
            </div>
          </div>
          {
            orderType === constants.NORMAL_TOP_TABS[0].value?(
              <TabContainer {...tabContainerProps}/>
            ):(
              <TemplateContainer {...templateContainerProps}/>
            )
          }
        </Grid>
        {/* orders */}
        <Grid item xs={2}>
          <OrderContainer {...orderContainerProps} />
        </Grid>
        {/* Other Info dialog */}
        {
          infoOrderType === constants.NORMAL_TOP_TABS[0].value?(
            <OtherInfoDialog {...infoDialogProps} />
          ):(
            <TemplateOrderInfoDialog {...templateInfoDialogProps} />
          )
        }
        {/* Search dialog */}
        <SearchIxDialog {...searchDialogProps} />
      </Grid>
    );
  }
}

export default withStyles(styles)(ContentContainer);
