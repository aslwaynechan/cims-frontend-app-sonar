import React, { Component } from 'react';
import {styles} from './TemplateContainerStyle';
import { withStyles, AppBar, Toolbar, Tooltip, Grid, Fab} from '@material-ui/core';
import { ArrowForward } from '@material-ui/icons';
import classNames from 'classnames';
import { cloneDeep } from 'lodash';
import CIMSMultiTabs from '../../../../../../components/Tabs/CIMSMultiTabs';
import CIMSMultiTab from '../../../../../../components/Tabs/CIMSMultiTab';
// import CustomizedSelectFieldValidator from '../../../../../../components/Select/CustomizedSelect/CustomizedSelectFieldValidator';
import * as constants from '../../../../../../constants/IOE/ixRequest/ixRequestConstants';
import * as utils from '../../utils/ixUtils';
import TemplateOrderContainer from '../TemplateOrderContainer/TemplateOrderContainer';
import SelectFieldValidator from '../../../../../../components/FormValidator/SelectFieldValidator';

class TemplateContainer extends Component {
  constructor(props){
    super(props);
    this.state = {
      open: false
    };
  }

  handleOrderNumberChange = event => {
    const { handleOrderNumberChange } = this.props;
    handleOrderNumberChange&&handleOrderNumberChange(event);
  }

  changeTabValue = (event, value) => {
    let { frameworkMap,updateStateWithoutStatus,categoryMap,basicInfo } = this.props;
    let { orderType } = basicInfo;
    let categoryObj = categoryMap.has(orderType)?categoryMap.get(orderType):null;
    let middlewareMap = new Map();
    if (!!categoryObj&&categoryObj.templateMap.size>0) {
      let templateObj = categoryObj.templateMap.get(value);
      // template has order list
      if (templateObj.storageMap.size>0) {
        for (let [storageKey,storageObj] of templateObj.storageMap) {
          // init middlewareObject
          let formObj = frameworkMap.has(storageObj.labId)?frameworkMap.get(storageObj.labId).formMap.get(storageObj.codeIoeFormId):null;
          let middlewareObject = utils.initMiddlewareObject(formObj,storageObj,true,orderType);
          middlewareMap.set(storageKey,middlewareObject);
        }
      }
    }
    updateStateWithoutStatus&&updateStateWithoutStatus({
      orderIsEdit:false,
      selectedOrderKey: null,
      middlewareMapObj:{
        templateId:value,
        templateSelectAll: false,
        middlewareMap
      },
      contentVals:{
        labId:null,
        selectedSubTabId: value,
        infoTargetLabId: null,
        infoTargetFormId: value
      }
    });
  }

  generateTab = () => {
    const { classes,selectedSubTabId,categoryMap,basicInfo } = this.props;
    let tabs = [];
    if (!!basicInfo.orderType&&categoryMap.size > 0) {
      if (categoryMap.has(basicInfo.orderType)) {
        let categoryObj = categoryMap.get(basicInfo.orderType);
        if (categoryObj.templateMap.size > 0) {
          for (let [templateId, templateObj] of categoryObj.templateMap) {
            tabs.push(
              <CIMSMultiTab
                  key={templateId}
                  id={`ix_request_sub_tab_${templateId}`}
                  value={templateId}
                  disableClose
                  className={selectedSubTabId === templateId ? 'tabSelected' : 'tabNavigation'}
                  label={
                    <span className={classes.tabSpan}>
                      {templateObj.templateName}
                    </span>
                  }
              />
            );
          }
        }
      }
    }
    return tabs;
  }

  handleAddOrderWithInfo = () => {
    let {
      orderIsEdit,
      categoryMap,
      itemMapping,
      diagnosisErrorFlag,
      basicInfo,
      orderNumber,
      updateStateWithoutStatus,
      updateGroupingContainerState,
      openCommonMessage,
      middlewareMapObj,
      selectedSubTabId,//templateId
      updateState,
      contentVals,
      temporaryStorageMap,
      handleResetOrderNumber
    } = this.props;
    let { orderType } = basicInfo;
    // validate
    diagnosisErrorFlag = basicInfo.infoDiagnosis === ''?true:false;
    if (!diagnosisErrorFlag) {
      let msgCode = utils.handleValidateTemplateItems(middlewareMapObj);
      if (msgCode === '') {
        let {templateOrderTotalKeys,templateOrderWithQuestionKeys} = utils.handleTemplateOrderWithQuestions(middlewareMapObj,itemMapping);
        let { middlewareMap } = middlewareMapObj;
        if (templateOrderWithQuestionKeys.length > 0) {
          //backup question
          templateOrderWithQuestionKeys.forEach(key => {
            let middlewareObject = middlewareMap.get(key);
            middlewareObject.backupQuestionValMap = cloneDeep(middlewareObject.questionValMap);
            if (!orderIsEdit) {
              utils.resetQuestionStatus(middlewareObject.questionValMap);
            }
            utils.resetQuestionGroupStatus(middlewareObject.questionGroupMap);
          });
          updateState&&updateState({
            middlewareMapObj,
            contentVals:{
              ...contentVals,
              infoTargetLabId: null,
              infoTargetFormId: selectedSubTabId // FromId as TemplateId
            }
          });
          // has question
          updateGroupingContainerState&&updateGroupingContainerState({
            infoIsOpen: true
          });
        } else {
          let { storageMap } = categoryMap.get(orderType).templateMap.get(selectedSubTabId);
          // no question
          // Edit => go to the Discipline
          // Add
          templateOrderTotalKeys.forEach(key => {
            let middlewareObject = middlewareMap.get(key);
            let obj = utils.initTemporaryStorageObj(middlewareObject,basicInfo,storageMap.get(key).labId,orderType);
            for (let i = 0; i < orderNumber; i++) {
              temporaryStorageMap.set(`${storageMap.get(key).codeIoeFormId}_${Math.random()}`,obj);
            }
          });
          updateState&&updateState({
            selectedOrderKey:null,
            orderIsEdit:false,
            temporaryStorageMap
          });
          handleResetOrderNumber&&handleResetOrderNumber();
        }
      } else {
        openCommonMessage&&openCommonMessage({msgCode});
      }
    }
    updateStateWithoutStatus&&updateStateWithoutStatus({
      diagnosisErrorFlag
    });
  }

  render() {
    const {
      classes,
      basicInfo,
      categoryMap,
      selectedSubTabId,
      updateState,
      orderIsEdit,
      orderNumber,
      dropdownMap,
      middlewareMapObj
    } = this.props;
    let { open } = this.state;
    let { orderType } = basicInfo;
    let isHasTemplate = categoryMap.has(orderType)?(categoryMap.get(orderType).templateMap.size>0?true:false):false;
    let isHasTemplateOrder = false;
    let storageMap = new Map();
    if (isHasTemplate) {
      storageMap = categoryMap.get(basicInfo.orderType).templateMap.get(selectedSubTabId).storageMap;
      isHasTemplateOrder = storageMap.size>0?true:false;
    }

    let templateOrderContainerProps = {
      isHasTemplateOrder,
      templateId:selectedSubTabId,
      storageMap,
      dropdownMap,
      middlewareMapObj,
      updateState
    };
    return (
      <div className={classes.wrapper}>
        <AppBar
            className={classNames(classes.appBar, {
              [classes.appBarShift]: open
            })}
            position="relative"
        >
          <Toolbar
              disableGutters={!open}
              classes={{
                regular:classes.toolBar
              }}
          >
          </Toolbar>
        </AppBar>
        {/* list content */}
        <div
            className={classNames(classes.content, {
              [classes.contentOpen]: open
            })}
        >
          {/* sub tab (form name or template name) */}
          <CIMSMultiTabs
              value={selectedSubTabId}
              onChange={this.changeTabValue}
              indicatorColor="primary"
          >
            {this.generateTab()}
          </CIMSMultiTabs>
          {/* content */}
          <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start"
          >
            {isHasTemplate?(
              <Grid item xs={12} classes={{'grid-xs-12':classes.fullWrapper}}>
                <TemplateOrderContainer {...templateOrderContainerProps} />
              </Grid>
            ):null}
          </Grid>
        </div>
        {/* action */}
        <div className={classes.actionWrapper}>
          <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
              className={classes.fabGird}
          >
            <Grid item style={{width:82,marginBottom:20}}>
              <SelectFieldValidator
                  id="ix_request_order_number_dropdown"
                  options={constants.ORDER_NUMBER_OPTIONS.map(option=>{
                    return {
                      label: option.label,
                      value: option.value
                    };
                  })}
                  isDisabled={orderIsEdit}
                  value={orderNumber}
                  onChange={event => {this.handleOrderNumberChange(event);}}
              />
              {/* <CustomizedSelectFieldValidator
                  id="ix_request_order_number_dropdown"
                  options={constants.ORDER_NUMBER_OPTIONS.map(option=>{
                    return {
                      label: option.label,
                      value: option.value
                    };
                  })}
                  isDisabled={orderIsEdit}
                  value={orderNumber}
                  onChange={event => {this.handleOrderNumberChange(event);}}
                  styles={{ menuPortal: base => ({ ...base, zIndex: 1600 }) }}
                  menuPortalTarget={document.body}
              /> */}
            </Grid>
            <Grid item>
              <Tooltip title="Add Order" classes={{tooltip:classes.tooltip}}>
                <Fab
                    size="small"
                    color="primary"
                    aria-label="Add Order"
                    id="btn_ix_request_add_order"
                    disabled={isHasTemplate?!isHasTemplateOrder:true}
                    className={classes.fab}
                    onClick={()=>{this.handleAddOrderWithInfo();}}
                >
                  <ArrowForward />
                </Fab>
              </Tooltip>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(TemplateContainer);
