import React, { Component } from 'react';
import { withStyles, Dialog, Paper, DialogTitle, DialogContent, Typography, DialogActions, Grid, Button, Card, CardContent, Divider } from '@material-ui/core';
import { CheckCircle, HighlightOff } from '@material-ui/icons';
import Draggable from 'react-draggable';
import { styles } from './TemplateOrderInfoDialogStyle';
import InputBoxField from '../../components/InputBoxField/InputBoxField';
import ClickBoxField from '../../components/ClickBoxField/ClickBoxField';
import DropdownField from '../../components/DropdownField/DropdownField';
import * as constants from '../../../../../../constants/IOE/ixRequest/ixRequestConstants';
import * as utils from '../../utils/ixUtils';
import { findIndex } from 'lodash';
import classNames from 'classnames';

function PaperComponent(props) {
  return (
    <Draggable
        enableUserSelectHack={false}
        onStart={(e)=>{
          if (e.target.getAttribute('customdraginfo') === 'allowed') {
            return true;
          } else {
            return false;
          }
        }}
    >
      <Paper {...props} />
    </Draggable>
  );
}

class TemplateOrderInfoDialog extends Component {
  constructor(props){
    super(props);
    this.displayGroupNamesMap = new Map();
  }

  handleMandatoryVerify = (templateOrderWithQuestionKeys,middlewareMap) => {
    let mandatoryFlag = true;
    templateOrderWithQuestionKeys.forEach(key => {
      let middlewareObject = middlewareMap.get(key);
      let { questionGroupMap,questionValMap } = middlewareObject;
      let displayGroupNames = this.displayGroupNamesMap.get(key);
      if (displayGroupNames.size > 0) {
        for (let groupName of displayGroupNames.values()) {
          let obj = questionGroupMap.get(groupName);
          let {ids} = obj;
          let mandatoryFlags = [];
          if (ids.length > 0) {
            ids.forEach(id => {
              if (questionValMap.get(id).isChecked) {
                mandatoryFlags.push(id);
              }
            });
            if (mandatoryFlags.length === 0) {
              obj.isError = true;
              mandatoryFlag = false;
            } else {
              obj.isError = false;
            }
          }
        }
      }
    });
    return mandatoryFlag;
  }

  handleAction = action => {
    let { orderNumber,selectedSubTabId, categoryMap, temporaryStorageMap, updateState,handleInfoDialogCancel,handleResetOrderNumber,basicInfo,middlewareMapObj,itemMapping } = this.props;
    let {templateOrderTotalKeys,templateOrderWithQuestionKeys} = utils.handleTemplateOrderWithQuestions(middlewareMapObj,itemMapping);
    let { middlewareMap } = middlewareMapObj;
    let { infoOrderType } = basicInfo;
    if (action === constants.INFO_DIALOG_ACTION_TYPE.CANCEL) {
      templateOrderWithQuestionKeys.forEach(key => {
        let middlewareObject = middlewareMap.get(key);
        middlewareObject.questionValMap = middlewareObject.backupQuestionValMap;
        delete middlewareObject.backupQuestionValMap;
      });
      handleInfoDialogCancel&&handleInfoDialogCancel();
      updateState&&updateState({
        selectedOrderKey:null,
        orderIsEdit:false,
        middlewareMapObj
      });
    } else {
      //verify
      let mandatoryFlag = this.handleMandatoryVerify(templateOrderWithQuestionKeys,middlewareMap);
      if (mandatoryFlag) {
        let { storageMap } = categoryMap.get(infoOrderType).templateMap.get(selectedSubTabId);
        templateOrderTotalKeys.forEach(key => {
          let middlewareObject = middlewareMap.get(key);
          // Add
          let obj = utils.initTemporaryStorageObj(middlewareObject,basicInfo,storageMap.get(key).labId,infoOrderType);
          for (let i = 0; i < orderNumber; i++) {
            temporaryStorageMap.set(`${storageMap.get(key).codeIoeFormId}_${Math.random()}`,obj);
          }
          delete middlewareObject.backupQuestionValMap;
        });
        updateState&&updateState({
          selectedOrderKey:null,
          orderIsEdit:false,
          temporaryStorageMap,
          middlewareMapObj
        });
        handleResetOrderNumber&&handleResetOrderNumber();
        handleInfoDialogCancel&&handleInfoDialogCancel();
      } else {
        updateState&&updateState({
          middlewareMapObj
        });
      }
    }
  }

  handleQuestionType = (id,valMap,masterTestMap,formId) => {
    let { frameworkMap,ioeFormMap } = this.props;
    let {labId} = ioeFormMap.get(formId);
    let questionItemsMap = frameworkMap.get(labId).formMap.get(formId).questionItemsMap;
    let iquIdSet = new Set();
    for (let items of questionItemsMap.values()) {
      let index = findIndex(items,item=>{
        return item.codeIoeFormItemId === id&&item.ioeType === constants.ITEM_QUESTION_TYPE.IQU&&item.frmItemTypeCd === constants.FORM_ITEM_TYPE.CLICK_BOX;
      });
      if (index!==-1) {
        items.forEach(item => {
          iquIdSet.add(item.codeIoeFormItemId);
        });
        break;
      }
    }
    utils.handleQuestionItem(id,valMap,iquIdSet);
  }

  generateItemByType = (item,type=constants.ITEM_VALUE.TYPE1,middlewareObject) => {
    const { classes,dropdownMap,updateState } = this.props;
    let itemType = null;
    if (type === constants.ITEM_VALUE.TYPE1) {
      itemType = item.frmItemTypeCd;
    } else if (type === constants.ITEM_VALUE.TYPE2) {
      itemType = item.frmItemTypeCd2;
    }
    let fieldProps = {
      id:item.codeIoeFormItemId,
      middlewareObject,
      itemValType:type,
      categoryType:constants.ITEM_CATEGORY_TYPE.QUESTION,
      updateState
    };

    let element = null;
    switch (itemType) {
      case constants.FORM_ITEM_TYPE.CLICK_BOX:{
        fieldProps.sideEffect = this.handleQuestionType;
        if (type === constants.ITEM_VALUE.TYPE1) {
          element = (
            <div className={classes.questionWrapper}>
              <div className={classes.componentWrapper}>
                <ClickBoxField {...fieldProps} />
              </div>
              <label>{item.frmItemName}</label>
            </div>
          );
        }
        break;
      }
      case constants.FORM_ITEM_TYPE.INPUT_BOX:{
        fieldProps.maxLength = item.fieldLength;
        fieldProps.sideEffect = utils.handleInfoOperationType;
        fieldProps.enableDisabled = false;
        element = (
          <div className={classes.questionWrapper}>
            <label>{item.frmItemName}</label>
            <InputBoxField {...fieldProps} />
          </div>
        );
        break;
      }
      case constants.FORM_ITEM_TYPE.DROP_DOWN_LIST:{
        fieldProps.dropdownMap = dropdownMap;
        fieldProps.sideEffect = utils.handleInfoOperationType;
        fieldProps.enableDisabled = false;
        element = (
          <div className={classes.questionWrapper}>
            <label>{item.frmItemName}</label>
            <DropdownField {...fieldProps} />
          </div>
        );
        break;
      }
      default:
        break;
    }
    return element;
  }

  isDislayItem = (item,middlewareObject) => {
    const { itemMapping } = this.props;
    let {codeIoeFormId} = middlewareObject;
    let displayItem = true;
    let { questionValMap } = middlewareObject;
    if (itemMapping.has(codeIoeFormId)) {
      let itemMap = itemMapping.get(codeIoeFormId);
      let currentItemId = item.codeIoeFormItemId;
      if (itemMap.has(currentItemId)) {
        let mappingIds = itemMap.get(currentItemId);
        mappingIds.forEach(id => {
          displayItem = questionValMap.has(id)?questionValMap.get(id).isChecked:false;
        });
      }
    }
    return displayItem;
  }

  generateGroupItems = (items,labId,formId,middlewareObject) => {
    let { classes } = this.props;
    let itemElements = [];
    if (items.length>0) {
      items.forEach(item => {
        let itemElement = this.generateItemByType(item,constants.ITEM_VALUE.TYPE1,middlewareObject);
        let displayItem = true;
        if (item.ioeType === constants.ITEM_QUESTION_TYPE.IQS) {
          displayItem = this.validateIQS(middlewareObject);
        } else {
          displayItem = this.isDislayItem(item,middlewareObject);
        }
        itemElements.push(
          <div
              key={`${labId}_${formId}_${item.codeIoeFormItemId}`}
              className={
                classNames({
                  [classes.displayNone]:!displayItem
                })
              }
          >
            {itemElement}
          </div>
        );
      });
    }
    return itemElements;
  }

  validateGroup = (questionItems,middlewareObject) => {
    let displayGroupFlag = true;
    let index = findIndex(questionItems,item => {
      return item.ioeType !== constants.ITEM_QUESTION_TYPE.IQS;
    });
    if (index !== -1) {
      displayGroupFlag = true;
    } else {
      // only exist IQS
      displayGroupFlag = this.validateIQS(middlewareObject);
    }
    return displayGroupFlag;
  }

  validateIQS = middlewareObject => {
    const { itemMapping } = this.props;
    let { codeIoeFormId,specimenValMap } = middlewareObject;
    let displayIQS = false;
    if (itemMapping.has(codeIoeFormId)) {
      let itemMap = itemMapping.get(codeIoeFormId);
      let specimenObj = null;
      for (let itemObj of specimenValMap.values()) {
        if (itemObj.isChecked) {
          specimenObj = itemObj;
          break;
        }
      }
      if (!!specimenObj) {
        for (let speciemnIdSet of itemMap.values()) {
          if (speciemnIdSet.has(specimenObj.codeIoeFormItemId)) {
            displayIQS = true;
            break;
          }
        }
      }
    }
    return displayIQS;
  }

  validateIsQuestion = items => {
    let displayRequired = false;
    let index = findIndex(items,item=>{
      return item.ioeType === constants.ITEM_QUESTION_TYPE.IQS || item.ioeType === constants.ITEM_QUESTION_TYPE.IQU || item.ioeType === constants.ITEM_QUESTION_TYPE.IQUM;
    });
    if (index !== -1) {
      displayRequired = true;
    }
    return displayRequired;
  }

  generateQuestionList = () => {
    const { classes,frameworkMap,middlewareMapObj,itemMapping,basicInfo,categoryMap,selectedSubTabId } = this.props;
    let { orderType } = basicInfo;
    let forms = [];
    if (frameworkMap.size > 0 && categoryMap.size > 0 && categoryMap.get(orderType).templateMap.size > 0) {
      let {templateOrderWithQuestionKeys} = utils.handleTemplateOrderWithQuestions(middlewareMapObj,itemMapping);
      let { storageMap } = categoryMap.get(orderType).templateMap.get(selectedSubTabId);
      templateOrderWithQuestionKeys.forEach(key => {
        let {labId,codeIoeFormId:formId,formShortName} = storageMap.get(key);
        let questionItemsMap = frameworkMap.get(labId).formMap.get(formId).questionItemsMap;
        let middlewareObject = middlewareMapObj.middlewareMap.get(key);
        let orderTitle = `${labId}, ${formShortName}`;
        if (!!questionItemsMap&&questionItemsMap.size>0) {
          let groups = [];
          for (let [groupName, items] of questionItemsMap) {
            let displayGroup = this.validateGroup(items,middlewareObject);
            let displayRequired = this.validateIsQuestion(items);
            let { questionGroupMap } = middlewareObject;
            let groupObj = questionGroupMap.get(groupName);
            let errorSpanElm = groupObj.isError?(
              <span className={classes.errorSpan}>(This field is required.)</span>
            ):null;

            if (!displayGroup) {
              continue;
            } else {
              let itemElements = this.generateGroupItems(items,labId,formId,middlewareObject);
              let tempSet = this.displayGroupNamesMap.has(key)?this.displayGroupNamesMap.get(key):new Set();
              tempSet.add(groupName);
              this.displayGroupNamesMap.set(key,tempSet);
              groups.push(
                <Card
                    key={`ix_request_other_info_${labId}_${formId}_${groupName}`}
                    className={
                      classNames(classes.card,{
                        [classes.errorCard]:groupObj.isError
                      })
                    }
                >
                  <CardContent classes={{root:classes.cardContent}}>
                    <Typography
                        component="div"
                        variant="subtitle1"
                        classes={{subtitle1: classes.groupNameTitle}}
                    >
                      {displayRequired?(
                        <label><span className={classes.requiredFlag}>*</span> {groupName} {errorSpanElm}</label>
                      ):(
                        <label>{groupName}</label>
                      )}
                    </Typography>
                    <Typography component="div">{itemElements}</Typography>
                  </CardContent>
                </Card>
              );
            }
          }
          forms.push(
            <div key={`ix_request_other_info_form_${formId}`}>
              <label className={classes.formTitle}>{orderTitle}</label>
              <div>
                {groups}
              </div>
              <Divider className={classes.divider}/>
            </div>
          );
        }
      });
    }
    return forms;
  }

  resetStatus = () => {
    this.displayGroupNamesMap = new Map();
  }

  render() {
    const { classes, isOpen=false, dialogTitle='' } = this.props;
    return (
      <Dialog
          classes={{paper: classes.paper}}
          fullWidth
          maxWidth="md"
          open={isOpen}
          scroll="paper"
          PaperComponent={PaperComponent}
          onExited={this.resetStatus}
          onEscapeKeyDown={()=>{this.handleAction(constants.INFO_DIALOG_ACTION_TYPE.CANCEL);}}
      >
        {/* title */}
        <DialogTitle
            className={classes.dialogTitle}
            disableTypography
            customdraginfo="allowed"
        >
          {dialogTitle}
        </DialogTitle>
        {/* content */}
        <DialogContent classes={{'root':classes.dialogContent}}>
          <Typography component="div">
            {this.generateQuestionList()}
          </Typography>
        </DialogContent>
        {/* button group */}
        <DialogActions className={classes.dialogActions}>
          <Grid container direction="row" justify="space-between" alignItems="center">
            <Grid item>
              <label><span className={classes.requiredFlag}>*</span> Mandatory field</label>
            </Grid>
            <Grid item>
              <Button
                  classes={{
                    'root':classes.btnRoot,
                    'label':classes.btnLabel
                  }}
                  color="primary"
                  id="btn_ix_request_other_info_dialog_ok"
                  onClick={()=>{this.handleAction(constants.INFO_DIALOG_ACTION_TYPE.OK);}}
                  variant="contained"
              >
                <CheckCircle className={classes.btnIcon}/>
                OK
              </Button>
              <Button
                  classes={{
                    'root':classes.btnRoot,
                    'label':classes.btnLabel
                  }}
                  id="btn_ix_request_other_info_dialog_cancel"
                  onClick={()=>{this.handleAction(constants.INFO_DIALOG_ACTION_TYPE.CANCEL);}}
                  variant="contained"
              >
                <HighlightOff className={classes.btnIcon}/>
                Cancel
              </Button>
            </Grid>
          </Grid>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles)(TemplateOrderInfoDialog);
