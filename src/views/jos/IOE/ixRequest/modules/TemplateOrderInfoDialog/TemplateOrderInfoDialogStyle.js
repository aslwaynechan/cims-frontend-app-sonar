export const styles = theme => ({
  paper: {
    minWidth: 600
  },
  dialogTitle: {
    lineHeight: 1.6,
    fontWeight: 'bold',
    fontSize: '1.5rem',
    fontFamily: 'Arial',
    paddingTop: '5px',
    paddingBottom: '5px',
    backgroundColor: '#b8bcb9'
  },
  dialogContent: {
    padding: '10px 24px',
    backgroundColor: 'white'
    // overflowY: 'hidden'
  },
  dialogActions: {
    margin:'0px' ,
    padding:'0 10px' ,
    backgroundColor:'white'
  },
  btnRoot: {
    margin: theme.spacing()
  },
  btnLabel: {
    fontSize: '16px',
    fontFamily: 'Arial',
    textTransform: 'capitalize'
  },
  errorCard: {
    border: '1px solid #FD0000 !important'
  },
  card: {
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
    boxShadow: 'none',
    border: '1px solid #0579C8',
    height: 'max-content'
  },
  cardContent: {
    padding: '5px 10px',
    '&:last-child':{
      paddingBottom: '5px'
    }
  },
  groupNameTitle: {
    cursor: 'default',
    fontSize: '1.125rem',
    fontWeight: 'bold'
  },
  questionWrapper: {
    clear: 'both'
  },
  componentWrapper: {
    float: 'left'
  },
  requiredFlag: {
    color: '#FD0000'
  },
  displayNone: {
    display: 'none'
  },
  errorSpan: {
    fontSize: 14,
    color: '#FD0000',
    fontWeight: 100
  },
  formTitle: {
    paddingLeft: 10,
    fontWeight: 'bold'
  },
  divider: {
    height: 2
  }
});
