import {COMMON_ACTION_TYPE} from '../../../../../constants/common/commonConstants';
import * as constants from '../../../../../constants/IOE/ixRequest/ixRequestConstants';
import { isNull,union,findIndex,uniq,isEmpty,keys,find, toInteger } from 'lodash';
import { IX_REQUEST_CODE } from '../../../../../constants/message/IOECode/ixRequestCode';

let generateMasterTestMap = (testItemsMap) => {
  let tempMap = new Map();
  if (testItemsMap.size > 0) {
    for (let items of testItemsMap.values()) {
      items.forEach(item => {
        if (item.ioeMasterTest === constants.TEST_ITEM_MASTER_TEST_FLAG) {
          tempMap.set(item.codeIoeFormItemId,[]);
        } else if (!isNull(item.ioeMasterId)) {
          if (tempMap.has(item.ioeMasterId)) {
            let tempIds = tempMap.get(item.ioeMasterId);
            tempIds.push(item.codeIoeFormItemId);
            tempMap.set(item.ioeMasterId,tempIds);
          } else {
            tempMap.set(item.ioeMasterId,[item.codeIoeFormItemId]);
          }
        }
      });
    }
  }
  return tempMap;
};

let generateValMap = (itemsMap,formId,storageObjMap,disableStorageChecked,orderType) => {
  let valMap = new Map();
  if (itemsMap.size>0) {
    for (let items of itemsMap.values()) {
      items.forEach(item => {
        let obj = {
          codeIoeFormItemId:item.codeIoeFormItemId,
          operationType: null,
          version: null,
          itemVal: null,
          itemVal2: null,
          codeIoeFormId:formId,
          itemIoeType: item.ioeType,
          isChecked: false, //Click box status
          itemName: item.frmItemName,
          frmItemTypeCd: item.frmItemTypeCd,
          frmItemTypeCd2: item.frmItemTypeCd2,
          createdBy:null,
          createdDtm:null,
          updatedBy:null,
          updatedDtm:null,
          ioeRequestId: null, // request id
          ioeRequestItemId:null // request item id
        };
        if (storageObjMap.has(item.codeIoeFormItemId)) {
          let valObj = storageObjMap.get(item.codeIoeFormItemId);
          obj.operationType = valObj.operationType;
          obj.version = valObj.version;
          obj.itemVal = valObj.itemVal;
          obj.itemVal2 = valObj.itemVal2;
          obj.isChecked = disableStorageChecked?false:true;
          obj.createdBy = valObj.createdBy;
          obj.createdDtm = valObj.createdDtm;
          obj.updatedBy = valObj.updatedBy;
          obj.updatedDtm = valObj.updatedDtm;
          obj.ioeRequestId = valObj.ioeRequestId;
          obj.ioeRequestItemId = valObj.ioeRequestItemId;
        }
        if (orderType === constants.NORMAL_TOP_TABS[0].value) {
          // Discipline
          valMap.set(item.codeIoeFormItemId,obj);
        } else {
          // Service / Personal / Nurse
          if (storageObjMap.has(item.codeIoeFormItemId)) {
            valMap.set(item.codeIoeFormItemId,obj);
          }
        }
      });
    }
  }
  return valMap;
};

let generateQuestionGroupMap = (itemsMap) => {
  let groupItemMap = new Map();
  if (itemsMap.size>0) {
    for (let [groupName, items] of itemsMap) {
      let obj = {
        ids: [],
        isError: false
      };
      items.forEach(item => {
        if (item.ioeType === constants.ITEM_QUESTION_TYPE.IQU||item.ioeType === constants.ITEM_QUESTION_TYPE.IQS||item.ioeType === constants.ITEM_QUESTION_TYPE.IQUM) {
          obj.ids.push(item.codeIoeFormItemId);
        }
      });
      groupItemMap.set(groupName,obj);
    }
  }
  return groupItemMap;
};

export function initMiddlewareObject(formObj,storageObj=null,disableStorageChecked=false,orderType=constants.NORMAL_TOP_TABS[0].value) {
  let valObj = {
    codeIoeFormId:null,
    formShortName:'',
    selectAll:false,
    testValMap:new Map(),
    specimenValMap:new Map(),
    otherValMap:new Map(),
    questionValMap:new Map(),
    masterTestMap:new Map(),
    questionGroupMap:new Map()
  };
  if (!isNull(formObj)) {
    let {codeIoeFormId,testItemsMap,specimenItemsMap,otherItemsMap,questionItemsMap,formShortName} = formObj;
    let testStorageObjMap = new Map();
    let specimenStorageObjMap = new Map();
    let otherStorageObjMap = new Map();
    let questionStorageObjMap = new Map();
    if (!isNull(storageObj)) {
      testStorageObjMap = storageObj.testItemsMap;
      specimenStorageObjMap = storageObj.specimenItemsMap;
      otherStorageObjMap = storageObj.otherItemsMap;
      questionStorageObjMap = storageObj.questionItemsMap;
    }
    valObj.codeIoeFormId = codeIoeFormId;
    valObj.formShortName = formShortName;
    valObj.testValMap = generateValMap(testItemsMap,codeIoeFormId,testStorageObjMap,disableStorageChecked,orderType);
    valObj.specimenValMap = generateValMap(specimenItemsMap,codeIoeFormId,specimenStorageObjMap,disableStorageChecked,orderType);
    valObj.otherValMap = generateValMap(otherItemsMap,codeIoeFormId,otherStorageObjMap,disableStorageChecked,constants.NORMAL_TOP_TABS[0].value);
    valObj.questionValMap = generateValMap(questionItemsMap,codeIoeFormId,questionStorageObjMap,disableStorageChecked,constants.NORMAL_TOP_TABS[0].value);
    valObj.masterTestMap = generateMasterTestMap(testItemsMap);
    valObj.questionGroupMap = generateQuestionGroupMap(questionItemsMap);
  }
  return valObj;
}

let transformValMap2StorageMap = (itemsMap) => {
  let valMap = new Map();
  if (itemsMap.size>0) {
    for (let item of itemsMap.values()) {
      if (item.isChecked||!isNull(item.operationType)) {
        valMap.set(item.codeIoeFormItemId,{
          codeIoeFormId: item.codeIoeFormId,
          codeIoeFormItemId:item.codeIoeFormItemId,
          operationType: item.operationType,
          version: item.version,
          itemVal: item.itemVal,
          itemVal2: item.itemVal2,
          itemName: item.itemName,
          createdBy: item.createdBy,
          createdDtm: item.createdDtm,
          updatedBy: item.updatedBy,
          updatedDtm: item.updatedDtm,
          itemIoeType: item.itemIoeType,
          frmItemTypeCd: item.frmItemTypeCd,
          frmItemTypeCd2: item.frmItemTypeCd2,
          ioeRequestId: item.ioeRequestId, // request id
          ioeRequestItemId: item.ioeRequestItemId // request item id
        });
      }
    }
  }
  return valMap;
};

let handleOtherInfoCB = (valObj,basicInfo) => {
  if (!isNull(valObj.version)) {
    if (basicInfo.urgentIsChecked) {
      valObj.operationType = COMMON_ACTION_TYPE.UPDATE;
    } else {
      valObj.operationType = COMMON_ACTION_TYPE.DELETE;
    }
  } else if (isNull(valObj.version)&&basicInfo.urgentIsChecked) {
    valObj.operationType = COMMON_ACTION_TYPE.INSERT;
  } else {
    valObj.operationType = null;
  }
};

let handleOtherInfoIB = (valObj,itemVal) => {
  valObj.itemVal = itemVal;
  if (!isNull(valObj.version)) {
    if (!!itemVal) {
      valObj.operationType = COMMON_ACTION_TYPE.UPDATE;
    } else if (itemVal === '') {
      valObj.operationType = COMMON_ACTION_TYPE.DELETE;
    }
  } else if (isNull(valObj.version)&&(!!itemVal)) {
    valObj.operationType = COMMON_ACTION_TYPE.INSERT;
  } else {
    valObj.operationType = null;
  }
};

let handleOtherInfoDL = (valObj,itemVal) => {
  valObj.itemVal = itemVal;
  if (!isNull(valObj.version)) {
    valObj.operationType = COMMON_ACTION_TYPE.UPDATE;
  } else if (isNull(valObj.version)&&(!!itemVal)) {
    valObj.operationType = COMMON_ACTION_TYPE.INSERT;
  } else {
    valObj.operationType = null;
  }
};

let transformInfoValMap2StorageMap = (itemsMap,basicInfo,orderType) => {
  let valMap = new Map();
  if (itemsMap.size>0) {
    for (let item of itemsMap.values()) {
      let tempObj = {
        codeIoeFormId: item.codeIoeFormId,
        codeIoeFormItemId:item.codeIoeFormItemId,
        operationType: item.operationType,
        version: item.version,
        itemVal: item.itemVal,
        itemVal2: item.itemVal2,
        itemName: item.itemName,
        createdBy: item.createdBy,
        createdDtm: item.createdDtm,
        updatedBy: item.updatedBy,
        updatedDtm: item.updatedDtm,
        itemIoeType: item.itemIoeType,
        frmItemTypeCd: item.frmItemTypeCd,
        ioeRequestId: item.ioeRequestId, // request id
        ioeRequestItemId: item.ioeRequestItemId // request item id
      };
      if (item.itemIoeType === constants.OTHER_ITEM_FIELD_IOE_TYPE.Urgent) {
        handleOtherInfoCB(tempObj,basicInfo);
      } else if (item.itemIoeType === constants.OTHER_ITEM_FIELD_IOE_TYPE.RefNo) {
        handleOtherInfoIB(tempObj,basicInfo.clinicRefNo);
      } else if (item.itemIoeType === constants.OTHER_ITEM_FIELD_IOE_TYPE.Diagnosis) {
        handleOtherInfoIB(tempObj,basicInfo.infoDiagnosis);
      } else if (item.itemIoeType === constants.OTHER_ITEM_FIELD_IOE_TYPE.ReportTo) {
        handleOtherInfoDL(tempObj,basicInfo.reportTo);
      } else if (item.itemIoeType === constants.OTHER_ITEM_FIELD_IOE_TYPE.Remark&&item.itemName === constants.REMARK_ITEM_NAME) {
        let val = basicInfo.infoRemark;
        if (orderType!==constants.NORMAL_TOP_TABS[0].value&&basicInfo.infoRemark === '') {
          val = item.itemVal;
        }
        handleOtherInfoIB(tempObj,val);
      } else if (item.itemIoeType === constants.OTHER_ITEM_FIELD_IOE_TYPE.Instruction&&item.itemName === constants.INSTRUCTION_ITEM_NAME) {
        let val = basicInfo.infoInstruction;
        if (orderType!==constants.NORMAL_TOP_TABS[0].value&&basicInfo.infoInstruction === '') {
          val = item.itemVal;
        }
        handleOtherInfoIB(tempObj,val);
      }

      if (!isNull(tempObj.operationType)) {
        valMap.set(item.codeIoeFormItemId,tempObj);
      }
    }
  }
  return valMap;
};

let handleStorageObjOperation = (testItemsMap,specimenItemsMap,otherItemsMap,questionItemsMap) => {
  let operationTypes = [];
  let targetOperation = null;
  if (testItemsMap.size>0) {
    for (let valObj of testItemsMap.values()) {
      if (!isNull(valObj.operationType)) {
        operationTypes.push(valObj.operationType);
      }
    }
  }
  if (specimenItemsMap.size>0) {
    for (let valObj of specimenItemsMap.values()) {
      if (!isNull(valObj.operationType)) {
        operationTypes.push(valObj.operationType);
      }
    }
  }
  if (otherItemsMap.size>0) {
    for (let valObj of otherItemsMap.values()) {
      if (!isNull(valObj.operationType)) {
        operationTypes.push(valObj.operationType);
      }
    }
  }
  if (questionItemsMap.size>0) {
    for (let valObj of questionItemsMap.values()) {
      if (!isNull(valObj.operationType)) {
        operationTypes.push(valObj.operationType);
      }
    }
  }
  operationTypes = uniq(operationTypes);
  if (operationTypes.length > 1) {
    targetOperation = COMMON_ACTION_TYPE.UPDATE;
  } else {
    targetOperation = operationTypes[0];
  }

  return targetOperation;
};

export function initTemporaryStorageObj(middlewareObj,basicInfo,labId,orderType=constants.NORMAL_TOP_TABS[0].value) {
  let obj = {
    clinicCd: basicInfo.requestingUnit,
    codeIoeFormId: middlewareObj.codeIoeFormId,
    codeIoeRequestTypeCd: basicInfo.codeIoeRequestTypeCd,
    requestUser: basicInfo.requestedBy,
    encounterId: basicInfo.encounterId,
    patientKey: basicInfo.patientKey,
    serviceCd: basicInfo.serviceCd,
    createdBy: basicInfo.createdBy,
    createdDtm: basicInfo.createdDtm,
    updatedBy: basicInfo.updatedBy,
    updatedDtm: basicInfo.updatedDtm,
    ioeRequestId: basicInfo.ioeRequestId,
    ioeRequestNumber: basicInfo.ioeRequestNumber,
    requestDatetime: basicInfo.requestDatetime,
    version: basicInfo.version,
    operationType: null,

    invldReason:basicInfo.invldReason, // invalid
    isInvld: basicInfo.isInvld, // invalid
    ivgRqstSeqNum: basicInfo.ivgRqstSeqNum, // invalid
    outputFormPrinted: basicInfo.outputFormPrinted, // invalid
    outputFormPrintedBy: basicInfo.outputFormPrintedBy, // invalid
    outputFormPrintedDatetime: basicInfo.outputFormPrintedDatetime, // invalid
    specimenCollectDatetime: basicInfo.specimenCollectDatetime, // invalid
    specimenCollected: basicInfo.specimenCollected, // invalid
    specimenCollectedBy: basicInfo.specimenCollectedBy, // invalid
    specimenLabelPrinted: basicInfo.specimenLabelPrinted, // invalid
    specimenLabelPrintedBy: basicInfo.specimenLabelPrintedBy, // invalid
    specimenLabelPrintedDatetime: basicInfo.specimenLabelPrintedDatetime, // invalid

    labId,
    formShortName: middlewareObj.formShortName,
    testItemsMap: transformValMap2StorageMap(middlewareObj.testValMap),
    specimenItemsMap: transformValMap2StorageMap(middlewareObj.specimenValMap),
    otherItemsMap: transformInfoValMap2StorageMap(middlewareObj.otherValMap,basicInfo,orderType),
    questionItemsMap: transformValMap2StorageMap(middlewareObj.questionValMap)
  };
  obj.operationType = handleStorageObjOperation(obj.testItemsMap,obj.specimenItemsMap,obj.otherItemsMap,obj.questionItemsMap);

  return obj;
}

export function handleClickBoxOperationType(valObj) {
  let { version, isChecked } = valObj;
  if (!isNull(version)) {
    if (isChecked) {
      valObj.operationType = COMMON_ACTION_TYPE.UPDATE;
    } else {
      valObj.operationType = COMMON_ACTION_TYPE.DELETE;
    }
  } else if (isNull(version)&&isChecked) {
    valObj.operationType = COMMON_ACTION_TYPE.INSERT;
  } else {
    valObj.operationType = null;
  }
}

export function handleInputBoxOperationType(valObj) {
  let { version, itemVal, itemVal2 } = valObj;
  if (!isNull(version)) {
    valObj.operationType = COMMON_ACTION_TYPE.UPDATE;
  } else if (isNull(version)&&(!!itemVal || !!itemVal2)) {
    valObj.operationType = COMMON_ACTION_TYPE.INSERT;
  } else {
    valObj.operationType = null;
  }
}

export function handleDropdownOperationType(valObj) {
  let { version, itemVal, itemVal2 } = valObj;
  if (!isNull(version)) {
    valObj.operationType = COMMON_ACTION_TYPE.UPDATE;
  } else if (isNull(version)&&(!!itemVal || !!itemVal2)) {
    valObj.operationType = COMMON_ACTION_TYPE.INSERT;
  } else {
    valObj.operationType = null;
  }
}

export function handleInfoOperationType(valObj) {
  let { version, itemVal } = valObj;
  if (!isNull(version)) {
    if (!!itemVal) {
      valObj.operationType = COMMON_ACTION_TYPE.UPDATE;
      valObj.isChecked = true;
    } else if (itemVal === '') {
      valObj.operationType = COMMON_ACTION_TYPE.DELETE;
      valObj.isChecked = false;
    }
  } else if (isNull(version)&&(!!itemVal)) {
    valObj.operationType = COMMON_ACTION_TYPE.INSERT;
    valObj.isChecked = true;
  } else {
    valObj.operationType = null;
    valObj.isChecked = false;
  }
}

let setDeletedItemsMap = (itemsMap) => {
  if (itemsMap.size > 0) {
    for (let valObj of itemsMap.values()) {
      valObj.operationType = COMMON_ACTION_TYPE.DELETE;
    }
  }
  return itemsMap;
};

export function handledeletedStorageObj(deletedStorageObj) {
  let { testItemsMap, specimenItemsMap, otherItemsMap, questionItemsMap } = deletedStorageObj;
  deletedStorageObj.testItemsMap = setDeletedItemsMap(testItemsMap);
  deletedStorageObj.specimenItemsMap = setDeletedItemsMap(specimenItemsMap);
  deletedStorageObj.otherItemsMap = setDeletedItemsMap(otherItemsMap);
  deletedStorageObj.questionItemsMap = setDeletedItemsMap(questionItemsMap);
  deletedStorageObj.operationType = COMMON_ACTION_TYPE.DELETE;
  return deletedStorageObj;
}

export function handleValidateItems(middlewareObject) {
  let {testValMap,specimenValMap} = middlewareObject;
  let msgCode = '';
  //1.test
  if (testValMap.size > 0) {
    let checkedIoeTypeArray = [];
    for (let valObj of testValMap.values()) {
      if (valObj.isChecked) {
        checkedIoeTypeArray.push(valObj.itemIoeType);
      }
    }
    if (checkedIoeTypeArray.length === 0) {
      msgCode = IX_REQUEST_CODE.TEST_NOT_SELECTED;
    } else {
      checkedIoeTypeArray = union(checkedIoeTypeArray);
      if (checkedIoeTypeArray.length === 1) {
        let index = findIndex(checkedIoeTypeArray, type => {
          return type === constants.TEST_ITEM_IOE_TYPE.ITEF;
        });
        if (index!==-1) {
          msgCode = IX_REQUEST_CODE.ONE_MORE_TEST;
        }
      }
    }
  }
  //2.specimen
  if (msgCode === ''&&specimenValMap.size>0) {
    let checkedIoeTypeArray = [];
    for (let valObj of specimenValMap.values()) {
      if (valObj.isChecked) {
        checkedIoeTypeArray.push(valObj.itemIoeType);
      }
    }
    if (checkedIoeTypeArray.length === 0) {
      msgCode = IX_REQUEST_CODE.SPECIMEN_NOT_SELECTED;
    }
  }
  return msgCode;
}

export function handleValidateTemplateItems(middlewareMapObj) {
  let msgCode = '';
  if (!middlewareMapObj.templateSelectAll) {
    let checkedFlags = [];
    let validate = false;
    //validate order
    if (middlewareMapObj.middlewareMap.size>0) {
      for (let orderObj of middlewareMapObj.middlewareMap.values()) {
        if (orderObj.selectAll) {
          validate = true;
          break;
        } else {
          if (orderObj.specimenValMap.size>0) {
            // validate specimen (0 or 1)
            for (let specimenObj of orderObj.specimenValMap.values()) {
              checkedFlags.push(specimenObj.isChecked);
            }
          } else {
            // validate test (n)
            for (let testObj of orderObj.testValMap.values()) {
              checkedFlags.push(testObj.isChecked);
            }
          }
        }
      }
      if (!validate) {
        checkedFlags = uniq(checkedFlags);
        let index = findIndex(checkedFlags,flag => {
          return flag === true;
        });
        if (index === -1) {
          msgCode = IX_REQUEST_CODE.TEMPLATE_ITEM_NOT_SELECTED;
        }
      }
    }
  }

  return msgCode;
}

export function handleQuestionItem(id,valMap,idSet) {
  if (idSet.size !== 0) {
    for (let itemId of idSet.keys()) {
      if (id !== itemId) {
        let tempValObj = valMap.get(itemId);
        tempValObj.isChecked = false;
        handleClickBoxOperationType(tempValObj);
      }
    }
  }
}

export function handleSepcimenItem(id,valMap) {
  for (let [itemId, valObj] of valMap) {
    if (itemId!==id) {
      valObj.isChecked = false;
      handleClickBoxOperationType(valObj);
    }
  }
}

export function handleTestItem(id,valMap,masterTestMap,selectedLabId) {
  let currentValObj = valMap.get(id);
  let currentItemIoeType = currentValObj.itemIoeType;
  let checkedFlag = currentValObj.isChecked;
  // handle comment test
  if (checkedFlag) {
    switch (currentItemIoeType) {
      case constants.TEST_ITEM_IOE_TYPE.ITEO:{
        for (let itemValObj of valMap.values()) {
          if (itemValObj.itemIoeType === constants.TEST_ITEM_IOE_TYPE.ITE) {
            itemValObj.isChecked = false;
            handleClickBoxOperationType(itemValObj);
          }
        }
        break;
      }
      case constants.TEST_ITEM_IOE_TYPE.ITE:{
        for (let itemValObj of valMap.values()) {
          if (itemValObj.itemIoeType === constants.TEST_ITEM_IOE_TYPE.ITEO) {
            itemValObj.isChecked = false;
            handleClickBoxOperationType(itemValObj);
          }
        }
        break;
      }
      default:
        break;
    }
  }
  // handle master test
  if (selectedLabId === constants.LAB_ID.CPLC) {
    // check master item id
    if (masterTestMap.has(id)) {
      let subItemIds = masterTestMap.get(id);
      for (let [itemId, itemValObj] of valMap) {
        let index = findIndex(subItemIds,subItemId => {
          return subItemId === itemId;
        });
        if (index !== -1) {
          itemValObj.isChecked = checkedFlag;
          handleClickBoxOperationType(itemValObj);
        }
      }
    } else {
      // check sub item id
      let targetMasterId = null;
      let targetMasterSubIds = [];
      for (let [masterId, subItemIds] of masterTestMap) {
        let index = findIndex(subItemIds,subItemId => {
          return subItemId === id;
        });
        if (index !== -1) {
          targetMasterId = masterId;
          targetMasterSubIds = subItemIds;
          break;
        }
      }
      if (!isNull(targetMasterId)&&targetMasterSubIds.length > 0) {
        let tempArray = [checkedFlag];
        targetMasterSubIds.forEach(subItemId => {
          tempArray.push(valMap.get(subItemId).isChecked);
        });
        tempArray = union(tempArray);
        if (tempArray.length === 1) {
          valMap.get(targetMasterId).isChecked = tempArray[0];
          handleClickBoxOperationType(valMap.get(targetMasterId));
        } else {
          valMap.get(targetMasterId).isChecked = false;
          handleClickBoxOperationType(valMap.get(targetMasterId));
        }
      }
    }
  }
}

export function resetQuestionStatus(questionValMap) {
  for (let obj of questionValMap.values()) {
    obj.operationType = null;
    obj.version = null;
    obj.itemVal = null;
    obj.itemVal2 = null;
    // obj.codeIoeFormId = null;
    obj.isChecked = false;
  }
}

export function resetQuestionGroupStatus(questionGroupMap) {
  for (let obj of questionGroupMap.values()) {
    obj.isError = false;
  }
}

export function handleOtherInfoDialogDisplay(middlewareObject,itemMapping) {
  let displayFlag = true;
  if (middlewareObject.questionValMap.size !== 0) {
    let valMap = middlewareObject.questionValMap;
    let onlyIQS = true;
    for (let question of valMap.values()) {
      if (question.itemIoeType !== constants.ITEM_QUESTION_TYPE.IQS) {
        onlyIQS = false;
      }
    }
    if (onlyIQS) {
      // only exist IQS
      let { codeIoeFormId,specimenValMap } = middlewareObject;
      if (itemMapping.has(codeIoeFormId)) {
        let itemMap = itemMapping.get(codeIoeFormId);
        let specimenObj = null;
        for (let itemObj of specimenValMap.values()) {
          if (itemObj.isChecked) {
            specimenObj = itemObj;
            break;
          }
        }
        if (!!specimenObj) {
          for (let speciemnIdSet of itemMap.values()) {
            if (!speciemnIdSet.has(specimenObj.codeIoeFormItemId)) {
              displayFlag = false;
              break;
            }
          }
        }
      }
    }
  } else {
    displayFlag = false;
  }
  return displayFlag;
}

export function getBasicInfo(valMap) {
  let tempObj = {
    urgentIsChecked: false,
    reportTo:'',
    clinicRefNo:'',
    infoDiagnosis:'',
    infoRemark:'',
    infoInstruction:''
  };
  for (let valObj of valMap.values()) {
    if (constants.OTHER_ITEM_FIELD_IOE_TYPE.Urgent === valObj.itemIoeType) {
      tempObj.urgentIsChecked = valObj.isChecked;
    } else if (constants.OTHER_ITEM_FIELD_IOE_TYPE.RefNo === valObj.itemIoeType) {
      tempObj.clinicRefNo = valObj.itemVal||'';
    } else if (constants.OTHER_ITEM_FIELD_IOE_TYPE.ReportTo === valObj.itemIoeType) {
      tempObj.reportTo = valObj.itemVal;
    } else if (constants.OTHER_ITEM_FIELD_IOE_TYPE.Diagnosis === valObj.itemIoeType) {
      tempObj.infoDiagnosis = valObj.itemVal||'';
    } else if (constants.OTHER_ITEM_FIELD_IOE_TYPE.Remark === valObj.itemIoeType&&constants.REMARK_ITEM_NAME === valObj.itemName) {
      tempObj.infoRemark = valObj.itemVal||'';
    } else if (constants.OTHER_ITEM_FIELD_IOE_TYPE.Instruction === valObj.itemIoeType&&constants.INSTRUCTION_ITEM_NAME === valObj.itemName) {
      tempObj.infoInstruction = valObj.itemVal||'';
    }
  }
  return tempObj;
}

export function isDisableSaving(temporaryStorageMap,deletedStorageMap) {
  let allowSaveFlag = false;
  if (temporaryStorageMap.size>0) {
    for (let valObj of temporaryStorageMap.values()) {
      if (!isNull(valObj.operationType)) {
        allowSaveFlag = true;
        break;
      }
    }
  }
  if (deletedStorageMap.size>0) {
    for (let valObj of deletedStorageMap.values()) {
      if (!isNull(valObj.operationType)) {
        allowSaveFlag = true;
        break;
      }
    }
  }
  return allowSaveFlag;
}

// transform value when loading
export function transformItemsObj(ixRequestItemMap,formId) {
  let tempObj = {
    testItemsMap: new Map(),
    specimenItemsMap: new Map(),
    otherItemsMap: new Map(),
    questionItemsMap: new Map()
  };
  if (ixRequestItemMap[formId]!==undefined) {
    let orderItems = ixRequestItemMap[formId];
    if (orderItems.length>0) {
      let tempMap = new Map();
      orderItems.forEach(orderItem => {
        if (constants.ITEM_CATEGORY_IOE_TYPE.TEST.has(orderItem.itemIoeType)) {
          // 1. Test
          tempMap = tempObj.testItemsMap;
        } else if (constants.ITEM_CATEGORY_IOE_TYPE.SPECIMEN.has(orderItem.itemIoeType)) {
          // 2. Specimen
          tempMap = tempObj.specimenItemsMap;
        } else if (orderItem.itemIoeType === constants.ITEM_CATEGORY_IOE_TYPE.OTHER.Urgent) {
          // 3. Other urgent
          tempMap = tempObj.otherItemsMap;
        } else if (orderItem.itemIoeType === constants.ITEM_CATEGORY_IOE_TYPE.OTHER.RefNo) {
          // 4. Other ref.no
          tempMap = tempObj.otherItemsMap;
        } else if (orderItem.itemIoeType === constants.ITEM_CATEGORY_IOE_TYPE.OTHER.Diagnosis) {
          // 5. Other diagnosis
          tempMap = tempObj.otherItemsMap;
        } else if (orderItem.itemIoeType === constants.ITEM_CATEGORY_IOE_TYPE.OTHER.ReportTo) {
          // 6. Other report to
          tempMap = tempObj.otherItemsMap;
        } else if (orderItem.itemIoeType === constants.ITEM_CATEGORY_IOE_TYPE.OTHER.Remark&&orderItem.itemName === constants.REMARK_ITEM_NAME) {
          // 7. Other remark
          tempMap = tempObj.otherItemsMap;
        } else if (orderItem.itemIoeType === constants.ITEM_CATEGORY_IOE_TYPE.OTHER.Instruction&&orderItem.itemName === constants.INSTRUCTION_ITEM_NAME) {
          // 8. Other remark
          tempMap = tempObj.otherItemsMap;
        } else if (constants.ITEM_CATEGORY_IOE_TYPE.QUESTION.has(orderItem.itemIoeType)) {
          // 9. Question
          tempMap = tempObj.questionItemsMap;
        } else if (orderItem.itemIoeType === constants.ITEM_CATEGORY_IOE_TYPE.OPTION && orderItem.itemName !== constants.REMARK_ITEM_NAME && orderItem.itemName !== constants.INSTRUCTION_ITEM_NAME) {
          // 10. Question optional
          tempMap = tempObj.questionItemsMap;
        }
        tempMap.set(orderItem.codeIoeFormItemId,{
          codeIoeFormId: orderItem.codeIoeFormId,
          codeIoeFormItemId:orderItem.codeIoeFormItemId,
          operationType: orderItem.operationType,
          version: orderItem.version,
          itemVal: orderItem.itemIoeType!==constants.OTHER_ITEM_FIELD_IOE_TYPE.ReportTo&&orderItem.frmItemTypeCd === constants.FORM_ITEM_TYPE.DROP_DOWN_LIST&&!!orderItem.itemVal?toInteger(orderItem.itemVal):orderItem.itemVal,
          itemVal2: orderItem.itemIoeType!==constants.OTHER_ITEM_FIELD_IOE_TYPE.ReportTo&&orderItem.frmItemTypeCd2 === constants.FORM_ITEM_TYPE.DROP_DOWN_LIST&&!!orderItem.itemVal2?toInteger(orderItem.itemVal2):orderItem.itemVal2,
          itemName: orderItem.itemName,
          createdBy: orderItem.createdBy,
          createdDtm: orderItem.createdDtm,
          updatedBy: orderItem.updatedBy,
          updatedDtm: orderItem.updatedDtm,
          itemIoeType: orderItem.itemIoeType,
          frmItemTypeCd: orderItem.frmItemTypeCd,
          frmItemTypeCd2: orderItem.frmItemTypeCd2,
          ioeRequestId: orderItem.ioeRequestId, // request id
          ioeRequestItemId: orderItem.ioeRequestItemId // request item id
        });
      });
    }
  }
  return tempObj;
}

// for creating template order obj
let transformTemplateItems = (valObjs) => {
  let tempObj = {
    testItemsMap: new Map(),
    specimenItemsMap: new Map(),
    otherItemsMap: new Map(),
    questionItemsMap: new Map()
  };
  if (valObjs.length>0) {
    let tempMap = new Map();
    valObjs.forEach(orderItem => {
      if (constants.ITEM_CATEGORY_IOE_TYPE.TEST.has(orderItem.itemIoeType)) {
        // 1. Test
        tempMap = tempObj.testItemsMap;
      } else if (constants.ITEM_CATEGORY_IOE_TYPE.SPECIMEN.has(orderItem.itemIoeType)) {
        // 2. Specimen
        tempMap = tempObj.specimenItemsMap;
      } else if (orderItem.itemIoeType === constants.ITEM_CATEGORY_IOE_TYPE.OTHER.Urgent) {
        // 3. Other urgent
        tempMap = tempObj.otherItemsMap;
      } else if (orderItem.itemIoeType === constants.ITEM_CATEGORY_IOE_TYPE.OTHER.RefNo) {
        // 4. Other ref.no
        tempMap = tempObj.otherItemsMap;
      } else if (orderItem.itemIoeType === constants.ITEM_CATEGORY_IOE_TYPE.OTHER.Diagnosis) {
        // 5. Other diagnosis
        tempMap = tempObj.otherItemsMap;
      } else if (orderItem.itemIoeType === constants.ITEM_CATEGORY_IOE_TYPE.OTHER.ReportTo) {
        // 6. Other report to
        tempMap = tempObj.otherItemsMap;
      } else if (orderItem.itemIoeType === constants.ITEM_CATEGORY_IOE_TYPE.OTHER.Remark&&orderItem.itemName === constants.REMARK_ITEM_NAME) {
        // 7. Other remark
        tempMap = tempObj.otherItemsMap;
      } else if (orderItem.itemIoeType === constants.ITEM_CATEGORY_IOE_TYPE.OTHER.Instruction&&orderItem.itemName === constants.INSTRUCTION_ITEM_NAME) {
        // 8. Other remark
        tempMap = tempObj.otherItemsMap;
      } else if (constants.ITEM_CATEGORY_IOE_TYPE.QUESTION.has(orderItem.itemIoeType)) {
        // 9. Question
        tempMap = tempObj.questionItemsMap;
      } else if (orderItem.itemIoeType === constants.ITEM_CATEGORY_IOE_TYPE.OPTION && orderItem.itemName !== constants.REMARK_ITEM_NAME && orderItem.itemName !== constants.INSTRUCTION_ITEM_NAME) {
        // 10. Question optional
        tempMap = tempObj.questionItemsMap;
      }
      tempMap.set(orderItem.codeIoeFormItemId,{
        codeIoeFormId: orderItem.codeIoeFormId,
        codeIoeFormItemId:orderItem.codeIoeFormItemId,
        operationType: null,
        version: null,
        itemVal: orderItem.itemIoeType!==constants.OTHER_ITEM_FIELD_IOE_TYPE.ReportTo&&orderItem.frmItemTypeCd === constants.FORM_ITEM_TYPE.DROP_DOWN_LIST&&!!orderItem.itemVal?toInteger(orderItem.itemVal):orderItem.itemVal,
        itemVal2: orderItem.itemIoeType!==constants.OTHER_ITEM_FIELD_IOE_TYPE.ReportTo&&orderItem.frmItemTypeCd2 === constants.FORM_ITEM_TYPE.DROP_DOWN_LIST&&!!orderItem.itemVal2?toInteger(orderItem.itemVal2):orderItem.itemVal2,
        itemName: orderItem.itemName,
        createdBy: null,
        createdDtm: null,
        updatedBy: null,
        updatedDtm: null,
        itemIoeType: orderItem.itemIoeType,
        frmItemTypeCd: orderItem.frmItemTypeCd,
        frmItemTypeCd2: orderItem.frmItemTypeCd2,
        ioeRequestId: null, // request id
        ioeRequestItemId: null // request item id
      });
    });
  }
  return tempObj;
};

// transform template value to storageObj
export function transformTemplateOrderItemsObj(ioeTestTmplItemMap,ioeFormMap) {
  let tempMap = new Map();
  if (!isEmpty(ioeTestTmplItemMap)) {
    for (const testGroup in ioeTestTmplItemMap) {
      if (Object.prototype.hasOwnProperty.call(ioeTestTmplItemMap,testGroup)) {
        let currentOrderObj = ioeTestTmplItemMap[testGroup];
        let currentFormIds = keys(currentOrderObj);
        let formId = parseInt(currentFormIds[0]);
        let formObj = ioeFormMap.get(formId);
        let tempMapObj = transformTemplateItems(currentOrderObj[formId]);
        let tempStorageObj = {
          clinicCd: null,
          codeIoeFormId: formId,
          codeIoeRequestTypeCd: null,
          requestUser: null,
          encounterId: null,
          patientKey: null,
          serviceCd: null,

          createdBy: null,
          createdDtm: null,
          updatedBy: null,
          updatedDtm: null,
          ioeRequestId: 0,
          ioeRequestNumber: null,
          requestDatetime: null,
          version: null,
          operationType: null,

          invldReason:null, // invalid
          isInvld: 0, // invalid
          ivgRqstSeqNum: 0, // invalid
          outputFormPrinted: 0, // invalid
          outputFormPrintedBy: null, // invalid
          outputFormPrintedDatetime: null, // invalid
          specimenCollectDatetime: null, // invalid
          specimenCollected: 0, // invalid
          specimenCollectedBy: null, // invalid
          specimenLabelPrinted: 0, // invalid
          specimenLabelPrintedBy: null, // invalid
          specimenLabelPrintedDatetime: null, // invalid

          labId:formObj.labId,
          formShortName: formObj.formShortName,
          testItemsMap: tempMapObj.testItemsMap,
          specimenItemsMap: tempMapObj.specimenItemsMap,
          otherItemsMap: tempMapObj.otherItemsMap,
          questionItemsMap: tempMapObj.questionItemsMap
        };
        tempMap.set(`${formId}_${testGroup}_${Math.random()}`,tempStorageObj);
      }
    }
  }
  return tempMap;
}

let handleTemplateValMapCheck = (value,valMap) => {
  if (valMap.size>0) {
    for (let valObj of valMap.values()) {
      valObj.isChecked = value;
      handleClickBoxOperationType(valObj);
    }
  }
};

// handle template cb
export function handleTemplateTabClickEffect(value,level,middlewareMapObj,orderKey,specimentId,testId) {
  switch (level) {
    case constants.IX_REQUEST_TEMPLATE_CB.LEVEL_1:{
      middlewareMapObj.templateSelectAll = value;
      if (middlewareMapObj.middlewareMap.size>0) {
        for (let valObj of middlewareMapObj.middlewareMap.values()) {
          valObj.selectAll = value;
          handleTemplateValMapCheck(value,valObj.testValMap);
          handleTemplateValMapCheck(value,valObj.specimenValMap);
        }
      }
      break;
    }
    case constants.IX_REQUEST_TEMPLATE_CB.LEVEL_2:{
      let middlewareObject = middlewareMapObj.middlewareMap.get(orderKey);
      middlewareObject.selectAll = value;
      handleTemplateValMapCheck(value,middlewareObject.testValMap);
      handleTemplateValMapCheck(value,middlewareObject.specimenValMap);
      // handle level 1
      let level2 = [];
      for (let valObj of middlewareMapObj.middlewareMap.values()) {
        level2.push(valObj.selectAll);
      }
      level2 = uniq(level2);
      if (level2.length === 1) {
        middlewareMapObj.templateSelectAll = level2[0];
      } else {
        middlewareMapObj.templateSelectAll = false;
      }
      break;
    }
    case constants.IX_REQUEST_TEMPLATE_CB.LEVEL_3:{
      let middlewareObject = middlewareMapObj.middlewareMap.get(orderKey);
      //handle specimen
      let specimenCb = [];
      if (middlewareObject.specimenValMap.size>0) {
        for (let valObj of middlewareObject.specimenValMap.values()) {
          if (valObj.codeIoeFormItemId === specimentId) {
            valObj.isChecked = event.target.checked;
            handleClickBoxOperationType(valObj);
          }
          specimenCb.push(valObj.isChecked);
        }
      }
      //handle test
      let testCb = [];
      if (middlewareObject.testValMap.size>0) {
        for (let valObj of middlewareObject.testValMap.values()) {
          if (valObj.codeIoeFormItemId === testId) {
            valObj.isChecked = event.target.checked;
            handleClickBoxOperationType(valObj);
          }
          testCb.push(valObj.isChecked);
        }
      }

      // handle level 2
      let total = union(specimenCb,testCb);
      if (total.length === 1) {
        middlewareObject.selectAll = total[0];
      } else {
        middlewareObject.selectAll = false;
      }

      // handle level 1
      let level2 = [];
      for (let valObj of middlewareMapObj.middlewareMap.values()) {
        level2.push(valObj.selectAll);
      }
      level2 = uniq(level2);
      if (level2.length === 1) {
        middlewareMapObj.templateSelectAll = level2[0];
      } else {
        middlewareMapObj.templateSelectAll = false;
      }
      break;
    }
    default:
      break;
  }
}

// search the template order with questions
export function handleTemplateOrderWithQuestions(middlewareMapObj,itemMapping) {
  let { middlewareMap } = middlewareMapObj;
  let templateOrderTotalKeys =[];
  let templateOrderWithQuestionKeys = [];

  for (let [orderKey, valObj] of middlewareMap) {
    let selectedFlag = false;
    if (valObj.specimenValMap.size>0) {
      // validate specimen (0 or 1)
      for (let specimenObj of valObj.specimenValMap.values()) {
        if (specimenObj.isChecked) {
          selectedFlag = true;
          break;
        }
      }
    } else {
      // validate test (n)
      for (let testObj of valObj.testValMap.values()) {
        if (testObj.isChecked) {
          selectedFlag = true;
          break;
        }
      }
    }
    if (selectedFlag) {
      let displayFlag = handleOtherInfoDialogDisplay(valObj,itemMapping);
      if (displayFlag) {
        templateOrderWithQuestionKeys.push(orderKey);
      }
      templateOrderTotalKeys.push(orderKey);
    }
  }

  return {
    templateOrderTotalKeys,
    templateOrderWithQuestionKeys
  };
}

export function transformStorageInfo(targetObj,storageObj) {
  targetObj.createdBy = storageObj.createdBy;
  targetObj.createdDtm = storageObj.createdDtm;
  targetObj.updatedBy = storageObj.updatedBy;
  targetObj.updatedDtm = storageObj.updatedDtm;
  targetObj.ioeRequestId = storageObj.ioeRequestId;
  targetObj.ioeRequestNumber = storageObj.ioeRequestNumber;
  targetObj.requestDatetime = storageObj.requestDatetime;
  targetObj.version = storageObj.version;
  targetObj.invldReason = storageObj.invldReason;
  targetObj.isInvld = storageObj.isInvld;
  targetObj.ivgRqstSeqNum = storageObj.ivgRqstSeqNum;
  targetObj.outputFormPrinted = storageObj.outputFormPrinted;
  targetObj.outputFormPrintedBy = storageObj.outputFormPrintedBy;
  targetObj.outputFormPrintedDatetime = storageObj.outputFormPrintedDatetime;
  targetObj.specimenCollectDatetime = storageObj.specimenCollectDatetime;
  targetObj.specimenCollected = storageObj.specimenCollected;
  targetObj.specimenCollectedBy = storageObj.specimenCollectedBy;
  targetObj.specimenLabelPrinted = storageObj.specimenLabelPrinted;
  targetObj.specimenLabelPrintedBy = storageObj.specimenLabelPrintedBy;
  targetObj.specimenLabelPrintedDatetime = storageObj.specimenLabelPrintedDatetime;
}

export function generateDropdownValue(dropdownMap,valObj,type) {
  let options = dropdownMap.get(valObj.codeIoeFormItemId).get(type);
  let targetOption = find(options,option=>{
    let tempVal = type === constants.ITEM_VALUE.TYPE1?valObj.itemVal:valObj.itemVal2;
    return option.codeIoeFormItemDropId === tempVal;
  });
  return !!targetOption?targetOption.value:'';
}