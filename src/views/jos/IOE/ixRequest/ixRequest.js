/*
 * Front-end UI for save/update IOE Ix Request
 * Load Ix Request Item Data Action: [ixRequest.js] componentDidMount -> getIxRequestFrameworkList
 * -> [ixRequestAction.js] getIxRequestFrameworkList
 * -> [ixRequestSaga.js] getIxRequestFrameworkList
 * -> Backend API = /ioe/ixRequest/forms
 * Load Ix Request Dropdown Item options Action: [ixRequest.js] componentDidMount -> getIxRequestItemDropdownList
 * -> [ixRequestAction.js] getIxRequestItemDropdownList
 * -> [ixRequestSaga.js] getIxRequestItemDropdownList
 * -> Backend API = /ioe/loadCodeIoeFormItemDrops
 * Load Ix Request Order List Action: [ixRequest.js] componentDidMount -> getIxRequestOrderList
 * -> [ixRequestAction.js] getIxRequestOrderList
 * -> [ixRequestSaga.js] getIxRequestOrderList
 * -> Backend API = /ioe/ixRequest/orders/${patientKey}/${encounterId}
 * Load Ix Request Item Mapping Action: [ixRequest.js] componentDidMount -> getIxRequestSpecificMapping
 * -> [ixRequestAction.js] getIxRequestSpecificMapping
 * -> [ixRequestSaga.js] getIxRequestSpecificMapping
 * -> Backend API = /ioe/ixRequest/specificItemMapping
 * Load Ix Profile Template Action: [ixRequest.js] componentDidMount -> getAllIxProfileTemplate
 * -> [ixRequestAction.js] getAllIxProfileTemplate
 * -> [ixRequestSaga.js] getAllIxProfileTemplate
 * -> Backend API = /ioe/ixRequest/templates
 * Load IOE Form Item(test & specimen) Action: [ixRequest.js] componentDidMount -> getIxAllItemsForSearch
 * -> [ixRequestAction.js] getIxAllItemsForSearch
 * -> [ixRequestSaga.js] getIxAllItemsForSearch
 * -> Backend API = /ioe/loadAllItemsForSearch
 * Save Action: [ixRequest.js] Save -> handleSave
 * -> [ixRequestAction.js] saveIxRequestOrder
 * -> [ixRequestSaga.js] saveIxRequestOrder
 * -> Backend API = /ioe/ixRequest/operation/${operationType}
 * operationType:
 * 1.save:data
 * 2.save and print reminder:dataReminder
 * 3.save print label or output form:dataLabelOrForm
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles, Typography, Card, CardContent, Grid } from '@material-ui/core';
import { styles } from './ixRequestStyle';
import { getIxClinicList,getIxRequestItemDropdownList,getIxRequestFrameworkList,getIxRequestSpecificMapping,saveIxRequestOrder,getIxRequestOrderList,getIxAllItemsForSearch,getAllIxProfileTemplate } from '../../../../store/actions/IOE/ixRequest/ixRequestAction';
import { openCommonCircularDialog,closeCommonCircularDialog } from '../../../../store/actions/common/commonAction';
import { openCommonMessage } from '../../../../store/actions/message/messageAction';
import { deleteSubTabs } from '../../../../store/actions/mainFrame/mainFrameAction';
import BasicInfo from './modules/BasicInfo/BasicInfo';
import ValidatorForm from '../../../../components/FormValidator/ValidatorForm';
import ContentContainer from './modules/ContentContainer/ContentContainer';
import * as utils from './utils/ixUtils';
import { includes,toUpper,concat, isNull } from 'lodash';
import Enum from '../../../../enums/enum';
import * as constants from '../../../../constants/IOE/ixRequest/ixRequestConstants';
import { COMMON_ACTION_TYPE } from '../../../../constants/common/commonConstants';
import Container from 'components/JContainer';

class ixRequest extends Component {
  constructor(props){
    super(props);
    let { loginInfo,encounterData,loginServiceCd } = props;
    let userRole = loginInfo.userRoleType;
    this.topTabs = []; // default
    this.codeIoeRequestTypeCd = null;
    this.defaultOrderType = null;
    if (includes(toUpper(userRole),Enum.USER_ROLE_TYPE.DOCTOR)) { //D
      this.topTabs = concat(this.topTabs,constants.NORMAL_TOP_TABS);
      this.codeIoeRequestTypeCd = constants.IOE_REQUEST_TYPE.DOCTOR;
      this.defaultOrderType = constants.NORMAL_TOP_TABS[0].value;
    } else if (includes(toUpper(userRole),Enum.USER_ROLE_TYPE.NURSE)) { //N
      this.topTabs = concat(this.topTabs,constants.NURSE_TOP_TABS);
      this.codeIoeRequestTypeCd = constants.IOE_REQUEST_TYPE.NURSE;
      this.defaultOrderType = constants.NURSE_TOP_TABS[0].value;
    }
    this.state={
      frameworkMap: new Map(),
      lab2FormMap: new Map(),
      ioeFormMap: new Map(),
      categoryMap: new Map(),
      isEdit: false,
      temporaryStorageMap: new Map(),
      deletedStorageMap: new Map(),
      middlewareObject: {},
      middlewareMapObj: new Map(),
      orderIsEdit:false,
      selectedOrderKey:null,
      diagnosisErrorFlag: false,
      basicInfo:{
        infoOrderType: this.defaultOrderType, // category of Info dialog (Discipline,Service,Personal,Nurse)
        orderType: this.defaultOrderType, // category of Top tab (Discipline,Service,Personal,Nurse)
        codeIoeRequestTypeCd: this.codeIoeRequestTypeCd,
        encounterId: encounterData.encounterId,
        patientKey: encounterData.patientKey,
        serviceCd: loginServiceCd,
        createdBy: null,
        createdDtm: null,
        updatedBy: null,
        updatedDtm: null,
        ioeRequestId: 0,
        ioeRequestNumber: null,
        requestDatetime: null,
        version:null,

        invldReason:null, // invalid
        isInvld: 0, // invalid
        ivgRqstSeqNum: 0, // invalid
        outputFormPrinted: 0, // invalid
        outputFormPrintedBy: null, // invalid
        outputFormPrintedDatetime: null, // invalid
        specimenCollectDatetime: null, // invalid
        specimenCollected: 0, // invalid
        specimenCollectedBy: null, // invalid
        specimenLabelPrinted: 0, // invalid
        specimenLabelPrintedBy: null, // invalid
        specimenLabelPrintedDatetime: null, // invalid

        urgentIsChecked: false,
        requestedBy:loginInfo.loginName,  //requestUser
        requestingUnit:encounterData.clinicCd||'',  //clinicCd
        reportTo:encounterData.clinicCd||'',
        clinicRefNo:'',
        infoDiagnosis:'',
        infoRemark:'',
        infoInstruction:''
      },
      contentVals: {
        labId: null,
        selectedSubTabId: null,
        infoTargetLabId: null,
        infoTargetFormId: null
      }
    };
  }

  componentDidMount(){
    const { loginServiceCd,encounterData } = this.props;
    this.props.ensureDidMount();
    this.props.openCommonCircularDialog();
    this.props.getIxAllItemsForSearch({});
    this.props.getIxRequestSpecificMapping({});
    this.props.getIxClinicList({
      params:{serviceCd:loginServiceCd}
    });
    this.props.getIxRequestItemDropdownList({});
    this.props.getIxRequestFrameworkList({
      params:{},
      callback: data => {
        this.initDefaultFramework(data);
        this.props.getAllIxProfileTemplate({
          params:{},
          callback:data=>{
            if (this.defaultOrderType === constants.NURSE_TOP_TABS[0].value) {
              this.initNurseFramework(data,this.defaultOrderType);
            }
            this.setState({
              categoryMap: data
            });
          }
        });
        this.props.getIxRequestOrderList({
          params:{
            patientKey: encounterData.patientKey,
            encounterId: encounterData.encounterId
          },
          callback:data=>{
            this.setState({
              temporaryStorageMap: data
            });
            this.props.closeCommonCircularDialog();
          }
        });
      }
    });
  }

  initNurseFramework = (categoryMap,orderType) => {
    let { frameworkMap } = this.state;
    let categoryObj = categoryMap.has(orderType)?categoryMap.get(orderType):null;
    let defaultTemplateId = null;
    let defaultMiddlewareMap = new Map();
    if (!!categoryObj&&categoryObj.templateMap.size>0) {
      for (let templateId of categoryObj.templateMap.keys()) {
        defaultTemplateId = templateId;
        break;
      }
      let defaultTemplateObj = categoryObj.templateMap.get(defaultTemplateId);
      // template has order list
      if (defaultTemplateObj.storageMap.size>0) {
        for (let [storageKey,storageObj] of defaultTemplateObj.storageMap) {
          // init middlewareObject
          let formObj = frameworkMap.has(storageObj.labId)?frameworkMap.get(storageObj.labId).formMap.get(storageObj.codeIoeFormId):null;
          let middlewareObject = utils.initMiddlewareObject(formObj,storageObj,true,orderType);
          defaultMiddlewareMap.set(storageKey,middlewareObject);
        }
      }
    }

    this.setState({
      middlewareMapObj:{
        templateId:defaultTemplateId,
        templateSelectAll: false,
        middlewareMap:defaultMiddlewareMap
      },
      contentVals:{
        labId:null,
        selectedSubTabId: defaultTemplateId,
        infoTargetLabId: null,
        infoTargetFormId: defaultTemplateId
      }
    });
  }

  resetDiscipline = (lab2FormMap) => {
    let defaultTabValue = null;
    let defaultSubTabValue = null;
    if (lab2FormMap.size > 0) {
      for (let [labId, formIds] of lab2FormMap) {
        defaultTabValue = labId;
        defaultSubTabValue = formIds[0];
        break;
      }
    }
    return {
      labId: defaultTabValue,
      selectedSubTabId: defaultSubTabValue,
      infoTargetLabId: defaultTabValue,
      infoTargetFormId: defaultSubTabValue
    };
  }

  initDefaultFramework = data => {
    let defaultObj = this.resetDiscipline(data.lab2FormMap);
    let formObj = data.frameworkMap.has(defaultObj.labId)?data.frameworkMap.get(defaultObj.labId).formMap.get(defaultObj.selectedSubTabId):null;
    let valObj = utils.initMiddlewareObject(formObj);
    this.setState({
      frameworkMap: data.frameworkMap,
      lab2FormMap: data.lab2FormMap,
      ioeFormMap: data.ioeFormMap,
      middlewareObject: valObj,
      contentVals: {
        ...defaultObj
      }
    });
  }

  generateItemList = (itemList,itemMap) => {
    if (itemMap.size>0) {
      for (let itemValObj of itemMap.values()) {
        if ((isNull(itemValObj.version)&&
          (itemValObj.operationType === COMMON_ACTION_TYPE.UPDATE||itemValObj.operationType === COMMON_ACTION_TYPE.DELETE))||
          (isNull(itemValObj.operationType))) {
          continue;
        } else {
          itemList.push({
            codeIoeFormId: itemValObj.codeIoeFormId,
            codeIoeFormItemId: itemValObj.codeIoeFormItemId,
            createdBy: itemValObj.createdBy,
            createdDtm: itemValObj.createdDtm,
            ioeRequestId: itemValObj.ioeRequestId,
            ioeRequestItemId: itemValObj.ioeRequestItemId,
            itemIoeType: itemValObj.itemIoeType,
            itemVal: itemValObj.itemVal,
            itemVal2: itemValObj.itemVal2,
            operationType: itemValObj.operationType,
            updatedBy: itemValObj.updatedBy,
            updatedDtm: itemValObj.updatedDtm,
            version: itemValObj.version
          });
        }
      }
    }
  }

  generateOrderObj = (valObj) => {
    let tempObj = {
      clinicCd: valObj.clinicCd,
      codeIoeFormId: valObj.codeIoeFormId,
      codeIoeRequestTypeCd: valObj.codeIoeRequestTypeCd,
      createdBy: valObj.createdBy,
      createdDtm: valObj.createdDtm,
      encounterId: valObj.encounterId,
      invldReason: valObj.invldReason,
      ioeRequestId: valObj.ioeRequestId,
      ioeRequestNumber: valObj.ioeRequestNumber,
      isInvld: valObj.isInvld,
      ivgRqstSeqNum: valObj.ivgRqstSeqNum,
      outputFormPrinted: valObj.outputFormPrinted,
      outputFormPrintedBy: valObj.outputFormPrintedBy,
      outputFormPrintedDatetime: valObj.outputFormPrintedDatetime,
      patientKey: valObj.patientKey,
      requestDatetime: valObj.requestDatetime,
      requestUser: valObj.requestUser,
      serviceCd: valObj.serviceCd,
      specimenCollectDatetime: valObj.specimenCollectDatetime,
      specimenCollected: valObj.specimenCollected,
      specimenCollectedBy: valObj.specimenCollectedBy,
      specimenLabelPrinted: valObj.specimenLabelPrinted,
      specimenLabelPrintedBy: valObj.specimenLabelPrintedBy,
      specimenLabelPrintedDatetime: valObj.specimenLabelPrintedDatetime,
      updatedBy: valObj.updatedBy,
      updatedDtm: valObj.updatedDtm,
      version: valObj.version,
      ixRequestItemList:[]
    };
    let { testItemsMap, specimenItemsMap, otherItemsMap, questionItemsMap } = valObj;
    this.generateItemList(tempObj.ixRequestItemList,testItemsMap);
    this.generateItemList(tempObj.ixRequestItemList,specimenItemsMap);
    this.generateItemList(tempObj.ixRequestItemList,otherItemsMap);
    this.generateItemList(tempObj.ixRequestItemList,questionItemsMap);
    tempObj.operationType = valObj.operationType;
    return tempObj;
  }

  generateResultObj = saveType => {
    let {
      temporaryStorageMap,
      deletedStorageMap
    } = this.state;

    let innerSaveIxRequestDtos = [];
    //handle delete
    if (deletedStorageMap.size > 0) {
      for (let valWrapperObj of deletedStorageMap.values()) {
        let tempObj = this.generateOrderObj(valWrapperObj);
        innerSaveIxRequestDtos.push(tempObj);
      }
    }

    //handle temporary
    if (temporaryStorageMap.size > 0) {
      for (let valWrapperObj of temporaryStorageMap.values()) {
        let tempObj = this.generateOrderObj(valWrapperObj);
        if (!!tempObj.operationType) {
          innerSaveIxRequestDtos.push(tempObj);
        }
      }
    }

    return {
      operationType:saveType,
      innerSaveIxRequestDtos
    };
  }

  handleSave = saveType => {
    const { encounterData } = this.props;
    let resultObj = this.generateResultObj(saveType);
    this.props.openCommonCircularDialog();
    this.props.saveIxRequestOrder({
      params:resultObj,
      callback: saveData => {
        this.props.getIxRequestOrderList({
          params:{
            patientKey: encounterData.patientKey,
            encounterId: encounterData.encounterId
          },
          callback:data=>{
            this.setState({
              temporaryStorageMap: data,
              deletedStorageMap: new Map()
            });
            this.props.closeCommonCircularDialog();
            let payload = {
              msgCode: saveData.msgCode,
              showSnackbar: true
            };
            this.props.openCommonMessage(payload);
          }
        });
      }
    });
  }

  updateStateWithoutStatus = obj => {
    this.setState({
      ...obj
    });
  }

  updateState=(obj)=>{
    this.setState({
      isEdit:true,
      ...obj
    });
  }

  render() {
    const {
      classes,
      loginClinicCd,
      loginInfo,
      dropdownMap,
      clinicList,
      itemMapping,
      openCommonMessage
    } = this.props;
    let {
      categoryMap,
      ioeFormMap,
      temporaryStorageMap,
      middlewareMapObj,
      middlewareObject,
      orderIsEdit,
      deletedStorageMap,
      selectedOrderKey,
      basicInfo,
      frameworkMap,
      lab2FormMap,
      contentVals,
      diagnosisErrorFlag,
      isEdit
    } = this.state;
    let basicInfoProps = {
      diagnosisErrorFlag,
      basicInfo,
      contentVals,
      clinicList,
      loginClinicCd,
      frameworkMap,
      updateState:this.updateState,
      updateStateWithoutStatus:this.updateStateWithoutStatus
    };
    let contentProps = {
      ioeFormMap,
      clinicList,
      diagnosisErrorFlag,
      basicInfo,
      contentVals,
      loginInfo,
      categoryMap,
      itemMapping,
      frameworkMap,
      lab2FormMap,
      dropdownMap,
      temporaryStorageMap,
      middlewareMapObj,
      middlewareObject,
      deletedStorageMap,
      orderIsEdit,
      selectedOrderKey,
      topTabs: this.topTabs,
      openCommonMessage,
      updateState:this.updateState,
      updateStateWithoutStatus:this.updateStateWithoutStatus,
      resetDiscipline:this.resetDiscipline
    };
    let disableSave = (temporaryStorageMap.size>0||deletedStorageMap.size>0)?!utils.isDisableSaving(temporaryStorageMap,deletedStorageMap):true;
    const buttonBar = {
      isEdit,
      position: 'fixed',
      style:{
        justifyContent:'space-between'
      },
      render:()=>{
        return(
          <div>
            <p className={classes.remark}><span className={classes.iteoSign}>@</span>: The test must be ordered independently.</p>
            <p className={classes.remark}><span className={classes.itefSign}>#</span>: The test must be ordered with other test(s) not labeled with #.</p>
          </div>
        );
      },
      buttons:[{
        title: 'Save and Print Reminder',
        id: 'btn_ix_request_save_print',
        disabled:disableSave
      },{
        title: 'Save and Print Label/Output Form',
        id: 'btn_ix_request_save_print_form',
        disabled:disableSave
      },{
        title: 'Save',
        id: 'btn_ix_request_save',
        disabled:disableSave,
        onClick:()=>{this.handleSave(constants.IX_REQUEST_SAVE_TYPE.IX_REQUEST_SAVE);}
      }]
    };
    return (
      <Container buttonBar={buttonBar}>
        <Typography component="div" id="wrapper" className={classes.wrapper}>
          <Card className={classes.cardWrapper}>
            <CardContent className={classes.cardContent}>
              <ValidatorForm id="ixForm" onSubmit={()=>{}}>
                <Grid container>
                  <Grid item xs={12}>
                    <BasicInfo {...basicInfoProps}/>
                  </Grid>
                  <Grid item xs={12}>
                    <ContentContainer {...contentProps}/>
                  </Grid>
                </Grid>
              </ValidatorForm>
            </CardContent>
          </Card>
        </Typography>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    encounterData: state.patient.encounterInfo,
    loginInfo: state.login.loginInfo,
    loginServiceCd: state.login.service.serviceCd,
    loginClinicCd: state.login.clinic.clinicCd,
    clinicList: state.ixRequest.clinicList,
    frameworkMap: state.ixRequest.frameworkMap,
    lab2FormMap: state.ixRequest.lab2FormMap,
    dropdownMap: state.ixRequest.dropdownMap,
    itemMapping: state.ixRequest.itemMapping,
    categoryMap: state.ixRequest.categoryMap
  };
};

const mapDispatchToProps = {
  getIxRequestSpecificMapping,
  getIxRequestFrameworkList,
  getIxClinicList,
  getIxRequestItemDropdownList,
  openCommonCircularDialog,
  closeCommonCircularDialog,
  openCommonMessage,
  deleteSubTabs,
  saveIxRequestOrder,
  getIxRequestOrderList,
  getIxAllItemsForSearch,
  getAllIxProfileTemplate
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ixRequest));
