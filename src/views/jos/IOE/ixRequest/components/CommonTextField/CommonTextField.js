import React, { Component } from 'react';
import { TextField, FormHelperText, withStyles } from '@material-ui/core';
import { styles } from './CommonTextFieldStyle';

class CommonTextField extends Component {

  handleTextFieldChanged = event => {
    const {onChange,stateParameter} = this.props;
    onChange&&onChange(event,stateParameter);
  }

  handleTextFieldBlur = event => {
    const {onBlur,stateParameter} = this.props;
    onBlur&&onBlur(event,stateParameter);
  }

  render() {
    const {classes,id='',value='',length,disabled=false,required=false,errorFlag} = this.props;
    let inputProps = {
      autoCapitalize: 'off',
      autoComplete: 'off',
      variant:'outlined',
      type:'text',
      inputProps: {
        style:{
          fontSize: '1rem',
          fontFamily: 'Arial'
        },
        maxLength:length || null
      }
    };

    let showRequiredError = required?errorFlag:false;

    return (
      <div className={classes.wrapper}>
        <TextField
            fullWidth
            id={id}
            value={value}
            error={showRequiredError}
            disabled={disabled}
            onChange={event => {this.handleTextFieldChanged(event);}}
            onBlur={event => {this.handleTextFieldBlur(event);}}
            {...inputProps}
        />
        {showRequiredError?(
          <FormHelperText error classes={{root:classes.helper_error}}>
            This field is required.
          </FormHelperText>
        ):null}
      </div>
    );
  }
}

export default withStyles(styles)(CommonTextField);
