export const styles = () => ({
  wrapper: {
    width: '100%',
    display: 'initial',
    float: 'left'
  },
  helper_error: {
    marginTop: 0,
    fontSize: '1rem !important',
    fontFamily: 'Arial',
    padding: '0 !important'
  }
});