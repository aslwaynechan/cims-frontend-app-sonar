import { COMMON_STYLE }from 'constants/commonStyleConstant';

export const style = {
  paper: {
    borderRadius:5,
    minWidth: 1100
  },
  dialogTitle: {
    backgroundColor: '#b8bcb9',
    borderTopLeftRadius:'5px',
    borderTopRightRadius:'5px',
    paddingLeft: '24px',
    padding: '10px 24px 0px 0px',
    color: '#404040',
    fontSize: '1.5rem',
    fontWeight: 500,
    lineHeight: 1.6,
    fontFamily: '-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif'
  },
  headRowStyle:{
    backgroundColor:COMMON_STYLE.TABLE_BGCKGROUNDCOLOR,
    color:'white'
  },
  headCellStyle:{
    color:'white',
    overflow: 'hidden',
    fontSize:'1.125rem'
  },
  fontLabel: {
    fontSize: '1rem',
    fontFamily: 'Arial'
  },
  buttonGroup: {
    textAlign:'right',
    marginRight:20
  },
  label:{
    fontSize:'1rem'
  },
  length1:{
    marginLeft: 100
  },

  CardPosition:{
    height:700,
    overflow:'auto',
    //width:1050,
    marginLeft: 23
  },
  paperTable:{
    width:'100%',
    overflow:'auto',
    minHeight:'700px'
  },
  dialogText:{
    border: '10px solid #b8bcb9'
  }
};


