import zIndex from '@material-ui/core/styles/zIndex';

export const styles = () => ({
  cellContent:{
    maxWidth:'8vw',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow:'hidden',
    display:'inherit',
    margin:'0px'
  },
  titleDiv: {
    padding: '16px',
    fontSize: ' 1.5rem',
    fontFamily:'-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif'
  },
  cardContent: {
    padding: '0px'
  },
  contentWrapper: {
    padding: '0px 16px 0px 16px',
    backgroundColor: 'white'
  },
  request_datetime_column: {
    width:93,
    fontSize:'1rem',
    fontFamily: 'Arial',
    // fontWeight: 'bold',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow:'hidden',
    marginRight:10
  },
  ioe_request_number_column: {
    width:140,
    fontSize:'1rem',
    fontFamily: 'Arial',
    // fontWeight: 'bold',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow:'hidden',
    marginRight:10
  },
  patientKey_column: {
    width:170,
    fontSize:'1rem',
    fontFamily: 'Arial',
    // fontWeight: 'bold',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow:'hidden',
    marginRight:10
  },
  formName_link:{
    whiteSpace: 'nowrap',
    overflow:'hidden',
    textOverflow:'ellipsis',
    display:'inline-block',
    width:'180px'
  },
  formName_column:{
    width:316,
    fontSize:'1rem',
    fontFamily: 'Arial',
    // fontWeight: 'bold',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow:'hidden',
    marginRight:10
  },
  serviceCd_column:{
    width:53,
    fontSize:'1rem',
    fontFamily: 'Arial',
    // fontWeight: 'bold',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow:'hidden',
    marginRight:10
  },
  clinicCd_column:{
    width:68,
    fontSize:'1rem',
    fontFamily: 'Arial',
    // fontWeight: 'bold',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow:'hidden',
    marginRight:10
  },
  test_column:{
    width:'180px',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow:'hidden',
    display:'inline-block'
  },
  specimen_column:{
     width:200,
    fontSize:'1rem',
    fontFamily: 'Arial',
     whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow:'hidden',
    display:'inline-block'
  },
  specimenCollectDatetime_column:{
    width:207,
    fontSize:'1rem',
    fontFamily: 'Arial',
    // fontWeight: 'bold',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow:'hidden',
    marginRight:10
  },
  rptRcvDatetime_column: {
    fontSize:'1rem',
    fontFamily: 'Arial',
    // fontWeight: 'bold',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow:'hidden',
    marginRight:10
  },
  labNum_column: {
    fontSize:'1rem',
    fontFamily: 'Arial',
    // fontWeight: 'bold',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow:'hidden'
  },
  label_div:{
    top:'-12px',
    position:'sticky',
    backgroundColor:'white',
    zIndex:10
    // paddingBottom:16
  }
});
