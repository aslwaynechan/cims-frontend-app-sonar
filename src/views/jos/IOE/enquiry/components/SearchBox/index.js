import React,{Component} from 'react';
import Form from '../../../../../../components/JForm';
import {MenuItem,Switch,FormControlLabel} from '@material-ui/core';
import Select from '../../../../../../components/JSelect';
import DatePicker from '../../../../MRAM/components/DateTextField/DatePicker';
import CIMSButton from '../../../../../../components/Buttons/CIMSButton';
import { cloneDeep } from 'lodash';
import moment from 'moment';

class Filter extends Component {
  constructor(props){
    super(props);
    this.state={
      ix:'request',
      exp:false,
      followUpStatus:'A',
      clinicCd:' ',
      emptyList:[{value:' ', title: '---Please Select---'}],
      fromDate:null,
      toDate:null,
      clinicCdList:[{value: this.props.loginInfo.clinic.code, title: this.props.loginInfo.clinic.code}]
    };
    console.log('fromDate========%s',moment(new Date()).format('DD-MMM-YYYY'));
  }

  handleChange=(value)=>{
    this.setState({ix:value});
  }
  toggleSwitch=(e,status)=>{
    this.setState({exp:status});
  }

  followUpStatusChange=(value)=>{
    this.setState({followUpStatus:value});
  }
  formDateChange=(value)=>{
    this.setState({fromDate:value !== null&&value !== '' ? moment(value).format('DD-MMM-YYYY') : null});
  }

  toDateChange=(value)=>{
    this.setState({toDate:value});
  }
  handleServiceCdChange=(value)=>{
    const {getClinics}=this.props;
     this.setState({clinicCd:' '});
    getClinics(value);
  }
  handleClinicChange=(value)=>{
     this.setState({clinicCd:value});
  }
  render(){
    const {onSearch,mode,options,getClinics,...rest}=this.props;
    const {forms=[],services=[],requestedByList=[]}=options;
    let {clinics=[]}=options;
    return (
      <Form  onSubmit={onSearch} {...rest} >
          <Select name="ix" label="Ix" onChange={this.handleChange} value={this.state.ix}>
            <MenuItem value={'request'}>Request</MenuItem>
            <MenuItem value={'report'}>Report</MenuItem>
          </Select>

          <DatePicker  maxDate={moment(new Date()).format('DD-MMM-YYYY')} name="fromDate" label="From" format="DD-MMM-YYYY" value={this.state.fromDate} onChange={this.formDateChange}/>
          <DatePicker  value={this.state.toDate} name="toDate" label="To" format="DD-MMM-YYYY" onChange={this.toDateChange}/>
          <Select name="formId" label="Form Name" options={forms} />
          {mode=='patient'&&<Select name="serviceCd" label="Service" options={services} onChange={this.handleServiceCdChange}/>}
          <Select name="clinicCd"  label="Clinic" value={mode==='clinic'?this.props.loginInfo.clinic.code:this.state.clinicCd} onChange={this.handleClinicChange} options={mode==='clinic'?this.state.clinicCdList:(clinics.length>0?clinics:this.state.emptyList)} readOnly={mode==='clinic'?true:false}/>
          <Select name="followUpStatus" label="Follow-up Status" disabled={this.state.ix==='request'?true:false} defaultValue={this.state.followUpStatus} >
            <MenuItem value={'A'}>All</MenuItem>
            <MenuItem value={'S'}>Screened</MenuItem>
            <MenuItem value={'R'}>Reviewed</MenuItem>
            <MenuItem value={'E'}>Explained</MenuItem>
          </Select>
          {mode=='clinic'&&
          <Select name="requestBy" label="Requested By">
             {requestedByList?requestedByList.map((item,index)=>{
          return (<MenuItem key={index} value={item.loginName}>{item.engSurname+' '+item.engGivenName+'('+item.loginName+')'}</MenuItem>);
        }):null}
            </Select>
          }
          {/* {mode=='patient'&&<input name="patientKey" value={patientKey} style={{display:'none'}}/>} */}

          <FormControlLabel name="turnaroundTime" control={<Switch checked={this.state.exp} onChange={this.toggleSwitch}  value={this.state.exp?'1':'0'} disabled={this.state.ix==='report'?true:false} />} label=">Exp Turnaround Time" no-need-input-label/>
          <CIMSButton style={{width:'30%'}} type={'submit'} variant="contained" color="primary">Search</CIMSButton>
      </Form>
    );
  }
}

export default Filter;
