export const styles = () => ({
  paper: {
    borderRadius:5,
    minWidth: 1550,
    maxWidth: 1550
  },
  dialogTitle: {
    backgroundColor: '#b8bcb9',
    borderTopLeftRadius:'5px',
    borderTopRightRadius:'5px',
    paddingLeft: '24px',
    padding: '10px 24px 7px 0px',
    color: '#404040',
    fontSize: '1.5rem',
    fontWeight: 500,
    lineHeight: 1.6,
    fontFamily: '-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif'
  },
  label:{
    fontWeight:'bold',
    paddingRight:4,
    paddingLeft:10
  },
  leftHeader: {
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    marginTop:2,
    paddingBottom:6
  },
  label_right:{
    fontSize:'1rem',
    fontFamily:'Arial'
  },
  left_warp: {
    padding: 5,
    backgroundColor: 'lightgray',
    height: 650,
    minHeight: 650
  },
  record_module: {
    maxWidth: '26%',
    flexBasis: '26%'
  },
  right_warp: {
    paddingLeft: 5,
    paddingTop: 5
  },
  table: {
    backgroundColor: '#fff',
    border: '1px solid rgba(0,0,0,0.5)',
    marginBottom: 5,
    overflow: 'auto'
  },
  title:{
    fontSize: '1rem',
    fontWeight: 600,
    fontFamily:'Arial',
    marginLeft: 5
  },
  save_div:{
    height:'13%',
    marginLeft: 5
  },
  textarea:{
    width:'65%',
    resize: 'none',
    float:'left',
    height:'80%'
  },
  record_detail_fab_icon: {
    fontSize: 31
  },
  comment_label:{
    float:'left',
    marginRight:10,
    fontWeight:600
  },
  toolTip:{
    margin:16
  },
  checkBox_root:{
    marginRight:0
  },
  table_width:{
    width:'242%'
  },
  table_header: {
    fontSize: '1rem',
    fontWeight: 600,
    fontFamily:'Arial',
    color: 'white',
    padding: '0, 0, 0, 10',
    position:'sticky',
    top: 0
  },
  table_cell:{
    fontSize: '1rem',
    paddingLeft: '2px',
    fontFamily:'Arial'
    // width: 100
  },
  btnDiv:{
    paddingLeft: 5
  },
  table_row_selected: {
    // height: 31,
    cursor: 'pointer',
    backgroundColor: 'lightgoldenrodyellow'
  },
  table_row: {
    // height: 31,
    cursor: 'pointer'
  },
  templateDetailValidation : {
    color: '#fd0000',
    margin: '0',
    fontSize: '1rem',
    float:'left',
   // marginTop: '8px',
    minHeight: '1em',
    display: 'block',
    marginTop:4,
    marginLeft:6
  },
  checkbox_sty:{
    paddingLeft:0,
    paddingRight:0,
    marginLeft:16
  }
});