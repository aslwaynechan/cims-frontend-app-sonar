import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import {Grid,Typography,Fab,Tooltip,FormControlLabel,Checkbox,List,ListItem,ListItemIcon,ListItemText} from '@material-ui/core';
import {styles} from './LaboratoryReportDialogStyle';
import  EditTemplateDialog from '../../../../../editTemplate/components/EditTemplateDialog';
import CIMSButton from '../../../../../../components/Buttons/CIMSButton';
import moment from 'moment';
import { SaveOutlined} from '@material-ui/icons';
import { previewReportDoctor} from '../../../../../../store/actions/MRAM/mramAction';
import { getIoeLaboratoryReportList,getIoeLaboratoryReportVersionList,getIoeLaboratoryReportPdf,saveIoeLaboratoryReportComment,getIoeLaboratoryReportCommentList,getPatientById,getRequestDetailById,updateIoeReportFollowUpStatus} from '../../../../../../store/actions/IOE/laboratoryReport/laboratoryReportAction';
import { openCommonMessage } from '../../../../../../store/actions/message/messageAction';
import LabTable from './components/Table/LabTable';
import CommentTable from './components/Table/CommentTable';
import PreLabPdf from './components/Pdf/PreLabPdf';
import { trim } from 'lodash';
import { COMMON_CODE } from '../../../../../../constants/message/common/commonCode';
import { IX_REQUEST_CODE } from '../../../../../../constants/message/IOECode/ixRequestCode';

const inital_state = {
    numPages: 0,
    pageNumber: 1,
    scale: 1.0,
    isPrinted: false,
    dataType: 'D',
    justifyContent: 'center',
    minScale: 1.0,
    maxScale: 3,
    padding: '5px'
};

const legends={
    '0':{codeName:'Fin',describe:'Final Report'},
    '1':{codeName:'N',describe:'Report Not Yet Received'},
    '2':{codeName:'IN',describe:'Interim Report'},
    '3':{codeName:'Pre',describe:'Preliminary Report'},
    '4':{codeName:'P',describe:'Provisional'},
    '5':{codeName:'PS',describe:'Provisional Supplementary'},
    '6':{codeName:'FS',describe:'Final Supplementary'},
    '7':{codeName:'A',describe:'Amend Report'},
    '8':{codeName:'AS',describe:'Amend Supplementary'},
    '9':{codeName:'X',describe:'Report Wipeout'}
  };

const Tip=withStyles(theme => ({
    tooltip: {
      backgroundColor: '#f5f5f9',
      color: 'rgba(0, 0, 0, 0.87)',
      fontSize: theme.typography.pxToRem(12),
      border: '1px solid #dadde9'
    }
  }))((props)=>{
    return (
      <Tooltip {...props} title={
        <React.Fragment>
          <List>
          {
            Object.keys(legends).map(key=>{
              const item=legends[key];
              return (
                <ListItem key={key}>
                  <ListItemIcon>{item.codeName}</ListItemIcon>
                  <ListItemText>{item.describe}</ListItemText>
                </ListItem>
              );
            })
          }
          </List>
        </React.Fragment>}
      >
        <lable>Rep Status</lable>
      </Tooltip>
    );
  });

class LaboratoryReportDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ...inital_state,
            medicalListData:[],
            checkBoxList:[{label:'Screened',value:'Screened',checked:false},{label:'Reviewed',value:'Reviewed',checked:false},{label:'Explained',value:'Explained',checked:false}],
            pageNumber: 1,
            previewData:'',
            textareaVal:'',
            reportDetailReportId:'',
            reportVersionId:'',
            laboratoryReportList:[],
            laboratoryReportVersionList:[],
            laboratoryReportCommentList:[],
            requestDetail:null,
            explainChecked:false,
            reviewChecked:false,
            screenedChecked:false,
            checkBoxListFlag:false,
            checkBoxEditFlag:false,
            messageEditFlag:false,
            funNormal:true
        };
      }

      UNSAFE_componentWillUpdate(nextProps){
        if (nextProps.open !== this.props.open) {
            this.initData(nextProps.open);
        }
      }

      initData(open){
        if(open){
             let {selected}=this.props;
             let selectedRequestIds='';
             let reportDetailReportId='';
             selectedRequestIds=this.getSelectedRequestIds(selected);
             if(selectedRequestIds.toString().length>0){
                let params={
                   requestIds:selectedRequestIds
               };
               this.props.getIoeLaboratoryReportList({
                   params,
                   callback:(result) =>{
                       this.setState({
                           laboratoryReportList:result.data,
                           currentIndex:0,
                           selectedRequestIds:selectedRequestIds
                       });
                       if(result.data.length>0){
                           reportDetailReportId=result.data[0].ioeReportId;
                           let params={
                               patientKey:result.data[0].patientKey
                           };
                           this.props.getPatientById({
                               params
                           });

                           params={requestId:result.data[0].ioeRequestId};
                           this.props.getRequestDetailById({
                               params,
                               callback:(result)=>{
                                   this.setState({requestDetail:result.data});
                               }
                           });
                           params={
                               ioeRequestId:result.data[0].ioeRequestId,
                               labNum:result.data[0].labNum
                           };
                           //get report version List
                           this.getIoeLaboratoryReportVersionList(params,reportDetailReportId);
                       }
                       else{
                           this.setState({
                               laboratoryReportList:[],
                               laboratoryReportVersionList:[],
                               laboratoryReportCommentList:[],
                               blob:null
                           });
                       }
                   }
               });
               this.setState({
                   checkBoxListFlag:false,
                   checkBoxEditFlag:false,
                   messageEditFlag:false,
                   textareaVal:''
               });
             }else{
                this.setState({
                    funNormal:false,
                    laboratoryReportList:[],
                    laboratoryReportVersionList:[],
                    laboratoryReportCommentList:[],
                    blob:null
                });
             }
        }

     }


     getSelectedRequestIds=(selected)=>{
        let selectedRequestIds='';
        for (let index = 0; index < selected.length; index++) {
            if(selected[index].labNum!=null){
                if (index===0) {
                    selectedRequestIds=selected[index].ioeRequestId;
                }
                else{
                 selectedRequestIds=selectedRequestIds+','+selected[index].ioeRequestId;
                }
            }
        }
        return selectedRequestIds;
     }

     getIoeLaboratoryReportVersionList=(params,ioeReportId)=>{
        this.props.getIoeLaboratoryReportVersionList({
            params,
            callback:(result)=>{
                this.setState({
                    laboratoryReportVersionList:result.data,
                    reportVersionId:result.data.length>0?result.data[0].ioeReportId:'',
                    reportDetailReportId:ioeReportId
                });
                if(result.data.length>0){
                    this.getIoeLaboratoryReportCommentList(result.data[0].ioeReportId);
                    this.getIoeLaboratoryReportPdf(result.data[0].ioeReportId);
                }
            }
        });
     }


     getIoeLaboratoryReportCommentList=(ioeReportId)=>{
        let params={
            reportId:ioeReportId
        };
        //get comment List
        this.props.getIoeLaboratoryReportCommentList({
            params,
            callback:(result)=>{
                this.setState({
                    laboratoryReportCommentList:result.data
                });
            }
        });
     }

     getIoeLaboratoryReportPdf=(ioeReportId)=>{
        let params={
            ioeReportId:ioeReportId
        };

        //get report pdf
        this.props.getIoeLaboratoryReportPdf({
            params,
            callback:(result)=>{
                this.b64toBlob(result.data.reportData, 'application/pdf');
            }
        });
     }

    handleDialogClose=()=>{
        let {updateState}=this.props;
        if(this.state.checkBoxEditFlag||this.state.messageEditFlag){
            let payload = {
                msgCode:COMMON_CODE.SAVE_WARING,
                btnActions:{
                  btn1Click: () => {
                    updateState&&updateState({LaboratoryReportDialogOpen:false});
                  }
                }
              };
              this.props.openCommonMessage(payload);
            } else {
                updateState&&updateState({LaboratoryReportDialogOpen:false});
            }
    }

    handleCheckBoxChange=(index)=>{
        let items = [...this.state.checkBoxList];
        items[index].checked =!items[index].checked;
        if(items[index].checked&&this.state.funNormal){
            this.setState({
                checkBoxListFlag:false,
                checkBoxEditFlag:true
            });
        }

        for (let index = 0; index < items.length; index++) {
          if(index===0){
            this.setState({screenedChecked:items[index].checked});
          }
          else if(index===1){
            this.setState({reviewChecked:items[index].checked});
          }
          else if(index===2){
            this.setState({explainChecked:items[index].checked});
          }
        }

        this.setState({checkBoxList:items});
      }

      b64toBlob = (b64Data, contentType, sliceSize) => {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
        let byteCharacters = atob(b64Data);
        let byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            let slice = byteCharacters.slice(offset, offset + sliceSize);

            let byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            let byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        let blob = new Blob(byteArrays, { type: contentType });
        this.setState({blob:blob});
    }

    getPreviewReportData=(type)=>{
        this.props.openCommonCircularDialog();
          let params={
            mramId:this.state.selectRow
          };
          this.props.previewReportPatient({
            params,
            callback:(previewData) =>{
              this.setState({
                previewData:previewData,
                previewShow:true
              });
            }
          });
    }


    changeTextarea=(e)=>{
        let messageEditFlag=false;
        if(this.state.funNormal){
            messageEditFlag=true;
        }else{
            messageEditFlag=false;
        }
        this.setState({
            textareaVal:e.target.value,
            messageEditFlag:messageEditFlag
        });
    }

    saveComment=()=>{
        let reportId=this.state.reportVersionId;
        let textareaVal=this.state.textareaVal;
        if(reportId!=''&&reportId!=null){
        if(trim(textareaVal)!=''){
        let params={
            cmnt:textareaVal,
            ioeReportId:reportId,
            createdBy:'',
            createdDtm:'',
            ioeReportCommentId:'',
            updatedBy:'',
            updatedDtm:''
        };
        this.props.saveIoeLaboratoryReportComment({
            params,
            callback:(result)=>{
                if(result.respCode===0){
                    let payload = {
                        msgCode:result.msgCode,
                        showSnackbar:true
                      };
                    this.props.openCommonMessage(payload);
                    this.getIoeLaboratoryReportCommentList(reportId);
                }

            }
        });
        this.setState({messageEditFlag:false});
        }else{
            let payload = {
                msgCode:IX_REQUEST_CODE.ENQUIRY_NOT_EMPTY
              };
            this.props.openCommonMessage(payload);
        }
    }else{
        return false;
    }
    }

    changeReportDetail=(item,index)=>{
        //get report version List
        let params={
            ioeRequestId:item.ioeRequestId,
            labNum:item.labNum
        };
        this.getIoeLaboratoryReportVersionList(params,item.ioeReportId);
        this.setState({currentIndex:index});
    }

    changeReportVersion=item=>{
        this.setState({reportVersionId:item.ioeReportId});
        this.getIoeLaboratoryReportCommentList(item.ioeReportId);
        this.getIoeLaboratoryReportPdf(item.ioeReportId);
    }

    previousReport=()=>{
        if(this.state.funNormal){
        if(!this.isEmptyCheckList()){
            this.setState({checkBoxListFlag:false});
            let params={
                ioeReportId:this.state.reportDetailReportId,
                explainChecked:this.state.explainChecked,
                reviewChecked:this.state.reviewChecked,
                screenedChecked:this.state.screenedChecked
            };
            this.props.updateIoeReportFollowUpStatus({
                params,
                callback:(result)=>{
                    let newIndex;
                    if(this.state.currentIndex>0){
                        newIndex=this.state.currentIndex-1;
                    }
                    else{
                        newIndex=this.state.currentIndex;
                    }
                    let currentReport=this.state.laboratoryReportList[newIndex];
                    this.reloadLaboratoryReport(currentReport);
                    this.setState({
                        currentIndex:newIndex,
                        explainChecked:false,
                        reviewChecked:false,
                        screenedChecked:false,
                        checkBoxEditFlag:false,
                        messageEditFlag:false,
                        textareaVal:''
                        // checkBoxList:[{label:'Screened',value:'Screened',checked:false},{label:'Reviewed',value:'Reviewed',checked:false},{label:'Explained',value:'Explained',checked:false}]
                    });

                }
            });
        }else{
            return false;
            // this.setState({checkBoxListFlag:true});
        }
    }
    }

    nextReport=()=>{
        if(this.state.funNormal){
        if(!this.isEmptyCheckList()){
            this.setState({checkBoxListFlag:false});
            let params={
                ioeReportId:this.state.reportDetailReportId,
                explainChecked:this.state.explainChecked,
                reviewChecked:this.state.reviewChecked,
                screenedChecked:this.state.screenedChecked
            };
            this.props.updateIoeReportFollowUpStatus({
                params,
                callback:(result)=>{
                    let newIndex;
                    if(this.state.currentIndex<this.state.laboratoryReportList.length-1){
                        newIndex=this.state.currentIndex+1;
                    }else{
                        newIndex=this.state.currentIndex;
                    }
                    let currentReport=this.state.laboratoryReportList[newIndex];
                        this.reloadLaboratoryReport(currentReport);
                        this.setState({
                            currentIndex:newIndex,
                            explainChecked:false,
                            reviewChecked:false,
                            screenedChecked:false,
                            checkBoxEditFlag:false,
                            messageEditFlag:false,
                            textareaVal:''
                            // checkBoxList:[{label:'Screened',value:'Screened',checked:false},{label:'Reviewed',value:'Reviewed',checked:false},{label:'Explained',value:'Explained',checked:false}]
                        });
                }
            });
        }else{
            return false;
            // this.setState({checkBoxListFlag:true});
        }
    }
    }

    isEmptyCheckList=()=>{
        let flag=true;
        let checkBoxList=this.state.checkBoxList;
        for (let index = 0; index < checkBoxList.length; index++) {
            if(checkBoxList[index].checked){
                flag=false;
                break;
            }
        }
        return flag;
    }

    reloadLaboratoryReport=(currentReport)=>{
        let params={
            requestIds:this.state.selectedRequestIds
        };
        this.props.getIoeLaboratoryReportList({
            params,
            callback:(result)=>{
                this.setState({laboratoryReportList:result.data});
                let reportDetailReportId=currentReport.ioeReportId;
                params={
                    patientKey:currentReport.patientKey
                };
                this.props.getPatientById({
                    params
                });

                params={requestId:currentReport.ioeRequestId};
                this.props.getRequestDetailById({
                    params,
                    callback:(result)=>{
                        this.setState({requestDetail:result.data});
                    }
                });
                params={
                    ioeRequestId:currentReport.ioeRequestId,
                    labNum:currentReport.labNum
            };
            //get report version List
            this.getIoeLaboratoryReportVersionList(params,reportDetailReportId);

            }
        });
    }

    render() {
        const { classes,open,patientInfo} = this.props;
        const {blob,laboratoryReportList,laboratoryReportVersionList,laboratoryReportCommentList,requestDetail,reportDetailReportId,reportVersionId}=this.state;
        let labTableParams={
            laboratoryReportList,
            laboratoryReportVersionList,
            reportDetailReportId,
            reportVersionId,
            changeReportVersion:this.changeReportVersion,
            changeReportDetail:this.changeReportDetail
        };
        let commentTableParams={
            laboratoryReportCommentList
        };
        let preLabPdfParams={
            blob
        };
        let hkid='';
        let hkidNum='';
        if (patientInfo) {
            hkid =patientInfo.hkid && patientInfo.hkid.substring(0, patientInfo.hkid.length - 1);
            hkidNum =patientInfo.hkid && patientInfo.hkid.substring(patientInfo.hkid.length - 1);
        }
        return (
        <EditTemplateDialog
            dialogTitle="Report Detail"
            open={open}
            classes={{
                paper: classes.paper
            }}
            handleEscKeyDown={this.handleDialogClose}
        >
            <Typography
                component="div"
                style={{ backgroundColor: 'white', marginLeft: 5, marginRight: 5, marginTop: 5,position:'relative'}}
            >
                <Typography component="div">
                    <label className={classes.label}>Name: {patientInfo!==null?patientInfo.engSurname+','+patientInfo.engGivename+'('+patientInfo.nameChi+')':''}</label>
                </Typography>
                <Typography component="div">
                    <label className={classes.label}>HKIC: {patientInfo!==null? `${hkid}(${hkidNum})`:''}</label>
                    {/* <label className={classes.label}>PMI: {patientInfo!==null?patientInfo.patientKey:''}</label> */}
                    <label className={classes.label}>DOB: {patientInfo!==null? moment(patientInfo.dob).format('DD-MMM-YYYY'):''}</label>
                    <label className={classes.label}>Age: {`${patientInfo!==null?patientInfo.age:''}${patientInfo!==null?patientInfo.ageUnit[0]:''}`}</label>
                    <label className={classes.label}>Sex: {patientInfo!==null?patientInfo.genderCd:''}</label>
                </Typography>
                <Typography className={classes.leftHeader}
                    component="h3"
                    variant="h5"
                >
                    <label className={classes.label}>Request No:</label><label className={classes.label_right}>{requestDetail!==null?requestDetail.ioeRequestNumber:''}</label>
                    <label className={classes.label}>Req Date:</label><label className={classes.label_right}>{requestDetail!==null?moment(requestDetail.requestDatetime).format('DD-MMM-YYYY'):''}</label>
                    <label title={requestDetail!==null?(requestDetail.test!==null?(requestDetail.test.length>54?requestDetail.test.slice(0,54)+'...':requestDetail.test):''):''} className={classes.label}>Test Profile:</label><label className={classes.label_right}>{requestDetail!==null?(requestDetail.test!==null?(requestDetail.test.length>54?requestDetail.test.slice(0,54)+'...':requestDetail.test):''):''}</label>
                    <label title={requestDetail!==null?(requestDetail.specimen!==null?(requestDetail.specimen.length>54?requestDetail.specimen.slice(0,54)+'...':requestDetail.specimen):''):''} className={classes.label}>Specimen:</label><label className={classes.label_right}>{requestDetail!==null?(requestDetail.specimen!==null?(requestDetail.specimen.length>54?requestDetail.specimen.slice(0,54)+'...':requestDetail.specimen):''):''}</label>
                </Typography>

                <Grid container>
                    <Grid
                        className={classes.left_warp}
                        item
                        style={{ marginBottom: 10 }}
                        xs={5}

                    >
                        <LabTable {...labTableParams}></LabTable>
                        <Typography component="div" className={classes.save_div}>
                            <Typography component="div" className={classes.comment_label}>
                                <label>Comment</label>
                            </Typography>
                            <textarea  maxLength="1000" value={this.state.textareaVal} onChange={this.changeTextarea} className={classes.textarea}></textarea>
                            <Tooltip onClick={this.saveComment} className={classes.toolTip} title="Save" aria-label="Save">
                                <Fab
                                    color="primary"
                                >
                                    <SaveOutlined  className={classes.record_detail_fab_icon} />
                                </Fab>
                            </Tooltip>
                        </Typography>
                        <Typography
                            className={classes.table}
                            component="div"
                            style={{minHeight: 166,height:'27%'}}
                        >
                          <CommentTable {...commentTableParams}/>
                        </Typography>
                    </Grid>
                    <Grid
                        item
                        xs={7}
                    >
                        {/* pdf function */}
                        <Typography className={classes.right_warp}
                            component="div"
                        >
                            <PreLabPdf {...preLabPdfParams}/>
                        </Typography>
                    </Grid>
                </Grid>

                <Typography component="div" className={classes.btnDiv}>
                    <Grid container>
                        <Grid
                            item
                            xs={7}

                        >
                            <Grid alignItems="center"
                                container
                                justify="flex-start"
                            >
                                {this.state.checkBoxList.map((item,index) => {
                                return (
                                    <FormControlLabel
                                        // className={classes.Checkbox}
                                        control={<Checkbox name="DASM_168" color="primary" checked={item.checked} onClick={()=>this.handleCheckBoxChange(index)}
                                            inputProps={{
                                                style:{
                                                    width:30
                                                }
                                            }}
                                            classes={{
                                                    root:classes.checkbox_sty
                                                }}
                                                 />}
                                        key={index}
                                        label={item.label}
                                        value={item.value}
                                        classes={{
                                        label: classes.font,
                                        root:classes.checkBox_root
                                        }}
                                    />
                                    );
                                })}

                                <CIMSButton
                                    classes={{
                                        label:classes.fontLabel
                                    }}
                                    color="primary"
                                    id="btn_ioe_lab_save"
                                    onClick={() =>this.previousReport()}
                                    size="small"
                                >
                                    Previous
                                </CIMSButton>
                                <CIMSButton
                                    classes={{
                                        label:classes.fontLabel
                                    }}
                                    color="primary"
                                    id="btn_ioe_lab_nextReport"
                                    onClick={() =>this.nextReport()}
                                    size="small"
                                >
                                    Next
                                </CIMSButton>
                                {this.state.checkBoxListFlag?(
                                    <div>
                                        <span className={classes.templateDetailValidation}
                                            id="checkBoxList_ioe_lab_validation"
                                        >This field selected at least one.</span>
                                    </div>
                                ):<div></div>}
                            </Grid>
                        </Grid>
                        <Grid
                            item
                            xs={5}
                        >
                            <Grid alignItems="center"
                                container
                                justify="flex-end"
                            >
                                <CIMSButton
                                    classes={{
                                        label:classes.fontLabel
                                    }}
                                    color="primary"
                                    id="btn_ioe_lab_close"
                                    onClick={() =>this.handleDialogClose()}
                                    size="small"
                                >
                                    Close
                                </CIMSButton>
                            </Grid>
                        </Grid>
                    </Grid>
                </Typography>
            </Typography>
        </EditTemplateDialog>
        );
    }
}
const mapStateToProps = state => {
    return {
        patientInfo:state.laboratoryReport.patientInfo
    };
  };

  const mapDispatchToProps = {
    previewReportDoctor,
    getIoeLaboratoryReportList,
    getIoeLaboratoryReportVersionList,
    getIoeLaboratoryReportPdf,
    saveIoeLaboratoryReportComment,
    getIoeLaboratoryReportCommentList,
    getPatientById,
    openCommonMessage,
    getRequestDetailById,
    updateIoeReportFollowUpStatus
  };

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(LaboratoryReportDialog));
