import React, { Component } from 'react';
import {Grid,Table,TableRow,Typography,TableCell,TableHead,TableBody,Tooltip,List,ListItem,ListItemIcon,ListItemText} from '@material-ui/core';
import {styles} from '../../LaboratoryReportDialogStyle';
import { withStyles } from '@material-ui/core/styles';
import moment from 'moment';
const legends={
    '0':{codeName:'Fin',describe:'Final Report'},
    '1':{codeName:'N',describe:'Report Not Yet Received'},
    '2':{codeName:'IN',describe:'Interim Report'},
    '3':{codeName:'Pre',describe:'Preliminary Report'},
    '4':{codeName:'P',describe:'Provisional'},
    '5':{codeName:'PS',describe:'Provisional Supplementary'},
    '6':{codeName:'FS',describe:'Final Supplementary'},
    '7':{codeName:'A',describe:'Amend Report'},
    '8':{codeName:'AS',describe:'Amend Supplementary'},
    '9':{codeName:'X',describe:'Report Wipeout'}
  };

const Tip=withStyles(theme => ({
    tooltip: {
      backgroundColor: '#f5f5f9',
      color: 'rgba(0, 0, 0, 0.87)',
      fontSize: theme.typography.pxToRem(12),
      border: '1px solid #dadde9'
    }
  }))((props)=>{
    return (
      <Tooltip {...props} title={
        <React.Fragment>
          <List>
          {
            Object.keys(legends).map(key=>{
              const item=legends[key];
              return (
                <ListItem key={key}>
                  <ListItemIcon>{item.codeName}</ListItemIcon>
                  <ListItemText>{item.describe}</ListItemText>
                </ListItem>
              );
            })
          }
          </List>
        </React.Fragment>}
      >
        <lable>Rep Status</lable>
      </Tooltip>
    );
  });
class LabTable extends Component{
    constructor(props) {
        super(props);
        this.state = {
            medicalListData:[],
            pageNumber: 1,
            previewData:'',
            textareaVal:'',
            reportVersionId:'',
            laboratoryReportList:[],
            laboratoryReportVersionList:[],
            laboratoryReportCommentList:[],
            requestDetail:null,
            explainChecked:false,
            reviewChecked:false,
            screenedChecked:false,
            checkBoxListFlag:false
        };
      }
      render(){
        const { classes,laboratoryReportList=[],laboratoryReportVersionList=[],reportDetailReportId='',reportVersionId='',changeReportVersion,changeReportDetail} = this.props;
        return (
        <Grid container>
            <Grid
                item
                xs={8}
            >
                <Typography
                    className={classes.title}
                    component="div"
                >
                    Report Detail
                </Typography>
                <Typography
                    className={classes.table}
                    component="div"
                    style={{minHeight: 348, height: 'calc(100vh - 750px)' }}
                >
                    <Table  classes={{root:classes.table_width}}>
                        <TableHead>
                            <TableRow style={{backgroundColor:'#7BC1D9'}} className={classes.table_head}  >
                                <TableCell
                                    className={classes.table_header}
                                    padding={'none'}
                                    style={{ paddingLeft: '2px', fontStyle: 'normal', fontSize: '1rem', fontWeight: 'bold', width: '12%' }}
                                >
                                    Rep Rcv Date
                                </TableCell>
                                <TableCell className={classes.table_header}
                                    padding={'none'}
                                    style={{ paddingLeft: '2px', fontStyle: 'normal', fontSize: '1rem', fontWeight: 'bold', width: '8%' }}
                                >
                                    Lab Num
                                </TableCell>
                                <TableCell
                                    className={classes.table_header}
                                    padding={'none'}
                                    style={{ paddingLeft: '2px', fontStyle: 'normal', fontSize: '1rem', fontWeight: 'bold', width: '10%' }}

                                >
                                    <Tip title={'Rep Status'}/>
                                    {/* Rep Status */}
                                </TableCell>
                                <TableCell
                                    className={classes.table_header}
                                    padding={'none'}
                                    style={{ paddingLeft: '2px', fontStyle: 'normal', fontSize: '1rem', fontWeight: 'bold',width:'14%'}}
                                >
                                    Follow-up Status
                                </TableCell>
                                <TableCell
                                    className={classes.table_header}
                                    padding={'none'}
                                    style={{ paddingLeft: '2px', fontStyle: 'normal', fontSize: '1rem', fontWeight: 'bold' }}
                                >
                                    Screened Date
                                </TableCell>
                                <TableCell
                                    className={classes.table_header}
                                    padding={'none'}
                                    style={{ paddingLeft: '2px', fontStyle: 'normal', fontSize: '1rem', fontWeight: 'bold' }}
                                >
                                    Screened By
                                </TableCell>
                                <TableCell
                                    className={classes.table_header}
                                    padding={'none'}
                                    style={{ paddingLeft: '2px', fontStyle: 'normal', fontSize: '1rem', fontWeight: 'bold' }}
                                >
                                    Reviewed Date
                                </TableCell>
                                <TableCell
                                    className={classes.table_header}
                                    padding={'none'}
                                    style={{ paddingLeft: '2px', fontStyle: 'normal', fontSize: '1rem', fontWeight: 'bold' }}
                                >
                                    Reviewed By
                                </TableCell>
                                <TableCell
                                    className={classes.table_header}
                                    padding={'none'}
                                    style={{ paddingLeft: '2px', fontStyle: 'normal', fontSize: '1rem', fontWeight: 'bold' }}
                                >
                                    Explained Date
                                </TableCell>
                                <TableCell
                                    className={classes.table_header}
                                    padding={'none'}
                                    style={{ paddingLeft: '2px', fontStyle: 'normal', fontSize: '1rem', fontWeight: 'bold' }}
                                >
                                    Explained By
                                </TableCell>

                            </TableRow>
                        </TableHead>
                        <TableBody>
                        {laboratoryReportList.length>0?laboratoryReportList.map((item, index) => (
                            <TableRow
                                className={
                                item.ioeReportId ===
                                reportDetailReportId
                                ? classes.table_row_selected
                                : classes.table_row
                            }
                                key={index}
                                onClick={() => changeReportDetail(item,index)}
                            >
                                <TableCell padding={'none'}
                                    className={classes.table_cell}
                                >
                                    {moment(item.rptRcvDatetime).format('DD-MMM-YYYY')}
                                </TableCell>
                                <TableCell padding={'none'}
                                    className={classes.table_cell}
                                >
                                    {(item.labNum)}
                                </TableCell>
                                <TableCell padding={'none'}
                                    className={classes.table_cell}
                                >
                                    {item.rptSts}
                                </TableCell>
                                <TableCell padding={'none'}
                                    className={classes.table_cell}
                                >
                                    {item.rptFlwSts}
                                </TableCell>
                                <TableCell padding={'none'}
                                    className={classes.table_cell}
                                >
                                    {item.scrnDatetime!==null?moment(item.scrnDatetime).format('DD-MMM-YYYY'):''}
                                </TableCell>
                                <TableCell padding={'none'}
                                    className={classes.table_cell}
                                >
                                    {item.scrnBy}
                                </TableCell>
                                <TableCell padding={'none'}
                                    className={classes.table_cell}
                                >
                                    {item.reviewDatetime!==null?moment(item.reviewDatetime).format('DD-MMM-YYYY'):''}
                                </TableCell>
                                <TableCell padding={'none'}
                                    className={classes.table_cell}
                                >
                                    {item.reviewBy}
                                </TableCell>
                                <TableCell padding={'none'}
                                    className={classes.table_cell}
                                >
                                    {item.explainDatetime!==null?moment(item.explainDatetime).format('DD-MMM-YYYY'):''}
                                </TableCell>
                                <TableCell padding={'none'}
                                    className={classes.table_cell}
                                >
                                    {item.explainBy}
                                </TableCell>
                            </TableRow>
                        )):
                            <TableRow >
                                <TableCell className={classes.fontLabel} colSpan={10} align="center">
                                    There is no data.
                                </TableCell>
                            </TableRow>
                            }
                        </TableBody>
                    </Table>
                </Typography>
            </Grid>
            <Grid
                item
                xs={4}
            >
                <Typography
                    className={classes.title}
                    component="div"
                >
                    Report Version
                </Typography>
                <Typography
                    className={classes.table}
                    component="div"
                    style={{minHeight: 348, height: 'calc(100vh - 750px)' }}
                >
                    <Table>
                        <TableHead>
                            <TableRow style={{backgroundColor:'#7BC1D9'}} className={classes.table_head}  >
                                <TableCell
                                    className={classes.table_header}
                                    padding={'none'}
                                    style={{ paddingLeft: '2px', fontStyle: 'normal', fontSize: '1rem', fontWeight: 'bold', width: '30%' }}
                                >
                                    Report Receive Date
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                        {laboratoryReportVersionList.length>0?laboratoryReportVersionList.map((item, index) => (
                            <TableRow
                                className={
                                item.ioeReportId ===
                                reportVersionId
                                ? classes.table_row_selected
                                : classes.table_row
                                }
                                key={index}
                                onClick={() => changeReportVersion(item)}
                            >
                                <TableCell padding={'none'}
                                    className={classes.table_cell}
                                >
                                    {moment(item.rptRcvDatetime).format('DD-MMM-YYYY')}
                                </TableCell>
                            </TableRow>
                             )):
                            <TableRow >
                                <TableCell className={classes.fontLabel} colSpan={1} align="center">
                                    There is no data.
                                </TableCell>
                            </TableRow>
                         }
                        </TableBody>
                    </Table>
                </Typography>
            </Grid>
        </Grid>

        );
      }
}

export default withStyles(styles)(LabTable);
