import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles,Tooltip,List,ListItem,ListItemIcon,ListItemText} from '@material-ui/core';
import {Help} from '@material-ui/icons';
import { styles } from './enquiryStyle';
import Table from '../../../../components/JTable';
import Filter from './components/SearchBox';
import LaboratoryReportDialog from './components/LaboratoryReportDialog/LaboratoryReportDialog';
import EnquiryPatientDialog from './EnquiryPatientDialog';
import {Priority1,Priority2,Priority3,Backdate,Urgent,Partial} from '../../../../components/Icons';
import * as actionTypes from '../../../../store/actions/IOE/enquiry/enquiryActionType';
import moment from 'moment';
import * as messageTypes from '../../../../store/actions/message/messageActionType';
import Container from 'components/JContainer';
import { COMMON_STYLE } from '../../../../constants/commonStyleConstant';


const legends={
  // '0':{icon:Priority1,color:'#ff0000',describe:'Attention High/Low For Chem/Haem'},
  '0':{icon:Priority1,color:'#ff0000',describe:'Attention High/Low For Chem/Haem'},
  'URGENT':{icon:Urgent,color:'#ff0000',describe:'Urgent Request/Important result marked by H&C'},
  '2':{icon:Backdate,color:'#0579c8',describe:'Backdate Encounter'},
  'IS_SUPP_AMEND_HC':{icon:Priority2,color:'#ff0000',describe:'Attentions Supplementary/Amendment Report for H&C'},
  'IS_WIPEOUT':{icon:Priority3,color:'#ff0000',describe:'Report Wipeout for H&C'},
  'IS_PARTIAL_MATCHED':{icon:Partial,color:'#ff0000',describe:'Partial Match on PMI Core Fields(s)'}
};

  // const options={
  //   selection:'ioeRequestNumber',
  //   maxBodyHeight:516
  //   // paging:true
  // };

const Tip=withStyles(theme => ({
  tooltip: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9'
  }
}))((props)=>{
  return (
    <Tooltip {...props} title={
      <React.Fragment>
        <List>
        {
          Object.keys(legends).map(key=>{
            const item=legends[key];
            return (
              <ListItem key={key}>
                <ListItemIcon><item.icon style={{color:item.color}} /></ListItemIcon>
                <ListItemText>{item.describe}</ListItemText>
              </ListItem>
            );
          })
        }
        </List>
      </React.Fragment>}
    >
      <Help/>
    </Tooltip>
  );
});

class Enquiry extends Component {
  constructor(props) {
    super(props);
    this.state = {
        openTooltip:false,
        LaboratoryReportDialogOpen:false,
        selected:[],
        selectedRequestIds:''
    };
  }

  componentDidMount() {
    this.props.ensureDidMount();
    const {dispatch,location}=this.props;
    let mode=location.pathname.indexOf('F117')>-1?'patient':'clinic';
    dispatch({type:actionTypes.getForms,params:{},callback:(data)=>{
      console.log(data);
    }});
    if(mode==='clinic'){
      dispatch({type:actionTypes.UPDATE_IOE_LABORATORY_FOLLOWUP_STATUS,params:{},callback:(data)=>{
        console.log('UPDATE_IOE_LABORATORY_FOLLOWUP_STATUS===========%o',data);
      }});
    }
    if(mode=='patient'){
      dispatch({type:actionTypes.getServices,params:{},callback:(data)=>{
        console.log(data);
      }});
      this.getClinics('ALL');
    }
    this.setState({
      selectedRequestIds:'',
      historyList:[]
    });
  }

  handleHint=(id)=>{
    let {dispatch} = this.props;
    event.stopPropagation();
    let params = {'ioeRequestId': id};
    dispatch({type:actionTypes.REQUEST_ORDER_DETAILS,params:params,callback:(data)=>{
      this.setState({
        openTooltip:true,
        details:data
      });
    }});
    event.preventDefault();
    // return false;
  }


  handleClose = () =>{
    this.setState({
       openTooltip:false
    });
  }

  toggleDialog=()=>{
    if(this.state.selected.length>0){
      this.setState({
        LaboratoryReportDialogOpen:true
      });
    }
    else{
      const {dispatch}=this.props;
      let payload = {
        msgCode:'101601'
      };
      dispatch({type:messageTypes.OPEN_COMMON_MESSAGE,payload:payload});
    }

  }

  handleSelectAll=()=>{
    const {historyList=[]}=this.state;
    this.setState({selected:historyList});
  }

  handleDeselectAll=()=>{
    this.setState({selected:[]});
  }

  handleSelectionChange=(selected)=>{
    this.setState({selected:selected});
  }
  handleSearch=(params)=>{
    const {dispatch,location}=this.props;
    const {patientKey=''} =this.props.patientInfo;
    let mode=location.pathname.indexOf('F117')>-1?'patient':'clinic';
    params.patientKey=patientKey;
    if(mode==='clinic'){
      const {loginInfo}=this.props;
      params.requestBy=params.requestBy===''?null:params.requestBy;
      params.clinicCd=loginInfo.clinic.code;
      params.serviceCd=loginInfo.service.code;
    }
    params.formId=params.formId===''?null:params.formId;
    params.fromDate=params.fromDate===''?null:moment(params.fromDate).format('YYYY-MM-DD');
    params.toDate=params.toDate===''?null:moment(params.toDate).format('YYYY-MM-DD');
    params.formId=params.formId===''?null:params.formId;
    if(params.followUpStatus==='A'){
      params.followUpStatus=null;
    }
    dispatch({type:actionTypes.getHistoryList,params:params,callback:(data)=>{
      this.setState({historyList:data});
    }});
  }

  getClinics=(value)=>{
    const {dispatch}=this.props;
    let params = {
      clinicCd:'',
      displaySeq:'',
      cdFlag:'',
      clinicName:'',
      serviceCd:value,
      clinicNameChi:'',
      locationEng:'',
      locationChi:'',
      phoneNo:'',
      fax:''
    };
    dispatch({type:actionTypes.getClinics,params:params,callback:(data)=>{
    }});
  }


  render() {
    const {location,loginInfo,classes}=this.props;
    const {forms,services,clinics,requestedByList}=this.props.enquiry;
    const {historyList=[],details} =this.state;
    const mode=location.pathname.indexOf('F117')>-1?'patient':'clinic';
    const options={
      selection:'ioeRequestNumber',
      // maxBodyHeight:474,
      overflowX:'unset',
      overflowY:'unset',
      draggable:false,
      headerStyle:{
        top:162,
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        overflow:'hidden',
        maxWidth:'8vw',
        fontSize:'1rem',
        fontFamily: 'Arial'
      }
      // paging:true
    };
    let { openTooltip,selected} = this.state;
    let EnquiryPatientDialogParams={
        details:details,
        openTooltip:openTooltip,
        handleClose:this.handleClose
      };
    const columns=[
 		{title:<Tip />,headerStyle:{paddingLeft:4},field:'warningFlag',cellStyle:{width:'2%'},
        render:(record)=>{
          if(record.warningFlag){
            const item=legends[record.warningFlag];
            if(item){
              return <Tooltip style={{margin:'0 0 0 4px'}} title={item.describe}><p><item.icon style={{color:item.color}} /></p></Tooltip>;
            }
          }
        }},
        {title:'Request Date',field:'requestDatetime',render:(record)=>{
          if(record.requestDatetime){
            return (<p title={moment(record.requestDatetime).format('DD-MMM-YYYY')} className={classes.cellContent}> {moment(record.requestDatetime).format('DD-MMM-YYYY')}</p>);
          }
        }},
        {title:'Request No.',field:'ioeRequestNumber',render:(record)=>{
          return (<p title={record.ioeRequestNumber} className={classes.cellContent}>{record.ioeRequestNumber}</p>);
        }},
        {title:'Patient Name',field:'patientName',render:(record)=>{
          return (<p title={record.patientName} className={classes.cellContent}>{record.patientName}</p>);
        }},
        {title:'Form Name',field:'formName',render:(record)=>{
          return (<a className={classes.cellContent} href="#" title={record.test} onClick={(event)=>{this.handleHint(record.ioeRequestId);}}>{record.formName}</a>);
        }},
        {title:'Service',field:'serviceCd',render:(record)=>{
          return (<p title={record.serviceCd} className={classes.cellContent}>{record.serviceCd}</p>);
        }},
        {title:'Clinic',field:'clinicCd',render:(record)=>{
          return (<p title={record.clinicCd} className={classes.cellContent}>{record.clinicCd}</p>);
        }},
        {title:'Test Profile',field:'test',render:(record)=>{
          return (<p title={record.test} className={classes.cellContent}>{record.test}</p>);
        }},
 		    {title:'Specimen',field:'specimen',headerStyle:{width:'10%'},render:(record)=>{
          return (<p title={record.specimen} className={classes.cellContent}>{record.specimen}</p>);        }},
        {title:'Specimen Collection Date',field:'specimenCollectDatetime',render:(record)=>{
          if(record.specimenCollectDatetime){
            return (<p className={classes.cellContent}>{moment(record.specimenCollectDatetime).format('DD-MMM-YYYY')}</p>);
          }
        }},
        {title:'Report Received Date', headerStyle:{width:'10%'},field:'rptRcvDatetime',render:(record)=>{
          if(record.rptRcvDatetime){
            return (<p className={classes.cellContent}>{moment(record.rptRcvDatetime).format('DD-MMM-YYYY')}</p>);
          }
        }},
        {title:'Lab No',field:'labNum',render:(record)=>{
          return (<p title={record.labNum} className={classes.cellContent}>{record.labNum}</p>);
        }}
    ];

    const buttonBar={
      isEdit:false,
      // height:'64px',
      position:'fixed',
      buttons:[
        {
          title:'Lab Report',
          onClick:this.toggleDialog,
          id:'labReport_button'
        },
        {
          title:'Select All',
          onClick:this.handleSelectAll,
          id:'selectAll_button'
        },
        {
          title:'Deselect All',
          // style:{
          //   fontSize:'16pt'
          // },
          onClick:this.handleDeselectAll,
          id:'deselectAll_button'
        }
      ],
      style:{
        justifyContent:'space-between'
      },
      render:()=>{
        return (
            <div>
                No.of Request: {historyList.length}
            </div>
        );
      }
    };
    return (
      <Container buttonBar={buttonBar} style={{padding:'12px'}}>
          <div style={{color: '#000000',fontSize: '1.5rem',fontFamily:'Arial'}}>
            Ix Enquiry
            {mode=='patient'?'':' by clinic'}
          </div><br/>
          <div className={classes.label_div}>
            <Filter onSearch={this.handleSearch} mode={mode} options={{forms,services,clinics,requestedByList}} getClinics={this.getClinics} loginInfo={loginInfo}/>
          </div>
          <div style={{marginTop:5}}>
            <Table size={'sticky'} columns={mode=='clinic'?columns.filter(item=>item.title!=='Service'&&item.title!=='Clinic'):columns.filter(item=>item.title!=='Patient Name')} data={historyList} options={options} selected={selected} onSelectionChange={this.handleSelectionChange}/>
          </div>


            {/* <BottomBar>

            </BottomBar> */}
            <LaboratoryReportDialog  selected={this.state.selected} open={this.state.LaboratoryReportDialogOpen} updateState={(state)=>{this.setState(state);}} />
            <EnquiryPatientDialog  {...EnquiryPatientDialogParams}/>
      </Container>
    );
  }
}

const mapStateToProps= state => {
  return {
    patientInfo:state.patient.patientInfo,
    enquiry:state.enquiry,
    loginInfo: {
      ...state.login.loginInfo,
      clinic:{
        code:state.login.clinic.clinicCd
      },
      service:{
        code:state.login.service.serviceCd
      }
    }
  };
};

const mapDispatchToProps = dispatch=>{return {dispatch};};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Enquiry));
