
export const styles = {
  inline: {
    display: 'inline-block',
    width: 148,
    fontWeight: 'bold'
  },
  margin:{
    marginLeft:0
  },
  redFont:{
    display: 'inline-block',
    width: 148,
    color: 'red',
    fontWeight: 'bold'
  },
  display:{
    display:'none'
  },
  marginLeft:{
    marginLeft:150
  },
  itemRoot:{
    paddingTop:0
  }
};


