import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { styles } from './JOSListStyle';
import { withStyles } from '@material-ui/core/styles';
import { Typography, ListItem,List } from '@material-ui/core';
import ListItemText from '@material-ui/core/ListItemText';

class JOSList extends Component {
  constructor(props){
    super(props);
    this.state={
      details : {
        urgent: '',
        diagnosis: '',
        clinicCd: '',
        clinicName: '',
        reportTo: '',
        clinicalNumber: '',
        specimen: '',
        remark: '',
        instruction: '',
        encounterId: '',
        ioeRequestNumber: '',
        requestUser: '',
        requestUserName: '',
        requestDatetime: '',
        question: '',
        test: '',
        requestByName:''
      }
    };
  }

  render() {
    let { detailsState } = this.state;
    const { classes,details=detailsState} = this.props;
    let specimen = details.specimen===null?[]:details.specimen.split('-|-');
    let test = details.test===null?[]:details.test.split('-|-');
    let question = details.question===null?[]:details.question.split('-|-');
    return (
      <List className={classes.root}>
        <ListItem alignItems="flex-end" className={details.urgent>0?null:classes.display} classes={{root: classes.itemRoot}}>
          <ListItemText
              primary={
              <Typography component="div">
                <Typography
                    component="div"
                    variant="body3"
                    className={classes.redFont}
                    color="textPrimary"
                >
                  Urgent
                </Typography>
                <Typography component="span" variant="body3">
                </Typography>
              </Typography>
            }
          />
        </ListItem>
        <ListItem alignItems="flex-start" classes={{root: classes.itemRoot}}>
          <ListItemText
              primary={
              <Typography component="div">
                <Typography
                    component="div"
                    variant="body3"
                    className={classes.inline}
                    color="textPrimary"
                >
                  Request No:
                </Typography>
                <Typography component="span" variant="body3">
                  {details.ioeRequestNumber}
                </Typography>
              </Typography>
            }
          />
        </ListItem>
        {/* <Divider variant="inset" component="li" className={classes.margin}/> */}
        <ListItem alignItems="flex-start" classes={{root: classes.itemRoot}}>
          <ListItemText
              primary={
              <Typography component="div">
                <Typography
                    component="div"
                    variant="body3"
                    className={classes.inline}
                    color="textPrimary"
                >
                  Diagnosis:
                </Typography>
                <Typography component="span" variant="body3">
                  {details.diagnosis}
                </Typography>
              </Typography>
            }
          />
        </ListItem>
        {/* <Divider variant="inset" component="li" className={classes.margin}/> */}
        <ListItem alignItems="flex-start" classes={{root: classes.itemRoot}}>
          <ListItemText
              primary={
              <Typography component="div">
                <Typography
                    component="div"
                    variant="body3"
                    className={classes.inline}
                    color="textPrimary"
                >
                  Request Unit:
                </Typography>
                <Typography component="span" variant="body3">
                  {details.clinicName!==null?details.clinicName:details.clinicCd}
                </Typography>
              </Typography>
            }
          />
        </ListItem>
        {/* <Divider variant="inset" component="li" className={classes.margin}/> */}
        <ListItem alignItems="flex-start" classes={{root: classes.itemRoot}}>
          <ListItemText
              primary={
              <Typography component="div">
                <Typography
                    component="div"
                    variant="body3"
                    className={classes.inline}
                    color="textPrimary"
                >
                  Report To:
                </Typography>
                <Typography component="span" variant="body3">
                  {details.clinicName!==null?details.clinicName:details.clinicCd}
                </Typography>
              </Typography>
            }
          />
        </ListItem>
        {/* <Divider variant="inset" component="li" className={classes.margin}/> */}
        <ListItem alignItems="flex-start" classes={{root: classes.itemRoot}}>
          <ListItemText
              primary={
              <Typography component="div">
                <Typography
                    component="div"
                    variant="body3"
                    className={classes.inline}
                    color="textPrimary"
                >
                  Request By:
                </Typography>
                <Typography component="span" variant="body3">
                  {details.requestUser}
                </Typography>
              </Typography>
            }
          />
        </ListItem>
        {/* <Divider variant="inset" component="li" className={classes.margin}/> */}
        <ListItem alignItems="flex-start" classes={{root: classes.itemRoot}}>
          <ListItemText
              primary={
              <Typography component="div">
                <Typography
                    component="div"
                    variant="body3"
                    className={classes.inline}
                    color="textPrimary"
                >
                  Request Date:
                </Typography>
                <Typography component="span" variant="body3">
                  {moment(details.requestDatetime).format('DD-MMM-YYYY')}
                </Typography>
              </Typography>
            }
          />
        </ListItem>
        {/* <Divider variant="inset" component="li" className={classes.margin}/> */}
        <ListItem alignItems="flex-start" classes={{root: classes.itemRoot}}>
          <ListItemText
              primary={
              <Typography component="div">
                <Typography
                    component="div"
                    variant="body3"
                    className={classes.inline}
                    color="textPrimary"
                >
                   Clinic Ref No:
                </Typography>
                <Typography component="span" variant="body3">
                  {details.clinicalNumber}
                </Typography>
              </Typography>
            }
          />
        </ListItem>
        {/* <Divider variant="inset" component="li" className={classes.margin}/> */}
        <ListItem alignItems="flex-start" classes={{root: classes.itemRoot}}>
          <ListItemText
              primary={
              <Typography component="div">
                <Typography
                    component="div"
                    variant="body3"
                    className={classes.inline}
                    color="textPrimary"
                >
                  Test Profile:
                </Typography>
                {
                  test.map((value,index)=>{
                    return (
                      <Typography key={index} component="span" variant="body3">
                        {value}
                      </Typography>
                    );
                  })
                }
              </Typography>
            }
          />
        </ListItem>
        {/* <Divider variant="inset" component="li" className={classes.margin}/> */}
        <ListItem alignItems="flex-start" classes={{root: classes.itemRoot}}>
          <ListItemText
              primary={
              <Typography component="div">
                <Typography
                    component="div"
                    variant="body3"
                    className={classes.inline}
                    color="textPrimary"
                >
                  Specimen:
                </Typography>
                {
                  specimen.map((value,index)=>{
                    return (
                      <Typography key={index} component="span" variant="body3">
                        {value}
                      </Typography>
                    );
                  })
                }
              </Typography>
            }
          />
        </ListItem>
        {/* <Divider variant="inset" component="li" className={classes.margin}/> */}
        <ListItem alignItems="flex-start" classes={{root: classes.itemRoot}}>
          <ListItemText
              primary={
              <Typography component="div">
                <Typography
                    component="div"
                    variant="body3"
                    className={classes.inline}
                    color="textPrimary"
                >
                  Question:

                </Typography>
                {
                  question.map((value,index)=>{
                    return (
                      <Typography key={index} component="span" variant="body3">
                         {value}
                      </Typography>
                    );
                  })
                }
                <Typography component="span" variant="body3">
                  {details.remark}
                </Typography>
              </Typography>
            }
          />
        </ListItem>
        {/* <Divider variant="inset" component="li" className={classes.margin}/> */}
        <ListItem alignItems="flex-start" classes={{root: classes.itemRoot}}>
          <ListItemText
              primary={
              <Typography component="div">
                <Typography
                    component="div"
                    variant="body3"
                    className={classes.inline}
                    color="textPrimary"
                >
                  Remark:
                </Typography>
                <Typography component="span" variant="body3">
                  {details.remark}
                </Typography>
              </Typography>
            }
          />
        </ListItem>
        {/* <Divider variant="inset" component="li" className={classes.margin}/> */}
        <ListItem alignItems="flex-start" classes={{root: classes.itemRoot}}>
          <ListItemText
              primary={
              <Typography component="div">
                <Typography
                    component="div"
                    variant="body3"
                    className={classes.inline}
                    color="textPrimary"
                >
                  Instruction:
                </Typography>
                <Typography component="span" variant="body3">
                  {details.instruction}
                </Typography>
              </Typography>
            }
          />
        </ListItem>
        {/* <Divider variant="inset" component="li" className={classes.margin}/> */}
      </List>
    );
  }
}

const mapStateToProps = state => {
  return {
    serviceListData: state.clinicalNote.serviceListData,
    service: state.clinicalNote.service,
    loginInfo: state.login.loginInfo
  };
};

const mapDispatchToProps = {

};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(JOSList));
