import { COMMON_STYLE }from '../../../../constants/commonStyleConstant';

export const styles = () => ({
  font_color: {
    fontSize: '1rem',
    color: '#0579c8'
  },
  headRowStyle:{
    height: 50
    // backgroundColor:COMMON_STYLE.TABLE_BGCKGROUNDCOLOR
  },
  headCellStyle:{
    color:'white',
    overflow: 'hidden',
    fontWeight: 'bold',
    fontSize: '1rem',
    fontFamily: 'Arial',
    position:'sticky',
    top:160,
    backgroundColor:COMMON_STYLE.TABLE_BGCKGROUNDCOLOR
  },
  customRowStyle:{
    fontSize: '1rem',
    fontFamily: 'Arial',
    whiteSpace: 'pre-line',
    wordWrap: 'break-word',
    wordBreak: 'break-all'
  },
  wrapper: {
    width: 'calc(100% - 22px)',
    height: 'calc(100% - 167px)',
    position: 'fixed'
  },
  fixedBottom: {
    color: '#6e6e6e',
    position:'fixed',
    bottom: 0,
    width: '100%',
    zIndex: 100,
    backgroundColor: '#FFFFFF',
    right: 30,
    margin: 10
  },
  cardWrapper: {
    margin: '8px 0px 0px 20px',
    marginRight: 20,
    height: 'calc(100% - -8px)',
    overflowY: 'auto'
  },
  favouriteCategoryLabel: {
    padding: 6,
    fontSize: '1rem',
    fontFamily: 'Arial',
    display: 'flex',
    alignItems: 'center'
  },
  favoriteCategory: {
    color: 'rgba(0, 0, 0, 0.7)',
    padding: 5,
    minWidth: 200
  },
  actionBtn: {
    textTransform: 'none'
  },
  label: {
    fontSize: '1.5rem',
    fontFamily: 'Arial',
    paddingLeft: 5
  },
  titleDiv: {
    top:0,
    position:'sticky',
    backgroundColor:'#FFFFFF',
    paddingBottom:16,
    paddingTop:16
  },
  favouriteCategoryDiv: {
    top:68,
    position:'sticky',
    backgroundColor:'#FFFFFF'
  },
  actionDiv: {
    top:118,
    position:'sticky',
    backgroundColor:'#FFFFFF',
    marginTop: 0
  },
  tableDiv: {
    marginTop: -5
  },
  cardContent: {
    paddingTop: 0
  }
});