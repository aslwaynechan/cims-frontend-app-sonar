/*
 * Front-end UI for save/update IOE Service Profile Maintenance Template Ordering
 * Load IOE Service Profile Maintenance Template List Action: [serviceProfileMaintenance.js] componentDidMount -> initData
 * -> [serviceProfileAction.js] getServiceProfileList
 * -> [serviceProfileSaga.js] getServiceProfileList
 * -> Backend API = /ioe/listServiceProfileByType
 * Clear Action: [serviceProfileMaintenance.js] Clear -> handleClear
 * -> [serviceProfileAction.js] getServiceProfileList
 * -> [serviceProfileSaga.js] getServiceProfileList
 * -> Backend API = /ioe/listServiceProfileByType
 * Save Ordering Action: [serviceProfileMaintenance.js] Save Ordering -> handleSave
 * -> [serviceProfileAction.js] saveServiceProfileList
 * -> [serviceProfileSaga.js] saveServiceProfileList
 * -> Backend API = /ioe/saveServiceProfileList
 * Get Lab Test Grouping Template Action: [serviceProfileMaintenance.js] Edit -> handleEdit
 * -> [serviceProfileAction.js] getServiceProfileTemplate
 * -> [serviceProfileSaga.js] getServiceProfileTemplate
 * -> Backend API = /ioe/getServiceProfileById
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { styles } from './serviceProfileMaintenanceStyle';
import { withStyles, Card, CardContent, Typography, Button, Grid} from '@material-ui/core';
import { AddCircle, Edit, ArrowUpwardOutlined, ArrowDownward } from '@material-ui/icons';
// import CIMSButton from '../../../../components/Buttons/CIMSButton';
import CIMSTable from '../../../../components/Table/CimsTableNoPagination';
import ServiceProfileDialog from '../../../../components/IOE/Dialog/ServiceProfile/ServiceProfileDialog';
import moment from 'moment';
import { openCommonMessage } from '../../../../store/actions/message/messageAction';
import {openCommonCircularDialog} from '../../../../store/actions/common/commonAction';
import { SERVICE_PROFILE_MAINTENANCE_CODE } from '../../../../constants/message/IOECode/serviceProfileMaintenanceCode';
import { getServiceProfileList, saveServiceProfileList, getServiceProfileTemplate } from '../../../../store/actions/IOE/serviceProfile/serviceProfileAction';
import ValidatorForm from '../../../../components/FormValidator/ValidatorForm';
import CustomizedSelectFieldValidator from '../../../../components/Select/CustomizedSelect/CustomizedSelectFieldValidator';
import { DL_ADMIN_FAVOURITE_CATEGORY,DL_DR_FAVOURITE_CATEGORY } from '../../../../constants/IOE/serviceProfile/serviceProfileConstants';
import { includes,toUpper,concat,toLower,isEqual,filter,isNull } from 'lodash';
import Enum from '../../../../enums/enum';
import { COMMON_OPERATION,COMMON_ACTION_TYPE } from '../../../../constants/common/commonConstants';
import Container from 'components/JContainer';

class serviceProfileMaintenance extends Component {
  constructor(props){
    super(props);
    let userRole = props.loginInfo.userRoleType;
    this.favoriteCategorys = [];
    if (includes(toUpper(userRole),Enum.USER_ROLE_TYPE.COUNTER)) { // TODO C?
      this.favoriteCategorys = concat(this.favoriteCategorys,DL_ADMIN_FAVOURITE_CATEGORY);
    } else if (includes(toUpper(userRole),Enum.USER_ROLE_TYPE.DOCTOR)) { //D
      this.favoriteCategorys = concat(this.favoriteCategorys,DL_DR_FAVOURITE_CATEGORY);
    }
    this.state={
      serviceCd:props.loginInfo.service.code,
      templateTypeCd:this.favoriteCategorys.length>0?this.favoriteCategorys[0].value:null,
      isOpen: false,
      dialogIsCreateMode: true,
      selectedObj: null,
      templateList: [],
      // getSelectRow: null,
      pageNum: null,
      selectRow: null,
      templateSeq: null,
      tipsListSize: parseInt(props.sysConfig.TMPL_TIPS_SIZE.value),
      isSave: true,
      tableRows: [
        { name: 'templateSeq',width: '3%', label:'Seq'},
        { name: 'templateName', width: 'auto', label: 'Template Name' },
        { name: 'isActive', width: '10%', label: 'Active'  ,   customBodyRender: value => {
          return value===0 ||value==='0'? 'No' : (value===1 ||value==='1'?'Yes':'No');
          }
        },
        { name: 'updatedBy', width: '10%', label: 'Updated By' },
        { name: 'updatedDtm', label: 'Updated On', width: '10%', customBodyRender: (value) => {
            return value ? moment(value).format('DD-MMM-YYYY') : null;
          }
        }
      ],
      tableOptions: {
        rowHover: true,
        rowsPerPage:5,
        onSelectIdName:'templateSeq', //显示tips的列
        tipsListName:'ioeTestTemplateItems', //显示tips的list
        tipsDisplayListName:'codeIoeFormItem', //显示tips的列
        tipsDisplayName:'frmItemName',//显示tips的值
        // onSelectedRow:(rowId,rowData,selectedData)=>{
        //   this.selectTableItem(selectedData);
        // },
        bodyCellStyle:this.props.classes.customRowStyle,
 	    	headRowStyle:this.props.classes.headRowStyle,
        headCellStyle:this.props.classes.headCellStyle
      },
      dialogExtraProps: {
        maxSeq: 0,
        maxVersion: null,
        userId: null
      }
    };
  }

  componentDidMount() {
    this.props.ensureDidMount();
    this.initData();
  }

  UNSAFE_componentWillReceiveProps(nextProps){
    let {templateList} = this.state;
    if (!isEqual(templateList,nextProps.serviceProfileTemplateList)) {
      this.setState({
        templateList:nextProps.serviceProfileTemplateList
      });
    }
  }

  handleFavouriteCategoryChanged = (e) => {
    this.props.getServiceProfileList({
      params:{
        favoriteType:e.value
      },
      callback:(templateList)=>{
        this.setState({
          templateTypeCd: e.value,
          isSave:true,
          templateList: templateList,
          templateSeq: null,
          selectRow:null,
          selectedObj:null,
          isChange: false
        });

      }
    });
  }

  initData = () => {
    let params = {
      favoriteType:this.state.templateTypeCd
    };
    this.props.getServiceProfileList({
      params,
      callback:(templateList)=>{
        this.setState({
          isSave:true,
          templateList: templateList,
          templateSeq: null,
          selectRow:null,
          selectedObj:null,
          isChange: false
        });
      }
    });
  };

  //获得选中行数据
  getSelectRow = (data) => {
    this.setState({
      templateSeq: data.templateSeq,
      selectRow:data.templateSeq,
      selectedObj:data
    });
  }

  //检查是否选中行数据，未选中提示相应提示
  checkSelect = (msgCode,action) => {
    if(this.state.templateSeq===null){
      let payload = {
        msgCode,
        params:[
          { name:'action1', value:action },
          { name:'action2', value:toLower(action) }
        ]
      };
      this.props.openCommonMessage(payload);
    }
  }

  handleCheckSave = (msgCode,action) => {
    let payload = {
      msgCode,
      params:[
        { name:'action', value:action }
      ]
    };
    this.props.openCommonMessage(payload);
  }

  handleAdd = () => {
    let { isSave,templateList } = this.state;
    if(!isSave){ //提示有其他操作判断未保存
      this.handleCheckSave(SERVICE_PROFILE_MAINTENANCE_CODE.TEMPLATE_IN_ACTION,COMMON_OPERATION.ADD);
    } else {
      let maxSeq = 0, maxVersion = null;
      if (templateList.length > 0) {
        let maxObj = templateList[templateList.length-1];
        maxSeq = maxObj.templateSeq;
        maxVersion = maxObj.version;
      }
      this.setState({
        isOpen: true,
        dialogIsCreateMode: true,
        dialogExtraProps:{
          maxSeq,
          maxVersion,
          userId:null
        }
      });
    }
  }

  handleEdit=()=>{
    let { isSave,selectedObj } = this.state;
    if (!isSave) { //提示有其他操作判断未保存
      this.handleCheckSave(SERVICE_PROFILE_MAINTENANCE_CODE.TEMPLATE_IN_ACTION,COMMON_OPERATION.EDIT);
    } else if (!selectedObj) {  //提示未选中行
      let payload = {
        msgCode:SERVICE_PROFILE_MAINTENANCE_CODE.TEMPLATE_BEFORE_PROCEEDING_OTHER_ACTION,
        params:[
          { name:'action1', value:COMMON_OPERATION.EDIT },
          { name:'action2', value:toLower(COMMON_OPERATION.EDIT) }
        ]
      };
      this.props.openCommonMessage(payload);
    } else {
      this.props.openCommonCircularDialog();
      this.props.getServiceProfileTemplate({
        params: {
          ioeTestTemplateId: selectedObj.ioeTestTemplateId
        },
        callback: () => {
          this.setState({
            isOpen: true,
            dialogIsCreateMode: false,
            dialogExtraProps:{
              maxSeq:0,
              maxVersion:null,
              userId:selectedObj.userId
            }
          });
        }
      });
    }
  }

  handleDown= () => {
    this.checkSelect(SERVICE_PROFILE_MAINTENANCE_CODE.TEMPLATE_BEFORE_PROCEEDING_OTHER_ACTION,COMMON_OPERATION.DOWN);
    let {templateList,templateSeq} = this.state;
    if(templateSeq){
      let index = templateSeq-1;
      if (templateList[index+1]) {
        templateList[index].templateSeq = templateSeq+1;
        templateList[index].operationType = COMMON_ACTION_TYPE.UPDATE;
        templateList[index+1].templateSeq = templateSeq;
        templateList[index+1].operationType = COMMON_ACTION_TYPE.UPDATE;
        [templateList[index],templateList[index+1]] = [templateList[index+1],templateList[index]];
        this.setState({
          templateSeq:templateSeq+1,
          selectRow:templateSeq+1,
          templateList : templateList,
          isSave:false,
          isChange: true
        });
      }
    }
  }

  handleUp= () => {
    this.checkSelect(SERVICE_PROFILE_MAINTENANCE_CODE.TEMPLATE_BEFORE_PROCEEDING_OTHER_ACTION,COMMON_OPERATION.UP);
    let {templateList,templateSeq} = this.state;
    if(templateSeq){
      let index = templateSeq-1;
      if ( templateList[index-1]) {
        templateList[index].templateSeq = index;
        templateList[index].operationType = COMMON_ACTION_TYPE.UPDATE;
        templateList[index-1].templateSeq = templateSeq;
        templateList[index-1].operationType = COMMON_ACTION_TYPE.UPDATE;
        [templateList[index],templateList[index-1]] = [templateList[index-1],templateList[index]];
        this.setState({
          templateSeq:index,
          selectRow:index,
          templateList : templateList,
          isSave:false,
          isChange: true
        });
      }
    }
  }

  // Save Ordering action
  handleSave = () => {
    let {templateTypeCd} = this.state;
    let resultObj = this.generateResultDtos();
    this.props.openCommonCircularDialog();
    this.props.saveServiceProfileList({
      params:resultObj,
      callback:(data)=>{
        this.props.getServiceProfileList({
          params:{
            favoriteType:templateTypeCd
          },
          callback:(templateList)=>{
            this.setState({
            isSave:true,
            templateList: templateList,
            templateSeq: null,
            selectRow:null,
            selectObj:null,
            isChange: false
            });
          }
        });
        this.props.openCommonMessage({
          msgCode:data.msgCode,
          showSnackbar:true
        });
      }
    });
  }

  handleClear = () => {
    let isChange = this.state.isChange;
    if (isChange){
      let payload = {
        msgCode :SERVICE_PROFILE_MAINTENANCE_CODE.TEMPLATE_IS_CLEAR,
        btnActions : {
          btn1Click: () => {
            this.initData();
          }
        }
      };
      this.props.openCommonMessage(payload);
    }else {
      this.initData();
    }
  }

  handleDialogCancel = () =>{
    this.setState({
      isOpen:false
    });
  }

  generateResultDtos = () => {
    let {templateList} = this.state;
    let dtos = [];
    dtos = filter(templateList,listItem => {
      return !isNull(listItem.operationType);
    });
    return {
      dtos
    };
  }

  render() {
    const { classes } = this.props;
    let {
      isOpen,
      serviceCd,
      templateTypeCd,
      dialogIsCreateMode,
      dialogExtraProps
      // isChange
    } = this.state;
    let dialogProps = {
      isOpen,
      templateTypeCd,
      dialogIsCreateMode,
      dialogExtraProps,
      dialogTitle:dialogIsCreateMode?'Create Ix Profile':'Edit Ix Profile',
      handleDialogCancel:this.handleDialogCancel
    };
    const buttonBar={
      isEdit:!this.state.isSave,
      // height:'64px',
      position:'fixed',
      buttons:[{
        title:'Save Ordering',
        id:'btn_group_service_profile_save',
        disabled:!this.state.isChange,
        onClick:this.handleSave
      }]
    };

    return (
      <Typography component="div" id="wrapper" className={classes.wrapper}>
        <Card className={classes.cardWrapper}>
          <CardContent classes={{root:classes.cardContent}}>
            <Container buttonBar={buttonBar}>
              {/* title */}
              <Typography component="div" className={classes.titleDiv}>
                <label className={classes.label}>{`Ix Profile Maintenance (${serviceCd})`}</label>
              </Typography>
              {/* Favorite Category */}
              <Typography component="div" className={classes.favouriteCategoryDiv}>
                <ValidatorForm
                    id="favouriteForm"
                    onSubmit={() => {}}
                    ref="form"
                >
                  <Grid container>
                    <label className={classes.favouriteCategoryLabel}>Favourite Category:</label>
                    <Grid item xs={3}>
                      <CustomizedSelectFieldValidator
                          className={classes.favoriteCategory}
                          id={'service_profile_favourite_category'}
                          options={
                            this.favoriteCategorys.map((item) => ({
                              value: item.value,
                              label: item.label
                            }))
                          }
                          onChange={this.handleFavouriteCategoryChanged}
                          value={templateTypeCd}
                      />
                    </Grid>
                  </Grid>
                </ValidatorForm>
              </Typography>
              {/* Table Action */}
              <Typography component="div" className={classes.actionDiv}>
                <Button
                    id="btn_service_profile_add"
                    onClick={this.handleAdd}
                    className={classes.actionBtn}
                >
                  <AddCircle color="primary" />
                  <span className={classes.font_color}>Add</span>
                </Button>
                <Button
                    id="btn_service_profile_edit"
                    onClick={this.handleEdit}
                    className={classes.actionBtn}
                >
                  <Edit color="primary" />
                  <span className={classes.font_color}>Edit</span>
                </Button>
                <Button
                    id="btn_service_profile_up"
                    onClick={this.handleUp}
                    className={classes.actionBtn}
                >
                  <ArrowUpwardOutlined color="primary" />
                  <span className={classes.font_color}>Up</span>
                </Button>
                <Button
                    id="btn_service_profile_down"
                    onClick={this.handleDown}
                    className={classes.actionBtn}
                >
                  <ArrowDownward color="primary" />
                  <span className={classes.font_color}>Down</span>
                </Button>
              </Typography>
              <Typography component="div" className={classes.tableDiv}>
                <CIMSTable
                    data={this.state.templateList}
                    getSelectRow={this.getSelectRow}
                    id="service_profile_table"
                    options={this.state.tableOptions}
                    rows={this.state.tableRows}
                    rowsPerPage={this.state.pageNum}
                    selectRow={this.state.selectRow}
                    style={{marginTop:20}}
                    tipsListSize={this.state.tipsListSize}
                />
              </Typography>
            </Container>
          </CardContent>
          <ServiceProfileDialog {...dialogProps} />
        </Card>
      </Typography>
    );
  }
}

const mapStateToProps = state => {
  return {
    loginInfo:{
      ...state.login.loginInfo,
      service:{
        code:state.login.service.serviceCd
      }
    },
    sysConfig:state.clinicalNote.sysConfig,
    serviceProfileTemplateList:state.serviceProfile.serviceProfileTemplateList
  };
};

const mapDispatchToProps = {
  getServiceProfileTemplate,
  getServiceProfileList,
  saveServiceProfileList,
  openCommonMessage,
  openCommonCircularDialog
};

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(serviceProfileMaintenance));
