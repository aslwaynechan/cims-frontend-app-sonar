
/*
 * Front-end UI for token template Management shows page
 * Load Token Template List Action: [tokenTemplateManagement.js] initData -> getTokenTemplateList
 * -> [tokenTemplateManagementAction.js] getTokenTemplateList
 * -> [tokenTemplateManagementSaga.js] getTokenTemplateList
 * -> Backend API = /ioe/listTokenTemplate
 * Save Action: [tokenTemplateManagement.js] Save -> saveTokenTemplateList
 * -> [tokenTemplateManagementAction.js] saveTokenTemplateList
 * -> [tokenTemplateManagementSaga.js] saveTokenTemplateList
 * -> Backend API = /ioe/saveTokenTemplateList
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import {style} from './tokenTemplateManagementCss/tokenTemplateManagementCss';
import { withStyles } from '@material-ui/core/styles';
import { AddCircle, Edit, ArrowUpwardOutlined, ArrowDownward } from '@material-ui/icons';
import { Card, CardHeader, CardContent, Grid, Button, Typography } from '@material-ui/core';
import CIMSButton from '../../../../components/Buttons/CIMSButton';
import en_US from '../../../../locales/en_US';
import moment from 'moment';
import CIMSTable from '../../../../components/Table/CimsTableNoPagination';
import {  Prompt } from 'react-router-dom';
import FieldConstant from '../../../../constants/fieldConstant';
import { getTokenTemplateList, saveTokenTemplateList } from '../../../../store/actions/IOE/tokenTemplateManagement/tokenTemplateManagementAction';
import { TOKEN_TEMPLATE_MANAGEMENT_CODE } from '../../../../constants/message/IOECode/tokenTemplateManagementCode';
import { openCommonMessage } from '../../../../store/actions/message/messageAction';
import ManageTokenInstruction from '../tokenTemplateManagement/manageTokenInstruction';
import TokenTemplateDialog from './tokenTemplateDialog';
import {openCommonCircularDialog} from '../../../../store/actions/common/commonAction';
import { COMMON_ACTION_TYPE } from '../../../../constants/common/commonConstants';

class tokenTemplateManagement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      serviceCd:JSON.parse(sessionStorage.getItem('service')).serviceCd,//
      // clinicEngDesc:JSON.parse(sessionStorage.getItem('loginInfo')).clinic.engDesc,//暂时无法提供此字段，先写死（by wentao）
      clinicEngDesc:'Kowloon Families Clinic',
      templateList: [],
      isChange: false,
      isSave: true,
      seq: null,
      getSelectRow: null,
      pageNum: null,
      selectRow: null,
      tipsListSize: parseInt(props.sysConfig.TMPL_TIPS_SIZE.value),
	    instructionOpen:false,
      open:false,
      isNew:true,
      ioeTokenTemplateId:'',
      tableRows: [
        { name: 'seq',width: 52, label:'Seq', id:'Seq'},
        { name: 'templateName', width: 260, label: 'Template Name' },
        { name: 'followUpLocation', width: 'auto', label: 'Follow Up Location' },
        { name: 'instructionName', width: 'auto', label: 'Instruction' },
        { name: 'updatedByName', width: 175, label: 'Updated By' },
        { name: 'updatedDtm', label: 'Updated On', width: 175, customBodyRender: (value) => {
            return value ? moment(value).format('DD-MMM-YYYY') : null;
          }
        }
      ],
      tableOptions: {
        rowHover: true,
        rowsPerPage:5,
        onSelectIdName:'seq', //显示tips的列
        tipsListName:'tokenTemplateItems', //显示tips的list
        tipsDisplayListName:'codeIoeFormItem', //显示tips的列
        tipsDisplayName: 'frmItemName', //显示tips的值
        // onSelectedRow:(rowId,rowData,selectedData)=>{
        //   this.selectTableItem(selectedData);
        // },
        bodyCellStyle:this.props.classes.customRowStyle,
 	    	headRowStyle:this.props.classes.headRowStyle,
        headCellStyle:this.props.classes.headCellStyle
           }
    };
  }

  componentDidMount() {
    this.props.ensureDidMount();
    this.initData();
  }

  initData = () => {
    let Params = {};
    this.props.getTokenTemplateList({Params, callback:(templateList)=>{
      this.setState({
        isSave:true,
        templateList: templateList,
        seq: null,
        selectRow:null,
        selectObj:null,
        isChange: false
      });
    }});
  };

  //获得选中行数据
  getSelectRow = (data) => {
      this.setState({
        seq: data.seq,
        selectRow:data.seq,
        selectObj:data
      });
    }
  //检查是否选中行数据，未选中提示相应提示
  checkSelect(msgCode){
    if(this.state.seq===null){
      let payload = {
        msgCode:msgCode,
        btnActions: {
          // Yes
          btn1Click: () => {
          }
        }
      };
      this.props.openCommonMessage(payload);
    }
  }

  handleCheckSave = (code) => {
    let payload = {
      msgCode:code,
      btnActions: {
        // Yes
        btn1Click: () => {
        }
      }
    };
    this.props.openCommonMessage(payload);
  }

  handleClickAdd = () => {
    let isSave = this.state.isSave;
	  if(!isSave){ //提示有其他操作判断未保存
      this.handleCheckSave(TOKEN_TEMPLATE_MANAGEMENT_CODE.SAVE_TEMPLATE_BEFORE_THE_ADD);
    } else {
      this.handleOpenTokenTemplateDialog('I');
      //to do
    }
  }

  handleClickEdit=()=>{
    let isSave = this.state.isSave;
    let selectObj=this.state.selectObj;
    if(!isSave){ //提示有其他操作判断未保存
      this.handleCheckSave(TOKEN_TEMPLATE_MANAGEMENT_CODE.SAVE_TEMPLATE_BEFORE_THE_EDIT);
    }else if(!selectObj){  //提示未选中行
     let payload = {
        msgCode:TOKEN_TEMPLATE_MANAGEMENT_CODE.IS_SELECTED_TEMPLATE_EDIT,
        btnActions: {
          // Yes
          btn1Click: () => {
          }
        }
      };
      this.props.openCommonMessage(payload);
    }
    else {
      this.setState({ioeTokenTemplateId:selectObj.ioeTokenTemplateId});
      this.handleOpenTokenTemplateDialog('U');
      //to do
    }
  }

  handleClickInstruction=()=>{
    let isSave = this.state.isSave;
  if(!isSave){ //提示有其他操作判断未保存
    this.handleCheckSave(TOKEN_TEMPLATE_MANAGEMENT_CODE.SAVE_TEMPLATE_BEFORE_OPEN_TOKEN_INSTRUCTION);
    } else {
      this.setState({
        instructionOpen:true
      });
    }
  }

  handleClickDown= () => {
    let msgCode = TOKEN_TEMPLATE_MANAGEMENT_CODE.IS_SELECTED_TEMPLATE_DOWN;
    this.checkSelect(msgCode);
    let templateList = this.state.templateList;
    let seq = this.state.seq;
    if(seq){
      let index = this.state.seq-1;
      if (templateList[index+1]) {
        templateList[index].seq = seq+1;
        templateList[index+1].seq = seq;
        [templateList[index],templateList[index+1]] = [templateList[index+1],templateList[index]];
        templateList[index].operationType=COMMON_ACTION_TYPE.UPDATE;
        templateList[index+1].operationType=COMMON_ACTION_TYPE.UPDATE;
        this.setState({
          seq:seq+1,
          selectRow:seq+1,
          templateList : templateList,
          isSave:false,
          isChange: true
        });
      }
   }
  }

  handleClickUp= () => {
    let msgCode = TOKEN_TEMPLATE_MANAGEMENT_CODE.IS_SELECTED_TEMPLATE_UP;
    this.checkSelect(msgCode);
    let templateList = this.state.templateList;
    let seq = this.state.seq;
      if(seq){
        let index = this.state.seq-1;
        if ( templateList[index-1]) {
        templateList[index].seq = index;
        templateList[index-1].seq = seq;
        [templateList[index],templateList[index-1]] = [templateList[index-1],templateList[index]];
        templateList[index].operationType=COMMON_ACTION_TYPE.UPDATE;
        templateList[index-1].operationType=COMMON_ACTION_TYPE.UPDATE;
        this.setState({
          seq:index,
          selectRow:index,
          templateList : templateList,
          isSave:false,
          isChange: true
        });
      }
    }
  }

  handleClickSave = () => {
    const templateList = this.state.templateList;
    let params = templateList;
    this.props.openCommonCircularDialog();//打开保存等待页面，后台调用返回后关闭
    this.props.saveTokenTemplateList({params,callback:(data)=>{
      const params = {};
      this.props.getTokenTemplateList({params,
        callback:(templateList)=>{
          this.setState({
          isSave:true,
          templateList: templateList,
          seq: null,
          selectRow:null,
          selectObj:null,
          isChange: false
          });
        }
      });
      let payload = {
        msgCode:data.msgCode,
        showSnackbar:true, //切换左下角（Snackbar）successfully 消息条
        btnActions: {
        }
      };
      this.props.openCommonMessage(payload);
    }});
  }

  handleClickCancel = () => {
    let isChange = this.state.isChange;
    if (isChange){
      let payload = {
        msgCode :TOKEN_TEMPLATE_MANAGEMENT_CODE.IS_CANCEL_CHANGE,
        btnActions : {
          btn1Click: () => {
            this.initData();
          }
        }
      };
      this.props.openCommonMessage(payload);
    }else {
      this.initData();
    }
  }



  handleInstructionDialogCancel = () =>{
    this.setState({
      instructionOpen:false
    });
  }

  handleOpenTokenTemplateDialog=(type)=>{
    if(type==='I'){
      this.setState({
        open:true,
        isNew:true
      });
    }
    else if(type==='U'){
      this.setState({
        open:true,
        isNew:false
      });
    }
  }

  handleCloseTokenTemplateDialog=()=>{
    this.setState({open:false});
}

  render() {
    const { classes} = this.props;
    const { instructionOpen,open,isNew,ioeTokenTemplateId} = this.state;
    let instructionPara={
      instructionOpen:instructionOpen,
      handleInstructionDialogCancel:this.handleInstructionDialogCancel
    };
	  let tokenTemplateDialogParams={
      open:open,
      isNew:isNew,
      handleCloseDialog:this.handleCloseTokenTemplateDialog,
      ioeTokenTemplateId:ioeTokenTemplateId,
      refreshData:this.initData,
      openInstruction:this.handleClickInstruction
    };
    return (
      <div className={classes.wrapper}>
        <Card className={classes.bigContainer}>
          <CardHeader
              titleTypographyProps={{
                style:{
                  fontSize: '1.5rem',
                fontFamily: 'Arial'}
              }}
              title={`${en_US.tokenTemplateManagement.label_title} (${this.state.serviceCd})`}
          />
            <CardContent>
              <ManageTokenInstruction {...instructionPara} />
              <Typography
                  component="div"
                  style={{marginBottom: 15, marginLeft: 5, marginRight: 5, marginTop: 5 }}
              >

                <Grid container
                    style={{ marginTop: -10,marginLeft:-8 }}
                >
                  <label
                      id="tokenTemplateForm_clinic_lable"
                      className={classes.left_Label}
                  >Clinic: {this.state.clinicEngDesc}</label>
                </Grid>

              </Typography>
              <Typography
                  component="div"
                  style={{ marginTop: 0,marginLeft:-7 }}
              >
              <Button id="btn_tokenTemplate_add"
                  onClick={this.handleClickAdd}
                  style={{ textTransform: 'none' }}
              >
                <AddCircle color="primary" />
                <span className={classes.font_color}>Add</span>
              </Button>
              <Button id="btn_tokenTemplate_edit"
                  onClick={this.handleClickEdit}
                  style={{ textTransform: 'none' }}
              >
                <Edit color="primary" />
                <span className={classes.font_color}>Edit</span>
              </Button>
              <Button id="btn_tokenTemplate_up"
                  onClick={this.handleClickUp}
                  style={{ textTransform: 'none' }}
              >
                <ArrowUpwardOutlined color="primary" />
                <span className={classes.font_color}>Up</span>
              </Button>
              <Button id="btn_tokenTemplate_down"
                  onClick={this.handleClickDown}
                  style={{ textTransform: 'none' }}
              >
                <ArrowDownward color="primary" />
                <span className={classes.font_color}>Down</span>
              </Button>
              <Button id="btn_tokenTemplate_instruction"
                  onClick={this.handleClickInstruction}
                  style={{ textTransform: 'none' }}
              >
                <AddCircle color="primary" />
                <span className={classes.font_color}>Instruction</span>
              </Button>
              <TokenTemplateDialog
                  {...tokenTemplateDialogParams}
              ></TokenTemplateDialog>
            </Typography>
            <CIMSTable data={this.state.templateList}
                getSelectRow={this.getSelectRow}
                id="tokenTemplateManagement_table"
                options={this.state.tableOptions}
                rows={this.state.tableRows}
                rowsPerPage={this.state.pageNum}
                selectRow={this.state.selectRow}
                style={{marginTop:20}}
                tipsListSize={this.state.tipsListSize}
                classes={{
                  label:classes.fontLabel
                }}
            />
            </CardContent>
        </Card>
        <Typography component="div" className={classes.fixedBottom}>
          <Grid alignItems="center"
              container
              justify="flex-end"
          >
            <Typography component="div">
              <CIMSButton
                  classes={{
                    label:classes.fontLabel
                  }}
                  color="primary"
                  id="btn_tokenTemplate_save"
                  onClick={() =>this.handleClickSave()}
                  size="small"
              >
                Save
              </CIMSButton>
            </Typography>
            <CIMSButton
                classes={{
                  label:classes.fontLabel
                }}
                color="primary"
                id="btn_tokenTemplate_reset"
                onClick={() =>this.handleClickCancel()}
                size="small"
            >
              Cancel
            </CIMSButton>
          </Grid>
          <Prompt message={FieldConstant.LEAVE_CONFIRM_PROMPT_CONTENT}
              when={!this.state.isSave}
          />
        </Typography>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    tokenTemplateList: state.tokenTemplateManagement.tokenTemplateList,
    sysConfig:state.clinicalNote.sysConfig
  };
}
const mapDispatchToProps = {
    getTokenTemplateList,
    saveTokenTemplateList,
    openCommonMessage,
    openCommonCircularDialog
  };
export default connect(mapStateToProps,mapDispatchToProps)(withStyles(style)(tokenTemplateManagement));
