export const styles = () => ({
  errorHelper: {
    marginTop: 5,
    fontSize: '1rem',
    fontFamily: 'Arial'
  },
  input: {
    fontSize:'1rem',
    width: 225,
    fontFamily: 'Arial'
  }
});
