import React, { Component } from 'react';
import { TextField, FormHelperText, withStyles } from '@material-ui/core';
import {trim} from 'lodash';
import { styles } from './StringTextFieldStyle';

class StringTextField extends Component {
  integerValCheck = val => {
    if (val.length > 300) {
      return true;
    }
    return false;
  }

  handleChange = event => {
    const { valueChange,updateState } = this.props;
    let value = event.target.value;
    let timeRequireFlag = false;
    let timeIllegalFlag = false;
    // check empty
    if (trim(value) === '') {
      timeRequireFlag = true;
      timeIllegalFlag = false;
    } else {
      // check illegal
      if (this.integerValCheck(value)) {
        timeRequireFlag = false;
        timeIllegalFlag = true;
      }
    }
    updateState&&updateState({
      timeRequireFlag,
      timeIllegalFlag
    });
    valueChange&&valueChange(value);
  }

  render() {
    const {
      classes,
      value,
      id='',
      placeholder='',
      timeRequireFlag=false,
      timeIllegalFlag=false
    } = this.props;

    return (
      <div>
        <TextField
            className={classes.input}
            id={`instruction_string_input_${id}`}
            autoCapitalize="off"
            error={timeRequireFlag||timeIllegalFlag?true:false}
            inputProps={{
              maxLength: 300,
              style:{
                fontSize:'1rem',
                fontFamily: 'Arial'
              }
            }}
            InputProps={{
              style:{
                fontSize:'1rem',
                fontFamily: 'Arial'
              }
            }}
            placeholder={placeholder}
            value={value||''}
            onChange={event => {this.handleChange(event);}}
        />
        <FormHelperText
            error
            classes={{
              'error':classes.errorHelper
            }}
        >
          {timeRequireFlag?'This field is required.':(timeIllegalFlag?'The input value cannot exceed 300.':null)}
        </FormHelperText>
      </div>
    );
  }
}

export default withStyles(styles)(StringTextField);
