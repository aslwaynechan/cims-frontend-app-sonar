
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {style} from './tokenTemplateManagementCss/tokenTemplateDialogCss';
import { withStyles } from '@material-ui/core/styles';
import {Grid, Typography,TextField,Divider,FormGroup,FormControlLabel,Checkbox,IconButton} from '@material-ui/core';
import CIMSButton from '../../../../components/Buttons/CIMSButton';
import {  Prompt } from 'react-router-dom';
import FieldConstant from '../../../../constants/fieldConstant';
import { getTokenTmplById,getTokenInsturctsByName,saveTokenTmplate,getCodeIoeFormItems} from '../../../../store/actions/IOE/tokenTemplateManagement/tokenTemplateManagementAction';
import  EditTemplateDialog from '../../../editTemplate/components/EditTemplateDialog';
import { openCommonMessage } from '../../../../store/actions/message/messageAction';
import TokenTemplateDialogSearchInput from './components/TokenTemplateDialogSearchInput';
import {Edit} from '@material-ui/icons';
import { TOKEN_TEMPLATE_MANAGEMENT_CODE } from '../../../../constants/message/IOECode/tokenTemplateManagementCode';


class tokenTemplateDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      serviceCd:JSON.parse(sessionStorage.getItem('service')).serviceCd,//
      // serviceEngDesc:JSON.parse(sessionStorage.getItem('loginInfo')).service.engDesc,//暂时无法提供此字段，先写死（by wentao）
      serviceEngDesc: JSON.parse(sessionStorage.getItem('service')).serviceName,
      templateList: [],
      getSelectRow: null,
      pageNum: null,
      selectRow: null,
      tipsListSize: null,
      templateName:'',
      tokenTemplatefollowUpLocation:'',
      templateDetailList:[],
      tokenTemplateList:[],
      searchRecordList:[],
      searchRecordTotalNums:0,
      isActive:1,
      ioeTokenInstructionId:null,
      instructionName:'',
      isNew: true,
      saveFlag:true,
      changed:false,
      tokenTemplateListFlag:false
  };
}
componentDidMount(){
  this.initData(this.props.isNew,this.props.ioeTokenTemplateId);
  this.setState({
    isNew:this.props.isNew,
    ioeTokenTemplateId:this.props.ioeTokenTemplateId
  });
}

UNSAFE_componentWillReceiveProps(nextProps) {
  if (nextProps.open) {
    if(nextProps.isNew!==this.props.isNew||!nextProps.isNew){
      this.setState({
        isNew:nextProps.isNew,
        ioeTokenTemplateId:nextProps.ioeTokenTemplateId
      });
      this.initData(nextProps.isNew,nextProps.ioeTokenTemplateId);
    }
  }
}


  initData = (isNew,ioeTokenTemplateId) => {
      if(!isNew){
        this.props.getTokenTmplById({
          params:{ioeTokenTemplateId:ioeTokenTemplateId},
          callback:(data)=>{
            this.setState({
              tokenTemplateList:data.data,
              templateDetailList:data.data.codeIoeFormItems,
              templateName:data.data.templateName,
              tokenTemplatefollowUpLocation:data.data.followUpLocation,
              isActive:data.data.isActive,
              ioeTokenInstructionId:data.data.ioeTokenInstructionId,
              instructionName:data.data.instructionName===null?'':data.data.instructionName
            });
          }
          });
      }
      //is new
      else{
        this.setState({
          tokenTemplateList:{},
          templateDetailList:[],
          templateName:'',
          tokenTemplatefollowUpLocation:'',
          ioeTokenInstructionId:null,
          instructionName:''
          //saveFlag:false
        });
        this.props.getCodeIoeFormItems({
          params:{},
          callback:(data)=>{
            this.setState({
              templateDetailList:data.data
            });
          }
        });
      }
      this.props.getTokenInsturctsByName({
        params:{instructDesc:''},
        callback:(data)=>{
          this.setState({
            searchRecordList:data.data
          });
        }
      });

  };

  handleCloseDialog=()=>{
    const {handleCloseDialog}= this.props;
    let {templateDetailList,changed}=this.state;
    if(changed){
      let payload = {
        msgCode: TOKEN_TEMPLATE_MANAGEMENT_CODE.CANCEL_TOKEN_TEMPLATE_CHANGE,
        btnActions: {
          // Yes
          btn1Click: () => {
            templateDetailList.map(obj=>obj.isSelect='N');
            this.setState({
              templateName:'',
              tokenTemplatefollowUpLocation:'',
              instructionName:'',
              isActive:1,
              templateDetailList:templateDetailList,
              changed:false,
              saveFlag:true,
              tokenTemplateListFlag:false
            });
            handleCloseDialog&&handleCloseDialog();
          }
        }
      };
      this.props.openCommonMessage(payload);
    }else{
      this.setState({
        saveFlag:true,
        tokenTemplateListFlag:false
      });
      handleCloseDialog&&handleCloseDialog();
    }
  }

  handleCheckBoxChange=index=>event=>{
    let templateDetailList=this.state.templateDetailList;
    let tokenTemplateList=this.state.tokenTemplateList;
    if (event.target.checked) {
      templateDetailList[index].isSelect='Y';
      this.setState({tokenTemplateListFlag:false});
    }
    else{
      templateDetailList[index].isSelect='N';
    }
    this.setState({
      templateDetailList:templateDetailList,
      tokenTemplateList:tokenTemplateList,
      //saveFlag:false,
      changed:true
      // tokenTemplateListFlag:false,
    });
    for(let i=0;i<templateDetailList.length;i++){
      if(templateDetailList[i].isSelect==='Y'){
        this.setState({tokenTemplateListFlag:false});
        break;
      }else{
        this.setState({tokenTemplateListFlag:true});
      }
    }
	  tokenTemplateList.codeIoeFormItems=templateDetailList;

}

  handleFuzzySearch=(textVal)=>{
    //update instructionName
    let tokenTemplateList=this.state.tokenTemplateList;
    // tokenTemplateList.ioeTokenInstructionId=textVal
    this.setState({
      ioeTokenInstructionId:null,
      tokenTemplateList:tokenTemplateList
    });
    this.props.getTokenInsturctsByName({
      params:{instructDesc:textVal},
      callback:(data)=>{
        this.setState({
          searchRecordList:data.data
        });
      }
    });
  }

  handleIsActiveCheckBox=(event)=>{
    if (event.target.checked) {
      this.setState({
        isActive:1,
        changed:true
      });
    }
    else{
      this.setState({
        isActive:0,
        changed:true
      });
    }
  }

  handleSelectItem = (item) => {
    let tokenTemplateList=this.state.tokenTemplateList;
    tokenTemplateList.ioeTokenInstructionId=item.ioeTokenInstructionId;
    this.setState({
      ioeTokenInstructionId:item.ioeTokenInstructionId,
      tokenTemplateList:tokenTemplateList,
      changed:true
    });
  }

  handleClickSave=()=>{
    let isNew=this.state.isNew?'Y':'N';
    let tokenTemplateList=this.state.tokenTemplateList;
    let templateDetailList=this.state.templateDetailList;
    let tokenTemplateListFlag = this.state.tokenTemplateListFlag;

    for(let i=0;i<templateDetailList.length;i++){
      if(templateDetailList[i].isSelect==='Y'){
        tokenTemplateListFlag = false;
        this.setState({tokenTemplateListFlag:false});
        break;
      }else{
        tokenTemplateListFlag = true;
        this.setState({tokenTemplateListFlag:true});
      }
    }

    if(this.state.templateName.trim()===''||this.state.tokenTemplatefollowUpLocation.trim()===''||tokenTemplateListFlag){
      this.setState({saveFlag:false});
      //judge item true

    }
    else{
    this.props.saveTokenTmplate({
      params:{
        ioeTokenTemplateId:tokenTemplateList.ioeTokenTemplateId,
        codeIoeFormId:tokenTemplateList.codeIoeFormId===undefined?'100060':tokenTemplateList.codeIoeFormId,
        serviceCd:tokenTemplateList.serviceCd===undefined?JSON.parse(sessionStorage.getItem('service')).serviceCd:tokenTemplateList.serviceCd,
        clinicCd:tokenTemplateList.clinicCd===undefined?JSON.parse(sessionStorage.getItem('clinic')).clinicCd:tokenTemplateList.clinicCd,
        templateName:this.state.templateName,
        ioeTokenInstructionId:this.state.ioeTokenInstructionId,
        followUpLocation:this.state.tokenTemplatefollowUpLocation,
        seq:tokenTemplateList.seq,
        isActive:this.state.isActive,
        version:tokenTemplateList.version,
        createdBy:tokenTemplateList.createdBy,
        createdDtm:tokenTemplateList.createdDtm,
        updatedBy:tokenTemplateList.updatedBy,
        updatedDtm:tokenTemplateList.updatedDtm,
        instructionName:tokenTemplateList.instructionName,
        codeIoeFormItems:tokenTemplateList.codeIoeFormItems,
        tokenTemplateItems:tokenTemplateList.tokenTemplateItems,
        updatedByName:tokenTemplateList.updatedByName,
        isNew:isNew
      },
      callback:(data)=>{
        let {handleCloseDialog,refreshData}=this.props;
        let {templateDetailList}=this.state;
        templateDetailList.map(obj=>obj.isSelect='N');
        this.setState({
          templateName:'',
          tokenTemplatefollowUpLocation:'',
          isActive:1,
          templateDetailList:templateDetailList,
          changed:false
        });
        handleCloseDialog&&handleCloseDialog();
        refreshData&&refreshData();
        let payload = {
          msgCode: data.msgCode,
          showSnackbar: true,
          btnActions: {
              // Yes
              btn1Click: () => {
                // let {handleCloseDialog,refreshData}=this.props
                // let {templateDetailList}=this.state;
                // templateDetailList.map(obj=>obj.isSelect='N')
                // this.setState({
                //   templateName:'',
                //   tokenTemplatefollowUpLocation:'',
                //   isActive:1,
                //   templateDetailList:templateDetailList
                // });
                // handleCloseDialog&&handleCloseDialog()
                // refreshData&&refreshData()
              }
          }
      };
      this.props.openCommonMessage(payload);
      }
    });
    this.setState({saveFlag:true});
    }
  }

  inputOnchange=(e)=>{
    this.setState({
      [e.target.name]:e.target.value,
      changed:true
      //saveFlag:false
    });
    // if(!this.state.isNew){
    //   this.setState({
    //    changed:true
    //   });
    // }
  }

  openInstruction=()=>{
    let {openInstruction}=this.props;
    openInstruction&&openInstruction();
  }

  handleEscKeyDown = () =>{
    this.handleCloseDialog();
  }


  render() {
    const {classes,open} = this.props;

    return (
        <EditTemplateDialog
      // dialogContentProps={{ style: { minWidth: 1000} }}
            dialogTitle={'Reminder Template Maintenance'}
            open={open}
            handleEscKeyDown={this.handleEscKeyDown}
        >
            <Typography
                component="div"
                style={{marginBottom: 15, marginLeft: 15, marginRight: 5, marginTop: 15 }}
            >
                <Grid container
                    spacing={8}
                >
                    <Grid item
                        xs={2}
                    >
                <span className={classes.inputStyle}><span style={{ color: 'red' }}>*</span>Template Name</span>
                </Grid>
                <Grid item
                    xs={10}
                >
                <TextField
                    autoCapitalize="off"
                    className={classes.normal_input}
                    //error={this.state.templateName.trim()===''&&this.state.saveFlag?true:false}
                    id={'tokenTemplateDialog_TemplateName'}
                    name="templateName"
                    onChange={this.inputOnchange}
                    style={{width: '80%' }}
                    type="text"
                    value={this.state.templateName}
                    variant="outlined"
                    InputProps={{
                      style:{
                        fontSize:'1rem'
                      }
                     }}
                />
                 {this.state.templateName.trim()===''&&!this.state.saveFlag?(

            <span className={classes.validation}
                id="span_tokenTemplateDialog_displayName_validation"
            >This field is required.</span>
            ):null}
                </Grid>
                </Grid>

                <Grid container
                    spacing={8}
                    style={{marginTop:15}}
                >
                    <Grid  item
                        xs={2}
                    >
                <span className={classes.inputStyle}><span style={{ color: 'red' }}>*</span>Follow up Location</span>
                </Grid>
                <Grid item
                    xs={10}
                >
                <TextField
                    autoCapitalize="off"
                    className={classes.normal_input}
                    //error={this.state.tokenTemplatefollowUpLocation.trim()===''&&this.state.saveFlag?true:false}
                    id={'tokenTemplateDialog_TemplateName'}
                    name="tokenTemplatefollowUpLocation"
                    onChange={this.inputOnchange}
                    style={{width: '80%' }}
                    type="text"
                    value={this.state.tokenTemplatefollowUpLocation}
                    variant="outlined"
                    InputProps={{
                      style:{
                        fontSize:'1rem'
                      }
                     }}
                />
                {this.state.tokenTemplatefollowUpLocation.trim()===''&&!this.state.saveFlag?(
                  <span className={classes.validation}
                      id="span_tokenTemplateDialog_displayName_validation"
                  >This field is required.</span>
                  ):null}
                </Grid>
                </Grid>


                <Grid container
                    spacing={8}
                    style={{marginTop:15,marginBottom:-15}}
                >
                    <Grid item
                        xs={2}
                    >
                <span className={classes.inputStyle}>&nbsp;Instruction</span>
                </Grid>
                <Grid  className={classes.searchGrid}
                    item
                    xs={3}
                >

                <TokenTemplateDialogSearchInput
                    dataList={this.state.searchRecordList}
                    displayField={['instructDesc']}
                    // handleSearchBoxLoadMoreRows={this.handleSearchBoxLoadMoreRows}
                    id="TokenTemplateDialogSearchInput"
                    inputPlaceHolder={'---Please Select---'}
                    limitValue={4}
                    onChange={this.handleFuzzySearch}
                    onSelectItem={this.handleSelectItem.bind(this)}
                    pageSize={30}
                    totalNums={this.state.searchRecordTotalNums}
                    value={this.state.instructionName}
                />
                </Grid>
                <Grid item
                    style={{marginTop:-8}}
                    xs={4}
                >
                  <IconButton id="template_btn_edit"
                      onClick={this.openInstruction}
                      style={{ textTransform: 'none',marginTop:'-6px'}}
                  >
              <Edit/>
              </IconButton>
              </Grid>
                </Grid>

                <Grid container
                    spacing={8}
                    style={{marginTop:15,marginLeft:0}}
                >
                <Grid
                    className={classes.checkBoxGrid}
                    item
                    key={Math.random()}
                    xs={4}
                >
                        <FormControlLabel
                            classes={{
                            label: classes.normalFont
                          }}
                            control={
                            <Checkbox
                                checked={this.state.isActive===0?false:true}
                                color="primary"
                                id="isActiveCheckBox"
                                onChange={this.handleIsActiveCheckBox}
                            />
                          }
                            label={'Active'}
                        />
                      </Grid>
                </Grid>


                <Divider style={{marginTop:12,marginLeft:-4}}/>
                <Typography component="div"
                    style={{marginLeft:-4}}
                >
                  <span className={classes.templateDetailStyle}>Template Detail</span>
                  <Divider />
                <Typography  component="div" style={{paddingTop:'15px'}}>
                 {/* checkbox */}
            <Typography component="div"
                style={{ marginLeft:8}}
            >
              <Grid container>
                <FormGroup className={classes.fullWidth}
                    row
                >
                  {this.state.templateDetailList.map((item,index) => {
                    return (
                      <Grid
                          className={classes.checkBoxGrid}
                          item
                          key={Math.random()}
                          xs={4}
                      >
                        <FormControlLabel
                            classes={{
                            label: classes.normalFont
                          }}
                            control={
                            <Checkbox
                                checked={item.isSelect==='N'?false:true}
                                color="primary"
                                id={item.codeAssessmentCd}
                                onChange={this.handleCheckBoxChange(index)}
                            />
                          }
                            label={item.frmItemName}
                        />
                      </Grid>
                    );
                  })}

                </FormGroup>


              </Grid>
              {this.state.tokenTemplateListFlag?(
                     <div>
                  <span className={classes.templateDetailValidation}
                      id="span_tokenTemplateDialog_displayName_validation"
                  >This field is required.</span>
                  </div>
                  ):<div className={classes.templateDetailValidationFail}></div>}
            </Typography>

                </Typography>
                </Typography>

          </Typography>
          <Typography component="div">
            <Grid alignItems="center"
                container
                justify="flex-end"
            >
              <Typography component="div">
                <CIMSButton
                    classes={{
                      label:classes.fontLabel
                    }}
                    color="primary"
                    id="template_btn_save"
                    onClick={() =>this.handleClickSave()}
                    size="small"
                >
                    Save
                </CIMSButton>
              </Typography>
              <CIMSButton
                  classes={{
                    label:classes.fontLabel
                  }}
                  color="primary"
                  id="template_btn_reset"
                  onClick={() =>this.handleCloseDialog()}
                  size="small"
              >
              Cancel
              </CIMSButton>
            </Grid>

          </Typography>
          <Prompt message={FieldConstant.LEAVE_CONFIRM_PROMPT_CONTENT}
              when={!this.state.isSave}
          />
          </EditTemplateDialog>
    );
  }
}

function mapStateToProps(state) {
  return {
    tokenTemplateList: state.tokenTemplateManagement.tokenTemplateList
  };
}
const mapDispatchToProps = {
  getTokenTmplById,
  getTokenInsturctsByName,
  saveTokenTmplate,
  getCodeIoeFormItems,
  openCommonMessage
  };
export default connect(mapStateToProps,mapDispatchToProps)(withStyles(style)(tokenTemplateDialog));
