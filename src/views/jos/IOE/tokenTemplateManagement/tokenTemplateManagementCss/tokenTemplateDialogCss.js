
export const style = {
    inputStyle: {
        fontWeight:400,
        fontSize:'1rem',
        fontFamily:'Arial'
    },
    templateDetailStyle:{
        fontWeight:'bold',
        fontSize:'1rem',
        fontFamily:'Arial'
    },
    fullWidth: {
        width: '100%'
      },
      checkBoxGrid: {
        marginTop: '-10px'
      },
      normalFont: {
        fontSize: '1rem',
        fontFamily: 'Arial'
      },
      searchGrid:{
        maxWidth:'39%',
        marginTop:-8
      },
      validation : {
        color: '#fd0000',
        margin: '0',
        fontSize: '0.75rem',
        float:'left',
       // marginTop: '8px',
        minHeight: '1em',
        display: 'block',
        marginTop:4,
        marginLeft:12
    },
    templateDetailValidation : {
      color: '#fd0000',
      margin: '0',
      fontSize: '0.75rem',
      float:'left',
     // marginTop: '8px',
      minHeight: '1em',
      display: 'block',
      marginTop:4
  },

  templateDetailValidationFail : {
    margin: '0',
    fontSize: '0.75rem',
    float:'left',
   // marginTop: '8px',
    minHeight: '1em',
    display: 'block',
    marginTop:4,
    height:18
},

    normal_input:{
      float: 'left'
    },
    fontLabel: {
      fontSize: '1rem'
    }
};