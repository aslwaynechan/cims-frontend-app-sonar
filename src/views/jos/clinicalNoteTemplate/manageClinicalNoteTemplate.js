/*
 * Front-end UI for clinical note template management
 * Load favourite list Action: [manageClinicalNoteTemplate.js] initData ->
 * -> [medicalSummarySaga.js] requestFavouriteList
 * -> Backend API = clinical-note/getClinicalNoteTmplTypeList
 * Load template list Action: [manageClinicalNoteTemplate.js] initData ->
 * -> [medicalSummarySaga.js] requestTemplateList
 * -> Backend API = clinical-note/listClinicalNoteTemplatesByType
 * Save template list Action: [manageClinicalNoteTemplate.js] recordListSave ->
 * -> [medicalSummarySaga.js] recordTemplateData
 * -> Backend API = clinical-note/reorderClinicalNoteTmpls
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Grid,
  Button,
  Table,
  TableRow,
  Typography,
  TableCell,
  TableHead,
  TableBody,
  Card,
  CardContent
} from '@material-ui/core';
import en_US from '../../../locales/en_US';
import { withStyles } from '@material-ui/core/styles';
import { AddCircle } from '@material-ui/icons';
import { RemoveCircle } from '@material-ui/icons';
import { Edit } from '@material-ui/icons';
import { ArrowUpwardOutlined } from '@material-ui/icons';
import { ArrowDownward } from '@material-ui/icons';
import moment from 'moment';
import * as manageClinicalNoteTemplateActionType from '../../../store/actions/clinicalNoteTemplate/manageClinicalNoteTemplateActionType';
import * as messageTypes from '../../../store/actions/message/messageActionType';
import 'react-quill/dist/quill.snow.css';
import CustomizedSelectFieldValidator from '../../../components/Select/CustomizedSelect/CustomizedSelectFieldValidator';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import EditTemplate from './editTemplate';
import {MANAGE_TEMPLATE_CODE} from '../../../constants/message/manageTemplateCode';
import * as commonTypes from '../../../store/actions/common/commonActionType';
import { CLINICAL_NOTE_TEMPLATE_TYPE,ACTION_TYPE } from '../../../constants/clinicalNote/clinicalNoteConstants';
import {style} from './manageClinicalNoteTemplateCss';
import { COMMON_STYLE }from '../../../constants/commonStyleConstant';
import classNames from 'classnames';
import Container from 'components/JContainer';

class manageClinicalNoteTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // favoriteValue: 'My Service',
      selectRowObj: null,
      sequence: null,
      open: false,  // open dialog
      codeClinicalnoteTmplTypeCd: CLINICAL_NOTE_TEMPLATE_TYPE.MY_FAVOURITE,
      isEdit:false,
      templateList:[],
      selectObj : {
        templateName : '',
        templateText : ''
      },
      upDownClick:false,
      editTemplateList:[],
      pageNum:null,
      deleteList:[],
      tableRows: [
        { name: 'sequence',width: 42, label:'Seq'},
        { name: 'serviceCd', width: 'auto', label: 'Service' },
        { name: 'templateName', width: 'auto', label: 'Name' },
        { name: 'templateText', width: 'auto', label: 'Text' },
        { name: 'updatedByName', width: 180, label: 'Updated By' },
        {
          name: 'updatedDtm', label: 'Updated On',  width: 140, customBodyRender: (value) => {
              return value ? moment(value).format('DD-MMM-YYYY') : null;
          }
      }
    ],
    tableOptions: {
        rowHover: true,
        rowsPerPage:5,
        onSelectIdName:'sequence',
        tipsListName:'diagnosisTemplates',
        tipsDisplayListName:null,
        tipsDisplayName:'diagnosisDisplayName',
        onSelectedRow:(rowId,rowData,selectedData)=>{
            this.selectTableItem(selectedData);
        },
        bodyCellStyle:this.props.classes.customRowStyle,
        headRowStyle:this.props.classes.headRowStyle,
        headCellStyle:this.props.classes.headCellStyle
    }

    };
  }

  componentDidMount() {
    this.props.ensureDidMount();
    this.initData();
  }

  initData = () => {
    let codeClinicalnoteTmplTypeCd = this.state.codeClinicalnoteTmplTypeCd;
    if(JSON.parse(sessionStorage.getItem('loginInfo')).admin_login){
      codeClinicalnoteTmplTypeCd = CLINICAL_NOTE_TEMPLATE_TYPE.SERVICE_FAVOURITE;
    }
    const params = {};
    this.props.dispatch({ type: manageClinicalNoteTemplateActionType.REQUEST_DATA, params });//拿到favorite Category List
    params.clinicalNoteTmplType = codeClinicalnoteTmplTypeCd;  //拿到templateList需要的条件clinicalNoteTmplType
    this.props.dispatch({ type: manageClinicalNoteTemplateActionType.TEMPLATE_DATA, params,callback:(templateList)=>{
      this.setState({
        codeClinicalnoteTmplTypeCd : codeClinicalnoteTmplTypeCd,
        templateList: templateList
       });
    }});

  };

  handleClickDeleteValidate = () => {
    let params=this.state.selectRowObj;
    if(params){
      if(!this.state.upDownClick){
        this.handleSave();
      }else{
        this.checkUpDown();
      }
    }else{
		  let payload = { msgCode:MANAGE_TEMPLATE_CODE.IS_SELECTED_DELETE};
      this.props.dispatch({ type: messageTypes.OPEN_COMMON_MESSAGE, payload });
    }
  };

  handleClose= () =>  {
    this.setState({ open: false });
    this.props.dispatch({ type: commonTypes.CLOSE_COMMON_CIRCULAR_DIALOG});
  }

  handleDialogClose=()=>{
    this.handleClose();
    this.reloadList();
  }

  favoriteValueOnChange = (e) => {
    const {upDownClick,codeClinicalnoteTmplTypeCd}=this.state;
    const params = { clinicalNoteTmplType: e.value };
    if(e.value===codeClinicalnoteTmplTypeCd){
        return false;
    }
    if(upDownClick){
        let payload = {
            msgCode:MANAGE_TEMPLATE_CODE.IS_CHANGE_CATEGORY,
            btnActions: {
            // Yes
              btn1Click: () => {
                this.props.dispatch({ type: manageClinicalNoteTemplateActionType.REQUEST_DATA, params });//获取favoriteList的数据
                this.props.dispatch({ type: manageClinicalNoteTemplateActionType.TEMPLATE_DATA, params,callback:(templateList)=>{
                  this.setState({
                    templateList: templateList,
                    isEdit:false,
                    upDownClick:false,
                    codeClinicalnoteTmplTypeCd: e.value,
                    sequence:null,
                    selectRowObj:null
                   });
                }});
              }
            }
        };
        this.props.dispatch({ type: messageTypes.OPEN_COMMON_MESSAGE, payload });
    }else{
    this.props.dispatch({ type: manageClinicalNoteTemplateActionType.REQUEST_DATA, params });//获取favoriteList的数据
    this.props.dispatch({ type: manageClinicalNoteTemplateActionType.TEMPLATE_DATA, params,callback:(templateList)=>{
      this.setState({
        templateList: templateList,
        isEdit:false,
        upDownClick:false,
        codeClinicalnoteTmplTypeCd: e.value,
        sequence:null,
        selectRowObj:null
       });
    }});
    }

  }

  //重新加载列表数据
  reloadList = () => {
    let {upDownClick} = this.state;
    if (upDownClick) {
      let payload = {
        msgCode: MANAGE_TEMPLATE_CODE.IS_CANCEL_CHANGE,
        btnActions: {
          // Yes
          btn1Click: () => {
            this.setState({
              upDownClick:false,
              sequence:null,
              selectRowObj:null
            });
            const params = { clinicalNoteTmplType: this.state.codeClinicalnoteTmplTypeCd};
            this.props.dispatch({ type: manageClinicalNoteTemplateActionType.TEMPLATE_DATA, params,callback:(templateList)=>{
              this.setState({
                open:false,
                templateList: templateList
                });
            }});
          }
        }
      };
      this.props.dispatch({
        type: messageTypes.OPEN_COMMON_MESSAGE,
        payload
      });
    } else {
      this.setState({
        upDownClick:false,
        sequence:null,
        selectRowObj:null
      });
      const params = { clinicalNoteTmplType: this.state.codeClinicalnoteTmplTypeCd};
      this.props.dispatch({ type: manageClinicalNoteTemplateActionType.TEMPLATE_DATA, params,callback:(templateList)=>{
        this.setState({
          open:false,
          templateList: templateList
          });
      }});
    }
  }

  //获得选中行数据
  getSelectTemplate = (e) => {
    this.setState({
      sequence: e.item.sequence,
      selectRowObj:e.item,
      selectObj: {
        templateName : e.item.templateName,
        templateText : e.item.templateText
      }
    });
  }


  recordListSave = () =>{
    let params=this.state.templateList;
    this.setState({
      upDownClick:false,
      sequence:null,
      selectRowObj:null
    });
    this.props.dispatch({ type: commonTypes.OPEN_COMMON_CIRCULAR_DIALOG});
    this.props.dispatch({ type: manageClinicalNoteTemplateActionType.RECORDLIST_DATA , params,callback:(data)=>{
    let params = { clinicalNoteTmplType: this.state.codeClinicalnoteTmplTypeCd};
    let payload = {
      msgCode:data.msgCode,
      showSnackbar:true,
      btnActions: {
    // Yes
        btn1Click: () => {
          this.props.dispatch({ type: manageClinicalNoteTemplateActionType.TEMPLATE_DATA, params,callback:(templateList)=>{
            this.setState({
              upDownClick:false,
              templateList: templateList,
              sequence:null,
              selectRowObj:null
            });
            }
          });
        }
      }
    };
      this.props.dispatch({ type: messageTypes.OPEN_COMMON_MESSAGE, payload });
    }});

  }

  handleClickDelete=()=>{
    let selectRowObj=JSON.parse(JSON.stringify(this.state.selectRowObj));
    let params=JSON.parse(JSON.stringify(this.state.templateList));
    for (let index = 0; index < params.length; index++) {
      const element = params[index];
      if(element.clinicalnoteTemplateId===selectRowObj.clinicalnoteTemplateId){
        params[index].deleteInd='Y';
      }
    }
    this.props.dispatch({ type: commonTypes.OPEN_COMMON_CIRCULAR_DIALOG});
      this.props.dispatch({ type: manageClinicalNoteTemplateActionType.DELETETEMPLATE_DATA , params ,callback:(data)=>{
        this.reloadList();
        let payload = {
          msgCode:data.msgCode,
          showSnackbar:true,
          btnActions: {
            // Yes
            btn1Click: () => {

            }
          }
        };
          this.props.dispatch({ type: messageTypes.OPEN_COMMON_MESSAGE, payload });
      }});
  }

  handleSave = () => {
    let payload = {
      msgCode:MANAGE_TEMPLATE_CODE.IS_DELETE_MANAGE_TEMPLATE,
      btnActions: {
        // Yes
        btn1Click: () => {
          this.handleClickDelete();
        },
        btn2Click:()=>{

          this.handleClose();
        }
      }
    };
    this.props.dispatch({ type: messageTypes.OPEN_COMMON_MESSAGE, payload });
  }
  checkUpDown = () => {
    let payload = {
      msgCode:MANAGE_TEMPLATE_CODE.IS_SAVE_COMFIRM,
      btnActions: {
        // Yes
        btn1Click: () => {
        }
      }
    };
    this.props.dispatch({ type: messageTypes.OPEN_COMMON_MESSAGE, payload });
  }

  checkSelect = (msgCode) => {
    let payload = {
      msgCode:msgCode,
      showSnackbar:false,
      btnActions: {
        // Yes
        btn1Click: () => {
        }
      }
    };
    this.props.dispatch({ type: messageTypes.OPEN_COMMON_MESSAGE, payload });
  }

  clickDownClick= () => {
    let rowObj={};
    rowObj=this.state.selectRowObj;
    if(!rowObj){
      let msgCode = MANAGE_TEMPLATE_CODE.IS_SELECTED_DOWN;
      this.checkSelect(msgCode);
    }
    let recordList=[];
    recordList = JSON.parse(JSON.stringify(this.state.templateList));
    if(rowObj && recordList[rowObj.sequence]){
      recordList[rowObj.sequence].operationType=ACTION_TYPE.UPDATE;
      recordList[rowObj.sequence-1].operationType=ACTION_TYPE.UPDATE;
      recordList[(rowObj.sequence-1)].sequence=recordList[(rowObj.sequence-1)].sequence+1;
      recordList[rowObj.sequence].sequence=recordList[rowObj.sequence].sequence-1;
      let tempa=[];
      tempa=recordList[rowObj.sequence-1];
	    recordList[rowObj.sequence-1]=recordList[rowObj.sequence];
      recordList[rowObj.sequence]=tempa;
      this.setState({
        upDownClick:true,
        templateList:recordList,
        sequence:recordList[rowObj.sequence].sequence,
        selectRowObj:recordList[rowObj.sequence]
      });
    }else{
      return false;
    }
  }

  clickUpClick= () => {
    let rowObj={};
    rowObj=this.state.selectRowObj;
    if(!rowObj){
      let msgCode = MANAGE_TEMPLATE_CODE.IS_SELECTED_UP;
      this.checkSelect(msgCode);
    }
    let recordList=[];
    recordList = JSON.parse(JSON.stringify(this.state.templateList));
    if(rowObj && recordList[(rowObj.sequence-2)]){
      recordList[rowObj.sequence-1].operationType=ACTION_TYPE.UPDATE;
      recordList[rowObj.sequence-2].operationType=ACTION_TYPE.UPDATE;
      recordList[(rowObj.sequence-1)].sequence=recordList[(rowObj.sequence-1)].sequence-1;
      recordList[rowObj.sequence-2].sequence=recordList[rowObj.sequence-2].sequence+1;
      let tempa=[];
      tempa=recordList[rowObj.sequence-2];
	    recordList[rowObj.sequence-2]=recordList[rowObj.sequence-1];
      recordList[rowObj.sequence-1]=tempa;
      this.setState({
        upDownClick:true,
        templateList:recordList,
        sequence:recordList[rowObj.sequence-2].sequence,
        selectRowObj:recordList[rowObj.sequence-2]
      });
    }else{
      return false;
    }
  }

  handleClickAdd = () => {
    if(!this.state.upDownClick){
      this.setState({
        selectObj :{
          templateName : null ,
          templateText : null
        },
        open: true
        });
      }else {
        this.checkUpDown();
      }
  };

  handleClickEdit = () => {
    if(!this.state.upDownClick){
      let params=this.state.selectRowObj;
      if(params==null){
        let msgCode = MANAGE_TEMPLATE_CODE.IS_SELECTED_EDIT;
        this.checkSelect(msgCode);
        return;
      }
      this.setState({
        open: true,
        selectObj :{
          templateName : this.state.selectRowObj.templateName ,
          templateText : this.state.selectRowObj.templateText
        }
      });
    }else {
      this.checkUpDown();
    }
  };

  render() {
    const { classes } = this.props;
    let editTemplateProps = {
      refreshEditList:this.refreshEditList,
      refreshList:this.refreshList,
      handleDialogClose:this.handleDialogClose
    };
    let disabled = this.state.upDownClick;

    const buttonBar={
        isEdit:disabled,
        // height:'64px',
        position:'fixed',
        buttons:[{
          title:'Save',
          onClick:this.recordListSave,
          id:'default_save_button'
        }]
      };
    return (
    //   <div className={classes.wrapper}>
     <Container className={classes.wrapper} buttonBar={buttonBar}  >
        <Card className={classes.cardContainer}>

          <CardContent>
            {/* <CardHeader
                titleTypographyProps={{
                  style:{
                    fontSize: '1.5rem',
                    fontFamily: 'Arial'
                  }
                }}
                title="Clinical Note Template Maintenance"
            /> */}
            <Typography
                component="div"
                className={classes.label_div}
            >
              <label style={{ fontSize: '1.5rem',fontFamily: 'Arial'}}>Clinical Note Template Maintenance</label>
            </Typography>

            <Typography
                component="div"
                style={{margin: '5px 0px 15px' }}
                className={classes.select_div}
            >
              <ValidatorForm
                  id="manageTemplateForm"
                  onSubmit={() => { }}
                  ref="form"
              >
                <Grid container style={{ marginTop: 10 }}>
                  <label className={classes.left_Label}>{en_US.manageTemplate.label_favorite_category}</label>
                  <Grid item
                      style={{ padding: 0 }}
                      xs={2}
                  >
                    <CustomizedSelectFieldValidator className={classes.favorite_category}
                        id={'bookingEncounterTypeSelectField'}
                        msgPosition="bottom"
                        options={this.props.favoriteCategoryListData.map((item) => ({ value: item.codeClinicalnoteTmplTypeCd, label: item.templateTypeDesc }))}
                        onChange={this.favoriteValueOnChange}
                        value={this.state.codeClinicalnoteTmplTypeCd}
                        width={'none'}
                        // rest={}
                    />
                  </Grid>
                </Grid>
              </ValidatorForm>
            </Typography>
            <Typography
                component="div"
                className={classes.btn_div}
            >
              <Button id="template_btn_add" style={{ textTransform: 'none' }}  onClick={this.handleClickAdd}>
                <AddCircle color="primary" />
                <span className={classes.font_color}>Add</span>
              </Button>
              <Button  id="template_btn_edit"  style={{ textTransform: 'none' }} onClick={this.handleClickEdit}>
                <Edit color="primary" />
                <span className={classes.font_color}>Edit</span>
              </Button>
              <Button  id="template_btn_delete"  style={{ textTransform: 'none' }} onClick={this.handleClickDeleteValidate}>
                <RemoveCircle color="primary" />
                <span className={classes.font_color}>Delete</span>
              </Button>
              <Button id="template_btn_up" style={{ textTransform: 'none' }} onClick={this.clickUpClick}>
                <ArrowUpwardOutlined color="primary" />
                <span className={classes.font_color}>Up</span>
              </Button>
              <Button id="template_btn_down" style={{ textTransform: 'none' }}  onClick={this.clickDownClick} >
                <ArrowDownward color="primary" />
                <span className={classes.font_color}>Down</span>
              </Button>
              <EditTemplate  style={{ height: 522}}  open={this.state.open}
                  initData={this.initData}
                  codeClinicalnoteTmplTypeCd={this.state.codeClinicalnoteTmplTypeCd}
                  {...editTemplateProps} selectObj={this.state.selectObj} selectRowObj={this.state.selectRowObj} templateList={this.state.templateList}
              />
            </Typography>
            {/* <Typography
                component="div"
                style={{overflowY:'auto',height:538}}
            > */}
              <Table id="manage_template_table" className={classes.table_itself}>
                <TableHead>
                  <TableRow style={{backgroundColor:COMMON_STYLE.TABLE_BGCKGROUNDCOLOR}} className={classes.table_head}>
                    <TableCell
                        style={{width: '4%'}}
                        className={classNames(classes.table_header,classes.tableCellBorder)}
                        padding={'none'}
                    >
                      Seq
                    </TableCell>
                    <TableCell
                        style={{width: '6%'}}
                        className={classNames(classes.table_header,classes.tableCellBorder)}
                        padding={'none'}
                    >
                      Service
                    </TableCell>
                    <TableCell
                        style={{width: '25%'}}
                        className={classNames(classes.table_header,classes.tableCellBorder)}
                        padding={'none'}
                    >
                      Name
                    </TableCell>
                    <TableCell className={classNames(classes.table_header,classes.tableCellBorder)}
                        style={{width: '25%'}}
                    >
                      Text
                    </TableCell>
                    <TableCell className={classNames(classes.table_header,classes.tableCellBorder)}
                        style={{paddingLeft:10,width: '10%'}}
                        padding={'none'}
                    >
                      Updated&nbsp;By
                    </TableCell>
                    <TableCell className={classNames(classes.table_header,classes.tableCellBorder)}
                        style={{width: '10%'}}
                    >
                      Updated&nbsp;On
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.templateList.length>0?this.state.templateList.map((item, index) => (
                    <TableRow
                        className={
                        item.sequence === this.state.sequence ? classes.table_row_selected : classes.table_row
                      }
                        key={index}
                        onClick={() => this.getSelectTemplate({ item })}
                    >
                      <TableCell style={{width: '5%'}} padding={'none'} className={classNames(classes.table_cell,classes.tableCellBorder)}>
                        {(item.sequence)}
                      </TableCell>
                      <TableCell padding={'none'}
                          style={{width: '5%'}}
                          className={classNames(classes.table_cell,classes.tableCellBorder)}
                      >
                        {(item.serviceCd)}
                      </TableCell>
                      <TableCell padding={'none'}
                          style={{width: '24%'}}
                          className={classNames(classes.table_cell,classes.tableCellBorder)}
                      >
                        {item.templateName}
                      </TableCell>
                      <TableCell padding={'none'}
                          className={classNames(classes.cell_text,classes.tableCellBorder)}
                          style={{paddingLeft:10,width: '20%'}}
                      >
                        {item.templateText}
                      </TableCell>
                      <TableCell
                          className={classNames(classes.cell_text,classes.tableCellBorder)}
                          style={{paddingLeft:10,width: '10%',wordBreak:'normal'}}
                      >
                        {item.updatedByName}
                      </TableCell>
                      <TableCell
                          className={classNames(classes.table_cell_1,classes.tableCellBorder)}
                          style={{paddingLeft:10,width: '10%'}}
                      >
                        {moment(item.updatedDtm).format('DD-MMM-YYYY')}
                      </TableCell>
                    </TableRow>
                  )):
                  <TableRow >
                    <TableCell classes={{root:classes.tbNoData}} className={classes.tbNoData} align="center"   colSpan={6}>There is no data.</TableCell>
                  </TableRow>}
                </TableBody>
              </Table>
            {/* </Typography> */}
          </CardContent>
        </Card>
        {/* <Typography component="div" className={classes.fixedBottom}>
          <Grid container alignItems="center" justify="flex-end">
            <Typography component="div">
              <CIMSButton
                  classes={{
                    label:classes.fontLabel
                  }}
                  disabled={disabled?false:true}
                  id="btn_template_maintenance_save"
                  color="primary"
                  size="small"
                  onClick={() =>this.recordListSave()}
              >
                Save Ordering
              </CIMSButton>
            </Typography>
            <CIMSButton
                classes={{
                  label:classes.fontLabel
                }}
                id="btn_template_maintenance_reset"
                color="primary"
                size="small"
                onClick={() =>this.reloadList()}
            >
              Cancel
            </CIMSButton>
          </Grid>
        </Typography> */}
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    favoriteCategoryListData: state.manageClinicalNoteTemplate.favoriteCategoryListData,
    deleteList: state.manageClinicalNoteTemplate.deleteList,
    recordList:state.manageClinicalNoteTemplate.recordList
  };
}
export default connect(mapStateToProps)(withStyles(style)(manageClinicalNoteTemplate));
