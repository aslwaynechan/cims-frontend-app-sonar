/*
 * Front-end UI for insert/update clinical note template
 * Insert template Action: [editTemplate.js] handleOk ->
 * -> action: ADDTEMPLATE_DATA
 * -> [medicalSummarySaga.js] addTemplateData
 * -> Backend API = clinical-note/insertClinicalNoteTemplate
 * Update template Action: [editTemplate.js] handleOk ->
 * -> action: EDITTEMPLATE_DATA
 * -> [medicalSummarySaga.js] editTemplateData
 * -> Backend API = clinical-note/updateClinicalNoteTemplate
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { DialogActions, DialogTitle, Dialog, Paper, Typography, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import CIMSButton from '../../../components/Buttons/CIMSButton';
import * as manageTemplateActionType from '../../../store/actions/clinicalNoteTemplate/manageClinicalNoteTemplateActionType';
import * as messageTypes from '../../../store/actions/message/messageActionType';
import * as commonTypes from '../../../store/actions/common/commonActionType';
import { style } from './manageClinicalNoteTemplateCss';
import { MANAGE_TEMPLATE_CODE } from '../../../constants/message/manageTemplateCode';
import Draggable from 'react-draggable';

function PaperComponent(props) {
  return (
    <Draggable enableUserSelectHack={false}
        onStart={(e) => {
        return e.target.getAttribute('customdrag') === 'allowed';
      }}
    >
      <Paper {...props} />
    </Draggable>
  );
}

class EditTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: this.props.open,
      fullWidth: true,
      nameValue: '',
      textValue: '',
      serviceCd: JSON.parse(sessionStorage.getItem('service')).serviceCd,//
      nameErrorFlag: false,
      textErrorFlag: false,
      editFlag: false
    };
  }

  handelChange = (e) => {
    let targetName = e.target.name;
    let name = `${targetName}Value`;
    let errorFlag = `${targetName}ErrorFlag`;
    let flag = false;
    e.target.value.trim() === '' && (flag = true);
    this.setState({
      [name]: e.target.value,
      [errorFlag]: flag,
      editFlag: true
    });
  }

  handleCancel = () => {
    let { handleDialogClose } = this.props;
    let { editFlag } = this.state;
    if (editFlag) {
      let payload = {
        msgCode: MANAGE_TEMPLATE_CODE.IS_CLOSE_DIALOG,
        btnActions: {
          // Yes
          btn1Click: () => {
            this.setState({
              nameErrorFlag: false,
              textErrorFlag: false,
              nameValue: '',
              textValue: '',
              editFlag: false
            });
            handleDialogClose && handleDialogClose();
          }
        }
      };
      this.props.dispatch({
        type: messageTypes.OPEN_COMMON_MESSAGE,
        payload
      });
    } else {
      this.setState({
        nameValue: '',
        textValue: '',
        nameErrorFlag: false,
        textErrorFlag: false,
        editFlag: false
      });
      handleDialogClose && handleDialogClose();
    }
  }

  handleOk = () => {
    let { handleDialogClose } = this.props;
    if (this.state.nameErrorFlag || this.state.textErrorFlag ? true : false) {
      return;
    }
    let editObj = this.props.selectObj;
    let editRowObj = this.props.selectRowObj;
    let templateList = this.props.templateList;
    let codeClinicalnoteTmplTypeCd = this.props.codeClinicalnoteTmplTypeCd;
    if (editObj.templateName === null) {
      if (this.state.nameValue === '' || this.state.textValue === '') {
        this.setState({
          nameErrorFlag: this.state.nameValue === '' ? true : false,
          textErrorFlag: this.state.textValue === '' ? true : false
        });
        return;
      }
      let params = {
        dtos: templateList,
        insertTemplateDto:
        {
          clinicalnoteTemplateId: '',
          codeClinicalnoteTmplTypeCd: codeClinicalnoteTmplTypeCd,
          createdBy: '',
          createdDtm: '',
          sequence: 0,
          serviceCd: this.state.serviceCd,
          templateName: this.state.nameValue, //FCS_002
          templateText: this.state.textValue,
          updatedBy: '',
          updatedByName: '',
          updatedDtm: '',
          userId: '',
          version: '',
          deleteInd: ''
        },
        selectedSeq: editRowObj != null ? editRowObj.sequence : ''
      };
      this.props.dispatch({ type: commonTypes.OPEN_COMMON_CIRCULAR_DIALOG });
      this.props.dispatch({
        type: manageTemplateActionType.ADDTEMPLATE_DATA, params, callback: (data) => {
          handleDialogClose && handleDialogClose();
          let payload = {
            msgCode: data.msgCode,
            showSnackbar: true
          };
          this.props.dispatch({
            type: messageTypes.OPEN_COMMON_MESSAGE, payload, callback: () => {
            }
          });
        }
      });
    } else {
      let params = {
        clinicalnoteTemplateId: editRowObj.clinicalnoteTemplateId,
        codeClinicalnoteTmplTypeCd: editRowObj.codeClinicalnoteTmplTypeCd,
        createdBy: editRowObj.createdBy,
        createdDtm: editRowObj.createdDtm,
        sequence: editRowObj.sequence,
        serviceCd: editRowObj.serviceCd,
        templateName: this.state.nameValue === '' ? this.props.selectObj.templateName : this.state.nameValue,
        templateText: this.state.textValue === '' ? this.props.selectObj.templateText : this.state.textValue,
        updatedB: editRowObj.updatedB,
        updatedByName: editRowObj.updatedByName,
        updatedDtm: editRowObj.updatedDtm,
        userId: editRowObj.id,
        version: editRowObj.version,
        deleteInd: ''
      };
      this.props.dispatch({ type: commonTypes.OPEN_COMMON_CIRCULAR_DIALOG });
      this.props.dispatch({
        type: manageTemplateActionType.EDITTEMPLATE_DATA, params, callback: (data) => {
          handleDialogClose && handleDialogClose();
          let payload = {
            msgCode: data.msgCode,
            showSnackbar: true
          };
          this.props.dispatch({
            type: messageTypes.OPEN_COMMON_MESSAGE, payload, callback: (data) => {
            }
          });
        }
      });
    }
    this.setState({
      editFlag: false,
      nameValue: '',
      textValue: '',
      nameErrorFlag: false,
      textErrorFlag: false
    });
  }

  handleEscKeyDown = () =>{
    this.handleCancel();
  }

  render() {
    let templateName = this.props.selectObj.templateName;
    let templateText = this.props.selectObj.templateText;
    let { nameErrorFlag, textErrorFlag } = this.state;
    let { classes } = this.props;
    return (
      <Dialog
          fullWidth={this.state.fullWidth}
          open={this.props.open}
          maxWidth="sm"
          PaperComponent={PaperComponent}
          onEscapeKeyDown={this.handleEscKeyDown}
      >
        <DialogTitle
            className={classes.dialogTitle}
            id="max-width-dialog-title"
            disableTypography customdrag="allowed"
        >
          Clinical Note Template Maintenance
        </DialogTitle>
        <Typography component="div" className={classes.dialogBorder}>
          <div style={{ display: 'flex', marginTop: 8, marginLeft: 10, marginRight: 20 }}>
            <span style={{ marginRight: 5 }} className={classes.templatetitle}>Name:</span>
            {/* <input name="name"
                  //placeholder="请输入..."
                  maxLength="255"
                  autoComplete="off"
                  defaultValue={templateName}
                  onChange={this.handelChange.bind(this)}
                  className={classes.inputName}
              >
              </input> */}
            <TextField
                fullWidth
                name="name"
                defaultValue={templateName}
                autoComplete="off"
                variant="outlined"
                className={classes.inputName}
                inputProps={{
                maxLength: 255,
                style: {
                  fontSize: '1rem',
                  fontFamily: 'Arial'
                }
              }}
                onChange={this.handelChange}
            />
          </div>
          {
            nameErrorFlag ?
              (
                <div><span id="span_editTemplate_name_validation" style={{ marginLeft: '13%' }} className={classes.validation}>This field is required.</span></div>
              ) : null
          }
          <span><hr /></span>
          <div style={{ marginLeft: 10, marginRight: 30 }}>
            <div>
              <div>
                <span className={classes.templatetitle}>Text:</span>
              </div>
              <textarea
                  name="text"
                  defaultValue={templateText}
                  onChange={this.handelChange.bind(this)}
                  className={classes.inputText}
              />
              {
                textErrorFlag ? (
                  <div><span id="span_editTemplate_text_validation" className={classes.validation}>This field is required.</span></div>
                ) : null
              }
            </div>
          </div>
          <DialogActions>
            <CIMSButton
                classes={{
                label: classes.fontLabel
              }}
                color="primary"
                id={'saveCimsButton'}
                onClick={this.handleOk}
                size="small"
                variant="contained"
            >Save
              </CIMSButton>
            <CIMSButton
                classes={{
                label: classes.fontLabel
              }}
                color="primary"
                id={'cancelCimsButton'}
                onClick={this.handleCancel}
                size="small"
                variant="contained"
            >Cancel</CIMSButton>
          </DialogActions>
        </Typography>
      </Dialog>
    );
  }
}


function mapStateToProps() {
  return {
    // favoriteCategoryListData: state.manageTemplate.favoriteCategoryListData,
    // deleteList: state.manageTemplate.deleteList,
    // recordList:state.manageTemplate.recordList,
  };
}
export default connect(mapStateToProps)(withStyles(style)(EditTemplate));


