export const styles = {
  infoImage: {
    width: '100%'
  },
  gridWrapper: {
    height: '670px',
    marginBottom: 0,
    overflowX: 'auto',
    padding: 0
  },
  selectLabel: {
    fontSize:'1rem',
    float: 'left',
    paddingTop: '3px',
    paddingRight: '10px',
    fontFamily: 'Arial',
    fontWeight: 'bold'
  },
  btnGroup: {
    width: 'calc(100% - 34px)'
  },
  fontLabel: {
    fontStyle: 'Arial',
    fontSize: '1rem'
  },
  fontTitle: {
    fontStyle: 'Arial',
    fontSize: '1.5rem',
    paddingLeft:4
  },
  cardContent: {
    paddingRight:0,
    paddingLeft:0,
    '&:last-child':{
      paddingBottom: 10,
      paddingTop: 0
    }
  }
};
