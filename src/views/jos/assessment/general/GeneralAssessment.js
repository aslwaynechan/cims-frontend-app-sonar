/*
 * Front-end UI for load&save assessment readings
 * Load Field Dropdown List Action: [GeneralAssessment.js] componentDidMount
 * -> [assessmentAction.js] getFieldDropList
 * -> [assessmentSaga.js] getfielddroplist
 * -> Backend API = assessment/listCodeAssessmentDrop
 * Load Assessment Field Normal Range Action: [GeneralAssessment.js] componentDidMount -> loadData
 * -> [assessmentAction.js] getFieldNormalRangeList
 * -> [assessmentSaga.js] getFieldNormalRangeList
 * -> Backend API = assessment/listAssessmentNormalRange
 * Load Assessment Data Action: [GeneralAssessment.js] componentDidMount -> loadData
 * -> [assessmentAction.js] getPatientAssessmentList
 * -> [assessmentSaga.js] getPatientAssessmentListByServiceCd
 * -> Backend API = assessment/listCodeAssessmentAndFieldByServiceCd
 * Save Action: [GeneralAssessment.js] Save -> handleSave
 * -> [assessmentAction.js] updatePatientAssessment
 * -> [assessmentSaga.js] updatePatientAssessment
 * -> Backend API = assessment/saveAssessment
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Typography,withStyles, Card, CardContent } from '@material-ui/core';
import AssessmentGrid from '../components/AssessmentGrid';
import { cloneDeep,findIndex,find,isUndefined,isNull,toNumber,isEqual } from 'lodash';
import { updatePatientAssessment,getPatientAssessmentList,getFieldDropList,getFieldNormalRangeList,getTempServiceList } from '../../../../store/actions/assessment/assessmentAction';
import { openErrorMessage,openCommonCircularDialog,closeCommonCircularDialog } from '../../../../store/actions/common/commonAction';
// import infoImage from '../../../../images/assessment/general_assessment.png';
import { styles } from './GeneralAssessmentStyle';
// import { Prompt } from 'react-router-dom';
import { OBJ_TYPE } from '../../../../constants/assessment/assessmentConstants';
import accessRightEnum from '../../../../enums/accessRightEnum';
import { deleteSubTabs } from '../../../../store/actions/mainFrame/mainFrameAction';
import Container from 'components/JContainer';

class GeneralAssessment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEdit:false,
      assessmentItems:[],
      originAssessmentItems:[],
      fieldValMap:new Map(),
      resultMap:new Map(),
      versionMap:new Map(),
      createdByMap:new Map(),
      createdDtmMap:new Map(),
      saveFlag:true
    };
  }

  componentDidMount(){
    this.props.ensureDidMount();
    this.props.getFieldDropList({
      params:{},
      callback:()=>{
        this.loadData();
      }
    });
    if(this.props.saveParams!==undefined){
      this.props.onRef('assessment', this);
    }
  }

  UNSAFE_componentWillUpdate(nextProps) {
    const {resultIdMap,versionMap,createdByMap,createdDtmMap} = this.props;
    if (!isEqual(nextProps.versionMap,versionMap)) {
      this.setState({
        versionMap:nextProps.versionMap
      });
    }
    if (!isEqual(nextProps.resultIdMap,resultIdMap)) {
      this.setState({
        resultIdMap:nextProps.resultIdMap
      });
    }
    if (!isEqual(nextProps.createdByMap,createdByMap)) {
      this.setState({
        createdByMap:nextProps.createdByMap
      });
    }
    if (!isEqual(nextProps.createdDtmMap,createdDtmMap)) {
      this.setState({
        createdDtmMap:nextProps.createdDtmMap
      });
    }
  }

  loadData=()=>{
    let { loginInfo,patientPanelInfo,encounterData } = this.props;
    this.props.getFieldNormalRangeList({
      params:{
        serviceCd:loginInfo.service.code,
        genderCd: patientPanelInfo.genderCd||'U', // U: Unknown
        age: toNumber(patientPanelInfo.age)||0
      }
    });
    this.props.getPatientAssessmentList({
      params:{
        encounterId: encounterData.encounterId||0,
        clinicCd: loginInfo.clinic.code,
        serviceCd:loginInfo.service.code,
        genderCd: patientPanelInfo.genderCd||'U' // U: Unknown
      },
      callback:()=>{
        this.initAssessmentState();
      }
    });
  }

  initAssessmentState=()=>{
    const { patientAssessmentList,patientAssessmentValMap,fieldDropList,resultIdMap,versionMap,createdByMap,createdDtmMap } = this.props;
    let originAssessmentItems = cloneDeep(patientAssessmentList);

    originAssessmentItems.forEach(element => {
      if (element.fields instanceof Array) {
        let fieldsArray = element.fields;
        fieldsArray.forEach((subFieldArray,rowId)=>{
          subFieldArray.forEach(field => {
            //handle DL options
            if (field.codeObjectTypeCd === OBJ_TYPE.DROP_DOWN_LIST) {
              let dropOptionObj = find(fieldDropList,item=>{
                return item.fieldId === field.codeAssessmentFieldId;
              });
              if (!isUndefined(dropOptionObj)) {
                field.dropDisabled = false;
                field.subSet = 0;  //default
                field.subSetOptions=dropOptionObj.subSet;
                for (let i = 0; i < dropOptionObj.subSet.length; i++) {
                  let optionObj = dropOptionObj.subSet[i];
                  if (!isNull(optionObj.dependedFieldId)&&patientAssessmentValMap.has(element.codeAssessmentCd)) {
                    field.dropDisabled = true;
                    let vals = patientAssessmentValMap.get(element.codeAssessmentCd).get(optionObj.dependedFieldId);
                    if (toNumber(vals[rowId].val) === optionObj.dependedDropId) {
                      field.subSet = rowId;
                      field.dropDisabled = false;
                      break;
                    }
                  }
                }
              }
            }
          });
          //handle union unit
          let index = findIndex(subFieldArray,item=>{
            return item.objUnit === '/';
          });
          if (index !== -1) {
            let tempField = subFieldArray[index+1];
            subFieldArray.splice(index+1,1);
            subFieldArray[index].codeAssessmentFieldId2=tempField.codeAssessmentFieldId;
            subFieldArray[index].codeObjectTypeCd2=tempField.codeObjectTypeCd;
            subFieldArray[index].objUnit2=tempField.objUnit;
          }
        });
      }
    });

    this.setState({
      assessmentItems: cloneDeep(originAssessmentItems),
      fieldValMap: cloneDeep(patientAssessmentValMap),
      resultMap: cloneDeep(resultIdMap),
      versionMap: cloneDeep(versionMap),
      createdByMap: cloneDeep(createdByMap),
      createdDtmMap: cloneDeep(createdDtmMap)
    });
  }

  updateState=(obj)=>{
    this.setState({
      ...obj
    });
  }

  errorValidation = () => {
    let { fieldValMap } = this.state;
    for (let fieldValues of fieldValMap.values()) {
      for (let values of fieldValues.values()) {
        values.forEach(valObj => {
          if (valObj.isError) {
            return false;
          }
        });
      }
    }
  }

  handleSave=()=>{
    const { loginInfo, encounterData } = this.props;
    let { fieldValMap,resultMap,versionMap,createdByMap,createdDtmMap,assessmentItems } = this.state;
    let resultObj = {
      encounterId: encounterData.encounterId||0,
      serviceCd:loginInfo.service.code,
      clinicCd: loginInfo.clinic.code,
      assessmentValueDtos: []
    };

    for (let [codeAssessmentCd, fieldValues] of fieldValMap) {
      let assessmentResultObj = {
        codeAssessmentCd,
        fieldValueDtos:[]
      };
      let tempAssessmentResultIdMap = resultMap.get(codeAssessmentCd);
      let tempVersionMap = versionMap.get(codeAssessmentCd);
      let tempCreatedByMap = createdByMap.get(codeAssessmentCd);
      let tempCreatedDtmMap = createdDtmMap.get(codeAssessmentCd);

      // assessmentItems.forEach(element => {
      //   if(element.codeAssessmentCd===codeAssessmentCd){
      //     isRequiredVal= element.fields[0][0].nullable;
      //   }
      // });

      for (let [fieldId, values] of fieldValues) {
        //check error
        let index = findIndex(values,valObj=>{
          return valObj.isError === true;
        });

        let isRequiredVal = 1;
        outer:
        for (let index = 0; index < assessmentItems.length; index++) {
          let element = assessmentItems[index];
          if(element.codeAssessmentCd===codeAssessmentCd){
            for (let index = 0; index < element.fields[0].length; index++) {
              let e = element.fields[0][index];
              if(fieldId===e.codeAssessmentFieldId||fieldId===e.codeAssessmentFieldId2){
                isRequiredVal= element.fields[0][index].nullable;
                break outer;
              }
            }
          }
      }
        //check nullable field.
        let isRequired=findIndex(values,valObj=>{
          return isRequiredVal === 0&&valObj.val==='';
        });
        console.log('isRequired======='+isRequired);
        if (index === -1) {
          if(isRequired!==-1){
            this.setState({saveFlag:false});
            return;
          }
          // else{
          //   this.setState({saveFlag:true});
          // }
          let resultArray = [];
          values.forEach(valObj => {
            resultArray.push(valObj.val);
          });
          assessmentResultObj.fieldValueDtos.push({
            codeAssessmentFieldId:fieldId,
            assessmentResults:resultArray,
            resultIds:tempAssessmentResultIdMap.get(fieldId),
            version:tempVersionMap.get(fieldId),
            createdBys:tempCreatedByMap.get(fieldId),
            createdDtms:tempCreatedDtmMap.get(fieldId)
          });
        } else {
            this.setState({saveFlag:false});
          return;
        }
      }
      resultObj.assessmentValueDtos.push(assessmentResultObj);
    }
    this.props.openCommonCircularDialog();
    this.props.updatePatientAssessment({
      params:resultObj,
      callback:()=>{
        this.setState({
          isEdit:false
        });
        this.props.closeCommonCircularDialog();
      }
    });
  }

  getSaveProps=()=>{
    const { loginInfo, encounterData } = this.props;
    let { fieldValMap,resultMap,versionMap,createdByMap,createdDtmMap } = this.state;

    let resultObj = {
      encounterId: encounterData.encounterId||0,
      serviceCd:loginInfo.service.code,
      clinicCd: loginInfo.clinic.code,
      assessmentValueDtos: []
    };

    for (let [codeAssessmentCd, fieldValues] of fieldValMap) {
      let assessmentResultObj = {
        codeAssessmentCd,
        fieldValueDtos:[]
      };
      let tempAssessmentResultIdMap = resultMap.get(codeAssessmentCd);
      let tempVersionMap = versionMap.get(codeAssessmentCd);
      let tempCreatedByMap = createdByMap.get(codeAssessmentCd);
      let tempCreatedDtmMap = createdDtmMap.get(codeAssessmentCd);
      for (let [fieldId, values] of fieldValues) {
        //check error
        let index = findIndex(values,valObj=>{
          return valObj.isError === true;
        });
        if (index === -1) {
          let resultArray = [];
          values.forEach(valObj => {
            resultArray.push(valObj.val);
          });
          assessmentResultObj.fieldValueDtos.push({
            codeAssessmentFieldId:fieldId,
            assessmentResults:resultArray,
            resultIds:tempAssessmentResultIdMap.get(fieldId),
            version:tempVersionMap.get(fieldId),
            createdBys:tempCreatedByMap.get(fieldId),
            createdDtms:tempCreatedDtmMap.get(fieldId)
          });
        } else {
          return;
        }
      }
      resultObj.assessmentValueDtos.push(assessmentResultObj);
    }
    return resultObj;
  }

  handleFunctionClose = (modeFlag = false) => {
    this.props.deleteSubTabs(accessRightEnum.generalAssessment);
  }

  render() {
    const {
      classes,
      outputAssesmentFieldMap,
      cascadeDropMap,
      emptyCascadeFieldMap,
      fieldNormalRangeMap,
      encounterData
    } = this.props;
    let {
      assessmentItems,
      fieldValMap,
      resultMap,
      versionMap,
      createdByMap,
      createdDtmMap,
      saveFlag
    } = this.state;

    let gridProps = {
      assessmentItems,
      fieldValMap,
      outputAssesmentFieldMap,
      cascadeDropMap,
      emptyCascadeFieldMap,
      fieldNormalRangeMap,
      resultMap,
      versionMap,
      createdByMap,
      createdDtmMap,
      updateState: this.updateState,
      encounterId: encounterData.encounterId||0,
      saveFlag
    };

    const buttonBar={
        isEdit:this.state.isEdit,
        // height:'64px',
        position:'fixed',
        buttons:[{
          title:'Save',
          onClick:this.handleSave,
          id:'default_save_button'
        }]
      };
    return (
      <Container buttonBar={buttonBar} className={'detail_warp'} style={{overflowY:'hidden'}}>
        <Card>
          <CardContent className={classes.cardContent}>
            {/* title */}
            <Typography component="div">
              <div className={classes.fontTitle}>General Assessment</div>
            </Typography>
            {/* assessment grid */}
            <div className={'nephele_content_body '+classes.gridWrapper}>
              <Typography component="div">
                  <AssessmentGrid {...gridProps}/>
              </Typography>
            </div>
            {/* button group */}
            {/* <Typography
                classes={{
                  'root':classes.btnGroup
                }}
                component="div"
            >
              <Grid
                  alignItems="center"
                  container
                  direction="row"
                  justify="flex-end"
              >
                <CIMSButton
                    classes={{
                      label:classes.fontLabel
                    }}
                    id="btn_general_assessment_cancel"
                    onClick={this.handleFunctionClose}
                >
                  Cancel
                </CIMSButton>
                <CIMSButton
                    classes={{
                      label:classes.fontLabel
                    }}
                    id="btn_general_assessment_save"
                    onClick={this.handleSave}
                >
                  Save
                </CIMSButton>
              </Grid>
            </Typography> */}
            {/* <Prompt
                message={FieldConstant.LEAVE_CONFIRM_PROMPT_CONTENT}
                when={isEdit}
            /> */}
          </CardContent>
        </Card>
      </Container>
    );
  }
}

const mapStatesToProps = state => {
  return {
    loginInfo: {
      ...state.login.loginInfo,
      service:{
        code:state.login.service.serviceCd
      },
      clinic:{
        code:state.login.clinic.clinicCd
      }
    },
    encounterData: state.patient.encounterInfo,
    patientPanelInfo: state.patient.patientInfo,
    patientAssessmentList: state.assessment.patientAssessmentList,
    assessmentDrop: state.assessment.assessmentDrop,
    patientAssessmentValMap: state.assessment.patientAssessmentValMap,
    outputAssesmentFieldMap: state.assessment.outputAssesmentFieldMap,
    fieldDropList: state.assessment.fieldDropList,
    cascadeDropMap: state.assessment.cascadeDropMap,
    emptyCascadeFieldMap: state.assessment.emptyCascadeFieldMap,
    fieldNormalRangeMap: state.assessment.fieldNormalRangeMap,
    resultIdMap: state.assessment.resultIdMap,
    versionMap: state.assessment.versionMap,
    createdByMap: state.assessment.createdByMap,
    createdDtmMap: state.assessment.createdDtmMap
  };
};

const mapDispatchToProps = {
  getPatientAssessmentList,
  updatePatientAssessment,
  getFieldDropList,
  getFieldNormalRangeList,
  getTempServiceList,
  openErrorMessage,
  openCommonCircularDialog,
  closeCommonCircularDialog,
  deleteSubTabs
};

export default connect(mapStatesToProps,mapDispatchToProps)(withStyles(styles)(GeneralAssessment));
