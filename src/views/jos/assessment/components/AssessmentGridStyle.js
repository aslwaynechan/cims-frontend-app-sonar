export const styles = {
  form_wrapper: {
    minWidth: '1150px'
  },
  btn_delete: {
    color: '#fd0000',
    backgroundColor: '#ffffff',
    marginTop: '-5px',
    border: '1px solid #fd0000',
    '&:hover': {
      backgroundColor: '#fd0000',
      border: '1px solid #ffffff',
      color: '#ffffff'
    }
  },
  title_span: {
    fontSize:'1rem',
    fontFamily: 'Arial',
    paddingTop: '5px',
    paddingRight: '2px',
    fontWeight: 'bold',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis'
  },
  field_grid_wrapper: {
    maxWidth: '90%'
  },
  label_assessment: {
    fontWeight: 'bold',
    paddingTop: '5px',
    minWidth: '170px',
    fontSize: '1rem',
    fontFamily: 'Arial'
  },
  field_wrapper: {
    display: 'inherit',
    height: '65px'
  },
  assessment_grid_wrapper: {
    backgroundColor: '#D1EEFC'
  },
  field_title_div: {
    display: 'inline-flex'
  },
  btn_wrapper: {
    height: '30px',
    width: '100%'
  },
  btn_add_grid: {
    textAlign: 'right'
  },
  btn_add: {
    marginTop: '4px'
  },
  item_wrapper: {
    marginLeft: -5
    // paddingLeft: '10px'
  },
  fontLabel :{
    fontSize: '1rem',
    fontFamily: 'Arial'
  },
  fabButtonDelete: {
    marginTop: 1,
    float: 'right',
    height: '28px',
    width: '28px',
    minHeight: '28px',
    backgroundColor: '#FD0000',
    '&:hover': {
      backgroundColor: '#fd0000'
    }
  },
  icon: {
    fontSize: '24px'
  },
  btnAdd: {
    height: '28px',
    width: '28px',
    minHeight: '28px',
    '&:hover': {
      backgroundColor: '#0098FF'
    }
  },
  btnGrid: {
    paddingRight: 2,
    maxWidth: 62,
    // minWidth: '45px',
    margin: '19px 0',
    textAlign: 'center'
  },
  unionFieldWrapper: {
    flexBasis: '25%',
    maxWidth: '25%',
    minWidth: 270
  }
};
