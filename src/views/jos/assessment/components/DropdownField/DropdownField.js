import React, { Component } from 'react';
// import SelectFieldValidator from '../../../../../components/FormValidator/SelectFieldValidator';
import { withStyles } from '@material-ui/core';
import { styles } from './DropdownFieldStyle';
import { toNumber,isUndefined,find } from 'lodash';
import * as generalUtil from '../../utils/generalUtil';
import CustomizedSelectFieldValidator from '../../../../../components/Select/CustomizedSelect/CustomizedSelectFieldValidator';

class DropdownField extends Component {
  constructor(props){
    super(props);
    this.state={
      abnormalFlag:false,
      showClear:false,
      val:'',
      encounterId:''
    };
  }

  static getDerivedStateFromProps(props, state) {
    let { field, rowId, assessmentCd, fieldValMap, fieldNormalRangeMap } = props;
    let val = '',
        abnormalFlag = false,
        required =1;
    if (fieldValMap.has(assessmentCd)) {
      let fieldValArray = fieldValMap.get(assessmentCd).get(field.codeAssessmentFieldId);
      val = !isUndefined(fieldValArray)?fieldValArray[rowId].val:'';
      required = field.nullable;
      let errorFlag = !isUndefined(fieldValArray)?fieldValArray[rowId].isError:false;
      if (!errorFlag) {
        abnormalFlag = generalUtil.abnormalDLCheck(val,assessmentCd,field.codeAssessmentFieldId,fieldNormalRangeMap);
      }
    }
    if (props.encounterId !== state.encounterId||val !== state.val||required !== state.required) {
      return {
        encounterId: props.encounterId,
        val,
        abnormalFlag,
        required
      };
    }
    return null;
  }

  handleSelectChange = (event, assessmentCd, fieldId, rowId) => {
    let { updateState, fieldValMap, cascadeDropMap, emptyCascadeFieldMap, assessmentItems, fieldNormalRangeMap } = this.props;
    let vals = fieldValMap.get(assessmentCd).get(fieldId);
    vals[rowId].val = !!event?event.value:'';
    // let abnormalFlag = false;
    let abnormalFlag = generalUtil.abnormalDLCheck(vals[rowId].val,assessmentCd,fieldId,fieldNormalRangeMap);
    this.setState({
      val: vals[rowId].val,
      abnormalFlag
    });
    let numberVal = toNumber(vals[rowId].val);
    // cascade
    if (cascadeDropMap.has(numberVal)) {
      let cascadeObj = cascadeDropMap.get(numberVal);
      let assessmentObj = find(assessmentItems,assessment=>{
        return assessment.codeAssessmentCd === assessmentCd;
      });
      if (!isUndefined(assessmentObj)) {
        let fieldsArray = assessmentObj.fields;
        fieldsArray[rowId].forEach(field => {
          if(field.codeAssessmentFieldId === cascadeObj.fieldId){
            field.dropDisabled = false;
            if (field.subSet !== cascadeObj.subSetId) {
              field.subSet = cascadeObj.subSetId;
              //change value
              vals = fieldValMap.get(assessmentCd).get(cascadeObj.fieldId);
              vals[rowId].val = '';
            }
          }
        });
      }
    } else {
      // empty
      if (emptyCascadeFieldMap.has(fieldId)) {
        let cascadeFieldId = emptyCascadeFieldMap.get(fieldId);
        let assessmentObj = find(assessmentItems,assessment=>{
          return assessment.codeAssessmentCd === assessmentCd;
        });
        if (!isUndefined(assessmentObj)) {
          let fieldsArray = assessmentObj.fields;
          fieldsArray[rowId].forEach(field => {
            if (field.codeAssessmentFieldId === cascadeFieldId) {
              field.dropDisabled = true;
              //change value
              vals = fieldValMap.get(assessmentCd).get(cascadeFieldId);
              vals[rowId].val = '';
            }
          });
        }
      }
    }
    updateState && updateState({
      isEdit:true,
      fieldValMap,
      assessmentItems
    });
  };

  handleFocus = () => {
    this.setState({
      showClear: true
    });
  }

  hanldeBlur = () => {
    this.setState({
      showClear: false
    });
  }

  render(){
    let { classes,field,rowId,assessmentCd,saveFlag} = this.props;
    let {val,showClear,abnormalFlag,required} = this.state;
    let subSetId = field.subSet;

    return(
      <div className={classes.select_wrapper}>
        <CustomizedSelectFieldValidator
            id={`assessment_item_dropdown_${field.codeAssessmentFieldId}_${rowId}`}
            options={field.subSetOptions[subSetId].options.map(option => {
              return {
                label: option.dropVal,
                value: option.dropId+''
              };
            })}
            notShowMsg={false}
            errorMessages={abnormalFlag?'':'the filed is required'}
            isValid={abnormalFlag?false:((!saveFlag&&val===''&&required===0)?false:true)}
            valIsAbnormal={abnormalFlag}
            // classes={{
            //   singleValue:abnormalFlag?classes.abnormal:null
            // }}
            onFocus={this.handleFocus}
            onBlur={this.hanldeBlur}
            isClearable={showClear}
            isDisabled={field.dropDisabled}
            value={val}
            onChange={event => {this.handleSelectChange(event,assessmentCd,field.codeAssessmentFieldId,rowId);}}
            styles={{ menuPortal: base => ({ ...base, zIndex: 1600 }) }}
            menuPortalTarget={document.body}
            msgPosition="bottom"
        />
      </div>
    );
  }
}

export default withStyles(styles)(DropdownField);