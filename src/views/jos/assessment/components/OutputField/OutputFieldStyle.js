export const styles = {
  unit_span: {
    marginLeft: 5,
    marginTop: 5,
    float: 'left'
  },
  field_outputbox: {
    fontSize: '1rem',
    fontFamily: 'Arial'
  },
  abnormal: {
    borderRadius: 5,
    width: 70,
    padding: '7px 14px 3px 14px',
    backgroundColor: '#DCDCDC',
    border: '1px solid',
    color: '#fd0000 !important',
    float: 'left',
    // marginTop: '5px',
    height: 27
  },
  normal: {
    borderRadius: 5,
    width: 70,
    padding: '7px 14px 3px 14px',
    backgroundColor: '#DCDCDC',
    float: 'left',
    // marginTop: '5px',
    border: '1px solid #c4c4c4',
    height: 27
  },
  unit_wrapper: {
    float: 'left',
    marginTop: '5px'
  }
};
