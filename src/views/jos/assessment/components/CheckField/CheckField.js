import React, { Component } from 'react';
import { FormControlLabel, withStyles, Checkbox, FormHelperText} from '@material-ui/core';
import { styles } from './CheckFieldStyle';
import { CHECKBOX_STATUS } from '../../../../../constants/assessment/assessmentConstants';
import * as generalUtil from '../../utils/generalUtil';
import { ErrorOutline } from '@material-ui/icons';

class CheckField extends Component {
  constructor(props){
    super(props);
    this.state={
      abnormalFlag:false,
      checkFlag:false
    };
  }

  static getDerivedStateFromProps(props, state) {
    let { field, rowId, assessmentCd, fieldValMap, fieldNormalRangeMap } = props;
    let val = '',
        abnormalFlag = false,
        required = 1;
    if (fieldValMap.has(assessmentCd)) {
      let tempfieldMap = fieldValMap.get(assessmentCd);
      let fieldValArray = tempfieldMap.get(field.codeAssessmentFieldId);
      val = fieldValArray!==undefined?fieldValArray[rowId].val:'';
      required = field.nullable;
      let errorFlag = fieldValArray!==undefined?fieldValArray[rowId].isError:false;
      if (!errorFlag) {
        abnormalFlag = generalUtil.abnormalCBCheck(val,assessmentCd,field.codeAssessmentFieldId,fieldNormalRangeMap);
      }
    }
    if (props.encounterId !== state.encounterId||val !== state.val||required !== state.required) {
      return {
        encounterId: props.encounterId,
        checkFlag:val!==''?true:false,
        abnormalFlag,
        required
      };
    }
    return null;
  }

  handleCheckBoxChange = (assessmentCd, fieldId, rowId, event) => {
    let { updateState,fieldValMap,fieldNormalRangeMap } = this.props;
    let vals = fieldValMap.get(assessmentCd).get(fieldId);
    vals[rowId].val = event.target.checked?CHECKBOX_STATUS.CHECKED:CHECKBOX_STATUS.UNCHECKED;
    let abnormalFlag = generalUtil.abnormalCBCheck(vals[rowId].val,assessmentCd,fieldId,fieldNormalRangeMap);
    this.setState({
      checkFlag:event.target.checked,
      abnormalFlag
    });
    updateState&&updateState({
      isEdit:true,
      fieldValMap
    });
  }

  render(){
    let { classes, field, rowId, assessmentCd, saveFlag } = this.props;
    let {checkFlag,abnormalFlag,required} = this.state;
    return(
      <div className={classes.check_wrapper}>
        <FormControlLabel
            control={
              <Checkbox
                  id={`assessment_item_checkbox_${field.codeAssessmentFieldId}_${rowId}`}
                  classes={{
                    checked: abnormalFlag?classes.abnormal:null
                  }}
                  color="primary"
                  checked={checkFlag}
                  onChange={event => {this.handleCheckBoxChange(assessmentCd,field.codeAssessmentFieldId,rowId,event);}}
              />
            }
        />
        {!saveFlag&&!checkFlag&&required===0?(
            <FormHelperText
                error
                classes={{
                  error:classes.helper_error
                }}
            >
              <ErrorOutline className={classes.error_icon}/>
                this filed is required
            </FormHelperText>
          ):null}
      </div>
    );
  }
}

export default withStyles(styles)(CheckField);