import React, { Component } from 'react';
import { Grid, withStyles, Fab } from '@material-ui/core';
// import CIMSButton from '../../../../components/Buttons/CIMSButton';
import ValidatorForm from '../../../../components/FormValidator/ValidatorForm';
import {findIndex,cloneDeep,isUndefined} from 'lodash';
import {styles} from './AssessmentGridStyle';
import { OBJ_TYPE,DATA_TYPE } from '../../../../constants/assessment/assessmentConstants';
import DoubleInputField from './InputField/DoubleInputField';
import OutputField from './OutputField/OutputField';
import DropdownField from './DropdownField/DropdownField';
import CheckField from './CheckField/CheckField';
import TimeInputField from './InputField/TimeInputField';
import StringInputField from './InputField/StringInputField';
import IntegerInputField from './InputField/IntegerInputField';
import { Remove, Add } from '@material-ui/icons';
import classNames from 'classnames';

class AssessmentGrid extends Component {

  handleAssessmentFieldsAdd = currentCd => {
    let { assessmentItems, updateState, fieldValMap, resultMap, versionMap, createdByMap, createdDtmMap } = this.props;
    let index = findIndex(assessmentItems, item => {
      return item.codeAssessmentCd === currentCd;
    });
    if (index !== -1) {
      let currentItem = assessmentItems[index];
      let tempFields = cloneDeep(currentItem.fields[0]);
      currentItem.fields.push(tempFields);

      let fieldVals = fieldValMap.get(currentCd);
      for (let value of fieldVals.values()) {
        value.push({val:'',isError:false,isRequired:value[0].isRequired});
      }
      let fieldResultIds = resultMap.get(currentCd);
      for (let value of fieldResultIds.values()) {
        value.push(null);
      }
      let fieldVersions = versionMap.get(currentCd);
      for (let value of fieldVersions.values()) {
        value.push(null);
      }
      let fieldCreatedBys = createdByMap.get(currentCd);
      for (let value of fieldCreatedBys.values()) {
        value.push(null);
      }
      let fieldCreatedDtms = createdDtmMap.get(currentCd);
      for (let value of fieldCreatedDtms.values()) {
        value.push(null);
      }
      updateState && updateState({
        isEdit:true,
        assessmentItems,
        fieldValMap,
        resultMap,
        versionMap,
        createdByMap,
        createdDtmMap
      });
    }
  };

  handleAssessmentFiledsDelete = (currentCd,rowId,cleanFlag) => {
    let { assessmentItems, updateState, fieldValMap, resultMap, versionMap, createdByMap, createdDtmMap } = this.props;
    let index = findIndex(assessmentItems, item => {
      return item.codeAssessmentCd === currentCd;
    });
    if (index !== -1) {
      let fieldVals = fieldValMap.get(currentCd);
      if (cleanFlag) {
        for (let value of fieldVals.values()) {
          value[rowId].val = '';
          value[rowId].isError = false;
        }
      } else {
        let currentItem = assessmentItems[index];
        currentItem.fields.splice(rowId,1);
        let fieldResultIds = resultMap.get(currentCd);
        let fieldVersions = versionMap.get(currentCd);
        let fieldCreatedBys = createdByMap.get(currentCd);
        let fieldCreatedDtms = createdDtmMap.get(currentCd);
        for (let value of fieldVals.values()) {
          value.splice(rowId,1);
        }
        for (let value of fieldResultIds.values()) {
          value.splice(rowId,1);
        }
        for (let value of fieldVersions.values()) {
          value.splice(rowId,1);
        }
        for (let value of fieldCreatedBys.values()) {
          value.splice(rowId,1);
        }
        for (let value of fieldCreatedDtms.values()) {
          value.splice(rowId,1);
        }
      }
      updateState && updateState({
        isEdit:true,
        assessmentItems,
        fieldValMap,
        resultMap,
        versionMap,
        createdByMap,
        createdDtmMap
      });
    }
  };

  renderField = (field, rowId, assessmentCd) => {
    let { updateState, fieldValMap, fieldNormalRangeMap, outputAssesmentFieldMap, cascadeDropMap, emptyCascadeFieldMap, assessmentItems, encounterId,saveFlag} = this.props;

    let fieldProps = {
      fieldValMap,
      assessmentCd,
      rowId,
      field,
      updateState,
      fieldNormalRangeMap,
      outputAssesmentFieldMap,
      cascadeDropMap,
      emptyCascadeFieldMap,
      assessmentItems,
      encounterId,
      saveFlag
    };

    switch (field.codeObjectTypeCd) {
      //IB
      case OBJ_TYPE.INPUT_BOX:{
        if (field.dataType === DATA_TYPE.TIME) {
          return (
            <TimeInputField {...fieldProps}/>
          );
        } else if (field.dataType === DATA_TYPE.STRING) {
          return (
            <StringInputField {...fieldProps}/>
          );
        } else if (field.dataType === DATA_TYPE.DOUBLE) {
          return(
            <DoubleInputField {...fieldProps}/>
          );
        } else if (field.dataType === DATA_TYPE.INTEGER) {
          return(
            <IntegerInputField {...fieldProps}/>
          );
        }
        break;
      }
      // DL
      case OBJ_TYPE.DROP_DOWN_LIST:{
        return (
          <DropdownField {...fieldProps}/>
        );
      }
      //OB
      case OBJ_TYPE.OUTPUT_BOX: {
        return (
          <OutputField {...fieldProps}/>
        );
      }
      //CB
      case OBJ_TYPE.CLICK_BOX: {
        return (
          <CheckField {...fieldProps}/>
        );
      }
      default:
        break;
    }
  };

  generateField = (fields, assessmentCd, isMultiple) => {
    let { classes,fieldValMap } = this.props;
    let tempValMap = fieldValMap.get(assessmentCd);
    let cleanFlag = true;
    if (!isUndefined(tempValMap)){
      for (let value of tempValMap.values()) {
        cleanFlag = value.length === 1?true:false;
        break;
      }
    }
    let group = fields.map((fieldGroup, index) => {
      let gridGroup = fieldGroup.map(field => {
        return (
          <Grid
              container
              item
              xs={field.codeAssessmentFieldId2 !== undefined ? 4 : (field.codeObjectTypeCd === OBJ_TYPE.OUTPUT_BOX? 6 : 3)}
              key={field.codeAssessmentFieldId}
              classes={{
                'grid-xs-3': classes.field_wrapper,
                'grid-xs-4': classNames(classes.field_wrapper,classes.unionFieldWrapper),
                'grid-xs-6': classes.field_wrapper
              }}
              direction="row"
              justify="flex-start"
              alignItems="center"
          >
            {!isUndefined(field.objTitle) ? (
              <div className={classes.field_title_div}>
                <span className={classes.title_span}>{`${field.objTitle}:`}</span>
              </div>
            ) : null}
            {/* index->row */}
            {this.renderField(field, index, assessmentCd)}
          </Grid>
        );
      });
      return (
        <Grid
            container
            item
            key={`${assessmentCd}_group_${index}`}
            direction="row"
            justify="flex-start"
            alignItems="center"
        >
          <Grid container item xs={11}>
            {gridGroup}
          </Grid>
          {isMultiple==='Y'?(
            <Grid container item xs={1}>
              <div className={classes.btn_wrapper}>
                <Fab
                    size="small"
                    color="secondary"
                    aria-label="Remove"
                    id={`btn_assessment_item_${assessmentCd}_delete_${index}`}
                    onClick={() => {this.handleAssessmentFiledsDelete(assessmentCd,index,cleanFlag);}}
                    classes={{
                      'secondary':classes.fabButtonDelete
                    }}
                >
                  <Remove className={classes.icon} />
                </Fab>
              </div>
            </Grid>
          ):null}
        </Grid>
      );
    });
    return group;
  };

  initGrid = () => {
    let { classes,assessmentItems } = this.props;
    let gridItems = [];
    gridItems = assessmentItems.map((item,index) => {
      let fields = this.generateField(item.fields, item.codeAssessmentCd, item.isMultiple);
      return (
        <Grid
            container
            key={item.codeAssessmentCd}
            direction="row"
            justify="space-evenly"
            className={classes.item_wrapper}
            classes={{
              'container':(index+1)%2===0?classes.assessment_grid_wrapper:null
            }}
        >
          <Grid
              item
              container
              xs={11}
              direction="row"
              justify="space-between"
              alignItems="center"
          >
            <Grid container item direction="row" alignItems="center" spacing={0} wrap="nowrap">
              <Grid
                  item
                  xs={1}
                  classes={{
                    'grid-xs-1': classes.label_assessment
                  }}
              >
                {item.assessmentName}
              </Grid>
              <Grid
                  item
                  container
                  xs={11}
                  classes={{
                    'grid-xs-11': classes.field_grid_wrapper
                  }}
              >
                {fields}
              </Grid>
            </Grid>
          </Grid>
          <Grid
              container
              direction="row"
              justify="center"
              alignItems="flex-end"
              item
              xs
              classes={{
                'container':classes.btnGrid
              }}
          >
            {item.isMultiple === 'Y' ? (
              <Fab
                  size="small"
                  color="primary"
                  aria-label="Add"
                  onClick={() => {this.handleAssessmentFieldsAdd(item.codeAssessmentCd);}}
                  id={`btn_assessment_item_${item.codeAssessmentCd}_add`}
                  classes={{
                    'primary':classes.btnAdd
                  }}
              >
                <Add className={classes.icon} />
              </Fab>
            ) : null}
          </Grid>
        </Grid>
      );
    });
    return gridItems;
  };

  render() {
    let {classes} = this.props;
    return (
      <ValidatorForm className={classes.form_wrapper} id="AssessmentForm" onSubmit={() => {}} ref="form">
        <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
        >
          {this.initGrid()}
        </Grid>
      </ValidatorForm>
    );
  }
}

export default withStyles(styles)(AssessmentGrid);
