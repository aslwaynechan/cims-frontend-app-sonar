import React, { Component } from 'react';
import { styles } from './InputFieldStyle';
import { TextField, withStyles, FormHelperText } from '@material-ui/core';
import { trim } from 'lodash';
import { ErrorOutline } from '@material-ui/icons';

class StringInputField extends Component {
  constructor(props){
    super(props);
    this.state={
      val:'',
      encounterId:''
    };
  }

  static getDerivedStateFromProps(props, state) {
    let { field, rowId, assessmentCd, fieldValMap } = props;
    let val = '',
        required=1;
    if (fieldValMap.has(assessmentCd)) {
      let tempfieldMap = fieldValMap.get(assessmentCd);
      let fieldValArray = tempfieldMap.get(field.codeAssessmentFieldId);
      val = fieldValArray[rowId].val;
      required = field.nullable;
    }
    if (props.encounterId !== state.encounterId||val!==state.val||required!==state.required) {
      return {
        encounterId: props.encounterId,
        val,
        required
      };
    }
    return null;
  }

  handleTextChange = (event, assessmentCd, fieldId, rowId) => {
    let { fieldValMap } = this.props;
    let vals = fieldValMap.get(assessmentCd).get(fieldId);
    vals[rowId].val = event.target.value;
    this.setState({
      val:event.target.value
    });
  };

  handleBlur = (assessmentCd, fieldId, rowId) => {
    let { updateState, fieldValMap } = this.props;
    let vals = fieldValMap.get(assessmentCd).get(fieldId);
    vals[rowId].val = trim(vals[rowId].val);
    updateState && updateState({
      isEdit:true,
      fieldValMap
    });
    this.setState({
      val:vals[rowId].val
    });
  }

  render(){
    let { classes, field, rowId, assessmentCd, saveFlag } = this.props;
    let { val,required } = this.state;

    return(
      <div className={classes.string_field_wrapper}>
        <TextField
            id={`assessment_field_string_${field.codeAssessmentFieldId}_${rowId}`}
            autoCapitalize="off"
            variant="outlined"
            type="text"
            className={classes.normal_input}
            onBlur={()=>{this.handleBlur(assessmentCd,field.codeAssessmentFieldId,rowId);}}
            onChange={event => {this.handleTextChange(event,assessmentCd,field.codeAssessmentFieldId,rowId);}}
            value={val}
            error={!saveFlag&&val===''&&required===0?true:false}
            inputProps={{
              style:{
                fontSize: '1rem',
                fontFamily: 'Arial'
              }
            }}
            InputProps={{
              className:(!saveFlag&&val===''&&required===0)?classes.abnormal:null
            }}
        />
        {(!saveFlag&&val===''&&required===0)?(
            <FormHelperText
                error
                classes={{
                  error:classes.helper_error
                }}
            >
            <ErrorOutline className={classes.error_icon}/>
              this filed is required
            </FormHelperText>
          ):null}
      </div>
    );
  }
}

export default withStyles(styles)(StringInputField);