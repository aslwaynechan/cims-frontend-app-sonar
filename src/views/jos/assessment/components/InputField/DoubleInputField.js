import React, { Component } from 'react';
import { TextField, withStyles, FormHelperText } from '@material-ui/core';
import {styles} from './InputFieldStyle';
import { isUndefined } from 'lodash';
import { DATA_TYPE,specialUnitMap } from '../../../../../constants/assessment/assessmentConstants';
import * as generalUtil from '../../utils/generalUtil';
import { ErrorOutline } from '@material-ui/icons';

class DoubleInputField extends Component {
  constructor(props){
    super(props);
    this.state={
      abnormalFlag1: false,
      abnormalFlag2: false,
      errorFlag1: false,
      errorFlag2: false,
      inputVal1: '',
      inputVal2: '',
      encounterId: ''
    };
  }

  static getDerivedStateFromProps(props, state) {
    let { fieldValMap, assessmentCd, rowId, field, fieldNormalRangeMap } = props;
    let abnormalFlag1 = false,
        abnormalFlag2 = false,
        errorFlag1 = false,
        errorFlag2 = false,
        inputVal1 = '',
        inputVal2 = '',
        required1 = 1,
        required2 = 1;

    if (fieldValMap.has(assessmentCd)) {
      let tempfieldMap = fieldValMap.get(assessmentCd);
      let fieldValArray = tempfieldMap.get(field.codeAssessmentFieldId);
      if (!isUndefined(fieldValArray)) {
        inputVal1 = fieldValArray[rowId].val;
        errorFlag1 = fieldValArray[rowId].isError;
        required1 = field.nullable;
        if (!errorFlag1) {
          abnormalFlag1 = generalUtil.abnormalCheck(inputVal1,assessmentCd,field.codeAssessmentFieldId,fieldNormalRangeMap);
        }
      }
      if (!isUndefined(field.codeAssessmentFieldId2)) {
        let tempfieldMap = fieldValMap.get(assessmentCd);
        let fieldValArray = tempfieldMap.get(field.codeAssessmentFieldId2);
        if (!isUndefined(fieldValArray)) {
          inputVal2 = fieldValArray[rowId].val;
          errorFlag2 = fieldValArray[rowId].isError;
          required2 = field.nullable;
          if (!errorFlag2) {
            abnormalFlag2 = generalUtil.abnormalCheck(inputVal2,assessmentCd,field.codeAssessmentFieldId,fieldNormalRangeMap);
          }
        }
      }
    }
    if (props.encounterId !== state.encounterId||inputVal1!==state.inputVal1||inputVal2!==state.inputVal2||required1!==state.required1||required2!=state.required2) {
      return {
        encounterId: props.encounterId,
        abnormalFlag1,
        abnormalFlag2,
        errorFlag1,
        errorFlag2,
        inputVal1,
        inputVal2,
        required1,
        required2
      };
    }
    return null;
  }

  handleDoubleChange = (event, assessmentCd, fieldId, rowId, targetNum) => {
    let { updateState,fieldValMap,fieldNormalRangeMap } = this.props;
    let vals = fieldValMap.get(assessmentCd).get(fieldId);
    vals[rowId].val = event.target.value;

    if (!generalUtil.illegalValidation(vals[rowId].val,DATA_TYPE.DOUBLE)) {
      vals[rowId].isError = false;
    } else {
      vals[rowId].isError = true;
    }

    let abnormalFlag = generalUtil.abnormalCheck(event.target.value,assessmentCd,fieldId,fieldNormalRangeMap);
    this.setState({
      ['inputVal'+targetNum]:event.target.value,
      ['errorFlag'+targetNum]:vals[rowId].isError,
      ['abnormalFlag'+targetNum]:abnormalFlag
    });
    updateState &&updateState({
      isEdit:true,
      fieldValMap
    });
  }

  render(){
    let { classes, assessmentCd, rowId, field, saveFlag} = this.props;
    let { abnormalFlag1, abnormalFlag2, errorFlag1, errorFlag2, inputVal1, inputVal2, required1, required2} = this.state;

    let numberInputProps = {
      autoCapitalize:'off',
      variant:'outlined',
      type:'text',
      inputProps: {
        style:{
          fontSize: '1rem',
          fontFamily: 'Arial'
        },
        maxLength: 6
      }
    };

    return(
      <div>
        <div className={classes.double_wrapper_1}>
          <TextField
              id={`assessment_field_double_${field.codeAssessmentFieldId}_${rowId}`}
              InputProps={{
                className: errorFlag1?classes.abnormal:(abnormalFlag1?classes.abnormal:(!saveFlag&&inputVal1===''&&required1===0?classes.abnormal:null))
              }}
              error={!errorFlag1?abnormalFlag1?true:(!saveFlag&&inputVal1===''&&required1===0?true:false):true}
              value={inputVal1}
              className={classes.normal_input}
              onChange={event => {this.handleDoubleChange(event,assessmentCd,field.codeAssessmentFieldId,rowId,'1');}}
              {...numberInputProps}
          />
          {!isUndefined(field.objUnit) ? (
            <div className={classes.unit_wrapper}>
              <span className={classes.unit_span} dangerouslySetInnerHTML={{__html:specialUnitMap.has(field.objUnit)?specialUnitMap.get(field.objUnit):field.objUnit}} />
              {/* <span className={classes.unit_span}>{specialUnitMap.has(field.objUnit)?specialUnitMap.get(field.objUnit):field.objUnit}</span> */}
            </div>
          ) : null}
          {errorFlag1?(
            <FormHelperText
                error
                classes={{
                  error:classes.helper_error
                }}
            >
              <ErrorOutline className={classes.error_icon}/>
              Illegal Characters
            </FormHelperText>
          ):(!saveFlag&&inputVal1===''&&required1===0?(
            <FormHelperText
                error
                classes={{
                  error:classes.helper_error
                }}
            >
            <ErrorOutline className={classes.error_icon}/>
              the filed is required
            </FormHelperText>
          ):null)}
          {/* :null} */}
        </div>
        {/* union unit */}
        {!isUndefined(field.codeAssessmentFieldId2) ? (
          <div className={classes.double_wrapper_2}>
            <TextField
                id={`assessment_field_double_${field.codeAssessmentFieldId2}_${rowId}`}
                InputProps={{
                  className: errorFlag2?classes.abnormal:(abnormalFlag2?classes.abnormal:(!saveFlag&&inputVal2===''&&required2===0?classes.abnormal:null))
                }}
                error={!errorFlag2?abnormalFlag2?true:(!saveFlag&&inputVal2===''&&required2===0?true:false):true}
                value={inputVal2}
                className={classes.normal_input}
                onChange={event => {this.handleDoubleChange(event,assessmentCd,field.codeAssessmentFieldId2,rowId,'2');}}
                {...numberInputProps}
            />
            {!isUndefined(field.objUnit2) ? (
              <div className={classes.unit_wrapper}>
                <span className={classes.unit_span} dangerouslySetInnerHTML={{__html:specialUnitMap.has(field.objUnit2)?specialUnitMap.get(field.objUnit2):field.objUnit2}} />
                {/* <span className={classes.unit_span}>{field.objUnit2}</span> */}
              </div>
            ) : null}
            {errorFlag2?(
              <div className={classes.helper_wrapper}>
                <FormHelperText
                    error
                    classes={{
                      error:classes.helper_error
                    }}
                >
                  <ErrorOutline className={classes.error_icon}/>
                  Illegal Characters
                </FormHelperText>
              </div>
            ):(!saveFlag&&inputVal2===''&&required2===0?(
              <div className={classes.helper_wrapper}>
                <FormHelperText
                    error
                    classes={{
                      error:classes.helper_error
                    }}
                >
                <ErrorOutline className={classes.error_icon}/>
                  the filed is required
                </FormHelperText>
              </div>
            ):null)}
          </div>
        ) : null}
      </div>
    );
  }
}

export default withStyles(styles)(DoubleInputField);