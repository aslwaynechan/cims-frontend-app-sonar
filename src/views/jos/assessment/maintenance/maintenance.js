/*
 * Front-end UI for load&save assessment settings
 * Load Action: [maintenance.js] componentDidMount -> initAssessmentMaintenanceState
 * -> [assessmentAction.js] getAssessmentSettingItemList
 * -> [assessmentSaga.js] getAssessmentSettingItemList
 * -> Backend API = assessment/listCodeAssessmentList
 * Save Action: [maintenance.js] Save -> handleSave
 * -> [assessmentAction.js] updateAssessmentSetting
 * -> [assessmentSaga.js] updateAssessmentSettingItemList
 * -> Backend API = assessment/insertAssessmentItems
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Typography,
  Divider,
  Grid,
  FormGroup,
  FormControlLabel,
  Checkbox,
  withStyles
} from '@material-ui/core';
import { getAssessmentSettingItemList,updateAssessmentSetting,getAssessmentCheckedItemList,getTempServiceList } from '../../../../store/actions/assessment/assessmentAction';
import { cloneDeep } from 'lodash';
import { openCommonMessage,closeCommonMessage } from '../../../../store/actions/message/messageAction';
import { openCommonCircularDialog } from '../../../../store/actions/common/commonAction';
import { withRouter } from 'react-router-dom';
// import FieldConstant from '../../../../constants/fieldConstant';
import {styles} from './maintenanceStyle';
import {ASSESSMENT_SETTING_CODE} from '../../../../constants/message/assessmentCode';
import classNames from 'classnames';
import Container from 'components/JContainer';

const mapStatesToProps = state => {
  return {
    loginInfo: {
      ...state.login.loginInfo,
      service_cd:state.login.service.serviceCd,
      service:{
        code:state.login.service.serviceCd
      }
    },
    assessmentItemList: state.assessment.assessmentSettingList
  };
};

const mapDispatchToProps = {
  getAssessmentSettingItemList,
  getAssessmentCheckedItemList,
  updateAssessmentSetting,
  openCommonMessage,
  closeCommonMessage,
  getTempServiceList,
  openCommonCircularDialog
};

class AssessmentMaintenance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedItemsSet: new Set(),
      isEdit:false
    };
  }

  componentDidMount() {
    this.props.ensureDidMount();
    this.initAssessmentMaintenanceState();
  }

  initAssessmentMaintenanceState = () => {
    let { loginInfo } = this.props;
    let params = {
      serviceCd:loginInfo.service_cd
    };
    this.props.getAssessmentSettingItemList({
      params,
      callback:(data)=>{
        this.setState({
          checkedItemsSet: cloneDeep(data)
        });
      }
    });
  };

  handleCheckBoxChange = codeAssessmentCd => event => {
    let { checkedItemsSet } = this.state;
    if (event.target.checked) {
      checkedItemsSet.add(codeAssessmentCd);
    } else {
      checkedItemsSet.delete(codeAssessmentCd);
    }
    this.setState({
      isEdit:true,
      checkedItemsSet
    });
  };

  handleReset = () => {
    let payload = {
      msgCode:ASSESSMENT_SETTING_CODE.RESET_SETTING_COMFIRM,
      btnActions: {
        // Yes
        btn1Click: () => {
          let { checkedItemsSet } = this.state;
          checkedItemsSet.clear();
          this.setState({
            isEdit:true,
            checkedItemsSet
          });
        }
      }
    };
    this.props.openCommonMessage(payload);
  };

  handleSave = () => {
    let { loginInfo } = this.props;
    let { checkedItemsSet } = this.state;
    if (checkedItemsSet.size === 0) {
      this.props.openCommonMessage({
        msgCode:ASSESSMENT_SETTING_CODE.SAVE_SETTING_EMPTY_ITEM
      });
      this.setState({
        isEdit:true
      });
    } else {
      let tempArray = [];
      for (let item of checkedItemsSet.values()) {
        tempArray.push({
          codeAssessmentCd:item,
          serviceCd:loginInfo.service.code
        });
      }
      this.props.openCommonCircularDialog();
      this.props.updateAssessmentSetting({
        params:tempArray,
        callback:(msgCode)=>{
          this.props.openCommonMessage({
            msgCode,
            showSnackbar:true,
            btnActions: {
              // Yes
              btn1Click: () => {

              }
            }
          });
          this.setState({
            isEdit:false
          });
        }
      });
    }
  };

  handleServiceChange = (obj) => {
    this.setState({
      selectedServiceLabel:obj.label,
      selectedServiceVal:obj.value
    });
    let params = {
      serviceCd:obj.value
    };
    this.props.getAssessmentCheckedItemList({
      params,
      callback:(data)=>{
        this.setState({
          checkedItemsSet: cloneDeep(data)
        });
      }
    });
    this.setState({
      isEdit:false
    });
  }

  render() {
    const { classes, loginInfo, assessmentItemList } = this.props;
    let { checkedItemsSet=new Set() } = this.state;

    const buttonBar={
        isEdit:this.state.isEdit,
        // height:'64px',
        position:'fixed',
        buttons:[
            {
                title:'Save',
                onClick:this.handleSave,
                id:'default_save_button'
            },
            {
                title:'Clear',
                onClick:this.handleReset,
                id:'default_clear_button'
            }
        ]
      };

    // let engDesc = loginInfo.service.engDesc;  //目前没有提供这个字段，暂时写死(by wentao)
    let engDesc = JSON.parse(sessionStorage.getItem('service')).serviceName;
    let serviceCd = loginInfo.service.code;
    return (
      <div className={classNames('detail_warp',classes.fixedDiv)}>
        <Container buttonBar={buttonBar} className={'nephele_content_body '+classes.settingWrapperDiv}>
          <Typography component="div">
            {/* title */}
            <Typography component="div">
              <div className={classes.titleFont}>
                Assessment Setting Maintenance
              </div>
              <div className={classes.serviceFont}>
                {`Service: ${engDesc} (${serviceCd})`}
              </div>
            </Typography>
            <Divider />
            {/* checkbox */}
            <Typography component="div" className={classes.wrapper}>
              <Grid container>
                <FormGroup className={classes.fullWidth} row>
                  {assessmentItemList.map(item => {
                    return (
                      <Grid
                          key={Math.random()}
                          className={classes.checkBoxGrid}
                          item
                          xs={4}
                      >
                        <FormControlLabel
                            classes={{
                            label: classes.normalFont
                          }}
                            control={
                            <Checkbox
                                id={item.codeAssessmentCd}
                                color="primary"
                                checked={checkedItemsSet.has(item.codeAssessmentCd)?true:false}
                                onChange={this.handleCheckBoxChange(
                                item.codeAssessmentCd
                              )}
                            />
                          }
                            label={item.assessmentName}
                        />
                      </Grid>
                    );
                  })}
                </FormGroup>
              </Grid>
            </Typography>
            {/* button */}
            {/* <Typography component="div" className={classes.fixedBottom}>
              <Grid container alignItems="center" justify="flex-end">
                <Typography component="div">
                  <CIMSButton
                      classes={{
                        label:classes.fontLabel
                      }}
                      id="btn_assessment_maintenance_save"
                      color="primary"
                      size="small"
                      onClick={this.handleSave}
                  >
                    Save
                  </CIMSButton>
                </Typography>
                <CIMSButton
                    classes={{
                      label:classes.fontLabel
                    }}
                    className={classes.btnClear}
                    id="btn_assessment_maintenance_reset"
                    color="primary"
                    size="small"
                    onClick={this.handleReset}
                >
                  Clear
                </CIMSButton>
              </Grid>
            </Typography> */}
          {/* <Prompt
              message={FieldConstant.LEAVE_CONFIRM_PROMPT_CONTENT}
              when={isEdit}
            /> */}
            </Typography>
        </Container>
      </div>
    );
  }
}

export default withRouter(connect(mapStatesToProps,mapDispatchToProps)(withStyles(styles)(AssessmentMaintenance)));
