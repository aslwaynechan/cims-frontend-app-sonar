export const styles = {
  settingWrapperDiv: {
    width: '100%'
  },
  fullWidth: {
    width: '100%',
    minWidth:600
  },
  checkBoxGrid: {
    marginTop: 0
  },
  btnClear: {
    marginLeft: 0
  },
  normalMsg: {
    fontWeight: 600,
    display: 'inline',
    color: '#0579C8'
  },
  normalFont: {
    fontSize: '1rem',
    fontFamily: 'Arial'
  },
  serviceLabel: {
    fontSize:'1rem',
    float: 'left',
    paddingTop: '3px',
    paddingRight: '10px',
    fontFamily: 'Arial',
    fontWeight: 'bold'
  },
  wrapper: {
    height: 'calc(100% - 255px)',
    marginRight: 8,
    marginTop: 8,
    position: 'fixed',
    overflowY: 'auto'
  },
  fixedBottom: {
    margin: 0,
    color: '#6e6e6e',
    position:'fixed',
    bottom: 5,
    width: '100%',
    zIndex: 100,
    backgroundColor: '#FFFFFF',
    right: 30
  },
  fixedDiv: {
    position: 'fixed'
  },
  titleFont:{
    color: '#000000',
    fontSize: '1.5rem',
    fontFamily:'Arial'
  },
  fontLabel: {
    fontFamily:'Arial',
    fontSize: '1rem'
  },
  serviceFont: {
    padding: 10,
    fontWeight:'bold',
    fontFamily:'Arial',
    fontSize: '1rem'
  }
};
