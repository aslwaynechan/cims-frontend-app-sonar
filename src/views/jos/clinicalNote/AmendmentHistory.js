import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    Tabs,
    Tab,
    Paper,
    ExpansionPanel,
    ExpansionPanelDetails,
    Typography,
    ExpansionPanelSummary,
    Grid,
    Avatar,
    List,
    ListItem,
    ListItemAvatar,
    ListItemText
} from '@material-ui/core';
import { ExpandMore, AccessTime,Add,Remove } from '@material-ui/icons';
import TabContainer from './components/TabContainer';
import moment from 'moment';
import { lowerCase } from 'lodash';

const styles = {
    root: {
        flex: '0 0 auto',
        padding: '12px 12px 0 12px',
        width:'100%'
    },
    summary: {
        minHeight: 'auto!important',
        maxHeight: '48px',
        backgroundColor:'#D1EEFC !important',
        color:'#0579C8'
    },
    disabledSummary: {
        minHeight: 'auto!important',
        maxHeight: '48px',
        backgroundColor:'#B8BCB9 !important',
        color:'#404040'
    },
    expansionPanel:{
        padding: 0,
        overflow: 'hidden',
        display: 'block'
    },
    tabsBar:{
        backgroundColor: '#f5f5f5'
    },
    tab: {
        textTransform: 'none'
    },
    primary:{
        color:'#333',
        fontWeight:'bold'
    },
    secondary:{
        whiteSpace:'pre-line',
        color:'#777'
    },
    expansionPanelSummary:{
        paddingLeft:8
    },
    rootItemAvatar:{
        minWidth:38,
        marginTop:0
    },
    rootAvatar:{
        width:30,
        height:30
    }
};

const noteTypeMap={
    D:'Doctor',
    N:'Nurse',
    C:'Counter',
    CP:'CP',
    DI:'Dietitian',
    AH:'AH'
  };

class AmendmentHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentTab: 'doctorNote',
            expanded:false
        };
    }

    handleTabsChange = (event, value) => {
        this.setState({ currentTab: value });
    };

    handleExpansionPanelChange = (event, isExpanded) => {
        if(isExpanded){
            this.setState({
              expanded:true
            });
        }else{
          this.setState({
            expanded:false
          });
        }
    };


    renderAmendmentHistory=(historys=[],index)=>{
        const {classes}=this.props;
        let node;
        if(historys.length){
            node=(
                <List style={{padding:0}}>
                    {historys.map((item,i)=>{
                        return (
                            <ListItem alignItems="flex-start" key={i} >
                                {/* <ListItemIcon>
                                    <AccessTime />
                                </ListItemIcon>  */}
                               <ListItemAvatar classes={{root:classes.rootItemAvatar}} >
                                  <Avatar classes={{root:classes.rootAvatar}}>
                                      <AccessTime />
                                  </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={
                                    <Typography   component="div" className={classes.primary}>
                                        {`${moment(item.updatedDtm).format('YYYY-MM-DD HH:mm')} (By ${item.updatedBy})`}
                                    </Typography>}
                                    secondary={<Typography className={classes.secondary}>{item.clinicalnoteText}</Typography>}
                                />
                            </ListItem>
                        );
                    })}
                </List>
            );
        }else{
            node= `No ${index} amendment history`;
        }
        return node;
    }

    render() {

        const { classes,amendmentHistoryList = {},loginInfo} = this.props;
        let {logDoctorNoteDto,logServiceNoteDto} = amendmentHistoryList;
        let { currentTab ,expanded} = this.state;
        return (
            <Grid item xs={12} className={classes.root}>
                <ExpansionPanel disabled={!(logDoctorNoteDto || logServiceNoteDto)}  onChange={this.handleExpansionPanelChange}>
                    <ExpansionPanelSummary expandIcon={<ExpandMore />} className={!(logDoctorNoteDto || logServiceNoteDto)?classes.disabledSummary:classes.summary}
                        classes={{root:classes.expansionPanelSummary}}
                    >
                    {expanded?<Remove/>:<Add/>}
                        <Typography>Amendment History</Typography>
                    </ExpansionPanelSummary>

                    <ExpansionPanelDetails className={classes.expansionPanel}>
                        <Paper square className={classes.tabsBar}>
                            <Tabs
                                value={currentTab}
                                onChange={this.handleTabsChange}
                                variant="fullWidth"
                                indicatorColor="primary"
                                textColor="primary"
                            >
                                <Tab
                                    label={`${noteTypeMap[loginInfo.userRoleType]} Note`}
                                    value={'doctorNote'}
                                    className={classes.tab}
                                />
                                <Tab
                                    label="Service Note"
                                    value={'serviceNote'}
                                    className={classes.tab}
                                />
                            </Tabs>
                        </Paper>
                        <TabContainer value={currentTab} index={'doctorNote'}>
                            {this.renderAmendmentHistory(logDoctorNoteDto||[],`${lowerCase(noteTypeMap[loginInfo.userRoleType])} note`)}
                        </TabContainer>
                        <TabContainer value={currentTab} index={'serviceNote'}>
                            {this.renderAmendmentHistory(logServiceNoteDto||[],'service note')}
                        </TabContainer>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </Grid>
        );
    }
}

export default withStyles(styles)(AmendmentHistory);
