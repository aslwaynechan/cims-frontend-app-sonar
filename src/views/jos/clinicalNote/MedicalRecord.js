import React,{Component} from 'react';
import Box from './components/Box';
import { Grid,FormControl,InputLabel} from '@material-ui/core';
import Table from '../../../components/JTable';
import Select from '../../../components/JSelect';
import {debounce} from 'lodash';
import moment from 'moment';
import { MTableBodyRow } from 'material-table';

const Filter=({services,recordTypes,currentService,handleServiceChange,handleRecordTypeChange})=>{
  return(
    <Grid container spacing={2} style={{padding:'6px 12px'}}>
      <Grid item xs={6}>
        <FormControl style={{width:'100%',fontFamily:'Arial'}}>
          <InputLabel>Service</InputLabel>
          <Select options={services} value={currentService} onChange={handleServiceChange}/>
        </FormControl>
      </Grid>
      <Grid item xs={6}>
        <FormControl style={{width:'100%',fontFamily:'Arial'}}>
          <InputLabel>Record Type</InputLabel>
          <Select options={recordTypes} onChange={handleRecordTypeChange}/>
        </FormControl>
      </Grid>
    </Grid>
  );
};

const List=({data,wrapperContentHeight,onSelectionChange,handleMedicalRecordStart})=>{
  const columns=[
    {title:'Visit Date',field:'createdDtm',  headerStyle: {width: '29%'},cellStyle:{padding:4},render:record=>{
      return moment(record.createdDtm).format('DD-MMM-YYYY');
    }},
    {title:'Service',field:'serviceCd',cellStyle:{padding:4}},
    {title:'Record Type',field:'noteTypeDesc',cellStyle:{padding:4}}
  ];

  const options={
    maxBodyHeight:wrapperContentHeight-58||'undefined'
  };
  return (
    <Table id="medical_table" columns={columns} data={data} options={options} onSelectionChange={onSelectionChange} size="small"
        components={{
      Row: props => {
          return (
              <MTableBodyRow
                  {...props}
                  draggable="true"
                  onDragStart={(e)=>{handleMedicalRecordStart(e,props.data);}}
              />
          );
      }
    }}
    />
  );
};


class MedicalRecord extends Component{
  constructor(props) {
    super(props);
    this.boxContent=React.createRef();
    this.state={};
  }

  componentDidMount(){
    this.resetHeight();
    window.addEventListener('resize',this.resetHeight);
  }

  componentWillUnmount(){
    window.removeEventListener('resize',this.resetHeight);
  }

  resetHeight=debounce(()=>{
    if(this.boxContent.current&&this.boxContent.current.clientHeight&&this.boxContent.current.clientHeight!==this.state.height){
      this.setState({
        height:this.boxContent.current.clientHeight
      });
    }
  },1000);



  render() {
    const {medicalRecords=[],height,onSelectionChange,handleStart,...rest}=this.props;
    return (
      <Box ref={this.boxContent} title={'Clinical Note History'} height={height}>
        <Filter {...rest}/>
        <List data={medicalRecords} wrapperContentHeight={this.state.height} onSelectionChange={onSelectionChange}
            handleMedicalRecordStart={handleStart}
        />
      </Box>
    );
  }
}

export default MedicalRecord;
