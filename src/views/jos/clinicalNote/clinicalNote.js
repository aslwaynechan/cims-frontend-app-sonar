import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid} from '@material-ui/core';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { openCommonMessage } from '../../../store/actions/message/messageAction';
import { CLINICALNOTE_CODE } from '../../../constants/message/clinicalNoteCode';
import MedicalRecord from './MedicalRecord';
import RecordDetail from './RecordDetail';
import Topbar from './Topbar';
import Note from './Note';
import * as actionTypes from '../../../store/actions/clinicalNote/clinicalNoteActionType';
import * as enquireTypes from '../../../store/actions/IOE/enquiry/enquiryActionType';
import {debounce} from 'lodash';
import AmendmentHistory from './AmendmentHistory';
import Container from 'components/JContainer';
import {ServiceNote,DoctorNote,NurseNote,CounterNote,CPNote,DietitianNote,AHNote,PersonNote} from '../../../components/Icons';

const dict={
  N:'NURSE',
  NN:'NURSE',
  NNS:'NURSE',
  D:'DOCTOR',
  DN:'DOCTOR',
  DNS:'DOCTOR',
  C:'CP'
};

const customTheme = (theme) =>{
  return createMuiTheme({
    ...theme,
    overrides: {
      MuiInputBase:{
        multiline:{
          height:'100%'
        },
        inputMultiline:{
          height:'100%'
        }
      },
      MuiBottomNavigation:{
        root:{
          height:'36px',
          '& .Mui-disabled':{
            color:'#ccc'
          }
        }
      },
      MuiBottomNavigationAction:{
        wrapper:{
          flexDirection:'inherit'
        }
      },
      MuiButtonGroup:{
        contained:{
          boxShadow:'note'
        }
      },
      MuiButton:{
        label:{
          fontSize:'0.8125rem'
        }
      },
      MuiInputLabel:{
        outlined:{
          display:'inherit'
        //   lineHeight:'13px'
        }
      }
    }
  });
};

function NoteLabel(props) {
    const { title, Icon } = props;
    return (
        Icon!=undefined? <><Icon style={{fontSize:'1rem',marginRight:4}}/> {` ${title}`} </>:title
    );
}

class ClinicalNote extends Component {
  constructor(props) {
    super(props);
    const {loginInfo={},encounter}=props;
    const {service={},userRoleType}=loginInfo;
    this.dragTargetContentType = '';
    this.state = {
      params:{
        curServiceCd:service.serviceCd,
        encounterId:encounter.encounterId,
        serviceCd:service.serviceCd,
        recordTypeCd:'ALL',
        userRoleType:userRoleType
      },
      todayNotes:{
        N:{clinicalnoteId:0,clinicalnoteText:''},
        SN:{clinicalnoteId:0,clinicalnoteText:''}
      },
      N:'',
      SN:'',
      PN:'',
      currentNote:'N',
      templateOpen:false,
	    userLogName:JSON.parse(sessionStorage.getItem('loginInfo')).loginName || null,
	    retrieveTitle:'Retrieve',
      retrieveOpen:false,
      personalNoteType:false,
      medicalRecords:[],
      focusIndex:0,
      clinicalNote:null,
      personalNote:null,
      serviceNote:null,
      isEdit:false,
      isRecordEdit:false,
      assessmentTextList:[],
      previousDxTextList:[],
      chronicProblemTextList:[],
      previousIOETextList:[],
      previousMOETextList:[]
    };
  }
  componentDidMount() {
    this.props.ensureDidMount();
    const {dispatch,loginInfo={}}=this.props;
    // dispatch({type:actionTypes.REQUEST_DATA,callback:()=>{
    dispatch({type:enquireTypes.getServices,params:{},callback:(data)=>{
      this.serviceList(data);
      this.getClinicalNoteData();
      dispatch({type:actionTypes.GET_RECORDTYPE_DATA_LIST}); //获取recordType 列表
    }}); //获取service 列表
  }

  serviceList=(data)=>{
    let defaultService = [{value:'ALL' , title: 'ALL'}];
    let services = data.map(item => {
        return { value: item.serviceCd, title: item.serviceCd };
    });
    services = defaultService.concat(services);
    this.setState({services:services});
  }

  getClinicalNoteData=()=>{
    const {dispatch}=this.props;
    const {patientKey} =this.props.patientInfo;
    let params={
      patientKey:patientKey,
      encounterId: this.state.params.encounterId,
      userRoleType:this.state.params.userRoleType
    };
    dispatch({type:actionTypes.GET_CLINICALNOTE,params:params,callback:(data)=>{
      if(data.respCode===0){
        this.getAmendmentHistory(data.data.clinicalNote,data.data.serviceNote);
        this.setState({
          clinicalNote:data.data.clinicalNote,
          N:data.data.clinicalNote===null?'':data.data.clinicalNote.clinicalnoteText,
          personalNote:data.data.personalNote,
          PN:data.data.personalNote===null?'':data.data.personalNote.personalNoteText,
          serviceNote:data.data.serviceNote,
          SN:data.data.serviceNote===null?'':data.data.serviceNote.clinicalnoteText
        });
      }
      else{
        dispatch(openCommonMessage({msgCode:data.msgCode}));
      }
    }});
  }

  getAmendmentHistory=(doctorNote,serviceNote)=>{
    let params={};
    let {dispatch}=this.props;
    if(doctorNote===null){
      params.doctorNoteId=0;
    }else{
      params.doctorNoteId=doctorNote.clinicalnoteId;
    }

    if(serviceNote===null){
      params.serviceNoteId=0;
    }else{
      params.serviceNoteId=serviceNote.clinicalnoteId;
    }
    dispatch({type:actionTypes.GET_AMENDMENT_HISTORY_LIST,params:params});
  }

  setNewData=(medicalRecords)=>{
    this.setState({
      medicalRecords:medicalRecords,
      note:{}
    });
  }

  getRecords=(params)=>{
    const {dispatch}=this.props;
    const {patientKey} =this.props.patientInfo;
    let medicalListParams={
      patientKey:patientKey,
      recordType:params.recordTypeCd,
      selectedServiceCd:params.serviceCd,
      encounterId: this.state.params.encounterId,
      userRoleType:this.state.params.userRoleType
    };
    dispatch({type:actionTypes.GET_MEDICALRECORD_LIST,params:medicalListParams,callback:(data)=>{
      if(data.respCode===0){
        this.setNewData(data.data);
      }else{
        dispatch(openCommonMessage({msgCode:data.msgCode}));
      }

    }

    });
    this.setState({params,note:undefined});
  }

  handleNoteFocus=(note)=>{
    this.setState({
      currentNote:note,
      focusIndex:0
    });
  }

  handleNoteChange=(value,type=this.state.currentNote)=>{
    this.setState({
      [type]:value,
      isEdit:true
    });
    this.clinicalNoteTextChange(type,value);
  }

  clinicalNoteTextChange=(currentNote,value)=>{
    if(currentNote==='N'){
      let clinicalNote=this.state.clinicalNote;
      if(clinicalNote!==null){
        clinicalNote.clinicalnoteText=value;
        this.setState({clinicalNote});
      }
      else{
        const {patientKey} =this.props.patientInfo;
        let clinicalNote={
          encounterId:this.state.params.encounterId,
          codeClinicalnoteTypeCd:'N',
          userRoleTypeCd:this.state.params.userRoleType,
          serviceCd:this.state.params.serviceCd,
          patientKey:patientKey,
          clinicalnoteText:value
        };
        this.setState({clinicalNote});
      }
    }
    else if(currentNote==='PN'){
      let personalNote=this.state.personalNote;
      if(personalNote!==null){
        personalNote.personalNoteText=value;
        this.setState({personalNote});
      }
      else{
        const {patientKey} =this.props.patientInfo;
        let {loginName}=this.props.loginInfo;
        let personalNote={
          patientKey:patientKey,
          personalNoteText:value,
          userId:loginName
        };
        this.setState({personalNote});
      }
    }
    else if(currentNote==='SN'){
      let serviceNote=this.state.serviceNote;
      if(serviceNote!==null){
        serviceNote.clinicalnoteText=value;
        this.setState({serviceNote});
      }
      else{
        const {patientKey} =this.props.patientInfo;
        let serviceNote={
          encounterId:this.state.params.encounterId,
          codeClinicalnoteTypeCd:'N',
          userRoleTypeCd:this.state.params.userRoleType,
          serviceCd:this.state.params.serviceCd,
          patientKey:patientKey,
          clinicalnoteText:value
        };
        this.setState({serviceNote});
      }
    }
    this.setState({});
  }

  handleNoteAppend=(value)=>{
      this.insertTextInTextArea(value);
  }

  handleNoteCopy=(value)=>{
    let newNoteFlag = false;
    let type=this.state.currentNote==='PN'?'personalNote':(this.state.currentNote==='SN'?'serviceNote':(this.state.currentNote==='N'?'doctorNote':''));
    let startPro=document.getElementById(type).selectionStart;
    if(startPro===this.state[this.state.currentNote].length&&startPro===0){
      newNoteFlag = true;
    }
    let result='';
    value.map((item,index)=>{
      if(value.length-1===index)
        result=result+item;
      else
        result=result+item+'\n';
    });
    let newValue = '';
    let oldValue = '';
    newValue= newValue + result;
    oldValue= result===''?oldValue+result:oldValue + '\n'+result;
    newNoteFlag?this.handleNoteChange(newValue):this.insertTextInTextArea(oldValue);
  }

  insertTextInTextArea=(value)=>{
    let type=this.state.currentNote==='PN'?'personalNote':(this.state.currentNote==='SN'?'serviceNote':(this.state.currentNote==='N'?'doctorNote':''));
    let startPro;
    if(this.state.focusIndex>0){
      startPro=this.state.focusIndex;
    }else{
      let doc=document.getElementById(type);
      startPro=doc.selectionStart;
    }
    let currentNote=this.state[this.state.currentNote]===null?'':this.state[this.state.currentNote];

    let preText=currentNote.slice(0,startPro);
    let lastText=currentNote.slice(startPro,currentNote.length);
    let midText=value;
    let result=preText+midText+lastText;
    let myField=document.getElementById(type);
    //自动设置焦点
    myField.focus();
    setTimeout(function() {
      myField.setSelectionRange(startPro+value.length,startPro+value.length);
    }, 0);
    this.setState({
      [this.state.currentNote]:result,
      focusIndex:startPro+value.length
    });
    this.clinicalNoteTextChange(this.state.currentNote,result);
  }

  handleInsertNote=(value)=>{
    let current=this.state.currentNote;
    this.setState({
      [current]:this.state[current]+value+''
    });
  }

  handleItemCopy=(value)=>{
    let newValue = value;
    value = '\n'+value;
    let type=this.state.currentNote==='PN'?'personalNote':(this.state.currentNote==='SN'?'serviceNote':(this.state.currentNote==='N'?'doctorNote':''));
    let startPro=document.getElementById(type).selectionStart;
    if(startPro===this.state[this.state.currentNote].length&&startPro===0){
      this.handleNoteChange(newValue);
    }else{
      this.insertTextInTextArea(value);
    }
  }

  toggleTemplate=()=>{
    if(!this.state.templates){
      const {dispatch}=this.props;
      dispatch(
        {
          type:actionTypes.GET_TEMPLATE_DATA_LIST,
          templateParams:{curServiceCd:this.state.params.curServiceCd,userLogName:this.state.userLogName},
          callback:(favTemplate,serTemplate)=>{
            this.setState({templates:{N:favTemplate,SN:serTemplate},templateOpen:!this.state.templateOpen});
          }
        }
      );
    }else{
      this.setState({
        templateOpen:!this.state.templateOpen
      });
    }
  }

  handleSaveRecoreDetails=(text)=>{
    const {dispatch}=this.props;
    dispatch({
      type:actionTypes.SAVE_RECORD_DETAIL_DATA,
      params:{
        ...this.state.selected,
        clinicalnoteText:text
      },
      callback:(res)=>{
        if(res.respCode===0){
          dispatch(openCommonMessage({msgCode:res.msgCode,showSnackbar:res.msgCode=='100403'}));
          this.resetNoteValue(res.data);
          this.setState({note:{...this.state.note,content:text},selected:res.data,isRecordEdit:false});
          const {patientKey} =this.props.patientInfo;
          let medicalListParams={
            patientKey:patientKey,
            recordType:this.state.params.recordTypeCd,
            selectedServiceCd:this.state.params.serviceCd,
            encounterId: this.state.params.encounterId,
            userRoleType:this.state.params.userRoleType
          };
          dispatch({type:actionTypes.GET_MEDICALRECORD_LIST,params:medicalListParams,callback:(medicalRecords)=>{
            this.setNewData(medicalRecords.data);
          }});
        }else{
          dispatch(openCommonMessage({msgCode:res.msgCode}));
        }

      }
    });
  }

  resetNoteValue=(data)=>{
      let type = data.codeClinicalnoteTypeCd;
      let typeObject = type==='N'?'clinicalNote':(type==='PN'?'personalNote':(type==='SN'?'serviceNote':''));
      if(typeObject==='clinicalNote'){
        if(data.clinicalnoteId === this.state.clinicalNote.clinicalnoteId){
          this.setState({
            [type]:data.clinicalnoteText,
            [typeObject]:data
          });
        }
      }
      else if(typeObject==='personalNote'){
        if(data.clinicalnoteId === this.state.personalNote.clinicalnoteId){
          this.setState({
            [type]:data.clinicalnoteText,
            [typeObject]:data
          });
        }
      }
      else if(typeObject==='serviceNote'){
        if(data.clinicalnoteId === this.state.serviceNote.clinicalnoteId){
          this.setState({
            [type]:data.clinicalnoteText,
            [typeObject]:data
          });
        }
      }
  }

  handleDeleteRecordDetails=()=>{
    const {dispatch}=this.props;
    const payload={
      msgCode: CLINICALNOTE_CODE.DELETE_NOTE,
      btnActions: {
        btn1Click:()=>{
          dispatch({
            type:actionTypes.DELETE_RECORD_DETAIL_DATA,
            params:this.state.selected,
            callback:(msgCode)=>{
              if(msgCode==='100415'){
                this.resetDeletedNoteValue();
                this.setState({selected:undefined,note:undefined});
                dispatch(openCommonMessage({msgCode,showSnackbar:msgCode=='100415'}));
                const {patientKey} =this.props.patientInfo;
                let medicalListParams={
                  patientKey:patientKey,
                  recordType:this.state.params.recordTypeCd,
                  selectedServiceCd:this.state.params.serviceCd,
                  encounterId: this.state.params.encounterId,
                  userRoleType:this.state.params.userRoleType
                };
                dispatch({type:actionTypes.GET_MEDICALRECORD_LIST,params:medicalListParams,callback:(medicalRecords)=>{
                  this.setNewData(medicalRecords.data);
                }});
              }else{
                dispatch(openCommonMessage({msgCode:msgCode}));
              }
            }
          });
        }
      }
    };
    dispatch(openCommonMessage(payload));
  }

  resetDeletedNoteValue=()=>{
    let type = this.state.selected.codeClinicalnoteTypeCd;
    let typeObject = type==='N'?'clinicalNote':(type==='PN'?'personalNote':(type==='SN'?'serviceNote':''));
    if(typeObject==='clinicalNote'){
      if(this.state.selected.clinicalnoteId === this.state.clinicalNote.clinicalnoteId){
        this.setState({
          [type]:'',
          [typeObject]:null
        });
      }
    }
    else if(typeObject==='personalNote'){
      if(this.state.selected.clinicalnoteId === this.state.personalNote.clinicalnoteId){
        this.setState({
          [type]:'',
          [typeObject]:null
        });
      }
    }
    else if(typeObject==='serviceNote'){
      if(this.state.selected.clinicalnoteId === this.state.serviceNote.clinicalnoteId){
        this.setState({
          [type]:'',
          [typeObject]:null
        });
      }
    }
}

  handleNoteClean=()=>{
    const {dispatch}=this.props;
    const payload={
      msgCode: CLINICALNOTE_CODE.CLEAR_CLINICALNOTE_COMFIRM,
      btnActions: {
        btn1Click:()=>{
          this.setState({N:'',SN:''});
        }
      }
    };
    dispatch(openCommonMessage(payload));
  }

  handleNoteSave=()=>{
    const {dispatch}=this.props;
    let params={
      clinicalNote:this.state.clinicalNote,
      personalNote:this.state.personalNote,
      serviceNote:this.state.serviceNote,
      encounterInfo:this.props.encounter
    };
    dispatch({
      type:actionTypes.SAVE_CLINICALNOTE,
      params,
      callback:(data)=>{
        if(data.respCode===0){
          dispatch(openCommonMessage({msgCode:data.msgCode,showSnackbar:data.msgCode=='100403'}));
          this.getAmendmentHistory(data.data.clinicalNote,data.data.serviceNote);
          this.setState({
            clinicalNote:data.data.clinicalNote,
            N:data.data.clinicalNote===null?'':data.data.clinicalNote.clinicalnoteText,
            personalNote:data.data.personalNote,
            PN:data.data.personalNote===null?'':data.data.personalNote.personalNoteText,
            serviceNote:data.data.serviceNote,
            SN:data.data.serviceNote===null?'':data.data.serviceNote.clinicalnoteText,
            isEdit:false
          });
          const {patientKey} =this.props.patientInfo;
          let medicalListParams={
            patientKey:patientKey,
            recordType:this.state.params.recordTypeCd,
            selectedServiceCd:this.state.params.serviceCd,
            encounterId: this.state.params.encounterId,
            userRoleType:this.state.params.userRoleType
          };
          dispatch({type:actionTypes.GET_MEDICALRECORD_LIST,params:medicalListParams,callback:(medicalRecords)=>{
            this.setNewData(medicalRecords.data);
          }});
        }else{
          dispatch(openCommonMessage({msgCode:data.msgCode}));
        }
      }
    });
  }

  toggleRetrieve=()=>{
    const {patientKey} =this.props.patientInfo;
    const {dispatch}=this.props;
    dispatch({
      type:actionTypes.GET_COPY_DATA,
      params:{
        patientKey:patientKey,
        encounterId:this.state.params.encounterId,
        copyType:'CA,CP,DX'
      },
      callback:(data)=>{
        this.setState({
          assessmentTextList:data.copyAssmentStr==null?[]:data.copyAssmentStr,
          previousDxTextList:data.copyDxStr==null?[]:data.copyDxStr,
          chronicProblemTextList:data.copyChronicProblemStr==null?[]:data.copyChronicProblemStr
        });
      }
    });

    this.setState({
      retrieveOpen:!this.state.retrieveOpen
      // retrieveTitle:!this.state.retrieveOpen?'COPY':'Retrieve'
    });
  }

  handleServiceChange=(value)=>{
    let newParams={
      ...this.state.params,
      serviceCd:value
    };
    this.getRecords(newParams);
  }

  handleRecordTypeChange=(value)=>{
    let newParams={
      ...this.state.params,
      recordTypeCd:value
    };
    this.getRecords(newParams);
  }

  onSelectionChange=(selected)=>{
    let {params={}}=this.state;
    let {curServiceCd,userRoleType}=params;
    let current=selected[0]||{};
    let note={
      id:current.clinicalnoteId,
      content:current.clinicalnoteText||'',
      readOnly:curServiceCd!==current.serviceCd||dict[current.userRoleTypeCd]!=dict[userRoleType]
    };
    this.setState({
      selected:current,
      note:note
    });
  }

  handleValueChange=(value)=>{
    this.setState({
        isRecordEdit:value
      });
  }

  handleStart = (event,selectedChronicProblem) => {
    event.dataTransfer.setData('text/plain',JSON.stringify(selectedChronicProblem));
    this.dragTargetContentType = 'M';
  };

  handleDragOver = event => {
    event.preventDefault();
    if (this.dragTargetContentType !== 'M') {
        event.dataTransfer.dropEffect = 'none';
    } else {
        event.dataTransfer.dropEffect = 'all';
    }
  }

  handleDrop = (event,type) => {
    event.preventDefault();
    let currentText = this.state[type];
    let newText = currentText+JSON.parse(event.dataTransfer.getData('text/plain')).clinicalnoteText;
    let id=type==='PN'?'personalNote':(type==='SN'?'serviceNote':(type==='N'?'doctorNote':''));
    console.log('id====='+id);
    document.getElementById(id).focus();
    this.handleNoteChange(newText,type);
    this.setState({
      currentNote:type
    });
  }

  render(){
	 const {recordTypes=[],loginInfo={},amendmentHistoryList=[]}=this.props;
    // const {}=this.props.enquiry;
    const {services=[],medicalRecords=[],clinicalNote,serviceNote} =this.state;
    const medicalRecordProps={
      services,
      recordTypes,
      medicalRecords,
      currentService:this.state.params.serviceCd,
      handleServiceChange:this.handleServiceChange,
      handleRecordTypeChange:this.handleRecordTypeChange,
      onSelectionChange:this.onSelectionChange,
      handleStart:this.handleStart
    };
    const recordDetailProps={
      note:this.state.note,
      onCopy:this.handleNoteAppend,
      onSave:this.handleSaveRecoreDetails,
      onDelete:this.handleDeleteRecordDetails,
      onValueChange:this.handleValueChange
    };

    const topbarProps={
      toggleTemplate:this.toggleTemplate,
      onClick:this.handleNoteAppend,
      onCopy:this.handleNoteCopy,
      onItemCopy:this.handleItemCopy,
      templateOpen:this.state.templateOpen,
      templates:this.state.templates||{},
      noteInfo:clinicalNote===null?serviceNote:clinicalNote,
      retrieve:this.state.retrieveTitle,
      retrieveOpen:this.state.retrieveOpen,
      toggleRetrieve:this.toggleRetrieve,
      loginInfo:loginInfo,
      assessmentTextList:this.state.assessmentTextList,
      previousDxTextList:this.state.previousDxTextList,
      chronicProblemTextList:this.state.chronicProblemTextList,
      previousIOETextList:this.state.previousIOETextList,
      previousMOETextList:this.state.previousMOETextList
    };

    const amendmentHistory={
      id:'amendmentHistory',
      amendmentHistoryList:JSON.parse(JSON.stringify(amendmentHistoryList)),
      loginInfo:loginInfo
    };

    const noteTypeMap={
      D:'Doctor',
      N:'Nurse',
      C:'Counter',
      CP:'CP',
      DI:'Dietitian',
      AH:'AH'
    };

    const noteIconTypeMap={
        D:DoctorNote,
        N:NurseNote,
        C:CounterNote,
        CP:CPNote,
        DI:DietitianNote,
        AH:AHNote
      };


    const buttonBar={
      isEdit:this.state.isEdit||this.state.isRecordEdit?true:false,
      // height:'64px',
      // position:'fixed',
      buttons:[{
        title:'Save',
        onClick:this.handleNoteSave,
        id:'default_save_button'
      }]
    };
    return (
        <MuiThemeProvider theme={customTheme}>
          <Grid container spacing={0} style={{height:'100%'}}>
            <Grid item xs style={{flex:'0 0 400px',width:'300px',background:'#ccc',height:'100%'}}>
              <MedicalRecord {...medicalRecordProps} height={'60%'}/>
              <RecordDetail {...recordDetailProps} height={'40%'}/>
            </Grid>
          {/*<Grid item xs container direction={'column'} justify={'space-between'} alignItems={'stretch'}>*/}
            <Container buttonBar={buttonBar} item xs container direction={'column'} justify={'space-between'} alignItems={'stretch'}>
                <Topbar {...topbarProps}/>
                  <AmendmentHistory {...amendmentHistory}/>
                <Grid item container spacing={2} style={{flex:'auto',padding:'12px',boxSizing:'border-box',overflow:'hidden'}}>
                  <Note id={'doctorNote'} title={<NoteLabel title={`${noteTypeMap[loginInfo.userRoleType]} Note`} Icon={noteIconTypeMap[loginInfo.userRoleType]}/>}
                      noteType={'N'} value={this.state.N} onChange={debounce(this.handleNoteChange,100)} onFocus={this.handleNoteFocus}
                      handleDragOver={this.handleDragOver} handleDrop={(e)=>this.handleDrop(e,'N')}
                      xs={12} md={8}
                  />
                  <Grid item  xs={12} md={4}>
                    <Grid  container
                        direction="column"
                        spacing={0} style={{flex:'auto',boxSizing:'border-box',height:'100%'}}
                    >
                      <Note id={'personalNote'} title={<NoteLabel  title={'Personal Note'} Icon={PersonNote} />}
                          noteType={'PN'} value={this.state.PN} onChange={debounce(this.handleNoteChange,100)} onFocus={this.handleNoteFocus}
                          handleDragOver={this.handleDragOver} handleDrop={(e)=>this.handleDrop(e,'PN')}
                          xs={12} md={5} style={{flexBasis:'49%',maxWidth:'100%'}}
                      />
                      <div style={{height:'2%'}}></div>
                      <Note id={'serviceNote'} title={<NoteLabel title={'Service Note'} Icon={ServiceNote} />}
                          noteType={'SN'} value={this.state.SN} onChange={debounce(this.handleNoteChange,100)} onFocus={this.handleNoteFocus}
                          handleDragOver={this.handleDragOver} handleDrop={(e)=>this.handleDrop(e,'SN')}
                          xs={12} md={5} style={{flexBasis:'49%',maxWidth:'100%'}}
                      />
                    </Grid>
                  </Grid>
                </Grid>
                {/*<Bottombar {...bottombarProps}/>*/}
            </Container>
          {/*</Grid>*/}
          </Grid>
        </MuiThemeProvider>
    );
  }
}

const mapDispatchToProps = dispatch=>{return {dispatch};};

function mapStateToProps(state) {
  return {
    loginInfo: {
      ...state.login.loginInfo,
      service:state.login.service
    },
    mainFrame:state.mainFrame,
    encounter: state.patient.encounterInfo,
    patientInfo: state.patient.patientInfo,
    // services: state.clinicalNote.serviceListData,
    recordTypes:state.clinicalNote.recordTypeListData,
    medicalRecords:state.clinicalNote.medicalListData,
	  todayNotes:state.clinicalNote.todayClinicalNoteListData,
    subTabsActiveKey:state.mainFrame.subTabsActiveKey,
    amendmentHistoryList:state.clinicalNote.amendmentHistoryList  };
}


export default connect(mapStateToProps)(ClinicalNote);
