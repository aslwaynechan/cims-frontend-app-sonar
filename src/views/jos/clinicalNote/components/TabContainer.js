import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    Typography,
    Box
} from '@material-ui/core';

const styles = {
    content:{
        maxHeight: '30vh',
        overflowX: 'auto',
        paddingTop:5,
        paddingLeft:5
    }
};

function TabContainer(props) {
    const { classes,children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            {...other}
        >
            {value === index && <Box p={3} className={classes.content}>{children}</Box>}
        </Typography>
    );
}

export default withStyles(styles)(TabContainer);
