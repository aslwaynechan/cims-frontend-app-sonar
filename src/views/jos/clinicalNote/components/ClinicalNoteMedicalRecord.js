import React, { Component } from 'react';
import { styles } from '../clinicalNoteStyle';
import { withStyles } from '@material-ui/core/styles';
import {
  Grid,
  Table,
  TableRow,
  Typography,
  TableCell,
  TableHead,
  TableBody,
  FormControl,
  Fab,
  Tooltip
} from '@material-ui/core';
// import SelectFieldValidator from '../../../../components/FormValidator/SelectFieldValidator';
import moment from 'moment';
// import CIMSButton from '../../../../components/Buttons/CIMSButton';
import { getSimpleText } from '../../../../services/utils';
import { openCommonMessage, closeCommonMessage } from '../../../../store/actions/message/messageAction';
import { saveRecordDetail, deleteRecordDetail } from '../../../../store/actions/clinicalNote/clinicalNoteAction';
import { connect } from 'react-redux';
import { openCommonCircularDialog } from '../../../../store/actions/common/commonAction';
import { CLINICALNOTE_CODE } from '../../../../constants/message/clinicalNoteCode';
import { FileCopyOutlined, SaveOutlined, DeleteOutline } from '@material-ui/icons';
import { COMMON_STYLE }from '../../../../constants/commonStyleConstant';
import CustomizedSelectFieldValidator from '../../../../components/Select/CustomizedSelect/CustomizedSelectFieldValidator';
import {includes,toLower} from 'lodash';
// import { ROLE_TYPE } from '../../../../constants/clinicalNote/clinicalNoteConstants';
import Enum from '../../../../enums/enum';

class ClinicalNoteMedicalRecord extends Component {
  constructor(props) {
    super(props);
    this.focusFlag = false;
    this.state = {
      service: null,
      recordType: null,
      medicalListData: [],
      clinicalnoteId: '',//judge current select item should be highlight
      isShowRecordDetailText: 'none',
      isShowRecordDetailDiv: 'block',
      noteContent: '',
      isShow: 'none',
      isCopyShow: 'none',
      selectedClinicalNote: {}
    };
  }
  //init data
  componentDidMount() {
    this.props.onRef(this);
  }

  componentDidUpdate() {
    let { isShow } = this.state;
    if (isShow === 'flex' && !this.focusFlag) {
      this._textarea.focus();
      this.focusFlag = true;
    }
  }

  clinicNoteServiceValueOnChange = (e) => {
    let { updateClinicalNoteData } = this.props;
    let service = e.value;
    let params = { name: 'service', value: service };
    this.setState({
      service: service,
      isShow: 'none',
      isCopyShow: 'none'
    });
    updateClinicalNoteData && updateClinicalNoteData(params);
  }

  clinicNoteRecordTypeValueOnChange = (e) => {
    let { updateClinicalNoteData } = this.props;
    let recordType = e.value;
    let params = { name: 'recordType', value: recordType };
    this.setState({
      recordType: recordType,
      isShow: 'none',
      isCopyShow: 'none'
    });
    updateClinicalNoteData && updateClinicalNoteData(params);
  }

  changeMedicalRecord = medicalRecord => {
    let { userRoleName, curservice } = this.props;
    if (medicalRecord.serviceCd === curservice) {
      if (medicalRecord.codeClinicalnoteTypeCd === 'NNS' || medicalRecord.codeClinicalnoteTypeCd === 'NN') {
        if (includes(toLower(userRoleName),toLower(Enum.USER_ROLE_TYPE.NURSE))) {
          this.setState({
            isShowRecordDetailText: 'block',
            isShowRecordDetailDiv: 'none',
            isShow: 'flex',
            isCopyShow: 'flex'
          });
        }
        else {
          this.setState({
            isShowRecordDetailText: 'none',
            isShowRecordDetailDiv: 'block',
            isShow: 'none',
            isCopyShow: 'flex'
          });
        }
      }
      else if (medicalRecord.codeClinicalnoteTypeCd === 'DNS' || medicalRecord.codeClinicalnoteTypeCd === 'DN') {
        if (includes(toLower(userRoleName),toLower(Enum.USER_ROLE_TYPE.DOCTOR))) {
          this.setState({
            isShowRecordDetailText: 'block',
            isShowRecordDetailDiv: 'none',
            isShow: 'flex',
            isCopyShow: 'flex'
          });
        }
        else {
          this.setState({
            isShowRecordDetailText: 'none',
            isShowRecordDetailDiv: 'block',
            isShow: 'none',
            isCopyShow: 'flex'
          });
        }
      }
    }
    else {
      this.setState({
        isShowRecordDetailText: 'none',
        isShowRecordDetailDiv: 'block',
        isShow: 'none',
        isCopyShow: 'flex'
      });
    }
    this.setState({
      noteContent: medicalRecord.clinicalnoteText === null ? '' : medicalRecord.clinicalnoteText,
      clinicalnoteId: medicalRecord.clinicalnoteId,
      selectedClinicalNote: medicalRecord
    });
    this.focusFlag = false;
  };

  hideRecordDetailSave = () => {
    if (!this.state.blurFlag) {
      this.setState({
        isShow: 'none'
      });
    }
  }

  changeRecordDetail = (e) => {
    this.setState({
      noteContent: e.target.value
    });
  }

  editRecordDetail = () => {
    if (this.state.clinicalnoteId !== '') {
      this.setState({
        isShow: 'flex'
        // isShow:'block'
      });
    }
  }

  blurMiss = () => {
    this.setState({
      blurFlag: false
    });
  }

  blurShow = () => {
    this.setState({
      blurFlag: true
    });
  }

  dragMedicalRecord = medicalRecord => {
    let { updateState } = this.props;
    medicalRecord.clinicalnoteText =
      medicalRecord.clinicalnoteText && getSimpleText(medicalRecord.clinicalnoteText);
    updateState && updateState({
      dragMedicalNote: medicalRecord.clinicalnoteText,
      dragMedicalNoteType: true
    });
  };

  updateChildState = (obj) => {
    this.setState({
      ...obj
    });
  }

  getRecordDetailToNote = () => {
    let { stateMark, clinicalNotes, serviceNotes, updateState } = this.props;
    if (this.state.clinicalnoteId !== '') {
      if (stateMark !== '') {
        let recordTypeData = this.state.noteContent;
        let mark = stateMark;
        if (mark === '1') {
          let clinicalNote = clinicalNotes;
          if (clinicalNote.length > 0) {
            clinicalNote = clinicalNote + '\n';
          }
          updateState && updateState({
            clinicalNotes: clinicalNote + recordTypeData,
            clinicalTextScrollFlag: true
          });
        } else if (mark === '0') {
          let serviceNote = serviceNotes;
          if (serviceNote.length > 0) {
            serviceNote = serviceNote + '\n';
          }
          updateState && updateState({
            serviceNotes: serviceNote + recordTypeData,
            serviceTextScrollFlag: true
          });
        } else {
          return;
        }
      }
      else {
        let payload = {
          msgCode: CLINICALNOTE_CODE.COPY_FROM_RECODE_DETAIL
        };
        this.props.openCommonMessage(payload);
      }
    }

    else {
      let payload = {
        msgCode: CLINICALNOTE_CODE.COPY_FROM_NOTE_TEMPLATE
      };
      this.props.openCommonMessage(payload);
    }
  }

  recordDetailSave = () => {
    //get current selected clinicalnote
    let { clinicalNotesId, serviceNotesId, updateClinicalNoteData, updateState,curservice} = this.props;
    let clinicalNote = this.state.selectedClinicalNote;
    clinicalNote.clinicalnoteText = this.state.noteContent;
    clinicalNote.serviceCd=curservice;
    const params = clinicalNote;
    let callBackFlag = false;
    this.props.openCommonCircularDialog();
    this.props.saveRecordDetail({
      params,
      callback: (clinicalnote) => {
        if (clinicalnote.data === null) {
          let payload = {
            msgCode: clinicalnote.msgCode
          };
          this.props.openCommonMessage(payload);
        }
        else {
          callBackFlag = true;
          if (clinicalnote.data.clinicalnoteText === this.state.noteContent) {
            if (this.state.clinicalnoteId === clinicalNotesId) {
              updateState && updateState({
                isShow: 'none',
                clinicalNotes: clinicalnote.data.clinicalnoteText
              });
              // this.setState({isShow:'none'});
            }
            else if (this.state.clinicalnoteId === serviceNotesId) {
              updateState && updateState({
                isShow: 'none',
                serviceNotes: clinicalnote.data.clinicalnoteText
              });
              // this.setState({isShow:'none'});
            }
          }
          let params = { name: '', recordDetailSaveFlag: true };
          updateClinicalNoteData && updateClinicalNoteData(params);
          let payload = {
            msgCode: clinicalnote.msgCode,
            showSnackbar: true,
            btnActions: {
              // Yes
              btn1Click: () => {
                //update data
                // let params = { name: '', recordDetailSaveFlag: true };
                // updateClinicalNoteData && updateClinicalNoteData(params);
              }
            }
          };
          this.props.openCommonMessage(payload);
          this.setState({
            selectedClinicalNote: clinicalnote.data
          });

        }
      }
    });
    updateState && updateState({
      isShow: 'none'
    });
    this.setState({ isShow: 'flex' });
    // this.setState({isShow:'none'});

    if (!callBackFlag) {
      let params = { name: '', recordDetailSaveFlag: true };
      updateClinicalNoteData && updateClinicalNoteData(params);
    }
  }

  recordDetailDelete = () => {
    //Delete
    let { updateClinicalNoteData, updateState } = this.props;
    let payload = {
      msgCode: CLINICALNOTE_CODE.DELETE_NOTE,
      // Yes
      btnActions: {
        btn1Click: () => {
          //get current selected clinicalnote
          let clinicalNote = this.state.selectedClinicalNote;
          clinicalNote.clinicalnoteText = this.state.noteContent;
          const params = clinicalNote;
          this.props.openCommonCircularDialog();
          this.props.deleteRecordDetail({
            params,
            callback: (msgCode) => {
              this.setState({
                isShow: 'none',
                noteContent: ''
              });
              if (msgCode === CLINICALNOTE_CODE.DELETE_NOTE_SUCESSFULLY) {
                //update data
                let params = { name: '', recordDetailSaveFlag: false };
                updateClinicalNoteData && updateClinicalNoteData(params);
              }
              let payload = {
                msgCode: msgCode,
                showSnackbar:true,
                // Yes
                btnActions: {
                  btn1Click: () => {
                    // if (msgCode === CLINICALNOTE_CODE.DELETE_NOTE_SUCESSFULLY) {
                    //   //update data
                    //   let params = { name: '', recordDetailSaveFlag: false };
                    //   updateClinicalNoteData && updateClinicalNoteData(params);
                    // }
                    // this.setState({
                    //   noteContent: ''
                    // });
                  }
                }
              };
              this.props.openCommonMessage(payload);
            }
          });
        }
      }
    };
    this.props.openCommonMessage(payload);
    updateState && updateState({
      isShow: 'none'
    });
  }

  onDragOver = (e) => {
    e.preventDefault();
  }

  render() {
    const { classes, serviceListData, recordTypeListData, medicalListData, curservice } = this.props;
    return (
      <Grid
          className={classes.left_warp}
          item
          style={{ marginBottom: 10 }}
          xs={3}
          classes={{
          'grid-xs-3': classes.record_module
        }}
      >
        <Typography
            className={classes.title}
            component="div"
        >
          Medical Record
      </Typography>
        <Typography
            className={classes.table}
            component="div"
            style={{ minHeight: 348, height: 'calc(100vh - 750px)' }}
        >
          <Typography
              className={classes.table_header}
              component="div"
              padding={'none'}
              style={{ paddingLeft: '15px', marginTop: '5px' }}
          >

            <FormControl className={classes.formControl}>
              <Grid
                  container
                  // spacing={2}
              >
                <Grid item
                    style={{ padding: 0, paddingLeft: '3px', paddingTop: '10px' }}
                    xs={4}
                >
                  <Typography className={classes.list_title}
                      variant="subtitle1"
                  >Service:</Typography>

                </Grid>
                <Grid item
                    style={{ padding: 0, paddingTop: '6px' }}
                    xs={8}
                >
                  <CustomizedSelectFieldValidator className={classes.select_user}
                      defaultValue={curservice}
                      id={'ServiceTypeSelectField'}
                      msgPosition="bottom"
                      onChange={this.clinicNoteServiceValueOnChange}
                      options={serviceListData.map((item) => ({ value: item.code, label: item.code }))}
                      value={this.state.service}
                    //   classes={{
                    //     singleValue:classes.fontLabel,
                    //     menuItem:classes.fontLabel
                    // }}
                  />
                </Grid>
                <Grid item
                    style={{ padding: 0, paddingLeft: '3px', paddingTop: '4px' }}
                    xs={4}
                >
                  <Typography className={classes.list_title}
                      variant="subtitle1"
                  >Record&nbsp;Type:</Typography>
                </Grid>
                <Grid item
                    style={{ padding: 0 }}
                    xs={8}
                >
                  <CustomizedSelectFieldValidator
                      className={classes.select_user}
                    //   classes={{
                    //   paper: classes.custom_paper
                    // }}
                      id={'RecordTypeSelectField'}
                      msgPosition="bottom"
                      onChange={this.clinicNoteRecordTypeValueOnChange}
                      options={recordTypeListData.map((item) => ({ value: item.codeClinicalnoteTypeCd, label: item.typeDesc }))}
                      value={this.state.recordType}
                  />
                </Grid>
              </Grid>
            </FormControl>

          </Typography>
          <Table style={{marginTop:14}}>
            <TableHead>
              <TableRow style={{backgroundColor:COMMON_STYLE.TABLE_BGCKGROUNDCOLOR}} className={classes.table_head}  >
                <TableCell
                    className={classes.table_header}
                    padding={'none'}
                    style={{ paddingLeft: '2px', fontStyle: 'normal', fontSize: '1rem', fontWeight: 'bold', width: '30%' }}
                >
                  Enc Date
              </TableCell>
                <TableCell className={classes.table_header}
                    padding={'none'}
                    style={{ paddingLeft: '2px', fontStyle: 'normal', fontSize: '1rem', fontWeight: 'bold', width: '20%' }}
                >
                  Service
              </TableCell>
                <TableCell className={classes.table_header}
                    padding={'none'}
                    style={{ paddingLeft: '2px', fontStyle: 'normal', fontSize: '1rem', fontWeight: 'bold' }}
                >
                  Record&nbsp;Type
              </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {medicalListData.length>0?medicalListData.map((item, index) => (
                <TableRow
                    className={
                    item.clinicalnoteId ===
                      this.state.clinicalnoteId
                      ? classes.table_row_selected
                      : classes.table_row
                  }
                    draggable="true"
                    key={index}
                    onClick={() => this.changeMedicalRecord(item)}
                    onDragStart={() => this.dragMedicalRecord(item)}
                >
                  <TableCell padding={'none'}
                      style={{ fontSize: '1rem',paddingLeft: '2px', width: 100 }}
                  >
                    {moment(item.createdDtm).format('DD MMM YYYY')}
                  </TableCell>
                  <TableCell padding={'none'}
                      style={{ fontSize: '1rem',paddingLeft: '2px' }}
                  >
                    {(item.serviceCd)}
                  </TableCell>
                  <TableCell padding={'none'}
                      style={{ fontSize: '1rem', paddingLeft: '2px', width: 150 }}
                  >{item.clinicalnoteTypeDesc}</TableCell>
                </TableRow>
              )):
              <TableRow >
                    <TableCell className={classes.fontLabel} colSpan={3} align="center">
                   There is no data!
                  </TableCell>
              </TableRow>
              }
            </TableBody>
          </Table>
        </Typography>
        <Typography className={classes.title}
            component="div"
        >
          Record Detail
      </Typography>
        <Typography
            className={classes.table}
            component="div"
            id="textRecord"
            style={{
            height: 'calc(100vh - 722px)',
            minHeight: '260px',
            color: '#3f51b5',
            position: 'relative',
            fontFamily: 'arial'
          }}
        >
          <Typography
              component="div"
              style={{height: '70%'}}
          >
          <Typography
              component="div"
              style={{
              margin: 10,
              padding: 5,
              //height: 'calc(48vh - 326px)',
              height: '90%',
              width: '90%',
              border: 'none',
              resize: 'none',
              userSelect: 'none',
              whiteSpace: 'pre-wrap',
              wordBreak: 'break-all',
              display: this.state.isShowRecordDetailDiv,
              overflow: 'auto',
              fontFamily: 'arial',
              fontSize:'1rem'
            }}
          >
            {this.state.noteContent}
          </Typography>
          <textarea
              ref={c => (this._textarea = c)}
              id="medical_record_textarea"
              component="textarea"
            // onBlur={this.hideRecordDetailSave}
              onChange={this.changeRecordDetail}
            // onClick={this.editRecordDetail}
            // onFocus={this.editRecordDetail}
              onDragOver={this.onDragOver}
              style={{
              color: '#404040',
              // fontSize:'0.875rem',
              margin: 10,
              padding: 5,
              //height: 'calc(48vh - 326px)',
              height: '90%',
              width: '90%',
              border: 'none',
              resize: 'none',
              whiteSpace: 'pre-wrap',
              wordBreak: 'break-all',
              display: this.state.isShowRecordDetailText,
              fontFamily: 'arial',
              fontSize:'1rem'
            }}
              value={this.state.noteContent}
          />
          </Typography>

          <Typography
              component="div"
          >
            <Typography
                component="div"
            >
              <Typography
                  component="div"
                  style={{ float: 'right', marginRight: '10px'}}
              >
                <Tooltip title="Copy" aria-label="Copy">
                  <Fab
                      className={classes.record_detail_fab}
                      color="primary"
                      id={'RecordDetailCopyButton'}
                      onClick={this.getRecordDetailToNote}
                      style={{ display: this.state.isCopyShow }}
                  >
                    <FileCopyOutlined className={classes.record_detail_fab_icon} />
                  </Fab>
                </Tooltip>
              </Typography>
            </Typography>
            <Typography
                component="div"
                onMouseOut={this.blurMiss}
                onMouseOver={this.blurShow}
                style={{ float: 'right' }}
            >
              <Tooltip title="Save" aria-label="Save">
                <Fab
                    className={classes.record_detail_fab}
                    color="primary"
                    id={'RecordDetailSaveButton'}
                    onClick={this.recordDetailSave}
                    style={{ display: this.state.isShow }}
                >
                  <SaveOutlined className={classes.record_detail_fab_icon} />
                </Fab>
              </Tooltip>
            </Typography>
            <Typography
                component="div"
                onMouseOut={this.blurMiss}
                onMouseOver={this.blurShow}
                style={{ float: 'right' }}
            >
              <Tooltip title="Delete" aria-label="Delete">
                <Fab
                    className={classes.record_detail_fab_delete}
                    color="secondary"
                    id={'RecordDetailDeleteButton'}
                    onClick={this.recordDetailDelete}
                    style={{ display: this.state.isShow }}
                >
                  <DeleteOutline className={classes.record_detail_fab_icon} />
                </Fab>
              </Tooltip>
            </Typography>
          </Typography>

        </Typography>
      </Grid>
    );
  }
}

const mapDispatchToProps = {
  openCommonMessage,
  closeCommonMessage,
  saveRecordDetail,
  deleteRecordDetail,
  openCommonCircularDialog
};
function mapStateToProps() {
  return {
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ClinicalNoteMedicalRecord));