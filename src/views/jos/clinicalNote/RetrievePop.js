import React,{Component} from 'react';
import { Popper,ClickAwayListener,Paper,Grid,Checkbox,List,ListItem,ListItemText,Collapse,ListItemIcon,Typography} from '@material-ui/core';
import {styles} from './clinicalNoteStyle';
import { withStyles } from '@material-ui/core/styles';
import CIMSButton from '../../../components/Buttons/CIMSButton';
import {ExpandLess,ExpandMore } from '@material-ui/icons';

class RetrievePop extends Component {
  state={
    nOpen:true,
    snOpen:true,
    checkBoxList:[
      {label:'Current Assessment',value:'Current Assessment',checked:false,open:false,textName:'assessmentTextList'},
      {label:'Chronic Problem',value:'Chronic Problem',checked:false,open:false,textName:'ChronicProblemTextList'},
      {label:'Previous Dx',value:'Previous Dx',checked:false,open:false,textName:'previousDxTextList'},
      {label:'Previous IOE',value:'Previous IOE',checked:false,open:false,textName:'ioe'},
      {label:'Previous MOE',value:'Previous MOE',checked:false,open:false,textName:'moe'}
    ],
    copyType:'CA,CP,DX,IOE,MOE',
    detailOpen:false,
    testList:[
      {label:'Current Assessment',value:'Height 180 cm',checked:false,open:false},
      {label:'Chronic Problem',value:'Weight 70 kg',checked:false,open:false},
      {label:'Previous Dx',value:'BMI 21.6 kg/m^2',checked:false,open:false},
      {label:'Previous IOE',value:'Pre-Pregnancy weight gain (BW) 67 kg Pre-pregnancy BMI: 20.7 kg/m(2)',checked:false,open:false}
    ],
    checkBoxTextList:
    [
      [],
      [],
      [],
      [],
      []
    ]
  }

  toggleList=(type)=>{
    this.setState({[type]:!this.state[type]});
  }

  handleCopy=()=>{
    const {onCopy,textList} = this.props;
    let assessmentSelected = this.state.checkBoxTextList[0];
    let chronicProblemSelected = this.state.checkBoxTextList[1];
    let previousDxSelected = this.state.checkBoxTextList[2];
    let previousIOESelected = this.state.checkBoxTextList[3];
    let previousMOESelected = this.state.checkBoxTextList[4];
    let arr=[];

    textList[0].map((item,index)=>{
      if(assessmentSelected.indexOf(index)>-1){
        arr.push(item);
      }
    });
    textList[1].map((item,index)=>{
      if(chronicProblemSelected.indexOf(index)>-1){
        arr.push(item);
      }
    });
    textList[2].map((item,index)=>{
      if(previousDxSelected.indexOf(index)>-1){
        arr.push(item);
      }
    });
    textList[3].map((item,index)=>{
      if(previousIOESelected.indexOf(index)>-1){
        arr.push(item);
      }
    });
    textList[4].map((item,index)=>{
      if(previousMOESelected.indexOf(index)>-1){
        arr.push(item);
      }
    });
    let checkBoxTextList = this.state.checkBoxTextList;
    checkBoxTextList = [[],[],[],[],[],[]];
    onCopy&&onCopy(arr);
    this.setState({checkBoxTextList:checkBoxTextList});
  }

  handleAllCopy=()=>{
    const {onCopy}=this.props;
    let {checkBoxList,copyType} = this.state;
    for(let item of checkBoxList){
      item['checked']= true;
    }
    // copyType = 'CA,CP,DX,IOE,MOE';
    this.setState({checkBoxList:checkBoxList,copyType:copyType});
    onCopy&&onCopy(copyType);
  }

  handleDetailItem=(item)=>{
    const {onItemCopy}=this.props;
    onItemCopy&&onItemCopy(item);
  }

  handleCheckBoxChange=(event,index)=>{
    let {checkBoxList} = this.state;
    let items = [...this.state.checkBoxList];
    items[index].checked =event.target.checked;
    this.checkBoxSelectAllClick(event.target.checked,index);
    let typeList = (checkBoxList[0].checked?'CA,':'')+(checkBoxList[1].checked?'CP,':'')+(checkBoxList[2].checked?'DX,':'')+(checkBoxList[3].checked?'IOE,':'')+(checkBoxList[4].checked?'MOE':'');
    this.setState({checkBoxList:items,copyType:typeList});
  }

  checkBoxSelectAllClick=(checked,index)=>{
    // let type=index===0?'CA':(index===1?'CP':(index===2?'DX':(index===3?'IOE':(index===4?'IOE':'MOE'))));
    let {textList} = this.props;
    let {checkBoxTextList} = this.state;
    // let type = index===0?'assessmentCheckBoxSelected':'';
    // if(type==='assessmentCheckBoxSelected'){
      if(checked){
        checkBoxTextList[index] = textList[index].map((item,index)=>index);
        this.setState({checkBoxTextList:checkBoxTextList});
      }else{
        checkBoxTextList[index] = [];
        this.setState({checkBoxTextList:checkBoxTextList});
      }
    // }
  }

  handleDetailOpen=(index)=>{
    let items = [...this.state.checkBoxList];
    items[index].open =!items[index].open;
    this.setState({checkBoxList:items});
  }

  handleClick = (event, itemId,id) => {
    const { checkBoxTextList } = this.state;
    const selectedIndex = checkBoxTextList[itemId].indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(checkBoxTextList[itemId], id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(checkBoxTextList[itemId].slice(1));
    } else if (selectedIndex === checkBoxTextList[itemId].length - 1) {
      newSelected = newSelected.concat(checkBoxTextList[itemId].slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        checkBoxTextList[itemId].slice(0, selectedIndex),
        checkBoxTextList[itemId].slice(selectedIndex + 1)
      );
    }
    checkBoxTextList[itemId] = newSelected;
    this.setState({ checkBoxTextList: checkBoxTextList });
    // this.props.getSelectRow(newSelected);
  };

  assessmentCheckBoxIsSelected = (index,rowId) => this.state.checkBoxTextList[index].indexOf(rowId) !== -1;

  render(){
    const {toggleRetrieve,targetEl,open,classes,textList}=this.props;
    const {checkBoxTextList}=this.state;

    return (
      <Popper open={open} anchorEl={targetEl.current} placement={'bottom-end'} style={{zIndex:100,width:'280px'}}>
        <Paper>
          <ClickAwayListener onClickAway={toggleRetrieve}>
          <List style={{maxHeight:'50vh',overflow:'auto'}}>
            {
              this.state.checkBoxList.map((item,index)=>{
                return (
                  <div  key={index}>
                    <ListItem button key={index} className={classes.listItem}>
                      <Checkbox color="primary" checked={textList[index].length>0?textList[index].length===checkBoxTextList[index].length:item.checked} onChange={(e)=>this.handleCheckBoxChange(e,index)}
                          inputProps={{
                          style:{
                              width:30
                          }
                        }}
                          classes={{
                            root:classes.checkbox_sty
                        }}
                      />
                      <ListItemText
                        //   classes={{
                        // root:classes.listItemText}}
                          primary={
                            <Typography className={classes.fontLabel} noWrap>
                              {item.label}
                            </Typography>
                          }
                          onClick={()=>this.handleDetailOpen(index)}
                      />
                      <ListItemIcon className={classes.listItemIcon} onClick={()=>this.handleDetailOpen(index)}>
                        {textList[index].length>0?(item.open? <ExpandLess /> : <ExpandMore />):null}
                      </ListItemIcon>
                    </ListItem>
                    {textList[index].length!==0?
                    <Collapse in={item.open} timeout="auto" unmountOnExit style={{marginLeft:35,color: '#0579c8'}}>
                      <List component="div" disablePadding>
                        {/* <ListItem button className={classes.listSubheader} onClick={()=>this.handleCopy(copyType)}>Copy All</ListItem> */}
                        {
                          textList[index].map((item,key)=>{
                            const isSelected = this.assessmentCheckBoxIsSelected(index,key);
                            return (
                              // <ListItem  key={index} button className={classes.listItem} onClick={()=>this.handleDetailItem(item)}>
                              <ListItem  key={key} button alignItems="flex-start" className={classes.listItem} >
                                <Checkbox checked={isSelected}
                                    color="primary"
                                    id={'assessmentCheckBox_'+key}
                                    onClick={event=>this.handleClick(event,index,key)}
                                />
                                <ListItemText
                                    primary={
                                        <Typography style={{marginTop:5}} className={classes.fontLabel} >
                                             {item}
                                        </Typography>
                                    }
                                />
                                {/* {item} */}
                              </ListItem>
                            );
                          })
                        }
                      </List>
                    </Collapse>
                    :null}
                  </div>
                );
              })
            }

          </List>
          </ClickAwayListener>
          <Grid alignItems="center"
              container
              justify="flex-end"
          >
                <CIMSButton
                    classes={{
                      label:classes.fontLabel
                  }}
                    color="primary"
                    id="btn_retrieve_copy"
                    // onClick={() =>this.previousReport()}
                    size="small"
                    onClick={()=>this.handleCopy()}
                >
                  Copy
                </CIMSButton>
                {/* <CIMSButton
                    classes={{
                      label:classes.fontLabel
                  }}
                    color="primary"
                    id="btn_retrieve_copy_all"
                    // onClick={() =>this.previousReport()}
                    size="small"
                    onClick={()=>this.handleAllCopy()}
                >
                  Copy All
                </CIMSButton> */}
              </Grid>
        </Paper>
      </Popper>
    );
  }
}

export default (withStyles)(styles)(RetrievePop);
