import React from 'react';
import { Grid,Button,ButtonGroup } from '@material-ui/core';
import {ArrowDropDown} from '@material-ui/icons';
import TemplatePop from './TemplatePop';
import moment from 'moment';
import RetrievePop from './RetrievePop';
const style={
  height:'33px',
  lineHeight:'33px',
  flex:'none',
  borderBottom:'1px solid #e6e6e6',
  padding:'0 12px',
  position:'relative',
  minWidth:1010
};

const noteTypeMap={
  D:'Doctor',
  N:'Nurse',
  C:'Counter',
  CP:'CP',
  DI:'Dietitian',
  AH:'AH'
};

const tools=['°C','°F','↑','↓','√','×','±'];

const Topbar=({onItemCopy,onCopy,onClick,templates,toggleTemplate,templateOpen,noteInfo={},retrieve,retrieveOpen,toggleRetrieve,loginInfo,assessmentTextList,chronicProblemTextList,previousDxTextList,previousIOETextList,previousMOETextList})=>{
  const textList = [
  assessmentTextList,
  chronicProblemTextList,
  previousDxTextList,
  previousIOETextList,
  previousMOETextList
  ];

  const anchorRef = React.useRef(null);
  return(
    <Grid item container xs={12} style={style}>
      <Grid item xs={12} md={6}>{noteInfo!==null?`${noteTypeMap[loginInfo.userRoleType]} Note (${loginInfo.loginName} / ${moment(noteInfo.createdDtm).format('DD-MMM-YYYY HH:mm')})`:`${noteTypeMap[loginInfo.userRoleType]} Note (${loginInfo.loginName})`}</Grid>
      <Grid item container justify="flex-end" xs={12} md={6} >
          <ButtonGroup size="small">
            {
              tools.map(item=>{
                return <Button key={item} onClick={()=>{onClick(item);}}>{item}</Button>;
              })
            }
            <Button style={{textTransform:'none'}} onClick={toggleRetrieve} ref={anchorRef}>{retrieve} <ArrowDropDown /></Button>
            <Button style={{textTransform:'none'}} onClick={toggleTemplate} ref={anchorRef}>Template <ArrowDropDown /></Button>
          </ButtonGroup>
        <RetrievePop onItemCopy={onItemCopy} onCopy={onCopy} onClick={onClick} templates={templates} targetEl={anchorRef} open={retrieveOpen} toggleRetrieve={toggleRetrieve} textList={textList}/>
        <TemplatePop onClick={onClick} templates={templates} targetEl={anchorRef} open={templateOpen} toggleTemplate={toggleTemplate}/>
      </Grid>
    </Grid>
  );
};

export default Topbar;
