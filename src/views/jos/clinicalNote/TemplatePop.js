import React,{Component} from 'react';
import { Popper,ClickAwayListener,Paper,Tooltip,Collapse,List,ListItem,ListItemText} from '@material-ui/core';
import {ExpandLess,ExpandMore} from '@material-ui/icons';

class TemplatePop extends Component {
  state={
    nOpen:true,
    snOpen:true
  }

  toggleList=(type)=>{
    this.setState({[type]:!this.state[type]});
  }
  render(){
    const {templates={},toggleTemplate,targetEl,open,onClick}=this.props;
    const {N=[],SN=[]}=templates;
    return (
      <Popper open={open} anchorEl={targetEl.current} placement={'bottom-end'} style={{zIndex:100,width:'260px'}}>
        <Paper>
          <ClickAwayListener onClickAway={toggleTemplate}>
          <List style={{maxHeight:'50vh',overflow:'auto'}}>
            <ListItem button onClick={()=>{this.toggleList('nOpen');}}>
              <ListItemText primary="My Favourite" />
              {this.state.nOpen ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={this.state.nOpen} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                {
                  N.map((item,index)=>{
                    return (
                      <ListItem button style={{paddingLeft:'2em'}} onClick={()=>{onClick('\n'+item.templateText);}} eventvalue={item.templateText} key={index}>
                        <Tooltip title={item.templateName} enterDelay={700}>
                          <ListItemText primary={item.templateName}  primaryTypographyProps={{noWrap:true,style:{display:'inline-block',width:'100%'}}} />
                        </Tooltip>
                      </ListItem>
                    );
                  })
                }
              </List>
            </Collapse>
            <ListItem button onClick={()=>{this.toggleList('snOpen');}}>
              <ListItemText primary="Service Favourite" />
              {this.state.snOpen ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={this.state.snOpen} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                {
                  SN.map((item,index)=>{
                    return (
                      <ListItem button style={{paddingLeft:'2em'}} onClick={()=>{onClick('\n'+item.templateText);}} eventvalue={item.templateText} key={index}>
                        <Tooltip title={item.templateName} enterDelay={700}>
                          <ListItemText primary={item.templateName} primaryTypographyProps={{noWrap:true,style:{display:'inline-block',width:'100%'}}} />
                        </Tooltip>
                      </ListItem>
                    );
                  })
                }
              </List>
            </Collapse>

          </List>
          </ClickAwayListener>
        </Paper>
      </Popper>
    );
  }
}

export default TemplatePop;
