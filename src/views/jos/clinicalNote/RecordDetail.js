import React,{Component} from 'react';
import Box from './components/Box';
import { TextField,BottomNavigation,BottomNavigationAction } from '@material-ui/core';
import {DeleteOutlined,SaveOutlined,FileCopyOutlined} from '@material-ui/icons';

class RecordDetail extends Component{
  constructor(props) {
    super(props);
    const {note={}}=this.props;
    this.state={
      id:note.id,
      content:note.content||'',
      readOnly:!!note.readOnly
    };
  }

  componentWillReceiveProps(props){
    const {note={}}=props;
    if(JSON.stringify(note)!==JSON.stringify(this.props.note)){
      this.setState({
        id:note.id,
        content:note.content||'',
        readOnly:!!note.readOnly
      });
    }
  }

  handleChange=(e)=>{
    const {onValueChange}=this.props;
    this.setState({
      content:e.target.value
    });
    onValueChange&&onValueChange(true);
  }

  handleDelete=(e)=>{
    const {onDelete}=this.props;
    onDelete&&onDelete();
  }
  handleSave=(e)=>{
    const {onSave}=this.props;
    onSave&&onSave(this.state.content);
  }
  handleCopy=(e)=>{
    const {onCopy}=this.props;
    onCopy&&onCopy('\n'+this.state.content);
  }
  render(){
    const {height}=this.props;
    return (
      <Box title={'Record Details'} height={height}>
        <TextField  multiline rows={1} value={this.state.content} disabled={this.state.id==undefined||this.state.readOnly} style={{width:'100%',height:'calc(100% - 34px)',padding:'6px',boxSizing:'border-box'}} variant="outlined"  onChange={this.handleChange}/>
        <BottomNavigation showLabels>
          <BottomNavigationAction label="Delete" disabled={this.state.id==undefined||this.state.readOnly} icon={<DeleteOutlined />} onClick={this.handleDelete}/>
          <BottomNavigationAction label="Save" disabled={this.state.id==undefined||this.state.readOnly} icon={<SaveOutlined />} onClick={this.handleSave}/>
          <BottomNavigationAction label="Copy" disabled={this.state.id==undefined} icon={<FileCopyOutlined />} onClick={this.handleCopy}/>
        </BottomNavigation>
      </Box>
    );
  }
}

export default RecordDetail;
