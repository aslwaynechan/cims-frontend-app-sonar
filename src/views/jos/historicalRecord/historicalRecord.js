import React, { Component } from 'react';
import { styles } from './historicalRecordCss';
import { withStyles } from '@material-ui/core/styles';
import {
    Grid,
    Typography,
    FormControl,
    Select,
    MenuItem,
    TextField,
    Avatar,
    IconButton,
    Checkbox,
    InputLabel
} from '@material-ui/core';
import moment from 'moment';
import CIMSButton from '../../../components/Buttons/CIMSButton';
import {
    openCommonMessage,
    closeCommonMessage
} from '../../../store/actions/message/messageAction';
import {
    saveRecordDetail,
    deleteRecordDetail
} from '../../../store/actions/clinicalNote/clinicalNoteAction';
import { connect } from 'react-redux';
import {
    openCommonCircularDialog,
    closeCommonCircularDialog
} from '../../../store/actions/common/commonAction';

import { MTableAction,MTableBodyRow,Paper } from 'material-table';
import {
    PROCEDURE_MODE,
    PROCEDURE_SEARCH_PROBLEM,
    PROCEDURE_SEARCH_LIST_TYPE,
    DEFAULT_PROCEDURE_SAVE_STATUS
} from '../../../constants/procedure/procedureConstants';
import FuzzySearchBox from '../../../components/Search/FuzzySearchBox';
import {
    PROBLEM_SEARCH_PROBLEM,
    PROBLEM_MODE,
    PROBLEM_SEARCH_LIST_TYPE,
    DEFAULT_PROBLEM_SAVE_STATUS,
    CHRONIC_SAVE_IND_TYPE,
    TABLE_CONTENT_TYPE
} from '../../../constants/diagnosis/diagnosisConstants';
// import { codeList } from '../../../constants/codeList';
import { find, isUndefined, toInteger, findIndex } from 'lodash';
import ServiceFavouriteDialog from '../serviceFavourite/ServiceFavouriteDialog';
import {
    getInputProcedureList,
    getProcedureCodeDiagnosisStatusList,
    updatePatientProcedure,
    deletePatientProcedure,
    searchProcedureList,
    queryProcedureList,
    saveInputProcedure,
    requestProcedureTemplateList
} from '../../../store/actions/procedure/procedureAction';
import {
    getInputProblemList,
    getProblemCodeDiagnosisStatusList,
    updatePatientProblem,
    deletePatientProblem,
    listCodeDiagnosisTypes,
    getHistoricalRecords,
    saveInputProblem,
    queryProblemList,
    requestProblemTemplateList,
    savePatient,
    getChronicProblemList
} from '../../../store/actions/diagnosis/diagnosisAction';
import {
    COMMON_ACTION_TYPE,
    COMMON_SYS_CONFIG_KEY,
    DEFAULT_OFFLINE_PAGE_SIZE,
    COMMON_CHECKBOX_TYPE
} from '../../../constants/common/commonConstants';
import axios from '../../../services/axiosInstance';
import { COMMON_CODE } from '../../../constants/message/common/commonCode';
import { Clear, Check, DeleteOutline, EditRounded } from '@material-ui/icons';
import { COMMON_STYLE } from '../../../constants/commonStyleConstant';
import Enum from '../../../enums/enum';
import { PROCEDURE_CODE } from '../../../constants/message/procedureCode';
import accessRightEnum from '../../../enums/accessRightEnum';
import { deleteSubTabs } from '../../../store/actions/mainFrame/mainFrameAction';
import SelectComponent from '../../../components/JSelect';
import { BottomNavigation,BottomNavigationAction,ButtonGroup } from '@material-ui/core';
import {DeleteOutlined,SaveOutlined,FileCopyOutlined,ArrowBackIos,ArrowForwardIos} from '@material-ui/icons';
import Container from 'components/JContainer';
import _ from 'lodash';
import JTable from '../../../components/JTable';

// const theme = createMuiTheme({
//     overrides: {
//         MuiTableCell: {
//             body: {
//                 fontSize: '1rem',
//                 fontFamily: 'Arial'
//             },
//             root: {
//                 padding: '1px 16px'
//                 // paddingBottom: 1,
//                 // paddingTop: 1
//             }
//         },
//         MuiTableSortLabel: {
//             root: {
//                 fontSize: '1rem'
//             }
//         },
//         MuiInputBase: {
//             inputSelect: {
//                 fontSize: '1rem',
//                 fontFamily: 'Arial'
//             },
//             input: {
//                 fontSize: '1rem',
//                 fontFamily: 'Arial'
//             }
//         },
//         MuiMenuItem: {
//             root: {
//                 fontFamily: 'Arial'
//             }
//         },
//         MuiIconButton: {
//             root: {
//                 paddingBottom: 4,
//                 paddingTop: 4
//             }
//         },
//         MuiCheckbox: {
//             colorPrimary: {
//                 padding: 0,
//                 '&$checked': {
//                     color: '#0579C8'
//                 },
//                 '&$disabled': {
//                     color: '#B8BCB9'
//                 }
//             }
//         }
//     }
// });

const List=({data,wrapperContentHeight,onSelectionChange,components})=>{
    const columns=[
      {title:'Visit Date',field:'createdDtm',  headerStyle: {width: '24%',textAlign:'center'},cellStyle:{padding:4},render:record=>{
        return <Grid container style={{minWidth:125}}>
                    <Avatar
                        style={{
                            fontSize:'13px',
                            fontFamily:'Arial',
                            textAlign:'center',
                            width:'20px',
                            height:'20px',
                            color: record.recordType ==='PX'?'###F8D186':'#fff',
                            backgroundColor: record.recordType ==='PX'?'#F8D186':'#38d1ff'
                        }}
                    >
                        {record.recordType ==='PX'?'Px':'Dx'}
                    </Avatar>
                    <span
                        style={{
                            fontSize:
                                '1rem'
                        }}
                    >
                        &nbsp;
                        {moment(
                            record.createdDtm
                        ).format(
                            'DD-MMM-YYYY'
                        )}
                    </span>
               </Grid>;
      }},
      {title:'Dx/Px',field:'diagnosisText',headerStyle: {width: 'auto',textAlign:'center'},cellStyle:{padding:4},render:record=>{
      return <span style={{fontSize:'1rem'}}>{record.diagnosisText}</span>;}},
      {title:'Status',field:'diagnosisStatusCd',headerStyle: {width: '9%',textAlign:'center'},cellStyle:{padding:4},render:record=>{
          return <Avatar
              title={record.diagnosisStatusDesc}
              style={{
                    fontSize:'13px',
                    fontFamily:'Arial',
                    textAlign:'center',
                    width:'20px',
                    height:'20px',
                    color: record.recordType ==='PX'?'###F8D186':'#fff',
                    backgroundColor: record.recordType ==='PX'?'#F8D186':'#38d1ff',
                    marginLeft:'30%'
                    }}
                 >
                        {record.diagnosisStatusCd}
                 </Avatar>;
      }}
    ];
    let dropdownElmHeight = 0;
    if (document.getElementById('historyForm')) {
        dropdownElmHeight = document.getElementById('historyForm').clientHeight;
    }

    const options={
        maxBodyHeight:wrapperContentHeight-dropdownElmHeight||'undefined',
        // maxBodyHeight:wrapperContentHeight-58||'undefined',
        draggable: false,
        headerStyle:{ color: '#ffffff',backgroundColor:COMMON_STYLE.TABLE_BGCKGROUNDCOLOR,fontWeight:600,fontSize:'1rem',fontFamily:'Arial'}
    };
    return (
      <JTable id="dxpx_history_table" components={components} columns={columns} data={data} options={options} onSelectionChange={onSelectionChange} size="small"/>
    );
  };

const Boxer=({title,height,children,contentRef})=>{
  return (
    <div style={{width:'100%',height:height||'auto'}}>
      <div style={{lineHeight:'32px',padding:'0 12px'}}>{title}</div>
      <div ref={contentRef} style={{backgroundColor:'#FFF',border:'1px solid rgba(0,0,0,.5)',height:'calc(100% - 38px)',overflow:'hidden'}}>{children}</div>
    </div>
  );
};

const Box=React.forwardRef((props,ref)=>{
    return <Boxer {...props} contentRef={ref} />;
});

class HistoricalRecord extends Component {
    constructor(props) {
        super(props);
        this.boxContent=React.createRef();
        this.historyContent=React.createRef();
        this.dragTargetContentType = '';
        this.state = {
            service: null,
            recordType: '',
            medicalListData: [],
            diagnosisId: '', //judge current select item should be highlight
            isShowRecordDetailText: 'none',
            isShowRecordDetailDiv: 'none',
            noteContent: '',
            isShow: 'none',
            selectedHistoricalRecord: {},
            dragHistoricalRecord: {},
            statusList: [],
            problemStatusList: [],
            procedureStatusList: [],
            problemDataList: [],
            problemDeleteDataList: [],
            procedureDataList: [],
            procedureDeleteDataList: [],
            recordTypeListData: [],
            chronicProblemDataList:[],
            chronicProblemDeletedDataList:[],
            isNew: null, // diagnosis histroy detail
            status: '',
            statusValue: '',
            matchNum: 0,
            searchProblemRecordList: [],
            historicalRecordProcedureScrollFlag: false,
            historicalRecordProblemScrollFlag: false,
            problemEditFlag: false,
            procedureEditFlag: false,
            procedureColumns: [
                {
                    title: 'Procedure',
                    field: 'procedureText',
                    editable: 'never',
                    headerStyle: {
                        zIndex: '0',
                        width: '41%'
                    },
                    cellStyle: {
                        fontSize: '1rem'
                    }
                },
                {
                    title: 'Status',
                    field: 'diagnosisStatusCd',
                    lookup: {},
                    headerStyle: {
                        zIndex: '0',
                        width: '10%',
                        minWidth: 90
                    },
                    cellStyle: {
                        fontSize: '1rem'
                    }
                },
                {
                    title: 'Remark',
                    field: 'remarks',
                    headerStyle: {
                        zIndex: '0',
                        width: 'auto'
                    },
                    cellStyle: {
                        fontSize: '1rem'
                    },
                    render: rowData => {
                        return (
                            <div
                                style={{
                                    overflow: 'hidden',
                                    display: '-webkit-box',
                                    WebkitBoxOrient: 'vertical',
                                    WebkitLineClamp: 3,
                                    textOverflow: 'ellipsis'
                                }}
                                title={rowData.remarks}
                            >
                                {rowData.remarks}
                            </div>
                        );
                    },editComponent: props => {
                        return(
                            <TextField
                                style={{
                                    width: 300,
                                    fontSize: '1rem',
                                    fontFamily: 'Arial'
                                }}
                                autoCapitalize="off"
                                placeholder={'Remark'}
                                value={props.value||''}
                                onChange={e => props.onChange(e.target.value)}
                                // onChange={(e)=>{this.handleIsNewChange(e,originChange);}}
                            />
                        );
					}
                }
            ],
            problemColumns: [
                {
                    title: 'Diagnosis',
                    field: 'problemText',
                    editable: 'never',
                    headerStyle: {
                        zIndex: '0',
                        width:'33%'
                    },
                    cellStyle: {
                        fontSize: '1rem',
                        height: 46
                    }
                },
                {
                    title: 'Status',
                    field: 'diagnosisStatusCd',
                    lookup: {},
                    headerStyle: {
                        zIndex: '0',
                        width:'20%'
                    },
                    cellStyle: {
                        fontSize: '1rem',
                        height: 46
                    }
                },
                {
                    title: 'Remark',
                    field: 'remarks',
                    headerStyle: {
                        zIndex: '0',
                        width:'11%'
                    },
                    cellStyle: {
                        fontSize: '1rem',
                        height: 46
                    },
                    render: rowData => {
                        return (
                            <div
                                style={{
                                    overflow: 'hidden',
                                    display: '-webkit-box',
                                    WebkitBoxOrient: 'vertical',
                                    WebkitLineClamp: 3,
                                    textOverflow: 'ellipsis'
                                }}
                                title={rowData.remarks}
                            >
                                {rowData.remarks}
                            </div>
                        );
                    }
                },{
                    title: 'New',
                    field: 'isNew',
                    headerStyle: {
                        zIndex: '1'
                    },
                    cellStyle: {
                        fontSize: '1rem',
                        height: 46
                    },
                    render: rowData => {
                        return(
                            <Checkbox
                                color="primary"
                                checked={!!rowData.isNew}
                                disabled
                            />
                        );
                    },
                    editComponent: props => {
                        let originChange = props.onChange;
                        return(
                            <Checkbox
                                color="primary"
                                checked={!!props.value}
                                onChange={(e)=>{this.handleIsNewChange(e,originChange);}}
                            />
                        );
                    }
                }
            ],
            chronicProblemColumns:[
                {
                    title:'Dx Date',
                    field:'createdDtm',
                    editable: 'never',
                    headerStyle: {
                        minWidth: 62,
                        width:'21%'
                    },
                    cellStyle: {
                        height: 46
                    },
                    render: rowData => {
                        return (
                            <div
                                style={{
                                    overflow: 'hidden',
                                    display: '-webkit-box',
                                    WebkitBoxOrient: 'vertical',
                                    WebkitLineClamp: 3,
                                    textOverflow: 'ellipsis'
                                }}
                            >
                                {!!rowData.createdDtm?moment(rowData.createdDtm).format(Enum.DATE_FORMAT_EDMY_VALUE):''}
                            </div>
                        );
                    }
                },
                {
                    title: 'Diagnosis',
                    field: 'problemText',
                    editable: 'never',
                    cellStyle: {
                        height: 46
                    },
                    headerStyle: {
                        width:'24%'
                    }
                },
                {
                    title: 'Status',
                    field: 'status',
                    lookup: {},
                    cellStyle: {
                        height: 46
                    },
                    headerStyle: {
                        width:'19%'
                    }
                },
                {
                    title: 'Remark',
                    field: 'remarks',
                    cellStyle: {
                        height: 46,
                        fontSize: '1rem',
                        fontFamily: 'Arial'
                    },
                    render: rowData => {
                        return (
                            <div
                                style={{
                                    overflow: 'hidden',
                                    display: '-webkit-box',
                                    WebkitBoxOrient: 'vertical',
                                    WebkitLineClamp: 3,
                                    textOverflow: 'ellipsis'
                                }}
                                title={rowData.remarks}
                            >
                                {rowData.remarks}
                            </div>
                        );
                    }
                }
            ],
            problemDialogOpenFlag: false,
            procedureDialogOpenFlag: false,
            historicalRecordList: [],
            remarks: '',
            searchProblemRecordTotalNums: 0,
            searchProcedureRecordList: [],
            searchProcedureRecordTotalNums: 0,
            isShowBtnProble: true,
            isShowBtnProcedure: true,
            leftEditFlag:false,
            rightEditFlag:false,
            height:0,
            diagnosisSelectionData:[],
            chronicProblemSelectionData:[]
        };
    }

    componentWillMount(){
        this.resetHeight();
        window.addEventListener('resize',this.resetHeight);
    }

    //init data
    componentDidMount() {
        // this.resetHeight();
        // window.addEventListener('resize',this.resetHeight);
        this.props.ensureDidMount();
        const { loginInfo, encounterData } = this.props;

        this.props.openCommonCircularDialog();
        this.setState({
            selectedServiceVal: loginInfo.service.code,
            service: loginInfo.service.code,
            selectedEncounterVal: encounterData.encounterId,
            selectedPatientKey: encounterData.patientKey,
            recordType: 'ALL'
        });
        this.loadHistoricalRecords({
            patientKey: encounterData.patientKey,
            serviceCd: loginInfo.service.code,
            recordType: 'ALL'
        });
        this.loadData({
            patientKey: encounterData.patientKey,
            encounterId: encounterData.encounterId
        });

        //get record type status
        this.props.listCodeDiagnosisTypes({ params: {} });

        //get problem status
        this.props.getProblemCodeDiagnosisStatusList({
            params: { diagnosisType: 1 },
            callback: data => {
                if (data.length > 0) {
                    let mapObj = new Map();
                    for (let i = 0; i < data.length; i++) {
                        mapObj.set(
                            data[i].codeDiagnosisStatusCd,
                            data[i].diagnosisStatusDesc
                        );
                    }
                    let problemColumns = this.state.problemColumns;
                    let { chronicProblemColumns } = this.state;
                    problemColumns[1].lookup = this.strMapToObj(mapObj);
                    chronicProblemColumns[2].lookup = this.strMapToObj(mapObj);
                    this.setState({
                        chronicProblemColumns,
                        problemColumns: problemColumns,
                        problemStatusList: data
                    });
                }
            }
        });

        //get procedure status
        this.props.getProcedureCodeDiagnosisStatusList({
            params: { diagnosisType: 2 },
            callback: data => {
                if (data.length > 0) {
                    let mapObj = new Map();
                    for (let i = 0; i < data.length; i++) {
                        mapObj.set(
                            data[i].codeDiagnosisStatusCd,
                            data[i].diagnosisStatusDesc
                        );
                    }
                    let procedureColumns = this.state.procedureColumns;
                    procedureColumns[1].lookup = this.strMapToObj(mapObj);
                    this.setState({
                        procedureColumns: procedureColumns,
                        procedureStatusList: data
                    });
                }
            }
        });
        // this.clearDivWorkFlow();
    }

    componentDidUpdate() {
        this.resetHistoryHeight();
        if (this.state.historicalRecordProcedureScrollFlag) {
            this.scrollToBottom('historicalRecordProcedureDiv');
        }
        if (this.state.historicalRecordProblemScrollFlag) {
            this.scrollToBottom('historicalRecordProblemDiv');
        }
    }

    componentWillUnmount(){
        window.removeEventListener('resize',this.resetHeight);
    }

    resetHeight=_.debounce(()=>{
        if(this.boxContent.current&&this.boxContent.current.clientHeight&&this.boxContent.current.clientHeight!==this.state.height){
            // console.log(this.boxContent.current.clientHeight,this.boxContent.current.clientHeight-127);
          this.setState({
            height:this.boxContent.current.clientHeight-127
            // historyHeight:this.historyContent.current.clientHeight
          });
        }
        this.resetHistoryHeight();
    },1000);

    resetHistoryHeight = () => {
        if (this.historyContent.current&&this.historyContent.current.clientHeight&&this.historyContent.current.clientHeight!==this.state.historyHeight) {
            this.setState({
                historyHeight:this.historyContent.current.clientHeight
            });
        }
    }

    handleIsNewChange = (e,originChange) => {
        originChange(e.target.checked);
    }

    // clearDivWorkFlow = () => {
    //     let divXParent = document
    //         .getElementById('materialTableDiv')
    //         .getElementsByTagName('div')[0];
    //     let divX = divXParent.getElementsByTagName('div')[0];
    //     divX.style.overflow = 'unset';
    //     let divYParent = divX.getElementsByTagName('div')[0];
    //     let divY = divYParent.getElementsByTagName('div')[0];
    //     divY.style.overflow = 'unset';

    //     let divProXParent = document
    //         .getElementById('materialTableProcedureDiv')
    //         .getElementsByTagName('div')[0];
    //     let divProX = divProXParent.getElementsByTagName('div')[0];
    //     divProX.style.overflow = 'unset';
    //     let divProYParent = divProX.getElementsByTagName('div')[0];
    //     let divProY = divProYParent.getElementsByTagName('div')[0];
    //     divProY.style.overflow = 'unset';
    // };

    //map change to obj
    strMapToObj = strMap => {
        let obj = Object.create(null);
        for (let [k, v] of strMap) {
            obj[k] = v;
        }
        return obj;
    };

    loadData = obj => {
        let params = obj;
        this.props.getInputProcedureList({
            params,
            callback: data => {
                this.setState({ procedureDataList: data.data });
            }
        });
        this.props.getInputProblemList({
            params,
            callback: data => {
                this.setState({ problemDataList: data.data });
            }
        });
        this.props.getChronicProblemList({
            params:{
                patientKey:obj.patientKey
            },
            callback: data => {
                this.setState({ chronicProblemDataList: data });
            }
        });
    };

    loadHistoricalRecords = obj => {
        let params = obj;
        this.props.getHistoricalRecords({
            params,
            callback: data => {
                //update select data.
                let selectedHistoricalRecord = this.state.selectedHistoricalRecord;
                let updateName = JSON.parse(sessionStorage.getItem('loginInfo')).loginName;
                for (let index = 0; index < data.length; index++) {
                    if (
                        (data[index].recordType === 'DX'||data[index].recordType === 'PX') &&
                        data[index].diagnosisId ===
                            selectedHistoricalRecord.diagnosisId
                    ) {
                        selectedHistoricalRecord = data[index];
                        selectedHistoricalRecord.updatedByName = updateName;
                        this.setState({
                            selectedHistoricalRecord: selectedHistoricalRecord,
                            statusValue:
                                selectedHistoricalRecord.diagnosisStatusDesc,
                            status: selectedHistoricalRecord.diagnosisStatusCd,
                            remarks:
                                selectedHistoricalRecord.remarks === null
                                    ? ''
                                    : selectedHistoricalRecord.remarks
                        });
                    }
                }
                this.setState({
                    historicalRecordList: data
                });
                this.props.closeCommonCircularDialog();
            }
        });
    };

    clinicNoteServiceValueOnChange = e => {
        let service = e;
        let { encounterData } = this.props;
        this.loadHistoricalRecords({
            patientKey: encounterData.patientKey,
            serviceCd: service,
            recordType: this.state.recordType
        });

        this.setState({
            service: service,
            isShowRecordDetailText: 'none',
            isShowRecordDetailDiv: 'none',
            diagnosisId: ''
        });
    };

    clinicNoteRecordTypeValueOnChange = e => {
        let recordType = e;
        let { encounterData } = this.props;
        let tempObj = encounterData;
        if (!isUndefined(tempObj)) {
            //get Historical Record List
            this.loadHistoricalRecords({
                patientKey: tempObj.patientKey,
                serviceCd: this.state.service,
                recordType: recordType
            });
        }
        this.setState({
            recordType: recordType,
            isShowRecordDetailText: 'none',
            isShowRecordDetailDiv: 'none',
            diagnosisId: ''
        });
    };

    changeMedicalRecord = record => {
        let medicalRecord=record[0]||{};
        if(medicalRecord.serviceCd!==undefined){
            if (medicalRecord.serviceCd === this.state.selectedServiceVal) {
                //show edit button
                if (medicalRecord.recordType === 'DX') {
                    this.setState({
                        statusList: this.state.problemStatusList,
                        status: medicalRecord.diagnosisStatusCd,
                        statusValue: medicalRecord.diagnosisStatusDesc,
                        isNew: medicalRecord.isNew,
                        remarks:
                            medicalRecord.remarks === null
                                ? ''
                                : medicalRecord.remarks,
                        isShowRecordDetailDiv: 'block',
                        isShowRecordDetailText: 'block',
                        isShow: 'block'
                    });
                } else if (medicalRecord.recordType === 'PX') {
                    this.setState({
                        statusList: this.state.procedureStatusList,
                        status: medicalRecord.diagnosisStatusCd,
                        statusValue: medicalRecord.diagnosisStatusDesc,
                        remarks:
                            medicalRecord.remarks === null
                                ? ''
                                : medicalRecord.remarks,
                        isShowRecordDetailDiv: 'block',
                        isShowRecordDetailText: 'block',
                        isShow: 'block'
                    });
                }
            } else {
                //hide edit button
                if (medicalRecord.recordType === 'DX') {
                    this.setState({
                        statusList: this.state.problemStatusList,
                        status: medicalRecord.diagnosisStatusCd,
                        statusValue: medicalRecord.diagnosisStatusDesc,
                        remarks:
                            medicalRecord.remarks === null
                                ? ''
                                : medicalRecord.remarks,
                        isShowRecordDetailDiv: 'block',
                        isShowRecordDetailText: 'none',
                        isShow: 'none'
                    });
                } else if (medicalRecord.recordType === 'PX') {
                    this.setState({
                        statusList: this.state.procedureStatusList,
                        status: medicalRecord.diagnosisStatusCd,
                        statusValue: medicalRecord.diagnosisStatusDesc,
                        remarks:
                            medicalRecord.remarks === null
                                ? ''
                                : medicalRecord.remarks,
                        isShowRecordDetailDiv: 'block',
                        isShowRecordDetailText: 'none',
                        isShow: 'none'
                    });
                }
            }
            medicalRecord.remarks =
                medicalRecord.remarks === null ? '' : medicalRecord.remarks;
            this.setState({
                diagnosisId: medicalRecord.diagnosisId,
                selectedHistoricalRecord: medicalRecord,
                leftEditFlag:false
            });
        }
        else{
            this.setState({
                isShowRecordDetailText: 'none',
                isShowRecordDetailDiv: 'none',
                isShow: 'none',
                selectedHistoricalRecord: medicalRecord,
                leftEditFlag: false,
                diagnosisId: ''
            });
        }
        // this.setState({
        //     diagnosisId: medicalRecord.diagnosisId,
        //     selectedHistoricalRecord: medicalRecord,
        //     leftEditFlag:false
        // });
    };

    hideRecordDetailSave = () => {
        if (!this.state.blurFlag) {
            this.setState({
                isShow: 'none'
            });
        }
    };

    changeRecordDetail = e => {
        this.setState({
            remarks: e.target.value,
            leftEditFlag:true
        });
    };

    editRecordDetail = () => {
        if (this.state.clinicalnoteId !== '') {
            this.setState({
                isShow: 'block'
            });
        }
    };

    blurMiss = () => {
        this.setState({
            blurFlag: false
        });
    };

    blurShow = () => {
        this.setState({
            blurFlag: true
        });
    };

    dragMedicalRecord = (event,medicalRecord) => {
        // event.dataTransfer.setData('text/plain',JSON.stringify(medicalRecord));
        event.dataTransfer.setData('text/plain',JSON.stringify(medicalRecord));
        this.dragTargetContentType = 'L'+medicalRecord.recordType;
        // this.setState({
        //     dragHistoricalRecord: medicalRecord,
        //     remarks: medicalRecord.remarks === null ? '' : medicalRecord.remarks
        // });
    };

    dragMedicalRecordEnd = event => {
        this.setState({
            dragHistoricalRecord: {}
            // remarks: medicalRecord.remarks === null ? '' : medicalRecord.remarks
        });
    }

    updateChildState = obj => {
        this.setState({
            ...obj
        });
    };

    recordDetailSave = () => {
        //get current selected clinicalnote
        let {
            clinicalNotesId,
            serviceNotesId,
            updateClinicalNoteData,
            updateState
        } = this.props;
        let clinicalNote = this.state.selectedClinicalNote;
        clinicalNote.clinicalnoteText = this.state.noteContent;
        const params = clinicalNote;
        let callBackFlag = false;
        this.props.openCommonCircularDialog();
        this.props.saveRecordDetail({
            params,
            callback: clinicalnote => {
                callBackFlag = true;
                if (
                    clinicalnote.data.clinicalnoteText ===
                    this.state.noteContent
                ) {
                    if (this.state.clinicalnoteId === clinicalNotesId) {
                        updateState &&
                            updateState({
                                isShow: 'none',
                                clinicalNotes:
                                    clinicalnote.data.clinicalnoteText
                            });
                    } else if (this.state.clinicalnoteId === serviceNotesId) {
                        updateState &&
                            updateState({
                                isShow: 'none',
                                serviceNotes: clinicalnote.data.clinicalnoteText
                            });
                    }
                }
                let payload = {
                    msgCode: clinicalnote.msgCode,
                    showSnackbar: true,
                    btnActions: {
                        // Yes
                        btn1Click: () => {
                            //update data
                            let params = {
                                name: '',
                                recordDetailSaveFlag: true
                            };
                            updateClinicalNoteData &&
                                updateClinicalNoteData(params);
                        }
                    }
                };
                this.props.openCommonMessage(payload);
                this.setState({
                    selectedClinicalNote: clinicalnote.data
                });
            }
        });
        updateState &&
            updateState({
                isShow: 'none'
            });

        if (!callBackFlag) {
            let params = { name: '', recordDetailSaveFlag: true };
            updateClinicalNoteData && updateClinicalNoteData(params);
        }
    };

    recordDetailDelete = () => {
        //Delete
        let { updateClinicalNoteData, updateState } = this.props;
        let payload = {
            msgCode: '100413',
            // Yes
            btnActions: {
                btn1Click: () => {
                    //get current selected clinicalnote
                    let clinicalNote = this.state.selectedClinicalNote;
                    clinicalNote.clinicalnoteText = this.state.noteContent;
                    const params = clinicalNote;
                    this.props.openCommonCircularDialog();
                    this.props.deleteRecordDetail({
                        params,
                        callback: msgCode => {
                            let payload = {
                                msgCode: msgCode,
                                // Yes
                                btnActions: {
                                    btn1Click: () => {
                                        if (msgCode === '100415') {
                                            //update data
                                            let params = {
                                                name: '',
                                                recordDetailSaveFlag: false
                                            };
                                            updateClinicalNoteData &&
                                                updateClinicalNoteData(params);
                                        }
                                        this.setState({
                                            noteContent: ''
                                        });
                                    }
                                }
                            };
                            this.props.openCommonMessage(payload);
                        }
                    });
                }
            }
        };
        this.props.openCommonMessage(payload);
        updateState &&
            updateState({
                isShow: 'none'
            });
    };

    handleChange = e => {
        this.setState({
            status: e.target.value,
            leftEditFlag:true
        });
    };

    handleDiagnosisHistoryDetailChange = event => {
        this.setState({
            isNew: event.target.checked?COMMON_CHECKBOX_TYPE.CHECKED:COMMON_CHECKBOX_TYPE.UNCHECKED,
            leftEditFlag:true
        });
    }

    //proble method
    handleSearch = e => {
        this.getSearchData(e);
    };

    getSearchData = searchText => {
        this.setState({
            termDisplayList: [],
            matchNum: 0
        });
        if (searchText) {
            this.props.searchProcedureList({
                params: {
                    diagnosisType: PROCEDURE_SEARCH_LIST_TYPE,
                    diagnosisText: searchText
                },
                callback: data => {
                    this.setState({
                        termDisplayList: data,
                        matchNum: data.length
                    });
                }
            });
        }
    };

    handleDiagnosisName = event => {
        if (event !== undefined) {
            let currentList = this.state.currentList;
            let resultList = [];
            let obj = {};
            if (event.name !== undefined) {
                obj = { name: event.name };
            } else {
                obj = { name: event };
            }
            currentList.push(obj);
            resultList = currentList;
            this.setState({ currentList: resultList });
        }
    };

    handleProblemServiceFavouriteDialogClose = () => {
        this.setState({
            problemDialogOpenFlag: false
        });
    };

    handleProblemServiceFavouriteDialogOpen = () => {
        this.props.requestProblemTemplateList({});
        this.setState({
            problemDialogOpenFlag: true
        });
    };

    updatePatientProblem = obj => {
        let { encounterData } = this.props;
        this.setState({
            historicalRecordProcedureScrollFlag: false,
            historicalRecordProblemScrollFlag: false
        });
        let tempObj = encounterData;
        if (!isUndefined(tempObj)) {
            this.props.openCommonCircularDialog();
            let params = {
                patientKey: tempObj.patientKey,
                encounterId: tempObj.encounterId,
                dtos: obj
            };
            this.props.updatePatientProblem({
                params,
                callback: data => {
                    let payload = {
                        msgCode: data.msgCode,
                        showSnackbar: true
                    };
                    this.props.openCommonMessage(payload);
                    this.loadHistoricalRecords({
                        patientKey: tempObj.patientKey,
                        serviceCd: this.state.service,
                        recordType: this.state.recordType
                    });
                    this.scrollToTop('historicalRecordProblemDiv');
                }
            });
        }
    };

    updateProblemObj = (newData, oldData,id) => {
        let editFlag=document.getElementById(id);
        if(!!!editFlag){
            this.props.openCommonCircularDialog();
            let problemDataList = this.state.problemDataList;
            const index = problemDataList.indexOf(oldData);
            if (
                newData.operationType === undefined ||
                (newData.operationType !== undefined &&
                    newData.operationType !== 'I')
            ) {
                newData.operationType = 'U';
            }
            let copyProblemDataList=JSON.parse(JSON.stringify(problemDataList));
            copyProblemDataList[index] = newData;
            this.setState({
                problemDataList: copyProblemDataList,
                historicalRecordProcedureScrollFlag: false,
                historicalRecordProblemScrollFlag: false,
                problemEditFlag: true
            });
            this.props.closeCommonCircularDialog();
        }
    };

    deletePatientProblem = obj => {
        let { encounterData } = this.props;
        this.setState({
            historicalRecordProcedureScrollFlag: false,
            historicalRecordProblemScrollFlag: false
        });
        let tempObj = encounterData;
        if (!isUndefined(tempObj)) {
            this.props.openCommonCircularDialog();
            let params = {
                patientKey: tempObj.patientKey,
                encounterId: tempObj.encounterId,
                dtos: obj
            };
            this.props.deletePatientProblem({
                params,
                callback: data => {
                    let payload = {
                        msgCode: data.msgCode,
                        showSnackbar: true
                    };
                    this.props.openCommonMessage(payload);
                    this.loadHistoricalRecords({
                        patientKey: tempObj.patientKey,
                        serviceCd: this.state.service,
                        recordType: this.state.recordType
                    });
                    this.scrollToTop('historicalRecordProblemDiv');
                }
            });
        }
    };

    deleteProblemObj = (obj,id) => {
        let editFlag=document.getElementById(id);
        if(!!!editFlag){
            let problemDataList = JSON.parse(JSON.stringify(this.state.problemDataList));
            let problemDeleteDataList = JSON.parse(JSON.stringify(this.state.problemDeleteDataList));
            const index = problemDataList.indexOf(obj);
            problemDataList.splice(index, 1);
            if (
                obj.operationType === undefined ||
                (obj.operationType !== undefined && obj.operationType !== 'I')
            ) {
                obj.operationType = 'D';
                problemDeleteDataList.push(obj);
                this.setState({ problemDeleteDataList: problemDeleteDataList });
            }
            this.setState({
                problemDataList: problemDataList,
                historicalRecordProcedureScrollFlag: false,
                historicalRecordProblemScrollFlag: false,
                problemEditFlag: true
            });
        }
    };

    //procedure method
    handleProcedureServiceFavouriteDialogClose = () => {
        this.setState({
            procedureDialogOpenFlag: false
        });
    };

    handleProcedureServiceFavouriteDialogOpen = () => {
        this.props.requestProcedureTemplateList({});
        this.setState({
            procedureDialogOpenFlag: true
        });
    };

    updatePatientProcedure = obj => {
        let { encounterData } = this.props;
        this.setState({
            historicalRecordProcedureScrollFlag: false,
            historicalRecordProblemScrollFlag: false
        });
        let tempObj = encounterData;
        if (!isUndefined(tempObj)) {
            this.props.openCommonCircularDialog();
            let params = {
                patientKey: tempObj.patientKey,
                encounterId: tempObj.encounterId,
                dtos: obj
            };
            this.props.updatePatientProcedure({
                params,
                callback: data => {
                    let payload = {
                        msgCode: data.msgCode,
                        showSnackbar: true
                    };
                    this.props.openCommonMessage(payload);
                    this.loadHistoricalRecords({
                        patientKey: tempObj.patientKey,
                        serviceCd: this.state.service,
                        recordType: this.state.recordType
                    });
                    this.scrollToTop('historicalRecordProcedureDiv');
                }
            });
        }
    };
    updateProcedureObj = (newData, oldData) => {
        this.props.openCommonCircularDialog();
        let procedureDataList = this.state.procedureDataList;
        // delete oldData.tableData;
        const index = procedureDataList.indexOf(oldData);
        if (
            newData.operationType === undefined ||
            (newData.operationType !== undefined &&
                newData.operationType !== 'I')
        ) {
            newData.operationType = 'U';
        }
        let copyProcedureDataList=JSON.parse(JSON.stringify(procedureDataList));
        copyProcedureDataList[index] = newData;
        this.setState({
            procedureDataList: copyProcedureDataList,
            historicalRecordProcedureScrollFlag: false,
            historicalRecordProblemScrollFlag: false,
            procedureEditFlag: true
        });
        this.props.closeCommonCircularDialog();
    };

    deletePatientProcedure = obj => {
        let { encounterData } = this.props;
        this.setState({
            historicalRecordProcedureScrollFlag: false,
            historicalRecordProblemScrollFlag: false
        });
        let tempObj = encounterData;
        if (!isUndefined(tempObj)) {
            this.props.openCommonCircularDialog();
            let params = {
                patientKey: tempObj.patientKey,
                encounterId: tempObj.encounterId,
                dtos: obj
            };
            this.props.deletePatientProcedure({
                params,
                callback: data => {
                    let payload = {
                        msgCode: data.msgCode,
                        showSnackbar: true
                    };
                    this.props.openCommonMessage(payload);
                    this.loadHistoricalRecords({
                        patientKey: tempObj.patientKey,
                        serviceCd: this.state.service,
                        recordType: this.state.recordType
                    });
                    this.scrollToTop('historicalRecordProcedureDiv');
                }
            });
        }
    };

    deleteProcedureObj = obj => {
        let procedureDataList = JSON.parse(JSON.stringify(this.state.procedureDataList));
        let procedureDeleteDataList = JSON.parse(JSON.stringify(this.state.procedureDeleteDataList));
        const index = procedureDataList.indexOf(obj);
        procedureDataList.splice(index, 1);
        if (
            obj.operationType === undefined ||
            (obj.operationType !== undefined && obj.operationType !== 'I')
        ) {
            obj.operationType = 'D';
            procedureDeleteDataList.push(obj);
            this.setState({ procedureDeleteDataList: procedureDeleteDataList });
        }
        this.setState({
            procedureDataList: procedureDataList,
            historicalRecordProcedureScrollFlag: false,
            historicalRecordProblemScrollFlag: false,
            procedureEditFlag: true
        });
    };

    copyHistoricalRecord = () => {
        //copy Record
        let historicalRecord = this.state.selectedHistoricalRecord;
        let { encounterData } = this.props;
        let tempObj = encounterData;
        this.setState({
            historicalRecordProcedureScrollFlag: false,
            historicalRecordProblemScrollFlag: false
        });

        if (!isUndefined(tempObj)) {
            if (historicalRecord.recordType === 'PX') {
                let cancelBtnNode = document.getElementById(
                    'btn_procedure_row_cancel'
                );
                if (!!cancelBtnNode) {
                    let payload = {
                        msgCode: COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
                    };
                    this.props.openCommonMessage(payload);
                } else {
                    if (
                        !(
                            this.state.remarks == '' &&
                            historicalRecord.remarks == null
                        ) &&
                        this.state.remarks !== historicalRecord.remarks
                    ) {
                        let payload = {
                            msgCode: '101101',
                            btnActions: {
                                btn1Click: () => {
                                    this.props.closeCommonCircularDialog();
                                },
                                btn2Click: () => {
                                    this.props.closeCommonCircularDialog();
                                }
                            }
                        };
                        this.props.openCommonMessage(payload);
                    } else {
                        let procedureObj = {
                            diagnosisStatusCd: this.state.status,
                            encounterId: tempObj.encounterId,
                            patientKey: tempObj.patientKey,
                            procedureText:
                                historicalRecord.diagnosisText === undefined
                                    ? historicalRecord.procedureText
                                    : historicalRecord.diagnosisText,
                            remarks: this.state.remarks,
                            termId: historicalRecord.termId,
                            operationType: 'I'
                        };
                        let procedureDataList = this.state.procedureDataList;
                        procedureDataList.unshift(procedureObj);
                        this.setState({
                            procedureDataList: procedureDataList,
                            procedureEditFlag: true
                        });
                        this.scrollToTop('historicalRecordProcedureDiv');
                    }
                    // }
                }
            } else if (historicalRecord.recordType === 'DX') {
                let cancelBtnNode = document.getElementById(
                    'btn_diagnosis_row_cancel'
                );
                if (!!cancelBtnNode) {
                    let payload = {
                        msgCode: COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
                    };
                    this.props.openCommonMessage(payload);
                } else {
                    if (
                        !(
                            this.state.remarks == '' &&
                            historicalRecord.remarks == null
                        ) &&
                        this.state.remarks !== historicalRecord.remarks
                    ) {
                        let payload = {
                            msgCode: '101101',
                            btnActions: {
                                btn1Click: () => {
                                    this.props.closeCommonCircularDialog();
                                },
                                btn2Click: () => {
                                    this.props.closeCommonCircularDialog();
                                }
                            }
                        };
                        this.props.openCommonMessage(payload);
                    } else {
                        let problemObj = {
                            diagnosisStatusCd: this.state.status,
                            encounterId: tempObj.encounterId,
                            patientKey: tempObj.patientKey,
                            problemText:
                                historicalRecord.diagnosisText === undefined
                                    ? historicalRecord.procedureText
                                    : historicalRecord.diagnosisText,
                            remarks: this.state.remarks,
                            termId: historicalRecord.termId,
                            operationType: 'I',
                            isNew: this.state.isNew
                        };
                        let problemDataList = this.state.problemDataList;
                        problemDataList.unshift(problemObj);
                        this.setState({
                            problemDataList: problemDataList,
                            problemEditFlag: true
                        });
                        this.scrollToTop('historicalRecordProblemDiv');
                    }
                    // }
                }
            }
        }
    };

    saveHistoricalRecord = () => {
        //update Record
        let historicalRecord = this.state.selectedHistoricalRecord;
        let { encounterData } = this.props;
        this.setState({
            historicalRecordProcedureScrollFlag: false,
            historicalRecordProblemScrollFlag: false,
            leftEditFlag:false
        });
        let tempObj = encounterData;
        if (!isUndefined(tempObj)) {
            if (historicalRecord.recordType === 'PX') {
                let cancelBtnNode = document.getElementById(
                    'btn_procedure_row_cancel'
                );
                if (!!cancelBtnNode) {
                    let payload = {
                        msgCode: COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
                    };
                    this.props.openCommonMessage(payload);
                } else {
                    if (
                        this.state.procedureEditFlag ||
                        this.state.problemEditFlag
                    ) {
                        let payload = {
                            msgCode:
                                COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
                        };
                        this.props.openCommonMessage(payload);
                    } else {
                        this.props.openCommonCircularDialog();
                        let params = {
                            patientKey: tempObj.patientKey,
                            encounterId: tempObj.encounterId,
                            dtos: {
                                createdBy: historicalRecord.createdBy,
                                createdClinicCd:
                                    historicalRecord.createdClinicCd,
                                createdDtm: historicalRecord.createdDtm,
                                diagnosisStatusCd: this.state.status,
                                encounterId: historicalRecord.encounterId,
                                patientKey: historicalRecord.patientKey,
                                patientProcedureId:
                                    historicalRecord.diagnosisId === undefined
                                        ? historicalRecord.patientProcedureId
                                        : historicalRecord.diagnosisId,
                                procedureText:
                                    historicalRecord.diagnosisText === undefined
                                        ? historicalRecord.procedureText
                                        : historicalRecord.diagnosisText,
                                remarks: this.state.remarks,
                                statusDisPlayName: '',
                                termId: historicalRecord.termId,
                                updatedBy: historicalRecord.updatedBy,
                                updatedClinicCd:
                                    historicalRecord.updatedClinicCd,
                                updatedDtm: historicalRecord.updatedDtm,
                                version: historicalRecord.version
                            }
                        };
                        this.props.updatePatientProcedure({
                            params,
                            callback: data => {
                                let payload = {
                                    msgCode: data.msgCode,
                                    showSnackbar: true
                                };
                                this.props.openCommonMessage(payload);
                                this.loadHistoricalRecords({
                                    patientKey: tempObj.patientKey,
                                    serviceCd: this.state.service,
                                    recordType: this.state.recordType
                                });
                                this.loadData({
                                    patientKey: tempObj.patientKey,
                                    encounterId: tempObj.encounterId
                                });
                                this.scrollToTop(
                                    'historicalRecordProcedureDiv'
                                );
                            }
                        });
                    }
                }
            } else if (historicalRecord.recordType === 'DX') {
                let cancelBtnNode = document.getElementById(
                    'btn_diagnosis_row_cancel'
                );
                if (!!cancelBtnNode) {
                    let payload = {
                        msgCode: COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
                    };
                    this.props.openCommonMessage(payload);
                } else {
                    if (
                        this.state.procedureEditFlag ||
                        this.state.problemEditFlag
                    ) {
                        let payload = {
                            msgCode:
                                COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
                        };
                        this.props.openCommonMessage(payload);
                    } else {
                        let params = {
                            patientKey: tempObj.patientKey,
                            encounterId: tempObj.encounterId,
                            dtos: {
                                createdBy: historicalRecord.createdBy,
                                createdClinicCd:
                                    historicalRecord.createdClinicCd,
                                createdDtm: historicalRecord.createdDtm,
                                diagnosisStatusCd: this.state.status,
                                encounterId: historicalRecord.encounterId,
                                patientKey: historicalRecord.patientKey,
                                patientProblemId:
                                    historicalRecord.diagnosisId === undefined
                                        ? historicalRecord.patientProblemId
                                        : historicalRecord.diagnosisId,
                                problemText:
                                    historicalRecord.diagnosisText === undefined
                                        ? historicalRecord.problemText
                                        : historicalRecord.diagnosisText,
                                remarks: this.state.remarks,
                                statusDisPlayName: '',
                                termId: historicalRecord.termId,
                                updatedBy: historicalRecord.updatedBy,
                                updatedClinicCd:
                                    historicalRecord.updatedClinicCd,
                                updatedDtm: historicalRecord.updatedDtm,
                                version: historicalRecord.version,
                                isNew: this.state.isNew
                            }
                        };
                        this.props.updatePatientProblem({
                            params,
                            callback: data => {
                                let payload = {
                                    msgCode: data.msgCode,
                                    showSnackbar: true
                                };
                                this.props.openCommonMessage(payload);
                                this.loadHistoricalRecords({
                                    patientKey: tempObj.patientKey,
                                    serviceCd: this.state.service,
                                    recordType: this.state.recordType
                                });
                                this.loadData({
                                    patientKey: tempObj.patientKey,
                                    encounterId: tempObj.encounterId
                                });
                                this.scrollToTop('historicalRecordProblemDiv');
                            }
                        });
                    }
                }
            }
        }
    };

    onEdit = e => {
        // this.refs.myTA.style.height = 'auto';
        // //关键是先设置为auto，目的为了重设高度（如果字数减少）
        // //如果高度不够，再重新设置
        // if (this.refs.myTA.scrollHeight >= this.refs.myTA.offsetHeight) {
        //   this.refs.myTA.style.height = this.refs.myTA.scrollHeight+'px';
        // }

        this.changeRecordDetail(e);
    };

    deleteHistoricalRecord = () => {
        //delete Record
        let historicalRecord = this.state.selectedHistoricalRecord;
        let { encounterData } = this.props;
        let tempObj = encounterData;
        if (!isUndefined(tempObj)) {
            if (historicalRecord.recordType === 'PX') {
                let cancelBtnNode = document.getElementById(
                    'btn_procedure_row_cancel'
                );
                if (!!cancelBtnNode) {
                    let payload = {
                        msgCode: COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
                    };
                    this.props.openCommonMessage(payload);
                } else {
                    if (
                        this.state.procedureEditFlag ||
                        this.state.problemEditFlag
                    ) {
                        let payload = {
                            msgCode:
                                COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
                        };
                        this.props.openCommonMessage(payload);
                    } else {
                        let payload = {
                            msgCode: '100906',
                            btnActions: {
                                btn1Click: () => {
                                    this.props.openCommonCircularDialog();
                                    let params = {
                                        patientKey: tempObj.patientKey,
                                        encounterId: tempObj.encounterId,
                                        dtos: {
                                            createdBy:
                                                historicalRecord.createdBy,
                                            createdClinicCd:
                                                historicalRecord.createdClinicCd,
                                            createdDtm:
                                                historicalRecord.createdDtm,
                                            diagnosisStatusCd: this.state
                                                .status,
                                            encounterId:
                                                historicalRecord.encounterId,
                                            patientKey:
                                                historicalRecord.patientKey,
                                            patientProcedureId:
                                                historicalRecord.diagnosisId ===
                                                undefined
                                                    ? historicalRecord.patientProcedureId
                                                    : historicalRecord.diagnosisId,
                                            procedureText:
                                                historicalRecord.diagnosisText ===
                                                undefined
                                                    ? historicalRecord.procedureText
                                                    : historicalRecord.diagnosisText,
                                            remarks: this.state.remarks,
                                            statusDisPlayName: '',
                                            termId: historicalRecord.termId,
                                            updatedBy:
                                                historicalRecord.updatedBy,
                                            updatedClinicCd:
                                                historicalRecord.updatedClinicCd,
                                            updatedDtm:
                                                historicalRecord.updatedDtm,
                                            version: historicalRecord.version
                                        }
                                    };
                                    this.props.deletePatientProcedure({
                                        params,
                                        callback: data => {
                                            let payload = {
                                                msgCode: data.msgCode,
                                                showSnackbar: true
                                            };
                                            this.props.openCommonMessage(
                                                payload
                                            );
                                            this.loadHistoricalRecords({
                                                patientKey: tempObj.patientKey,
                                                serviceCd: this.state.service,
                                                recordType: this.state
                                                    .recordType
                                            });
                                            this.loadData({
                                                patientKey: tempObj.patientKey,
                                                encounterId: tempObj.encounterId
                                            });
                                            this.setState({
                                                isShowRecordDetailText: 'none',
                                                isShowRecordDetailDiv: 'none',
                                                isShow: 'none'
                                            });
                                        }
                                    });
                                }
                            }
                        };
                        this.props.openCommonMessage(payload);
                    }
                }
            } else if (historicalRecord.recordType === 'DX') {
                let cancelBtnNode = document.getElementById(
                    'btn_diagnosis_row_cancel'
                );
                if (!!cancelBtnNode) {
                    let payload = {
                        msgCode: COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
                    };
                    this.props.openCommonMessage(payload);
                } else {
                    if (
                        this.state.procedureEditFlag ||
                        this.state.problemEditFlag
                    ) {
                        let payload = {
                            msgCode:
                                COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
                        };
                        this.props.openCommonMessage(payload);
                    } else {
                        let payload = {
                            msgCode: '100813',
                            btnActions: {
                                btn1Click: () => {
                                    this.props.openCommonCircularDialog();
                                    let params = {
                                        patientKey: tempObj.patientKey,
                                        encounterId: tempObj.encounterId,
                                        dtos: {
                                            createdBy:
                                                historicalRecord.createdBy,
                                            createdClinicCd:
                                                historicalRecord.createdClinicCd,
                                            createdDtm:
                                                historicalRecord.createdDtm,
                                            diagnosisStatusCd:
                                                historicalRecord.diagnosisStatusCd,
                                            encounterId:
                                                historicalRecord.encounterId,
                                            patientKey:
                                                historicalRecord.patientKey,
                                            patientProblemId:
                                                historicalRecord.diagnosisId ===
                                                undefined
                                                    ? historicalRecord.patientProblemId
                                                    : historicalRecord.diagnosisId,
                                            problemText:
                                                historicalRecord.diagnosisText ===
                                                undefined
                                                    ? historicalRecord.problemText
                                                    : historicalRecord.diagnosisText,
                                            remarks: this.state.remarks,
                                            statusDisPlayName: '',
                                            termId: historicalRecord.termId,
                                            updatedBy:
                                                historicalRecord.updatedBy,
                                            updatedClinicCd:
                                                historicalRecord.updatedClinicCd,
                                            updatedDtm:
                                                historicalRecord.updatedDtm,
                                            version: historicalRecord.version
                                        }
                                    };
                                    this.props.deletePatientProblem({
                                        params,
                                        callback: data => {
                                            let payload = {
                                                msgCode: data.msgCode,
                                                showSnackbar: true
                                            };
                                            this.props.openCommonMessage(
                                                payload
                                            );
                                            this.loadHistoricalRecords({
                                                patientKey: tempObj.patientKey,
                                                serviceCd: this.state.service,
                                                recordType: this.state
                                                    .recordType
                                            });
                                            this.loadData({
                                                patientKey: tempObj.patientKey,
                                                encounterId: tempObj.encounterId
                                            });
                                            this.setState({
                                                isShowRecordDetailText: 'none',
                                                isShowRecordDetailDiv: 'none',
                                                isShow: 'none'
                                            });
                                        }
                                    });
                                }
                            }
                        };
                        this.props.openCommonMessage(payload);
                    }
                }
            }
        }
    };

    handleAddSearchData = (textVal, clickAddMark) => {
        let { encounterData } = this.props;
        let tempObj = encounterData;
        if (clickAddMark != null && clickAddMark) {
            let problemObj = {
                encounterId: tempObj.encounterId,
                patientKey: tempObj.patientKey,
                problemText: textVal,
                remarks: '',
                termId: '-9999',
                operationType: 'I',
                diagnosisStatusCd: 'A'
            };
            let problemDataList = this.state.problemDataList;
            problemDataList.push(problemObj);
            this.setState({
                problemDataList: problemDataList,
                problemEditFlag: true
            });
            this.scrollToTop('historicalRecordProblemDiv');
        } else {
            let procedureObj = {
                encounterId: tempObj.encounterId,
                patientKey: tempObj.patientKey,
                procedureText: textVal,
                remarks: '',
                termId: '-9999',
                operationType: 'I',
                diagnosisStatusCd: 'CP'
            };
            let procedureDataList = this.state.procedureDataList;
            procedureDataList.push(procedureObj);
            this.setState({
                procedureDataList: procedureDataList,
                procedureEditFlag: true,
                isShowBtnProcedure: true
            });
            this.scrollToTop('historicalRecordProcedureDiv');
        }
    };

    handleProcedureAddSearchData = (textVal, clickAddMark) => {
        if (clickAddMark != null && clickAddMark) {
            let { encounterData } = this.props;
            let tempObj = encounterData;
            let procedureObj = {
                diagnosisStatusCd: this.state.status,
                encounterId: tempObj.encounterId,
                patientKey: tempObj.patientKey,
                procedureText: textVal,
                remarks: '',
                termId: '-9999',
                operationType: 'I'
                // diagnosisStatusCd:'CP'
            };
            let procedureDataList = this.state.procedureDataList;
            procedureDataList.push(procedureObj);
            this.setState({
                procedureDataList: procedureDataList,
                procedureEditFlag: true,
                addProcedureMark: true
            });
            this.scrollToTop('historicalRecordProblemDiv');
        }
    };

    handleProblemFuzzySearch = (textVal, InputBlur) => {
        let { sysConfig } = this.props;
        this.props.queryProblemList({
            params: {
                diagnosisText: textVal,
                diagnosisType: PROBLEM_SEARCH_LIST_TYPE,
                start: 0,
                end: !!sysConfig[COMMON_SYS_CONFIG_KEY.PAGE_SIZE]
                    ? toInteger(
                          sysConfig[COMMON_SYS_CONFIG_KEY.PAGE_SIZE].value
                      )
                    : DEFAULT_OFFLINE_PAGE_SIZE
            },
            callback: data => {
                this.setState({
                    searchProblemRecordList: data.recordList,
                    searchProblemRecordTotalNums: data.totalRecord || 0
                });
            }
        });
    };

    handleProblemSearchBoxLoadMoreRows = (
        startIndex,
        stopIndex,
        valText,
        dataList,
        updateState
    ) => {
        return axios
            .post('/diagnosis/listCodeTerminology', {
                diagnosisText: valText,
                diagnosisType: PROBLEM_SEARCH_LIST_TYPE,
                start: startIndex,
                end: stopIndex + 1
            })
            .then(response => {
                if (response.data.respCode === 0) {
                    dataList = dataList.concat(response.data.data.recordList);
                    updateState &&
                        updateState({
                            dataList
                        });
                }
            });
    };

    handleProblemSelectItem = item => {
        const { selectedPatientKey, selectedEncounterVal } = this.state;
        let paraObj = {
            patientKey: selectedPatientKey,
            encounterId: selectedEncounterVal,
            diagnosisStatusCd: DEFAULT_PROBLEM_SAVE_STATUS,
            termId: item.cdTermId,
            problemText: item.termDisplayName,
            remark: ''
        };
        let cancelBtnNode = document.getElementById('btn_diagnosis_row_cancel');
        if (!!cancelBtnNode) {
            let payload = {
                msgCode: COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
            };
            this.props.openCommonMessage(payload);
        } else {
            let problemArr = [];
            problemArr.push(paraObj);
            this.addProblemObj(problemArr);
            this.setState({
                historicalRecordProblemScrollFlag: true,
                rightEditFlag:true
            });
        }
    };

    handleProcedureSearchBoxLoadMoreRows = (
        startIndex,
        stopIndex,
        valText,
        dataList,
        updateState
    ) => {
        return axios
            .post('/diagnosis/listCodeTerminology', {
                diagnosisText: valText,
                diagnosisType: PROCEDURE_SEARCH_LIST_TYPE,
                start: startIndex,
                end: stopIndex + 1
            })
            .then(response => {
                if (response.data.respCode === 0) {
                    dataList = dataList.concat(response.data.data.recordList);
                    updateState &&
                        updateState({
                            dataList
                        });
                }
            });
    };

    handleProcedureFuzzySearch = textVal => {
        this.props.queryProcedureList({
            params: {
                diagnosisText: textVal,
                diagnosisType: PROCEDURE_SEARCH_LIST_TYPE,
                start: 0,
                end: 30
            },
            callback: data => {
                let tempObj = find(data.recordList, item => {
                    return item.termDisplayName === textVal.trim();
                });
                if (tempObj) {
                    this.setState({
                        isShowBtnProcedure: true
                    });
                } else {
                    this.setState({
                        isShowBtnProcedure: false
                    });
                }
                this.setState({
                    searchProcedureRecordList: data.recordList,
                    searchProcedureRecordTotalNums: data.totalRecord || 0
                });
            }
        });
    };

    handleProcedureSelectItem = item => {
        const { selectedPatientKey, selectedEncounterVal } = this.state;
        let paraObj = {
            patientKey: selectedPatientKey,
            encounterId: selectedEncounterVal,
            diagnosisStatusCd: DEFAULT_PROCEDURE_SAVE_STATUS,
            termId: item.cdTermId,
            procedureText: item.termDisplayName,
            remark: ''
        };
        let cancelBtnNode = document.getElementById('btn_procedure_row_cancel');
        if (!!cancelBtnNode) {
            let payload = {
                msgCode: COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
            };
            this.props.openCommonMessage(payload);
        } else {
            let procedureArr = [];
            procedureArr.push(paraObj);
            this.addProcedureObj(procedureArr);
            this.setState({
                historicalRecordProcedureScrollFlag: true,
                isShowBtnProcedure: true,
                rightEditFlag:true
            });
        }
    };
    closeSearchData = () => {
        this.setState({
            isShowBtnProble: false,
            isShowBtnProcedure: true
        });
    };

    // handle diagnosis drag start
    handleDiagnosisDragStart = (event,selectedDiagnosis) => {
        event.dataTransfer.setData('text/plain',JSON.stringify(selectedDiagnosis));
        this.dragTargetContentType = TABLE_CONTENT_TYPE.CHRONIC;
    };

    // handle chronic problem drag start
    handleChronicProblemDragStart = (event,selectedChronicProblem) => {
        event.dataTransfer.setData('text/plain',JSON.stringify(selectedChronicProblem));
        this.dragTargetContentType = TABLE_CONTENT_TYPE.DIAGNOSIS;
    };

    // handle chronic problem drag over
    handleChronicProblemDragOver = event => {
        event.preventDefault();
        if (this.dragTargetContentType !== TABLE_CONTENT_TYPE.CHRONIC) {
            event.dataTransfer.dropEffect = 'none';
        } else {
            event.dataTransfer.dropEffect = 'all';
        }
    }

    handleChronicProblemDragEnd = event => {
        this.dragTargetContentType = '';
    }

    // handle chronic problem drop
    handleChronicProblemDrop = event => {
        event.preventDefault();
        let cancelBtnNode = document.getElementById('btn_chronic_problem_row_cancel');
        if (!!cancelBtnNode) {
            let payload = {
                msgCode: COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
            };
            this.props.openCommonMessage(payload);
        } else {
            const { encounterData } = this.props;
            let { chronicProblemDataList } = this.state;
            let data = event.dataTransfer.getData('text/plain');
            data = JSON.parse(data);
            // validation
            let index = findIndex(chronicProblemDataList,(dataItem)=>{
                return dataItem.problemText === data.problemText;
            });
            if (index !== -1) {
                this.props.openCommonMessage({
                    msgCode:PROCEDURE_CODE.DUPLICATE_CHRONIC_PROBLEM
                });
            } else {
                let currentDate = new Date();
                chronicProblemDataList.unshift({
                    chronicProblemId:null,
                    patientKey:encounterData.patientKey,
                    termId:data.termId,
                    problemText:data.problemText,
                    status:'A',
                    remarks:data.remarks||'',
                    saveInd:CHRONIC_SAVE_IND_TYPE.SAVE, // Add => Save
                    version:null,
                    operationType:COMMON_ACTION_TYPE.INSERT,
                    createdBy:null,
                    createdClinicCd:null,
                    createdDtm:currentDate,
                    updatedBy:null,
                    updatedClinicCd:null,
                    updatedDtm:null
                });
                this.setState({
                    chronicProblemDataList,
                    rightEditFlag:true
                });
            }
        }
        this.dragTargetContentType = '';
    }

    handleChronicProblemRowUpdate = (newData,oldData) => {
        let chronicProblemDataList = this.state.chronicProblemDataList;
        return new Promise((resolve) => {
            let index = chronicProblemDataList.indexOf(oldData);
            if (!!newData.version) {
                newData.operationType = COMMON_ACTION_TYPE.UPDATE;
            }
            let copyChronicProblemDataList=JSON.parse(JSON.stringify(chronicProblemDataList));
            copyChronicProblemDataList[index] = newData;
            this.setState({
                chronicProblemDataList:copyChronicProblemDataList,
                problemEditFlag: true
            },()=>resolve());
        });
    }

    handleChronicProblemRowDelete = oldData => {
        let chronicProblemDataList = JSON.parse(JSON.stringify(this.state.chronicProblemDataList));
        let chronicProblemDeletedDataList  = JSON.parse(JSON.stringify(this.state.chronicProblemDeletedDataList));
        return new Promise((resolve) => {
            let index = chronicProblemDataList.indexOf(oldData);
            if (!!oldData.version) {
                oldData.saveInd = CHRONIC_SAVE_IND_TYPE.DELETE; // Delete => D
                oldData.operationType = COMMON_ACTION_TYPE.DELETE;
                chronicProblemDeletedDataList.push(oldData);
            }
            chronicProblemDataList.splice(index, 1);
            this.setState({
                chronicProblemDataList,
                chronicProblemDeletedDataList,
                problemEditFlag: true,
                rightEditFlag:true
            },()=>resolve());
        });
    }

    onProblemDragOver = e => {
        e.preventDefault();
        let type = this.state.dragHistoricalRecord.recordType;
        if (this.dragTargetContentType!==TABLE_CONTENT_TYPE.DIAGNOSIS&&type !== 'DX'&&this.dragTargetContentType!==TABLE_CONTENT_TYPE.LEFTDIAGNOSIS) e.dataTransfer.dropEffect = 'none';
        else e.dataTransfer.dropEffect = 'all';
        // this.setState({
        //     historicalRecordProcedureScrollFlag: false,
        //     historicalRecordProblemScrollFlag: false
        // });
    };

    onProcedureDragOver = e => {
        e.preventDefault();

        let type = this.state.dragHistoricalRecord.recordType;
        if (type !== 'PX'&&this.dragTargetContentType!==TABLE_CONTENT_TYPE.LEFTPROCEDURE) e.dataTransfer.dropEffect = 'none';
        else e.dataTransfer.dropEffect = 'all';
        // this.setState({
        //     historicalRecordProcedureScrollFlag: false,
        //     historicalRecordProblemScrollFlag: false
        // });
    };

    onProcedureDrop = () => {
        // let historicalRecord = this.state.dragHistoricalRecord;
        let cancelBtnNode = document.getElementById('btn_procedure_row_cancel');
        if (!!cancelBtnNode) {
            let payload = {
                msgCode: COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
            };
            this.props.openCommonMessage(payload);
        } else {
            if (this.dragTargetContentType===TABLE_CONTENT_TYPE.LEFTPROCEDURE) {
                let { encounterData } = this.props;
                let tempObj = encounterData;
                let historicalRecord = event.dataTransfer.getData('text/plain');
                historicalRecord = JSON.parse(historicalRecord);
                if (!isUndefined(tempObj)) {
                    this.props.openCommonCircularDialog();
                    let params = {
                        // createdBy: historicalRecord.createdBy,
                        // createdClinicCd: historicalRecord.createdClinicCd,
                        // createdDtm: historicalRecord.createdDtm,
                        diagnosisStatusCd: 'CP',
                        encounterId: encounterData.encounterId,
                        // encounterId: historicalRecord.encounterId,
                        patientKey: historicalRecord.patientKey,
                        // patientProcedureId: null,
                        procedureText:
                            historicalRecord.diagnosisText === undefined
                                ? historicalRecord.procedureText
                                : historicalRecord.diagnosisText,
                        remarks: historicalRecord.remarks=== null ? '' :historicalRecord.remarks,
                        statusDisPlayName: '',
                        termId: historicalRecord.termId
                        // updatedBy: historicalRecord.updatedBy,
                        // updatedClinicCd: historicalRecord.updatedClinicCd,
                        // updatedDtm: historicalRecord.updatedDtm,
                        // version: historicalRecord.version
                    };
                    let procedureArr = [];
                    procedureArr.push(params);
                    this.addProcedureObj(procedureArr);
                    this.loadHistoricalRecords({
                        patientKey: tempObj.patientKey,
                        serviceCd: this.state.service,
                        recordType: this.state.recordType
                    });
                    this.setState({
                        historicalRecordProcedureScrollFlag: true,
                        rightEditFlag:true
                    });
                }
            }
        }
    };

    onProblemDrop = () => {
        let cancelBtnNode = document.getElementById('btn_diagnosis_row_cancel');
        if (!!cancelBtnNode) {
            let payload = {
                msgCode: COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
            };
            this.props.openCommonMessage(payload);
        } else {
            if (this.dragTargetContentType!==''&&this.dragTargetContentType===TABLE_CONTENT_TYPE.DIAGNOSIS) {
                let data = event.dataTransfer.getData('text/plain');
                data = JSON.parse(data);
                const { encounterData } = this.props;
                let problemDataList = JSON.parse(JSON.stringify(this.state.problemDataList));

                problemDataList.push({
                    // createdBy: null,
                    // createdClinicCd: null,
                    // createdDtm: null,
                    diagnosisStatusCd: 'A',
                    encounterId: encounterData.encounterId,
                    patientKey: encounterData.patientKey,
                    // patientProblemId: null,
                    problemText: data.problemText,
                    remarks: data.remarks,
                    statusDisPlayName: '',
                    termId: data.termId,
                    // updatedBy: null,
                    // updatedClinicCd: null,
                    // updatedDtm: null,
                    // version: null,
                    operationType:COMMON_ACTION_TYPE.INSERT
                });
                this.setState({
                    problemDataList,
                    problemEditFlag:true
                });
            } else if (this.dragTargetContentType===TABLE_CONTENT_TYPE.LEFTDIAGNOSIS) {
                this.props.openCommonCircularDialog();
                let { encounterData } = this.props;
                let tempObj = encounterData;
                let historicalRecord = event.dataTransfer.getData('text/plain');
                historicalRecord = JSON.parse(historicalRecord);
                if (!isUndefined(tempObj)) {
                    let params = {
                        // createdBy: historicalRecord.createdBy,
                        // createdClinicCd: historicalRecord.createdClinicCd,
                        // createdDtm: historicalRecord.createdDtm,
                        diagnosisStatusCd: 'A',
                        encounterId: encounterData.encounterId,
                        // encounterId: historicalRecord.encounterId,
                        patientKey: historicalRecord.patientKey,
                        // patientProblemId: null,
                        problemText:
                            historicalRecord.diagnosisText === undefined
                                ? historicalRecord.problemText
                                : historicalRecord.diagnosisText,
                        remarks: historicalRecord.remarks=== null ? '' :historicalRecord.remarks,
                        statusDisPlayName: '',
                        termId: historicalRecord.termId
                        // updatedBy: historicalRecord.updatedBy,
                        // updatedClinicCd: historicalRecord.updatedClinicCd,
                        // updatedDtm: historicalRecord.updatedDtm,
                        // version: historicalRecord.version
                    };
                    let problemArr = [];
                    problemArr.push(params);
                    this.addProblemObj(problemArr);
                    this.loadHistoricalRecords({
                        patientKey: tempObj.patientKey,
                        serviceCd: this.state.service,
                        recordType: this.state.recordType
                    });
                    this.setState({
                        historicalRecordProblemScrollFlag: true,
                        rightEditFlag:true
                    });
                }
            }
        }
    };

    handleActionCancel = (event, tableProps) => {
        tableProps.action.onClick(event, tableProps.data);
    };

    scrollToTop = type => {
        let anchorElement = document.getElementById(type);
        if (anchorElement) {
            anchorElement.scrollTop = 0;
        }
    };

    scrollToBottom = type => {
        let anchorElement = document.getElementById(type);
        if (anchorElement) {
            anchorElement.scrollTop = anchorElement.scrollHeight;
        }
    };

    addProblemObj = obj => {
        let problemDataList = JSON.parse(JSON.stringify(this.state.problemDataList));
        for (let index = 0; index < obj.length; index++) {
            obj[index].operationType = 'I';
            problemDataList.unshift(obj[index]);
        }
        //problemDataList.reverse;
        this.setState({
            problemDataList: problemDataList,
            historicalRecordProblemScrollFlag: true,
            problemEditFlag: true,
            rightEditFlag:true
        });
    };

    addProcedureObj = obj => {
        let procedureDataList = JSON.parse(JSON.stringify(this.state.procedureDataList));
        for (let index = 0; index < obj.length; index++) {
            obj[index].operationType = 'I';
            procedureDataList.unshift(obj[index]);
        }
        this.setState({
            procedureDataList: procedureDataList,
            historicalRecordProcedureScrollFlag: true,
            procedureEditFlag: true,
            rightEditFlag:true
        });
    };

    reverse = arr =>{
      let newArr = [];
      for (let i = 0; i < arr.length; i++) {
        let newObj = [];
        for (let index = 0; index < arr[i].length; index++) {
          let item = arr[i][index];
          newObj.unshift(item);
        }
        newArr[i] = newObj;
      }
      return newArr;
    }

    handleClickSave = () => {
        let procedureCancelBtnNode = document.getElementById('btn_procedure_row_cancel');
        if (!!procedureCancelBtnNode) {
            let payload = {
                msgCode: COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
            };
            this.props.openCommonMessage(payload);
        } else {
            let diagnosisCancelBtnNode = document.getElementById('btn_diagnosis_row_cancel');
            let chronicProblemCancelBtnNode = document.getElementById('btn_chronic_problem_row_cancel');
            if (!!diagnosisCancelBtnNode||!!chronicProblemCancelBtnNode) {
                let payload = {
                    msgCode: COMMON_CODE.COMPLETE_PREVIOUS_ACTION_COMFIRM
                };
                this.props.openCommonMessage(payload);
            } else {
                let procedureDataList = this.state.procedureDataList;
                let procedureDeleteDataList = this.state
                    .procedureDeleteDataList;
                let procedureData = [];
                let problemDataList = this.state.problemDataList;
                let problemDeleteDataList = this.state.problemDeleteDataList;
                let problemData = [];
                let { chronicProblemDataList,chronicProblemDeletedDataList } = this.state;
                let chronicData = [];

                for (let index = 0; index < problemDataList.length; index++) {
                    if (
                        problemDataList[index].operationType !== undefined &&
                        problemDataList[index].operationType !== null
                    ) {
                        problemDataList[index].isNew = problemDataList[index].isNew!==null?(
                            problemDataList[index].isNew?COMMON_CHECKBOX_TYPE.CHECKED:COMMON_CHECKBOX_TYPE.UNCHECKED
                        ):null;
                        problemData.push(problemDataList[index]);
                    }
                }
                problemData = problemDeleteDataList.concat(problemData);

                // chronic problem
                for (let i = 0; i < chronicProblemDataList.length; i++) {
                    if (
                        chronicProblemDataList[i].operationType !== undefined &&
                        chronicProblemDataList[i].operationType !== null
                    ) {
                        chronicData.push(chronicProblemDataList[i]);
                    }
                }
                chronicData = chronicData.concat(chronicProblemDeletedDataList);

                for (let index = 0; index < procedureDataList.length; index++) {
                    if (
                        procedureDataList[index].operationType !== undefined &&
                        procedureDataList[index].operationType !== null
                    ) {
                        procedureData.push(procedureDataList[index]);
                    }
                }
                procedureData = procedureData.concat(procedureDeleteDataList);

                if (
                    procedureData.length === 0 &&
                    procedureDataList.length === 0 &&
                    problemData.length === 0 &&
                    problemDataList.length === 0 &&
                    chronicData.length === 0 &&
                    chronicProblemDataList.length === 0
                ) {
                    let payload = {
                        msgCode: '100103'
                    };
                    this.props.openCommonMessage(payload);
                } else {
                    if (
                        procedureData.length === 0 &&
                        problemData.length === 0 &&
                        chronicData.length === 0
                    ) {
                        let payload = {
                            msgCode: '100917',
                            showSnackbar: true
                        };
                        this.props.openCommonMessage(payload);
                        this.setState({
                            problemEditFlag: false,
                            procedureEditFlag: false
                        });
                    } else {
                        this.props.openCommonCircularDialog();
                          //reverse data
                        let newData= this.reverse([problemData,procedureData,chronicData]);
                        let newProblemData = newData[0];
                        let newProcedureData = newData[1];
                        let newChronicData = newData[2];
                        // for (let index = 0; index < problemData.length; index++) {
                        // let item = problemData[index];
                        //   newProblemData.unshift(item);
                        // }
                        // problemData = newProblemData;

                        let params = {
                          problemDtos: newProblemData,
                          procedureDtos: newProcedureData,
                          chronicDtos: newChronicData
                        };
                        this.props.savePatient({
                            params,
                            callback: data => {
                                let payload = {
                                    msgCode: data.msgCode
                                };
                                if (data.msgCode === '100917') {
                                    payload.showSnackbar = true;
                                }
                                this.props.openCommonMessage(payload);
                                let { encounterData } = this.props;
                                let tempObj = encounterData;
                                if (!isUndefined(tempObj)) {
                                    this.loadHistoricalRecords({
                                        patientKey: tempObj.patientKey,
                                        serviceCd: this.state.service,
                                        recordType: this.state.recordType
                                    });
                                    this.loadData({
                                        patientKey: tempObj.patientKey,
                                        encounterId: tempObj.encounterId
                                    });
                                    this.scrollToTop(
                                        'historicalRecordProcedureDiv'
                                    );
                                    this.scrollToTop(
                                        'historicalRecordProblemDiv'
                                    );
                                    this.setState({
                                        problemEditFlag: false,
                                        procedureEditFlag: false,
                                        isShowRecordDetailDiv: 'none',
                                        diagnosisId: ''
                                    });
                                }
                            }
                        });
                        this.setState({
                            historicalRecordProcedureScrollFlag: false,
                            historicalRecordProblemScrollFlag: false,
                            problemDeleteDataList: [],
                            procedureDeleteDataList: [],
                            chronicProblemDeletedDataList: []
                        });
                    }
                }
            }
        }
        this.setState({rightEditFlag:false});
    };

    handleCloseDialog = () => {
        // this.props.openCommonCircularDialog();
        // let { encounterData } = this.props;
        // let tempObj = encounterData;
        // if (!isUndefined(tempObj)) {
        //     this.loadHistoricalRecords({
        //         patientKey: tempObj.patientKey,
        //         serviceCd: this.state.service,
        //         recordType: this.state.recordType
        //     });
        //     this.loadData({
        //         patientKey: tempObj.patientKey,
        //         encounterId: tempObj.encounterId
        //     });
        // }
        // this.setState({
        //     isShowRecordDetailDiv: 'none',
        //     diagnosisId: '',
        //     problemDataList: [],
        //     problemDeleteDataList: []
        // });
        let { leftEditFlag,rightEditFlag } = this.state;
        if (leftEditFlag||rightEditFlag) {
          let payload = {
            msgCode:COMMON_CODE.SAVE_WARING,
            btnActions:{
              btn1Click: () => {
                this.props.deleteSubTabs(accessRightEnum.dxpxHistoricalRecord);
              }
            }
          };
          this.props.openCommonMessage(payload);
        } else {
          this.props.deleteSubTabs(accessRightEnum.dxpxHistoricalRecord);
        }
    };

    handleActionSave = (event, tableProps) => {
        this.setState({
            noDataTip: 'There is no data.',
            rightEditFlag:true
        });
        tableProps.action.onClick(event, tableProps.data);
    };

    editStatusCheck=()=>{
        let editFlag=false;
        let diagnosisEditFlag=document.getElementById('btn_diagnosis_row_cancel');
        let procedureEditFlag=document.getElementById('btn_procedure_row_cancel');
        let chronicEditFlag=document.getElementById('btn_chronic_problem_row_cancel');
        if(!!diagnosisEditFlag||!!procedureEditFlag||!!chronicEditFlag||this.state.leftEditFlag||this.state.rightEditFlag){
            editFlag=true;
        } else {
            editFlag=this.state.problemEditFlag||this.state.procedureEditFlag;
        }
        return editFlag;
    }

    diagnosisSelectionChange=(selectionData)=>{
        this.setState({diagnosisSelectionData:selectionData});
    }

    chronicProblemSelectionChange=(selectionData)=>{
        this.setState({chronicProblemSelectionData:selectionData});
    }

    copyDiagnosisSelectionToChronicProblemTable=()=>{
        const {diagnosisSelectionData=[]}=this.state;
        if(diagnosisSelectionData.length){
            let currentDate = new Date();
            const { encounterData } = this.props;
            let chronicProblemDataList = JSON.parse(JSON.stringify(this.state.chronicProblemDataList))||[];

            diagnosisSelectionData.forEach((data)=>{
                let index =findIndex(chronicProblemDataList,(dataItem)=>{
                    return dataItem.problemText === data.problemText;
                });
                if (index === -1) {
                    chronicProblemDataList.unshift({
                        chronicProblemId:null,
                        patientKey:encounterData.patientKey,
                        termId:data.termId,
                        problemText:data.problemText,
                        status:'A',
                        remarks:data.remarks||'',
                        saveInd:CHRONIC_SAVE_IND_TYPE.SAVE, // Add => Save
                        version:null,
                        operationType:COMMON_ACTION_TYPE.INSERT,
                        createdBy:null,
                        createdClinicCd:null,
                        createdDtm:currentDate,
                        updatedBy:null,
                        updatedClinicCd:null,
                        updatedDtm:null
                    });
                }

            });

            this.setState({
                chronicProblemDataList,
                rightEditFlag:true,
                diagnosisSelectionData:[]
            });
        }
    }
    copyChronicProblemSelectionToDiagnosisTable=()=>{
        const {chronicProblemSelectionData=[]}=this.state;
        if(chronicProblemSelectionData.length){
            const { encounterData } = this.props;
            let problemDataList = JSON.parse(JSON.stringify(this.state.problemDataList));
            chronicProblemSelectionData.forEach((data)=>{
                problemDataList.push({
                    createdBy: null,
                    createdClinicCd: null,
                    createdDtm: null,
                    diagnosisStatusCd: 'A',
                    encounterId: encounterData.encounterId,
                    patientKey: encounterData.patientKey,
                    patientProblemId: null,
                    problemText: data.problemText,
                    remarks: data.remarks,
                    statusDisPlayName: '',
                    termId: data.termId,
                    updatedBy: null,
                    updatedClinicCd: null,
                    updatedDtm: null,
                    version: null,
                    operationType:COMMON_ACTION_TYPE.INSERT
                });
            });
            this.setState({
                problemDataList,
                problemEditFlag:true,
                chronicProblemSelectionData:[]
            });
        }
    }
    render() {
        const { classes, encounterData, sysConfig } = this.props;
        let {service = []}=this.props;
        let {
            problemDialogOpenFlag,
            procedureDialogOpenFlag,
            searchProblemRecordTotalNums,
            searchProblemRecordList,
            searchProcedureRecordList,
            searchProcedureRecordTotalNums,
            isShowBtnProble,
            chronicProblemColumns
        } = this.state;
        let searchAddProblem = {
            isShowBtnProble: isShowBtnProble,
            prbleMark: true,
            patientKey: !!encounterData ? encounterData.patientKey : null,
            encounterId: !!encounterData ? encounterData.encounterId : null,
            status: DEFAULT_PROBLEM_SAVE_STATUS,
            pageSize: !!sysConfig[COMMON_SYS_CONFIG_KEY.PAGE_SIZE]
                ? toInteger(sysConfig[COMMON_SYS_CONFIG_KEY.PAGE_SIZE].value)
                : DEFAULT_OFFLINE_PAGE_SIZE
        };

        let problemFavouriteDialogProps = {
            patientKey: !!encounterData ? encounterData.patientKey : null,
            encounterId: !!encounterData ? encounterData.encounterId : null,
            isOpen: problemDialogOpenFlag,
            mode: PROBLEM_MODE,
            handleClose: this.handleProblemServiceFavouriteDialogClose,
            cancelBtnNode: document.getElementById('btn_diagnosis_row_cancel'),
            addMethod: this.addProblemObj
        };

        let searchAddProcedure = {
            //  addButtonMark:isShowBtnProcedure,
            patientKey: !!encounterData ? encounterData.patientKey : null,
            encounterId: !!encounterData ? encounterData.encounterId : null,
            status: DEFAULT_PROCEDURE_SAVE_STATUS,
            pageSize: !!sysConfig[COMMON_SYS_CONFIG_KEY.PAGE_SIZE]
                ? toInteger(sysConfig[COMMON_SYS_CONFIG_KEY.PAGE_SIZE].value)
                : DEFAULT_OFFLINE_PAGE_SIZE
        };

        let procedureFavouriteDialogProps = {
            patientKey: !!encounterData ? encounterData.patientKey : null,
            encounterId: !!encounterData ? encounterData.encounterId : null,
            isOpen: procedureDialogOpenFlag,
            mode: PROCEDURE_MODE,
            handleClose: this.handleProcedureServiceFavouriteDialogClose,
            cancelBtnNode: document.getElementById('btn_procedure_row_cancel'),
            addMethod: this.addProcedureObj
        };

        const MenuProps = {
            style: {
                fontSize: '1rem',
                fontFamily: 'Arial'
            }
        };

        const buttonBar={
            // isEdit:this.state.leftEditFlag||this.state.rightEditFlag,
            isEdit:this.editStatusCheck,
            // editFlag:this.state.editFlag,
            buttons:[{
              title:'Save',
              onClick:this.handleClickSave,
              id:'default_save_button'
            }]
          };

        return (

            <Grid container style={{height:'100%'}}>
                <Grid item xs style={{flex:'0 0 auto',width:'30%',background:'#ccc',height:'100%'}}>

                    <Box ref={this.historyContent} title={'Dx/Px History'} height={'50%'}>
                        <div id="historyForm">
                            <FormControl
                                className={classes.formControl}
                            >
                                <Grid container spacing={2} style={{padding:'6px 12px'}}>
                                    <Grid item xs={6}>
                                        <FormControl style={{width:'100%'}}>
                                            <InputLabel>Service</InputLabel>
                                            <SelectComponent
                                                className={
                                                    classes.select_user
                                                }
                                                id={
                                                    'DxPx_ServiceTypeSelectField'
                                                }
                                                onChange={
                                                    this.clinicNoteServiceValueOnChange
                                                }
                                                options={service.map(
                                                    item => ({
                                                        value:item.serviceCd,
                                                        title: item.serviceCd
                                                    })
                                                )}
                                                value={
                                                    this.state.service
                                                }
                                                classes={{select:classes.select_padding}}
                                            />
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <FormControl style={{width:'100%'}}>
                                        <InputLabel>Record Type</InputLabel>
                                        <SelectComponent
                                            className={
                                                classes.select_user
                                            }
                                            id={
                                                'DxPx_RecordTypeSelectField'
                                            }
                                            onChange={
                                                this
                                                    .clinicNoteRecordTypeValueOnChange
                                            }
                                            options={this.props.recordTypeList.map(
                                                item => ({
                                                    value:
                                                        item.diagnosisTypeCd,
                                                    title:
                                                        item.diagnosisTypeDesc ===
                                                        'Problem'
                                                            ? 'Diagnosis'
                                                            : item.diagnosisTypeDesc
                                                })
                                            )}
                                            value={
                                                this.state
                                                    .recordType
                                            }
                                            classes={{select:classes.select_padding}}
                                        />
                                        </FormControl>
                                    </Grid>
                                </Grid>
                            </FormControl>
                        </div>

                        <List data={this.state.historicalRecordList}
                            wrapperContentHeight={this.state.historyHeight}
                            onSelectionChange={this.changeMedicalRecord}
                            components={{
                                Row: props => {
                                    return (
                                        <MTableBodyRow
                                            {...props}
                                            draggable="true"
                                            // onDragStart={(e)=>{this.handleDiagnosisDragStart(e,props.data);}}
                                            onDragEnd={this.dragMedicalRecordEnd}
                                            onDragStart={(e)=>{this.dragMedicalRecord(e,props.data);}}
                                        />
                                    );
                                }
                              }}

                        />
                    </Box>

                    <Box title={'Record Details'} height={'50%'}>
                        <Typography
                            className={classes.table}
                            component="div"
                            id="textRecordWhite"
                            style={{
                                height: '100%',
                                // height: 'calc(90vh - 512px)',
                                minHeight: '275px',
                                // color: '#3f51b5',
                                position: 'relative',
                                display:
                                    this.state.isShowRecordDetailDiv ===
                                    'none'
                                        ? 'block'
                                        : 'none'
                            }}
                        />
                        <Typography
                            className={classes.table}
                            component="div"
                            id="textRecord"
                            style={{
                                height: '100%',
                                // height: 'calc(90vh - 512px)',
                                minHeight: '286px',
                                // color: '#3f51b5',
                                position: 'relative',
                                display: this.state
                                    .isShowRecordDetailDiv
                            }}
                        >

                            <Typography
                                className={classes.recordDetailFont}
                                component="div"
                                style={{
                                    paddingLeft: '9px',
                                    fontStyle: 'normal',
                                    fontSize: '1rem',
                                    fontWeight: 'bold'
                                }}
                            >
                                {this.state.selectedHistoricalRecord.recordType === 'PX'?'Px:':'Dx:'}
                                &nbsp;
                                {this.state.selectedHistoricalRecord
                                    .diagnosisText !== undefined
                                    ? this.state
                                          .selectedHistoricalRecord
                                          .diagnosisText
                                    : ''}
                            </Typography>

                            <Typography
                                className={classes.recordDetailFont}
                                component="div"
                                style={{
                                    paddingLeft: '8px',
                                    fontStyle: 'normal',
                                    fontSize: '1rem',
                                    fontWeight: 'bold'
                                }}
                            >
                                <span
                                    style={{
                                        fontStyle: 'normal',
                                        fontSize: '1rem',
                                        fontWeight: 'bold'
                                    }}
                                >
                                    Status:{' '}
                                </span>
                                <span
                                    style={{
                                        fontStyle: 'normal',
                                        fontSize: '1rem',
                                        fontWeight: 'bold',
                                        display:
                                            this.state
                                                .isShowRecordDetailText ===
                                            'none'
                                                ? 'contents'
                                                : 'none'
                                    }}
                                >
                                    {this.state.statusValue}
                                </span>
                                <FormControl
                                    className={
                                        classes.SelectFormControl
                                    }
                                    style={{
                                        display:
                                            this.state
                                                .isShowRecordDetailText ===
                                            'block'
                                                ? 'contents'
                                                : 'none'
                                    }}
                                >
                                    <Select
                                        sytle={{
                                            fontSize: '1rem',
                                            fontFamily: 'Arial'
                                        }}
                                        displayEmpty
                                        name="age"
                                        onChange={this.handleChange}
                                        SelectDisplayProps={MenuProps}
                                        value={this.state.status}
                                    >
                                        {this.state.statusList.map(
                                            (item, index) => (
                                                <MenuItem
                                                    classes={{
                                                        root:
                                                            classes.list_title
                                                    }}
                                                    key={index}
                                                    value={
                                                        item.codeDiagnosisStatusCd
                                                    }
                                                >
                                                    {
                                                        item.diagnosisStatusDesc
                                                    }
                                                </MenuItem>
                                            )
                                        )}
                                    </Select>
                                </FormControl>
                            </Typography>

                            <Typography
                                className={classes.recordDetailFont}
                                component="div"
                                style={{
                                    float: 'left',
                                    marginTop: '2px',
                                    paddingLeft: '9px',
                                    fontStyle: 'normal',
                                    fontWeight: 'bold'
                                }}
                            >
                                <span style={{ fontSize: '1rem' }}>
                                    Remark:
                                </span>
                            </Typography>

                            <Typography
                                component="div"
                                style={{
                                    paddingLeft: 10,
                                    marginTop: 3,
                                    marginLeft: 65,
                                    paddingTop: 2,
                                    fontWeight: '600',
                                    fontSize: '1rem',
                                    height: '43%',
                                    width: '77%',
                                    border: 'none',
                                    resize: 'none',
                                    userSelect: 'none',
                                    whiteSpace: 'pre-wrap',
                                    wordBreak: 'break-all',
                                    display:
                                        this.state
                                            .isShowRecordDetailText ===
                                        'none'
                                            ? 'block'
                                            : 'none',
                                    overflow: 'auto'
                                }}
                            >
                                {this.state.remarks}
                            </Typography>

                            <textarea
                                onChange={this.onEdit}
                                ref="myTA"
                                style={{
                                    paddingLeft: 10,
                                    marginTop: 5,
                                    marginLeft: 65,
                                    // marginLeft:4,
                                    paddingTop: 1,
                                    color: 'rgba(0, 0, 0, 0.7)',
                                    fontWeight: '600',
                                    fontFamily: 'Arial',
                                    fontSize: '1rem',
                                    height: '43%',
                                    width: '77%',
                                    resize: 'none',
                                    whiteSpace: 'pre-wrap',
                                    wordBreak: 'break-all',
                                    display: this.state
                                        .isShowRecordDetailText
                                }}
                                value={this.state.remarks}
                            />
                            {/* </Typography> */}

                            {/* isNew */}
                            {this.state.selectedHistoricalRecord.recordType === TABLE_CONTENT_TYPE.DIAGNOSIS?(
                                <Typography
                                    className={classes.recordDetailFont}
                                    component="div"
                                    style={{
                                        paddingLeft: '8px',
                                        fontStyle: 'normal',
                                        fontSize: '1rem',
                                        fontWeight: 'bold'
                                    }}
                                >
                                    <div style={{float:'left'}}>New:</div>
                                    <div>
                                        <Checkbox
                                            className={classes.historyCheckbox}
                                            id="checkbox_diagnosis_history_detail_new"
                                            color="primary"
                                            checked={!!this.state.isNew}
                                            onChange={this.handleDiagnosisHistoryDetailChange}
                                        />
                                    </div>
                                </Typography>
                            ):null}

                            <Typography
                                component="div"
                                style={{ clear: 'both' }}
                            />

                            <Typography
                                className={classes.recordDetailFont}
                                component="div"
                                style={{
                                    paddingTop: 2,
                                    paddingLeft: '8px',
                                    fontStyle: 'normal',
                                    fontSize: '1rem',
                                    fontWeight: 'bold'
                                }}
                            >
                                Last Updated: &nbsp;
                                {(this.state.selectedHistoricalRecord
                                    .updatedByName !== undefined
                                    ? this.state
                                          .selectedHistoricalRecord
                                          .updatedByName
                                    : '')+
                                    '('+(this.state.selectedHistoricalRecord
                                        .updatedDtm !== undefined
                                        ? moment(
                                              this.state
                                                  .selectedHistoricalRecord
                                                  .updatedDtm
                                          ).format('DD-MMM-YYYY')
                                        : '')+')'}
                            </Typography>
                            <BottomNavigation showLabels style={{justifyContent:'flex-end'}}>
                                <BottomNavigationAction style={{maxWidth:96}} disabled={this.state.isShow!=='none'?false:true} label="Delete" icon={<DeleteOutlined />} onClick={this.deleteHistoricalRecord}/>
                                <BottomNavigationAction style={{maxWidth:96}} disabled={this.state.isShow!=='none'?false:true} label="Save"  icon={<SaveOutlined />} onClick={this.saveHistoricalRecord}/>
                                <BottomNavigationAction style={{maxWidth:96}} label="Copy"  icon={<FileCopyOutlined />} onClick={this.copyHistoricalRecord}/>
                            </BottomNavigation>
                        </Typography>
                    </Box>

                </Grid>
                <Container item container direction={'column'} justify={'space-between'} alignItems={'stretch'} buttonBar={buttonBar}>
                    {/* input problem  */}
                    <div style={{minHeight:'600px',height:'100%',overflowY:'auto'}}>
                        <Grid item container direction={'column'} wrap={'nowrap'} ref={this.boxContent} style={{padding:'8px',flex:'auto',height:'50%'}}>
                            <Grid item style={{flex:'0 0 auto'}}>
                                <Avatar
                                    className={
                                        classes.diagnosisAvatar
                                    }
                                    style={{
                                        fontSize: '13px',
                                        fontFamily: 'Arial',
                                        textAlign: 'center',
                                        width: '21px',
                                        height: '21px',
                                        float: 'left'
                                    }}
                                >
                                    Dx
                                </Avatar>
                                <span
                                    className={classes.list_title}
                                    style={{ float: 'left' }}
                                >
                                    &nbsp;Diagnosis
                                </span>
                            </Grid>
                            <Grid container item style={{border:'1px solid #949494',padding:'6px', flex:'auto',position:'relative',paddingBottom: 0}}>
                                <ButtonGroup
                                    orientation="vertical"
                                    color="primary"
                                    aria-label="vertical outlined primary button group"
                                    style={{
                                        position:'absolute',
                                        left:'calc(50% - 20px)',
                                        top:'64px',
                                        bottom:'22px',
                                        background:'#fff',
                                        border:'1px solid #e9e9e9',
                                        boxSizing:'border-box',
                                        zIndex:99,
                                        borderRadius:0,
                                        flexDirection: 'column',
                                        boxShadow: 'rgba(0, 0, 0, 0.2) 0px 3px 1px -2px, rgba(0, 0, 0, 0.14) 0px 4px 2px -2px, rgba(0, 0, 0, 0.12) 0px 1px 2px -2px'
                                    }}
                                >
                                    <IconButton size="small" style={{height:'50%',borderRadius:0,minWidth:'38px'}} onClick={this.copyDiagnosisSelectionToChronicProblemTable}>
                                        <ArrowForwardIos fontSize="inherit" />
                                    </IconButton>
                                    <IconButton size="small" style={{height:'50%',borderRadius:0,minWidth:'38px',paddingLeft:'8px'}} onClick={this.copyChronicProblemSelectionToDiagnosisTable}>
                                        <ArrowBackIos fontSize="inherit" />
                                    </IconButton>
                                </ButtonGroup>
                                <Grid item xs={6} style={{padding:'6px 16px 6px 6px',marginTop:'-6px'}} onDragOver={this.onProblemDragOver} onDrop={this.onProblemDrop} >
                                    <Grid
                                        alignItems="center"
                                        container
                                        direction="row"
                                        // justify="space-between"
                                    >
                                        <Grid item lg={8} md={12} sm={12}>
                                            <FuzzySearchBox
                                                dataList={
                                                    searchProblemRecordList
                                                }
                                                displayField={[
                                                    'termDisplayName'
                                                ]}
                                                handleSearchBoxLoadMoreRows={
                                                    this
                                                        .handleProblemSearchBoxLoadMoreRows
                                                }
                                                id={PROBLEM_MODE}
                                                inputPlaceHolder={
                                                    'Search Diagnosis'
                                                }
                                                // limitValue={4}
                                                onChange={
                                                    this
                                                        .handleProblemFuzzySearch
                                                }
                                                onSelectItem={this.handleProblemSelectItem.bind(
                                                    this
                                                )}
                                                totalNums={
                                                    searchProblemRecordTotalNums
                                                }
                                                {...searchAddProblem}
                                                closeSearchData={this.closeSearchData.bind(
                                                    this
                                                )}
                                                handleAddSearchData={
                                                    this
                                                        .handleAddSearchData
                                                }
                                            />
                                        </Grid>
                                        <Grid item lg={4} md={12} sm={12}>
                                            <CIMSButton
                                                id="btn_inputProblem_favourite"
                                                onClick={
                                                    this.handleProblemServiceFavouriteDialogOpen
                                                }
                                                variant="contained"
                                                color="primary"
                                                className={classes.btnDiagnosisSerivceFavourite}
                                            >
                                                {PROBLEM_SEARCH_PROBLEM}
                                            </CIMSButton>
                                        </Grid>
                                    </Grid>

                                    <ServiceFavouriteDialog {...problemFavouriteDialogProps} />
                                    <JTable
                                        size="small"
                                        id="dxpx_diagnosis_table"
                                        actions={[
                                            {
                                                icon: () => (
                                                    <DeleteOutline id="btn_diagnosis_row_delete" />
                                                ),
                                                onClick: (event,rowData) => {
                                                    this.deleteProblemObj(
                                                        rowData,'btn_diagnosis_row_cancel'
                                                    );
                                                }
                                            }
                                        ]}
                                        columns={this.state.problemColumns}
                                        components={{
                                            Action: props => {
                                                if (props.action.tooltip ==='Cancel') {
                                                    return (
                                                        <IconButton
                                                            className={classes.iconBtn}
                                                            id="btn_diagnosis_row_cancel"
                                                            onClick={event => {
                                                                this.handleActionCancel(event,props);
                                                            }}
                                                        >
                                                            <Clear />
                                                        </IconButton>
                                                    );
                                                } else if (props.action.tooltip ==='Save') {
                                                    return (
                                                        <IconButton
                                                            id="btn_diagnosis_row_save"
                                                            className={classes.iconBtn}
                                                            onClick={event => {
                                                                this.handleActionSave(event,props);
                                                            }}
                                                        >
                                                            <Check />
                                                        </IconButton>
                                                    );
                                                } else {
                                                    return (
                                                        <MTableAction {...props} />
                                                    );
                                                }
                                            },
                                            OverlayLoading: () => (
                                                <div />
                                            ),
                                            Row: props => {
                                                return (
                                                    <MTableBodyRow
                                                        {...props}
                                                        draggable="true"
                                                        onDragStart={(e)=>{this.handleDiagnosisDragStart(e,props.data);}}
                                                    />
                                                );
                                            }
                                        }}
                                        data={this.state.problemDataList}
                                        editable={{
                                            onRowUpdate: (
                                                newData,
                                                oldData
                                            ) =>
                                                new Promise(
                                                    resolve => {
                                                        setTimeout(
                                                            () => {
                                                                this.updateProblemObj(
                                                                    newData,
                                                                    oldData
                                                                );
                                                                resolve();
                                                            },
                                                            100
                                                        );
                                                    }
                                                )
                                        }}
                                        icons={{
                                            Edit: props => (
                                                <EditRounded
                                                    id="btn_diagnosis_row_edit"
                                                    {...props}
                                                />
                                            )

                                        }}
                                        localization={{
                                            body: {
                                                emptyDataSourceMessage:
                                                    'There is no data.',
                                                editRow: {
                                                    deleteText:
                                                        'Are you sure delete this record?'
                                                }
                                            },
                                            header: {
                                                actions: 'Action'
                                            }
                                        }}
                                        options={{
                                            actionsColumnIndex: -1,
                                            maxBodyHeight:this.state.height,
                                            draggable: false,
                                            selection:'termId',
                                            headerStyle:{ color: '#ffffff',backgroundColor:COMMON_STYLE.TABLE_BGCKGROUNDCOLOR,fontWeight:600,fontSize:'1rem',fontFamily:'Arial'},
                                            actionsCellStyle: {
                                                direction:'rtl',
                                                fontSize: '1rem'
                                            }
                                        }}
                                        onSelectionChange={this.diagnosisSelectionChange}
                                        selected={this.state.diagnosisSelectionData}
                                    />
                                </Grid>
                                <Grid item xs={6} style={{padding:'6px 6px 6px 16px',marginTop:'-6px'}} onDragOver={this.handleChronicProblemDragOver} onDrop={this.handleChronicProblemDrop}>
                                    <div style={{height:'59px',lineHeight:'59px',fontWeight:'bold'}}>
                                        Chronic Problem
                                    </div>
                                    <JTable
                                        size="small"
                                        id="dxpx_chronic_problem_table"
                                        actions={[
                                            {
                                                icon: () => (
                                                    <DeleteOutline id="btn_chronic_problem_row_delete" />
                                                ),
                                                onClick: (event,rowData) => {
                                                    this.handleChronicProblemRowDelete(rowData);
                                                }
                                            }
                                        ]}
                                        columns={chronicProblemColumns}
                                        components={{
                                            Action: props => {
                                                if (props.action.tooltip ==='Cancel') {
                                                    return (
                                                        <IconButton
                                                            id="btn_chronic_problem_row_cancel"
                                                            className={classes.iconBtn}
                                                            onClick={event => {
                                                                this.handleActionCancel(event,props);
                                                            }}
                                                        >
                                                            <Clear />
                                                        </IconButton>
                                                    );
                                                } else if (props.action.tooltip === 'Save') {
                                                    return (
                                                        <IconButton
                                                            id="btn_chronic_problem_row_save"
                                                            className={classes.iconBtn}
                                                            onClick={event => {
                                                                this.handleActionSave(event,props);
                                                            }}
                                                        >
                                                            <Check />
                                                        </IconButton>
                                                    );
                                                } else {
                                                    return (
                                                        <MTableAction {...props} />
                                                    );
                                                }
                                            },
                                            OverlayLoading: () => (
                                                <div />
                                            ),
                                            Row: props => {
                                                return (
                                                    <MTableBodyRow
                                                        {...props}
                                                        draggable="true"
                                                        onDragEnd={this.handleChronicProblemDragEnd}
                                                        onDragStart={(e)=>{this.handleChronicProblemDragStart(e,props.data);}}
                                                    />
                                                );
                                            }
                                        }}
                                        data={this.state.chronicProblemDataList}
                                        editable={{
                                            onRowUpdate: (newData,oldData) =>{
                                                return this.handleChronicProblemRowUpdate(newData,oldData);
                                            }
                                        }}
                                        icons={{
                                            Edit: props => (
                                                <EditRounded
                                                    id="btn_chronic_problem_row_edit"
                                                    {...props}
                                                />
                                            )
                                        }}
                                        localization={{
                                            body: {
                                                emptyDataSourceMessage:'There is no data.',
                                                editRow: {
                                                    deleteText:'Are you sure delete this record?'
                                                }
                                            },
                                            header: {
                                                actions: 'Action'
                                            }
                                        }}
                                        options={{
                                            actionsColumnIndex: -1,
                                            maxBodyHeight:this.state.height,
                                            draggable: false,
                                            selection:'termId',
                                            headerStyle:{ color: '#ffffff',backgroundColor:COMMON_STYLE.TABLE_BGCKGROUNDCOLOR,fontWeight:600,fontSize:'1rem',fontFamily:'Arial'},
                                            actionsCellStyle: {
                                                direction:'rtl',
                                                fontSize: '1rem'
                                            }
                                        }}
                                        onSelectionChange={this.chronicProblemSelectionChange}
                                        selected={this.state.chronicProblemSelectionData}
                                    />
                                </Grid>
                            </Grid>
                        </Grid>

                        {/* input Procedure  */}
                        <Grid item container direction={'column'} wrap={'nowrap'} style={{padding:'8px',flex:'auto',height:'50%',paddingBottom:0,minWidth:683}}>
                            <Grid item style={{flex:'0 0 auto'}}>
                                <Avatar
                                    className={
                                        classes.procedureAvatar
                                    }
                                    style={{
                                        fontSize: '13px',
                                        fontFamily: 'Arial',
                                        textAlign: 'center',
                                        width: '21px',
                                        height: '21px',
                                        float: 'left'
                                    }}
                                >
                                    Px
                                </Avatar>
                                <span
                                    className={classes.list_title}
                                    style={{ float: 'left' }}
                                >
                                    &nbsp;Procedure
                                </span>
                            </Grid>

                            <Grid container item style={{border:'1px solid #949494',padding:'12px',flex:'auto'}}>
                                <Grid id="historicalRecordProcedureDiv" style={{marginTop:'-6px',minWidth:527}} item md={12} onDragOver={this.onProcedureDragOver} onDrop={this.onProcedureDrop}>
                                    <Grid
                                        alignItems="center"
                                        container
                                        direction="row"
                                        justify="space-between"
                                    >
                                        <FuzzySearchBox
                                            dataList={
                                                searchProcedureRecordList
                                            }
                                            displayField={[
                                                'termDisplayName'
                                            ]}
                                            handleSearchBoxLoadMoreRows={
                                                this
                                                    .handleProcedureSearchBoxLoadMoreRows
                                            }
                                            id={PROCEDURE_MODE}
                                            inputPlaceHolder={
                                                'Search Procedure'
                                            }
                                            onChange={
                                                this
                                                    .handleProcedureFuzzySearch
                                            }
                                            onSelectItem={this.handleProcedureSelectItem.bind(
                                                this
                                            )}
                                            totalNums={
                                                searchProcedureRecordTotalNums
                                            }
                                            {...searchAddProcedure}
                                            closeSearchData={this.closeSearchData.bind(
                                                this
                                            )}
                                            handleAddSearchData={
                                                this.handleAddSearchData
                                            }
                                        />
                                        <CIMSButton
                                            id="btn_inputProcedure_favourite"
                                            onClick={
                                                this
                                                    .handleProcedureServiceFavouriteDialogOpen
                                            }
                                            variant="contained"
                                            color="primary"
                                        >
                                            {PROCEDURE_SEARCH_PROBLEM}
                                        </CIMSButton>
                                    </Grid>
                                    <ServiceFavouriteDialog {...procedureFavouriteDialogProps} />

                                    <JTable
                                        size="small"
                                        id="dxpx_procedure_table"
                                        actions={[
                                            {
                                                icon: () => (
                                                    <DeleteOutline id="btn_procedure_row_delete" />
                                                ),
                                                onClick: (
                                                    event,
                                                    rowData
                                                ) => {
                                                    this.deleteProcedureObj(
                                                        rowData
                                                    );
                                                }
                                            }
                                        ]}
                                        columns={
                                            this.state
                                                .procedureColumns
                                        }
                                        components={{
                                            Action: props => {
                                                if (
                                                    props.action
                                                        .tooltip ===
                                                    'Cancel'
                                                ) {
                                                    return (
                                                        <IconButton
                                                            className={
                                                                classes.iconBtn
                                                            }
                                                            id="btn_procedure_row_cancel"
                                                            onClick={event => {
                                                                this.handleActionCancel(
                                                                    event,
                                                                    props
                                                                );
                                                            }}
                                                        >
                                                            <Clear />
                                                        </IconButton>
                                                    );
                                                } else if (
                                                    props.action
                                                        .tooltip ===
                                                    'Save'
                                                ) {
                                                    return (
                                                        <IconButton
                                                            id="btn_procedure_row_save"
                                                            className={
                                                                classes.iconBtn
                                                            }
                                                            onClick={event => {
                                                                this.handleActionSave(
                                                                    event,
                                                                    props
                                                                );
                                                            }}
                                                        >
                                                            <Check />
                                                        </IconButton>
                                                    );
                                                } else {
                                                    return (
                                                        <MTableAction
                                                            {...props}
                                                        />
                                                    );
                                                }
                                            },
                                            OverlayLoading: props => (
                                                <div />
                                            )
                                        }}
                                        data={
                                            this.state
                                                .procedureDataList
                                        }
                                        editable={{
                                            onRowUpdate: (
                                                newData,
                                                oldData
                                            ) =>
                                                new Promise(
                                                    resolve => {
                                                        setTimeout(
                                                            () => {
                                                                this.updateProcedureObj(
                                                                    newData,
                                                                    oldData
                                                                );
                                                                resolve();
                                                            },
                                                            100
                                                        );
                                                    }
                                                )
                                        }}
                                        icons={{
                                            Edit: props => (
                                                <EditRounded
                                                    id="btn_procedure_row_edit"
                                                    {...props}
                                                />
                                            )
                                        }}
                                        localization={{
                                            body: {
                                                emptyDataSourceMessage:
                                                    'There is no data.',
                                                editRow: {
                                                    deleteText:
                                                        'Are you sure delete this record?'
                                                }
                                            },
                                            header: {
                                                    actions: 'Action'
                                                }
                                        }}
                                        options={{
                                            actionsColumnIndex: -1,
                                            maxBodyHeight:this.state.height,
                                            draggable: false,
                                            headerStyle:{ color: '#ffffff',backgroundColor:COMMON_STYLE.TABLE_BGCKGROUNDCOLOR,fontWeight:600,fontSize:'1rem',fontFamily:'Arial'},
         									                  rowStyle: {
                                                wordBreak:'break-all',
                                                height: '30px'
                                            },
                                            actionsCellStyle: {
                                                direction:'rtl',
                                                fontSize: '1rem'
                                            }
    									                  }}
                                    />
                                </Grid>
                            </Grid>
                        </Grid>
                    </div>
                </Container>
            </Grid>
        );
    }
}

const mapDispatchToProps = {
    openCommonMessage,
    closeCommonMessage,
    saveRecordDetail,
    deleteRecordDetail,
    openCommonCircularDialog,
    getInputProblemList,
    getInputProcedureList,
    searchProcedureList,
    getProcedureCodeDiagnosisStatusList,
    updatePatientProcedure,
    deletePatientProcedure,
    getProblemCodeDiagnosisStatusList,
    updatePatientProblem,
    deletePatientProblem,
    listCodeDiagnosisTypes,
    getHistoricalRecords,
    saveInputProcedure,
    saveInputProblem,
    closeCommonCircularDialog,
    queryProblemList,
    queryProcedureList,
    requestProcedureTemplateList,
    requestProblemTemplateList,
    savePatient,
    getChronicProblemList,
    deleteSubTabs
};
function mapStateToProps(state) {
    return {
        loginInfo: {
            ...state.login.loginInfo,
            service: {
                code: state.login.service.serviceCd
            }
        },
        encounterData:state.patient.encounterInfo,
        patientPanelInfo: state.patient.patientInfo,
        service: state.common.serviceList,
        inputProcedureList: state.procedureReducer.inputProcedureList,
        inputProblemList: state.diagnosisReducer.inputProblemList,
        recordTypeList: state.diagnosisReducer.recordTypeList,
        sysConfig: state.clinicalNote.sysConfig
    };
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(HistoricalRecord));
