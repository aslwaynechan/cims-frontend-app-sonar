
export const styles = theme => ({
  left_warp: {
    padding: 5,
    backgroundColor: 'lightgray',
    height: 'calc(100vh - 175px)',
    minHeight: 710,
    // minWidth:339,
    // overflowX:'hidden',
    border:'1px solid #000'
  },
  title: {
    fontSize: '1rem',
    fontWeight: 600,
    fontFamily:'Arial',
    marginLeft: 10
  },
  table: {
    backgroundColor: '#fff',
    border: '1px solid rgba(0,0,0,0.5)',
    marginBottom: 5,
    overflowY: 'auto'
  },
  table_header: {
    fontSize: '1rem',
    fontWeight: 600,
    fontFamily:'Arial',
    color: 'white',
    padding: '0, 0, 0, 10'
  },
  formControl: {
    width: '100%'
  },
  list_title: {
    fontSize: '1rem',
    fontWeight: 600,
    fontFamily:'Arial'
  },
  select_user:
  {
    color: 'rgba(0, 0, 0, 0.7)',
    width:'calc(100%-20px)',
    fontWeight: 100
    // padding:5
  },
  table_head: {
    height: 31,
    fontSize:'13px',
    fontWeight:'bold'
  },
  table_row_selected: {
    height: 31,
    cursor: 'pointer',
    backgroundColor: 'lightgoldenrodyellow'
  },
  table_row: {
    height: 31,
    cursor: 'pointer'
  },
  SelectFormControl:{
    marginTop:-5,
    minWidth: 120
  },
  clinical_note_box: {
    border: '1px solid rgba(0,0,0,0.42)',
    height: 'calc(45vh - 100px)',
    minHeight: 311,
    width: 'calc(100% - 46px)',
    padding: 15,
    marginLeft: 10,
    overflow:'auto',
    paddingTop:0,
    paddingBottom:0
    // minWidth:1009
  },
  searchHead:{
    paddingBottom:2,
    marginTop:5,
    position:'sticky',
    top:0,
    backgroundColor:'white',
    zIndex:'10'
  },
  button:{
    textTransform:'none',
    lineHeight:'inherit',
    marginLeft:6
  },
  materialTable:{
    overflow:'scroll',
    tableLayout:'fixed',
    wordBreak:'break-all'
 },
 selectLabel: {
  fontSize: '1rem',
  float: 'left',
  paddingTop: '3px',
  paddingRight: '10px',
  fontFamily: 'Arial',
  fontWeight: 'bold',
  marginLeft:6
},
person_info_div:{
  width:'100%'
},
procedureAvatar: {
  color: '###F8D186',
  backgroundColor: '#F8D186'
},
diagnosisAvatar: {
  color: '#fff',
  backgroundColor: '#38d1ff'
},
test:{
  position:'static'
},
materialStyle:{
  Button:{
    root:{
      position: 'static'
    }
  },
  overflowY:'auto',
  height:236
},
iconBtn:{
  color: 'inherit'
},

recordDetailFont: {
  fontSize: '12pt',
  fontWeight: 600,
  fontFamily:'Arial',
  padding: '0, 0, 0, 10'
},
tableCell : {
  fontSize:'1rem',
  fontFamily:'Arial',
  whiteSpace:'inherit'
},
buttonSpan:{
  fontSize:'1rem'
},
record_title: {
  fontSize: '1rem',
  fontWeight: 600,
  fontFamily:'Arial',
  padding: '0, 0, 0, 10'
},
titlePaddingTop: {
  paddingTop: 10
},
historyCheckbox: {
  padding: '0 10px'
},
btnDiagnosisSerivceFavourite: {
  [theme.breakpoints.between('xs', 'md')]: {
    float: 'left'
  },
  float: 'right'
},
select_padding:{
  padding:0
}

});