export const styles = () => ({
  paper: {
    minWidth: 800
  },
  dialogTitle: {
    lineHeight: 1.6,
    fontWeight: 'bold',
    fontSize: '1.5rem',
    paddingTop: '5px',
    paddingBottom: '5px',
    backgroundColor: '#b8bcb9'
  },
  dialogContent: {
    padding: '0 24px 10px',
    backgroundColor: 'white',
    '&:first-child': {
      paddingTop: 0
    }
  },
  grid_xs_12: {
    marginTop: '2vh'
  },
  dialogActions: {
    margin:'0px' ,
    padding:'10px' ,
    backgroundColor:'white'
  },
  gridBorder: {
    backgroundColor:'#b8bcb9',
    padding: '0px 10px 10px'
  },
  fontLabel: {
    fontFamily: 'Arial',
    fontSize: '1rem'
  }
});