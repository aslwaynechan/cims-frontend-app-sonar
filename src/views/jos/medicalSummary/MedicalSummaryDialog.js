/*
 * Front-end UI for insert/update medical summary
 * Save Action: [MedicalSummaryDialog.js] Save -> handleSave
 * -> [medicalSummaryAction.js] updateMedicalSummary
 * -> [medicalSummarySaga.js] updateMedicalSummary
 * -> Backend API = medical-summary/saveMedicalSummary
 */
import React, { Component } from 'react';
import { Dialog, DialogContent, Divider, DialogActions, Grid, DialogTitle, withStyles } from '@material-ui/core';
import TextBox from './components/TextBox/TextBox';
import CIMSButton from '../../../components/Buttons/CIMSButton';
import {styles} from './medicalSummaryDialogStyle';
import CIMSGrid from '../../../components/Grid/CIMSGrid';
import SocialHistory from './components/SocialHistory/SocialHistory';
import { connect } from 'react-redux';
import {updateMedicalSummary} from '../../../store/actions/medicalSummary/medicalSummaryAction';
import {openCommonMessage} from '../../../store/actions/message/messageAction';
import {cloneDeep} from 'lodash';
import { MEDICAL_SUMMARY_TYPE } from '../../../constants/medicalSummary/medicalSummaryConstants';
import {MEDICAL_SUMMARY_CODE} from '../../../constants/message/medicalSummaryCode';
import {openCommonCircularDialog} from '../../../store/actions/common/commonAction';
import Draggable from 'react-draggable';
import Paper from '@material-ui/core/Paper';


function PaperComponent(props) {
  return (
    <Draggable enableUserSelectHack={false}
        onStart={(e)=>{
          return e.target.getAttribute('customdrag') === 'allowed';
        }}
    >
      <Paper {...props} />
    </Draggable>
  );
}

class MedicalSummaryDialog extends Component {
  constructor(props){
    super(props);
    this.state={
      isEdit:false,
      pastHistoryValDtos:[],
      smokingHistoryValDtos:[],
      drinkingHistoryValDtos:[],
      substanceAbuseHistoryValDtos:[],
      occupationalHistoryValDtos:[],
      familyHistoryValDtos:[],
      remarkValDtos:[]
    };
  }

  componentDidMount(){
    const {
      pastHistoryValDtos,
      smokingHistoryValDtos,
      drinkingHistoryValDtos,
      substanceAbuseHistoryValDtos,
      occupationalHistoryValDtos,
      familyHistoryValDtos,
      remarkValDtos
    } = this.props;

    this.initValDtos(pastHistoryValDtos,'pastHistoryValDtos');
    this.initValDtos(smokingHistoryValDtos,'smokingHistoryValDtos');
    this.initValDtos(drinkingHistoryValDtos,'drinkingHistoryValDtos');
    this.initValDtos(substanceAbuseHistoryValDtos,'substanceAbuseHistoryValDtos');
    this.initValDtos(occupationalHistoryValDtos,'occupationalHistoryValDtos');
    this.initValDtos(familyHistoryValDtos,'familyHistoryValDtos');
    this.initValDtos(remarkValDtos,'remarkValDtos');
  }

  initValDtos = (dtos,name) => {
    let tempValDtos = [];
    if (dtos.length>0) {
      tempValDtos = cloneDeep(dtos);
    } else {
      //default
      tempValDtos.push({
        medicalSummaryId:'',
        medicalSummaryStatus: '',
        codeMedicalSummaryType: '',
        medicalSummaryText: '',
        version: null,
        rowId: null,
        createdBy: null,
        createdDtm: null
      });
    }
    this.setState({
      [name]:tempValDtos
    });
  }

  updateState=(obj)=>{
    this.setState({
      ...obj
    });
  }

  generateResultDtos = (resultDtos,valDtos,codeMedicalSummaryCd) => {
    resultDtos.push({
      codeMedicalSummaryCd,
      medicalSummaryValueDto:valDtos
    });
  }

  handleSave=()=>{
    let {patientKey,serviceCd,handleDialogCancel} = this.props;
    let {
      pastHistoryValDtos,
      smokingHistoryValDtos,
      drinkingHistoryValDtos,
      substanceAbuseHistoryValDtos,
      occupationalHistoryValDtos,
      familyHistoryValDtos,
      remarkValDtos
    } = this.state;
    let resultObj = {
      patientKey,
      serviceCd,
      medicalSummaryResultDtos:[]
    };
    this.generateResultDtos(resultObj.medicalSummaryResultDtos,pastHistoryValDtos,MEDICAL_SUMMARY_TYPE.PAST_MEDICAL_HISTORY);
    this.generateResultDtos(resultObj.medicalSummaryResultDtos,smokingHistoryValDtos,MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SMOKING);
    this.generateResultDtos(resultObj.medicalSummaryResultDtos,drinkingHistoryValDtos,MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_DRINKING);
    this.generateResultDtos(resultObj.medicalSummaryResultDtos,substanceAbuseHistoryValDtos,MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SUBSTANCE_ABUSE);
    this.generateResultDtos(resultObj.medicalSummaryResultDtos,occupationalHistoryValDtos,MEDICAL_SUMMARY_TYPE.OCCUPATIONAL_HISTORY);
    this.generateResultDtos(resultObj.medicalSummaryResultDtos,familyHistoryValDtos,MEDICAL_SUMMARY_TYPE.FAMILY_HISTORY);
    this.generateResultDtos(resultObj.medicalSummaryResultDtos,remarkValDtos,MEDICAL_SUMMARY_TYPE.REMARK);
    this.props.openCommonCircularDialog();
    this.props.updateMedicalSummary({
      params:resultObj,
      callback:(data)=>{
        handleDialogCancel&&handleDialogCancel();
        let payload = {
          msgCode:data.msgCode,
          showSnackbar:true,
          btnActions: {
          }
        };
        this.props.openCommonMessage(payload);
      }
    });
  }

  handleCancel=()=>{
    const {handleDialogCancel} = this.props;
    let {isEdit} = this.state;
    if (isEdit) {
      let payload = {
        msgCode: MEDICAL_SUMMARY_CODE.CLOSE_DIALOG_COMFIRM,
        btnActions: {
          // Yes
          btn1Click: () => {
            handleDialogCancel&&handleDialogCancel();
          }
        }
      };
      this.props.openCommonMessage(payload);
    } else {
      handleDialogCancel&&handleDialogCancel();
    }
  }


  render() {
    const {
      classes,
      isOpen=false,
      smokingDropMap,
      drinkingDropMap,
      substanceAbuseDropMap
    } = this.props;
    let {
      pastHistoryValDtos,
      smokingHistoryValDtos,
      drinkingHistoryValDtos,
      substanceAbuseHistoryValDtos,
      occupationalHistoryValDtos,
      familyHistoryValDtos,
      remarkValDtos
    } = this.state;

    let pastHistoryProps = {
      labelText: 'Past Medical History',
      pastHistoryValDtos,
      codeMedicalSummaryCd: 'P',
      updateState:this.updateState
    };
    let socialHistoryProps = {
      labelText: 'Social History',
      smokingDropMap,
      drinkingDropMap,
      substanceAbuseDropMap,
      smokingHistoryValDtos,
      drinkingHistoryValDtos,
      substanceAbuseHistoryValDtos,
      updateState:this.updateState
    };
    let occupationalHistoryProps = {
      labelText: 'Occupational History',
      occupationalHistoryValDtos,
      codeMedicalSummaryCd: 'O',
      updateState:this.updateState
    };
    let familyHistoryProps = {
      labelText: 'Family History',
      familyHistoryValDtos,
      codeMedicalSummaryCd: 'F',
      updateState:this.updateState
    };
    let remarkProps = {
      labelText: 'Remark',
      remarkValDtos,
      codeMedicalSummaryCd: 'R',
      updateState:this.updateState
    };

    return (
      <Dialog
          classes={{
            paper: classes.paper
          }}
          id="medical_summary_dialog"
          fullWidth
          maxWidth="md"
          open={isOpen}
          scroll="body"
          PaperComponent={PaperComponent}
          onEscapeKeyDown={this.handleCancel}
      >
        {/* title */}
        <DialogTitle
            className={classes.dialogTitle}
            disableTypography
            customdrag="allowed"
        >
          Medical Summary
        </DialogTitle>
        <Grid className={classes.gridBorder} >
        {/* content */}
        <DialogContent
            classes={{
              'root':classes.dialogContent
            }}
        >
          <CIMSGrid container>
            {/* Past Medical History */}
            <Grid
                item
                xs={12}
                classes={{
                  'grid-xs-12':classes.grid_xs_12
                }}
            >
              <TextBox {...pastHistoryProps}/>
            </Grid>
            {/* Social History */}
            <Grid
                item
                xs={12}
                classes={{
                  'grid-xs-12':classes.grid_xs_12
                }}
            >
              <SocialHistory {...socialHistoryProps}/>
            </Grid>
            {/* Occupational History */}
            <Grid
                item
                xs={12}
                classes={{
                  'grid-xs-12':classes.grid_xs_12
                }}
            >
              <TextBox {...occupationalHistoryProps}/>
            </Grid>
            {/* Family History */}
            <Grid
                item
                xs={12}
                classes={{
                  'grid-xs-12':classes.grid_xs_12
                }}
            >
              <TextBox {...familyHistoryProps}/>
            </Grid>
            {/* Remark History */}
            <Grid
                item
                xs={12}
                classes={{
                  'grid-xs-12':classes.grid_xs_12
                }}
            >
              <TextBox {...remarkProps}/>
            </Grid>
          </CIMSGrid>
        </DialogContent>
        <Divider />
        {/* button group */}
        <DialogActions className={classes.dialogActions}>
          <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="center"
          >
            <CIMSButton
                classes={{
                  label:classes.fontLabel
                }}
                id="btn_medical_summary_dialog_save"
                onClick={this.handleSave}
            >
              Save
            </CIMSButton>
            <CIMSButton
                classes={{
                  label:classes.fontLabel
                }}
                id="btn_medical_summary_dialog_cancel"
                onClick={this.handleCancel}
            >
              Cancel
            </CIMSButton>
          </Grid>
        </DialogActions>
        </Grid>
      </Dialog>
    );
  }
}

const mapStateToProps = state => {
  return {
    loginInfo: state.login.loginInfo,
    smokingDropMap: state.medicalSummary.smokingDropMap,
    drinkingDropMap: state.medicalSummary.drinkingDropMap,
    substanceAbuseDropMap: state.medicalSummary.substanceAbuseDropMap,
    pastHistoryValDtos: state.medicalSummary.pastHistoryValDtos,
    smokingHistoryValDtos: state.medicalSummary.smokingHistoryValDtos,
    drinkingHistoryValDtos: state.medicalSummary.drinkingHistoryValDtos,
    substanceAbuseHistoryValDtos: state.medicalSummary.substanceAbuseHistoryValDtos,
    occupationalHistoryValDtos: state.medicalSummary.occupationalHistoryValDtos,
    familyHistoryValDtos: state.medicalSummary.familyHistoryValDtos,
    remarkValDtos: state.medicalSummary.remarkValDtos
  };
};

const mapDispatchToProps = {
  updateMedicalSummary,
  openCommonMessage,
  openCommonCircularDialog
};

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(MedicalSummaryDialog));
