/**
 * Front-end UI Component for MedicalSummaryDialog  past history props textbox、occupational history、family history、remark
 */
import React, { Component } from 'react';
import { Grid, withStyles, Typography } from '@material-ui/core';
import {styles} from './TextBoxStyle';
import {isUndefined,isNull} from 'lodash';
import { MEDICAL_SUMMARY_TYPE } from '../../../../../constants/medicalSummary/medicalSummaryConstants';

class TextBox extends Component {
  constructor(props){
    super(props);
    this.state={
      textVal:''
    };
  }

  componentDidMount(){
    let dto = this.initValDto();
    if (!isUndefined(dto)) {
      this.setState({
        textVal: isNull(dto.medicalSummaryText)?'':dto.medicalSummaryText
      });
    }
  }

  initValDto = () => {
    const {codeMedicalSummaryCd,pastHistoryValDtos,occupationalHistoryValDtos,familyHistoryValDtos,remarkValDtos} = this.props;
    let dto = {};
    switch (codeMedicalSummaryCd) {
      case MEDICAL_SUMMARY_TYPE.PAST_MEDICAL_HISTORY:
        dto = pastHistoryValDtos[0];
        break;
      case MEDICAL_SUMMARY_TYPE.OCCUPATIONAL_HISTORY:
        dto = occupationalHistoryValDtos[0];
        break;
      case MEDICAL_SUMMARY_TYPE.FAMILY_HISTORY:
        dto = familyHistoryValDtos[0];
        break;
      case MEDICAL_SUMMARY_TYPE.REMARK:
        dto = remarkValDtos[0];
        break;
      default:
        break;
    }
    return dto;
  }

  updateDtos = () => {
    const {codeMedicalSummaryCd,pastHistoryValDtos,occupationalHistoryValDtos,familyHistoryValDtos,remarkValDtos,updateState} = this.props;
    switch (codeMedicalSummaryCd) {
      case MEDICAL_SUMMARY_TYPE.PAST_MEDICAL_HISTORY:
        updateState&&updateState({
          isEdit:true,
          pastHistoryValDtos
        });
        break;
      case MEDICAL_SUMMARY_TYPE.OCCUPATIONAL_HISTORY:
        updateState&&updateState({
          isEdit:true,
          occupationalHistoryValDtos
        });
        break;
      case MEDICAL_SUMMARY_TYPE.FAMILY_HISTORY:
        updateState&&updateState({
          isEdit:true,
          familyHistoryValDtos
        });
        break;
      case MEDICAL_SUMMARY_TYPE.REMARK:
        updateState&&updateState({
          isEdit:true,
          remarkValDtos
        });
        break;
      default:
        break;
    }
  }

  handleTextChange = (event) => {
    let dto = this.initValDto();
    this.setState({
      textVal:event.target.value
    });
    dto.medicalSummaryText = event.target.value;
    this.updateDtos();
  }

  render() {
    const { classes,labelText='',codeMedicalSummaryCd } = this.props;
    let { textVal } = this.state;
    return (
      <Grid container direction="column" alignItems="flex-start">
        <Grid container item alignItems="baseline" spacing={0} className={classes.labelWrapper}>
          <Grid item>
            <label className={classes.label}>
              {labelText}
            </label>
          </Grid>
        </Grid>
        <Grid container item direction="row" alignItems="center" spacing={0} wrap="nowrap">
          <Grid item className={classes.textBoxWrapper}>
            <Typography
                id={`summary_text_box_${codeMedicalSummaryCd}`}
                className={classes.textBox}
                component="textarea"
                onChange={(e)=>{this.handleTextChange(e);}}
                value={textVal}
            />
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(TextBox);