/**
 * Front-end UI Component for MedicalSummaryDialog about  social history
 */
import React, { Component } from 'react';
import {styles} from './SocialHistoryStyle';
import { withStyles, Grid, Paper, Fab, Divider } from '@material-ui/core';
import ValidatorForm from '../../../../../components/FormValidator/ValidatorForm';
import HistoryGrid from '../HistoryGrid/HistoryGrid';
import { Add } from '@material-ui/icons';
import { MEDICAL_SUMMARY_TYPE,MEDICAL_SUMMARY_DROP_OPTION_SIGN } from '../../../../../constants/medicalSummary/medicalSummaryConstants';

class SocialHistory extends Component {

  handleSmokingAdd = () => {
    let {smokingHistoryValDtos,updateState} = this.props;
    smokingHistoryValDtos.push({
      medicalSummaryStatus: '',
      codeMedicalSummaryType: '',
      medicalSummaryText: '',
      medicalSummaryId: null,
      version: null,
      rowId: null,
      createdBy: null,
      createdDtm: null
    });

    updateState&&updateState({
      smokingHistoryValDtos
    });
  }

  handleDrinkingAdd = () => {
    let {drinkingHistoryValDtos,updateState} = this.props;
    drinkingHistoryValDtos.push({
      medicalSummaryStatus: '',
      codeMedicalSummaryType: '',
      medicalSummaryText: '',
      medicalSummaryId: null,
      version: null,
      rowId: null,
      createdBy: null,
      createdDtm: null
    });

    updateState&&updateState({
      drinkingHistoryValDtos
    });
  }

  handleSubstanceAbuseAdd = () => {
    let {substanceAbuseHistoryValDtos,updateState} = this.props;
    substanceAbuseHistoryValDtos.push({
      medicalSummaryStatus: '',
      codeMedicalSummaryType: '',
      medicalSummaryText: '',
      medicalSummaryId: null,
      version: null,
      rowId: null,
      createdBy: null,
      createdDtm: null
    });

    updateState&&updateState({
      substanceAbuseHistoryValDtos
    });
  }

  render() {
    const {
      classes,
      labelText='',
      smokingDropMap,
      drinkingDropMap,
      substanceAbuseDropMap,
      smokingHistoryValDtos,
      drinkingHistoryValDtos,
      substanceAbuseHistoryValDtos,
      updateState
    } = this.props;

    let smokingHistoryGridProps = {
      codeMedicalSummaryCd:MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SMOKING,
      dropList1:smokingDropMap.get(MEDICAL_SUMMARY_DROP_OPTION_SIGN.STATUS),
      dropList2:smokingDropMap.get(MEDICAL_SUMMARY_DROP_OPTION_SIGN.TYPE),
      removeFlag:smokingHistoryValDtos.length === 1?true:false,
      smokingHistoryValDtos,
      valDtos:smokingHistoryValDtos,
      updateState
    };
    let drinkingHistoryGridProps = {
      codeMedicalSummaryCd:MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_DRINKING,
      dropList1:drinkingDropMap.get(MEDICAL_SUMMARY_DROP_OPTION_SIGN.STATUS),
      dropList2:drinkingDropMap.get(MEDICAL_SUMMARY_DROP_OPTION_SIGN.TYPE),
      removeFlag:drinkingHistoryValDtos.length === 1?true:false,
      drinkingHistoryValDtos,
      valDtos:drinkingHistoryValDtos,
      updateState
    };
    let substanceAbuseHistoryGridProps = {
      codeMedicalSummaryCd:MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SUBSTANCE_ABUSE,
      dropList1:substanceAbuseDropMap.get(MEDICAL_SUMMARY_DROP_OPTION_SIGN.STATUS),
      dropList2:substanceAbuseDropMap.get(MEDICAL_SUMMARY_DROP_OPTION_SIGN.TYPE),
      removeFlag:substanceAbuseHistoryValDtos.length === 1?true:false,
      substanceAbuseHistoryValDtos,
      valDtos:substanceAbuseHistoryValDtos,
      updateState
    };

    return (
      <ValidatorForm id="SocialHistoryForm" onSubmit={() => {}} ref="form">
        <Grid container direction="column" alignItems="flex-start">
          <Grid container item alignItems="baseline" spacing={0} className={classes.labelContainer}>
            <Grid item>
              <label className={classes.label}>
                {labelText}
              </label>
            </Grid>
          </Grid>
          <Paper className={classes.paperRoot} elevation={5}>
            {/* Smoking */}
            <Grid
                container
                item
                spacing={0}
                direction="row"
                justify="space-evenly"
                className={classes.subGridContainer}
            >
              <Grid
                  item
                  container
                  xs={11}
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                  classes={{
                    'grid-xs-11':classes.contentGrid
                  }}
              >
                <Grid container item direction="row" alignItems="center" spacing={0} wrap="nowrap">
                  <Grid
                      item
                      container
                      direction="row"
                      justify="space-evenly"
                      alignItems="center"
                      classes={{
                          container:classes.summaryGridContainer
                      }}
                  >
                    <Grid item xs={1}>
                      <label className={classes.label}>
                        Smoking:
                      </label>
                    </Grid>
                    <Grid item xs={11} className={classes.itemContainer}>
                      {
                        smokingHistoryValDtos.map((element,index) => {
                          let props = {
                            ...smokingHistoryGridProps,
                            rowIndex:index
                          };
                          return(
                            <HistoryGrid key={`S_${index}`} {...props}/>
                          );
                        })
                      }
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              <Grid
                  container
                  direction="row"
                  justify="center"
                  alignItems="flex-end"
                  item
                  xs
                  classes={{
                    'container':classes.btnGrid
                  }}
              >
                <Fab
                    size="small"
                    color="primary"
                    aria-label="Add"
                    onClick={()=>{this.handleSmokingAdd();}}
                    id="btn_smoking_add"
                    classes={{
                      'primary':classes.btnAdd
                    }}
                >
                  <Add className={classes.icon} />
                </Fab>
              </Grid>
            </Grid>
            <Divider className={classes.divider}/>
            {/* Drinking */}
            <Grid
                container
                item
                spacing={0}
                direction="row"
                justify="space-evenly"
                className={classes.subGridContainer}
            >
              <Grid
                  item
                  container
                  xs={11}
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                  classes={{
                    'grid-xs-11':classes.contentGrid
                  }}
              >
                <Grid container item direction="row" alignItems="center" spacing={0} wrap="nowrap">
                  <Grid
                      item
                      container
                      direction="row"
                      justify="space-evenly"
                      alignItems="center"
                      classes={{
                          container:classes.summaryGridContainer
                      }}
                  >
                    <Grid item xs={1}>
                      <label className={classes.label}>
                        Drinking:
                      </label>
                    </Grid>
                    <Grid item xs={11} className={classes.itemContainer}>
                      {
                        drinkingHistoryValDtos.map((element,index) => {
                          let props = {
                            ...drinkingHistoryGridProps,
                            rowIndex:index
                          };
                          return(
                            <HistoryGrid key={`D_${index}`} {...props}/>
                          );
                        })
                      }
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              <Grid
                  container
                  direction="row"
                  justify="center"
                  alignItems="flex-end"
                  item
                  xs
                  classes={{
                    'container':classes.btnGrid
                  }}
              >
                <Fab
                    size="small"
                    color="primary"
                    aria-label="Add"
                    id="btn_drinking_add"
                    onClick={()=>{this.handleDrinkingAdd();}}
                    classes={{
                      'primary':classes.btnAdd
                    }}
                >
                  <Add className={classes.icon} />
                </Fab>
              </Grid>
            </Grid>
            <Divider className={classes.divider}/>
            {/* Substance Abuse */}
            <Grid
                container
                item
                spacing={0}
                direction="row"
                justify="space-evenly"
                className={classes.subGridContainer}
            >
              <Grid
                  item
                  container
                  xs={11}
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                  classes={{
                    'grid-xs-11':classes.contentGrid
                  }}
              >
                <Grid container item direction="row" alignItems="center" spacing={0} wrap="nowrap">
                  <Grid
                      item
                      container
                      direction="row"
                      justify="space-evenly"
                      alignItems="center"
                      classes={{
                          container:classes.summaryGridContainer
                      }}
                  >
                    <Grid item xs={1}>
                      <label className={classes.label}>
                        Substance Abuse:
                      </label>
                    </Grid>
                    <Grid item xs={11} className={classes.itemContainer}>
                      {
                        substanceAbuseHistoryValDtos.map((element,index) => {
                          let props = {
                            ...substanceAbuseHistoryGridProps,
                            rowIndex:index
                          };
                          return(
                            <HistoryGrid key={`A_${index}`} {...props}/>
                          );
                        })
                      }
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              <Grid
                  container
                  direction="row"
                  justify="center"
                  alignItems="flex-end"
                  item
                  xs
                  classes={{
                    'container':classes.btnGrid
                  }}
              >
                <Fab
                    size="small"
                    color="primary"
                    aria-label="Add"
                    id="btn_substance_abuse_add"
                    onClick={()=>{this.handleSubstanceAbuseAdd();}}
                    classes={{
                      'primary':classes.btnAdd
                    }}
                >
                  <Add className={classes.icon} />
                </Fab>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </ValidatorForm>
    );
  }
}

export default withStyles(styles)(SocialHistory);
