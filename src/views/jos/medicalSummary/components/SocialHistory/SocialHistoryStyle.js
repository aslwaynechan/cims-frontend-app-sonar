export const styles = () => ({
  icon: {
    fontSize: '24px'
  },
  labelContainer: {
    paddingBottom: '1px'
  },
  subGridContainer: {
    width: '893px'
  },
  itemContainer: {
    paddingLeft: '30px'
  },
  label: {
    fontFamily: 'Arial',
    fontSize: '1rem',
    fontWeight: 'bold'
  },
  summaryGridContainer: {
    width: 'calc(100% - 10px)',
    margin: '8px 0'
  },
  paperRoot: {
    width: 'calc(100% - 13px)',
    maxHeight: '190px',
    padding: '5px',
    overflowY: 'auto',
    overflowX: 'hidden',
    border: '1px solid rgba(0,0,0,0.42)',
    minWidth: '810px'
  },
  divider: {
    width: '98%',
    marginLeft: '5px'
  },
  btnAdd: {
    height: '28px',
    width: '28px',
    minHeight: '28px',
    '&:hover': {
      backgroundColor: '#0098FF'
    }
  },
  wrapperGrid: {
    width: '893px'
  },
  contentGrid: {
    maxWidth: '830px',
    flexBasis: '830px'
  },
  btnGrid: {
    paddingRight: '30px',
    maxWidth: '62px',
    minWidth: '45px',
    margin: '17px 0',
    textAlign: 'center'
  }
});
