import React, { Component } from 'react';
import { Grid, TextField, Fab, withStyles } from '@material-ui/core';
// import SelectFieldValidator from '../../../../../components/FormValidator/SelectFieldValidator';
import {trim,isNull,toString} from 'lodash';
import { Remove } from '@material-ui/icons';
import { styles } from './HistoryGridStyle';
import { MEDICAL_SUMMARY_TYPE,MEDICAL_SUMMARY_STATUS_NEVER_MAP,MEDICAL_SUMMARY_DROP_OPTION_SIGN } from '../../../../../constants/medicalSummary/medicalSummaryConstants';
import CustomizedSelectFieldValidator from '../../../../../components/Select/CustomizedSelect/CustomizedSelectFieldValidator';

class HistoryGrid extends Component {
  constructor(props){
    super(props);
    this.state={
      status:'',
      type:'',
      text:'',
      rowIndex:''
    };
  }

  static getDerivedStateFromProps(props, state) {
    const {codeMedicalSummaryCd,smokingHistoryValDtos,drinkingHistoryValDtos,substanceAbuseHistoryValDtos,rowIndex} = props;
    let dto = {};
    switch (codeMedicalSummaryCd) {
      case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SMOKING:
        dto = smokingHistoryValDtos[rowIndex];
        break;
      case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_DRINKING:
        dto = drinkingHistoryValDtos[rowIndex];
        break;
      case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SUBSTANCE_ABUSE:
        dto = substanceAbuseHistoryValDtos[rowIndex];
        break;
      default:
        break;
    }
    if (props.rowIndex !== state.rowIndex||dto.medicalSummaryStatus !== state.status||dto.codeMedicalSummaryType !== state.type||dto.medicalSummaryText !== state.text) {
      return {
        status:isNull(dto.medicalSummaryStatus)?'':dto.medicalSummaryStatus,
        type:isNull(dto.codeMedicalSummaryType)?'':dto.codeMedicalSummaryType,
        text:isNull(dto.medicalSummaryText)?'':dto.medicalSummaryText,
        rowIndex:props.rowIndex
      };
    }
    return null;
  }

  initValDto = () => {
    const {codeMedicalSummaryCd,smokingHistoryValDtos,drinkingHistoryValDtos,substanceAbuseHistoryValDtos,rowIndex} = this.props;
    let dto = {};
    switch (codeMedicalSummaryCd) {
      case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SMOKING:
        dto = smokingHistoryValDtos[rowIndex];
        break;
      case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_DRINKING:
        dto = drinkingHistoryValDtos[rowIndex];
        break;
      case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SUBSTANCE_ABUSE:
        dto = substanceAbuseHistoryValDtos[rowIndex];
        break;
      default:
        break;
    }
    return dto;
  }

  updateDtos = () => {
    const {codeMedicalSummaryCd,smokingHistoryValDtos,drinkingHistoryValDtos,substanceAbuseHistoryValDtos,updateState} = this.props;
    switch (codeMedicalSummaryCd) {
      case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SMOKING:
        updateState&&updateState({
          isEdit:true,
          smokingHistoryValDtos
        });
        break;
      case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_DRINKING:
        updateState&&updateState({
          isEdit:true,
          drinkingHistoryValDtos
        });
        break;
      case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SUBSTANCE_ABUSE:
        updateState&&updateState({
          isEdit:true,
          substanceAbuseHistoryValDtos
        });
        break;
      default:
        break;
    }
  }

  handleSelectChange = (event,name) => {
    let dto = this.initValDto();
    if (name === MEDICAL_SUMMARY_DROP_OPTION_SIGN.STATUS) {
      dto.medicalSummaryStatus = event.value;
      if (MEDICAL_SUMMARY_STATUS_NEVER_MAP.has(event.value)) {
        dto.codeMedicalSummaryType = MEDICAL_SUMMARY_STATUS_NEVER_MAP.get(event.value);
      }
    } else if (name === MEDICAL_SUMMARY_DROP_OPTION_SIGN.TYPE) {
      dto.codeMedicalSummaryType = event.value;
    }
    this.updateDtos();
  }

  handleTextChange = (event) => {
    let dto = this.initValDto();
    dto.medicalSummaryText = event.target.value;
    this.updateDtos();
  }

  handleTextBlur = (event) => {
    let dto = this.initValDto();
    dto.medicalSummaryText = trim(event.target.value);
    this.updateDtos();
  }

  handleGridDelete = (removeFlag) => {
    const {codeMedicalSummaryCd,smokingHistoryValDtos,drinkingHistoryValDtos,substanceAbuseHistoryValDtos,updateState,rowIndex} = this.props;
    switch (codeMedicalSummaryCd) {
      case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SMOKING:{
        if (removeFlag) {
          let tempObj = smokingHistoryValDtos[0];
          tempObj.medicalSummaryStatus = '';
          tempObj.codeMedicalSummaryType = '';
          tempObj.medicalSummaryText = '';
        } else {
          smokingHistoryValDtos.splice(rowIndex,1);
        }
        updateState&&updateState({
          smokingHistoryValDtos
        });
        return ;
      }
      case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_DRINKING:{
        if (removeFlag) {
          let tempObj = drinkingHistoryValDtos[0];
          tempObj.medicalSummaryStatus = '';
          tempObj.codeMedicalSummaryType = '';
          tempObj.medicalSummaryText = '';
        } else {
          drinkingHistoryValDtos.splice(rowIndex,1);
        }
        updateState&&updateState({
          drinkingHistoryValDtos
        });
        return ;
      }
      case MEDICAL_SUMMARY_TYPE.SOCIAL_HISTORY_SUBSTANCE_ABUSE:{
        if (removeFlag) {
          let tempObj = substanceAbuseHistoryValDtos[0];
          tempObj.medicalSummaryStatus = '';
          tempObj.codeMedicalSummaryType = '';
          tempObj.medicalSummaryText = '';
        } else {
          substanceAbuseHistoryValDtos.splice(rowIndex,1);
        }
        updateState&&updateState({
          substanceAbuseHistoryValDtos
        });
        return ;
      }
      default:
        break;
    }
  }

  render() {
    const {
      classes,
      codeMedicalSummaryCd,
      dropList1=[],
      dropList2=[],
      removeFlag,
      rowIndex
    } = this.props;
    let {
      status,
      type,
      text
    } = this.state;

    return (
      <Grid
          container
          direction="row"
          justify="space-around"
          alignItems="center"
          classes={{
            'container':classes.gridContainer
          }}
      >
        {/* Medical Summary Status */}
        <Grid
            item
            xs={3}
            classes={{
              'grid-xs-3':classes.selectGrid
            }}
        >
          <CustomizedSelectFieldValidator
              id={`social_select_status_${codeMedicalSummaryCd}_${rowIndex}`}
              options={dropList1.map(option => {
                return {
                  label: option.value,
                  value: toString(option.codeMedicalSummaryDropId)
                };
              })}
              value={status}
              styles={{ menuPortal: base => ({ ...base, zIndex: 1600 }) }}
              onChange={event=>{this.handleSelectChange(event,MEDICAL_SUMMARY_DROP_OPTION_SIGN.STATUS);}}
              // menuPortalTarget={document.querySelector('#medical_summary_dialog')}
              menuPortalTarget={document.body}
          />
        </Grid>
        {/* Medical Summary Type */}
        <Grid
            item
            xs={3}
            classes={{
              'grid-xs-3':classes.selectGrid
            }}
        >
          <CustomizedSelectFieldValidator
              id={`social_select_type_${codeMedicalSummaryCd}_${rowIndex}`}
              options={dropList2.map(option => {
                return {
                  label: option.value,
                  value: toString(option.codeMedicalSummaryDropId)
                };
              })}
              // classes={{
              //   paper:classes.selectPaper
              // }}
              styles={{ menuPortal: base => ({ ...base, zIndex: 1600 }) }}
              isDisabled={status===''?true:false}
              value={type}
              onChange={event=>{this.handleSelectChange(event,MEDICAL_SUMMARY_DROP_OPTION_SIGN.TYPE);}}
              // menuPortalTarget={document.querySelector('#medical_summary_dialog')}
              menuPortalTarget={document.body}
          />
        </Grid>
        {/* Medical Summary Text */}
        <Grid
            item
            xs={4}
            classes={{
              'grid-xs-4':classes.inputGrid
            }}
        >
          <TextField
              className={classes.fullyWidth}
              variant="outlined"
              id={`social_text_${codeMedicalSummaryCd}_${rowIndex}`}
              value={text}
              onChange={event => {this.handleTextChange(event);}}
              onBlur={event => {this.handleTextBlur(event);}}
              inputProps={{
                style:{
                  fontSize:'1rem',
                  fontFamily: 'Arial'
                }
              }}
          />
        </Grid>
        <Grid
            item
            xs={1}
            classes={{
              'grid-xs-1':classes.btnGrid
            }}
        >
          <Fab
              size="small"
              color="secondary"
              aria-label="Remove"
              id={`btn_delete_social_${codeMedicalSummaryCd}_${rowIndex}`}
              onClick={()=>{this.handleGridDelete(removeFlag);}}
              classes={{
                'secondary':classes.fabButtonDelete
              }}
          >
            <Remove className={classes.icon} />
          </Fab>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(HistoryGrid);
