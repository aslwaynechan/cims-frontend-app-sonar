export const styles = () => ({
  fullyWidth: {
    width: '100%'
  },
  icon: {
    fontSize: '24px'
  },
  gridContainer: {
    margin: '3px 0'
  },
  btnGrid: {
    padingTop: 2,
    paddingLeft: 5,
    maxWidth: '30px'
  },
  selectGrid: {
    maxWidth: '180px',
    flexBasis: '180px'
  },
  inputGrid: {
    maxWidth: '280px',
    flexBasis: '280px'
  },
  fabButtonDelete: {
    height: '28px',
    width: '28px',
    minHeight: '28px',
    backgroundColor: '#FD0000',
    '&:hover': {
      backgroundColor: '#fd0000'
    }
  },
  selectPaper: {
    width: 'max-content',
    maxWidth: 600
  }
});
