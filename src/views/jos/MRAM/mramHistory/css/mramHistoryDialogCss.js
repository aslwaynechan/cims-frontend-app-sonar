import { COMMON_STYLE }from '../../../../../constants/commonStyleConstant';

export const style = {
  paper: {
    borderRadius:5,
    minWidth: 965
  },
  dialogTitle: {
    backgroundColor: '#b8bcb9',
    borderTopLeftRadius:'5px',
    borderTopRightRadius:'5px',
    paddingLeft: '24px',
    padding: '10px 24px 7px 0px',
    color: '#404040',
    fontSize: '1.5rem',
    fontWeight: 500,
    lineHeight: 1.6,
    fontFamily: '-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif'
  },
  headRowStyle:{
    backgroundColor:COMMON_STYLE.TABLE_BGCKGROUNDCOLOR,
    color:'white'
  },
  headCellStyle:{
    color:'white',
    overflow: 'hidden',
    fontSize:'1.125rem'
  },
  fontLabel: {
    fontSize: '1rem',
    fontFamily: 'Arial'
  },
  buttonGroup: {
    padding: '0px 60px'
  },
  label:{
    fontSize:'1rem',
    paddingLeft: 17
  },
  length1:{
    marginLeft: 100
  },
  bodyCellStyle:{
    fontSize: '1rem',
    fontFamily: 'Arial'
  },
  paperTable:{
    width:'100%',
    overflow:'auto',
    maxHeight: 420
  },
  cardHearder: {
    paddingBottom: 0
  },
  btnGroup: {
    float: 'right',
    paddingRight: 10
  }
};


