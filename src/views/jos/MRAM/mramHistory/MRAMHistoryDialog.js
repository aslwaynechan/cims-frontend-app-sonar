
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {style} from './css/mramHistoryDialogCss';
import { withStyles } from '@material-ui/core/styles';
import {Grid, Typography} from '@material-ui/core';
import CIMSButton from '../../../../components/Buttons/CIMSButton';
import  EditTemplateDialog from '../../MRAM/mramHistory/EditMRAMHistoryDialog';
import {requestHistoryService} from '../../../../store/actions/MRAM/mramHistory/mramHistoryAction';
import { openCommonMessage } from '../../../../store/actions/message/messageAction';
import {Check} from '@material-ui/icons';
import { getMramFieldValueList,initMramFieldValueList} from '../../../../store/actions/MRAM/mramAction';
import {openCommonCircularDialog,closeCommonCircularDialog} from '../../../../store/actions/common/commonAction';
import {MRAM_HISTORY_CODE} from '../../../../constants/message/MRAMCode/mramHistoryCode';
import Paper from '@material-ui/core/Paper';
import {
    Card,
    CardContent,
    CardHeader
  } from '@material-ui/core';
import CIMSTable from '../../../../../src/components/Table/CimsTableNoPagination';
import moment from 'moment';

class MRAMHistoryDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
        pageNum:null,
        openHistory: this.props.openHistory,
        isEdit:false,
        selectRow: null,
        templateList:[],
        patientKey:0,
        tableOptions: {
            rowHover: true,
            rowsPerPage:5,
            onSelectIdName:'mramId',
            tipsListName:'diagnosisTemplates',
            tipsDisplayListName:null,
            tipsDisplayName: 'diagnosisDisplayName',
            onSelectedRow:(rowId,rowData,selectedData)=>{
                this.selectTableItem(selectedData);
            },
            bodyCellStyle:this.props.classes.bodyCellStyle,
            headRowStyle:this.props.classes.headRowStyle,
            headCellStyle:this.props.classes.headCellStyle
        },
        tableRows: [

            {name:'mramAssessmentDtm', width: 300, label: 'Metabolic Risk Assessment Date', customBodyRender: (value) => {
                return value ? moment(value).format('DD-MMM-YYYY') : null;
            }},
            {name:'serviceCd', width: 150, label: 'Patient Specialty' },
            {name:'dmInput', width: 40, label: 'DM',customBodyRender: (value) => {
              return value==='Y' && value!=null  ? <Check color="primary"/> :value;
            }},
            {name:'htInput', width: 40, label: 'HT',customBodyRender: (value) => {
              return value==='Y' && value!=null  ? <Check color="primary"/> :value;
            }},
            {name:'eyesInput', width: 46, label: 'Eyes',customBodyRender: (value) => {
              return value==='Y' && value!=null  ? <Check color="primary"/> :value;
            }},
            {name:'feetInput', width: 40, label: 'Feet',customBodyRender: (value) => {
              return value==='Y' && value!=null  ? <Check color="primary"/> :value;
            }},
            {name:'dietInput', width: 40, label: 'Diet',customBodyRender: (value) => {
              return value==='Y' && value!=null  ? <Check color="primary"/> :value;
            }},
            {name:'mramAssessmentStatusName', width: 'auto', label: 'Status'}
        ]
  };
}

// static getDerivedStateFromProps(props, state) {
//   let { deleteId,patientKey} = props;
//   let {templateList} = state;
//   console.log(templateList);
//   if(isEqual(deleteId, state.templateList.mramId)){
//     return {
//       templateList:templateList
//     };
//   }
//   if(isEqual(patientKey, state.patientKey)){
//     return {
//       patientKey:state.patientKey
//     };
//   }

//   // if (!isEqual(feetFieldValMap,state.feetFieldValMap)) {
//   //   return {
//   //     feetFieldValMap
//   //   };
//   // }
//   return null;
// }

UNSAFE_componentWillUpdate(nextProps) {
  let { patientKey} = this.state;
  let {openHistory}=this.state;
  if (nextProps.patientKey!== patientKey) {
    this.initData(nextProps.patientKey);
    this.setState({patientKey:nextProps.patientKey});  }
  if (nextProps.patientKey=== patientKey&&nextProps.openHistory!== openHistory) {
    this.initData(this.state.patientKey);
    this.setState({openHistory:nextProps.openHistory});
  }
}

  //  componentDidMount(){
  //     this.initData();
  //  }


  initData = (patientKey) => {
    if(patientKey!==undefined){
      this.props.openCommonCircularDialog();
      const params = {patientKey:patientKey};
      this.props.requestHistoryService({
        params,
        callback:(templateList) =>{
          this.props.closeCommonCircularDialog();
          this.setState({
            templateList: templateList.data,
            selectRow:null
          });
        }
      });
    }
  };

  getSelectRow = (data) => {
    // updateState&&updateState({selectRow:data.mramId});
    this.setState({
      selectRow:data.mramId,
      selectObj:data
    });
  }

  handleDialogClose=()=>{
    let {handleFunctionClose}=this.props;
    // handleClose&&handleClose();
    handleFunctionClose && handleFunctionClose(true);
  }

  openInstruction=()=>{
    let {openInstruction}=this.props;
    openInstruction&&openInstruction();
  }

  newMram=()=>{
    let {handleClose,updateState}=this.props;
    let view=false;
    this.props.initMramFieldValueList({});
    updateState&&updateState({view:view, selectRow:null,dervieType:0,dateTime: new Date()});
    handleClose&&handleClose();
  }

  viewMram=()=>{
    let {getPreviewReportData,updateState}=this.props;
    // let view=true;
    // this.props.openCommonCircularDialog();
    // this.props.getMramFieldValueList({
    //     params: {
    //       mramId: this.state.selectRow
    //     }
    // });
    // updateState&&updateState({selectRow: this.state.selectRow});
    getPreviewReportData&&getPreviewReportData('Metabolic Risk Assessment Report',this.state.selectRow);
  }

  editMram=()=>{
    let {handleClose,updateState}=this.props;
    let view=false;
    this.props.openCommonCircularDialog();
    this.props.getMramFieldValueList({
        params: {
          mramId: this.state.selectRow
        }
    });
    updateState&&updateState({view:view,selectRow:this.state.selectRow,dateTime:this.state.selectObj.mramAssessmentDtm});
    handleClose&&handleClose();
  }

  handleViewOpen = () => {
    let params=this.state.selectRow;
    if(params){
      this.viewMram();
    }else{
      let payload = {
        msgCode:MRAM_HISTORY_CODE.IS_HISTORY_EDIT,
        params:[
          {
            name:'action',
            value:'view'
          },
          {
            name:'title',
            value:'View'
          }
        ]
      };
      this.props.openCommonMessage(payload);
    }
  }

  handleEditOpen = () => {
    let params=this.state.selectRow;
    if(params){
      this.editMram();
    }else{
      let payload = {
        msgCode:MRAM_HISTORY_CODE.IS_HISTORY_EDIT,
        params:[
          {
            name:'action',
            value:'edit'
          },
          {
            name:'title',
            value:'Edit'
          }
        ]
      };
      this.props.openCommonMessage(payload);
    }
  };

  render() {
    const {classes,openHistory} = this.props;

    return (
      <EditTemplateDialog
          dialogTitle={'MRAM History List'}
          open={openHistory}
          onEscapeKeyDown={() =>this.handleDialogClose()}
      >
        <Card className={classes.bigContainerbigContainer}>
          <CardHeader className={classes.cardHearder} title={'Please select a case for editing'} />
          <Paper className={classes.paperTable} >
            <CardContent>
              <CIMSTable id="mram_history_table"
                  data={this.state.templateList}
                  getSelectRow={this.getSelectRow}
                  options={this.state.tableOptions}
                  rows={this.state.tableRows}
                  rowsPerPage={this.state.pageNum}
                  selectRow={this.state.selectRow}
                  style={{marginTop:20}}
                  tipsListSize={this.state.tipsListSize}
              />
            </CardContent>
          </Paper>
        </Card>
        <Typography component="div" className={classes.fixedBottom}>
          <Grid container direction="row" justify="space-between" alignItems="center">
            <Grid item xs={6}>
              <label className={classes.label}>MRAM records over 6 months old should be signed off.</label>
            </Grid>
            <Grid item xs>
              <div className={classes.btnGroup}>
                <CIMSButton
                    classes={{label:classes.fontLabel}}
                    color="primary"
                    id="btn_mramHistory_view"
                    size="small"
                    onClick={this.handleViewOpen}
                >
                  View
                </CIMSButton>
                <CIMSButton
                    classes={{label:classes.fontLabel}}
                    color="primary"
                    id="btn_mramHistory_new"
                    size="small"
                    onClick={this.newMram}
                >
                  New
                </CIMSButton>
                <CIMSButton
                    classes={{label:classes.fontLabel}}
                    color="primary"
                    id="btn_mramHistory_save"
                    size="small"
                    onClick={this.handleEditOpen}
                >
                  Edit
                </CIMSButton>
                <CIMSButton
                    classes={{label:classes.fontLabel}}
                    color="primary"
                    id="btn_mramHistory_reset"
                    onClick={() =>this.handleDialogClose()}
                    size="small"
                >
                  Cancel
                </CIMSButton>
              </div>
            </Grid>
          </Grid>
        </Typography>
      </EditTemplateDialog>
    );
  }
}

function mapStateToProps(state) {
  return {
    templateList: state.diagnosisReducer.templateList
  };
}
const mapDispatchToProps = {
  requestHistoryService,
  getMramFieldValueList,
  openCommonCircularDialog,
  closeCommonCircularDialog,
  openCommonMessage,
  initMramFieldValueList
};
export default connect(mapStateToProps,mapDispatchToProps)(withStyles(style)(MRAMHistoryDialog));
