const drawerWidth = 240;

export const styles = theme => ({
  infoImage: {
    width: '100.3%',
    marginLeft: -3
  },
  root: {
    display: 'flex'
  },
  appBar: {
    zIndex: 2,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36
  },
  hide: {
    display: 'none'
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap'
  },
  drawerRoot: {
    position: 'absolute',
    marginTop: -49,
    float: 'left'
    // height: 'calc(74vh - 10px)'
  },
  drawerPaperRoot: {
    position: 'unset'
  },
  drawerOpen: {
    width: drawerWidth,
    borderRight: '1px solid rgba(0, 0, 0, 0.12)',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerClose: {
    borderRight: '1px solid rgba(0, 0, 0, 0.12)',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1
    }
  },
  listRoot: {
    overflowY: 'auto',
    overflowX: 'hidden'
  },
  content: {
    marginLeft: 74,
    padding: 10,
    minWidth: 920,
    minHeight: 650,
    height: 'calc(69vh - 15px)',
    width: 'calc(97% - 33px)'
    // overflowX: 'auto',
    // overflowY: 'hidden'
  },
  contentOpen: {
    marginLeft: drawerWidth,
    width: 'calc(94% - 149px)'
  },
  contentWrapper: {
    // height: 'calc(68vh - 78px)',
    // width: 'calc(95% - 33px)'
  },
  moduleWrapper: {
    overflowX: 'auto',
    overflowY: 'hidden',
    height: 'inherit',
    minHeight: 400
  },
  bottomGropContainer: {
    paddingBottom: 10
    // paddingRight: 48
  },
  btnGroup: {
    margin: '0 70px 0 70px'
  },
  timeWrapper: {
    minWidth: 925
  },
  timeGroupDiv: {
    minWidth: 750
  },
  timeTitle: {
    paddingTop: 10,
    float: 'left',
    fontWeight: 'bold'
  },
  timeSpan: {
    color: 'red'
  },
  timeRemark: {
    float: 'left',
    paddingTop: 10,
    marginLeft: 30,
    fontWeight: 'bold'
  },
  timeRemarkSpan: {
    color: '#0579C8'
  },
  bottomGroup: {
    minWidth: 685,
    color: '#6e6e6e',
    position:'fixed',
    bottom: 0,
    width: '100%',
    zIndex: 100,
    backgroundColor: '#FFFFFF'
  },
  marginRightNone: {
    marginRight: 0
  },
  tooltip: {
    backgroundColor: '#707070'
  },
  selectedItem: {
    backgroundColor: '#DCDCDC'
  },
  bottomIcon: {
    marginLeft: 7
  },
  selectLabel: {
    fontSize: '1rem',
    float: 'left',
    paddingTop: '3px',
    paddingRight: '10px',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    marginLeft:6
  }
});
