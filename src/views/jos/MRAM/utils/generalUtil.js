import {toNumber,isUndefined} from 'lodash';
import {COMMON_ACTION_TYPE} from '../../../../constants/common/commonConstants';

export function DecimalValCheck(val) {
  // keep at most three decimal places
  let partten = /^([0-9]\d*|0)(\.[0-9]{1,3})?$/;
  if (!partten.test(val)) {
    return true;
  }
  return false;
}

export function NaturalValCheck(val) {
  let partten = /^(0|\+?[1-9][0-9]*)$/;
  if (!partten.test(val)) {
    return true;
  }
  return false;
}

export function abnormalCheck(val,rangeValObj) {
  let abnormalCheck = false;
  if (val!==''&&!isUndefined(rangeValObj)) {
    let minVal = toNumber(rangeValObj.minVal);
    let maxVal = toNumber(rangeValObj.maxVal);
    let currentVal = toNumber(val);
    if (currentVal<minVal||currentVal>maxVal) {
      abnormalCheck = true;
    }
  }
  return abnormalCheck;
}

export function handleOperationType(fieldValObj) {
  let { version,value } = fieldValObj;
  if (version!==null) {
    if (value!=='') {
      fieldValObj.operationType = COMMON_ACTION_TYPE.UPDATE;
    } else {
      fieldValObj.operationType = COMMON_ACTION_TYPE.DELETE;
    }
  } else if (version === null && value !== '') {
    fieldValObj.operationType = COMMON_ACTION_TYPE.INSERT;
  } else {
    fieldValObj.operationType = null;
  }
}
