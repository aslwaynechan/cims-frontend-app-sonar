export const styles = () => ({
  normalFont: {
    fontSize: '1rem',
    fontFamily: 'Arial'
  },
  card: {
    minWidth: 1519,
    overflowY: 'auto',
    height: 'calc(68vh - 78px)'
    // overflowX: 'auto'
  },
  cardContent: {
    paddingLeft: 0
  },
  form: {
    width: '100%'
  },
  leftHeader: {
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    borderTopLeftRadius: 5,
    fontFamily: 'Arial'
  },
  rightHeader: {
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    borderTopRightRadius: 5,
    fontFamily: 'Arial'
  },
  radioGroup: {
    display: 'inherit'
  },
  table: {
    borderRight: '1px solid #D5D5D5'
  },
  tableHeadFirstCell: {
    width: '28%',
    paddingRight: 5
  },
  tableHeadCell: {
    color: '#404040',
    fontSize: '1rem',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    width: '30%'
  },
  tableRowFieldCell: {
    fontSize: '1rem',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    paddingRight: 5
  },
  sup: {
    fontSize: 12,
    color: '#0579C8'
  },
  input: {
    width: '100%'
  },
  tableRow: {
    height: 57
  },
  fullwidth: {
    width: '100%'
  },
  width50: {
    width: '50%',
    borderLeft: 0
  },
  borderNone: {
    border: 'none'
  },
  textBox: {
    resize: 'none',
    border: '1px solid rgba(0,0,0,0.42)',
    height: '75px',
    width: 'calc(100% - 6px)',
    borderRadius: '5px',
    color: '#000000',
    fontSize: '1rem',
    fontFamily: 'Arial',
    '&:focus': {
      outline: 'none',
      border: '2px solid #0579C8'
    },
    boxShadow:
      '0px 3px 5px -1px rgba(0,0,0,0.2), 0px 5px 8px 0px rgba(0,0,0,0.14), 0px 1px 14px 0px rgba(0,0,0,0.12)'
  },
  tableCrossRow: {
    backgroundColor: '#D1EEFC'
  },
  paddingRightNone: {
    paddingRight: 0
  },
  tableCellSpan: {
    padding:'0 5px',
    '&:last-child': {
      padding:'0 5px'
    }
  },
  checkBoxField: {
    marginLeft: -5
  },
  eyesBorder:{
    borderRight: '4px solid',
    borderRightColor: '#D5D5D5'
  }
});