export const styles = () => ({
  abnormal: {
    color: '#fd0000 !important'
  },
  helper_error: {
    marginTop: 0,
    fontSize: '1rem !important',
    fontFamily: 'Arial',
    padding: '0 !important'
  },
  error_icon: {
    fontSize: '14px'
  },
  wrapper: {
    display: 'initial',
    float: 'left'
  },
  extraContent: {
    display: 'inline-grid',
    paddingTop: 12
  },
  coreWrapper: {
    float: 'left'
  },
  inputWrapper: {
    display: 'inline',
    float: 'left'
  },
  abpWrapper: {
    paddingLeft: 5
  },
  bbpWrapper: {
    paddingLeft: 5,
    paddingTop: 20
  },
  abiWrapper: {
    display: 'inline',
    float: 'left',
    marginLeft:10
  },
  icon: {
    fontSize: 50,
    transform: 'rotate(90deg)',
    marginLeft: -15,
    marginTop: 22
  },
  normalInput: {
    width: 80
  },
  abiInput: {
    marginTop:29,
    width:120
  },
  bottomWrapper: {
    float: 'left'
  }
});