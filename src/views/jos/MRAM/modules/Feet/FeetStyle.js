export const styles = () => ({
  normalFont: {
    fontSize: '1rem',
    fontFamily: 'Arial'
  },
  card: {
    minWidth: 1255,
    overflowY: 'auto',
    height: 'calc(68vh - 78px)'
    // overflowX: 'auto'
  },
  cardContent: {
    paddingLeft: 0
  },
  form: {
    width: '100%'
  },
  leftHeader: {
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    borderTopLeftRadius: 5
  },
  rightHeader: {
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    borderTopRightRadius: 5
  },
  sup: {
    fontSize: 12,
    color: '#0579C8'
  },
  radioGroup: {
    display: 'inherit'
  },
  table: {
    borderRight: '1px solid #D5D5D5'
  },
  tableHeadFirstCell: {
    width: '50%',
    paddingRight: 5
  },
  width25: {
    width: '25%',
    fontSize: '1rem',
    fontFamily: 'Arial',
    fontWeight: 'bold'
  },
  tableHeadCell: {
    color: '#404040',
    fontSize: '1rem',
    fontFamily: 'Arial',
    fontWeight: 'bold'
  },
  tableRowFieldCell: {
    fontSize: '1rem',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    paddingRight: 5
  },
  topHeader: {
    fontFamily: 'Arial',
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    borderTopRightRadius: 5,
    borderTopLeftRadius:5
  },
  middleHeader: {
    fontFamily: 'Arial',
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF'
  },
  tableRowFieldCellIndent: {
    fontSize: '1rem',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    textIndent: '15px'
  },
  tableCrossRow: {
    backgroundColor: '#D1EEFC'
  },
  tableRow: {
    height: 57
  },
  button:{
    marginLeft:8,
    minWidth:30
  },
  tableCellSpan: {
    padding:'0 5px',
    '&:last-child': {
      padding:'0 5px'
    }
  },
  firstLevelHeadCell: {
    paddingLeft: 15,
    '&:last-child':{
      paddingLeft: 15
    }
  },
  borderNone: {
    border: 'none'
  },
  borderLeftRight: {
    borderTop: 'none',
    borderBottom: 'none',
    borderLeft: '1px solid #d5d5d5',
    borderRight: '1px solid #d5d5d5'
  },
  remarkTitleTableRow: {
    height: 20
  },
  remarkTitleTableCell: {
    '&:last-child': {
      paddingTop: 5
    }
  },
  remarkTableCell: {
    height: 100
  },
  width50: {
    width: '50%'
  },
  AbpWrapper: {
    paddingLeft: 5,
    float: 'left'
  },
  BbpWrapper: {
    paddingLeft: 5,
    paddingTop: 20
  },
  paddingTop:{
    paddingTop:10
  },
  feetBorder:{
    borderRight: '4px solid',
    borderRightColor: '#D5D5D5'
  }
});