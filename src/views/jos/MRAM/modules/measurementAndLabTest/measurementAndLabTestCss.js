
export const style = {
  sup: {
    color:'blue',
    fontSize: '6pt',
    fontFamily: 'Arial',
    fontWeight:'bold'

  },
  span: {
    fontSize:'inherit',
    fontWeight:'bold',
    padding:'5px 10px 30px 10px'
  },
  divRow: {
    display:'inline-flex'
    //paddingTop: 10
  },
  divBodyLeft: {
    display:'inline-flex',
    textAlign:'right'
    //paddingTop: 10
  },
  leftHeader: {
    fontFamily : 'inherit',
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    borderTopLeftRadius: 5
  },
  rightHeader: {
    fontFamily : 'inherit',
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    borderTopRightRadius: 5
  },
  divBmi:{
    border: '2px solid rgb(196, 196, 196)',
    display:'inline',
    float:'left',
    minWidth: 140,
    marginTop:10
  },
  divBmiWarning:{
    border: '2px solid #fd0000',
    display:'inline',
    float:'left',
    minWidth: 140,
    marginTop:10
  },
  divWhr:{
    border: '2px solid rgb(196, 196, 196)',display:'inline',
    float:'left',
    minWidth: 140,
    marginTop:15
  },
  textBox: {
    resize: 'none',
    border: '1px solid rgba(0,0,0,0.42)',
    height: '100px',
    width: 'calc(100% - 6px)',
    borderRadius: '5px',
    color: '#404040',
    fontSize: '1rem',
    fontFamily: 'Arial',
    '&:focus': {
      outline: 'none',
      border: '2px solid #0579C8'
    },
    boxShadow:
      '0px 3px 5px -1px rgba(0,0,0,0.2), 0px 5px 8px 0px rgba(0,0,0,0.14), 0px 1px 14px 0px rgba(0,0,0,0.12)'
  },
  validation_left_span : {
    paddingLeft:'92px',
    color: '#fd0000',
    margin: '0',
    fontSize: '0.75rem',
   // marginTop: '8px',
    minHeight: '1em'
  },
  validation_right_span : {
    paddingLeft:'32px',
    color: '#fd0000',
    margin: '0',
    fontSize: '0.75rem',
   // marginTop: '8px',
    minHeight: '1em'
  },
  row_span : {
    height:'17px'
  },
  customRowStyle: {
    fontSize:'1rem',
    fontFamily: 'Arial'
  },
  headRowStyle:{
    height:'40px',
    fontFamily: 'Arial'

  },
  headCellStyle:{
    overflow: 'hidden',
    fontFamily: 'Arial',
    fontSize:'1rem',
    fontWeight: 'bold'
  },
  mergeTypeSharpIcon : {
    fontSize:'50px',
    transform: 'rotate(90deg)',
    marginTop:18,
    marginLeft:5
  },
  defultFont:{
    fontSize:'1rem',
    fontWeight:'bold',
    fontFamily: 'Arial'
  },
  error_icon: {
    fontSize: '14px'
  },
  helper_error: {
    fontSize: '10pt !important'
  },
  bloodPressure_error: {
    marginTop: 0,
    fontSize: '10pt !important',
    fontFamily: 'Arial',
    padding: '0 !important',
    marginLeft:'15%'
  },
  card:{
    minWidth: 1350,
    overflowY: 'auto',
    height: 'calc(68vh - 78px)'
  },
  bottom:{
    paddingBottom:0
  },
  bmiError:{
    marginTop: 0,
    fontSize: '10pt !important',
    fontFamily: 'Arial',
    padding: '0 !important',
    display: 'inline-block'
  },
  paddingTop:{
    paddingTop:10
  },
  labRetrievalBorder:{
    borderLeft:'1px solid #d5d5d5'
  }
};