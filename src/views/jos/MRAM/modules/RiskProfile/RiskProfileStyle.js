export const styles = () => ({
  card: {
    minWidth: 1069,
    overflowX: 'auto',
    overflowY: 'auto',
    height: 'calc(68vh - 78px)'
  },
  form: {
    width: '100%'
  },
  header: {
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    borderTopLeftRadius: 5,
    fontFamily:'Arial'
  },
  table: {
    borderRight: '1px solid #D5D5D5'
  },
  tableHeadFirstCell: {
    width: '28%',
    paddingRight: 5
  },
  tableHeadCell: {
    color: '#404040',
    fontSize: '14pt',
    fontWeight: 'bold',
    fontFamily:'Arial'
  },
  firstLevelHeadCell: {
    paddingLeft: 50
  },
  secondLevelHeadCell: {
    paddingLeft: 75
  },
  tableRowFieldCell: {
    fontSize: '12pt',
    fontWeight: 'bold',
    paddingRight: 5,
    fontFamily:'Arial'
  },
  input: {
    width: '100%',
    fontFamily:'Arial',
    fontSize: '1rem',
    fontWeight: 'bold'
  },
  tableRow: {
    height: 57
  },
  width50: {
    width: '50%'
  },
  borderNone: {
    border: 'none',
    whiteSpace:'inherit',
    fontFamily:'Arial'
  },
  tableCrossRow: {
    backgroundColor: '#D1EEFC'
  },
  riskText:{
    fontFamily:'Arial'
  },
  tableText:{
    paddingRight:6
  }
});