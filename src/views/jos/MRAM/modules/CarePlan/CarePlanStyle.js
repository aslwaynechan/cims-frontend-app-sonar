export const styles = () => ({
  card: {
    minWidth: 1069,
    overflowX: 'auto',
    overflowY: 'auto',
    height: 'calc(68vh - 78px)'
  },
  form: {
    width: '100%'
  },
  header: {
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5
  },
  radioGroup: {
    display: 'inherit',
    fontSize:'1rem'
  },
  table: {
    borderRight: '1px solid #D5D5D5'
  },
  tableHeadFirstCell: {
    width: '28%',
    paddingRight: 5
  },
  tableHeadCell: {
    color: '#404040',
    fontSize:'1rem',
    fontWeight: 'bold'
  },
  tableRowFieldCell: {
    fontSize:'1rem',
    fontWeight: 'bold',
    paddingRight: 5
  },
  tableRow: {
    height: 57
  },
  width50: {
    width: '50%'
  },
  borderNone: {
    border: 'none'
  },
  textBox: {
    resize: 'none',
    border: '1px solid rgba(0,0,0,0.42)',
    height: '75px',
    width: 'calc(100% - 6px)',
    borderRadius: '5px',
    color: '#000000',
    fontSize: '1rem',
    fontFamily: 'Arial',
    '&:focus': {
      outline: 'none',
      border: '2px solid #0579C8'
    },
    boxShadow:
      '0px 3px 5px -1px rgba(0,0,0,0.2), 0px 5px 8px 0px rgba(0,0,0,0.14), 0px 1px 14px 0px rgba(0,0,0,0.12)'
  },
  tableCrossRow: {
    backgroundColor: '#D1EEFC'
  },
  levelGrid: {
    margin: 5
  },
  levelLabel: {
    float: 'left',
    fontWeight: 'bold',
    marginRight: 10,
    fontSize:'1rem',
    marginTop:7
  },
  levelSelect: {
    float: 'left',
    width: 300
  },
  firstLevelHeadCell: {
    paddingLeft: 50
  },
  paddingLeft25: {
    paddingLeft: 25
  },
  middleHeader: {
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF'
  },
  tableCell : {
    fontSize:'1rem'
  }
});