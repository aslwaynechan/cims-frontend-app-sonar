export const styles = {
  normalFont: {
    fontSize: '1rem',
    fontFamily: 'Arial',
    marginLeft:-10
  },
  inputStyle: {
    fontFamily: 'Arial',
    fontWeight: 600,
    fontSize: '1rem',
    marginRight:15
  },
  selectLabel: {
    float:'left',
    paddingTop:'3px',
    paddingRight:'10px',
    fontFamily: 'Arial',
    fontWeight: 600,
    fontSize: '1rem'
  },
  divHeader:{
    marginLeft:10,
    borderRadius:0,
    backgroundColor:'rgb(5,121,200)',
    color:'white',
    width:'98%'
  },
  self_monitoring_box: {
    border: '1px solid rgba(0,0,0,0.42)',
    padding: 15,
    marginLeft: 10,
    paddingBottom:30
  },
  right_box: {
    border: '1px solid rgba(0,0,0,0.42)',
    minHeight: 190,
    padding: 15
  },
  card: {
    minWidth: 1480,
    overflowX: 'none',
    overflowY: 'auto',
    height: 'calc(68vh - 78px)'
  },
  leftHeader: {
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    borderTopLeftRadius: 5
  },
  checkBoxGrid:{
    marginLeft:11
  },
  dmText:{
    width: '100%',
     float: 'left',
     fontSize: '1rem'
  },
  gridContainer:{
    marginLeft: 35
  },
  gridContainerSecond:{
    marginLeft: 35,
    marginTop: 10
  },
  gridSup:{
    fontSize: 4,
    color: 'rgb(5,121,200)'
  },
  radioGroup:{
  },
  leftSpan:{
      fontFamily: 'Arial',
      fontSize: '1rem',
      marginRight:15
  },
  socialHistoryGrid:{
    paddingBottom:20,
    paddingTop:10
  },
  rightHeader: {
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    borderTopRightRadius: 5
  },
  headerNoRegistration:{
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF'
  },
  gridLable:{
    marginTop:10
  },
  formControlLabel:{
    marginTop: -10
  },
  affectedSibling:{
    width: '100%',
    float: 'left',
    marginLeft: -9,
    paddingLeft: 6,
    paddingRight: 0,
    fontSize: '1rem'
  },
  gridTotal:{
    marginTop: 8,
    marginLeft:-5
  },
  yearDiagnosisHt:{
    width: '100%',
     float: 'left',
     fontSize: '1rem'
  },
  radioGroupContainer:{
    marginLeft: 35, marginTop: 10
  },
  disableInputStyle:{
    fontFamily: 'Arial',
    fontWeight: 600,
    fontSize: '1rem',
    marginRight:15,
    color:'gainsboro'
  },
  disableLeftSpan:{
    fontFamily: 'Arial',
    fontSize: '1rem',
    // marginRight:10,
    marginRight:15,
    color:'gainsboro'
  },
  disabledPunctuationTotal:{
    fontSize: '1rem',
    color:'gainsboro'
  },
  punctuationTotal:{
    fontSize: '1rem'
  },
  tableCell : {
    fontSize:'1rem'
  },
  leftLableCenter:{
    marginTop:7
  }
};