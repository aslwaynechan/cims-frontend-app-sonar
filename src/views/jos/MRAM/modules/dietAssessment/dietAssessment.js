import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Grid, Typography,Checkbox,RadioGroup,FormControlLabel,Radio,Table,TableBody,TableRow,TableCell,Button,TextField,Card,CardContent,Paper} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import {style} from './dietAssessmentStyle';
import {Person} from '@material-ui/icons';
import moment from 'moment';
import { isEqual,includes} from 'lodash';
import * as generalUtil from '../../utils/generalUtil';
import {COMMON_ACTION_TYPE} from '../../../../../constants/common/commonConstants';
import {
  MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID,
  MRAM_DIETASSESSMENT_DASM_PREFIX
} from '../../../../../constants/MRAM/dietAssessment/dietAssessmentConstants';
import { MRAM_FEILD_MAX_LENGTH } from '../../../../../constants/MRAM/mramConstant';
import DatePicker from '../../components/DateTextField/DateTextField';

class DietAssessment extends Component {

  static initCheckBoxValue=(dietAssessmentFieldValMap)=>{
    let fieldValObj = dietAssessmentFieldValMap.get('dasm_168');
    let fiveList=[{label:'Breakfast',value:'Breakfast',checked:false},{label:'Lunch',value:'Lunch',checked:false},{label:'Dinner',value:'Dinner',checked:false},{label:'Rarely eat out',value:'Rarely eat out',checked:false}];
    if(fieldValObj.value!==null){
        // arr=fieldValObj.value.split(',');
        // for (let index = 0; index < arr.length; index++) {
        //   fiveList[index].checked=arr[index]==='1'?true:false;
        // }
        for (let index = 0; index < fiveList.length; index++) {
          fiveList[index].checked=includes(fieldValObj.value,fiveList[index].value)?true:false;
        }
    }
    return fiveList;
  }

  constructor(props) {
    super(props);
    this.state = {
      dateTime:'',
      textArea:'',
      loginName:'',
      gender5:[],
      oneList:[{label:'2 meals',value:'2 meals'},{label:'3 meals',value:'3 meals'},{label:'>3 meals',value:'>3 meals'},{label:'Not fixed meals',value:'Not fixed meals'}],
      twoList:[{label:'Regular portion',value:'Regular portion'},{label:'Irregular portion',value:'Irregular portion'},{label:'No carbonhydrates sometimes',value:'No carbonhydrates sometimes'},{label:'No carbonhydrates at all',value:'No carbonhydrates at all'}],
      threeList:[{label:'< 1 bowl',value:'< 1 bowl'},{label:'1 bowl',value:'1 bowl'},{label:'2 bowls',value:'2 bowls'},{label:'3 bowls',value:'3 bowls'}],
      fourList:[{label:'< 1 portion',value:'< 1 portion'},{label:'1 portion',value:'1 portion'},{label:'2 portions',value:'2 portions'},{label:'3 portions',value:'3 portions'}],
      fiveList:[{label:'Breakfast',value:'Breakfast',checked:false},{label:'Lunch',value:'Lunch',checked:false},{label:'Dinner',value:'Dinner',checked:false},{label:'Rarely eat out',value:'Rarely eat out',checked:false}],
      sixList:[{label:'> 4 times',value:'> 4 times'},{label:'2-3 times',value:'2-3 times'},{label:'Once',value:'Once'},{label:'Never',value:'Never'}],
      dietAssessmentFieldValMap:new Map()
  };
}

static getDerivedStateFromProps(props, state) {
  let { dietAssessmentFieldValMap } = props;
  if (!isEqual(dietAssessmentFieldValMap,state.dietAssessmentFieldValMap)) {
    let dateTime=dietAssessmentFieldValMap.get(`${MRAM_DIETASSESSMENT_DASM_PREFIX}_${MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.ASSESSMETN_DATE}`).value;
    let fiveList=DietAssessment.initCheckBoxValue(dietAssessmentFieldValMap);
    return {
      dietAssessmentFieldValMap,
      fiveList,
      dateTime
    };
  }
  return null;
}

  handleChange=(obj,prefix,mramId)=>{
    let { dietAssessmentFieldValMap } = this.state;
    let fieldValObj = dietAssessmentFieldValMap.get(`${prefix}_${mramId}`);
    fieldValObj.value = obj.target.value;
    generalUtil.handleOperationType(fieldValObj);
    this.updateValMaps(dietAssessmentFieldValMap);
    this.setState({
      dietAssessmentFieldValMap
    });
  }

  handleCheckBoxChange=(index)=>{
    let { dietAssessmentFieldValMap } = this.state;
    let fieldValObj = dietAssessmentFieldValMap.get('dasm_168');
    let items = [...this.state.fiveList];
    let checkBoxArr='';
    items[index].checked =!items[index].checked;
    let falseTimes=0;
    for (let index = 0; index < items.length; index++) {
      if(index===0){
        if(items[index].checked){
          checkBoxArr=items[index].value;
        }
        else{
          checkBoxArr='0';
          falseTimes++;
        }
      }
      else
      {
        if(items[index].checked){
          checkBoxArr=checkBoxArr+','+items[index].value;
        }
        else{
          falseTimes++;
        }
      }
    }
    this.handleCheckBoxOperationType(fieldValObj,falseTimes);
    this.updateValMaps(dietAssessmentFieldValMap);
    fieldValObj.value=checkBoxArr;
    this.setState({
      dietAssessmentFieldValMap
    });
    this.setState({fiveList:items});
  }

  handleCheckBoxOperationType=(fieldValObj,falseTimes)=>{
    let { version } = fieldValObj;
    if (version!==null) {
      if (falseTimes!==4) {
        fieldValObj.operationType = COMMON_ACTION_TYPE.UPDATE;
      } else {
        fieldValObj.operationType = COMMON_ACTION_TYPE.DELETE;
      }
    } else if (version === null && falseTimes !==4) {
      fieldValObj.operationType = COMMON_ACTION_TYPE.INSERT;
    } else {
      fieldValObj.operationType = null;
    }

  }

  handleTextAreaChange=(e)=>{
    this.setState({
      textArea:e.target.value
    });
  }

  getLoginName=(obj,prefix,mramId)=>{
    let { loginName } = this.props;
    let { dietAssessmentFieldValMap } = this.state;
    let fieldValObj = dietAssessmentFieldValMap.get(`${prefix}_${mramId}`);
    fieldValObj.value = loginName;
    // fieldValObj.value = sessionStorage.getItem('loginName') || null;
    generalUtil.handleOperationType(fieldValObj);
    this.updateValMaps(dietAssessmentFieldValMap);
    this.setState({
      dietAssessmentFieldValMap
    });
  }

  changeLoginName=(e)=>{
    this.setState({loginName:e.target.value});
  }

  changeDateTime=(e)=>{
    this.setState({dateTime:e!==null?moment(e).format('DD-MM-YYYY'):''});
  }

  updateValMaps = (map) => {
    const { updateState } = this.props;
    updateState&&updateState({
      dietAssessmentFieldValMap:map
    });
  }

  initCheckBoxValue=(dietAssessmentFieldValMap)=>{
      let fieldValObj = dietAssessmentFieldValMap.get('dasm_168');
      let arr=[];
      let fiveList=this.state.fiveList;
      if(fieldValObj.value!==null){
          arr=fieldValObj.value.split(',');
          for (let index = 0; index < arr.length; index++) {
            fiveList[index].checked=arr[index]==='1'?true:false;
          }
        }
      this.setState({fiveList});
  }

  handleDateChanged = (obj,prefix,mramId) => {
    let { updateState} = this.props;
    let { dietAssessmentFieldValMap } = this.state;
    let fieldValObj = dietAssessmentFieldValMap.get(`${prefix}_${mramId}`);
    let val = obj!==null?moment(obj).format('DD-MMM-YYYY'):'';
    if(val==='Invalid date'){
      fieldValObj.isError=true;
    }
    else{
      fieldValObj.isError=false;
    }
    fieldValObj.value = val===''?(moment(new Date()).format('DD-MMM-YYYY')):val;
    generalUtil.handleOperationType(fieldValObj);
    if(val!==''){
      this.setState({
        val
      });
    }
    updateState&&updateState({
      dietAssessmentFieldValMap
    });
  }

  render() {
    const { classes,view=false } = this.props;
    let { dietAssessmentFieldValMap } = this.state;
    return (

      <Card className={classes.card}>
        <CardContent >
          <Typography component="div">
            <Paper elevation={1}>

              <Typography className={classes.leftHeader}
                  component="h3"
                  variant="h5"
              >
              Dietitian Assessment
              </Typography>

                <Table id="tableDietitianAssessment">
                  <TableBody>
                    <TableRow>
                      <TableCell>
                        <Typography className={classes.font}
                            component="div"
                        >
                        Please answer the following questions based on your diet in the past 7days
                        </Typography>
                      </TableCell>
                    </TableRow>

                    <TableRow>
                      <TableCell>
                        <Grid alignItems="center"
                            container
                        >
                          <Grid item
                              xs={4}
                              className={classes.wordWrap}
                          >
                            <Typography className={classes.font}
                                component="div"
                            >
                              <span className={classes.font}>1.How many main meals do you have per day?</span>
                            </Typography>
                          </Grid>
                          <Grid item
                              xs={8}
                              className={classes.wordWrap}
                          >
                            <RadioGroup
                                className={classes.radioGroup}
                                id="DASM_164"
                                onChange={(obj)=>{this.handleChange(obj,MRAM_DIETASSESSMENT_DASM_PREFIX,MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_1);}}
                                value={dietAssessmentFieldValMap.has(`${MRAM_DIETASSESSMENT_DASM_PREFIX}_${MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_1}`)?
                                dietAssessmentFieldValMap.get(`${MRAM_DIETASSESSMENT_DASM_PREFIX}_${MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_1}`).value:
                                ''}
                            >
                            {this.state.oneList.map((item,index) => {
                              return (
                                <FormControlLabel
                                    className={classes.Checkbox}
                                    control={<Radio id={`Radio_DASM_164_${index}`} disabled={view} color="primary"/>}
                                    key={index}
                                    label={item.label}
                                    style={{marginLeft:50,width:'16%'}}
                                    value={item.value}
                                    classes={{
                                      label: classes.font
                                    }}
                                />
                                );
                            })}
                              </RadioGroup>
                            </Grid>
                          </Grid>
                        </TableCell>
                      </TableRow>

                      <TableRow>
                        <TableCell>
                          <Grid alignItems="center"
                              container
                          >
                            <Grid item
                                xs={4}
                                className={classes.wordWrap}
                            >
                              <Typography className={classes.font}
                                  component="div"
                              >
                                <span className={classes.font}>2.Regarding carbonhydrates intake(e.g.rice/noodles/congee/spaghetti),how is the portion in your main meal usually?</span>
                              </Typography>
                              </Grid>
                            <Grid item
                                xs={8}
                                className={classes.wordWrap}
                            >
                              <RadioGroup
                                  className={classes.radioGroup}
                                  id="DASM_165"
                                  onChange={(obj)=>{this.handleChange(obj,MRAM_DIETASSESSMENT_DASM_PREFIX,MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_2);}}
                                  // value={this.state}
                                  value={dietAssessmentFieldValMap.has(`${MRAM_DIETASSESSMENT_DASM_PREFIX}_${MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_2}`)?
                                  dietAssessmentFieldValMap.get(`${MRAM_DIETASSESSMENT_DASM_PREFIX}_${MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_2}`).value:
                                  ''}
                              >
                              {this.state.twoList.map((item,index) => {
                                  return (
                                    <FormControlLabel
                                        className={classes.Checkbox}
                                        control={<Radio id={`Radio_DASM_165_${index}`} disabled={view} color="primary"/>}
                                        key={index}
                                        label={item.label}
                                        value={item.value}
                                        classes={{
                                          label: classes.font
                                        }}
                                    />
                                    );
                              })}
                              </RadioGroup>
                            </Grid>
                          </Grid>
                        </TableCell>
                      </TableRow>

                      <TableRow>
                        <TableCell>
                          <Grid alignItems="center"
                              container
                          >
                            <Grid item
                                xs={4}
                                className={classes.wordWrap}
                            >
                              <Typography className={classes.font}
                                  component="div"
                              >
                                <span className={classes.font}>3.How much vegetables do you have per day?</span>
                              </Typography>
                            </Grid>
                            <Grid item
                                xs={8}
                                className={classes.wordWrap}
                            >
                              <RadioGroup
                                  className={classes.radioGroup}
                                  id="DASM_166"
                                  onChange={(obj)=>{this.handleChange(obj,MRAM_DIETASSESSMENT_DASM_PREFIX,MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_3);}}
                                  value={dietAssessmentFieldValMap.has(`${MRAM_DIETASSESSMENT_DASM_PREFIX}_${MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_3}`)?
                                  dietAssessmentFieldValMap.get(`${MRAM_DIETASSESSMENT_DASM_PREFIX}_${MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_3}`).value:
                                  ''}
                              >
                              {this.state.threeList.map((item,index) => {
                                return (
                                  <FormControlLabel
                                      className={classes.Checkbox}
                                      control={<Radio id={`Radio_DASM_166_${index}`} disabled={view} color="primary"/>}
                                      key={index}
                                      label={item.label}
                                      value={item.value}
                                      classes={{
                                        label: classes.font
                                      }}
                                  />
                                  );
                              })}
                              </RadioGroup>
                            </Grid>
                          </Grid>
                        </TableCell>
                      </TableRow>

                      <TableRow>
                        <TableCell>
                          <Grid alignItems="center"
                              container
                          >
                            <Grid item
                                xs={4}
                                className={classes.wordWrap}
                            >
                              <Typography className={classes.font}
                                  component="div"
                              >
                                <span className={classes.font}>4.How many portions of fruits do you have per day?</span>
                              </Typography>
                            </Grid>
                            <Grid item
                                xs={8}
                                className={classes.wordWrap}
                            >
                              <RadioGroup
                                  className={classes.radioGroup}
                                  name="DASM_167"
                                  onChange={(obj)=>{this.handleChange(obj,MRAM_DIETASSESSMENT_DASM_PREFIX,MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_4);}}
                                  value={dietAssessmentFieldValMap.has(`${MRAM_DIETASSESSMENT_DASM_PREFIX}_${MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_4}`)?
                                  dietAssessmentFieldValMap.get(`${MRAM_DIETASSESSMENT_DASM_PREFIX}_${MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_4}`).value:
                                  ''}
                              >
                              {this.state.fourList.map((item,index) => {
                                    return (
                                      <FormControlLabel
                                          className={classes.Checkbox}
                                          control={<Radio id={`Radio_DASM_167_${index}`} disabled={view} color="primary"/>}
                                          key={index}
                                          label={item.label}
                                          value={item.value}
                                          classes={{
                                            label: classes.font
                                          }}
                                      />
                                      );
                              })}
                              </RadioGroup>
                            </Grid>
                          </Grid>
                        </TableCell>
                      </TableRow>

                      <TableRow>
                        <TableCell>
                          <Grid alignItems="center"
                              container
                          >
                            <Grid item
                                xs={4}
                                className={classes.wordWrap}
                            >
                              <Typography className={classes.font}
                                  component="div"
                              >
                                <span className={classes.font}>5.When do you mostly eat out?(Tick all that apply)</span>
                              </Typography>
                            </Grid>
                            <Grid item
                                xs={8}
                                className={classes.wordWrap}
                            >
                            {this.state.fiveList.map((item,index) => {
                              return (
                                <FormControlLabel
                                    className={classes.Checkbox}
                                    control={<Checkbox disabled={view} name="DASM_168" color="primary" checked={item.checked} onClick={()=>this.handleCheckBoxChange(index)} />}
                                    key={index}
                                    label={item.label}
                                    value={item.value}
                                    classes={{
                                      label: classes.font
                                    }}
                                />
                                );
                            })}
                            </Grid>
                          </Grid>
                        </TableCell>
                      </TableRow>

                      <TableRow>
                        <TableCell>
                          <Grid alignItems="center"
                              container
                          >
                            <Grid item
                                xs={4}
                                className={classes.wordWrap}
                            >
                              <Typography className={classes.font}
                                  component="div"
                              >
                                <span className={classes.font}>6.How often do you have food or beverages that contain simple sugar?(e.g fruit juice/ soda/ sweet soup)</span>
                              </Typography>
                            </Grid>
                            <Grid item
                                xs={8}
                                className={classes.wordWrap}
                            >
                              <RadioGroup
                                  className={classes.radioGroup}
                                  id="DASM_169"
                                  onChange={(obj)=>{this.handleChange(obj,MRAM_DIETASSESSMENT_DASM_PREFIX,MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_6);}}
                                  value={dietAssessmentFieldValMap.has(`${MRAM_DIETASSESSMENT_DASM_PREFIX}_${MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_6}`)?
                                  dietAssessmentFieldValMap.get(`${MRAM_DIETASSESSMENT_DASM_PREFIX}_${MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_6}`).value:
                                  ''}
                              >
                              {this.state.sixList.map((item,index) => {
                                  return (
                                    <FormControlLabel
                                        className={classes.Checkbox}
                                        control={<Radio id={`Radio_DASM_169_${index}`} disabled={view} color="primary"/>}
                                        key={index}
                                        label={item.label}
                                        value={item.value}
                                        classes={{
                                          label: classes.font
                                        }}
                                    />
                                    );
                              })}
                              </RadioGroup>
                            </Grid>
                          </Grid>
                        </TableCell>
                      </TableRow>

                      <TableRow>
                        <TableCell>
                          <Grid alignItems="center"
                              container
                          >
                            <Grid item
                                xs={4}
                                className={classes.wordWrap}
                            >
                              <Typography className={classes.font}
                                  component="div"
                              ><span className={classes.font}>7.Other</span>
                              </Typography>
                            </Grid>
                            <Grid item
                                xs={8}
                                className={classes.wordWrap}
                            >
                              <textarea id="DASM_170" className={classes.textArea}
                                  value={dietAssessmentFieldValMap.has(`${MRAM_DIETASSESSMENT_DASM_PREFIX}_${MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_7}`)?
                                    dietAssessmentFieldValMap.get(`${MRAM_DIETASSESSMENT_DASM_PREFIX}_${MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_7}`).value:
                                  ''}
                                  onChange={(obj)=>{this.handleChange(obj,MRAM_DIETASSESSMENT_DASM_PREFIX,MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.QUESTION_7);}}
                                  disabled={view}
                                  maxLength={MRAM_FEILD_MAX_LENGTH.remarks}
                              ></textarea>
                            </Grid>
                          </Grid>
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>

                  <Typography className={classes.contentHeader}
                      component="h3"
                      variant="h5"
                  >
                    Other Information
                  </Typography>
                  <Typography className={classes.otherInformation}
                      component="div" id="divOtherInformation"
                  >
                    <Grid className={classes.font}
                        container
                    >
                      <Grid item
                          xs={2}
                          className={classes.font}
                          style={{marginTop:7}}
                      >Assessed by
                      </Grid>
                      <Grid item
                          xs={4}
                      >
                        <TextField autoCapitalize="off"
                            style={{}}
                            type="text"
                            variant="outlined"
                            value={dietAssessmentFieldValMap.has(`${MRAM_DIETASSESSMENT_DASM_PREFIX}_${MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.ASSESSED_BY}`)?
                            dietAssessmentFieldValMap.get(`${MRAM_DIETASSESSMENT_DASM_PREFIX}_${MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.ASSESSED_BY}`).value:
                          ''}
                            inputProps={{style:{fontSize:'1.125rem'}}}
                            onChange={(obj)=>{this.handleChange(obj,MRAM_DIETASSESSMENT_DASM_PREFIX,MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.ASSESSED_BY);}}
                            id="DASM_171"
                            disabled={view}
                        />
                        <Button className={classes.button}
                            onClick={(obj)=>{this.getLoginName(obj,MRAM_DIETASSESSMENT_DASM_PREFIX,MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.ASSESSED_BY);}}
                            size="small"
                            variant="contained"
                            disabled={view}
                        >
                          <Person style={{fontSize:'1.125rem'}}/>
                        </Button>
                      </Grid>
                      <Grid item
                          xs={2}
                          className={classes.font}
                          style={{marginTop:7}}
                      >Assessment date
                      </Grid>
                      <Grid item
                          style={{
                              flexBasis:'32%'
                            }}
                          xs={4}
                      >
                        {/* <ValidatorForm>
                          <DateFieldValidator
                              InputProps={{style:{fontSize:'1.125rem'}}}
                              value={this.state.dateTime!==''?moment(this.state.dateTime,'DD-MM-YYYY'):null}
                              format={Enum.DATE_FORMAT_EDMY_VALUE}
                              onChange={(obj)=>{this.handleDateChanged(obj,MRAM_DIETASSESSMENT_DASM_PREFIX,MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.ASSESSMETN_DATE);}}
                              minDate={new Date(MIN_DATE)}
                              maxDate={new Date(MAX_DATE)}
                              id="DASM_172"
                              disabled={view}
                          />
                        </ValidatorForm> */}

                        <DatePicker
                            fieldValMap={dietAssessmentFieldValMap}
                            prefix={MRAM_DIETASSESSMENT_DASM_PREFIX}
                            mramId={MRAM_DIETASSESSMENT_DASM_EXAMINATION_ID.ASSESSMETN_DATE}
                            updateState={this.props.updateState}
                            id="DASM_172"
                            disabled={view}
                            // maxDate={moment(new Date()).format('DD-MMM-YYYY')}
                            // name="fromDate"
                            // label="From"
                            // format="DD-MMM-YYYY"
                            // value={this.state.fromDate}
                            // onChange={this.formDateChange}
                        />

                      </Grid>
                    </Grid>
                  </Typography>
                </Paper>
              </Typography>
            </CardContent>
          </Card>
    );
  }
}

const mapStateToProps = state => {
  return {
    loginName: state.login.loginInfo.loginName
  };
};

export default connect(mapStateToProps)(withStyles(style)(DietAssessment));
