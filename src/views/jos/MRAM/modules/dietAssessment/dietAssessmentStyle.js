
export const style = () => ({

  divider:{
    height:2
  },
  radioGroup: {
    display: 'inherit'
  },
  leftHeader: {
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    borderTopLeftRadius: 5,
    marginTop:2,
    borderTopRightRadius:5
  },
  font: {
    fontSize: '1.125rem',
    fontWeight: 600,
    fontFamily:'Arial'
  },
  Checkbox:{
    marginLeft:50,
    width:'16%',
    fontSize: '1.125rem',
    fontWeight: 600,
    fontFamily:'Arial'
  },
  textArea:{
    marginTop:8,
    marginLeft:64,
    width:'82%',
    fontSize: '1.125rem',
    fontWeight: 600,
    fontFamily:'Arial',
    resize: 'none'
  },
  otherInformation:{
    marginLeft:24,
    marginTop:4,
    width:'auto',
    paddingBottom:4
  },
  button:{
    marginLeft:8,
    minWidth:30
  },
  card: {
    minWidth: 1069,
    overflowX: 'auto',
    overflowY: 'auto',
    height: 'calc(68vh - 78px)'
  },
  contentHeader: {
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    marginTop:2
  },
  wordWrap:{
    whiteSpace:'normal'
  }
});
