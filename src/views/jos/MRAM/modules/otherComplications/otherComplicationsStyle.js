export const style = () => ({
  card: {
    minWidth: 1069,
    overflowY: 'auto',
    height: 'calc(68vh - 78px)'
  },
  form: {
    padding:10,
    width: '100%'
  },
  leftHeader: {
    fontFamily:'Arial',
    padding: '5px 0 5px 20px',
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    borderTopLeftRadius: 5
  },
  rightHeader: {
    padding: '5px 0 5px 0',
    backgroundColor: '#0579C8',
    color: '#FFFFFF',
    borderTopRightRadius: 5
  },
  radioGroup: {
    display: 'inherit'
  },
  tableHeadCell: {
    color: '#404040',
    fontSize: '14pt',
    fontWeight: 'bold'
  },
  textBox: {
    resize: 'none',
    border: '1px solid rgba(0,0,0,0.42)',
    height: '300px',
    width: 'calc(100% - 30px)',
    borderRadius: '5px',
    color: '#000000',
    fontSize: '12pt',
    fontFamily: 'Arial',
    '&:focus': {
      outline: 'none',
      border: '2px solid #0579C8'
    },
    boxShadow:
      '0px 3px 5px -1px rgba(0,0,0,0.2), 0px 5px 8px 0px rgba(0,0,0,0.14), 0px 1px 14px 0px rgba(0,0,0,0.12)'
  },
  borderNone: {
    border: 'none'
  },
  tableRow: {
    height: 57
  },
  tableRowFieldCell: {
    fontFamily: 'Arial',
    borderColor:'white',
    fontSize: '12pt',
    fontWeight: 'bold',
    padding: '5px'
  },
  width50: {
    width: '50%'
  },
  sup: {
    fontSize: 12,
    color: '#0579C8'
  },
  checkboxGrid:{
    paddingLeft: 35
  },
  checkPadding: {
    margin: 0,
    padding: '0 10px 0 0'
  },
  selectLabel: {
    fontSize:'1rem',
    float: 'left',
    paddingTop: '3px',
    paddingRight: '10px',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    marginLeft:6
  },
  disable:{
    fontFamily: 'Arial',
    borderColor:'white',
    fontSize: '12pt',
    fontWeight: 'bold',
    paddingRight: 5,
    color:'rgba(0,0,0,0.38)'
  }
});