import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './JDecimalTextFieldStyle';
import { TextField, FormHelperText } from '@material-ui/core';
import { ErrorOutline } from '@material-ui/icons';
import * as generalUtil from '../../utils/generalUtil';

class DecimalTextField extends Component {
  constructor(props){
    super(props);
    this.state={
      errorFlag: false,
      abnormalFlag: false,
      val:''
    };
  }

  static getDerivedStateFromProps(props, state) {
    let { fieldValMap,prefix,mramId } = props;
    let val = '';
    let fieldValObj = fieldValMap.get(`${prefix}_${mramId}`);
    val = fieldValObj!==undefined?fieldValObj.value:'';

    if (val!==state.val) {
      return {
        val
      };
    }
    return null;
  }

  handleDecimalChanged = (event) => {
    let { updateState,fieldValMap,prefix,mramId,rangeValObj,sideEffect,abnormalMsg='' } = this.props;
    let errorFlag = false;
    let fieldValObj = fieldValMap.get(`${prefix}_${mramId}`);
    fieldValObj.value = event.target.value;

    if (event.target.value!=='') {
      if (!generalUtil.DecimalValCheck(event.target.value)) {
        errorFlag = false;
      } else {
        errorFlag = true;
      }
    }
    let abnormalFlag = generalUtil.abnormalCheck(event.target.value,rangeValObj);
    fieldValObj.isError = errorFlag||(abnormalFlag&&abnormalMsg!=='');
    sideEffect&&sideEffect(event.target.value,mramId,event);
    generalUtil.handleOperationType(fieldValObj);
    this.setState({
      val:event.target.value,
      errorFlag,
      abnormalFlag
    });
    updateState&&updateState({
      fieldValMap
    });
  }

  render() {
    const { id='',abnormalMsg='',classes, maxLength, extraContent='', msgPosition = 'bottom', viewMode=false, errorIconOpen=true, errorController=false ,redBorderController=false} = this.props;
    let { errorFlag,abnormalFlag,val } = this.state;
    let numberInputProps = {
      autoCapitalize:'off',
      autoComplete:'off',
      variant:'outlined',
      type:'text',
      inputProps: {
        style:{
          fontSize: '1rem',
          fontFamily: 'Arial'
        },
        maxLength: maxLength || null
      }
    };

    return (
      <div>
        <div className={classes.wrapper}>
          {msgPosition === 'top'&&(errorFlag||abnormalFlag)?(
            <FormHelperText
                error
                classes={{
                  error:classes.helper_error
                }}
            >
              <ErrorOutline className={classes.error_icon} />
              {errorFlag?'Illegal Characters':(abnormalFlag?abnormalMsg:'')}
            </FormHelperText>
          ):null}
          <TextField
              id={id}
              error={!errorFlag?abnormalFlag||errorController||redBorderController:true}
              value={val}
              disabled={viewMode}
              InputProps={{
                className: errorFlag?classes.abnormal:(abnormalFlag?classes.abnormal:null)
              }}
              onChange={event => {this.handleDecimalChanged(event);}}
              {...numberInputProps}
          />
          <div className={classes.extraContent}>
            <label>{extraContent}</label>
          </div>
          {msgPosition === 'bottom'&&(errorFlag||abnormalFlag||errorController)?(
            <FormHelperText
                error
                classes={{
                  root:classes.helper_error
                }}
            >
              {(errorIconOpen)?(
                <ErrorOutline className={classes.error_icon} />
                ):null
              }
              {errorFlag?'Illegal Characters':((abnormalFlag||errorController)?abnormalMsg:'')}
            </FormHelperText>
          ):null}
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(DecimalTextField);
