export const styles = () => ({
  radioGroup: {
    display: 'inherit'
  },
  normalFont: {
    fontSize: '1rem',
    fontFamily: 'Arial'
  }
});