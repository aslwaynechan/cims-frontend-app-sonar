import React, { Component } from 'react';
import { styles } from './RadioFieldStyle';
import { withStyles } from '@material-ui/core/styles';
import { RadioGroup, Radio, FormControlLabel } from '@material-ui/core';
import * as generalUtil from '../../utils/generalUtil';

class RadioField extends Component {
  constructor(props){
    super(props);
    this.state={
      val: ''
    };
  }

  static getDerivedStateFromProps(props, state) {
    let { fieldValMap,prefix,mramId } = props;
    let val = '';
    let fieldValObj = fieldValMap.get(`${prefix}_${mramId}`);
    val = fieldValObj!==undefined?fieldValObj.value:'';

    if (val!==state.val) {
      return {
        val
      };
    }
    return null;
  }

  generateRadios = () => {
    const { id='',radioOptions=[],classes,viewMode=false } = this.props;
    let radioElms = radioOptions.map(elm => {
      return(
        <FormControlLabel
            key={`${id}_Radio_${elm.value}`}
            classes={{
              label: classes.normalFont
            }}
            disabled={viewMode}
            value={elm.value}
            control={<Radio color="primary" id={`${id}_Radio_${elm.value}`} />}
            label={elm.label}
        />
      );
    });
    return radioElms;
  }

  handleRadioChanged = (event) => {
    let { updateState,fieldValMap,prefix,mramId, sideEffect } = this.props;
    let fieldValObj = fieldValMap.get(`${prefix}_${mramId}`);
    fieldValObj.value = event.target.value;
    sideEffect&&sideEffect(event.target.value,mramId);
    generalUtil.handleOperationType(fieldValObj);
    this.setState({
      val:event.target.value
    });
    updateState&&updateState({
      fieldValMap
    });
  }

  render() {
    const { classes } = this.props;
    let { val } = this.state;
    return (
      <div>
        <RadioGroup
            className={classes.radioGroup}
            value={val}
            onChange={(e)=>{this.handleRadioChanged(e);}}
        >
          {this.generateRadios()}
        </RadioGroup>
      </div>
    );
  }
}

export default withStyles(styles)(RadioField);
