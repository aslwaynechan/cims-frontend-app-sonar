export const styles = () => ({
  abnormal: {
    color: '#fd0000 !important'
  },
  helper_error: {
    marginTop: 0,
    fontSize: '1rem !important',
    fontFamily: 'Arial',
    padding: '0 !important'
  },
  error_icon: {
    fontSize: '14px'
  },
  wrapper: {
    display: 'initial',
    float: 'left'
  },
  extraContent: {
    display: 'inline-grid',
    paddingTop: 12
  }
});