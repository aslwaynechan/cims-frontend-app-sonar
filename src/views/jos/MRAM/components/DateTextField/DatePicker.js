import React,{Component} from 'react';
import { KeyboardDatePicker} from '@material-ui/pickers';

class DatePicker extends Component {
  constructor(props){
    super(props);
    this.state={
      value:props.value,
      format: this.props.format
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps){
    if(nextProps.value!==this.state.value){
      this.setState({value:nextProps.value});
    }
  }

  handleChange=(m,f)=>{
    const {onChange}=this.props;
    this.setState({value:m});
    onChange&&onChange(m,f);
  }

  inputOnFocus = () => {
    let editFormat = this.props.format;
    if (this.props.format === 'DD-MMM-YYYY') {
        editFormat = 'DD-MM-YYYY';
    } else if (this.props.format === 'MMM-YYYY') {
        editFormat = 'MM-YYYY';
    }
    this.setState({ format: editFormat });
  }

  inputOnBlur = () => {
    const { format } = this.props;
    this.setState({ format });
}

  render(){
    const {format}=this.props;
    return <KeyboardDatePicker
        autoFocus={(this.state.format === 'DD-MM-YYYY' && format === 'DD-MMM-YYYY')} {...this.props} onChange={this.handleChange} value={this.state.value} onFocus={this.inputOnFocus} onBlur={this.inputOnBlur}   autoOk  format={this.state.format}
           />;
  }
}

DatePicker.defaultProps={
  'no-need-input-label':true
};

export default DatePicker;
