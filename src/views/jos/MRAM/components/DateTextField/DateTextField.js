import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './DateTextFieldStyle';
import Enum from '../../../../../enums/enum';
import { MAX_DATE,MIN_DATE } from '../../../../../constants/common/commonConstants';
import * as generalUtil from '../../utils/generalUtil';
import moment from 'moment';
import DatePicker from './DatePicker';

class DateTextField extends Component {
  constructor(props){
    super(props);
    this.state={
      val:''
    };
  }

  static getDerivedStateFromProps(props, state) {
    let { fieldValMap,prefix,mramId } = props;
    let val = null;
    let fieldValObj = fieldValMap.get(`${prefix}_${mramId}`);
    val = fieldValObj!==undefined?fieldValObj.value:'';
    console.log('fieldValObj================== %o',fieldValObj);
    if (val!==state.val) {
      return {
        val
      };
    }
    return null;
  }

  handleDateChanged = (event) => {
    let { updateState,fieldValMap,prefix,mramId } = this.props;
    let fieldValObj = fieldValMap.get(`${prefix}_${mramId}`);
    let val = event!==null?moment(event).format('DD-MMM-YYYY'):'';
    if(val==='Invalid date'){
      fieldValObj.isError=true;
    }
    else{
      fieldValObj.isError=false;
    }
    fieldValObj.value = val===''?(moment(new Date()).format('DD-MMM-YYYY')):val;
    generalUtil.handleOperationType(fieldValObj);
    if(val!==''){
      this.setState({
        val
      });
    }

    updateState&&updateState({
      fieldValMap
    });
  }

  render() {
    const { id='', classes, viewMode=false } = this.props;
    let { val } = this.state;
    return (
      <div>
        {/* <DateFieldValidator
            id={`${id}_Date`}
            className={classes.fullwidth}
            inputVariant="outlined"
            format={Enum.DATE_FORMAT_EDMY_VALUE}
            minDate={new Date(MIN_DATE)}
            maxDate={new Date(MAX_DATE)}
            value={val!==''?moment(val,'DD-MM-YYYY'):moment(new Date()).format('DD-MMM-YYYY')}
            disabled={viewMode}
            errorMessages="Invalid Date Format"
            InputProps={{
              style: {
                fontSize: '1rem',
                fontFamily: 'Arial'
              }
            }}
            onChange={(e)=>{this.handleDateChanged(e);}}
        /> */}
        <DatePicker
            id={`${id}_Date`}
            placeholder={moment(new Date()).format('DD-MMM-YYYY')}
            className={classes.fullwidth}
            inputVariant="outlined"
            format={Enum.DATE_FORMAT_EDMY_VALUE}
            minDate={new Date(MIN_DATE)}
            maxDate={new Date(MAX_DATE)}
            value={val!==''?moment(val,'DD-MMM-YYYY'):moment(new Date()).format('DD-MMM-YYYY')}
            disabled={viewMode}
            InputProps={{
              style: {
                fontSize: '1rem',
                fontFamily: 'Arial'
              }
            }}
            onChange={(e)=>{this.handleDateChanged(e);}}
        />
      </div>
    );
  }
}

export default withStyles(styles)(DateTextField);
