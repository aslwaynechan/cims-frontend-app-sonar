export const styles = () => ({
  fullwidth: {
    width: '100%'
  },
  helper_error: {
    marginTop: 1,
    fontSize: '1rem !important',
    fontFamily: 'Arial',
    padding: '0 !important'
  }
});