import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles, createMuiTheme } from '@material-ui/core/styles';
import { Grid, FormControl } from '@material-ui/core';
import CIMSButton from '../../../components/Buttons/CIMSButton';
import CIMSTable from '../../../components/Table/CIMSTable';
import CIMSInputLabel from '../../../components/InputLabel/CIMSInputLabel';
import {
    updateField,
    listHoliday,
    printHolidayList,
    resetAll
} from '../../../store/actions/administration/publicHoliday/publicHolidayAction';
import {
    openCommonCircular
} from '../../../store/actions/common/commonAction';
import * as CommmonUtilities from '../../../utilities/commonUtilities';
import Enum from '../../../enums/enum';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import ValidatorEnum from '../../../enums/validatorEnum';
import CommonMessage from '../../../constants/commonMessage';
import TextFieldValidator from '../../../components/FormValidator/TextFieldValidator';
import RequiredIcon from '../../../components/InputLabel/RequiredIcon';

const styles = () => ({
    root: {
        width: '100%',
        display: 'flex',
        height: '100%',
        flexFlow: 'column'
    },
    dateInput: {
        marginLeft: 20
    }

});

const highResolution = (theme) => createMuiTheme({
    ...theme,
    spacing: 8,
    overrides: {
        ...theme.overrides,
        MuiInputBase: {
            ...theme.overrides.MuiInputBase,
            root: {
                //height: 36,
                height: 48,
                lineHeight: 'inherit'
            }
        },
        MuiTableRow: {
            ...theme.overrides.MuiTableRow,
            root: {
                height: 65
            },
            head: {
                height: 70
            }
        },
        MuiTableCell: {
            ...theme.overrides.MuiTableCell,
            root: {
                fontSize: '1.0rem',
                lineHeight: 1,
                borderColor: '#d5d5d5',
                borderStyle: 'solid',
                borderWidth: 1
            },
            head: {
                fontSize: '1.0rem',
                fontWeight: 'bold'
            }

        },
        MuiButtonBase: {
            ...theme.overrides.MuiButtonBase,
            root: {
                height: 43
            }
        },
        MuiButton: {
            ...theme.overrides.MuiButton,
            label: {
                //transform: 'translate(20px, 16px) scale(1)',
                //backgroundColor: 'white'
                fontSize: '1.3rem'
            }
        }
    }
});

const lowResolution = (theme) => createMuiTheme({
    ...theme,
    overrides: {
        ...theme.overrides,
        MuiInputBase: {
            ...theme.overrides.MuiInputBase,
            root: {
                height: 36,
                //height: 48,
                lineHeight: 'inherit'
            }
        }
    }
});

const normalResolution = (theme) => createMuiTheme({
    ...theme,
    overrides: {
        ...theme.overrides,
        MuiInputBase: {
            ...theme.overrides.MuiInputBase,
            root: {
                height: 36,
                //height: 48,
                lineHeight: 'inherit'
            }
        },
        MuiTableRow: {
            ...theme.overrides.MuiTableRow,
            root: {
                height: 65
            },
            head: {
                height: 75
            }
        },
        MuiTableCell: {
            ...theme.overrides.MuiTableCell,
            root: {
                fontSize: '1.0rem',
                lineHeight: 1,
                borderColor: '#d5d5d5',
                borderStyle: 'solid',
                borderWidth: 1
            },
            head: {
                fontSize: '1.1rem',
                fontWeight: 'bold'
            }

        }
    }
});


class PublicHoliday extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tableRows: [
                { name: 'dateStr', label: 'Date', width: 120 },
                { name: 'engDesc', label: 'Description' }
            ],
            tableStyle: {
                fontSize: '12pt',
                height: 'calc( 100vh - 316px)'
            },
            tableOptions: {
                rowsPerPage: 10,
                rowsPerPageOptions: [10],
                rowExpand: true
                //headCellStyle: this.props.classes.customTableHeadCell
            },
            theme: highResolution
        };
        this.resizeBind = this.handleWindowResize.bind(this);
    }


    componentDidMount() {
        this.props.ensureDidMount();
        this.handleSearch();
        window.addEventListener('resize', this.resizeBind);
        this.loadCurrentTheme();
    }

    componentWillUnmount() {
        this.props.resetAll();
    }


    loadCurrentTheme = () => {
        let totalHeight = window.innerHeight + 25;
        //let totalWidth = window.innerWidth;
        let sysRatio = CommmonUtilities.getSystemRatio();
        let tempTheme = normalResolution;
        switch (sysRatio) {
            //4:3 screen
            case Enum.SYSTEM_RATIO_ENUM.RATIO1: {
                if (totalHeight >= 720 && totalHeight < 1024) {
                    tempTheme = lowResolution;
                }
                else if (totalHeight > 1024) {
                    tempTheme = highResolution;

                }
                break;
            }
            //5:4 screen
            case Enum.SYSTEM_RATIO_ENUM.RATIO2: {
                //if()
                tempTheme = normalResolution;
                break;
            }
            //16:9 screen
            case Enum.SYSTEM_RATIO_ENUM.RATIO3: {
                if (totalHeight > 1024) {
                    tempTheme = highResolution;

                }
                break;
            }
            default: {
                tempTheme = normalResolution;
                break;
            }
        }
        this.setState({ theme: tempTheme });
    }

    handleWindowResize = () => {
        this.loadCurrentTheme();
    }

    updateSearchParam = (value, name) => {
        this.props.updateField({ [name]: value });
    }

    handleSearch = () => {
        this.refs.yearFrom.isFormValid(false);
        this.refs.yearTo.isFormValid(false);
        let { yearFrom, yearTo } = this.props;
        if (yearFrom.toString().length == 0 || yearTo.toString().length == 0) {
            return;
        }
        let searchParam = {
            yearFrom: yearFrom,
            yearTo: yearTo
        };
        this.props.listHoliday(searchParam);
    }

    handlePrint = () => {
        this.props.openCommonCircular();
        let { yearFrom, yearTo } = this.props;
        let searchParam = {
            yearFrom: yearFrom,
            yearTo: yearTo
        };
        this.props.printHolidayList(
            searchParam
            // (printSuccess)=>{

            // }
        );
    }

    render() {
        const { classes, holidayList } = this.props;

        return (
            // <MuiThemeProvider theme={this.state.theme}>
            <Grid container className={classes.root}>
                <Grid container >
                    <Grid item container xs={9} alignItems={'center'}>
                        {/* <Grid container> */}
                        <Grid ><CIMSInputLabel>From Year</CIMSInputLabel><RequiredIcon/></Grid>
                        <Grid item xs={3} className={classes.dateInput}>
                            <ValidatorForm ref="yearFrom">
                                <FormControl className={classes.form_input}>
                                    <TextFieldValidator
                                        id={'publicHoliday_yearfrom_textField'}
                                        name={'yearFrom'}
                                        value={this.props.yearFrom}
                                        validByBlur
                                        upperCase
                                        inputProps={{ maxLength: 4 }}
                                        validators={[
                                            ValidatorEnum.required
                                        ]}
                                        errorMessages={[
                                            CommonMessage.VALIDATION_NOTE_REQUIRED()
                                        ]}
                                        warning={[
                                            ValidatorEnum.isEnglishWarningChar
                                        ]}
                                        msgPosition="bottom"
                                        onChange={e => this.updateSearchParam(e.target.value, e.target.name)}
                                        onBlur={e => this.updateSearchParam(e.target.value, e.target.name)}
                                    />
                                </FormControl>

                            </ValidatorForm>

                        </Grid>
                        {/* </Grid> */}
                        {/* <Grid container> */}
                        <Grid item xs={1}></Grid>
                        <Grid ><CIMSInputLabel>To</CIMSInputLabel><RequiredIcon/></Grid>
                        <Grid item xs={3} className={classes.dateInput}>
                            <ValidatorForm ref="yearTo">
                                <FormControl className={classes.form_input}>
                                    <TextFieldValidator
                                        id={'publicHoliday_yearTo_textField'}
                                        name={'yearTo'}
                                        value={this.props.yearTo}
                                        validByBlur
                                        upperCase
                                        inputProps={{ maxLength: 4 }}
                                        validators={[
                                            ValidatorEnum.required
                                        ]}
                                        errorMessages={[
                                            CommonMessage.VALIDATION_NOTE_REQUIRED()
                                        ]}
                                        warning={[
                                            ValidatorEnum.isEnglishWarningChar
                                        ]}
                                        msgPosition="bottom"
                                        onChange={e => this.updateSearchParam(e.target.value, e.target.name)}
                                        onBlur={e => this.updateSearchParam(e.target.value, e.target.name)}
                                    />
                                </FormControl>

                            </ValidatorForm>
                        </Grid>
                        {/* </Grid> */}
                    </Grid>
                    <Grid item container xs={3} alignItems={'center'} justify={'flex-end'} >
                        <Grid>
                            <CIMSButton
                                id={'publicHoliday_search_button'}
                                variant={'contained'}
                                color={'primary'}
                                size={'small'}
                                onClick={this.handleSearch}
                                children={'Search'}
                            />
                            <CIMSButton
                                id={'publicHoliday_print_button'}
                                variant={'contained'}
                                color={'primary'}
                                size={'small'}
                                children={'Print'}
                                onClick={this.handlePrint}
                            />
                            <CIMSButton
                                id={'publicHoliday_exit_button'}
                                variant={'contained'}
                                color={'primary'}
                                size={'small'}
                                children={'Exit'}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid container >
                    <CIMSTable
                        id={'publicHoliday_holiday_table'}
                        innerRef={ref => this.tableRef = ref}
                        rows={this.state.tableRows}
                        data={holidayList ? holidayList : null}
                        options={this.state.tableOptions}
                        // tableStyles={{
                        //     minHeight: 406
                        // }}
                        //remote
                        totalCount={0}
                    //onChangePage={(...args) => this.handleOnChangePage(...args)}
                    //onChangeRowsPerPage={(...args) => this.handleOnChangeRowPerPage(...args)}
                    />
                </Grid>
            </Grid>
            // </MuiThemeProvider>
        );
    }

}
const mapStateToProps = (state) => {
    return {
        //holidayList: state.publicHoliday.holidayList,
        ...state.publicHoliday
    };
};

const mapDispatchToProps = {
    updateField,
    listHoliday,
    printHolidayList,
    openCommonCircular,
    resetAll
};
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(PublicHoliday));