import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    // Avatar,
    Grid,
    Typography,
    List,
    ListItem,
    ListItemText,
    // ListItemIcon,
    AppBar,
    Drawer,
    Divider
} from '@material-ui/core';
// import FolderSpecialIcon from '@material-ui/icons/FolderSpecial';
import AccessRightList from './accessRight';
import Enum from '../../../../enums/enum';
import * as CommonUtilties from '../../../../utilities/commonUtilities';
// import Palette from '../../../../theme/palette';

const drawerWidth = 240;

const styles = () => ({
    h6Title: {
        marginTop: 4,
        marginBottom: 4
    },
    listNav: {
        width: '100%'
    },
    accessRightGpListItem: {
        padding: '0px 16px'
    },
    accessRightGpDiv: {
        border: 'solid 1px',
        marginRight: 10
    },
    appBar: {
        position: 'relative'
    },
    drawerRoot: {
        float: 'left',
        height: 'calc(100% - 10px)',
        width: drawerWidth
    },
    drawerPaperRoot: {
        position: 'unset'
    },
    containerRoot: {
        border: 'solid 1px'
    },
    menuAvatarRoot: {
        width: 15,
        height: 15,
        borderRadius: '5%',
        paddingRight: 5
    },
    accessRightContent: {
        marginLeft: drawerWidth,
        marginTop: 45,
        position: 'absolute',
        width: 'calc(50% - 480px)'
    }
});

class UserRoleAccessRightPanel extends React.Component {
    state = {
        tabValue: 0,
        curAccessRightType: Enum.ACCESS_RIGHT_TYPE.FUNCTION,
        allMatchTypeMenuList: [],
        curSelectedMenu: [],
        curSelectedItem: -1
    }

    initAllMenuRight = () => {
        const { userRoleStatus } = this.props;
        let allMatchTypeMenuList = [];
        if (userRoleStatus.menuBK && userRoleStatus.menuBK.length > 0) {
            allMatchTypeMenuList = userRoleStatus.menuBK.filter(item => item.accessRightType === Enum.ACCESS_RIGHT_TYPE.FUNCTION);
            this.setState({ allMatchTypeMenuList });
        }
    }

    changeTabValue = (event, value) => {
        let accessRightType = Enum.ACCESS_RIGHT_TYPE.FUNCTION;
        if (value === 1) {
            accessRightType = Enum.ACCESS_RIGHT_TYPE.BUTTON;
        }
        this.setState({ tabValue: value, curAccessRightType: accessRightType });
    }

    handleOpenMenu = (accessRightCd) => {
        this.props.openMenu(accessRightCd);
    }

    handleAccessRightChecked = (accessRights) => {
        this.props.selectAccessRight(accessRights);
    }

    handleAccessRightGPClick = (right, idx) => {
        let newMenuList = [];
        let curAccessRightType = Enum.ACCESS_RIGHT_TYPE.FUNCTION;
        if (right.accessRightCd === 'F9999') {
            //non function right;
            const { userRoleStatus } = this.props;
            if (userRoleStatus.menuBK && userRoleStatus.menuBK.length > 0) {
                newMenuList = userRoleStatus.menuBK.filter(item => item.accessRightType != Enum.ACCESS_RIGHT_TYPE.FUNCTION);
                curAccessRightType = Enum.ACCESS_RIGHT_TYPE.BUTTON;       //maybe will change type name to non-function;
            }
        }
        else {
            CommonUtilties.changeAllRightsOpenStatus(right, true);
            newMenuList.push(right);
        }
        this.setState({ curSelectedMenu: newMenuList, curSelectedItem: idx, curAccessRightType });
    }

    resetCurSelectedItem = () => {
        this.setState({ curSelectedItem: -1, curSelectedMenu: [] });
    }

    render() {
        const { classes, userRoleStatus } = this.props;
        const allMatchTypeMenuList = userRoleStatus.menuBK.filter(item => item.accessRightType === Enum.ACCESS_RIGHT_TYPE.FUNCTION);
        const { curSelectedMenu, curSelectedItem, curAccessRightType } = this.state;
        const privilegesRight = [{ accessRightCd: 'F9999', accessRightName: 'Privileges' }];
        const totalMenuLength = allMatchTypeMenuList.length;

        return (
            <Grid container >
                <AppBar
                    className={classes.appBar}
                >
                    <Typography variant="h6" color="inherit" noWrap>
                        Access Control
                  </Typography>
                </AppBar>
                <Drawer
                    open
                    variant={'permanent'}
                    // className={classes.drawerRoot}
                    classes={{
                        root: classes.drawerRoot,
                        paper: classes.drawerPaperRoot
                    }}
                >
                    <List>
                        {allMatchTypeMenuList.map((right, idx) => {
                            return (
                                <ListItem
                                    className={classes.accessRightGpListItem}
                                    id={`userRole_accessRight_panel_accessRightGroup_listItem_${idx}`}
                                    key={idx}
                                    // className={this.state.selectUser === index ? classes.listSelectItem : classes.listItem}
                                    button
                                    value={right.accessRightCd}
                                    disabled={userRoleStatus.disabelUserRoleForm}
                                    onClick={() => { this.handleAccessRightGPClick(right, idx); }}
                                    selected={curSelectedItem === idx}
                                >
                                    {/* {right.icon ? <Avatar src={`data:image/gif;base64,${right.icon}`} className={classes.menuAvatarRoot} /> : null} */}
                                    {/* <ListItemIcon>

                                    </ListItemIcon> */}
                                    <ListItemText
                                        primary={<div dangerouslySetInnerHTML={{ __html: right.accessRightName }}></div>}
                                    />
                                </ListItem>
                            );
                        })}
                    </List>
                    <Divider />
                    <List>
                        {privilegesRight.map(right => {
                            return (
                                <ListItem
                                    className={classes.accessRightGpListItem}
                                    id={`userRole_accessRight_panel_accessRightGroup_listItem_${totalMenuLength}`}
                                    key={totalMenuLength}
                                    button
                                    value={totalMenuLength}
                                    disabled={userRoleStatus.disabelUserRoleForm}
                                    onClick={() => { this.handleAccessRightGPClick(right, totalMenuLength); }}
                                    selected={curSelectedItem === totalMenuLength}
                                >
                                    {/* <ListItemIcon
                                    >
                                        <FolderSpecialIcon />
                                    </ListItemIcon> */}
                                    <ListItemText
                                        primary={<div dangerouslySetInnerHTML={{ __html: right.accessRightName }}></div>}
                                    />
                                </ListItem>
                            );
                        })}

                    </List>
                </Drawer>
                <Grid container className={classes.accessRightContent}>
                    <AccessRightList
                        id={'functionAccessRight'}
                        roleClass={classes}
                        disabled={userRoleStatus.disabelUserRoleForm}
                        allMenuList={curSelectedMenu}
                        openMenu={this.handleOpenMenu}
                        selectAccessRight={this.handleAccessRightChecked}
                        accessRights={userRoleStatus.accessRights}
                        accessRightType={curAccessRightType}
                    />
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(UserRoleAccessRightPanel);