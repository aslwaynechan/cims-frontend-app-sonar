import React from 'react';
import {
    InputBase,
    Grid,
    List,
    ListItem,
    ListItemText,
    Typography,
    IconButton,
    CircularProgress,
    Paper
} from '@material-ui/core';
import { KeyboardArrowRight, KeyboardArrowLeft, Search } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
import _ from 'lodash';


const styles = theme => ({
    searchInputRoot: {
        padding: '2px 0px',
        display: 'flex',
        alignItems: 'center',
        borderRadius: '15px',
        border: '1px solid rgba(0,0,0,0.42)',
        height: 26,
        width: '100%'
    },
    searchInput: {
        marginLeft: 8,
        flex: 1,
        fontSize: '12pt'
    },
    list: {
        border: '1px solid #ccc',
        borderRadius: 6,
        backgroundColor: theme.palette.background.paper,
        overflow: 'auto',
        maxHeight: 'calc(100vh * 0.38)',
        minHeight: 'calc(100vh * 0.38)',
        height: '100%'
    },
    listNav: {
        width: '100%'
    },
    listItem: {
        paddingTop: 4,
        paddingBottom: 4
    },
    listSelectItem: {
        paddingTop: 4,
        paddingBottom: 4,
        background: '#ccc'
    },
    grid: {
        marginTop: 5,
        marginBottom: 5
    },
    h6Title: {
        marginTop: 4,
        marginBottom: 4
    },
    userMemberTitle: {
        marginTop: 4,
        marginBottom: 4,
        textDecorationLine: 'underline'
    },
    button: {
        margin: theme.spacing(1),
        textTransform: 'none'
    },
    buttonLabel: {
        fontSize: 12
    },
    icon: {
        margin: theme.spacing(2)
    },
    searchInputContainer: {
        margin: '5px 0px'
    }
});


class UserRoleMember extends React.Component {
    state = {
        searching: false,
        searchValue: '',
        searchedValue: '',
        selectUser: null,
        selectDeleteUser: null,
        unassignedUsers: [],
        assignedUsers: [],
        fullUserSearchValue: '',
        selectedUserSearchValue: '',
        fullUserMatchValue: '',
        selectedMatchValue: ''
    }

    UNSAFE_componentWillMount() {
    }

    componentDidMount() {
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.unassignedUsers !== this.props.unassignedUsers || nextProps.assignedUsers !== this.props.assignedUsers) {
            nextState.searching = false;
            nextState.searchValue = '';
            nextState.searchedValue = '';
            nextState.selectUser = null;
            nextState.selectDeleteUser = null;
            nextState.unassignedUsers = nextProps.unassignedUsers || [];
            nextState.assignedUsers = nextProps.assignedUsers || [];
            nextState.fullUserSearchValue = '';
            nextState.selectedUserSearchValue = '';
            nextState.fullUserMatchValue = '';
            nextState.selectedMatchValue = '';
            this.store.unassignedUsers = _.cloneDeep(nextProps.unassignedUsers);
            this.store.assignedUsers = _.cloneDeep(nextProps.assignedUsers);
        }
        return true;

    }

    componentWillUnmount() {
    }

    store = {
        unassignedUsers: [],
        assignedUsers: []
    }


    handleToggle = e => {
        let value = e.target.value;
        let name = e.target.name;
        value = value.replace(/(\s)\1+/g, '$1');

        if (name === 'fullUserSearchInput') {
            this.setState({ fullUserSearchValue: value || '' }, () => {
                if (value && value !== '' && value.length >= 4) {
                    this.filterFullUser();
                }
                else if (value === '') {
                    let unassignedUsers = this.store.unassignedUsers;
                    this.setState({ unassignedUsers });
                }
            });
        }
        else if (name === 'selectedUserSearchInput') {
            this.setState({ selectedUserSearchValue: value || '' }, () => {
                if (value && value !== '' && value.length >= 4) {
                    this.filterSelectedUser();
                }
                else if (value === '') {
                    let assignedUsers = this.store.assignedUsers;
                    this.setState({ assignedUsers });
                }
            });
        }
    };

    search = () => {
        let searchValue = this.state.searchValue || '';
        let searchResult = this.store.unassignedUsers.filter(function (p) {
            return p.engGivenName.toString().toLowerCase().indexOf(searchValue.toLowerCase()) !== -1 || p.engSurname.toString().toLowerCase().indexOf(searchValue.toLowerCase()) !== -1;
        });
        this.setState({ unassignedUsers: searchResult, selectUser: null, selectDeleteUser: null, searchedValue: searchValue });
    }

    filterFullUser = () => {
        let searchValue = this.state.fullUserSearchValue || '';
        let searchResult = this.store.unassignedUsers.filter(function (p) {
            return p.engGivenName.toString().toLowerCase().indexOf(searchValue.toLowerCase()) !== -1 || p.engSurname.toString().toLowerCase().indexOf(searchValue.toLowerCase()) !== -1;
        });
        this.setState({ unassignedUsers: searchResult, selectUser: null, selectDeleteUser: null, fullUserMatchValue: searchValue });
    }

    filterSelectedUser = () => {
        let searchValue = this.state.selectedUserSearchValue || '';
        let searchResult = this.store.assignedUsers.filter(function (p) {
            return p.engGivenName.toString().toLowerCase().indexOf(searchValue.toLowerCase()) !== -1 || p.engSurname.toString().toLowerCase().indexOf(searchValue.toLowerCase()) !== -1;
        });
        // this.setState({ unassignedUsers: searchResult, selectUser: null, selectDeleteUser: null, searchedValue: searchValue });
        this.setState({ assignedUsers: searchResult, selectUser: null, selectDeleteUser: null, selectedMatchValue: searchValue });
    }

    addUserInRole = () => {
        let { assignedUsers, unassignedUsers, selectUser } = this.state;
        selectUser = selectUser || 0;
        if (unassignedUsers.length > 0) {
            let selectUserJson = _.cloneDeep(unassignedUsers[selectUser]);
            let assignedUser = assignedUsers.find(user => user.userId === selectUserJson.userId);
            if (!assignedUser) {
                assignedUsers.push(unassignedUsers[selectUser]);
                // unassignedUsers.splice(selectUser, 1);
                this.setState({ assignedUsers: assignedUsers, unassignedUsers: unassignedUsers, selectUser: null, selectDeleteUser: null });
                this.store.assignedUsers.push(selectUserJson);
                this.store.unassignedUsers = this.store.unassignedUsers.filter(function (item) { return item.userId !== selectUserJson.userId; });
            }
        }
    }
    deleteUserInRole = () => {
        let { assignedUsers, unassignedUsers, selectDeleteUser } = this.state;
        selectDeleteUser = selectDeleteUser || 0;
        if (assignedUsers.length > 0) {
            let selectDeleteUserJson = _.cloneDeep(assignedUsers[selectDeleteUser]);
            //  unassignedUsers.push(assignedUsers[selectDeleteUser]);
            assignedUsers.splice(selectDeleteUser, 1);
            this.setState({ assignedUsers: assignedUsers, unassignedUsers: unassignedUsers, selectUser: null, selectDeleteUser: null });
            this.store.assignedUsers = this.store.assignedUsers.filter(function (item) { return item.userId !== selectDeleteUserJson.userId; });
            this.store.unassignedUsers.push(selectDeleteUserJson);
        }
    }

    highlight = (targetStr, highlightStr) => {
        if (!highlightStr) {
            return targetStr;
        } else {
            let lowerTargetStr = targetStr.toLowerCase();
            let lowerHighlightStr = highlightStr.toLowerCase();
            let reTargetStr = '';
            let preIndex = 0;
            for (let i = 0; i >= 0;) {
                i = lowerTargetStr.indexOf(lowerHighlightStr, i);
                if (i !== -1) {
                    reTargetStr += targetStr.substring(preIndex, i) + '<font style="color: #0579c8">';
                    reTargetStr += targetStr.substr(i, highlightStr.length) + '</font>';
                    i += highlightStr.length;
                    preIndex = i;
                } else {
                    reTargetStr += targetStr.substring(preIndex, targetStr.length);
                }
            }
            return reTargetStr;
        }

    }

    keyDown = e => {
        let targetName = e.target.name;
        if (e.keyCode === 13) {
            if (targetName === 'fullUserSearchInput') {
                this.filterFullUser(e);
            }
            else if (targetName === 'selectedUserSearchInput') {
                this.filterSelectedUser(e);
            }

        }
    };

    render() {
        const { classes, disabled } = this.props;
        return (
            <Grid container>
                <Grid container id={this.props.id}>
                    <Grid container><Typography variant="h6" className={classes.h6Title}>User Role Member</Typography></Grid>
                </Grid>
                <Grid container alignItems="center">
                    <Grid item container xs={5} style={{ height: '100%' }}>
                        <Grid container><Typography className={classes.userMemberTitle}>Full User List</Typography></Grid>
                        <Grid item container className={classes.searchInputContainer}>
                            <Paper className={classes.searchInputRoot} elevation={1}>
                                <InputBase
                                    id={this.props.id + 'FullUserSearchInput'}
                                    name={'fullUserSearchInput'}
                                    className={classes.searchInput}
                                    autoComplete={'off'}
                                    onChange={this.handleToggle}
                                    placeholder={this.props.inputPlaceHolder}
                                    value={this.state.fullUserSearchValue}
                                    disabled={disabled}
                                    onKeyDown={this.keyDown}
                                />
                                <IconButton
                                    id={this.props.id + 'FullUserSearchButton'}
                                    onClick={this.filterFullUser}
                                    className={classes.iconButton}
                                    aria-label="Search"
                                    color={'primary'}
                                    disabled={disabled}
                                >
                                    {this.state.searching ? (
                                        <CircularProgress size={20} />
                                    ) : (
                                            <Search />
                                        )}
                                </IconButton>
                            </Paper>
                        </Grid>
                        <Grid item container className={classes.list}>
                            <List component="nav" className={classes.listNav} id={this.props.id + 'UnassignedUsersList'} >
                                {this.state.unassignedUsers.map((item, index) => {
                                    let listItemText = `${item.engSurname},${item.engGivenName}`;
                                    return (
                                        <ListItem
                                            id={this.props.id + 'UnassignedUsersListItem' + index}
                                            key={index}
                                            className={classes.listItem}
                                            button
                                            value={item.userId}
                                            disabled={disabled}
                                            onClick={() => { this.setState({ selectUser: index, selectDeleteUser: null }); }}
                                            selected={this.state.selectUser === index}
                                        >
                                            <ListItemText
                                                primary={<div dangerouslySetInnerHTML={{ __html: listItemText }}></div>}
                                            />
                                        </ListItem>
                                    );
                                }
                                )}
                            </List>
                        </Grid>
                    </Grid>

                    <Grid item container xs={1} >
                        <IconButton
                            id={this.props.id + 'MoveLeftListToRightListButton'}
                            style={{ flex: 1, margin: 0 }}
                            color="primary"
                            className={classes.button}
                            aria-label="keyboard_arrow_right"
                            onClick={this.addUserInRole}
                            disabled={disabled}
                        >
                            <KeyboardArrowRight />
                        </IconButton>
                        <IconButton
                            id={this.props.id + 'MoveRightListToLeftListButton'}
                            style={{ flex: 1, margin: 0 }}
                            color="primary"
                            className={classes.button}
                            aria-label="keyboard_arrow_left"
                            onClick={this.deleteUserInRole}
                            disabled={disabled}
                        >
                            <KeyboardArrowLeft />
                        </IconButton>
                    </Grid>

                    <Grid item container xs={5} style={{ height: '100%' }}>
                        <Grid container><Typography className={classes.userMemberTitle}>Selected List</Typography></Grid>
                        <Grid item container className={classes.searchInputContainer}>
                            <Paper className={classes.searchInputRoot} elevation={1}>
                                <InputBase
                                    id={this.props.id + 'SelectedUserSearchInput'}
                                    name={'selectedUserSearchInput'}
                                    className={classes.searchInput}
                                    autoComplete={'off'}
                                    onChange={this.handleToggle}
                                    placeholder={this.props.inputPlaceHolder}
                                    value={this.state.selectedUserSearchValue}
                                    disabled={disabled}
                                    onKeyDown={this.keyDown}
                                />
                                <IconButton
                                    id={this.props.id + 'SelectedUserSearchButton'}
                                    onClick={this.filterSelectedUser}
                                    className={classes.iconButton}
                                    aria-label="Search"
                                    color={'primary'}
                                    disabled={disabled}
                                >
                                    {this.state.searching ? (
                                        <CircularProgress size={20} />
                                    ) : (
                                            <Search />
                                        )}
                                </IconButton>
                            </Paper>
                        </Grid>
                        <Grid item container className={classes.list}>
                            <List component="nav" className={classes.listNav} id={this.props.id + 'AssignedUsersList'}>
                                {this.state.assignedUsers.map((item, index) => {
                                    let listItemText = `${item.engSurname},${item.engGivenName}`;
                                    return (
                                        <ListItem
                                            id={this.props.id + 'AssignedUsersListItem' + index}
                                            key={index}
                                            className={classes.listItem}
                                            button
                                            value={item.userId}
                                            disabled={disabled}
                                            onClick={() => { this.setState({ selectUser: null, selectDeleteUser: index }); }}
                                            selected={this.state.selectDeleteUser === index}
                                        >
                                            <ListItemText
                                                primary={<div dangerouslySetInnerHTML={{ __html: listItemText }}></div>}
                                            />
                                        </ListItem>
                                    );
                                }
                                )}
                            </List>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

export default (withStyles(styles)(UserRoleMember));