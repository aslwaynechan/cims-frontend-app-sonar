import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    Grid,
    Typography,
    Switch,
    TextField,
    FormControlLabel
} from '@material-ui/core';
import TextFieldValidator from '../../../../components/FormValidator/TextFieldValidator';
import SelectFieldValidator from '../../../../components/FormValidator/SelectFieldValidator';
import ValidatorEnum from '../../../../enums/validatorEnum';
import CommonMessage from '../../../../constants/commonMessage';
// import UserRoleMember from '../userRoleMember';
import * as CommonUtilities from '../../../../utilities/commonUtilities';
import CommonRegex from '../../../../constants/commonRegex';

const styles = theme => ({
    root: {
        height: 'calc(100vh - 105px)'
    },
    list: {
        flex: 1,
        height: 200,
        //maxWidth: 360,
        border: '1px solid #ccc',
        borderRadius: 6,
        backgroundColor: theme.palette.background.paper
    },
    listItem: {
        paddingTop: 4,
        paddingBottom: 4
    },
    grid: {
        marginTop: 5,
        marginBottom: 5
    },
    h6Title: {
        marginTop: 4,
        marginBottom: 4
    },
    button: {
        margin: theme.spacing(1),
        textTransform: 'none'
    },
    buttonLabel: {
        fontSize: 12
    },
    icon: {
        margin: theme.spacing(2)
    },
    accessRightGrid: {
        height: 'calc(100% - 76px)',
        overflow: 'auto',
        borderTop: '1px solid #e6e6e6',
        marginLeft: 4,
        paddingTop: 4
    }
});

function RequiredTips() {
    return (
        <span style={{ color: 'red' }}>*</span>
    );
}

class UserRoleBasicInfoPanel extends React.Component {

    handleChange = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        if (name === 'userRoleName') {
            let reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
            if (reg.test(value)) {
                return;
            }
        }

        if (e.target.getAttribute('maxlength')) {
            value = value.slice(0, e.target.getAttribute('maxlength'));
        }
        this.props.updateField(name, value);
    }
    handleBlur = (e) => {
        let name = e.target.name;
        let value = e.target.value.trim();
        this.props.updateField(name, value);
    }
    handleSelectChange = (e, name) => {
        let value = e != null ? e.value : null;
        if (name === 'statusInd') {
            value = this.props.userRoleStatus.statusInd === 'Y' ? 'N' : 'Y';
        }
        this.props.updateField(name, value);
    }

    render() {
        const { classes, userRoleStatus, serviceList, serviceCd } = this.props;
        const serviceName = CommonUtilities.getServiceNameByServiceCd(serviceCd, serviceList);

        return (
            <Grid item container direction="column">
                <Grid item container>
                    <Typography variant="h6" className={classes.h6Title}>Basic Information</Typography>
                    <Grid container className={classes.grid}>
                        <Grid item xs={3}>
                            <Typography variant="subtitle1">Service</Typography>
                        </Grid>
                        <Grid item xs={9}>
                            <TextField
                                id="userRoleServiceTextField"
                                style={{ margin: 0, padding: 0, width: '73%' }}
                                disabled
                                // value={this.props.serviceCd || ''}
                                value={serviceName}
                                margin="normal"
                            />
                        </Grid>
                    </Grid>

                    <Grid container className={classes.grid}>
                        <Grid item xs={3}>
                            <Typography variant="subtitle1">User Role Name<RequiredTips /></Typography>
                        </Grid>
                        <Grid item xs={9}>
                            <TextFieldValidator
                                id={'userRoleNameTextFiled'}
                                upperCase
                                fullWidth
                                validByBlur
                                /* eslint-disable */
                                InputProps={{
                                    className: 'input_field'
                                }}
                                inputProps={{
                                    maxLength: 50
                                }}
                                /* eslint-enable */
                                msgPosition="right"
                                name="userRoleName"
                                validators={[
                                    ValidatorEnum.required,
                                    ValidatorEnum.isSpecialEnglish
                                ]}
                                errorMessages={[
                                    CommonMessage.VALIDATION_NOTE_REQUIRED(),
                                    CommonMessage.VALIDATION_NOTE_SPECIAL_ENGLISH()
                                ]}
                                warning={[
                                    ValidatorEnum.isEnglishWarningChar
                                ]}
                                warningMessages={[
                                    CommonMessage.WARNING_NOTE_SPECIAL_ENGLISH()
                                ]}
                                disabled={userRoleStatus.disabelUserRoleForm}
                                value={userRoleStatus.userRoleName || ''}
                                onChange={this.handleChange}
                                onBlur={this.handleBlur}
                            />
                        </Grid>
                    </Grid>

                    <Grid container className={classes.grid}>
                        <Grid item xs={3}>
                            <Typography variant="subtitle1">User Role Description<RequiredTips /></Typography>
                        </Grid>
                        <Grid item xs={9}>
                            <TextFieldValidator
                                id={'userRoleDescriptionTextField'}
                                fullWidth
                                validByBlur
                                /* eslint-disable */
                                InputProps={{
                                    className: 'input_field'
                                }}
                                inputProps={{
                                    maxLength: 130
                                }}
                                /* eslint-enable */
                                name="userRoleDesc"
                                msgPosition="right"
                                validators={[ValidatorEnum.required]}
                                errorMessages={['User Role Description, This field is required.']}
                                disabled={userRoleStatus.disabelUserRoleForm}
                                value={userRoleStatus.userRoleDesc || ''}
                                onChange={this.handleChange}
                                onBlur={this.handleBlur}
                            />
                        </Grid>
                    </Grid>

                    <Grid container className={classes.grid}>
                        <Grid item xs={3}>
                            <Typography variant="subtitle1">Copy from</Typography>
                        </Grid>
                        <Grid item xs={9}>
                            <SelectFieldValidator
                                id={'userRoleCopyFromSelectField'}
                                options={userRoleStatus.replicableRole.map((item) => ({ value: item.userRoleId, label: item.userRoleName }))}
                                msgPosition="right"
                                isDisabled={userRoleStatus.disabelUserRoleForm}
                                value={userRoleStatus.replicableRoleSelect}
                                addNullOption
                                onChange={(e) => this.handleSelectChange(e, 'replicableRoleSelect')}
                            />
                        </Grid>
                    </Grid>

                    <Grid container className={classes.grid} style={{ display: userRoleStatus.showActiveButton ? 'flex' : 'none' }}>
                        <Grid item xs={3}>
                            <Typography variant="subtitle1">Active/Inactive</Typography>
                        </Grid>
                        <Grid item xs={9}>
                            <FormControlLabel
                                labelPlacement={'end'}
                                // color
                                disabled={userRoleStatus.disabelUserRoleForm}
                                control={
                                    <Switch
                                        id={'userRoleActiveSwitch'}
                                        style={{ height: 20 }}
                                        checked={userRoleStatus.statusInd === 'Y'}
                                        onChange={(e) => this.handleSelectChange(e, 'statusInd')}
                                        value="Y"
                                        color="primary"
                                    />
                                }
                                label={userRoleStatus.statusInd === 'Y' ? <font color={userRoleStatus.disabelUserRoleForm ? null : '#0579c8'}>Active</font> : <font>Inactive</font>}
                            />
                        </Grid>
                    </Grid>

                </Grid>

            </Grid>
        );
    }
}

export default withStyles(styles)(UserRoleBasicInfoPanel);