import React from 'react';
import { connect } from 'react-redux';
import SearchInput from '../../compontent/searchInput';
import {
    FormControl,
    Grid,
    Typography
} from '@material-ui/core';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import CIMSButton from '../../../components/Buttons/CIMSButton';
import { withStyles } from '@material-ui/core/styles';
import accessRightEnum from '../../../enums/accessRightEnum';
import {
    resetAll,
    createUserRole,
    editUserRole,
    searchUserRole,
    getUserRoleById,
    saveUserRole,
    updateField,
    searchAccessRight,
    openMenu,
    selectAccessRight,
    cancelEdit
} from '../../../store/actions/administration/userRole/userRoleAction';
import { openCommonMessage, closeCommonMessage } from '../../../store/actions/message/messageAction';
import EditModeMiddleware from '../../compontent/editModeMiddleware';
import CIMSButtonGroup from '../../../components/Buttons/CIMSButtonGroup';
import UserRoleBasicInfoPanel from './component/userRoleBasicInfoPanel';
import UserRoleAccessRightPanel from './component/userRoleAccessRightPanel';
import UserRoleMember from './component/userRoleMember';

const styles = theme => ({
    root: {
        height: 'calc(100vh - 105px)'
    },
    list: {
        flex: 1,
        height: 200,
        //maxWidth: 360,
        border: '1px solid #ccc',
        borderRadius: 6,
        backgroundColor: theme.palette.background.paper
    },
    listItem: {
        paddingTop: 4,
        paddingBottom: 4
    },
    grid: {
        marginTop: 5,
        marginBottom: 5
    },
    h6Title: {
        marginTop: 4,
        marginBottom: 4
    },
    button: {
        margin: theme.spacing(1),
        textTransform: 'none'
    },
    buttonLabel: {
        fontSize: 12
    },
    icon: {
        margin: theme.spacing(2)
    },
    accessRightGrid: {
        height: 'calc(100% - 76px)',
        overflow: 'auto',
        borderTop: '1px solid #e6e6e6',
        marginLeft: 4,
        paddingTop: 4
    }
});

class UserProfile extends React.Component {

    state = { tabValue: 0 }

    UNSAFE_componentWillMount() {
        this.props.resetAll();
    }

    componentDidMount() {
        this.props.ensureDidMount();
        let accessRightParams = { functionType: '', statusCd: 'Y' };
        this.props.searchAccessRight(accessRightParams);
    }

    componentDidUpdate() {
        if (this.props.userRoleStatus.saveSuccess === true) {
            const params = {
                msgCode: '110021',
                btnActions: {
                    btn1Click: () => {
                        this.props.cancelEdit();
                        this.props.closeCommonMessage();
                    }
                }
            };
            this.props.openCommonMessage(params);
        }
    }

    componentWillUnmount() {
        this.props.resetAll();
    }

    handleSearchUserRole = value => {
        const params = { userRoleName: value };
        this.props.searchUserRole(params);
    }

    handleOnSelectUserRole = item => {
        if (item != null && item.userRoleId != null) {
            let params = { userRoleId: item.userRoleId };
            this.props.getUserRoleById(params);
        }
    }

    changeTabValue = (event, value) => {
        this.setState({ tabValue: value });
    }


    handleOnSubmit = () => {
        let submitData = {
            statusInd: this.props.userRoleStatus.statusInd,
            userRoleDesc: this.props.userRoleStatus.userRoleDesc,
            userRoleId: this.props.userRoleStatus.currentRoleId,
            userRoleName: this.props.userRoleStatus.userRoleName,
            // users: this.refs.users.props.assignedUsers,
            userDtos: this.userRoleBasicInfoPanelRef.props.userRoleStatus.assignedUsers,
            version: this.props.userRoleStatus.version,
            accessRights: this.props.userRoleStatus.accessRights,
            landingPage: isNaN(this.props.userRoleStatus.landingSuccessPage) ? null : this.props.userRoleStatus.landingSuccessPage
        };
        this.props.saveUserRole(submitData);
        this.userRoleAccessRightPanelRef.resetCurSelectedItem();
    }

    handleCancel = () => {
        this.props.cancelEdit();
        this.refs.form.resetValidations();
        this.userRoleAccessRightPanelRef.resetCurSelectedItem();
    }

    handleCreate = () => {
        this.props.createUserRole();
    }
    handleEdit = () => {
        this.props.editUserRole();
    }
    handleOpenMenu = (accessRightCd) => {
        this.props.openMenu(accessRightCd);
    }
    handleAccessRightChecked = (accessRights) => {
        this.props.selectAccessRight(accessRights);
    }
    render() {
        const { classes, userRoleStatus, serviceList} = this.props;
        return (
            <Grid className={classes.root} >
                {!userRoleStatus.showUserRoleSearch ? null :
                    <Grid container alignItems="center" justify="space-between">
                        <Typography component="div">
                            <SearchInput
                                id="userRoleSearchInput"
                                limitValue={4}
                                displayField={['userRoleName']}
                                inputPlaceHolder="Search User Role"
                                upperCase
                                onChange={this.handleSearchUserRole}
                                dataList={userRoleStatus.searchUserRoleList}
                                onSelectItem={this.handleOnSelectUserRole}
                            />
                        </Typography>
                        <Typography component="div">
                            <CIMSButton
                                id={'userRoleCreateButton'}
                                variant="contained"
                                color="primary"
                                size="small"
                                onClick={this.handleCreate}
                            >Create User Role</CIMSButton>
                            <CIMSButton
                                id={'userRoleEditButton'}
                                disabled={userRoleStatus.disabelEditButton}
                                variant="contained"
                                color="primary"
                                size="small"
                                onClick={this.handleEdit}
                            >Edit User Role</CIMSButton>
                        </Typography>
                    </Grid>

                }
                <Grid container direction={'row'}>
                    <Grid container item xs={6}>
                        <FormControl fullWidth>
                            <ValidatorForm
                                ref="form"
                                onSubmit={this.handleOnSubmit}
                                style={{ height: '35%', flexFlow: 'column', display: userRoleStatus.showUserRoleForm ? 'flex' : 'none' }}
                            >
                                <Grid container>
                                    <UserRoleBasicInfoPanel
                                        innerRef={ref => this.userRoleBasicInfoPanelRef = ref}
                                        serviceCd={this.props.serviceCd}
                                        userRoleStatus={userRoleStatus}
                                        serviceList={serviceList}
                                        updateField={this.props.updateField}
                                    />
                                </Grid>
                            </ValidatorForm>
                        </FormControl>
                        <Grid item container style={{ display: userRoleStatus.showUserRoleForm ? 'flex' : 'none' }}>
                            <UserRoleMember
                                id={'userRoleMember'}
                                ref="users"
                                inputPlaceHolder="Search User"
                                display={'name'}
                                value={'id'}
                                unassignedUsers={userRoleStatus.unassignedUsers}
                                assignedUsers={userRoleStatus.assignedUsers}
                                disabled={userRoleStatus.disabelUserRoleForm}
                                updateField={this.props.updateField}
                                fullUserSearchValue={userRoleStatus.fullUserSearchUser}
                                selectedUserSearchUser={userRoleStatus.selectedUserSearchUser}
                            />
                        </Grid>
                    </Grid>
                    <Grid container item xs={6} style={{ display: userRoleStatus.showUserRoleForm ? 'flex' : 'none' }}>
                        <UserRoleAccessRightPanel
                            innerRef={ref => this.userRoleAccessRightPanelRef = ref}
                            userRoleStatus={userRoleStatus}
                            openMenu={this.props.openMenu}
                            selectAccessRight={this.props.selectAccessRight}
                            accessRights={this.props.accessRights}
                        />
                    </Grid>
                </Grid>
                <CIMSButtonGroup
                    buttonConfig={
                        [
                            {
                                id: 'userRoleSaveButton',
                                name: 'Save',
                                disabled: userRoleStatus.disabelSaveButton,
                                onClick: () => this.refs.form.submit()
                            },
                            {
                                id: 'userRoleCancelButton',
                                name: 'Cancel',
                                onClick: this.handleCancel
                            }
                        ]
                    }
                />
                <EditModeMiddleware componentName={accessRightEnum.userRole} when={!this.props.userRoleStatus.disabelSaveButton} />
            </Grid>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        serviceCd: state.login.service.serviceCd,
        userRoleStatus: state.userRole,
        serviceList: state.common.serviceList,
        accessRights: state.login.accessRights
    };
};

const mapDispatchToProps = {
    searchUserRole,
    getUserRoleById,
    resetAll,
    createUserRole,
    editUserRole,
    saveUserRole,
    updateField,
    searchAccessRight,
    openMenu,
    selectAccessRight,
    cancelEdit,
    openCommonMessage,
    closeCommonMessage
};


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(UserProfile));