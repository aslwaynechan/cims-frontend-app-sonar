import React from 'react';
import { connect } from 'react-redux';
import {
    Tabs,
    Tab,
    Typography,
    Grid
} from '@material-ui/core';
import UserProfile from './userProfile/userProfile';
import ChangePassword from './changePassword/changePassword';

class UserProfileMaintenance extends React.Component {

    state = {
        tabValue: 0
    }

    componentDidMount() {
        this.props.ensureDidMount();
    }

    changeTabValue = (event, value) => {
        this.setState({ tabValue: value });
    }

    render() {
        return (
            <Grid>
                <Tabs
                    value={this.state.tabValue}
                    onChange={this.changeTabValue}
                    indicatorColor={'primary'}
                >
                    {this.props.isTemporaryLogin ? null :
                        <Tab id={'administrationTabUserProfileMaintenance'} label={<Typography style={{ fontSize: 16, textTransform: 'none' }}>User Profile Maintenance</Typography>} className={this.state.tabValue === 0 ? 'tabSelected' : 'tabNavigation'} />
                    }
                    <Tab id={'administrationTabChangePassword'} label={<Typography style={{ fontSize: 16, textTransform: 'none' }}>Change Password</Typography>} className={this.state.tabValue === 1 ? 'tabSelected' : 'tabNavigation'} />
                </Tabs>
                <Grid style={{ padding: '0px 10px' }}>
                    {this.props.isTemporaryLogin ? null :
                        <Typography component={'div'} style={{ display: this.state.tabValue === 0 ? 'inline' : 'none' }}>
                            <Grid>
                                <UserProfile />
                            </Grid>
                        </Typography>
                    }
                    <Typography component={'div'} style={{ display: (this.state.tabValue === 1 || this.props.isTemporaryLogin) ? 'inline' : 'none' }}>
                        <Grid>
                            <ChangePassword />
                        </Grid>
                    </Typography>
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isTemporaryLogin: state.login.isTemporaryLogin === 'Y'
    };
};

const mapDispatchToProps = {

};

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileMaintenance);