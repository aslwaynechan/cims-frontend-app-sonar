import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    Grid,
    Typography
} from '@material-ui/core';
import DateFieldValidator from '../../../../components/FormValidator/DateFieldValidator';
import TextFieldValidator from '../../../../components/FormValidator/TextFieldValidator';
import SelectFieldValidator from '../../../../components/FormValidator/SelectFieldValidator';
import Enum from '../../../../enums/enum';
import ValidatorEnum from '../../../../enums/validatorEnum';
import CommonMessage from '../../../../constants/commonMessage';
import RequiredIcon from '../../../../components/InputLabel/RequiredIcon';

const styles = () => ({
    grid: {
        margin: '8px 0px'
    },
    h6Title: {
        marginTop: 0,
        marginBottom: 8
    },
    button: {
        margin: 4,
        textTransform: 'none'
    },
    buttonLabel: {
        fontSize: 12
    }
});

class UserProfileBasicInfoPanel extends React.Component {

    render() {
        const { classes, userSearchData, isInputDisabled } = this.props;
        const id = this.props.id ? this.props.id : 'user_profile_basic_info_panel';

        const fieldValidators = {
            salutation: [],
            engSurName: [ValidatorEnum.required, ValidatorEnum.isEnglishOrSpace],
            engGivenName: [ValidatorEnum.required, ValidatorEnum.isEnglishOrSpace],
            chineseName: [ValidatorEnum.isChinese],
            loginName: [ValidatorEnum.required],
            email: [ValidatorEnum.required, ValidatorEnum.isEmail],
            gender: [ValidatorEnum.required],
            contactPhone: [ValidatorEnum.required, ValidatorEnum.isNumber, ValidatorEnum.phoneNo, ValidatorEnum.minStringLength(8)],
            position: [ValidatorEnum.required],
            supervisor: [],
            accountExpiryDate: [],
            accountStatus: [ValidatorEnum.required]
        };

        const fieldErrorMessages = {
            salutation: [],
            engSurName: [
                CommonMessage.VALIDATION_NOTE_REQUIRED(),
                CommonMessage.VALIDATION_NOTE_ENGLISHFIELD()
            ],
            engGivenName: [
                CommonMessage.VALIDATION_NOTE_REQUIRED(),
                CommonMessage.VALIDATION_NOTE_ENGLISHFIELD()
            ],
            chineseName: [CommonMessage.VALIDATION_NOTE_CHINESEFIELD()],
            loginName: [CommonMessage.VALIDATION_NOTE_REQUIRED()],
            email: [
                CommonMessage.VALIDATION_NOTE_REQUIRED(),
                CommonMessage.VALIDATION_NOTE_EMAILFIELD()
            ],
            gender: [CommonMessage.VALIDATION_NOTE_REQUIRED()],
            contactPhone: [
                CommonMessage.VALIDATION_NOTE_REQUIRED(),
                CommonMessage.VALIDATION_NOTE_NUMBERFIELD(),
                CommonMessage.VALIDATION_NOTE_PHONE_NO(),
                CommonMessage.VALIDATION_NOTE_PHONE_BELOWMINWIDTH()
            ],
            position: [CommonMessage.VALIDATION_NOTE_REQUIRED()],
            supervisor: [],
            accountExpiryDate: [],
            accountStatus: [CommonMessage.VALIDATION_NOTE_REQUIRED()]
        };
        return (
            <Grid item container xs={12} >
                <Typography variant="h6" className={classes.h6Title}>Basic Information</Typography>
                <Grid container className={classes.grid}>
                    <Grid item xs={12}>
                        <SelectFieldValidator
                            id={`${id}_salutation`}
                            options={Enum.COMMON_SALUTATION.map((item) => ({ value: item.code, label: item.engDesc }))}
                            msgPosition={'bottom'}
                            validators={fieldValidators.salutation}
                            errorMessages={fieldErrorMessages.salutation}
                            isDisabled={isInputDisabled}
                            value={userSearchData.salutation}
                            onChange={(e) => this.props.handleSelectChange(e, 'salutation')}
                            TextFieldProps={{
                                variant: 'outlined',
                                label: <>Salutation</>
                            }}
                            addNullOption
                        />
                    </Grid>
                </Grid>

                <Grid container className={classes.grid}>
                    <Grid item xs={12}>
                        <TextFieldValidator
                            id={`${id}_surname`}
                            upperCase
                            fullWidth
                            onlyOneSpace
                            validByBlur
                            /* eslint-disable */
                            InputProps={{
                                className: 'input_field'
                            }}
                            inputProps={{
                                maxLength: 40
                            }}
                            /* eslint-enable */
                            msgPosition={'bottom'}
                            name="engSurname"
                            validators={[
                                ValidatorEnum.required,
                                ValidatorEnum.isSpecialEnglish
                            ]}
                            errorMessages={[
                                CommonMessage.VALIDATION_NOTE_REQUIRED(),
                                CommonMessage.VALIDATION_NOTE_SPECIAL_ENGLISH()
                            ]}
                            warning={[
                                ValidatorEnum.isEnglishWarningChar
                            ]}
                            warningMessages={[
                                CommonMessage.WARNING_NOTE_SPECIAL_ENGLISH()
                            ]}
                            disabled={isInputDisabled}
                            value={userSearchData.engSurname}
                            onChange={this.props.handleChange}
                            label={<>Surname<RequiredIcon /></>}
                            variant={'outlined'}
                            trim={'all'}
                        />
                    </Grid>
                </Grid>

                <Grid container className={classes.grid}>
                    <Grid item xs={12}>
                        <TextFieldValidator
                            id={`${id}_given_name`}
                            fullWidth
                            upperCase
                            onlyOneSpace
                            validByBlur
                            /* eslint-disable */
                            InputProps={{
                                className: 'input_field'
                            }}
                            inputProps={{
                                maxLength: 40
                            }}
                            /* eslint-enable */
                            name="engGivenName"
                            msgPosition={'bottom'}
                            validators={[
                                ValidatorEnum.required,
                                ValidatorEnum.isSpecialEnglish
                            ]}
                            errorMessages={[
                                CommonMessage.VALIDATION_NOTE_REQUIRED(),
                                CommonMessage.VALIDATION_NOTE_SPECIAL_ENGLISH()
                            ]}
                            warning={[
                                ValidatorEnum.isEnglishWarningChar
                            ]}
                            warningMessages={[
                                CommonMessage.WARNING_NOTE_SPECIAL_ENGLISH()
                            ]}
                            disabled={isInputDisabled}
                            value={userSearchData.engGivenName}
                            onChange={this.props.handleChange}
                            label={<>Given Name<RequiredIcon /></>}
                            variant={'outlined'}
                            trim={'all'}
                        />
                    </Grid>
                </Grid>

                <Grid container className={classes.grid}>
                    <Grid item xs={12}>
                        <TextFieldValidator
                            id={`${id}_chinese_name`}
                            fullWidth
                            /* eslint-disable */
                            InputProps={{
                                className: 'input_field'
                            }}
                            inputProps={{
                                maxLength: 6
                            }}
                            /* eslint-enable */
                            name="chiFullName"
                            msgPosition={'bottom'}
                            validByBlur
                            validators={fieldValidators.chineseName}
                            errorMessages={fieldErrorMessages.chineseName}
                            disabled={isInputDisabled}
                            value={userSearchData.chiFullName}
                            onChange={this.props.handleChange}
                            label={<>Chinese Name</>}
                            variant={'outlined'}
                            trim={'all'}
                        />
                    </Grid>
                </Grid>

                <Grid container className={classes.grid}>
                    <Grid item xs={12}>
                        <TextFieldValidator
                            id={`${id}_salutation_login_name`}
                            fullWidth
                            upperCase
                            validByBlur
                            /* eslint-disable */
                            InputProps={{
                                className: 'input_field'
                            }}
                            inputProps={{
                                maxLength: 20
                            }}
                            /* eslint-enable */
                            name="loginName"
                            msgPosition={'bottom'}
                            validators={[
                                ValidatorEnum.required,
                                ValidatorEnum.isEnglishOrNumber
                            ]}
                            errorMessages={[
                                CommonMessage.VALIDATION_NOTE_REQUIRED(),
                                CommonMessage.VALIDATION_NOTE_ENGLISH_OR_NUM()
                            ]}
                            disabled={isInputDisabled}
                            value={userSearchData.loginName}
                            onChange={this.props.handleChange}
                            label={<>Login Name<RequiredIcon /></>}
                            variant={'outlined'}
                        />
                    </Grid>
                </Grid>

                <Grid container className={classes.grid}>
                    <Grid item xs={12}>
                        <TextFieldValidator
                            id={`${id}_salutation_email`}
                            fullWidth
                            validByBlur
                            /* eslint-disable */
                            InputProps={{
                                className: 'input_field'
                            }}
                            inputProps={{
                                maxLength: 40
                            }}
                            /* eslint-enable */
                            name="email"
                            msgPosition={'bottom'}
                            validators={fieldValidators.email}
                            errorMessages={fieldErrorMessages.email}
                            disabled={isInputDisabled}
                            value={userSearchData.email}
                            onChange={this.props.handleChange}
                            label={<>Email<RequiredIcon /></>}
                            variant={'outlined'}
                        />
                    </Grid>
                </Grid>

                <Grid container className={classes.grid}>
                    <Grid item xs={12}>
                        <SelectFieldValidator
                            id={`${id}_gender`}
                            options={this.props.pageCodeList.gender &&
                                this.props.pageCodeList.gender.map((item) => (
                                    { value: item.code, label: item.engDesc }))}
                            msgPosition={'bottom'}
                            validators={fieldValidators.gender}
                            errorMessages={fieldErrorMessages.gender}
                            isDisabled={isInputDisabled}
                            value={userSearchData.genderCd}
                            onChange={(e) => this.props.handleSelectChange(e, 'genderCd')}
                            TextFieldProps={{
                                variant: 'outlined',
                                label: <>Gender<RequiredIcon /></>
                            }}
                        />
                    </Grid>
                </Grid>

                <Grid container className={classes.grid}>
                    <Grid item xs={12}>
                        <TextFieldValidator
                            id={`${id}_contact_phone`}
                            fullWidth
                            validByBlur
                            /* eslint-disable */
                            InputProps={{
                                className: 'input_field'
                            }}
                            inputProps={{
                                maxLength: 8
                            }}
                            /* eslint-enable */
                            name="contactPhone"
                            msgPosition={'bottom'}
                            validators={fieldValidators.contactPhone}
                            errorMessages={fieldErrorMessages.contactPhone}
                            disabled={isInputDisabled}
                            value={userSearchData.contactPhone}
                            onChange={this.props.handleChange}
                            label={<>Contact Phone<RequiredIcon /></>}
                            variant={'outlined'}
                        />
                    </Grid>
                </Grid>

                <Grid container className={classes.grid}>
                    <Grid item xs={12}>
                        <TextFieldValidator
                            id={`${id}_position`}
                            fullWidth
                            validByBlur
                            /* eslint-disable */
                            InputProps={{
                                className: 'input_field'
                            }}
                            inputProps={{
                                maxLength: 33
                            }}
                            /* eslint-enable */
                            name="position"
                            msgPosition={'bottom'}
                            validators={fieldValidators.position}
                            errorMessages={fieldErrorMessages.position}
                            disabled={isInputDisabled}
                            value={userSearchData.position}
                            onChange={this.props.handleChange}
                            label={<>Position<RequiredIcon /></>}
                            variant={'outlined'}
                        />
                    </Grid>
                </Grid>

                <Grid container className={classes.grid}>
                    <Grid item xs={12}>
                        <TextFieldValidator
                            id={`${id}_supervisor`}
                            fullWidth
                            validByBlur
                            /* eslint-disable */
                            InputProps={{
                                className: 'input_field'
                            }}
                            inputProps={{
                                maxLength: 13
                            }}
                            /* eslint-enable */
                            name="supervisor"
                            msgPosition={'bottom'}
                            validators={fieldValidators.supervisor}
                            errorMessages={fieldErrorMessages.supervisor}
                            disabled={isInputDisabled}
                            value={userSearchData.supervisor}
                            onChange={this.props.handleChange}
                            label={<>Supervisor</>}
                            variant={'outlined'}
                        />
                    </Grid>
                </Grid>

                <Grid container className={classes.grid}>
                    <Grid item xs={12}>
                        <DateFieldValidator
                            id={`${id}_account_expiry_date`}
                            InputProps={{
                                className: 'input_field'
                            }}
                            msgPosition={'bottom'}
                            disabled={isInputDisabled}
                            disablePast
                            isRequired
                            value={userSearchData.userExpiryDate}
                            onChange={(e) => this.props.handleDateChange(e, 'userExpiryDate')}
                            label={<>Account Expiry Date<RequiredIcon /></>}
                        />
                    </Grid>
                </Grid>

                <Grid container className={classes.grid}>
                    <Grid item xs={12}>
                        <SelectFieldValidator
                            id={`${id}_account_status`}
                            options={Enum.COMMON_STATUS.map((item) => ({ value: item.code, label: item.engDesc }))}
                            msgPosition={'bottom'}
                            validators={fieldValidators.accountStatus}
                            errorMessages={fieldErrorMessages.accountStatus}
                            isDisabled={isInputDisabled}
                            value={userSearchData.status}
                            onChange={(e) => this.props.handleSelectChange(e, 'status')}
                            TextFieldProps={{
                                variant: 'outlined',
                                label: <>Account Status<RequiredIcon /></>
                            }}
                        />
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(UserProfileBasicInfoPanel);