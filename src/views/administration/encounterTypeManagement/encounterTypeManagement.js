import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Grid
} from '@material-ui/core';
import EncounterTypeInformation from './encounterTypeComponent/encounterTypeInformation';
import ScrollBarTable from './encounterTypeComponent/scrollBarTable';
import CIMSButton from '../../../components/Buttons/CIMSButton';

import {
    updateField,
    resetAll,
    requestEncounterType,
    saveData,
    deleteData,
    verifyCodeIsLegal
} from '../../../store/actions/administration/encounterTypeManagement/encounterTypeManagementAction';
import moment from 'moment';
import * as commonUtilities from '../../../utilities/commonUtilities';
import Enum from '../../../enums/enum';
import EditModeMiddleware from '../../compontent/editModeMiddleware';
import CommonRegex from '../../../constants/commonRegex';
import accessRightEnum from '../../../enums/accessRightEnum';


class EncounterTypeManagement extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }


    componentDidMount() {
        this.props.ensureDidMount();
        this.props.requestEncounterType();
    }

    componentWillUnmount() {
        this.props.resetAll();
    }

    encounterTypeTableColumns = [
        { name: 'encounterTypeCd', lable: 'Encounter', style: { width: 160 } },
        { name: 'description', lable: 'Description', style: { flex: 1, textOverflow: 'ellipsis', overflow: 'hidden', 'whiteSpace': 'nowrap' } }
    ]
    subEncounterTypeTableColumns = [
        { name: 'subEncounterTypeCd', lable: 'Sub-encounter', style: { width: 160 } },
        { name: 'description', lable: 'Description', style: { flex: 1, textOverflow: 'ellipsis', overflow: 'hidden', 'whiteSpace': 'nowrap' } }
    ]

    /*ScrollBar Table event*/
    handleEncounterTypeSelect = (row, index) => {
        if (index === this.props.selectEncounterTypeIndex) {
            this.props.updateField({
                selectEncounterType: {},
                subEncounterType: [],
                selectEncounterTypeIndex: -1,
                selectSubEncounterType: {},
                selectSubEncounterTypeIndex: -1,
                isSubEncounterTypeDetail: false
            });
        } else {
            this.props.updateField({
                selectEncounterType: row,
                subEncounterType: row.subEncounterTypeList,
                selectEncounterTypeIndex: index,
                selectSubEncounterType: {},
                selectSubEncounterTypeIndex: -1,
                isSubEncounterTypeDetail: false
            });
        }
    }
    handleSubEncounterTypeSelect = (row, index) => {
        if (index === this.props.selectSubEncounterTypeIndex) {
            this.props.updateField({
                selectSubEncounterType: {},
                selectSubEncounterTypeIndex: -1,
                isSubEncounterTypeDetail: false
            });
        } else {
            let where = { serviceCd: this.props.serviceCd, clinicCd: this.props.clinicCd, encounterCd: row.encounterTypeCd, subEncounterCd: row.subEncounterTypeCd };
            let sessionTime = commonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.AMPM_CUTOFF_TIME, this.props.clinicConfig, where);
            let nighSessionTime = commonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.EVENING_CUTOFF_TIME, this.props.clinicConfig, where);
            row.sessionTime = !row.sessionTime ? sessionTime.configValue : row.sessionTime;
            row.nighSessionTime = !row.nighSessionTime ? nighSessionTime.configValue : row.nighSessionTime;
            this.props.updateField({
                selectSubEncounterType: row,
                selectSubEncounterTypeIndex: index,
                isSubEncounterTypeDetail: true
            });
        }
    }

    /*Detail data onChange*/
    subEncounterTypeDetailOnChange = (e, fieldName, name) => {
        let newSubEncounterTypeDetail = { ...this.props.subEncounterTypeDetail };
        let reg = '';
        switch (fieldName) {
            case 'selectField':
                newSubEncounterTypeDetail[name] = e.value;
                break;
            case 'timeField':
                newSubEncounterTypeDetail[name] = !e ? null : moment(e).format('HH:mm');
                break;
            default:
                {
                    /* file max length */
                    switch (e.target.name) {
                        case 'chineseName':
                            {
                                if (e.target.value.length > 33) { return; }
                                break;
                            }
                        case 'description':
                            {
                                reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
                                if (reg.test(e.target.value)) {
                                    return;
                                }
                                if (e.target.value.length > 100) { return; } break;
                            }
                        case 'locationChi':
                            { if (e.target.value.length > 166) { return; } break; }
                        case 'locationEng':
                            {
                                reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
                                if (reg.test(e.target.value)) {
                                    return;
                                }
                                if (e.target.value.length > 100) { return; } break;
                            }
                        case 'phoneNo':
                            {
                                reg = new RegExp(/^[1-9][0-9]*$/);
                                if (e.target.value && !reg.test(e.target.value)) {
                                    return;
                                }
                                if (e.target.value.length > 8) { return; } break;
                            }
                        case 'remindDays':
                            { if (e.target.value.length > 5 || isNaN(e.target.value)) { return; } break; }
                        case 'shortName':
                            {
                                reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
                                if (reg.test(e.target.value)) {
                                    return;
                                }
                                if (e.target.value.length > 50) { return; } break;
                            }
                        case 'subEncounterTypeCd':
                            { if (e.target.value.length > 4) { return; } else { e.target.value = e.target.value.toUpperCase(); } break; }
                        default:
                            break;
                    }
                    newSubEncounterTypeDetail[e.target.name] = e.target.value;
                    break;
                }
        }
        if (newSubEncounterTypeDetail.subEncounterTypeCd !== this.props.subEncounterTypeDetail.subEncounterTypeCd) {
            this.props.updateField({
                subEncounterTypeDetail: newSubEncounterTypeDetail,
                isLegalCode: false
            });
        } else {
            this.props.updateField({
                subEncounterTypeDetail: newSubEncounterTypeDetail
            });
        }
    }
    encounterTypeDetailOnChange = (e, fieldName, name) => {
        let newEncounterTypeDetail = { ...this.props.encounterTypeDetail };
        switch (fieldName) {
            case 'selectField':
                newEncounterTypeDetail[name] = e.value;
                break;
            case 'timeField':
                newEncounterTypeDetail[name] = moment(e).format('HH:mm');
                break;

            default:
                {
                    const reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
                    if (reg.test(e.target.value)) {
                        return;
                    }
                    /* file max length */
                    switch (e.target.name) {
                        case 'description':
                            { if (e.target.value.length > 100) { return; } break; }
                        case 'shortName':
                            { if (e.target.value.length > 50) { return; } break; }
                        case 'encounterTypeCd':
                            { if (e.target.value.length > 4) { return; } else { e.target.value = e.target.value.toUpperCase(); } break; }
                        default:
                            break;
                    }
                    newEncounterTypeDetail[e.target.name] = e.target.value;
                    break;
                }
        }
        if (newEncounterTypeDetail.encounterTypeCd !== this.props.encounterTypeDetail.encounterTypeCd) {
            this.props.updateField({
                encounterTypeDetail: newEncounterTypeDetail,
                isLegalCode: false
            });
        } else {
            this.props.updateField({
                encounterTypeDetail: newEncounterTypeDetail
            });
        }
    }
    /*Detail data CD field verify*/
    subEncounterTypeDetailCodeOnBlur = (e) => {
        if (e.target.value) {
            let verifyData = { ...this.props.subEncounterTypeDetail };
            verifyData[e.target.name] = e.target.value;
            this.props.verifyCodeIsLegal({
                verifyType: 'verifySubEncounterTypeCd',
                verifyData: verifyData,
                updateFieldsFunction: this.props.updateField
            });
        }
    }
    encounterTypeDetailCodeOnBlur = (e) => {
        if (e.target.value) {
            let verifyData = { ...this.props.encounterTypeDetail };
            verifyData[e.target.name] = e.target.value;
            this.props.verifyCodeIsLegal({
                verifyType: 'verifyEncounterTypeCd',
                verifyData: verifyData,
                updateFieldsFunction: this.props.updateField
            });
        }
    }

    /*encounter Type button event */
    encounterTypeNewButtonOnClick = () => {
        this.props.updateField({
            encounterTypeDetail: {},
            isSubEncounterTypeDetail: false,
            isEditState: true,
            isNewData: true,
            selectEncounterType: {},
            selectSubEncounterType: {},
            selectEncounterTypeIndex: -1,
            selectSubEncounterTypeIndex: -1
        });
    }
    encounterTypeEditButtonOnClick = () => {
        this.props.updateField({
            isSubEncounterTypeDetail: false,
            isEditState: true,
            selectSubEncounterType: {},
            selectSubEncounterTypeIndex: -1
        });
    }
    encounterTypeDeleteButtonOnClick = () => {
        this.props.requestEncounterType();
        if (Object.keys(this.props.selectEncounterType).length > 1) {
            const param = {
                deleteType: 'deleteEncounterTypeDetail',
                deleteData: { ...this.props.selectEncounterType }
            };
            this.props.deleteData(param);
        }
    }

    /*sub-encounter Type button event */
    subEncounterTypeNewButtonOnClick = () => {

        let where = { serviceCd: this.props.serviceCd, clinicCd: this.props.clinicCd, encounterCd: this.props.selectEncounterType.encounterTypeCd };
        let sessionTime = commonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.AMPM_CUTOFF_TIME, this.props.clinicConfig, where);
        let nighSessionTime = commonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.EVENING_CUTOFF_TIME, this.props.clinicConfig, where);
        this.props.updateField({
            subEncounterTypeDetail: {
                encounterTypeCd: this.props.selectEncounterType.encounterTypeCd,
                sessionTime: sessionTime.configValue,
                nighSessionTime: nighSessionTime.configValue
            },
            isSubEncounterTypeDetail: true,
            isEditState: true,
            isNewData: true,
            selectSubEncounterType: {},
            selectSubEncounterTypeIndex: -1
        });
    }
    subEncounterTypeEditButtonOnClick = () => {
        this.props.updateField({
            isSubEncounterTypeDetail: true,
            isEditState: true
        });
    }
    subEncounterTypeDeleteButtonOnClick = () => {
        if (Object.keys(this.props.selectSubEncounterType).length > 1) {
            const param = {
                deleteType: 'deleteSubEncounterTypeDetail',
                deleteData: { ...this.props.selectSubEncounterType }
            };
            this.props.deleteData(param);
        }
    }

    /*save or cancel button event */
    encounterTypeInformationSaveButtonOnClick = () => {
        let param = {};
        if (this.props.isSubEncounterTypeDetail) {
            param = {
                saveType: this.props.isNewData ? 'insertSubEncounterType' : 'updateSubEncounterTypeDetail',
                saveData: { ...this.props.subEncounterTypeDetail }
            };
        } else {
            param = {
                saveType: this.props.isNewData ? 'insertEncounterType' : 'updateEncounterTypeDetail',
                saveData: { ...this.props.encounterTypeDetail }
            };
        }
        this.props.saveData(param);
    }
    encounterTypeInformationCancelButtonOnClick = (form) => {
        this.props.updateField({
            encounterTypeDetail: { ...this.props.selectEncounterType },
            subEncounterTypeDetail: { ...this.props.selectSubEncounterType },
            isEditState: false,
            isNewData: false,
            isLegalCode: false
        });
        form.resetValidations();
    }

    render() {
        const {
            encounterType,
            subEncounterType,
            selectEncounterTypeIndex,
            selectSubEncounterTypeIndex,
            isSubEncounterTypeDetail,
            isEditState,
            isNewData,
            isLegalCode,
            encounterTypeDetail,
            subEncounterTypeDetail
        } = this.props;
        return (
            <Grid style={{ height: '100%', display: 'flex', flexFlow: 'column' }}>
                <Grid style={{ display: 'flex' }}>
                    <Grid style={{ flex: 1, overflow: 'hidden', paddingRight: 10 }}>
                        <Grid>
                            <CIMSButton id={'encounterTypenewButton'} disabled={isEditState} onClick={this.encounterTypeNewButtonOnClick}>New</CIMSButton>
                            <CIMSButton id={'encounterTypeEditButton'} disabled={isEditState || selectEncounterTypeIndex === -1} onClick={this.encounterTypeEditButtonOnClick}>Edit</CIMSButton>
                            <CIMSButton id={'encounterTypeDeleteButton'} disabled={isEditState || selectEncounterTypeIndex === -1} onClick={this.encounterTypeDeleteButtonOnClick}>Delete</CIMSButton>
                        </Grid>
                        <ScrollBarTable
                            id={'encounterTypeScrollBarTable'}
                            columns={this.encounterTypeTableColumns}
                            store={encounterType}
                            selectOnChange={this.handleEncounterTypeSelect}
                            selectIndex={selectEncounterTypeIndex}
                            disabled={isEditState}
                        />
                    </Grid>
                    <Grid style={{ flex: 1, overflow: 'hidden', paddingLeft: 10 }}>
                        <Grid>
                            <CIMSButton id={'subEncounterTypeNewButton'} disabled={isEditState || selectEncounterTypeIndex === -1} onClick={this.subEncounterTypeNewButtonOnClick}>New</CIMSButton>
                            <CIMSButton id={'subEncounterTypeEditButton'} disabled={isEditState || selectSubEncounterTypeIndex === -1} onClick={this.subEncounterTypeEditButtonOnClick}>Edit</CIMSButton>
                            <CIMSButton id={'subEncounterTypeDeleteButton'} disabled={isEditState || selectSubEncounterTypeIndex === -1} onClick={this.subEncounterTypeDeleteButtonOnClick}>Delete</CIMSButton>
                        </Grid>
                        <ScrollBarTable
                            id={'subEncounterTypeScrollBarTable'}
                            columns={this.subEncounterTypeTableColumns}
                            store={subEncounterType}
                            selectOnChange={this.handleSubEncounterTypeSelect}
                            selectIndex={selectSubEncounterTypeIndex}
                            disabled={isEditState}
                        />
                    </Grid>
                </Grid>
                <Grid style={{ marginTop: 10, flex: 1, overflow: 'hidden' }}>
                    <EncounterTypeInformation
                        isSubEncounterTypeDetail={isSubEncounterTypeDetail}
                        encounterTypeDetail={encounterTypeDetail}
                        subEncounterTypeDetail={subEncounterTypeDetail}
                        subEncounterTypeDetailOnChange={this.subEncounterTypeDetailOnChange}
                        encounterTypeDetailOnChange={this.encounterTypeDetailOnChange}
                        subEncounterTypeDetailCodeOnBlur={this.subEncounterTypeDetailCodeOnBlur}
                        encounterTypeDetailCodeOnBlur={this.encounterTypeDetailCodeOnBlur}

                        isEditState={isEditState}
                        isNewData={isNewData}
                        isLegalCode={isLegalCode}
                        saveButtonOnClick={this.encounterTypeInformationSaveButtonOnClick}
                        cancelButtonOnClick={this.encounterTypeInformationCancelButtonOnClick}
                    />
                </Grid>
                <EditModeMiddleware componentName={accessRightEnum.encounterTypeManagement} when={isEditState} />
            </Grid>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        encounterType: state.encounterTypeManagement.encounterType,
        subEncounterType: state.encounterTypeManagement.subEncounterType,
        selectEncounterType: state.encounterTypeManagement.selectEncounterType,
        selectEncounterTypeIndex: state.encounterTypeManagement.selectEncounterTypeIndex,
        selectSubEncounterType: state.encounterTypeManagement.selectSubEncounterType,
        selectSubEncounterTypeIndex: state.encounterTypeManagement.selectSubEncounterTypeIndex,
        isSubEncounterTypeDetail: state.encounterTypeManagement.isSubEncounterTypeDetail,
        isEditState: state.encounterTypeManagement.isEditState,
        isNewData: state.encounterTypeManagement.isNewData,
        isLegalCode: state.encounterTypeManagement.isLegalCode,
        encounterTypeDetail: state.encounterTypeManagement.encounterTypeDetail,
        subEncounterTypeDetail: state.encounterTypeManagement.subEncounterTypeDetail,
        clinicConfig: state.common.clinicConfig,
        loginInfo: state.login.loginInfo,
        serviceCd: state.login.service.serviceCd,
        clinicCd: state.login.clinic.clinicCd
        //field
    };
};
const dispatchToProps = {
    updateField,
    resetAll,
    requestEncounterType,
    saveData,
    deleteData,
    verifyCodeIsLegal
};
export default connect(mapStateToProps, dispatchToProps)(EncounterTypeManagement);