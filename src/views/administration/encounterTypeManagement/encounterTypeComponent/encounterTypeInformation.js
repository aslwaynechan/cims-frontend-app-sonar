import React, { Component } from 'react';
import {
    Tabs,
    Tab,
    Typography,
    Grid
} from '@material-ui/core';

import CIMSButton from '../../../../components/Buttons/CIMSButton';
import EncounterTypeInformationDetail from './encounterTypeInformationDetail';
import EncounterTypeInformationCustom from './encounterTypeInformationCustom';
import ValidatorForm from '../../../../components/FormValidator/ValidatorForm';



class EncounterTypeInformation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tabValue: 0
        };
    }


    changeTabValue = (event, value) => {
        this.setState({ tabValue: value });
    }
    render() {
        return (
            <ValidatorForm ref="form" style={{ height: '100%', display: 'flex', flexDirection: 'column' }} id={this.props.id} onSubmit={this.props.saveButtonOnClick}>
                <Grid style={{ display: 'flex', alignItems: 'center' }}>
                    <Grid style={{ flex: 1 }}>
                        {this.props.isSubEncounterTypeDetail ? 'Sub Encounter Detail' : 'Encounter Detail'}
                    </Grid>
                    <Grid>
                        <CIMSButton
                            id={'encounterTypeInformationSaveButton'}
                            disabled={!this.props.isEditState || (this.props.isNewData && !this.props.isLegalCode)}
                            type="submit"
                        >
                            Save
                        </CIMSButton>
                        <CIMSButton
                            disabled={!this.props.isEditState}
                            onClick={() => { this.props.cancelButtonOnClick(this.refs.form); }}
                            id={'encounterTypeInformationCancelButton'}
                        >
                            Cancel
                        </CIMSButton>
                    </Grid>
                </Grid>
                <Grid style={{ flex: 1, display: 'flex', flexDirection: 'column', overflow: 'hidden' }}>
                    <Tabs
                        value={this.state.tabValue}
                        onChange={this.changeTabValue}
                        indicatorColor={'primary'}
                    >
                        <Tab id={'encounterTypeInformationTabDetail'} label={<Typography style={{ fontSize: 16, textTransform: 'none' }}>Detail</Typography>} className={this.state.tabValue === 0 ? 'tabSelected' : 'tabNavigation'} />
                        <Tab id={'encounterTypeInformationTabCustom'} label={<Typography style={{ fontSize: 16, textTransform: 'none' }}>Custom</Typography>} className={this.state.tabValue === 1 ? 'tabSelected' : 'tabNavigation'} />
                    </Tabs>
                    <Grid style={{ flex: 1 }}>
                        <Typography component={'div'} style={{ display: this.state.tabValue === 0 ? 'block' : 'none' }}>
                            <EncounterTypeInformationDetail
                                id={'encounterTypeInformationDetail'}
                                isSubEncounterTypeDetail={this.props.isSubEncounterTypeDetail}
                                encounterTypeDetail={this.props.encounterTypeDetail}
                                subEncounterTypeDetail={this.props.subEncounterTypeDetail}
                                subEncounterTypeDetailOnChange={this.props.subEncounterTypeDetailOnChange}
                                subEncounterTypeDetailCodeOnBlur={this.props.subEncounterTypeDetailCodeOnBlur}
                                encounterTypeDetailCodeOnBlur={this.props.encounterTypeDetailCodeOnBlur}
                                encounterTypeDetailOnChange={this.props.encounterTypeDetailOnChange}
                                onSubmit={this.props.saveButtonOnClick}
                                isEditState={this.props.isEditState}
                                isNewData={this.props.isNewData}
                            />
                        </Typography>
                        <Typography component={'div'} style={{ display: this.state.tabValue === 1 ? 'block' : 'none' }}>
                            <EncounterTypeInformationCustom id={'encounterTypeInformationCustom'} />
                        </Typography>
                    </Grid>
                </Grid>
            </ValidatorForm>
        );
    }
}

export default EncounterTypeInformation;