import React, { Component } from 'react';
import {
    Grid
} from '@material-ui/core';

import TextFieldValidator from '../../../../components/FormValidator/TextFieldValidator';
import SelectFieldValidator from '../../../../components/FormValidator/SelectFieldValidator';
import TimeFieldValidator from '../../../../components/FormValidator/TimeFieldValidator';
import ValidatorForm from '../../../../components/FormValidator/ValidatorForm';
import ValidatorEnum from '../../../../enums/validatorEnum';
import CommonMessage from '../../../../constants/commonMessage';
import moment from 'moment';
import * as RegUtil from '../../../../utilities/administrationUtilities';

class EncounterTypeInformationDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };

        ValidatorForm.addValidationRule('isChinese', (value) => {
            return RegUtil.checkChinese(value);
        });
    }

    yesOrNoSelet = [{ label: 'Yes', value: 'Y' }, { label: 'No', value: 'N' }]

    render() {
        return (
            <Grid id={this.props.id} >
                {this.props.isSubEncounterTypeDetail ?
                    <Grid>
                        <Grid style={{ display: 'flex', marginBottom: 6 }}>
                            <Grid style={{ flex: 2, display: 'flex' }}>
                                <Grid style={{ flex: 1, paddingRight: 10 }}>
                                    <TextFieldValidator
                                        id={this.props.id + 'SubEncounterTypeDetailSubEncounterTypeCdTextField'}
                                        name="subEncounterTypeCd"
                                        labelText="Sub-encounter:"
                                        labelPosition="left"
                                        validByBlur
                                        isRequired
                                        variant={!this.props.isNewData ? 'standard' : 'outlined'}
                                        msgPosition="bottom"
                                        labelProps={{ style: { minWidth: 134 } }}
                                        fullWidth
                                        disabled={!this.props.isNewData}
                                        validators={[ValidatorEnum.required,ValidatorEnum.codeVerification]}
                                        errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED(),CommonMessage.VALIDATION_CODE_VALIDATION()]}
                                        value={this.props.subEncounterTypeDetail.subEncounterTypeCd || ''}
                                        onChange={this.props.subEncounterTypeDetailOnChange}
                                        onBlur={this.props.subEncounterTypeDetailCodeOnBlur}
                                    />
                                </Grid>
                                <Grid style={{ flex: 1 }}>
                                    <TextFieldValidator
                                        id={this.props.id + 'SubEncounterTypeDetailDescriptionTextField'}
                                        name="description"
                                        labelText="Description:"
                                        labelPosition="left"
                                        validByBlur
                                        isRequired
                                        variant={!this.props.isEditState ? 'standard' : 'outlined'}
                                        msgPosition="bottom"
                                        labelProps={{ style: { minWidth: 116 } }}
                                        fullWidth
                                        disabled={!this.props.isEditState}
                                        validators={[ValidatorEnum.required,ValidatorEnum.isNoChinese]}
                                        errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED(),CommonMessage.VALIDATION_NOTE_IS_NO_CHINESE()]}
                                        value={this.props.subEncounterTypeDetail.description || ''}
                                        onChange={this.props.subEncounterTypeDetailOnChange}
                                    />
                                </Grid>
                            </Grid>
                            <Grid style={{ flex: 1, marginLeft: 10 }}>
                                <TextFieldValidator
                                    id={this.props.id + 'SubEncounterTypeDetailChineseNameTextField'}
                                    name="chineseName"
                                    labelText="Chinese Name:"
                                    labelPosition="left"
                                    variant={!this.props.isEditState ? 'standard' : 'outlined'}
                                    msgPosition="bottom"
                                    labelProps={{ style: { minWidth: 120 } }}
                                    fullWidth
                                    validByBlur
                                    disabled={!this.props.isEditState}
                                    value={this.props.subEncounterTypeDetail.chineseName || ''}
                                    onChange={this.props.subEncounterTypeDetailOnChange}
                                    validators={[ValidatorEnum.isChinese]}
                                    errorMessages={[CommonMessage.VALIDATION_NOTE_CHINESEFIELD()]}
                                />
                            </Grid>
                        </Grid>
                        <Grid style={{ display: 'flex', marginBottom: 6 }}>
                            <Grid style={{ flex: 2 }}>
                                <TextFieldValidator
                                    id={this.props.id + 'SubEncounterTypeDetailLocationChiTextField'}
                                    name="locationChi"
                                    labelText="Location(Chi):"
                                    labelPosition="left"
                                    variant={!this.props.isEditState ? 'standard' : 'outlined'}
                                    msgPosition="bottom"
                                    labelProps={{ style: { minWidth: 116 } }}
                                    fullWidth
                                    disabled={!this.props.isEditState}
                                    value={this.props.subEncounterTypeDetail.locationChi || ''}
                                    onChange={this.props.subEncounterTypeDetailOnChange}
                                />
                            </Grid>
                            <Grid style={{ flex: 1, marginLeft: 10 }}>
                                <TextFieldValidator
                                    id={this.props.id + 'SubEncounterTypeDetailShortNameTextField'}
                                    name="shortName"
                                    labelText="Short Name:"
                                    labelPosition="left"
                                    isRequired
                                    variant={!this.props.isEditState ? 'standard' : 'outlined'}
                                    msgPosition="bottom"
                                    labelProps={{ style: { minWidth: 120 } }}
                                    fullWidth
                                    validByBlur
                                    disabled={!this.props.isEditState}
                                    validators={[ValidatorEnum.required,ValidatorEnum.isNoChinese]}
                                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED(),CommonMessage.VALIDATION_NOTE_IS_NO_CHINESE()]}
                                    value={this.props.subEncounterTypeDetail.shortName || ''}
                                    onChange={this.props.subEncounterTypeDetailOnChange}
                                />
                            </Grid>
                        </Grid>
                        <Grid style={{ display: 'flex', marginBottom: 6 }}>
                            <Grid style={{ flex: 2 }}>
                                <TextFieldValidator
                                    id={this.props.id + 'SubEncounterTypeDetailLocationEngTextField'}
                                    name="locationEng"
                                    labelText="Location(Eng):"
                                    labelPosition="left"
                                    variant={!this.props.isEditState ? 'standard' : 'outlined'}
                                    msgPosition="bottom"
                                    labelProps={{ style: { minWidth: 116 } }}
                                    fullWidth
                                    validByBlur
                                    disabled={!this.props.isEditState}
                                    validators={[ValidatorEnum.isNoChinese]}
                                    errorMessages={[CommonMessage.VALIDATION_NOTE_IS_NO_CHINESE()]}
                                    value={this.props.subEncounterTypeDetail.locationEng || ''}
                                    onChange={this.props.subEncounterTypeDetailOnChange}
                                />
                            </Grid>
                            {/* <Grid style={{ flex: 1, marginLeft: 10 }}>
                            </Grid> */}
                        </Grid>
                        <Grid style={{ display: 'flex', marginBottom: 6 }}>
                            <Grid style={{ flex: 2, display: 'flex' }}>
                                <Grid style={{ flex: 1, paddingRight: 10 }}>
                                    <SelectFieldValidator
                                        id={this.props.id + 'SubEncounterTypeDetailMulitBookSelectField'}
                                        name="mulitBook"
                                        labelText="Enable Multi Booking:"
                                        labelPosition="left"
                                        variant="outlined"
                                        msgPosition="bottom"
                                        labelProps={{ style: { minWidth: 176 } }}
                                        fullWidth
                                        TextFieldProps={!this.props.isEditState ? {
                                            variant: 'standard',
                                            disabled: true
                                        } : null}
                                        isDisabled={!this.props.isEditState}
                                        options={this.yesOrNoSelet}
                                        value={this.props.subEncounterTypeDetail.mulitBook}
                                        onChange={(e) => { this.props.subEncounterTypeDetailOnChange(e, 'selectField', 'mulitBook'); }}
                                    />
                                </Grid>
                                <Grid style={{ flex: 1 }}>
                                    <TimeFieldValidator
                                        id={this.props.id + 'SubEncounterTypeDetailSessionTimeTimeField'}
                                        name="sessionTime"
                                        labelText="AM/PM Cut-Off Time:"
                                        labelPosition="left"
                                        variant={!this.props.isEditState ? 'standard' : 'outlined'}
                                        msgPosition="bottom"
                                        labelProps={{ style: { minWidth: 190 } }}
                                        fullWidth
                                        disabled={!this.props.isEditState}
                                        isRequired
                                        validators={[ValidatorEnum.required]}
                                        errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                        value={!this.props.subEncounterTypeDetail.sessionTime ? null : moment(this.props.subEncounterTypeDetail.sessionTime, 'HH:mm')}
                                        onChange={(e) => { this.props.subEncounterTypeDetailOnChange(e, 'timeField', 'sessionTime'); }}
                                    />
                                </Grid>
                            </Grid>
                            <Grid style={{ flex: 1, marginLeft: 10 }}>

                                <TimeFieldValidator
                                    id={this.props.id + 'SubEncounterTypeDetailNighSessionTimeField'}
                                    name="nighSessionTime"
                                    labelText="Night Session Time:"
                                    labelPosition="left"
                                    variant={!this.props.isEditState ? 'standard' : 'outlined'}
                                    msgPosition="bottom"
                                    labelProps={{ style: { minWidth: 170 } }}
                                    fullWidth
                                    disabled={!this.props.isEditState}
                                    isRequired
                                    validators={[ValidatorEnum.required]}
                                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                    value={!this.props.subEncounterTypeDetail.nighSessionTime ? null : moment(this.props.subEncounterTypeDetail.nighSessionTime, 'HH:mm')}
                                    onChange={(e) => { this.props.subEncounterTypeDetailOnChange(e, 'timeField', 'nighSessionTime'); }}
                                />
                            </Grid>
                        </Grid>
                        <Grid style={{ display: 'flex', marginBottom: 6 }}>
                            <Grid style={{ flex: 2, display: 'flex' }}>
                                <Grid style={{ flex: 2, paddingRight: 6 }}>
                                    <SelectFieldValidator
                                        id={this.props.id + 'SubEncounterTypeDetailReminderSelectField'}
                                        name="reminder"
                                        labelText="Appointment Reminder:"
                                        labelPosition="left"
                                        variant="outlined"
                                        labelProps={{ style: { minWidth: 200 } }}
                                        fullWidth
                                        TextFieldProps={!this.props.isEditState ? {
                                            variant: 'standard',
                                            disabled: true
                                        } : null}
                                        isDisabled={!this.props.isEditState}
                                        options={this.yesOrNoSelet}
                                        value={this.props.subEncounterTypeDetail.reminder}
                                        onChange={(e) => { this.props.subEncounterTypeDetailOnChange(e, 'selectField', 'reminder'); }}
                                    />
                                </Grid>
                                <Grid style={{ flex: 1, paddingRight: 6 }}>
                                    <TextFieldValidator
                                        id={this.props.id + 'SubEncounterTypeDetailRemindDaysTextField'}
                                        name="remindDays"
                                        variant={!this.props.isEditState ? 'standard' : 'outlined'}
                                        fullWidth
                                        disabled={!this.props.isEditState}
                                        value={this.props.subEncounterTypeDetail.remindDays || ''}
                                        onChange={this.props.subEncounterTypeDetailOnChange}
                                    />
                                </Grid>
                                <Grid style={{ flex: 1 }}>
                                    <SelectFieldValidator
                                        id={this.props.id + 'SubEncounterTypeDetailOutlinedSelectField'}
                                        variant="outlined"
                                        fullWidth
                                        // TextFieldProps={!this.props.isEditState? {
                                        //     variant: 'standard',
                                        //     disabled: true
                                        // } : null}
                                        // isDisabled={!this.props.isEditState}
                                        TextFieldProps={{
                                            variant: 'standard',
                                            disabled: true
                                        }}
                                        isDisabled
                                        options={[{ label: 'Days', value: 'd' }]}
                                        value={'d'}
                                        onChange={(e) => { this.props.subEncounterTypeDetailOnChange(e, 'selectField'); }}
                                    />
                                </Grid>
                            </Grid>
                            <Grid style={{ flex: 1, marginLeft: 10 }}>
                                <TextFieldValidator
                                    id={this.props.id + 'SubEncounterTypeDetailPhoneNoTextField'}
                                    name="phoneNo"
                                    labelText="Phone:"
                                    labelPosition="left"
                                    variant={!this.props.isEditState ? 'standard' : 'outlined'}
                                    msgPosition="bottom"
                                    labelProps={{ style: { minWidth: 110 } }}
                                    fullWidth
                                    validByBlur
                                    disabled={!this.props.isEditState}
                                    validators={[ValidatorEnum.phoneNo,ValidatorEnum.minStringLength(8)]}
                                    errorMessages={[CommonMessage.VALIDATION_NOTE_PHONE_NO(),CommonMessage.VALIDATION_NOTE_PHONE_BELOWMINWIDTH()]}
                                    value={this.props.subEncounterTypeDetail.phoneNo || ''}
                                    onChange={this.props.subEncounterTypeDetailOnChange}
                                />
                            </Grid>
                        </Grid>
                    </Grid> :
                    <Grid>
                        <Grid style={{ display: 'flex', marginBottom: 6 }}>
                            <Grid style={{ flex: 1, paddingRight: 10 }}>
                                <TextFieldValidator
                                    id={this.props.id + 'EncounterTypeDetailEncounterTypeCdTextField'}
                                    name="encounterTypeCd"
                                    labelText="Encounter:"
                                    isRequired
                                    labelPosition="left"
                                    variant={!this.props.isNewData ? 'standard' : 'outlined'}
                                    msgPosition="bottom"
                                    labelProps={{ style: { minWidth: 136 } }}
                                    fullWidth
                                    validByBlur
                                    disabled={!this.props.isNewData}
                                    value={this.props.encounterTypeDetail.encounterTypeCd || ''}
                                    validators={[ValidatorEnum.required,ValidatorEnum.codeVerification]}
                                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED(),CommonMessage.VALIDATION_CODE_VALIDATION()]}
                                    onChange={this.props.encounterTypeDetailOnChange}
                                    onBlur={this.props.encounterTypeDetailCodeOnBlur}
                                />
                            </Grid>

                            <Grid style={{ flex: 1, paddingRight: 10 }}>
                                <TextFieldValidator
                                    id={this.props.id + 'EncounterTypeDetailDescriptionTextField'}
                                    name="description"
                                    isRequired
                                    labelText="Description:"
                                    labelPosition="left"
                                    variant={!this.props.isEditState ? 'standard' : 'outlined'}
                                    msgPosition="bottom"
                                    labelProps={{ style: { minWidth: 116 } }}
                                    fullWidth
                                    validByBlur
                                    disabled={!this.props.isEditState}
                                    value={this.props.encounterTypeDetail.description || ''}
                                    validators={[ValidatorEnum.required,ValidatorEnum.isNoChinese]}
                                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED(),CommonMessage.VALIDATION_NOTE_IS_NO_CHINESE()]}
                                    onChange={this.props.encounterTypeDetailOnChange}
                                />
                            </Grid>
                            <Grid style={{ flex: 1, marginLeft: 10 }}>
                                <Grid style={{ flex: 1 }}>
                                    <TextFieldValidator
                                        id={this.props.id + 'EncounterTypeDetailShortNameTextField'}
                                        name="shortName"
                                        labelText="Short Name:"
                                        labelPosition="left"
                                        isRequired
                                        variant={!this.props.isEditState ? 'standard' : 'outlined'}
                                        msgPosition="bottom"
                                        labelProps={{ style: { minWidth: 120 } }}
                                        fullWidth
                                        validByBlur
                                        disabled={!this.props.isEditState}
                                        value={this.props.encounterTypeDetail.shortName || ''}
                                        validators={[ValidatorEnum.required,ValidatorEnum.isNoChinese]}
                                        errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED(),CommonMessage.VALIDATION_NOTE_IS_NO_CHINESE()]}
                                        onChange={this.props.encounterTypeDetailOnChange}
                                    />
                                </Grid>

                            </Grid>

                        </Grid>
                    </Grid>
                }
            </Grid>
        );
    }
}

export default EncounterTypeInformationDetail;