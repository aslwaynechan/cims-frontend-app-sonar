const styles = theme => ({
    radioLabel: {
        margin: 0
    },
    radioBtn: {
        height: 18,
        paddingTop: 0,
        paddingBottom: 5,
        '&$radioBtnChecked': {
            height: 18,
            paddingTop: 0,
            paddingBottom: 5
        }
    },
    radioBtnChecked: {},
    fullWidth: {
        maxWidth: '100%',
        overflowY: 'unset',
        width: '100%'
    },
    rightBtn: {
        position: 'absolute',
        right: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItem: 'center'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16
    },
    //context menu
    contextMenuItem: {
        padding: '5px 5px 5px 10px',
        minHeight: 20
    },
    //order date style
    orderDateFont: {
        width: 102,
        textAlign: 'right'
    },
    orderDateValue: {
        width: 102,
        textAlign: 'right'
    },
    tooltipPlacementBottom: {
        transformOrigin: 'center top',
        margin: '12px 0',
        [theme.breakpoints.up('sm')]: {
            margin: '2px 0'
        }
    }
    // textFieldStyle: {
    //     root: {
    //         '&$textFieldFocused': {
    //             borderColor: '#0579c8',
    //             borderWdth: '2px'
    //         }
    //     }
    // },
    // textFieldFocused:{}
});

export default styles;