import React, { Component } from 'react';
import { connect } from 'react-redux';


class Welcome extends Component {
  render() {
    return (
      <div className={'nephele_main_body'}>
        <h1>Welcome</h1>
      </div>
    );
  }
}
export default connect()(Welcome);
