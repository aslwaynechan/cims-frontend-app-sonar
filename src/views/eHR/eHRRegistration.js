import React, { Component } from 'react';
import { connect } from 'react-redux';
import Iframe from 'react-iframe';
import { getEHRUrl } from '../../store/actions/patient/patientAction';
import { openCommonMessage } from '../../store/actions/message/messageAction';
import accessRightEnum from '../../enums/accessRightEnum';
import Enum from '../../enums/enum';
import * as PatientUtil from '../../utilities/patientUtilities';

class eHRRegistration extends Component {

    componentDidMount() {
        let isEHRAccessRight = false;
        const { accessRights, patient } = this.props;

        // TODO : Add the ehruId check in accessRights
        accessRights && accessRights.filter(item => item.type === 'button')
                .forEach(right => {
            if (right.isPatRequired === 'Y'
                    && right.name === accessRightEnum.eHRRegistered) {
                isEHRAccessRight = true;
            }
        });

        let hkId;
        patient && patient.documentPairList.filter(item => item.patientKey === patient.patientKey
                && item.docTypeCd === Enum.DOC_TYPE.HKID_ID).forEach(patientInfo => {
            if (patientInfo.docNo) {
                hkId = patientInfo.docNo;
            }
        });

        patient && patient.documentPairList.filter(item => item.patientKey === patient.patientKey)
                .forEach(patientInfo => {
            let isHKIC = PatientUtil.isHKIDFormat(patientInfo.docTypeCd);
            if (isHKIC && !hkId) {
                hkId = patientInfo.docNo;
            }
        });

        let documentId;
        let documentType;
        patient && patient.documentPairList.filter(item => item.patientKey === patient.patientKey)
                .forEach(patientInfo => {
            if (!documentId && !documentType) {
                let isHKIC = PatientUtil.isHKIDFormat(patientInfo.docTypeCd);
                if (!isHKIC) {
                    documentId = patientInfo.docNo;
                    documentType = patientInfo.docTypeCd;
                }
            }
        });

        // TODO : Add the ehruId and uamUserId
        let patientData = {
            dob: patient.dob ? patient.dob : '' ,
            documentId: documentId ? documentId : '' ,
            documentType: documentType ? documentType : '' ,
            ehrNo: patient.patientEhr && patient.patientEhr.ehrNo ? patient.patientEhr.ehrNo : '' ,
            ehruId: 1,
            givenName: patient.engGivename ? patient.engGivename : '',
            hkId: hkId ? hkId : '' ,
            pmiPersId: patient.patientKey ? patient.patientKey : '' ,
            resolutionHeight: window.screen.height,
            resolutionWidth: window.screen.width,
            serviceId: this.props.serviceCd ? this.props.serviceCd : '' ,
            sex: patient.genderCd ? patient.genderCd : '' ,
            surname: patient.engSurname ? patient.engSurname : '' ,
            uamUserId: 1
        };

        this.btnEHROnClick(patientData, isEHRAccessRight);
    }

    btnEHROnClick = (patient, isEHRAccessRight) => {
      this.props.getEHRUrl(patient, isEHRAccessRight);
    }

    render() {
        return (
            <Iframe
                url={(this.props.eHRUrl ? this.props.eHRUrl : '')}
                width="100%"
                height="99%"
            />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        patient: state.patient.patientInfo,
        serviceCd: state.login.service.serviceCd,
        accessRights: state.login.accessRights,
        eHRUrl: state.patient.eHRUrl
    };
};

const mapDispatchToProps = {
    getEHRUrl,
    openCommonMessage
};

export default connect(mapStateToProps, mapDispatchToProps)((eHRRegistration));