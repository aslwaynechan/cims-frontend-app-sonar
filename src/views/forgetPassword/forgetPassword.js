import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
// import styles from './login.module.scss';
import { withStyles } from '@material-ui/core/styles';
import {
    Grid,
    Link
} from '@material-ui/core';
import ValidatorForm from '../../components/FormValidator/ValidatorForm';
import TextField from '../../components/FormValidator/TextFieldValidator';
import CIMSButton from '../../components/Buttons/CIMSButton';
import { Link as RouterLink } from 'react-router-dom';
import { updateField, send, resetAll } from '../../store/actions/forgetPassword/forgetPasswordAction';
import ValidatorEnum from '../../enums/validatorEnum';
import CommonMessage from '../../constants/commonMessage';
import CIMSPromptDialog from '../../components/Dialog/CIMSPromptDialog';


const style = {
    root: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        marginTop: 56
    },
    from: {
        width: 460
    },
    pageTitle: {
        color: '#0579c8',
        fontSize: 20,
        fontWeight: 'bold',
        margin: '20px 0'
    },
    textField: {
        marginBottom: 20
    },
    ul: {
        margin: 0
    },
    li: {
        display: 'flex',
        alignItems: 'center'
    },
    liText: {
        flex: 1
    },
    link: {
        textDecoration: 'none',
        '&:hover': {
            textDecoration: 'none'
        }
    }


};

class ForgetPassword extends Component {

    componentWillUnmount() {
        this.props.resetAll();
    }

    onChangeLoginId = (e) => {
        this.props.updateField({ loginId: e.target.value.toUpperCase() });
    }

    submitData = (e) => {
        this.props.send({
            loginId: this.props.loginId,
            sendType: e.sendType
        });
    }

    dialogOKButtonOnClick = () => {
        this.props.updateField({
            dialog: {
                open: false,
                title: this.props.dialog.title,
                contentText: this.props.dialog.contentText
            }
        });
    }

    render() {
        const { classes, loginId } = this.props;
        return (
            <div className={classes.root}>
                <Grid>
                    <ValidatorForm ref="form" className={classes.from} onSubmit={this.submitData}>
                        <Grid className={classes.pageTitle}>
                            Forget Password
                            </Grid>
                        <Grid className={classes.textField}>
                            <TextField
                                id={'forgetPasswordLoginIdTextField'}
                                labelText="Please enter your login ID:"
                                labelProps={{ style: { width: 380 } }}
                                labelPosition="left"
                                msgPosition="bottom"
                                fullWidth
                                value={loginId}
                                validators={[ValidatorEnum.isNoChinese]}
                                errorMessages={[CommonMessage.VALIDATION_NOTE_IS_NO_CHINESE()]}
                                onChange={this.onChangeLoginId}
                            />
                        </Grid>
                        <Grid>
                            <Grid>
                                Please choose below for retrieving temporary password thru:
                            </Grid>
                            <ul className={classes.ul}>
                                <li className={classes.li}>
                                    <Grid className={classes.liText}>
                                        1.Registered email
                                        </Grid>
                                    <Grid>
                                        <CIMSButton
                                            id={'forgetPasswordSendByEmail'}
                                            disabled={loginId.length <= 0}
                                            onClick={(e) => { e.sendType = 'EMAIL'; this.refs.form.submit(e); }}
                                        >
                                            Send
                                        </CIMSButton>
                                    </Grid>
                                </li>
                                <li className={classes.li}>
                                    <Grid className={classes.liText}>
                                        2.SMS(registered contact number)
                                        </Grid>
                                    <Grid>
                                        <CIMSButton
                                            id={'forgetPasswordSendBySMS'}
                                            disabled={loginId.length <= 0}
                                            onClick={(e) => { e.sendType = 'SMS'; this.refs.form.submit(e); }}
                                        >
                                            Send
                                        </CIMSButton>
                                    </Grid>
                                </li>
                            </ul>
                            <Link id={'forgetPasswordReturnLogin'} className={classes.link} component={RouterLink} to="/login" >{'<<go to login Page'}</Link>
                        </Grid>
                    </ValidatorForm>
                </Grid>
                <CIMSPromptDialog
                    id={'forgetPasswordPromptDialog'}
                    dialogTitle={this.props.dialog.title}
                    dialogContentText={this.props.dialog.contentText}
                    open={this.props.dialog.open}
                    buttonConfig={
                        [
                            {
                                id: 'forgetPasswordPromptDialogOKButton',
                                name: 'OK',
                                //style: { flex: 1 },
                                onClick: this.dialogOKButtonOnClick
                            }
                        ]
                    }
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        loginId: state.forgetPassword.loginId,
        dialog: state.forgetPassword.dialog
    };
}
const dispatchProps = {
    updateField,
    send,
    resetAll
};
export default withRouter(connect(mapStateToProps, dispatchProps)(withStyles(style)(ForgetPassword)));
