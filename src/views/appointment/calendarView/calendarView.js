import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import {
    Grid
} from '@material-ui/core';
import { connect } from 'react-redux';
import * as commonUtilities from '../../../utilities/commonUtilities';
import Enum from '../../../enums/enum';
import moment from 'moment';
import _ from 'lodash';
import {
    requestData,
    resetAll,
    updateField
} from '../../../store/actions/appointment/calendarView/calendarViewAction';
import { initBookingData, initEncounterType } from '../../../constants/appointment/bookingInformation/bookingInformationConstants';
import {
    skipTab
} from '../../../store/actions/mainFrame/mainFrameAction';
import AccessRightEnum from '../../../enums/accessRightEnum';
import FilterBar from './component/filterBar';
import CalendarDateBar from './component/calendarDateBar';
import RemarkDialog from './component/remarkDialog';
import RemarkPopper from './component/remarkPopper';
import DayView from './component/dayView/dayView';
import WeekView from './component/weekView/weekView';
import MonthView from './component/monthView/monthView';
const styles = theme => ({
    root: {
        // padding: 16,
        // '&:last-child': { paddingBottom: 24 },
        height: '100%',
        display: 'flex'
    },
    form: {
        width: 200,
        paddingRight: 24,
        height: '100%'
    },
    formControl: {
        marginBottom: 10,
        width: '100%'
    },
    formLabel: {
        marginBottom: 10,
        fontWeight: 'bold'
    },
    formGroup: {
        overflowY: 'hidden',
        border: '1px solid #cccccc',
        padding: 6
    },
    checkboxFormGroup: {
        overflow: 'hidden',
        border: '1px solid #cccccc',
        paddingLeft: 6,
        height: 94
    },
    checkboxGroup: {
        overflow: 'auto'
    },
    formControlLabel: {
        margin: 0,
        width: '100%'
    },
    checkboxLabel: {
        width: '100%',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis'
    },
    shortNameLable: {
        paddingLeft: 14,
        color: '#404040',
        wordBreak: 'break-all'
    },

    calendarRoot: {
        flex: 1,
        display: 'flex',
        flexFlow: 'column',
        overflow: 'hidden'
    },

    calendarDateBar: {
        display: 'flex',
        marginBottom: 10,
        '& .dateAction': {
            flex: 1,
            display: 'flex',
            alignItems: 'center',
            '& .buttonGroup': {
                marginRight: 10
            }
        }
    },
    calendarView: {
        flex: 1,
        overflow: 'auto'
    },




    buttonRoot: {
        textTransform: 'none',
        '&:hover': {
            backgroundColor: 'rgb(0, 152, 255)'
        },
        padding: '1px 12px'
    },
    buttonLabel: {
        fontSize: 12
    },
    buttonDisabled: {
        borderColor: theme.palette.action.disabledBackground
    }
    // sizeLarge: {
    //     margin: theme.spacing(1),
    //     padding: '8px 24px',
    //     fontSize: 14
    // },
    // label: {
    //     fontSize: 12
    // },
    // disabled: {
    //     borderColor: theme.palette.action.disabledBackground
    // }
});

class CalendarView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openRemarkDialog: false,
            remarkStore: [],
            openPopper: false,
            popperAnchorEl: null,
            popperRemark: {}
        };
    }

    componentDidMount() {
        this.props.ensureDidMount();
        let where = { serviceCd: this.props.serviceCd, clinicCd: this.props.clinicCd };
        let defaultEncounterCd = commonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.DEFAULT_ENCOUNTER_CD, this.props.clinicConfig, where);
        this.props.updateField({ clinicValue: this.props.clinicCd, encounterTypeValue: defaultEncounterCd.configValue });
    }

    shouldComponentUpdate(nextP) {
        //Refresh data when entering the interface
        if (nextP.tabsActiveKey !== this.props.tabsActiveKey && nextP.tabsActiveKey === AccessRightEnum.calendarView) {
            this.getCalendarData();
            return false;
        }
        return true;
    }

    componentWillUnmount() {
        this.props.resetAll();
    }

    getCalendarData = (newData) => {
        let fileData = newData || {};
        let submitData = {
            clinicCd: fileData.clinicValue || this.props.clinicValue,
            dateFrom: fileData.dateFrom || this.props.dateFrom,
            dateTo: fileData.dateTo || this.props.dateTo,
            encounterTypeCd: fileData.encounterTypeValue || this.props.encounterTypeValue,
            subEncounterTypeCds: fileData.subEncounterTypeValue || this.props.subEncounterTypeValue,
            viewType: fileData.calendarViewValue || this.props.calendarViewValue
        };
        if (submitData.subEncounterTypeCds.length > 0) {
            if (submitData.viewType === 'M') {
                // submitData.dateFrom = moment(submitData.dateFrom).startOf('week').format('YYYY-MM-DD');
                // submitData.dateTo = moment(submitData.dateTo).endOf('week').format('YYYY-MM-DD');
                submitData.dateFrom = moment(submitData.dateFrom).startOf('week').format(Enum.DATE_FORMAT_EYMD_VALUE);
                submitData.dateTo = moment(submitData.dateTo).endOf('week').format(Enum.DATE_FORMAT_EYMD_VALUE);
            } else {
                // submitData.dateFrom = moment(submitData.dateFrom).format('YYYY-MM-DD');
                // submitData.dateTo = moment(submitData.dateTo).format('YYYY-MM-DD');
                submitData.dateFrom = moment(submitData.dateFrom).format(Enum.DATE_FORMAT_EYMD_VALUE);
                submitData.dateTo = moment(submitData.dateTo).format(Enum.DATE_FORMAT_EYMD_VALUE);
            }
            this.props.requestData('calendarData', submitData, fileData);
        } else {
            fileData['calendarData'] = [];
            this.props.updateField(fileData);
        }
    }

    encounterTypeValueOnChange = (e) => {
        let keyAndValue = {};
        let fileData = {};
        fileData.encounterTypeValue = e.value;
        fileData.subEncounterTypeListData = e.rowData.subEncounterTypeList.map(item => { keyAndValue[item.subEncounterTypeCd] = item; return item; });
        fileData.subEncounterTypeListKeyAndValue = keyAndValue;
        fileData.subEncounterTypeValue = [];
        fileData.selectEncounterType = { ...e.rowData };
        console.log('fileData', fileData);
        this.props.updateField(fileData);
    }

    checkboxOnChange = (name, value) => {
        let fileData = {};
        fileData[name] = _.cloneDeep(this.props[name] || []);
        let valueIndex = fileData[name].indexOf(value);
        if (valueIndex === -1) {
            fileData[name].push(value);
        } else {
            fileData[name].splice(valueIndex, 1);
        }
        if (name === 'availableQuotaValue') {
            if (fileData[name].indexOf('force') === -1 && fileData[name].indexOf('normal') === -1) {
                fileData[name] = [];
            }
            this.props.updateField(fileData);
        } else {
            this.getCalendarData(fileData);
        }
    }
    calendarViewValueOnChange = (value) => {
        let fileData = {};
        fileData.calendarViewValue = value;
        let dateType = 'D';
        switch (value) {
            case 'M':
                dateType = 'month';
                break;
            case 'W':
                dateType = 'week';
                break;
            default:
                dateType = 'day';
                break;
        }
        fileData.dateFrom = moment().startOf(dateType);
        fileData.dateTo = moment().endOf(dateType);
        this.getCalendarData(fileData);
    }

    subtractDate = () => {
        let fileData = {};
        fileData.dateFrom = moment(this.props.dateFrom).subtract(1, this.props.calendarViewValue === 'D' ? 'days' : this.props.calendarViewValue);
        fileData.dateTo = moment(this.props.dateTo).subtract(1, this.props.calendarViewValue === 'D' ? 'days' : this.props.calendarViewValue);
        this.getCalendarData(fileData);
    }
    addDate = () => {
        let fileData = {};
        fileData.dateFrom = moment(this.props.dateFrom).add(1, this.props.calendarViewValue === 'D' ? 'days' : this.props.calendarViewValue);
        fileData.dateTo = moment(this.props.dateTo).add(1, this.props.calendarViewValue === 'D' ? 'days' : this.props.calendarViewValue);
        this.getCalendarData(fileData);
    }
    backToday = () => {
        let fileData = {};
        fileData.dateFrom = moment();
        fileData.dateTo = moment(fileData.dateFrom).add(1, this.props.calendarViewValue === 'D' ? 'days' : this.props.calendarViewValue);
        this.getCalendarData(fileData);
    }

    dayViewSelectTimeSlot = (rowData) => {
        let appointmentData = {};
        let bookData = _.cloneDeep(initBookingData);
        let newEncounterType = _.cloneDeep(initEncounterType);
        newEncounterType.encounterTypeCd = rowData.encounterType;
        newEncounterType.subEncounterTypeCd = rowData.subEncounterType;
        newEncounterType.appointmentDate = rowData.date;
        newEncounterType.appointmentTime = rowData.time;
        newEncounterType.appointmentTypeCd = rowData.normalRemain > 0 ? 'N' : 'F';
        bookData.clinicCd = this.props.clinicValue;
        bookData.encounterTypes[0] = newEncounterType;
        appointmentData.bookData = bookData;
        appointmentData.showMakeAppointmentView = true;
        console.log(bookData);
        console.log(newEncounterType);
        if (this.props.patientInfo && this.props.patientInfo.patientKey) {
            this.props.skipTab(AccessRightEnum.booking, appointmentData, true);
        } else {
            this.props.skipTab(AccessRightEnum.bookingAnonymous, appointmentData, true);
        }
    }

    showRemarkDialog = (remarkStore) => {
        this.setState({ remarkStore: remarkStore, openRemarkDialog: true });
    }

    closeRemarkDialog = () => {
        this.setState({ openRemarkDialog: false });
    }
    openOrClosePopper = (e, action, remark) => {
        this.setState({
            openPopper: action,
            popperAnchorEl: e,
            popperRemark: remark
        });
    }
    render() {
        const {
            classes,
            encounterTypeListData,
            encounterTypeValue,
            selectEncounterType,
            subEncounterTypeListData,
            subEncounterTypeValue,
            calendarViewValue,
            dateFrom,
            dateTo
        } = this.props;
        return (
            <Grid id={'calendarView'} className={classes.root}>
                <FilterBar
                    id={'calendarViewFilterBar'}
                    classes={classes}
                    encounterTypeListData={encounterTypeListData}
                    encounterTypeValue={encounterTypeValue}
                    encounterTypeValueOnChange={this.encounterTypeValueOnChange}
                    selectEncounterType={selectEncounterType}
                    subEncounterTypeListData={subEncounterTypeListData}
                    subEncounterTypeValue={subEncounterTypeValue}
                    checkboxOnChange={this.checkboxOnChange}
                />
                <Grid className={classes.calendarRoot}>
                    <CalendarDateBar
                        id={'calendarCalendarDateBar'}
                        classes={classes}
                        calendarViewValue={calendarViewValue}
                        dateFrom={dateFrom}
                        dateTo={dateTo}
                        subtractDate={this.subtractDate}
                        addDate={this.addDate}
                        backToday={this.backToday}
                        calendarViewValueOnChange={this.calendarViewValueOnChange}
                    />
                    <Grid className={classes.calendarView}>
                        <DayView
                            id={'calendarDayView'}
                            selectTimeSlot={this.dayViewSelectTimeSlot}
                            showRemarkDialog={this.showRemarkDialog}
                            openOrClosePopper={this.openOrClosePopper}
                        />
                        <WeekView
                            id={'calendarWeekView'}
                        />
                        <MonthView
                            id={'calendarMonthView'}
                        />
                    </Grid>
                </Grid>
                <RemarkDialog
                    id={'calendarViewRemarkDialog'}
                    remarkStore={this.state.remarkStore}
                    open={this.state.openRemarkDialog}
                    // remarkHover={this.remarkHover}
                    onClose={this.closeRemarkDialog}
                />
                <RemarkPopper
                    id={'calendarViewRemarkPopper'}
                    open={this.state.openPopper}
                    anchorEl={this.state.popperAnchorEl}
                    remarkData={this.state.popperRemark}
                    transition
                />
            </Grid>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        tabsActiveKey: state.mainFrame.tabsActiveKey,
        serviceCd: state.login.service.serviceCd,
        clinicCd: state.login.clinic.clinicCd,
        clinicConfig: state.common.clinicConfig,
        calendarViewValue: state.calendarView.calendarViewValue,
        availableQuotaValue: state.calendarView.availableQuotaValue,
        clinicValue: state.calendarView.clinicValue,
        encounterTypeValue: state.calendarView.encounterTypeValue,
        selectEncounterType: state.calendarView.selectEncounterType,
        subEncounterTypeValue: state.calendarView.subEncounterTypeValue,
        dateFrom: state.calendarView.dateFrom,
        dateTo: state.calendarView.dateTo,
        encounterTypeListData: state.calendarView.encounterTypeListData,
        subEncounterTypeListData: state.calendarView.subEncounterTypeListData,
        calendarData: state.calendarView.calendarData,
        subEncounterTypeListKeyAndValue: state.calendarView.subEncounterTypeListKeyAndValue,
        patientInfo: state.patient.patientInfo
    };
};

const mapDispatchToProps = {
    requestData,
    resetAll,
    updateField,
    skipTab
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(CalendarView));