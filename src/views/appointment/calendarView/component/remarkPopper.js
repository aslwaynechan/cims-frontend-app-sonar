import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import {
    Grid,
    Popper,
    Paper
} from '@material-ui/core';
import * as appointmentUtilities from '../../../../utilities/appointmentUtilities';
const styles = () => ({
    root: {
        zIndex: 1360
    },
    paper: {
        maxWidth: '30%',
        minWidth: 480,
        padding: 10
    },
    remarkTitle: {
        background: 'rgb(208, 240, 251)',
        borderLeft: '6px solid #a6d1f5',
        padding: '10px 8px',
        marginBottom: 10,
        fontWeight: 'bolder',
        wordBreak: 'break-word'
    },
    remarkContent: {
        color: '#666',
        fontWeight: 'bold',
        '& .remarkTime':{
            marginBottom:10
        },
        '& .remarkValue':{
            marginLeft:10,
            fontWeight: 'normal',
            wordBreak: 'break-word'
        }
    }
});
class RemarkPopper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openPopper: false,
            anchorEl: null
        };
    }
    render() {
        const { id,remarkData, classes, ...rest } = this.props;
        return (
            <Popper
                id={id}
                className={classes.root}
                {...rest}
            >
            {!remarkData?<div></div>:
                <Paper className={classes.paper}>
                    <Grid id={id+'RemarkTitle'} className={classes.remarkTitle}>{appointmentUtilities.slotRemark(remarkData)}</Grid>
                    <Grid id={id+'RemarkContent'} className={classes.remarkContent}>
                        <Grid className={'remarkTime'}>
                            <Grid id={id+'StartTime'}>Start:<span className={'remarkValue'}>{`${remarkData.slotDate} ${remarkData.startTime}`}</span></Grid>
                        </Grid>
                        <Grid>
                            {
                                !remarkData.engGivename&&!remarkData.engSurname?null:
                                <Grid id={id+'Name'}>Name:<span className={'remarkValue'}>{`${remarkData.engGivename||''} ${remarkData.engSurname||''}`}</span></Grid>
                            }
                            {
                                !remarkData.phoneNo?null:
                                <Grid id={id+'Phone'}>Phone:<span className={'remarkValue'}>{remarkData.phoneNo}</span></Grid>
                            }
                            {
                                !remarkData.userEngGivenName&&!remarkData.userEngSurname?null:
                                <Grid id={id+'RecordedBy'}>Recorded by:<span className={'remarkValue'}>{`${remarkData.userEngGivenName||''} ${remarkData.userEngSurname||''}`}</span></Grid>
                            }
                        </Grid>
                    </Grid>
                </Paper>
            }
            </Popper>
        );
    }
}

export default (withStyles(styles)(RemarkPopper));