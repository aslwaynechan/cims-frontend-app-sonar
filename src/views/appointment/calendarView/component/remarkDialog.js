import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import AutoScrollTable from '../../../../components/Table/AutoScrollTable';
import CIMSPromptDialog from '../../../../components/Dialog/CIMSPromptDialog';
import moment from 'moment';
import _ from 'lodash';
import Enum from '../../../../enums/enum';
// import Enum from '../../../enums/enum';

const styles = () => ({
    root: {
        minHeight: 300,
        maxHeight: 400,
        overflowY: 'auto',
        overflowX: 'hidden'
    },
    slotRemarks: {
        flex: 1,
        width: '100%',
        marginBottom: 6,
        '& .remark': {
            background: 'rgb(208, 240, 251)',
            borderLeft: '6px solid #a6d1f5',
            marginBottom: 4,
            paddingLeft: 6,
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
            padding: 5
        },
        '& .remark:last-child': {
            marginBottom: 0
        }
    },
    paper: {
        minWidth: 300,
        maxWidth: 1300,
        borderRadius: 16,
        backgroundColor: 'rgba(249, 249, 249, 0.08)'
    }
});

class RemarkDialog extends Component {

    state = {
        columns: [
            { label: 'Appt. Date/Time', name: 'date', width: '150px', customBodyRender: (value, rowData) => `${moment(rowData.slotDate).format(Enum.DATE_FORMAT_EDMY_VALUE)} ${moment(rowData.startTime, Enum.TIME_FORMAT_24_HOUR_CLOCK).format(Enum.TIME_FORMAT_24_HOUR_CLOCK)}` },
            { label: 'Name', name: 'name', width: '120px', customBodyRender: (value, rowData) => `${rowData.engGivename || ''} ${rowData.engSurname || ''}` },
            { label: 'Phone', name: 'phoneNo', width: '120px', customBodyRender: (value, rowData) => `${rowData.countryCd && rowData.countryCd != '852' && rowData.countryCd != 'HKG' ? '(' + rowData.countryCd + ') ' : ''}${rowData.phoneNo || ''}` },
            { label: 'By User', name: 'byUser', width: '150px', customBodyRender: (value, rowData) => `${rowData.engGivename || ''} ${rowData.engSurname || ''}` },
            { label: 'Remark/Memo', name: 'appointmentRemark', customBodyRender: (value, rowData) => `${rowData.appointmentRemark ? rowData.appointmentRemark : ''}${rowData.appointmentRemark && rowData.appointmentMemo ? ', ' : ''}${rowData.appointmentMemo ? rowData.appointmentMemo : ''}` }
        ],
        serviceCd: '',
        selectIndex: []
    }


    handleTableRowClick = (e, row, index) => {
        if (this.state.selectIndex.indexOf(index) > -1) {
            this.setState({ selectIndex: [] });
        } else {
            this.setState({ selectIndex: [index] });
        }
    }

    onHover = (e, action, remark) => {
        if (typeof this.props.remarkHover === 'function') {
            this.props.remarkHover(e, this.props.rowData, action, remark);
        }
    };

    render() {
        //eslint-disable-next-line
        const { id, classes, open, remarkStore, onClose } = this.props;
        return (
            <CIMSPromptDialog
                paperStyl={this.props.classes.paper}
                id={id}
                open={open}
                dialogTitle="Calendar View Detail"
                onClose={(e) => { onClose(e); }}
                dialogContentText={
                    <AutoScrollTable
                        id={id + 'AutoScrollTable'}
                        columns={this.state.columns}
                        store={remarkStore}
                        selectIndex={this.state.selectIndex}
                        handleRowClick={this.handleTableRowClick}
                    />
                }
                buttonConfig={
                    [
                        {
                            id: id + 'CloseButton',
                            name: 'Close',
                            onClick: onClose
                        }
                    ]
                }
            />
        );
    }
}

export default (withStyles(styles)(RemarkDialog));