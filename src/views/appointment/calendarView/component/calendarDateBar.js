import React from 'react';
import {
    Grid,
    ButtonGroup,
    Button
} from '@material-ui/core';
import moment from 'moment';
// import CIMSButton from '../../../../components/Buttons/CIMSButton'
export default function (props) {
    const {
        classes,
        id,
        calendarViewValue,
        dateFrom,
        dateTo,
        subtractDate,
        addDate,
        backToday,
        calendarViewValueOnChange
    } = props;
    return (
        <Grid id={id} className={classes.calendarDateBar}>
            <Grid className={'dateAction'}>
                <ButtonGroup
                    size="small"
                    className={'buttonGroup'}
                >
                    <Button
                        id={id+'TodayButton'}
                        classes={{
                            root: classes.buttonRoot,
                            label: classes.buttonLabel,
                            disabled: classes.buttonDisabled
                        }}
                        onClick={backToday}
                    >Today</Button>
                </ButtonGroup>
                <ButtonGroup
                    size="small"
                    className={'buttonGroup'}
                >
                    <Button
                        id={id+'PreviousButton'}
                        classes={{
                            root: classes.buttonRoot,
                            label: classes.buttonLabel,
                            disabled: classes.buttonDisabled
                        }}
                        onClick={subtractDate}
                    >{'<'}</Button>
                    <Button
                        id={id+'NextButton'}
                        classes={{
                            root: classes.buttonRoot,
                            label: classes.buttonLabel,
                            disabled: classes.disabled
                        }}
                        onClick={addDate}
                    >{'>'}</Button>
                </ButtonGroup>
                <label>
                    {
                        calendarViewValue === 'M' ? moment(dateFrom).format('MMM-YYYY') :
                            calendarViewValue === 'W' ? moment(dateFrom).format('MMM-D-YYYY') + ' ~ ' + moment(dateTo).format('MMM-D-YYYY') :
                                moment(dateFrom).format('dddd MMMM D, YYYY')
                    }
                </label>
            </Grid>
            <ButtonGroup
                size="small"
                className={classes.dateBarViewType}
            >
                <Button
                    id={id+'ViewTypeDayButton'}
                    classes={{
                        root: classes.buttonRoot,
                        label: classes.buttonLabel,
                        disabled: classes.buttonDisabled
                    }}
                    disabled={calendarViewValue === 'D'}
                    onClick={()=>{calendarViewValueOnChange('D');}}
                >Day</Button>
                <Button
                    id={id+'ViewTypeWeekButton'}
                    classes={{
                        root: classes.buttonRoot,
                        label: classes.buttonLabel,
                        disabled: classes.disabled
                    }}
                    disabled={calendarViewValue === 'W'}
                    onClick={()=>{calendarViewValueOnChange('W');}}
                >Week</Button>
                <Button
                    id={id+'ViewTypeMonthButton'}
                    classes={{
                        root: classes.buttonRoot,
                        label: classes.buttonLabel,
                        disabled: classes.buttonDisabled
                    }}
                    disabled={calendarViewValue === 'M'}
                    onClick={()=>{calendarViewValueOnChange('M');}}
                >Month</Button>
            </ButtonGroup>
        </Grid>
    );
}