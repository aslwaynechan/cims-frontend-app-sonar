import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import {
    Grid
} from '@material-ui/core';
import { connect } from 'react-redux';
import moment from 'moment';
import Enum from '../../../../../enums/enum';
// import DayViewRow from './dayViewRow';
// import RemarkPopper from './remarkPopper';
// import RemarkDialog from '../remarkDialog';
// import {
//     requestData,
//     resetAll,
//     destroyPage,
//     updateField
// } from '../../../store/actions/appointment/booking/bookingAction';

const styles = () => ({
    root: {
        width: '100%',
        height: '100%',
        display: 'flex'
    },
    button: {
        textTransform: 'none',
        width: '100%',
        height: '100%',
        padding: 0
    },
    table: {
        flex: 1,
        height: '100%',
        maxHeight: 800,
        marginRight: 10,
        display: 'flex',
        flexFlow: 'column',
        '&:last-child': {
            marginRight: 0
        },
        '& .tableHead': {
            '& .row': {
                display: 'flex',
                border: '1px solid #ccc',
                '&:not(.titleRow)': {
                    background: 'rgb(208, 240, 251)'
                },
                '&.titleRow': {
                    border: 'none',
                    '& .tableCell': {
                        borderColor: 'rgba(255,255,255,0)',
                        height: 30,
                        '&.titleCell': {
                            border: '1px solid #ccc',
                            borderBottom: 'none',
                            lineHeight: '30px',
                            background: '#fcf7b6',
                            'text-overflow': 'ellipsis',
                            'white-space': 'nowrap',
                            overflow: 'hidden'
                        }
                    }
                },
                '& .tableCell': {
                    textAlign: 'center',
                    height: 40,
                    lineHeight: '40px'
                }
            }
        },
        '& .tableBody': {
            flex: 1,
            display: 'flex',
            flexFlow: 'column',
            maxHeight: '100%',
            border: '1px solid #ccc',
            borderTop: 'none',
            // overflowY: 'auto',
            marginBottom: 7,
            '& .row': {
                display: 'flex',
                flex: 1,
                borderBottom: '1px solid #ccc',
                '&:last-child': {
                    marginRight: 0
                },
                '& .tableCell': {
                    minHeight: 100
                }
            }
        },
        '& .tableCell': {
            flex: 1,
            borderRight: '1px solid #ccc',
            minWidth: 109,
            '&:last-child': {
                borderRight: 'none'
            }
        }
    }
});

class MonthView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openPopper: false,
            anchorEl: null,
            openDialog: false,
            rowData: null,
            tables: []
        };
    }

    dataProcessing = (data = [{}]) => {
        let testWeek = '0111111';
        console.log('moment().isoWeekday(1)', moment().isoWeekday(1).format('dddd'));
        let tables = [];
        console.log('calendarData', data);
        data.forEach(item => {
            let table = {};
            table.title =
                this.props.subEncounterTypeListKeyAndValue[item.subEncounterType] &&
                this.props.subEncounterTypeListKeyAndValue[item.subEncounterType].shortName;
            table.columns = [];
            for (let i = testWeek.indexOf('1'); i > -1; i = testWeek.indexOf('1', i + 1)) {
                table.columns.push(moment().isoWeekday(i === 0 ? 7 : i).format('dddd'));
            }
            table.rows = [];
            let row = {};
            let cell = {
                encounterType: this.props.encounterTypeValue,
                subEncounterType: item.subEncounterType,
                time: ''
            };
            item.slotByDateDtos.forEach((dateDto, i) => {
                if (i > 0 && i % 7 === 0) {
                    table.rows.push(row);

                } else {
                    // row[moment(dateDto.date, 'YYYY-MM-DD').format('dddd')] = {
                        row[moment(dateDto.date, Enum.DATE_FORMAT_EYMD_VALUE).format('dddd')] = {
                        ...cell,
                        date: dateDto.date,
                        holiday: dateDto.holiday,
                        noSlot: dateDto.tolalRemain === undefined,
                        holidayDesc: dateDto.holidayDesc,
                        normalRemain: dateDto.normalRemain || 0,
                        urgentRemain: dateDto.urgentRemain || 0,
                        tolalRemain: dateDto.tolalRemain || 0,
                        remark: dateDto.appointmentRemarkDtos
                    };
                }
            });
            console.log('table', table);

            // table.row = [];
            // let startTime = '09:00';
            // let endTime = '18:00';
            // table.isHoliday = item.slotByDateDtos[0].holiday;
            // table.holidayDesc = item.slotByDateDtos[0].holidayDesc;
            // table.date = item.slotByDateDtos[0].date;
            // let startHours = moment(startTime, 'HH:mm').hours();
            // let endHours = moment(endTime, 'HH:mm').hours();
            // for (let i = startHours; i <= endHours; i++) {
            //     table.row.push({
            //         encounterType:this.props.encounterTypeValue,
            //         subEncounterType:item.subEncounterType,
            //         date: item.slotByDateDtos[0].date,
            //         time: moment(i, 'H').format('HH:mm'),
            //         normalRemain: 0,
            //         urgentRemain: 0,
            //         tolalRemain: 0,
            //         remark: [],
            //         timeSlot: []
            //     });
            // }
            // if (!item.slotByDateDtos[0].slotByHourDtos || item.slotByDateDtos[0].slotByHourDtos.length < 1) {
            //     table.isNotSlot = true;
            // } else {
            //     item.slotByDateDtos[0].slotByHourDtos.forEach(hourDtos => {
            //         console.log('hourDtos',hourDtos);
            //         let rowIndex = moment(hourDtos.startTime, 'HH:mm').hours() - startHours;
            //         console.log('rowIndex',rowIndex);
            //         if(rowIndex>=0&&rowIndex<table.row.length){
            //             let appointmentRemar = hourDtos.appointmentRemarkDtos || [];
            //             table.row[rowIndex].normalRemain += hourDtos.normalRemain * 1;
            //             table.row[rowIndex].urgentRemain += hourDtos.urgentRemain * 1;
            //             table.row[rowIndex].tolalRemain += hourDtos.tolalRemain * 1;
            //             table.row[rowIndex].remark = [...table.row[rowIndex].remark, ...appointmentRemar];
            //             table.row[rowIndex].timeSlot.push(hourDtos);
            //         }
            //     });
            // }
            tables.push(table);
        });
        return tables;
    }
    render() {
        //eslint-disable-next-line
        const { classes, id, calendarViewValue, selectTimeSlot, calendarData } = this.props;
        let tables = this.dataProcessing(calendarData);
        console.log('tables', tables);
        return (
            calendarViewValue !== 'M' ? null :
                <Grid id={id} className={classes.root}>
                    {tables.map((table, i) => (
                        <Grid key={i} className={classes.table}>
                            <Grid className={'tableHead'}>
                                <Grid className={'row titleRow'}>
                                    <Grid className={'titleCell tableCell'} title={table.title}>{table.title}</Grid>
                                    {table.columns.map((v, i) =>
                                        i < table.columns.length - 1 ? <Grid key={i} className={'tableCell'}></Grid> : null
                                    )}
                                </Grid>
                                <Grid className={'row'}>
                                    {table.columns.map((v, i) =>
                                        <Grid key={i} className={'tableCell'}>{v}</Grid>
                                    )}
                                </Grid>
                            </Grid>
                            <Grid className={'tableBody'}>
                                {table.rows.map((v, i) => (
                                    <Grid key={i} className={'row'}>
                                        {table.columns.map((v, i) => <Grid key={i} className={'tableCell'}></Grid>
                                        )}
                                    </Grid>
                                ))}
                            </Grid>
                        </Grid>
                    ))}
                </Grid>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        calendarViewValue: state.calendarView.calendarViewValue,
        calendarData: state.calendarView.calendarData,
        encounterTypeValue: state.calendarView.encounterTypeValue,
        subEncounterTypeListKeyAndValue: state.calendarView.subEncounterTypeListKeyAndValue
    };
};

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(MonthView));