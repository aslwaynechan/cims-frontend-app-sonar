import React from 'react';
// import { withStyles } from '@material-ui/core';
import {
    //Checkbox,
    Grid,
    FormControl,
    FormControlLabel,
    FormGroup,
    FormLabel
    // RadioGroup,
    // Radio,
    // IconButton
} from '@material-ui/core';
// import { KeyboardArrowRight, KeyboardArrowLeft } from '@material-ui/icons';
import ValidatorForm from '../../../../components/FormValidator/ValidatorForm';
import SelectFieldValidator from '../../../../components/FormValidator/SelectFieldValidator';
// import moment from 'moment';
// import _ from 'lodash';
import CIMSCheckBox from '../../../../components/CheckBox/CIMSCheckBox';

export default function (props) {
    const {
        classes,
        id,
        encounterTypeListData,
        encounterTypeValue,
        encounterTypeValueOnChange,
        selectEncounterType,
        subEncounterTypeListData,
        subEncounterTypeValue,
        checkboxOnChange
    } = props;
    return (
        <ValidatorForm
            id={id + 'Form'}
            className={classes.form}
            // ref="form"
            onSubmit={() => { }}
        >
            <FormControl className={classes.formControl}>
                <FormLabel component="legend" className={classes.formLabel}>Encounter</FormLabel>
                <SelectFieldValidator
                    id={id + 'EncounterTypeSelectField'}
                    options={encounterTypeListData.map((item) => ({ value: item.encounterTypeCd, label: item.encounterTypeCd, rowData: item }))}
                    msgPosition="bottom"
                    value={encounterTypeValue}
                    onChange={encounterTypeValueOnChange}
                />
                <Grid className={classes.shortNameLable}>
                    {selectEncounterType.shortName}
                </Grid>
            </FormControl>
            <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend" className={classes.formLabel} >Sub-encounter</FormLabel>
                <FormGroup id={id+'SubEncounterTypeCheckboxGroup'} className={classes.checkboxFormGroup}>
                    {subEncounterTypeListData && subEncounterTypeListData.length > 0 ?
                        <Grid className={classes.checkboxGroup}>
                            {subEncounterTypeListData.map((item, index) => (
                                <FormControlLabel
                                    key={index}
                                    id={id+'SubEncounterTypeCheckboxGroupCheckbox' + index}
                                    className={classes.formControlLabel}
                                    control={
                                        <CIMSCheckBox
                                            //style={{ padding: 4 }}
                                            checked={subEncounterTypeValue.indexOf(item.subEncounterTypeCd) !== -1}
                                            onChange={() => { checkboxOnChange('subEncounterTypeValue', item.subEncounterTypeCd); }}
                                            value={item.subEncounterTypeCd}
                                            //color={'Secondary'}
                                            //disabled
                                        />
                                    }
                                    label={<div className={classes.checkboxLabel}>{item.subEncounterTypeCd + ' ' + item.shortName}</div>}
                                />
                            ))
                            }
                        </Grid> : null
                    }
                </FormGroup>
            </FormControl>
        </ValidatorForm>
    );
}