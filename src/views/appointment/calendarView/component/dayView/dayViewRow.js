import React, { Component } from 'react';
import {
    Grid,
    Button
} from '@material-ui/core';
import * as appointmentUtilities from '../../../../../utilities/appointmentUtilities';
import moment from 'moment';
import Enum from '../../../../../enums/enum';
class DayViewRow extends Component {
    moreBtnOnClick = (e) => {
        e.preventDefault();
        e.stopPropagation();
        if (typeof this.props.moreOnClick === 'function') {
            this.props.moreOnClick(this.props.rowData);
        }
    };
    handleHours = (hours) => {
        if (moment() >= moment(hours, 'HH:mm') && moment() < moment((hours.substring(0, 3) + '30'),'HH:mm')) {
            return hours;
        } else if (moment() >= moment((hours.substring(0, 3) + '30'),'HH:mm') && moment() < moment((hours.substring(0, 3) + '00'),'HH:mm').add(1,'hours')) {
            return moment((hours.substring(0, 3) + '30'),'HH:mm').format('HH:mm');
        } else if (moment() == moment((hours.substring(0, 3) + '00'),'HH:mm').add(1,'hours')) {
            return moment((hours.substring(0, 3) + '00'),'HH:mm').format('HH:mm');
        }
        return hours;
    }
    seletRow = () => {
        if (typeof this.props.selectTimeSlot === 'function') {
            let hours = this.handleHours(this.props.rowData.time);
            let params = { ...this.props.rowData, time: hours };
            this.props.selectTimeSlot(params);
        }
    };
    onHover = (e, action, remark) => {
        if (typeof this.props.remarkHover === 'function') {
            this.props.remarkHover(e.currentTarget, action, remark, this.props.rowData);
        }
    };
    render() {
        const { id, classes, rowData } = this.props;

        return (
            !rowData ? null :
                <Grid id={id} className={'row slot'}>
                    <Grid className={'leftCell'}>
                        <Grid className={'slotTime'}>{rowData.time}</Grid>
                        {
                            rowData.timeSlot.length <= 0 || rowData.tolalRemain === 0 ?
                                <Grid className={'quota'} style={{ color: '#666' }}>
                                    {
                                        rowData.timeSlot.length <= 0 ?
                                            <span>{'No Slot'}</span> :
                                            <span>{'No available quota'}</span>
                                    }
                                </Grid>
                                :
                                <Grid className={'quota'} style={{ color: moment(rowData.date + ' ' + rowData.time, Enum.DATE_FORMAT_EYMD_VALUE + ' ' + Enum.TIME_FORMAT_24_HOUR_CLOCK) < moment().subtract(1, 'hours') ? 'gray' : (rowData.normalRemain === 0 ? 'red' : 'limegreen') }}>
                                    <span>{'N:' + rowData.normalRemain}</span>
                                    <span>{'F:' + rowData.forceRemain}</span>
                                    <span>{'T:' + rowData.tolalRemain}</span>
                                </Grid>
                        }

                    </Grid>
                    <Grid id={id + 'SelectButton'} className={moment(rowData.date + ' ' + rowData.time, Enum.DATE_FORMAT_EYMD_VALUE + ' ' + Enum.TIME_FORMAT_24_HOUR_CLOCK) < moment().subtract(1, 'hours') || rowData.tolalRemain === 0 ? 'rightCell noSlot' : 'rightCell'} onClick={moment(rowData.date + ' ' + rowData.time, Enum.DATE_FORMAT_EYMD_VALUE + ' ' + Enum.TIME_FORMAT_24_HOUR_CLOCK) < moment().subtract(1, 'hours') || rowData.tolalRemain === 0 ? null : this.seletRow}>
                        {!rowData.remark || rowData.remark.length <= 0 ? null :
                            <Grid className={'slotRemarks'} >
                                {rowData.remark.map((item, index) =>
                                    index >= 2 ? null :
                                        <Grid
                                            key={index}
                                            className={'remark'}
                                            onMouseEnter={(e) => this.onHover(e, true, item)}
                                            onMouseLeave={(e) => this.onHover(e, false, item)}
                                        >
                                            {
                                                appointmentUtilities.slotRemark(item)
                                            }
                                        </Grid>
                                )}
                            </Grid>
                        }
                        {!rowData.remark || rowData.remark.length <= 2 ? null :
                            <Grid className={'moreRemark'}>
                                <Button
                                    id={id + 'MoreButton'}
                                    size="small"
                                    color="primary"
                                    className={classes.button}
                                    onClick={this.moreBtnOnClick}
                                >
                                    {`+${rowData.remark.length - 2} more`}
                                </Button>
                            </Grid>
                        }
                    </Grid>
                </Grid>
        );
    }
}
export default DayViewRow;