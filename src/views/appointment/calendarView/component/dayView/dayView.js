import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import {
    Grid
} from '@material-ui/core';
import { connect } from 'react-redux';
import moment from 'moment';
import DayViewRow from './dayViewRow';
// import {
//     requestData,
//     resetAll,
//     destroyPage,
//     updateField
// } from '../../../store/actions/appointment/booking/bookingAction';

const styles = () => ({
    root: {
        width: '100%',
        height: '100%',
        display: 'flex'
    },
    button: {
        textTransform: 'none',
        width: '100%',
        height: '100%',
        padding: 0
    },
    table: {
        minWidth: 400,
        flex: 1,
        height: '100%',
        marginRight: 10,
        display: 'flex',
        flexFlow: 'column',
        '&:last-child': {
            marginRight: 0
        },
        '& .tableTitle': {
            display: 'flex',
            '& .leftCell': {
                border: '1px solid #ccc',
                borderBottom: 'none',
                height: 28,
                lineHeight: '28px',
                background: '#fcf7b6',
                'text-overflow': 'ellipsis',
                'white-space': 'nowrap',
                overflow: 'hidden'
            }
        },
        '& .tableHead': {
            display: 'flex',
            border: '1px solid #ccc',
            borderBottom: 'none',
            background: 'rgb(208, 240, 251)',
            '& .rightCell': {
                textAlign: 'center',
                display: 'block'
            }
        },
        '& .tableBody': {
            maxHeight: '100%',
            border: '1px solid #ccc',
            overflowY: 'auto',
            marginBottom: 7
        },
        '& .row': {
            display: 'flex',
            borderBottom: '1px solid #ccc',
            height: 52,
            '&:last-child': {
                borderBottom: 'none'
            },
            '&.noSlot': {
                color: ' rgb(92, 96, 94)',
                '& .rightCell': {
                    textAlign: 'center',
                    lineHeight: '40px',
                    background: 'rgb(255, 255, 204)',
                    display: 'block',
                    '&.holiday': {
                        background: 'rgb(254, 210, 217)'
                    }
                }
            },
            '&.slot': {
                '& .rightCell': {
                    cursor: 'pointer',
                    '&.noSlot': {
                        cursor: 'auto'
                    }
                }
            },
            '& .leftCell': {
                position: 'relative',
                '& .slotTime': {
                    paddingTop: 10
                },
                '& .quota': {
                    position: 'absolute',
                    bottom: 0,
                    left: 0,
                    width: '100%',
                    '& span': {
                        fontSize: 12,
                        marginRight: 4,
                        '&:last-child': {
                            marginRight: 0
                        }
                    }
                }
            }
        },
        '& .leftCell': {
            width: 120,
            minWidth: 120,
            borderRight: '1px solid #ccc',
            padding: '0 4px',
            textAlign: 'center'
        },
        '& .rightCell': {
            width: 'calc(100% - 135px )',
            display: 'flex',
            padding: 3,
            '& .slotRemarks': {
                flex: 1,
                width: 'calc(100% - 80px )',
                '& .remark': {
                    background: 'rgb(208, 240, 251)',
                    borderLeft: '6px solid #a6d1f5',
                    marginBottom: 4,
                    paddingLeft: 6,
                    textOverflow: 'ellipsis',
                    overflow: 'hidden',
                    whiteSpace: 'nowrap'
                },
                '& .remark:last-child': {
                    marginBottom: 0
                }
            },
            '& .moreRemark': {
                width: 80,
                height: '100%',
                color: '#0579c8',
                alignSelf: 'flex-end'
            }
        }
    }
});

class DayView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tables:[]
        };
    }
    shouldComponentUpdate(lastPro,lastState){
        if(lastPro.calendarData!==this.props.calendarData&&lastPro.calendarViewValue==='D'){
            lastState.tables = this.dataProcessing(lastPro.calendarData);
        }
        return true;
    }

    dataProcessing = (data = []) => {
        let tables = [];
        data.forEach(item => {
            let table = {};
            table.title = this.props.subEncounterTypeListKeyAndValue[item.subEncounterType].shortName;
            table.head = moment(item.slotByDateDtos[0].date).format('dddd');
            table.row = [];
            let startTime = '09:00';
            let endTime = '18:00';
            table.isHoliday = item.slotByDateDtos[0].holiday;
            table.holidayDesc = item.slotByDateDtos[0].holidayDesc;
            table.date = item.slotByDateDtos[0].date;
            let startHours = moment(startTime, 'HH:mm').hours();
            let endHours = moment(endTime, 'HH:mm').hours();
            for (let i = startHours; i <= endHours; i++) {
                table.row.push({
                    encounterType:this.props.encounterTypeValue,
                    subEncounterType:item.subEncounterType,
                    date: item.slotByDateDtos[0].date,
                    time: moment(i, 'H').format('HH:mm'),
                    normalRemain: 0,
                    forceRemain: 0,
                    tolalRemain: 0,
                    remark: [],
                    timeSlot: []
                });
            }
            if (!item.slotByDateDtos[0].slotByHourDtos || item.slotByDateDtos[0].slotByHourDtos.length < 1) {
                table.isNotSlot = true;
            } else {
                item.slotByDateDtos[0].slotByHourDtos.forEach(hourDtos => {
                    console.log('hourDtos',hourDtos);
                    let rowIndex = moment(hourDtos.startTime, 'HH:mm').hours() - startHours;
                    console.log('rowIndex',rowIndex);
                    if(rowIndex>=0&&rowIndex<table.row.length){
                        let appointmentRemar = hourDtos.appointmentRemarkDtos || [];
                        table.row[rowIndex].normalRemain += hourDtos.normalRemain * 1;
                        table.row[rowIndex].forceRemain += hourDtos.forceRemain * 1;
                        table.row[rowIndex].tolalRemain += hourDtos.tolalRemain * 1;
                        table.row[rowIndex].remark = [...table.row[rowIndex].remark, ...appointmentRemar];
                        table.row[rowIndex].timeSlot.push(hourDtos);
                    }
                });
            }
            tables.push(table);
        });
        return tables;
    }

    moreOnClick = (rowData) => {
        this.props.showRemarkDialog(rowData.remark||[]);
    }
    remarkHover = (e, action, remark) => {
        this.props.openOrClosePopper(e, action, remark);
    }
    render() {
        const { classes, id,calendarViewValue,selectTimeSlot } = this.props;
        return (
            calendarViewValue!=='D'?null:
            <Grid id={id} className={classes.root}>
                {this.state.tables.map((table, i) => (
                    <Grid id={`${id}Table${i}`} key={table.title + table.head} className={classes.table}>
                        <Grid className={'tableTitle'}>
                            <Grid id={`${id}Table${i}Title`} className={'leftCell'} title={table.title}>{table.title}</Grid>
                            <Grid className={'rightCell'}></Grid>
                        </Grid>
                        <Grid className={'tableHead'}>
                            <Grid className={'leftCell'}></Grid>
                            <Grid className={'rightCell'}>{table.head}</Grid>
                        </Grid>
                        <Grid className={'tableBody'}>
                            {
                                table.isHoliday || table.isNotSlot ?
                                    <Grid className={'row noSlot'}>
                                        <Grid className={'leftCell'}>
                                        </Grid>
                                        <Grid id={`${id}Table${i}HolidayOrNoSlot`} className={`rightCell ${table.isHoliday?'holiday':''}`}>
                                            {table.isHoliday?table.holidayDesc:'No Slot'}
                                        </Grid>
                                    </Grid> :
                                    table.row.map((row, index) => (
                                        <DayViewRow
                                            id={`${id}Table${i}Row${index}`}
                                            key={row.startTime || index}
                                            classes={classes}
                                            rowData={row}
                                            moreOnClick={this.moreOnClick}
                                            selectTimeSlot={selectTimeSlot}
                                            remarkHover={this.remarkHover}
                                        />
                                    ))
                            }
                        </Grid>
                    </Grid>
                ))}
            </Grid>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        calendarViewValue: state.calendarView.calendarViewValue,
        calendarData: state.calendarView.calendarData,
        encounterTypeValue: state.calendarView.encounterTypeValue,
        subEncounterTypeListKeyAndValue: state.calendarView.subEncounterTypeListKeyAndValue
    };
};

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DayView));