import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import {
    Grid
} from '@material-ui/core';
import { connect } from 'react-redux';
import moment from 'moment';
// import DayViewRow from './dayViewRow';
// import RemarkPopper from './remarkPopper';
// import RemarkDialog from '../remarkDialog';
// import {
//     requestData,
//     resetAll,
//     destroyPage,
//     updateField
// } from '../../../store/actions/appointment/booking/bookingAction';

const styles = () => ({
    root: {
        width: '100%',
        height: '100%',
        display: 'flex'
    },
    button: {
        textTransform: 'none',
        width: '100%',
        height: '100%',
        padding: 0
    },
    table: {
        minWidth: 400,
        flex: 1,
        height: '100%',
        marginRight: 10,
        display: 'flex',
        flexFlow: 'column',
        '&:last-child': {
            marginRight: 0
        },
        '& .tableTitle': {
            display: 'flex',
            '& .leftCell': {
                border: '1px solid #ccc',
                borderBottom: 'none',
                height: 28,
                lineHeight: '28px',
                background: '#fcf7b6'
            }
        },
        '& .tableHead': {
            display: 'flex',
            border: '1px solid #ccc',
            borderBottom: 'none',
            background: 'rgb(208, 240, 251)',
            '& .rightCell': {
                textAlign: 'center',
                display: 'block'
            }
        },
        '& .tableBody': {
            maxHeight: '100%',
            border: '1px solid #ccc',
            overflowY: 'auto',
            marginBottom: 7
        },
        '& .row': {
            display: 'flex',
            borderBottom: '1px solid #ccc',
            height: 52,
            '&:last-child': {
                borderBottom: 'none'
            },
            '&.noSlot': {
                color: ' rgb(92, 96, 94)',
                '& .rightCell': {
                    textAlign: 'center',
                    lineHeight: '40px',
                    background: 'rgb(255, 255, 204)',
                    display: 'block',
                    '&.holiday': {
                        background: 'rgb(254, 210, 217)'
                    }
                }
            },
            '&.slot': {
                '& .rightCell': {
                    cursor: 'pointer',
                    '&.noSlot': {
                        cursor: 'auto'
                    }
                }
            },
            '& .leftCell': {
                position: 'relative',
                '& .slotTime': {
                    paddingTop: 10
                },
                '& .quota': {
                    position: 'absolute',
                    bottom: 0,
                    left: 0,
                    width: '100%',
                    '& span': {
                        fontSize: 12,
                        marginRight: 4,
                        '&:last-child': {
                            marginRight: 0
                        }
                    }
                }
            }
        },
        '& .leftCell': {
            width: 120,
            minWidth: 120,
            borderRight: '1px solid #ccc',
            padding: '0 4px',
            textAlign: 'center'
        },
        '& .rightCell': {
            width: 'calc(100% - 135px )',
            display: 'flex',
            padding: 3,
            '& .slotRemarks': {
                flex: 1,
                width: 'calc(100% - 80px )',
                '& .remark': {
                    background: 'rgb(208, 240, 251)',
                    borderLeft: '6px solid #a6d1f5',
                    marginBottom: 4,
                    paddingLeft: 6,
                    textOverflow: 'ellipsis',
                    overflow: 'hidden',
                    whiteSpace: 'nowrap'
                },
                '& .remark:last-child': {
                    marginBottom: 0
                }
            },
            '& .moreRemark': {
                width: 80,
                height: '100%',
                color: '#0579c8',
                alignSelf: 'flex-end'
            }
        }
    }
});

class WeekView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openPopper: false,
            anchorEl: null,
            openDialog: false,
            rowData: null,
            tables: []
        };
    }
    shouldComponentUpdate(lastPro, lastState) {
        if(lastPro.calendarData!==this.props.calendarData&&lastPro.calendarViewValue==='W'){
            lastState.tables = this.dataProcessing(lastPro.calendarData);
        }
        return true;
    }

    dataProcessing = (data = []) => {
        let tables = [];
        data.forEach(item => {
            let table = {};
            table.title = this.props.subEncounterTypeListKeyAndValue[item.subEncounterType];
            table.head = moment(item.slotByDateDtos[0].date).format('dddd');
            table.row = [];
            let startTime = '09:00';
            let endTime = '18:00';
            table.isHoliday = item.slotByDateDtos[0].holiday;
            table.holidayDesc = item.slotByDateDtos[0].holidayDesc;
            table.date = item.slotByDateDtos[0].date;
            let startHours = moment(startTime, 'HH:mm').hours();
            let endHours = moment(endTime, 'HH:mm').hours();
            for (let i = startHours; i <= endHours; i++) {
                table.row.push({
                    encounterType: this.props.encounterTypeValue,
                    subEncounterType: item.subEncounterType,
                    date: item.slotByDateDtos[0].date,
                    time: moment(i, 'H').format('HH:mm'),
                    normalRemain: 0,
                    urgentRemain: 0,
                    tolalRemain: 0,
                    remark: [],
                    timeSlot: []
                });
            }
            if (!item.slotByDateDtos[0].slotByHourDtos || item.slotByDateDtos[0].slotByHourDtos.length < 1) {
                table.isNotSlot = true;
            } else {
                item.slotByDateDtos[0].slotByHourDtos.forEach(hourDtos => {
                    let rowIndex = moment(hourDtos.startTime, 'HH:mm').hours() - startHours;
                    let appointmentRemar = hourDtos.appointmentRemarkDtos || [];
                    table.row[rowIndex].normalRemain += hourDtos.normalRemain * 1;
                    table.row[rowIndex].urgentRemain += hourDtos.urgentRemain * 1;
                    table.row[rowIndex].tolalRemain += hourDtos.tolalRemain * 1;
                    table.row[rowIndex].remark = [...table.row[rowIndex].remark, ...appointmentRemar];
                    table.row[rowIndex].timeSlot.push(hourDtos);
                });
            }
            tables.push(table);
        });
        return tables;
    }

    moreOnClick = (rowData) => {
        console.log('moreOnClick', rowData);
        this.setState({ rowData: rowData, openDialog: true });
    }
    remarkHover = (e, rowData, action, remark) => {
        console.log('remarkHover', e, rowData, action, remark);
        this.setState({
            openPopper: action,
            anchorEl: e.target,
            remark: remark
        });
    }
    closeDialog = () => {
        this.setState({ openDialog: false });
    }
    render() {
        //eslint-disable-next-line
        const { classes, id, calendarViewValue, selectTimeSlot } = this.props;
        console.log('tables', this.state.tables);
        return (
            calendarViewValue !== 'W' ? null :
                <Grid id={id} className={classes.root}>
                    <Grid className={classes.table}>
                        <Grid className={'tableTitle'}>
                            <Grid className={'leftCell'}>{'title'}</Grid>
                            <Grid className={'rightCell'}></Grid>
                        </Grid>
                        <Grid className={'tableHead'}>
                            <Grid className={'leftCell'}></Grid>
                            <Grid className={'rightCell'}>{1}</Grid>
                            <Grid className={'rightCell'}>{1}</Grid>
                            <Grid className={'rightCell'}>{1}</Grid>
                            <Grid className={'rightCell'}>{1}</Grid>
                            <Grid className={'rightCell'}>{1}</Grid>
                            <Grid className={'rightCell'}>{1}</Grid>
                        </Grid>
                        <Grid className={'tableBody'}>
                            <Grid className={'row slot'}>
                                <Grid className={'leftCell'}>
                                </Grid>
                                <Grid className={'rightCell'}>
                                    {1}
                                </Grid>
                                <Grid className={'rightCell'}>{1}</Grid>
                                <Grid className={'rightCell'}>{1}</Grid>
                                <Grid className={'rightCell'}>{1}</Grid>
                                <Grid className={'rightCell'}>{1}</Grid>
                                <Grid className={'rightCell'}>{1}</Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        calendarViewValue: state.calendarView.calendarViewValue,
        calendarData: state.calendarView.calendarData,
        encounterTypeValue: state.calendarView.encounterTypeValue,
        subEncounterTypeListKeyAndValue: state.calendarView.subEncounterTypeListKeyAndValue
    };
};

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(WeekView));