import React from 'react';
import {
    Grid
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import AppointmentList from '../attendance/component/appointmentList';
import CIMSButton from '../../../components/Buttons/CIMSButton';
import SelectFieldValidator from '../../../components/FormValidator/SelectFieldValidator';
import TextFieldValidator from '../../../components/FormValidator/TextFieldValidator';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import Enum from '../../../enums/enum';
import { codeList } from '../../../constants/codeList';
import { openCommonMessage } from '../../../store/actions/message/messageAction';
import * as AttendanceUtilities from '../../../utilities/attendanceUtilities';
import * as CommonUtilities from '../../../utilities/commonUtilities';
import { openCaseNoDialog } from '../../../store/actions/caseNo/caseNoAction';
import {
    getPatientById as getPatientPanelPatientById
} from '../../../store/actions/patient/patientAction';
import { getPatientById as getRegPatientById } from '../../../store/actions/registration/registrationAction';
import { selectCaseTrigger } from '../../../store/actions/caseNo/caseNoAction';
import * as AppointmentUtilities from '../../../utilities/appointmentUtilities';
import moment from 'moment';
import {
    getPatientStatusList,
    listAppointmentList,
    backTakeAttendance,
    updateField,
    resetAll
} from '../../../store/actions/appointment/backTakeAttendance/backTakeAttendanceAction';
import CommonRegex from '../../../constants/commonRegex';
import {
    getEncounterType
} from '../../../store/actions/common/commonAction';
import ConfirmationWindow from '../../compontent/confirmationWindow';
import CIMSMultiTextField from '../../../components/TextField/CIMSMultiTextField';

const sysRatio = CommonUtilities.getSystemRatio();
const unit = CommonUtilities.getResizeUnit(sysRatio);
const styles = (theme) => ({
    root: {
        padding: 4
    },
    form: {
        width: '100%',
        margin: 0,
        padding: 0,
        flex: 1
        //height: '90%'
    },
    alignRightBtn: {
        position: 'absolute',
        right: 0,
        display: 'inline-block'
    },
    attendanceInfo: {
        height: '100%',
        backgroundColor: theme.palette.primary.main
    },
    attendanceInfoItem: {
        width: '100%',
        fontSize: '2.5em',
        color: theme.palette.white,
        textAlign: 'center',
        padding: '0px 8px'
    },
    apptInfoRow: {
        padding: `${18 * unit}px ${8 * unit}px`
    }
});

class BackTakeAttendance extends React.Component {
    state = {
        listApptParams: null,
        appointmentDateRangeStr: moment().format(Enum.DATE_FORMAT_EDMY_VALUE),
        bookedApptType: {
            caseType: ''
        }
    }

    componentWillMount() {
        this.props.resetAll();
    }

    componentDidMount() {
        this.props.ensureDidMount();
        this.getPatientStatusList();
        this.getAppointmentList();
        // this.getCasePatientStatus();
    }

    getPatientStatusList = () => {
        let params = [
            codeList.patient_status
        ];
        this.props.getPatientStatusList(params);
    }

    getAppointmentList = () => {
        const { clinicConfig } = this.props;
        const where = { serviceCd: this.props.serviceCd, clinicCd: this.props.clinicCd };
        // const backTakeDay = CommonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.BACK_TAKE_ATTENDANCE_DAY, clinicConfig, where);
        let allowBackTakeDay = AppointmentUtilities.getAllowBackTakeDay(clinicConfig, where);
        let params = {
            allService: false,
            page: 1,
            pageSize: 10,
            patientKey: this.props.patientInfo.patientKey,
            attnStatusCd: 'N',
            statusCd: 'A',
            startDate: moment().add(allowBackTakeDay, 'day').format(Enum.DATE_FORMAT_EYMD_VALUE),
            endDate: moment().add(-1, 'day').format(Enum.DATE_FORMAT_EYMD_VALUE)
        };
        // this.setState({ listApptParams: params });
        const apptDateRangeStr = `${moment(params.startDate).format(Enum.DATE_FORMAT_EDMY_VALUE)} to ${moment(params.endDate).format(Enum.DATE_FORMAT_EDMY_VALUE)}`;
        this.props.listAppointmentList(params);
        this.updateListApptParam(params);
        this.setState({ appointmentDateRangeStr: apptDateRangeStr });
    }

    getCasePatientStatus = (curAppointment) => {
        const { patientInfo } = this.props;
        let patientStatus = AppointmentUtilities.getPatientStatusCd(curAppointment, patientInfo);
        return patientStatus;
    }


    getPatientStatusFlag = (curAppointment) => {
        let patientStatusFlag = 'N';
        this.props.getEncounterType({ clinicCd: '' }, (encounterList) => {
            patientStatusFlag = AppointmentUtilities.getPatientStatusFlag(encounterList, curAppointment);
            this.props.updateField({ patientStatusFlag: patientStatusFlag });
        });
    }


    handleChange = (name, value) => {
        let updateData = { [name]: value };
        this.props.updateField(updateData);
    }

    handleDiscNumOnChange = (e) => {
        let inputProps = this.refs.discNum.props.inputProps;
        let value = e.target.value;
        let reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
        if (reg.test(value)) {
            return;
        }

        if (inputProps.maxLength && value.length > inputProps.maxLength) {
            value = value.slice(0, inputProps.maxLength);
        }

        this.handleChange(e.target.name, value);
    }


    handleSubmit = () => {
        const { patientInfo, caseNoInfo } = this.props;

        if (!caseNoInfo.caseNo) {
            this.props.openCaseNoDialog({
                caseDialogStatus: Enum.CASE_DIALOG_STATUS.CREATE,
                caseNoForm: { patientKey: patientInfo.patientKey },
                isNoPopup: true,
                caseCallBack: (data) => {
                    if (data && data.caseNo) {
                        this.props.getRegPatientById(patientInfo.patientKey);
                        this.props.getPatientPanelPatientById(patientInfo.patientKey, null, data.caseNo, () => {
                            this.backTakeAttendance();
                        });
                    }
                }
            });
        } else {
            this.backTakeAttendance();
        }
    }

    backTakeAttendance = () => {
        let patientStatusCd = this.props.patientStatus;
        let { listApptParams } = this.state;
        if (patientStatusCd === 'None') {
            patientStatusCd = '';
            this.props.updateField({ patientStatus: '' });
        }
        let params = {
            appointmentId: this.props.currentAppointment.appointmentId,
            // attnTimestamp: moment().format('x'),
            patientStatusFlag: this.props.patientStatusFlag,
            patientStatusCd: patientStatusCd,
            discNumber: this.props.discNum,
            caseNo: this.props.caseNoInfo.caseNo,
            version: this.props.currentAppointment.version
        };
        // this.props.markAttendance(params, this.getPatientQueueParams(this.props.searchParameter));
        listApptParams.page = 1;
        this.props.backTakeAttendance(params, listApptParams);
        this.updateListApptParam(listApptParams);
        this.apptListRef.updateCurrentPage(0);
    }

    handleRowSelect = (rowId, rowData, selectedData) => {
        const selected = selectedData.length === 0 ? null : selectedData[0];
        let patientStatus = this.getCasePatientStatus(selected);
        this.getPatientStatusFlag(selected);
        this.props.updateField({ currentAppointment: selected, isAttend: false, patientStatus: patientStatus });

    }

    updateListApptParam = (listApptParams) => {
        this.setState({ listApptParams });
    }


    render() {
        const { classes, currentAppointment, patientInfo, appointmentConfirmInfo, appointmentList, clinicConfig, patientStatusFlag } = this.props;
        // let { patientStatusFlag } = this.state;
        let where = { serviceCd: this.props.serviceCd, clinicCd: this.props.clinicCd };
        const patientStatusList = AttendanceUtilities.getPaitentStatusOption(this.props.patientStatusList);
        // const lateApptTime = calcLateApptTime(showLateApptConfig, lateApptTimeConfig, currentAppointment);

        let bookedApptType = {
            caseType: '',
            bookedQuotaType: ''
        };

        if (currentAppointment) {
            bookedApptType = AppointmentUtilities.getBookedApptType(currentAppointment.caseTypeCd, currentAppointment.appointmentTypeCd, clinicConfig, where);
        }
        return (
            <Grid container spacing={1} className={classes.root}>
                <Grid item container xs={6}>
                    <AppointmentList
                        id={'back_take_attendance_appointment_list'}
                        innerRef={ref => this.apptListRef = ref}
                        isAttend={this.props.isAttend}
                        // data={currentDayAppointment}
                        patientInfo={patientInfo}
                        appointmentList={appointmentList}
                        rowSelect={this.handleRowSelect}
                        selectIdName={'appointmentId'}
                        listApptParams={this.state.listApptParams}
                        listAppointment={(params) => this.props.listAppointmentList(params)}
                        updateListApptParam={this.updateListApptParam}
                        appointmentDateRangeStr={this.state.appointmentDateRangeStr}
                    />
                </Grid>
                <Grid item container xs={6} style={{ margin: 0, padding: 10, display: 'flex' }} direction={'column'}>
                    <ValidatorForm onSubmit={this.handleSubmit} className={classes.form}>
                        <Grid item container xs={12} style={{ position: 'relative', backgroundColor: '#fff' }} alignItems="flex-end" justify="space-between">
                            {!this.props.isAttend ?
                                <CIMSButton id={'back_take_attendance_btn_ocsss'}>OCSSS</CIMSButton>
                                : null
                            }
                            {!this.props.isAttend ?
                                <CIMSButton
                                    id={'back_take_attendance_btn_attend'}
                                    type={'submit'}
                                    //className={classes.alignRightBtn}
                                    disabled={!currentAppointment}
                                >Attend</CIMSButton>
                                : null
                            }
                        </Grid>
                        {
                            this.props.isAttend && currentAppointment === null ?
                                <ConfirmationWindow
                                    id={'back_take_attendance_confirmation'}
                                    type={Enum.APPOINTMENT_TYPE.BACK_TAKE_ATTENDANCE}
                                    patientInfo={patientInfo}
                                    confirmationInfo={appointmentConfirmInfo}
                                />
                                :
                                <Grid item container xs={12}>
                                    {/* <MarkLayer open={this.props.isAttend} id={'markLayer'} /> */}
                                    <Grid item container xs={12} direction={'column'} className={classes.apptInfoRow}>
                                        {/* <Typography variant={'subtitle1'}>{CommonUtilities.getPatientCall()} Status</Typography> */}
                                        <Grid item xs={4} >
                                            {/* <Typography component={'div'} style={{ width: '30%' }}> */}

                                            <SelectFieldValidator
                                                id={'back_take_attendance_patient_status'}
                                                options={patientStatusList &&
                                                    patientStatusList.map((item) => (
                                                        { value: item.code, label: item.code }
                                                    ))}
                                                TextFieldProps={{
                                                    variant: 'outlined',
                                                    label: 'Status'
                                                }}
                                                // placeholder={'None'}
                                                name={'patientStatus'}
                                                isDisabled={patientStatusFlag !== 'Y'}
                                                onChange={(e) => this.handleChange('patientStatus', e.value)}
                                                value={patientStatusFlag === 'Y' ? this.props.patientStatus : ''}
                                            />
                                            {/* </Typography> */}
                                        </Grid>
                                    </Grid>
                                    <Grid item container alignItems={'flex-end'}>
                                        <Grid item container spacing={1} justify={'space-around'} className={classes.apptInfoRow}>
                                            <Grid item xs={4}>
                                                {/* <Typography variant={'subtitle1'}>Encounter</Typography> */}
                                                <TextFieldValidator
                                                    fullWidth
                                                    disabled
                                                    id={'back_take_attendance_encounter'}
                                                    value={currentAppointment && currentAppointment.encounterTypeCd}
                                                    variant={'outlined'}
                                                    label={'Encounter'}
                                                />
                                            </Grid>
                                            <Grid item xs={4}>
                                                {/* <Typography variant={'subtitle1'}>Sub-encounter</Typography> */}
                                                <TextFieldValidator
                                                    fullWidth
                                                    disabled
                                                    id={'back_take_attendance_sub_encounter'}
                                                    value={currentAppointment && currentAppointment.subEncounterTypeCd}
                                                    variant={'outlined'}
                                                    label={'Sub-encounter'}
                                                />
                                            </Grid>
                                            <Grid item container xs={4}>
                                                {/* <Typography style={{ width: '100%' }} variant={'subtitle1'}>Appointment Type</Typography> */}
                                                <Grid item xs={6} style={{ width: '80%', paddingRight: 10 }} >
                                                    <TextFieldValidator
                                                        fullWidth
                                                        disabled
                                                        // id={'new'}
                                                        id={'back_take_attendance_case_type'}
                                                        // value={currentAppointment && currentAppointment.caseTypeCd}
                                                        value={currentAppointment && bookedApptType.caseType}
                                                        variant={'outlined'}
                                                        label={'Case Type'}
                                                    />
                                                </Grid>
                                                <Grid item xs={6} style={{ width: '80%' }}>
                                                    <TextFieldValidator
                                                        fullWidth
                                                        disabled
                                                        // id={'normal'}
                                                        id={'back_take_attendance_appointment_type'}
                                                        // value={currentAppointment && currentAppointment.appointmentTypeCd}
                                                        value={currentAppointment && bookedApptType.bookedQuotaType}
                                                        variant={'outlined'}
                                                        label={'Appointment Type'}
                                                    />
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        <Grid item container spacing={1} justify={'space-around'} className={classes.apptInfoRow}>
                                            <Grid item xs={4} >
                                                {/* <Typography variant={'subtitle1'}>Date/Time</Typography> */}
                                                <TextFieldValidator
                                                    fullWidth
                                                    disabled
                                                    id={'back_take_attendance_date'}
                                                    value={currentAppointment && currentAppointment.appointmentDate}
                                                    variant={'outlined'}
                                                    label={'Appointmnet Date'}
                                                />
                                            </Grid>
                                            <Grid item container xs={4} alignItems={'flex-end'}>
                                                <TextFieldValidator
                                                    fullWidth
                                                    disabled
                                                    id={'back_take_attendance_time'}
                                                    // value={currentAppointment && currentAppointment.appointmentTime}
                                                    value={currentAppointment && moment(currentAppointment.appointmentTime, Enum.TIME_FORMAT_24_HOUR_CLOCK).format(Enum.TIME_FORMAT_24_HOUR_CLOCK)}
                                                    variant={'outlined'}
                                                    label={'Appointment Time'}
                                                />
                                            </Grid>
                                            <Grid item xs={4} >
                                                {/* <Typography variant={'subtitle1'}>Disc Number</Typography> */}
                                                <TextFieldValidator
                                                    fullWidth
                                                    variant={'outlined'}
                                                    id={'back_take_attendance_discNum'}
                                                    name={'discNum'}
                                                    ref={'discNum'}
                                                    //onChange={(e) => this.handleChange('discNum', e.target.value)}
                                                    onChange={(e) => this.handleDiscNumOnChange(e)}
                                                    value={this.props.discNum}
                                                    inputProps={{
                                                        maxLength: 10
                                                    }}
                                                    label={'Disc Number'}
                                                />
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    {/* <Typography variant={'subtitle1'}>Remark</Typography> */}
                                    <Grid item container xs={12} className={classes.apptInfoRow}>
                                        <TextFieldValidator
                                            disabled
                                            fullWidth
                                            id={'back_take_attendance_attendance_remark'}
                                            value={currentAppointment && currentAppointment.remark}
                                            variant={'outlined'}
                                            label={'Remark'}
                                        />
                                    </Grid>
                                    <Grid item container xs={12} className={classes.apptInfoRow}>
                                        {/* <TextFieldValidator
                                            disabled
                                            fullWidth
                                            id={'back_take_attendance_attendance_memo'}
                                            value={currentAppointment && currentAppointment.memo}
                                            variant={'outlined'}
                                            label={'Memo'}
                                        /> */}
                                        <CIMSMultiTextField
                                            disabled
                                            fullWidth
                                            id={'back_take_attendance_attendance_memo'}
                                            value={currentAppointment && currentAppointment.memo}
                                            variant={'outlined'}
                                            label={'Memo'}
                                            rows={'4'}
                                            calActualLength
                                        />
                                    </Grid>
                                </Grid>
                        }
                    </ValidatorForm>
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        accessRights: state.login.accessRights,
        serviceCd: state.login.service.serviceCd,
        patientStatusList: state.backTakeAttendance.patientStatusList,
        patientStatus: state.backTakeAttendance.patientStatus,
        patientStatusFlag: state.backTakeAttendance.patientStatusFlag,
        discNum: state.backTakeAttendance.discNum,
        isAttend: state.backTakeAttendance.isAttend,
        patientInfo: state.patient.patientInfo,
        appointmentInfo: state.patient.appointmentInfo,
        caseNoInfo: state.patient.caseNoInfo,
        clinicCd: state.login.clinic.clinicCd,
        clinicConfig: state.common.clinicConfig,
        appointmentList: state.backTakeAttendance.appointmentList,
        currentAppointment: state.backTakeAttendance.currentAppointment,
        appointmentConfirmInfo: state.backTakeAttendance.appointmentConfirmInfo
    };
};

const mapDispatchToProps = {
    getPatientStatusList,
    updateField,
    openCommonMessage,
    openCaseNoDialog,
    getRegPatientById,
    getPatientPanelPatientById,
    selectCaseTrigger,
    listAppointmentList,
    backTakeAttendance,
    resetAll,
    getEncounterType
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(BackTakeAttendance));