import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
    Grid,
    Box
} from '@material-ui/core';
import Enum from '../../../../enums/enum';
import moment from 'moment';
import SelectFieldValidator from '../../../../components/FormValidator/SelectFieldValidator';
import TextFieldValidator from '../../../../components/FormValidator/TextFieldValidator';
import * as AppointmentUtilities from '../../../../utilities/appointmentUtilities';
import * as AttendanceUtilities from '../../../../utilities/attendanceUtilities';
import CommonRegex from '../../../../constants/commonRegex';

import * as CommonUtilities from '../../../../utilities/commonUtilities';

import CIMSButton from 'components/Buttons/CIMSButton';

import {
    listRemarkCode
} from '../../../../store/actions/appointment/booking/bookingAnonymousAction';
import CIMSMultiTextField from '../../../../components/TextField/CIMSMultiTextField';
import * as EcsUtilities from '../../../../utilities/ecsUtilities';
import EcsResultTextField from 'components/ECS/Ecs/EcsResultTextField';
import OcsssResultTextField from 'components/ECS/Ocsss/OcsssResultTextField';

import MwecsResultTextField from 'components/ECS/Mwecs/MwecsResultTextField';
import MwecsMessageIdTextField from 'components/ECS/Mwecs/MwecsMessageIdTextField';
import {openMwecsDialog,openEcsDialog,openOcsssDialog, setMwecsPatientStatus,checkOcsss, setOcsssPatientStatus, setEcsPatientStatus} from '../../../../store/actions/ECS/ecsAction';
import {ecsSelector, ocsssSelector, mwecsSelector, patientKeySelector, patientInfoSelector} from '../../../../store/selectors/ecsSelectors';
import { openCommonMessage } from '../../../../store/actions/message/messageAction';


const sysRatio = CommonUtilities.getSystemRatio();
const unit = CommonUtilities.getResizeUnit(sysRatio);

const styles = () => ({
    apptInfoRow: {
        padding: `${18 * unit}px ${8 * unit}px`
    }
});

class AttendanceDetail extends React.Component {

    componentDidMount() {
        this.props.listRemarkCode();
    }

    handleDiscNumOnChange = (e) => {
        let inputProps = this.refs.discNum.props.inputProps;
        let value = e.target.value;
        let reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
        if (reg.test(value)) {
            return;
        }

        if (inputProps.maxLength && value.length > inputProps.maxLength) {
            value = value.slice(0, inputProps.maxLength);
        }

        this.props.handleChange(e.target.name, value);
    }

    render() {
        const { classes, currentAppointment, clinicConfig, patientStatusFlag, ecs, ocsss, mwecs,  openOcsssDialog, openMwecsDialog, openEcsDialog, checkOcsss } = this.props;
        let where = { serviceCd: this.props.serviceCd, clinicCd: this.props.clinicCd };
        const patientStatusList = AttendanceUtilities.getPaitentStatusOption(this.props.patientStatusList);
        let bookedApptType = {
            caseType: '',
            bookedQuotaType: ''
        };

        if (currentAppointment) {
            bookedApptType = AppointmentUtilities.getBookedApptType(
                currentAppointment.caseTypeCd,
                currentAppointment.appointmentTypeCd,
                clinicConfig,
                where
            );
        }

        const id = this.props.id || 'attendance_attendanceDetail';
        return (
            <Grid item container xs={12}>
                <Grid item container xs={12} direction={'column'} className={classes.apptInfoRow}>
                    <Grid item xs={4} >
                        <SelectFieldValidator
                            id={id + '_patientStatus'}
                            options={patientStatusList &&
                                patientStatusList.map((item) => (
                                    { value: item.code, label: item.code }
                                ))}
                            TextFieldProps={{
                                variant: 'outlined',
                                label: 'Status'
                            }}
                            name={'patientStatus'}
                            isDisabled={patientStatusFlag !== 'Y'}
                            onChange={(e) => this.props.handleChange('patientStatus', e.value)}
                            value={patientStatusFlag === 'Y' ? this.props.patientStatus : ''}
                        />
                    </Grid>
                </Grid>
                <Grid item container alignItems={'flex-end'}>
                    <Grid item container spacing={1} justify={'space-around'} className={classes.apptInfoRow}>
                        <Grid item xs={4}>
                            <TextFieldValidator
                                fullWidth
                                disabled
                                id={id + 'encounterType'}
                                value={currentAppointment && currentAppointment.encounterTypeCd}
                                variant={'outlined'}
                                label={'Encounter'}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextFieldValidator
                                fullWidth
                                disabled
                                id={id + 'subEncoutnerType'}
                                value={currentAppointment && currentAppointment.subEncounterTypeCd}
                                variant={'outlined'}
                                label={'Sub-encounter'}
                            />
                        </Grid>
                        <Grid item container xs={4}>
                            <Grid item xs={6} style={{ width: '80%', paddingRight: 10 }} >
                                <TextFieldValidator
                                    fullWidth
                                    disabled
                                    id={id + 'caseType'}
                                    value={currentAppointment && bookedApptType.caseType}
                                    variant={'outlined'}
                                    label={'Case Type'}
                                />
                            </Grid>
                            <Grid item xs={6} style={{ width: '80%' }}>
                                <TextFieldValidator
                                    fullWidth
                                    disabled
                                    id={id + 'appointmentTypeCd'}
                                    value={currentAppointment && bookedApptType.bookedQuotaType}
                                    variant={'outlined'}
                                    label={'Appointment Type'}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item container spacing={1} justify={'space-around'} className={classes.apptInfoRow}>
                        <Grid item xs={4} >
                            <TextFieldValidator
                                fullWidth
                                disabled
                                id={id + 'date'}
                                value={currentAppointment && currentAppointment.appointmentDate}
                                variant={'outlined'}
                                label={'Appointmnet Date'}
                            />
                        </Grid>
                        <Grid item container xs={4} alignItems={'flex-end'}>
                            <TextFieldValidator
                                fullWidth
                                disabled
                                id={id + 'time'}
                                value={currentAppointment && moment(currentAppointment.appointmentTime, Enum.TIME_FORMAT_24_HOUR_CLOCK).format(Enum.TIME_FORMAT_24_HOUR_CLOCK)}
                                variant={'outlined'}
                                label={'Appointment Time'}
                            />
                        </Grid>
                        <Grid item xs={4} >
                            <TextFieldValidator
                                fullWidth
                                variant={'outlined'}
                                id={id + 'discNum'}
                                name={'discNum'}
                                ref={'discNum'}
                                onChange={(e) => this.handleDiscNumOnChange(e)}
                                value={this.props.discNum}
                                inputProps={{
                                    maxLength: 10
                                }}
                                label={'Disc Number'}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item container xs={12} className={classes.apptInfoRow}>
                    <TextFieldValidator
                        disabled
                        fullWidth
                        id={id + 'attendance_remark'}
                        value={currentAppointment && currentAppointment.remark}
                        variant={'outlined'}
                        label={'Remark'}
                    />
                </Grid>
                <Grid item container xs={12} className={classes.apptInfoRow}>
                    {/* <TextFieldValidator
                        disabled
                        fullWidth
                        id={id + 'attendance_memo'}
                        value={currentAppointment && currentAppointment.memo}
                        variant={'outlined'}
                        label={'Memo'}
                    /> */}
                    <CIMSMultiTextField
                        disabled
                        id={id + 'attendance_memo'}
                        fullWidth
                        value={currentAppointment && currentAppointment.memo}
                        inputProps={{
                            maxLength: 500
                        }}
                        variant={'outlined'}
                        label={'Memo'}
                        calActualLength
                        rows="4"
                    />
                </Grid>
                <Grid item container xs={12} >
                <Box display="flex" width={1} pb={1}>
                        <Box display="flex" width={1}>
                            <Box pr={1}>
                               <CIMSButton
                                   disabled={!EcsUtilities.isMwecsEnable(
                                        mwecs.accessRights,
                                        mwecs.serviceStatus)}
                                   style={{ padding: '0px', margin: '0px' }}
                                   onClick={(e) => {
                                        openMwecsDialog({
                                            idType: mwecs.idType,
                                            idNum: mwecs.idNum,
                                            patientKey: mwecs.patientKey,
                                            appointmentId: mwecs.appointmentId
                                        }, null, setMwecsPatientStatus);
                                    }}
                               >MWECS</CIMSButton>
                            </Box>
                            <Box pr={1} flexGrow={1}>
                                <MwecsResultTextField  mwecsStore={mwecs.selectedPatientMwecsStatus} fullWidth ></MwecsResultTextField>
                            </Box>
                            <Box  flexGrow={1}>
                                <MwecsMessageIdTextField  mwecsStore={mwecs.selectedPatientMwecsStatus} fullWidth ></MwecsMessageIdTextField>
                            </Box>
                        </Box>

                </Box>
                    <Box display="flex" width={1} pb={1} >
                        <Box display="flex" width={1}>
                            <Box pr={1}>
                            <CIMSButton
                                disabled={!EcsUtilities.isEcsEnable(
                                ecs.accessRights,
                                ecs.docTypeCds,
                                ecs.ecsUserId,
                                ecs.ecsLocCode,
                                false,
                                ecs.ecsServiceStatus ,
                                ecs.hkicForEcs)}
                                style={{ padding: '0px', margin: '0px' }}
                                onClick={(e) => {
                                    openEcsDialog({
                                        docTypeCd:ecs.docTypeCd,
                                        disableMajorKeys: true,
                                        engSurname: ecs.engSurname,
                                        engGivename: ecs.engGivename,
                                        chineseName: ecs.chineseName,
                                        cimsUser: ecs.ecsUserId,
                                        locationCode: ecs.ecsLocCode,
                                        patientKey: ecs.patientKey,
                                        hkid: ecs.hkicForEcs,
                                        dob: ecs.dob,
                                        exactDob:ecs.exactDobCd,
                                        appointmentId: ecs.appointmentId
                                    },
                                    null,
                                    setEcsPatientStatus);
                                }}
                            >ECS</CIMSButton>
                            </Box>
                            <Box  flexGrow={1}  pr={1}>
                                <EcsResultTextField  ecsStore={ecs.selectedPatientEcsStatus} fullWidth ></EcsResultTextField>

                            </Box>
                        </Box>
                        <Box display="flex" width={1}>
                        <Box pr={1}>
                        <CIMSButton
                            style={{ padding: '0px', margin: '0px' }}
                            disabled={!EcsUtilities.isOcsssEnable(
                                ocsss.accessRights,
                                ocsss.docTypeCds,
                                ocsss.ocsssServiceStatus,
                                ocsss.hkicForEcs)
                            }
                            onClick={(e) => {
                                openOcsssDialog(
                                    {
                                        hkid: ocsss.hkicForEcs,
                                        patientKey: ocsss.patientKey,
                                        appointmentId: ocsss.appointmentId
                                    },
                                    null, setOcsssPatientStatus, checkOcsss);
                            }}
                        >OCSSS</CIMSButton>

                        </Box>
                        <Box  flexGrow={1}>
                            <OcsssResultTextField  ocsssStore={ocsss.selectedPatientOcsssStatus} fullWidth ></OcsssResultTextField>
                        </Box>
                    </Box>
                </Box>


            </Grid>
            </Grid>
        );
    }
}


const mapStatetoProps = (state) => {
    return ({
        remarkCodeList: state.bookingInformation.remarkCodeList,

        ecs: ecsSelector(state, patientInfoSelector ,patientKeySelector),
        ocsss: ocsssSelector(state, patientInfoSelector ,patientKeySelector),
        mwecs: mwecsSelector(state, patientInfoSelector ,patientKeySelector)
    });
};
const mapDispatchtoProps = {
    listRemarkCode,
    openMwecsDialog,
    openEcsDialog,
    openOcsssDialog,
    openCommonMessage,
    checkOcsss
};


export default connect(mapStatetoProps, mapDispatchtoProps)(withStyles(styles)(AttendanceDetail));