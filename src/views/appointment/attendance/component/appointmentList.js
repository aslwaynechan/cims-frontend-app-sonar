import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import {
    Grid,
    Paper,
    Typography
} from '@material-ui/core';
import CIMSTable from '../../../../components/Table/CIMSTable';
import CIMSButton from '../../../../components/Buttons/CIMSButton';
import moment from 'moment';
import Enum from '../../../../enums/enum';
import * as AppointmentUntil from '../../../../utilities/appointmentUtilities';
import * as CommonUtilities from '../../../../utilities/commonUtilities';

const sysRatio = CommonUtilities.getSystemRatio();
const unit = CommonUtilities.getResizeUnit(sysRatio);

const styles = theme => ({
    paperRoot: {
        backgroundColor: theme.palette.dialogBackground,
        padding: 4,
        overflowX: 'auto',
        overflowY: 'hidden'
    },
    tableTitle: {
        fontWeight: 500,
        color: theme.palette.white
    },
    tableGrid: {
        backgroundColor: '#fff'
    },
    customTableHeadRow: {
        fontWeight: 400,
        height: 40 * unit
    },
    backdropRoot: {
        zIndex: 1200,
        position: 'absolute',
        top: 0
    },
    buttonRoot: {
        margin: 2,
        padding: 0,
        height: 35 * unit
    },
    tableContainer: {
        padding: 2
    },
    gridTitle: {
        padding: '4px 0px'
    }
});

class AppointmentLit extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tableRows: AppointmentUntil.getAppointmentListTableHeader(Enum.APPOINTMENT_LIST_TYPE.APPOINTMENT_LIST),
            tableOptions: {
                rowExpand: true,
                rowsPerPage: 10,
                rowsPerPageOptions: [5, 10, 15],
                headRowStyle: this.props.classes.customTableHeadRow,
                headCellStyle: this.props.classes.customTableHeadCell,
                onSelectIdName: this.props.selectIdName || '',
                onSelectedRow: (rowId, rowData, selectedData) => {
                    this.props.rowSelect(rowId, rowData, selectedData);
                }
            }
        };
    }

    handleOnChangePage = (event, newPage, rowPerPage) => {
        let listApptParams = this.props.listApptParams;
        listApptParams.page = newPage + 1;
        this.props.updateListApptParam(listApptParams);
        this.props.listAppointment(listApptParams);
    }

    handleOnChangeRowPerPage = (event, page, rowPerPage) => {
        let listApptParams = this.props.listApptParams;

        listApptParams.page = 1;
        listApptParams.pageSize = rowPerPage;
        this.updateCurrentPage(0);
        this.props.updateListApptParam(listApptParams);
        this.props.listAppointment(listApptParams);
    }

    updateCurrentPage = (newPage) => {
        this.tableRef.updatePage(newPage);
    }

    setSelectedAppt = (appointment) => {
        this.tableRef.setSelected(appointment);
    }

    render() {
        const { classes, appointmentList, patientInfo, appointmentDateRangeStr } = this.props;
        return (
            <Paper className={classes.paperRoot}>
                <Grid
                    container
                    item
                    alignItems="center"
                    wrap="nowrap"
                    className={classes.gridTitle}
                >
                    <Grid container>
                        {/* <Typography className={classes.tableTitle}>Appointment List - {moment().format(Enum.DATE_FORMAT_EDMY_VALUE)}</Typography> */}
                        <Typography className={classes.tableTitle}>Appointment List - {appointmentDateRangeStr ? appointmentDateRangeStr : moment().format(Enum.DATE_FORMAT_EDMY_VALUE)}</Typography>
                    </Grid>
                    <Grid container justify={'flex-end'}>
                        <CIMSButton
                            id="attendance_appointmentList_printAllBtn"
                            classes={{ sizeSmall: classes.buttonRoot }}
                            disabled={this.props.isAttend}
                        >Print All</CIMSButton>
                    </Grid>

                </Grid>
                <Grid container className={classes.tableGrid} >
                    {/* <MarkLayer open={this.props.isAttend} /> */}
                    <Grid item container className={classes.tableContainer}>
                        <CIMSTable
                            innerRef={ref => this.tableRef = ref}
                            id={'tb_appointmentList'}
                            rows={this.state.tableRows}
                            //data={this.props.data}
                            data={patientInfo && patientInfo.deadInd === '0' && appointmentList ? appointmentList.appointmentDtos : null}
                            options={this.state.tableOptions}
                            remote
                            totalCount={appointmentList ? appointmentList.totalNum : 0}
                            // onChangePage={(...args) => this.handleOnChangePage(...args)}
                            // onChangeRowsPerPage={(...args) => this.handleOnChangeRowPerPage(...args)}
                            onChangePage={this.handleOnChangePage}
                            onChangeRowsPerPage={this.handleOnChangeRowPerPage}
                        />
                    </Grid>
                </Grid>
            </Paper>
        );
    }
}
const mapStateToProps = () => ({

});
const dispatchToProps = {

};
export default connect(mapStateToProps, dispatchToProps)(withStyles(styles)(AppointmentLit));