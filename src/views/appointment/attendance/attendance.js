import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
    Typography,
    Grid,
    Box
} from '@material-ui/core';
import AppointmentList from './component/appointmentList';
import CIMSButton from '../../../components/Buttons/CIMSButton';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import Enum from '../../../enums/enum';
import { codeList } from '../../../constants/codeList';
import {
    getPatientStatusList,
    updateField,
    markAttendance,
    destroyMarkAttendance,
    getAppointmentForAttend,
    listAppointmentList
} from '../../../store/actions/appointment/attendance/attendanceAction';
import * as attendanceActionTypes from '../../../store/actions/appointment/attendance/attendanceActionType';
import { openCommonMessage } from '../../../store/actions/message/messageAction';
import * as CommonUtilities from '../../../utilities/commonUtilities';
import moment from 'moment';
import { deleteSubTabs } from '../../../store/actions/mainFrame/mainFrameAction';
import { openCaseNoDialog } from '../../../store/actions/caseNo/caseNoAction';
import accessRightEnum from '../../../enums/accessRightEnum';
import EditModeMiddleware from '../../compontent/editModeMiddleware';
import CommonRegex from '../../../constants/commonRegex';
import {
    getPatientById as getPatientPanelPatientById,
    getPatientAppointment,
    getPatientEncounter
} from '../../../store/actions/patient/patientAction';
import { getPatientById as getRegPatientById } from '../../../store/actions/registration/registrationAction';
import { selectCaseTrigger } from '../../../store/actions/caseNo/caseNoAction';
import * as PatientUtil from '../../../utilities/patientUtilities';

// import AttendSuccessPanel from './component/attendSuccessPanel';
import ConfirmationWindow from '../../compontent/confirmationWindow';
import AttendanceDetail from './component/attendanceDetail';
import * as AppointmentUtilities from '../../../utilities/appointmentUtilities';
import {
    getEncounterType
} from '../../../store/actions/common/commonAction';
import {
    getCodeList
} from '../../../store/actions/patient/patientAction';

import {refreshServiceStatus} from '../../../store/actions/ECS/ecsAction';
import * as ecsActionType from '../../../store/actions/ECS/ecsActionType';

const styles = (theme) => ({
    root: {
        padding: 4
    },
    form: {
        width: '100%',
        margin: 0,
        padding: 0,
        flex: 1
        //height: '90%'
    },
    attendanceInfoItem: {
        width: '100%',
        fontSize: '2.5em',
        color: theme.palette.white,
        textAlign: 'center',
        padding: '0px 8px'
    },
    lateApptPanel: {
        // position: 'fixed',
        padding: '5px 10px',
        maxHeight: '10%',
        backgroundColor: theme.palette.primary.main,
        borderRadius: 6,
        flex: 1
    }
});

function calcLateApptTime(showLateApptConfig, lateApptTimeConfig, apptInfo) {
    if (showLateApptConfig.configValue !== 'Y' || !apptInfo) {
        return;
    }
    let allowLateApptTime = null;
    let lateTimeUnit = lateApptTimeConfig.configValue.substr(-1);
    let lateTime = lateApptTimeConfig.configValue.substr(0, lateApptTimeConfig.configValue.length - 1);
    let timeUnit = '';

    switch (lateTimeUnit) {
        case 'h': {
            timeUnit = 'hour';
            break;
        }
        case 'm': {
            timeUnit = 'minute';
            break;
        }
        default: {
            timeUnit = 'hour';
            break;
        }
    }
    allowLateApptTime = moment(apptInfo.appointmentTime, Enum.TIME_FORMAT_24_HOUR_CLOCK).add(lateTime, timeUnit).format(Enum.TIME_FORMAT_24_HOUR_CLOCK);
    let currentTime = moment().format(Enum.TIME_FORMAT_24_HOUR_CLOCK);
    if (moment().isBefore(moment(allowLateApptTime, Enum.TIME_FORMAT_24_HOUR_CLOCK))) {
        return;
    }

    return {
        hours: moment(currentTime, Enum.TIME_FORMAT_24_HOUR_CLOCK).diff(moment(allowLateApptTime, Enum.TIME_FORMAT_24_HOUR_CLOCK), 'hours'),
        minutes: moment(currentTime, Enum.TIME_FORMAT_24_HOUR_CLOCK).diff(moment(allowLateApptTime, Enum.TIME_FORMAT_24_HOUR_CLOCK), 'minutes')
    };
}

class Attendance extends React.Component {
    state = {
        listApptParams: null,
        bookedApptType: {
            caseType: ''
        }
    }
    UNSAFE_componentWillMount() {
        let params = [
            codeList.patient_status
        ];
        this.props.getPatientStatusList(params);
    }

    componentDidMount() {
        let getCodeListParams = [
            codeList.exact_date_of_birth
        ];
        this.props.getCodeList(getCodeListParams, ecsActionType.PUT_GET_CODE_LIST );
        this.props.ensureDidMount();
        this.props.refreshServiceStatus();

        this.listAppointmentList();
    }
    componentDidUpdate() {
        if (this.props.openPromptDialog === true) {
            const msgCd = this.props.appointmentInfo && this.props.appointmentInfo.status === Enum.ATTENDANCE_STATUS.ATTENDED ? '110228' : '110229';
            this.props.openCommonMessage({
                msgCode: msgCd,
                // params: [{ name: 'PATIENT_CALL', value: CommonUtilities.getPatientCall() }],
                btnActions: {
                    btn1Click: this.handleNoAppointmentConfirm
                }
            });
        }
    }
    componentWillUnmount() {
        this.props.destroyMarkAttendance();
    }

    listAppointmentList = () => {
        const { appointmentInfo } = this.props;
        let params = {
            allService: false,
            page: 1,
            pageSize: 10,
            patientKey: this.props.patientInfo.patientKey,
            attnStatusCd: 'N',
            statusCd: 'A',
            startDate: moment().format(Enum.DATE_FORMAT_EYMD_VALUE),
            endDate: moment().format(Enum.DATE_FORMAT_EYMD_VALUE)
        };
        this.props.listAppointmentList(params, (appointmentListData) => {

            // let curAppointment = AppointmentUtilities.getCurentAppointment(appointmentInfo.appointmentId, appointmentListData.appointmentDtos);
            const appointmentList = appointmentListData.appointmentDtos;
            const apptTotalNum = appointmentListData.totalNum;

            let curAppointment = appointmentList.find(item => item.appointmentId === appointmentInfo.appointmentId);
            let patientStatus = '';
            let openPromptDialog = false;

            if (!curAppointment) {
                if (apptTotalNum > 0) {
                    curAppointment = appointmentList[0];
                }
                else {
                    openPromptDialog = true;
                    curAppointment = null;
                }
            }

            patientStatus = this.getCasePatientStatus(curAppointment);
            if (apptTotalNum > 0) {
                this.apptListRef.setSelectedAppt(curAppointment.appointmentId);
            }

            this.getPatientStatusFlag(curAppointment);
            this.props.updateField({ currentAppointment: curAppointment, patientStatus: patientStatus, openPromptDialog: openPromptDialog });

        });
        this.setState({ listApptParams: params });
    }

    getCasePatientStatus = (curAppointment) => {
        const { patientInfo } = this.props;
        let patientStatus = AppointmentUtilities.getPatientStatusCd(curAppointment, patientInfo);
        return patientStatus;
    }

    getPatientStatusFlag = (curAppointment) => {
        let patientStatusFlag = 'N';
        this.props.getEncounterType({ clinicCd: '' }, (encounterList) => {
            patientStatusFlag = AppointmentUtilities.getPatientStatusFlag(encounterList, curAppointment);
            this.props.updateField({ patientStatusFlag: patientStatusFlag });
        });
    }

    handleChange = (name, value) => {
        let updateData = { [name]: value };
        this.props.updateField(updateData);
    }


    handleSubmit = () => {
        const { patientInfo, caseNoInfo } = this.props;
        if (!caseNoInfo.caseNo) {
            this.props.openCaseNoDialog({
                caseDialogStatus: Enum.CASE_DIALOG_STATUS.CREATE,
                caseNoForm: { patientKey: patientInfo.patientKey },
                isNoPopup: true,
                caseCallBack: (data) => {
                    if (data && data.caseNo) {
                        this.props.getRegPatientById(patientInfo.patientKey);
                        this.props.getPatientPanelPatientById(patientInfo.patientKey, null, data.caseNo, () => {
                            this.markAttendance();
                        });
                        // this.markAttendance();
                    }
                }
            });
        } else {
            this.markAttendance();
        }
    }

    markAttendance = () => {
        let patientStatusCd = this.props.patientStatus;
        let { listApptParams } = this.state;
        const { currentAppointment, discNum, patientInfo } = this.props;
        console.log(currentAppointment);
        if (patientStatusCd === 'None') {
            patientStatusCd = '';
            this.props.updateField({ patientStatus: '' });
        }
        let params = {
            appointmentId: currentAppointment.appointmentId,
            // attnTimestamp: moment().format('x'),
            patientStatusFlag: currentAppointment.patientStatusFlag,
            patientStatusCd: patientStatusCd,
            discNumber: discNum,
            caseNo: currentAppointment.caseNo,
            version: currentAppointment.version
        };
        listApptParams.page = 1;
        this.props.markAttendance(params, listApptParams, () => {
            if (currentAppointment.appointmentId) {
                this.props.getPatientAppointment(currentAppointment.appointmentId, patientInfo && patientInfo.caseList);
                this.props.getPatientEncounter(currentAppointment.appointmentId);
            }
        });
        this.updateListApptParam(listApptParams);
        this.apptListRef.updateCurrentPage(0);
    }

    nextPatient = () => {
        this.props.hidden();
    }

    isReadyToAttend = () => {
        let { appointmentInfo } = this.props;
        if (!appointmentInfo || !appointmentInfo.appointmentId || appointmentInfo.status === Enum.ATTENDANCE_STATUS.ATTENDED) {
            this.props.updateField({ openPromptDialog: true });
            return;
        }
        this.props.getAppointmentForAttend(appointmentInfo.appointmentId);
    }

    handleNoAppointmentConfirm = () => {
        this.props.deleteSubTabs(accessRightEnum.attendance);
        this.props.updateField({ openPromptDialog: false });
    }

    handleDiscNumOnChange = (e) => {
        let inputProps = this.refs.discNum.props.inputProps;
        let value = e.target.value;
        let reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
        if (reg.test(value)) {
            return;
        }

        if (inputProps.maxLength && value.length > inputProps.maxLength) {
            value = value.slice(0, inputProps.maxLength);
        }

        this.handleChange(e.target.name, value);
    }

    handleRowSelect = (rowId, rowData, selectedData) => {
        const selected = selectedData.length === 0 ? null : selectedData[0];
        let patientStatus = this.getCasePatientStatus(selected);
        this.getPatientStatusFlag(selected);
        this.props.updateField({ currentAppointment: selected, isAttend: false, patientStatus: patientStatus });
    }

    updateListApptParam = (listApptParams) => {
        this.setState({ listApptParams });
    }

    render() {
        const { classes, currentAppointment, patientInfo, appointmentConfirmInfo, appointmentList, clinicConfig, patientStatusList, patientStatus, patientStatusFlag, ecs, checkEcs, ocsss, checkOcsss, openCommonMessage } = this.props;
        let where = { serviceCd: this.props.serviceCd, clinicCd: this.props.clinicCd };
        const showLateApptConfig = CommonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.LATE_ATTENDANCE_FLAG, clinicConfig, where);
        const lateApptTimeConfig = CommonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.LATE_ATTENDANCE_TIME, clinicConfig, where);
        const lateApptTime = calcLateApptTime(showLateApptConfig, lateApptTimeConfig, currentAppointment);

        let isSameDayAppt = currentAppointment && (moment(currentAppointment.appointmentDate).isSame(moment().format(Enum.DATE_FORMAT_EDMY_VALUE)));
        return (
            <Grid container spacing={1} className={classes.root}>
                <Grid item container xs={6}>
                    <AppointmentList
                        id={'tbAppointmentList'}
                        innerRef={ref => this.apptListRef = ref}
                        isAttend={this.props.isAttend}
                        // data={currentDayAppointment}
                        patientInfo={patientInfo}
                        appointmentList={appointmentList}
                        rowSelect={this.handleRowSelect}
                        selectIdName="appointmentId"
                        listApptParams={this.state.listApptParams}
                        listAppointment={(params) => this.props.listAppointmentList(params)}
                        updateListApptParam={this.updateListApptParam}
                    />
                </Grid>
                <Grid item container xs={6} style={{ margin: 0, padding: 10, display: 'flex' }} direction={'column'}>
                    <ValidatorForm onSubmit={this.handleSubmit} className={classes.form}>
                        <Grid item container xs={12} style={{ position: 'relative', backgroundColor: '#fff' }} alignItems="flex-end" justify="flex-end">

                            {!this.props.isAttend ?
                                <CIMSButton
                                    id={'btnAttend'}
                                    type={'submit'}
                                    disabled={currentAppointment && currentAppointment.appointmentDate !== moment().format(Enum.DATE_FORMAT_EDMY_VALUE)}
                                >Attend</CIMSButton>
                                : null
                            }
                        </Grid>
                        {
                            this.props.isAttend && currentAppointment === null ?
                                // <AttendSuccessPanel
                                //     patientInfo={patientInfo}
                                //     appointmentConfirmInfo={appointmentConfirmInfo}
                                // />
                                <ConfirmationWindow
                                    id={'take_attendance_confirmation'}
                                    type={Enum.APPOINTMENT_TYPE.TAKE_ATTENDANCE}
                                    patientInfo={patientInfo}
                                    confirmationInfo={appointmentConfirmInfo}
                                />
                                :
                                <AttendanceDetail
                                    currentAppointment={currentAppointment}
                                    patientStatusList={patientStatusList}
                                    patientStatus={patientStatus}
                                    patientStatusFlag={patientStatusFlag}
                                    clinicConfig={clinicConfig}
                                    handleChange={this.handleChange}
                                />
                        }
                    </ValidatorForm>
                    {showLateApptConfig.configValue === 'Y' && !this.props.isAttend && lateApptTime && isSameDayAppt ?
                        <Grid item container direction="column" xs={12} justify="center" className={classes.lateApptPanel} id={'appointment_late_appointment_info_panel'}>
                            <Typography className={classes.attendanceInfoItem}>
                                {`The ${CommonUtilities.getPatientCall().toLowerCase()} is ${lateApptTime.hours} hrs ${lateApptTime.minutes - (lateApptTime.hours * 60)} mins Late`}
                            </Typography>
                        </Grid>
                        : null}
                </Grid>
                <EditModeMiddleware componentName={accessRightEnum.attendance} when={!this.props.isAttend} />
            </Grid>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        accessRights: state.login.accessRights,
        serviceCd: state.login.service.serviceCd,
        patientStatusList: state.attendance.patientStatusList,
        patientStatus: state.attendance.patientStatus,
        discNum: state.attendance.discNum,
        isAttend: state.attendance.isAttend,
        patientInfo: state.patient.patientInfo,
        appointmentConfirmInfo: state.attendance.appointmentConfirmInfo,
        searchParameter: state.attendance.searchParameter,
        appointmentInfo: state.patient.appointmentInfo,
        caseNoInfo: state.patient.caseNoInfo,
        openPromptDialog: state.attendance.openPromptDialog,
        clinicCd: state.login.clinic.clinicCd,
        clinicConfig: state.common.clinicConfig,
        appointmentList: state.attendance.appointmentList,
        currentAppointment: state.attendance.currentAppointment,
        patientStatusFlag: state.attendance.patientStatusFlag
    };
};

const mapDispatchToProps = {
    getPatientStatusList,
    updateField,
    markAttendance,
    destroyMarkAttendance,
    getAppointmentForAttend,
    deleteSubTabs,
    openCommonMessage,
    openCaseNoDialog,
    getPatientPanelPatientById,
    getRegPatientById,
    selectCaseTrigger,
    listAppointmentList,
    getEncounterType,
    getCodeList,
    refreshServiceStatus,
    getPatientAppointment,
    getPatientEncounter
};
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Attendance));