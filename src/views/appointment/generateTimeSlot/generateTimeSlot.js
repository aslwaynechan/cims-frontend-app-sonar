import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { withStyles } from '@material-ui/core/styles';
import {
    // ListItemText,
    // ListItem,
    // List,
    Grid,
    InputLabel
} from '@material-ui/core';
import CommonTableDialog from '../../compontent/commonDialog/commonTableDialog';
import CIMSButton from '../../../components/Buttons/CIMSButton';
import CIMSGrid from '../../../components/Grid/CIMSGrid';
import DateFieldValidator from '../../../components/FormValidator/DateFieldValidator';
import SelectFieldValidator from '../../../components/FormValidator/SelectFieldValidator';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import _ from 'lodash';
import {
    resetAll,
    getCodeList,
    updateField,
    generateTimeSlot,
    searchTimeSlotTemplate,
    handleSelectTemplate,
    closeDialog
} from '../../../store/actions/appointment/generateTimeSlot/generateTimeSlotAction';
import { openCommonMessage } from '../../../store/actions/message/messageAction';
import CIMSTable from '../../../components/Table/CIMSTable';
import ValidatorEnum from '../../../enums/validatorEnum';
import CommonMessage from '../../../constants/commonMessage';
import * as CommonUtil from '../../../utilities/commonUtilities';
import Enum from '../../../enums/enum';

const styles = () => ({
    tablebutton: {
        width: '150px',
        margin: '0px',
        marginBottom: '5px'
    },
    MUIDataRowSelected: {
        backgroundColor: '#6e6e6e',
        color: '#ffffff'
    },
    container: {
        boxSizing: 'border-box',
        display: 'flex',
        flexWrap: 'wrap',
        width: '100%',
        marginBottom: 0
    },
    shortName: {
        paddingLeft: '10px',
        wordBreak: 'break-all'
    }
});

class GenerateTimeSlot extends Component {
    constructor(props) {
        super(props);

        const headRows = [
            { name: 'slotProfileCode', label: 'Slot Profile Code', width: 50 },
            { name: 'sun', label: 'SUN', width: 20 },
            { name: 'mon', label: 'MON', width: 20 },
            { name: 'tue', label: 'TUE', width: 20 },
            { name: 'wed', label: 'WED', width: 20 },
            { name: 'thur', label: 'THUR', width: 20 },
            { name: 'fri', label: 'FRI', width: 20 },
            { name: 'sat', label: 'SAT', width: 20 },
            { name: 'startTime', label: 'Start Time', width: 50 },
            { name: 'overallQuota', label: 'Overall Quota', width: 20 },
            { name: 'newNormal', label: 'New Normal', width: 20 },
            { name: 'newForce', label: 'New Force', width: 20 },
            { name: 'newPublic', label: 'New Public', width: 20 },
            { name: 'oldNormal', label: 'Old Normal', width: 20 },
            { name: 'oldForce', label: 'Old Force', width: 20 },
            { name: 'oldPublic', label: 'Old Public', width: 20 }
        ];

        this.state = {
            data: null,
            rows: headRows,
            options: {
                page: 0,
                rowsPerPage: 10,
                rowsPerPageOptions: [10, 15, 20]
            },
            encounterTypeTips: '',
            subEncounterTypeTips: '',
            tableRows: [
                { name: 'slotProfileCode', label: 'Slot Profile Code' },
                { name: 'sun', label: 'SUN' },
                { name: 'mon', label: 'MON' },
                { name: 'tue', label: 'TUE' },
                { name: 'wed', label: 'WED' },
                { name: 'thur', label: 'THUR' },
                { name: 'fri', label: 'FRI' },
                { name: 'sat', label: 'SAT' },
                { name: 'startTime', label: 'Start Time' },
                { name: 'overallQuota', label: 'Overall Quota' }
                // { name: 'newNormal', label: 'New Normal' },
                // { name: 'newForce', label: 'New Force' },
                // { name: 'newPublic', label: 'New Public' },
                // { name: 'oldNormal', label: 'Old Normal' },
                // { name: 'oldForce', label: 'Old Force' },
                // { name: 'oldPublic', label: 'Old Public' }
            ],
            tableOptions: {
                rowExpand: true,
                rowsPerPage: 10,
                rowsPerPageOptions: [10, 15, 20],
                onSelectIdName: 'slotTemplateId',
                onMultiSelect: true,
                onSelectedRow: (rowId, rowData, selectedData) => this.handleRowClick(rowId, rowData, selectedData)
            },
            direction: 'asc',
            orderBy: '',
            defaultQuotaDescValue: null
        };
    }
    UNSAFE_componentWillMount() {
        const where = { serviceCd: this.props.serviceCd, clinicCd: this.props.clinicCd };
        const defaultQuotaDesc = CommonUtil.getPriorityConfig(Enum.CLINIC_CONFIGNAME.QUOTA_TYPE_DESC, this.props.clinicConfig, where);


        const quotaArr = defaultQuotaDesc.configValue ? defaultQuotaDesc.configValue.split('|') : null;
        let newQuotaArr = CommonUtil.transformToMap(quotaArr);
        this.setState({ defaultQuotaDescValue: newQuotaArr });
    }

    componentDidMount() {
        this.props.ensureDidMount();
        this.props.getCodeList();
        let { tableRows } = this.state;
        this.state.defaultQuotaDescValue.forEach((item) => {
            let newParams = {};
            let oldParams = {};
            newParams.name = 'new' + item.engDesc;
            newParams.label = 'New ' + item.engDesc;
            oldParams.name = 'old' + item.engDesc;
            oldParams.label = 'Old ' + item.engDesc;
            tableRows.push(newParams);
            tableRows.push(oldParams);
            this.setState({ tableRows });
        });
    }

    UNSAFE_componentWillUpdate(nextProps) {
        if (nextProps.slotTemplateDto !== this.props.slotTemplateDto) {
            let data = [];
            const { slotTemplateDto } = nextProps;
            data = _.cloneDeep(slotTemplateDto);
            for (let i = 0; i < data.length; i++) {
                data[i].rowId = i;
                if (data[i]['week']) {
                    data[i].sun = data[i]['week'][0] === '1' ? 'Y' : 'N';
                    data[i].mon = data[i]['week'][1] === '1' ? 'Y' : 'N';
                    data[i].tue = data[i]['week'][2] === '1' ? 'Y' : 'N';
                    data[i].wed = data[i]['week'][3] === '1' ? 'Y' : 'N';
                    data[i].thur = data[i]['week'][4] === '1' ? 'Y' : 'N';
                    data[i].fri = data[i]['week'][5] === '1' ? 'Y' : 'N';
                    data[i].sat = data[i]['week'][6] === '1' ? 'Y' : 'N';
                }
            }
            this.setState({ data });
        }
        if (nextProps.subEncounterTypeCd !== this.props.subEncounterTypeCd || nextProps.encounterTypeCd !== this.props.encounterTypeCd ||
            nextProps.encounterCodeList !== this.props.encounterCodeList || nextProps.subEncounterCodeList !== this.props.subEncounterCodeList) {
            let encounterDo = nextProps.encounterCodeList.find(item => item.encounterTypeCd === nextProps.encounterTypeCd);
            let subEncounterDo = nextProps.subEncounterCodeList.find(item => item.subEncounterTypeCd === nextProps.subEncounterTypeCd);
            this.setState({ encounterTypeTips: encounterDo ? encounterDo.shortName : '', subEncounterTypeTips: subEncounterDo ? subEncounterDo.shortName : '' });
        }
    }

    componentWillUnmount() {
        this.props.resetAll();
    }

    handleSearch = () => {
        let params = {
            encounterTypeCd: this.props.encounterTypeCd,
            subEncounterTypeCd: this.props.subEncounterTypeCd,
            slotProfileCode: ''
        };
        params.page = 1;
        this.tableRef.updatePage(0);
        this.props.searchTimeSlotTemplate(params);
        this.clearTableSelected();
    }

    handleFilter = () => {
    }

    handleRowClick = (rowId, rowData, selectedData) => {
        this.props.handleSelectTemplate(selectedData);
    }

    // handlePageChange = (currentPage) => {

    // }

    handleSubmitGenerate = () => {
        this.refs.form2.submit();
    }

    handleOpenGenerateDialog = () => {
        const { selectTemplate } = this.props;
        if (!selectTemplate || selectTemplate.length <= 0) {
            this.props.openCommonMessage({
                msgCode: '110213'

            });
            return;
        }
        for (let i = 0; i < selectTemplate.length - 1; i++) {
            for (let j = i + 1; j < selectTemplate.length; j++) {
                if (selectTemplate[i]['startTime'] === selectTemplate[j]['startTime'] && (selectTemplate[i]['week'] & selectTemplate[j]['week']) !== 0) {
                    this.props.openCommonMessage({
                        msgCode: '110215',
                        params: [
                            { name: 'CODE1', value: selectTemplate[i]['slotProfileCode'] },
                            { name: 'CODE2', value: selectTemplate[j]['slotProfileCode'] }
                        ]
                    });
                    return;
                }
            }
        }
        this.props.openCommonMessage({
            msgCode: '110212',
            params: [
                { name: 'FROM_DATE', value: moment(this.props.fromDate).format('DD MMM YYYY') },
                { name: 'TO_DATE', value: moment(this.props.toDate).format('DD MMM YYYY') }
            ],
            btnActions: {
                btn1Click: this.handleGenerate
            }
        });
    }

    handleGenerate = () => {
        let idArr = [];
        this.props.selectTemplate.forEach((item) => {
            idArr.push(item.slotTemplateId);
        });
        let params = {
            dateFrom: moment(this.props.fromDate).format(Enum.DATE_FORMAT_EYMD_VALUE),
            dateTo: moment(this.props.toDate).format(Enum.DATE_FORMAT_EYMD_VALUE),
            slotTemplateIds: idArr
        };
        this.props.generateTimeSlot(params);
    }

    handleReset = () => {
        this.props.resetAll();
        this.clearTableSelected();
    }

    handleDateChange = (e, name) => {
        this.props.updateField(name, e);
        if (name === 'fromDate' && moment(this.props.toDate).isBefore(moment(e))) {
            this.props.updateField('toDate', e);
        }
    }

    handleSelectChange = (e, name) => {
        this.props.updateField(name, e.value);
    }

    handleCloseDialog = () => {
        this.props.closeDialog();
    }

    clearTableSelected = () => {
        this.tableRef.clearSelected();
    }

    render() {
        const { classes } = this.props;
        const fieldValidators = {
            encounterType: [ValidatorEnum.required],
            subEncounterType: [ValidatorEnum.required],
            fromDate: [ValidatorEnum.required],
            toDate: [ValidatorEnum.required]
        };

        const fieldErrorMessages = {
            encounterType: [CommonMessage.VALIDATION_NOTE_REQUIRED()],
            subEncounterType: [CommonMessage.VALIDATION_NOTE_REQUIRED()],
            fromDate: [CommonMessage.VALIDATION_NOTE_REQUIRED()],
            toDate: [CommonMessage.VALIDATION_NOTE_REQUIRED()]
        };
        return (
            <Grid>
                <ValidatorForm ref="form" onSubmit={this.handleSearch}>
                    <Grid container>
                        <Grid item container xs={5} alignItems="center" >
                            <Grid item container xs={6}>
                                <SelectFieldValidator
                                    options={this.props.encounterCodeList && this.props.encounterCodeList.map((item) => ({ value: item.encounterTypeCd, label: item.encounterTypeCd, shortName: item.shortName }))}
                                    id="select_generate_timeslot_encountertype"
                                    TextFieldProps={{
                                        variant: 'outlined',
                                        label: 'Encounter'
                                    }}
                                    value={this.props.encounterTypeCd}
                                    onChange={e => this.handleSelectChange(e, 'encounterTypeCd')}
                                    validators={fieldValidators.encounterType}
                                    errorMessages={fieldErrorMessages.encounterType}
                                    ref="encounterTypeRef"
                                />
                            </Grid>
                            <Grid item container xs={6}>
                                <InputLabel className={classes.shortName}>{this.state.encounterTypeTips}</InputLabel>
                            </Grid>
                        </Grid>
                        <Grid item container xs={5} alignItems="center">
                            <Grid item container xs={6}>
                                <SelectFieldValidator
                                    options={this.props.subEncounterCodeList && this.props.subEncounterCodeList.map((item) => ({ value: item.subEncounterTypeCd, label: item.subEncounterTypeCd, shortName: item.shortName }))}
                                    id="select_generate_timeslot_subencountertype"
                                    TextFieldProps={{
                                        variant: 'outlined',
                                        label: 'Sub-encounter'
                                    }}
                                    value={this.props.subEncounterTypeCd}
                                    onChange={e => this.handleSelectChange(e, 'subEncounterTypeCd')}
                                    validators={fieldValidators.subEncounterType}
                                    errorMessages={fieldErrorMessages.subEncounterType}
                                    ref="subEncounterTypeRef"
                                />
                            </Grid>
                            <Grid item container xs={6}>
                                <InputLabel className={classes.shortName}>{this.state.subEncounterTypeTips}</InputLabel>
                            </Grid>
                        </Grid>
                        <Grid item container xs={2}>
                            <CIMSButton
                                id="generateTimeSlot_search"
                                type={'submit'}
                                style={{ width: '100%' }}
                            >Search</CIMSButton>
                        </Grid>
                    </Grid>
                </ValidatorForm>


                <Grid container>
                    <Grid container item justify="flex-end" style={{ margin: '9px 0' }}>
                        <ValidatorForm ref="form2" onSubmit={this.handleOpenGenerateDialog} style={{ flex: 1, marginRight: 86 }}>
                            <CIMSGrid container item spacing={1}>
                                <Grid item container xs={6}>
                                    <DateFieldValidator
                                        id="date_generate_timeslot_fromdate"
                                        isRequired
                                        label="From Date"
                                        disablePast
                                        value={this.props.fromDate}
                                        onChange={e => this.handleDateChange(e, 'fromDate')}
                                    />
                                </Grid>
                                <Grid item container xs={6}>
                                    <DateFieldValidator
                                        id="date_generate_timeslot_todate"
                                        isRequired
                                        label="To Date"
                                        disablePast
                                        minDate={this.props.fromDate}
                                        value={this.props.toDate}
                                        onChange={e => this.handleDateChange(e, 'toDate')}
                                    />
                                </Grid>
                            </CIMSGrid>
                        </ValidatorForm>
                        <CIMSButton
                            id="btn_timeslot_filter"
                            className={classes.tablebutton}
                            onClick={this.handleFilter}
                        >Filter</CIMSButton>

                        <CIMSButton
                            id="btn_timeslot_generate"
                            className={classes.tablebutton}
                            onClick={this.handleSubmitGenerate}
                        >Generate</CIMSButton>

                        <CIMSButton
                            id="generateTimeSlot_2in1"
                            className={classes.tablebutton}
                        >2 in 1</CIMSButton>

                        <CIMSButton
                            id="btn_timeslot_reset"
                            className={classes.tablebutton}
                            onClick={this.handleReset}
                        >Reset</CIMSButton>
                    </Grid>
                    <Grid container>
                        <CIMSTable
                            innerRef={ref => this.tableRef = ref}
                            data={this.state.data}
                            rows={this.state.tableRows}
                            options={this.state.tableOptions}
                            orderDirection={this.state.direction}
                            orderBy={this.state.orderBy}
                            onRequestSort={(e, data) => {
                                const isDesc = this.state.orderBy === data.name && this.state.direction === 'desc';
                                this.setState({ orderBy: data.name, direction: isDesc ? 'asc' : 'desc' });
                            }}
                        />
                    </Grid>
                </Grid>
                <CommonTableDialog
                    id="generate-timeslot-dialog2"
                    dialogTitle="Generate Timeslot"
                    open={this.props.timeslotOpenDialog}
                    message={this.props.timeslotErrorMessage}
                    showTable={this.props.timeslotErrorData && this.props.timeslotErrorData.length > 0}
                    store={this.props.timeslotErrorData}
                    columns={[{ name: 'detail', label: 'Detail' }]}
                    onClose={this.handleCloseDialog}
                />
            </Grid>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        encounterTypeCd: state.generateTimeSlot.encounterTypeCd,
        subEncounterTypeCd: state.generateTimeSlot.subEncounterTypeCd,
        fromDate: state.generateTimeSlot.fromDate,
        toDate: state.generateTimeSlot.toDate,
        encounterCodeList: state.generateTimeSlot.encounterCodeList,
        subEncounterCodeList: state.generateTimeSlot.subEncounterCodeList,
        slotTemplateDto: state.generateTimeSlot.slotTemplateDto,
        selectTemplate: state.generateTimeSlot.selectTemplate,
        timeslotOpenDialog: state.generateTimeSlot.timeslotOpenDialog,
        timeslotErrorMessage: state.generateTimeSlot.timeslotErrorMessage,
        timeslotErrorData: state.generateTimeSlot.timeslotErrorData,
        clinicConfig: state.common.clinicConfig,
        serviceCd: state.login.service.serviceCd,
        clinicCd: state.login.clinic.clinicCd
        // isGenerateSuccess: state.generateTimeSlot.isGenerateSuccess
    };
};

const mapDispatchToProps = {
    resetAll,
    getCodeList,
    updateField,
    generateTimeSlot,
    searchTimeSlotTemplate,
    handleSelectTemplate,
    closeDialog,
    openCommonMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(GenerateTimeSlot));
