import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { RemoveCircle, AddCircle } from '@material-ui/icons';
import {
    Grid,
    Typography,
    IconButton,
    DialogContent,
    DialogActions,
    TextField,
    Box
} from '@material-ui/core';
import HistoryGrid from './component/historyGrid';
import CIMSButton from '../../../components/Buttons/CIMSButton';
import SelectFieldValidator from '../../../components/FormValidator/SelectFieldValidator';
import TextFieldValidator from '../../../components/FormValidator/TextFieldValidator';
import TimeFieldValidator from '../../../components/FormValidator/TimeFieldValidator';
import DateFieldValidator from '../../../components/FormValidator/DateFieldValidator';
import CIMSMultiTextField from '../../../components/TextField/CIMSMultiTextField';

import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import CIMSDialog from '../../../components/Dialog/CIMSDialog';
import Enum from '../../../enums/enum';
import * as appointmentUtilities from '../../../utilities/appointmentUtilities';
import {
    resetInfoAll,
    appointmentBook,
    listTimeSlot,
    listAppointment,
    getEncounterTypeList,
    getClinicList,
    bookConfirm,
    bookConfirmWaiting,
    updateState,
    updateField,
    getAppointmentReport,
    walkInAttendance,
    listSeachLogic,
    listRemarkCode,
    cancelAppointment,
    editAppointment,
    cancelEditAppointment,
    submitUpdateAppointment,
    listContatHistory,
    insertContatHistory,
    updateContatHistory,
    clearContactList,
    listToalAppointment
} from '../../../store/actions/appointment/booking/bookingAction';
import {
    updateState as updatePatientState,
    getPatientAppointment,
    getPatientById as getPatientPanelPatientById,
    getPatientEncounter
} from '../../../store/actions/patient/patientAction';
import { openCaseNoDialog, selectCaseTrigger } from '../../../store/actions/caseNo/caseNoAction';
import {
    openCommonCircular,
    closeCommonCircular,
    getEncounterType
} from '../../../store/actions/common/commonAction';
import { openCommonMessage } from '../../../store/actions/message/messageAction';
import moment from 'moment';
import _ from 'lodash';
import CommonMessage from '../../../constants/commonMessage';
import CommonRegex from '../../../constants/commonRegex';
import ValidatorEnum from '../../../enums/validatorEnum';
import { initEncounterType } from '../../../constants/appointment/bookingInformation/bookingInformationConstants';
import WalkInAttendanceInfo from './component/walkInAttendanceInfo';
import { codeList } from '../../../constants/codeList';
import {
    getPatientStatusList
} from '../../../store/actions/appointment/attendance/attendanceAction';
import * as commonUtilities from '../../../utilities/commonUtilities';
import EditModeMiddleware from '../../compontent/editModeMiddleware';
// import accessRightEnum from '../../../enums/accessRightEnum';
import { updateCurTab, deleteSubTabs } from '../../../store/actions/mainFrame/mainFrameAction';
import RequiredIcon from '../../../components/InputLabel/RequiredIcon';
import CIMSFormLabel from '../../../components/InputLabel/CIMSFormLabel';
import * as CommonUtil from '../../../utilities/commonUtilities';
import ConfirmationWindow from '../../compontent/confirmationWindow';
import * as AppointmentUtilities from '../../../utilities/appointmentUtilities';
import OutlinedRadioValidator from '../../../components/FormValidator/OutlinedRadioValidator';
import CIMSPromptDialog from '../../../components/Dialog/CIMSPromptDialog';
import CIMSTable from '../../../components/Table/CIMSTable';
import EcsResultTextField from '../../../components/ECS/Ecs/EcsResultTextField';
import * as EcsUtilities from '../../../utilities/ecsUtilities';
import {openEcsDialog, refreshServiceStatus, setEcsPatientStatus} from '../../../store/actions/ECS/ecsAction';
import { getCodeList } from '../../../store/actions/patient/patientAction';
import * as ecsActionType from '../../../store/actions/ECS/ecsActionType';


const sysRatio = commonUtilities.getSystemRatio();
const unit = commonUtilities.getResizeUnit(sysRatio);
import AccessRightEnum from '../../../enums/accessRightEnum';

const styles = theme => ({
    root: {
        padding: 4
    },
    maintitleRoot: {
        paddingTop: 6,
        fontSize: '14pt',
        fontWeight: 600
    },
    addEncounterContainer: {
        marginTop: 8,
        marginBottom: 8
    },
    addEncounterContainerItem: {
        padding: 4
    },
    addButtonRoot: {
        borderRadius: '0%',
        padding: '0px'
    },
    dialogContent: {
        textAlign: 'center',
        padding: '0px 6px'
    },
    dialogErrorContent: {
        padding: '6px 6px',
        textAlign: 'center',
        backgroundColor: theme.palette.error.main,
        color: theme.palette.background.default
    },
    dialogAction: {
        justifyContent: 'center'
    },
    errorFieldNameText: {
        fontSize: '12px',
        wordBreak: 'break-word',
        color: '#fd0000'
    },
    customTableHeadCell: {
        fontWeight: 400
    },
    customTableBodyCell: {
        fontSize: '14px',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        overflow: 'hidden'
    },
    customSelectedTableBodyCell: {
        fontSize: '14px',
        whiteSpace: 'normal',
        wordWrap: 'break-word'
    },
    bookingSuccessInfo: {
        backgroundColor: theme.palette.primary.main
    },
    bookingSuccessInfoItem: {
        fontSize: '2.5em',
        color: theme.palette.white,
        textAlign: 'center'
    },
    formLabelContainer: {
        paddingTop: 15,
        paddingBottom: 15
    },
    iconButton: {
        padding: 5,
        borderRadius: 'unset',
        height: 30
    },
    marginTop20: {
        marginTop: 6
    },
    radioGroup: {
        //height: 36
        height: 39,
        marginBottom: 20
    },
    gridTitle: {
        padding: '4px 0px'
    },
    tableTitle: {
        fontWeight: 500,
        color: theme.palette.white
    },
    buttonRoot: {
        margin: 2,
        padding: 0,
        height: 35
    },
    highLineRowRoot: {
        '& td': {
            color: '#0579c8',
            fontStyle: 'italic'
        }
    },
    detailTypography: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        textAlign: 'left',
        fontSize: '0.875rem',
        fontWeight: 400,
        padding: 10

    },
    paper: {
        minWidth: 300,
        maxWidth: '80%',
        borderRadius: 16,
        backgroundColor: 'rgba(249, 249, 249, 0.08)'
    }
});

class BookingInformation extends Component {
    constructor(props) {
        super(props);

        this.state = {
            failSearchExactTime: false,
            failSearchReason: '',
            errorMessageList: [],
            clinicCd_valid: true,
            clinicCd_errMsg: '',
            listApptPara: null,
            bookingDataBK: null,
            bookingData: {},
            clinicCdEdit: true,
            encounterTypeCdEdit: true,
            subEncounterTypeCdEdit: true,
            caseTypeCdEdit: true,
            appointmentTypeCdEdit: true,
            appointmentDateEdit: true,
            appointmentTimeEdit: true,
            elapsedPeriodEdit: true,
            elapsedPeriodUnitEdit: true,
            searchLogicCdEdit: true,
            bookingUnitEdit: true,
            remarkIdEdit: true,
            memoEdit: true,
            quotaTypeList: [],
            patientStatusFlag: 'N',
            contactHistoryOpen: false,
            tableRows: [
                { name: 'contactType', label: 'Type', width: 60 },
                { name: 'contactDesc', label: 'Contact Description', width: 100 },
                {
                    name: 'notificationDtm', label: 'Notification Date/Time', width: 150, customBodyRender: (value) => {
                        return moment(value).format(`${Enum.DATE_FORMAT_EDMY_VALUE} ${Enum.TIME_FORMAT_24_HOUR_CLOCK}`);
                    }
                },
                { name: 'callerName', label: 'Caller Name', width: 100 },
                { name: 'notes', label: 'Notes', width: 150 }
            ],
            tableOptions: {
                rowExpand: true,
                rowsPerPage: 10,
                rowsPerPageOptions: [10],
                onSelectIdName: 'contactHistoryId',
                customRowStyle: (rowData) => {
                    let className = '';
                    let currentContact = this.state.currentSelectedContactInfo;
                    if (currentContact && rowData.contactHistoryId === currentContact.contactHistoryId) {
                        className = `${className} ${this.props.classes.highLineRowRoot}`;
                    }
                    return className;
                },
                onSelectedRow: (rowId, rowData, selectedData) => {
                    const selected = selectedData.length == 0 ? null : selectedData[0];
                    if (this.props.loginName == rowData.callerName) {
                        this.setState({ currentSelectedContact: selected, currentSingleSelectedContactInfo: selected });
                    } else {
                        this.setState({ currentSelectedContact: null, currentSingleSelectedContactInfo: selected });
                    }
                },
                onRowDoubleClick: (rowData) => {
                    if (this.state.currentSelectedContactInfo) {
                        return;
                    }
                    if (this.props.loginName == rowData.callerName) {

                        this.setState({
                            currentSelectedContactInfo: rowData,
                            appointmentId: rowData.appointmentId,
                            callerName: rowData.callerName,
                            NotificationDate: moment(rowData.notificationDtm, Enum.DATE_FORMAT_24_HOUR),
                            NotificationTime: moment(rowData.notificationDtm, Enum.DATE_FORMAT_24_HOUR),
                            contactType: rowData.contactType,
                            note: rowData.notes
                        });

                        if (rowData.contactType == 'Tel') {
                            this.setState({ tel: rowData.contactDesc });
                        } else if (rowData.contactType == 'Fax') {
                            this.setState({ fax: rowData.contactDesc });
                        } else if (rowData.contactType == 'Mail') {
                            this.setState({ email: rowData.contactDesc });
                        }
                    }

                }
            },
            callerName: this.props.loginName,
            NotificationDate: moment(),
            NotificationTime: moment(),
            tel: this.props.patientInfo.phoneList.filter(item => item.phonePriority == 1)[0] && this.props.patientInfo.phoneList.filter(item => item.phonePriority == 1)[0].phoneNo || '',
            email: '',
            fax: '',
            note: '',
            contactType: 'Tel',
            rowData: {},
            currentSelectedContactInfo: null,
            currentSelectedContact: null,
            currentSingleSelectedContactInfo: null
        };
    }

    UNSAFE_componentWillMount() {
        const where = { serviceCd: this.props.serviceCd, clinicCd: this.props.clinicCd };
        const defaultQuotaDesc = CommonUtil.getPriorityConfig(Enum.CLINIC_CONFIGNAME.QUOTA_TYPE_DESC, this.props.clinicConfig, where);
        const quotaArr = defaultQuotaDesc.configValue ? defaultQuotaDesc.configValue.split('|') : null;
        let quotaTypeList = CommonUtil.transformToMap(quotaArr);
        quotaTypeList.push({
            code: 'U',
            engDesc: 'Urgent'
        });

        this.setState({ quotaTypeList });
    }
    componentDidMount() {
        let codePara = [codeList.patient_status];
        let getCodeListParams = [
            codeList.exact_date_of_birth
        ];
        this.props.getCodeList(getCodeListParams, ecsActionType.PUT_GET_CODE_LIST);
        this.props.refreshServiceStatus();

        this.props.resetInfoAll();
        this.props.getClinicList(this.props.serviceCd);
        this.props.getPatientStatusList(codePara);
        this.props.listSeachLogic();
        this.props.listRemarkCode();
        this.loadDefaultEncounterCd();
        this.props.updateCurTab('F007', this.doClose);
        if (this.props.isWalkIn) {
            let walkInfo = { ...this.props.walkInAttendanceInfo, patientStatus: this.props.caseNoInfo.patientStatus };
            this.props.updateState({ walkInAttendanceInfo: walkInfo });
        }
        const { caseNoInfo } = this.props;
        if (!caseNoInfo || !caseNoInfo.caseNo) {
            this.props.openCommonMessage({
                msgCode: '110294',
                btnActions: {
                    btn1Click: () => { this.props.deleteSubTabs(AccessRightEnum.booking); }
                }
            });
        }
    }

    componentDidUpdate() {
        if (this.props.bookingConfirmed && this.props.confirmData && (!this.props.appointmentInfo || !this.props.appointmentInfo.appointmentId)) {
            this.props.getPatientAppointment(this.props.confirmData.appointmentId);
        }
    }

    componentWillUnmount() {
        this.props.resetInfoAll();
    }

    doClose = (callback) => {
        const { bookingConfirmed, isUpdating } = this.props;

        if (!bookingConfirmed || isUpdating === true) {
            this.props.openCommonMessage({
                msgCode: '110018',
                btnActions: {
                    btn1Click: () => {
                        callback(true);
                    }
                }
            });
        }
        else {
            callback(true);
        }

    }

    handleBookClick = () => {
        this.props.openCommonCircular();
        this.refs.form.submit();
    }

    loadDefaultEncounterCd = () => {
        let where = { serviceCd: this.props.serviceCd, clinicCd: this.props.clinicCd };
        let defaultEncounterCd = commonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.DEFAULT_ENCOUNTER_CD, this.props.clinicConfig, where);
        this.props.updateState({ defaultEncounterCd: defaultEncounterCd.configValue });
    }

    handleBooking = () => {
        this.props.closeCommonCircular();
        let params = _.cloneDeep(this.props.bookingData);
        params = appointmentUtilities.handleBookingDataToParams(params, this.props.defaultCaseTypeCd, this.props.openCommonMessage);
        if (!params) return;
        delete params.page;
        delete params.pageSize;
        if (this.props.isWalkIn) {
            //Case No handle
            const { caseNoInfo, patientInfo } = this.props;
            if (!caseNoInfo.caseNo) {
                this.props.openCaseNoDialog({
                    caseDialogStatus: Enum.CASE_DIALOG_STATUS.CREATE,
                    caseNoForm: { patientKey: patientInfo.patientKey },
                    isNoPopup: true,
                    caseCallBack: (data) => {
                        if (data && data.caseNo) {
                            this.props.getPatientPanelPatientById(patientInfo.patientKey, null, data.caseNo, () => {
                                this.handleBookAndAttend();
                            });
                        }
                    }
                });
            } else {
                this.handleBookAndAttend();
            }
        } else {
            if (params && params.encounterTypes) {
                params.patientKey = this.props.patientInfo.patientKey;
                this.props.appointmentBook(params);
            }
        }

    }

    handleBookConfirm = () => {
        let fieldData = {};
        let patientInfo = this.props.patientInfo;
        let submitParams = {};
        //no patient
        if (!patientInfo || !patientInfo.patientKey) {
            this.props.openCommonMessage({
                msgCode: '110205',
                params: [{ name: 'PATIENT_CALL', value: commonUtilities.getPatientCall() }]
            });
            fieldData.openConfirmDialog = false;
            this.props.updateState(fieldData);
            return;
        }
        //dead patient
        if (parseInt(patientInfo.deadInd)) {
            this.props.openCommonMessage({
                msgCode: '110206',
                params: [{ name: 'PATIENT_CALL', value: commonUtilities.getPatientCall() }]
            });
            fieldData.openConfirmDialog = false;
            this.props.updateState(fieldData);
            return;
        }
        //no caseno
        if (!this.props.caseNoInfo.caseNo) {
            const caseActiveList = patientInfo.caseList && patientInfo.caseList.filter(item => item.statusCd === Enum.CASE_STATUS.ACTIVE);
            if (caseActiveList.length > 0) {
                this.props.selectCaseTrigger({ trigger: true });
                return;
            }
        }

        this.props.updateState({ handlingBooking: true });
        let bookingData = _.cloneDeep(this.props.bookingData);
        let bookTimeSlotList = _.cloneDeep(this.props.bookTimeSlotData);
        let params = [];

        for (let i = 0; i < bookingData.encounterTypes.length; i++) {
            let obj = {
                appointmentDateVo: bookTimeSlotList[i].slotDate,
                appointmentTime: bookTimeSlotList[i].startTime,
                appointmentTypeCd: bookingData.encounterTypes[i].appointmentTypeCd,
                bookingUnit: bookTimeSlotList[i].list.length,
                caseTypeCd: bookingData.encounterTypes[i].caseTypeCd || this.props.defaultCaseTypeCd,
                clinicCd: bookingData.clinicCd,
                patientKey: patientInfo.patientKey === '0' ? '' : patientInfo.patientKey,
                encounterTypeCd: bookingData.encounterTypes[i].encounterTypeCd,
                subEncounterTypeCd: bookingData.encounterTypes[i].subEncounterTypeCd,
                slots: bookTimeSlotList[i].list,
                remarkId: bookingData.encounterTypes[i].remarkId,
                memo: bookingData.encounterTypes[i].memo,
                caseNo: this.props.caseNoInfo.caseNo || ''
            };
            params.push(obj);

            bookingData.encounterTypes[i].bookingUnit = obj.bookingUnit;
            bookingData.encounterTypes[i].appointmentDate = moment(bookTimeSlotList[i].slotDate);
            let timeSplit = bookTimeSlotList[i].startTime.split(':');
            bookingData.encounterTypes[i].appointmentTime = moment().set({ 'hours': timeSplit[0], 'minutes': timeSplit[1] });
            bookingData.encounterTypes[i].elapsedPeriod = '';
        }
        submitParams = {
            appointmentDtos: params
        };
        if (this.props.waitingList) {
            submitParams.waitingListDto = this.props.waitingList;
            this.props.bookConfirmWaiting(submitParams, bookingData);
        } else if (this.props.isUpdating) {
            let { listApptPara } = this.state;
            let { currentSelectedApptInfo, updateOrBookNew } = this.props;
            let updateApptPara = {
                ...submitParams.appointmentDtos[0],
                appointmentId: currentSelectedApptInfo.appointmentId,
                version: currentSelectedApptInfo.version
            };
            this.props.submitUpdateAppointment(updateApptPara, listApptPara, updateOrBookNew, bookingData, submitParams);
        } else {
            this.props.bookConfirm(submitParams, bookingData);
        }
        this.props.updateState(fieldData);
    }

    handleBookSearch = (e, index) => {
        let params = _.cloneDeep(this.props.bookingData);
        params.page = 1;
        params.pageSize = 10;
        params = appointmentUtilities.handleBookingDataToParams(params);
        params.encounterTypes[index].caseTypeCd = this.props.bookingData.encounterTypes[index].caseTypeCd || this.props.defaultCaseTypeCd;
        this.props.updateState({ openConfirmDialog: false, changeEncounterIndex: index, handlingBooking: false });
        this.props.listTimeSlot(params);
    }

    handleBookCancel = () => {
        this.props.updateState({ openConfirmDialog: false, handlingBooking: false });
    }

    handleRemoveEncounter = (e, index) => {
        let bookingData = _.cloneDeep(this.props.bookingData);
        if (bookingData.encounterTypes && bookingData.encounterTypes.length > 1) {
            bookingData.encounterTypes.splice(index, 1);
        }
        this.props.updateState({ bookingData });
        let { errorMessageList } = this.state;
        if (errorMessageList && errorMessageList[index]) {
            errorMessageList.splice(index, 1);
        }
        this.setState({ errorMessageList });
    }

    handleAddEncounter = () => {
        let bookingData = _.cloneDeep(this.props.bookingData);
        let newData = _.cloneDeep(initEncounterType);
        let defaultEncounterCd = this.props.defaultEncounterCd;
        newData.encounterTypeList = bookingData.encounterTypes[0].encounterTypeList;
        newData.subEncounterTypeList = bookingData.encounterTypes[0].subEncounterTypeList;
        bookingData.encounterTypes.push(newData);
        bookingData = appointmentUtilities.initBookData(bookingData, newData.encounterTypeList, defaultEncounterCd);
        this.props.updateState({ bookingData });
    }

    handleSelectFieldChange = (e, name) => {
        let bookingData = _.cloneDeep(this.props.bookingData);
        bookingData[name] = e.value;
        if (name === 'clinicCd') {
            this.props.getEncounterTypeList(e.value, this.props.serviceCd, e.value, this.props.clinicConfig);
        }
        this.props.updateState({ bookingData });
        let params = {};
        if ((this.state.bookingData[name] || '') != e.value) {
            params[name + 'Edit'] = false;
            this.setState({
                ...params
            });
        } else {
            params[name + 'Edit'] = true;
            this.setState({
                ...params
            });
        }

    }

    handleTextFieldChange = (e, name) => {
        let bookingData = _.cloneDeep(this.props.bookingData);

        bookingData[name] = e.target.value;
        this.props.updateState({ bookingData });
    }

    updateEncounterField = (name, value, index) => {
        let bookingData = _.cloneDeep(this.props.bookingData);
        bookingData.encounterTypes[index][name] = value;
        if (name === 'encounterTypeCd') {
            let encounterSelected = bookingData.encounterTypes[index].encounterTypeList.find(item => item.encounterTypeCd === value);
            bookingData.encounterTypes[index].subEncounterTypeList = encounterSelected ? encounterSelected.subEncounterTypeList || [] : [];
            bookingData.encounterTypes[index].subEncounterTypeCd = '';
        }

        if ((name === 'appointmentDate' || name === 'appointmentTime') && value) {
            bookingData.encounterTypes[index]['elapsedPeriod'] = '';
            bookingData.encounterTypes[index]['elapsedPeriodUnit'] = '';
            if (name === 'appointmentDate' && !bookingData.encounterTypes[index]['appointmentTime']) {
                if (moment(value).isAfter(moment(), 'day')) {
                    bookingData.encounterTypes[index]['appointmentTime'] = moment(value).set({ hours: 0, minute: 0, second: 0 });
                } else if (moment(value).isSame(moment(), 'day')) {
                    bookingData.encounterTypes[index]['appointmentTime'] = moment().set({ hours: 0, minute: 0, second: 0 });
                }
            }
        }

        if (name === 'elapsedPeriod' && value) {
            bookingData.encounterTypes[index]['appointmentDate'] = null;
            bookingData.encounterTypes[index]['appointmentTime'] = null;
        }

        if (name === 'elapsedPeriodUnit' && value && !bookingData.encounterTypes[index]['elapsedPeriod']) {
            bookingData.encounterTypes[index][name] = '';
        }

        let params = {};
        if ((this.state.bookingData['encounterTypes'] && this.state.bookingData['encounterTypes'][index][name] || '') != value) {
            params[name + 'Edit'] = false;
            this.setState({
                ...params
            });
        } else {
            params[name + 'Edit'] = true;
            this.setState({
                ...params
            });
        }

        this.props.updateState({ bookingData }, index);
        // this.getPatientStatusFlag(bookingData);
    }

    handleEncounterSelectFieldChange = (e, name, index) => {
        this.updateEncounterField(name, e.value, index);
    }

    handleEncounterDateFieldChange = (e, name, index) => {
        // if (name === 'appointmentTime') {
        //     if (e && moment(e).diff(moment()) < 0) {
        //         this.props.openCommonMessage({
        //             msgCode: '110207'
        //         });
        //         return;
        //     }
        // }
        this.updateEncounterField(name, e, index);
    }

    //eslint-disable-next-line
    handleEncounterTimeOnChange = (e, name, index, date) => {
        // if (date && e && date !== 'Invalid date' && e !== 'Invalid date') {
        //     if (appointmentUtilities.isExpireTime(date, e)) {
        //         this.props.openCommonMessage({
        //             msgCode: '110207'
        //         });
        //         return;
        //     }
        // }
        // let curTime = e.format(Enum.TIME_FORMAT_24_HOUR_CLOCK);
        // this.updateEncounterField(name, curTime, index);
        this.updateEncounterField(name, e, index);
    }

    handleEncounterTextFieldChange = (e, name, index) => {
        let value = e.target.value;
        if (name === 'elapsedPeriod' || name === 'bookingUnit') {
            let reg = new RegExp(CommonRegex.VALIDATION_REGEX_NOT_NUMBER);
            if (reg.test(value)) {
                return;
            }
        }
        this.updateEncounterField(name, e.target.value, index);
    }

    handleEncounterTextFieldOnBlur = (e, name, index) => {
        let value = e.target.value;
        if (name === 'bookingUnit' && !parseInt(value || 0)) {
            value = 1;
        }

        this.updateEncounterField(name, value, index);
    }

    handleSubmitError = () => {
        this.props.closeCommonCircular();
        // abandon: no need to show
        // this.props.openCommonMessage({
        //     msgCode: '110209'
        // });
    }

    validatorListener = (e, message, name, index) => {
        let { errorMessageList } = this.state;
        if (!errorMessageList[index]) {
            errorMessageList[index] = [];
        }
        let currentErrorList = errorMessageList[index];
        if (e && message === true) {
            currentErrorList = currentErrorList.filter((item) => {
                return item.fieldName !== name;
            });
        } else {
            let errorObj = currentErrorList.find(item => item.fieldName === name);
            if (errorObj) {
                errorObj.errMsg = message;
            } else {
                currentErrorList.push({ fieldName: name, errMsg: message });
            }
        }
        errorMessageList[index] = currentErrorList;
        this.setState({ errorMessageList });
    }

    validatorOtherListener = (isValid, message, name) => {
        if (isValid) {
            this.setState({ [name + '_valid']: true, [name + '_errMsg']: '' });
        } else {
            this.setState({ [name + '_valid']: false, [name + '_errMsg']: message });
        }
    }

    handleTimeSlotListOnClose = () => {
        this.props.updateState({ openTimeSlotModal: false });
    }

    handleWalkInInfoChange = (info) => {
        let updateData = { walkInAttendanceInfo: info };
        this.props.updateState(updateData);
    }

    handleBookAndAttend = () => {
        let { bookingData, patientInfo, walkInAttendanceInfo, caseNoInfo, defaultCaseTypeCd } = this.props;
        let encounterData = bookingData.encounterTypes[0];
        let reqDto = {
            appointmentDate: moment(encounterData.appointmentDate).format('YYYY-MM-DD'),
            appointmentTime: moment().format('HH:mm'),
            // appointmentTypeCd: encounterData.appointmentTypeCd,
            appointmentTypeCd: '',
            caseTypeCd: encounterData.caseTypeCd || defaultCaseTypeCd,
            clinicCd: bookingData.clinicCd,
            discNumber: walkInAttendanceInfo.discNumber,
            encounterTypeCd: encounterData.encounterTypeCd,
            patientKey: patientInfo.patientKey,
            patientStatusCd: walkInAttendanceInfo.patientStatus,
            // patientStatusCd: caseNoInfo.patientStatus,
            subEncounterTypeCd: encounterData.subEncounterTypeCd,
            caseNo: caseNoInfo.caseNo,
            amount: walkInAttendanceInfo.amount,
            paymentMeanCD: walkInAttendanceInfo.paymentMeanCD
        };
        this.props.walkInAttendance(reqDto, (appointmentId) => {
            if (appointmentId) {
                const { patientInfo } = this.props;
                this.props.getPatientAppointment(appointmentId, patientInfo && patientInfo.caseList);
                this.props.getPatientEncounter(appointmentId);
            }
        });
    }

    cancelAppointment = (data, listApptPara) => {
        let apptInfo = data;
        let cancelApptPara = {
            appointmentId: apptInfo.appointmentId,
            version: apptInfo.version
        };
        this.props.cancelAppointment(cancelApptPara, listApptPara);
        this.props.updateState({ currentSelectedApptInfo: null });
    }

    handleCancelAppointment = (data, listApptPara) => {
        this.props.openCommonMessage({
            msgCode: '110232',
            btnActions: {
                btn1Click: () => { this.cancelAppointment(data, listApptPara); }
            }
        });
    }

    handleEditAppointment = (data, listApptPara) => {
        this.props.editAppointment(data);
        this.setState({ listApptPara });
    }

    handleUpdateOrBookNew = () => {
        let currentAppt = this.props.currentSelectedApptInfo;
        let newAppt = this.props.bookingData.encounterTypes[0];
        let tempUpdateOrBookNew = 'update';

        if (currentAppt.encounterTypeCd !== newAppt.encounterTypeCd) {
            tempUpdateOrBookNew = 'bookNew';
        }

        if (currentAppt.subEncounterTypeCd !== newAppt.subEncounterTypeCd) {
            tempUpdateOrBookNew = 'bookNew';
        }

        if (currentAppt.caseTypeCd !== newAppt.caseTypeCd) {
            tempUpdateOrBookNew = 'bookNew';
        }

        if (currentAppt.appointmentTypeCd !== newAppt.appointmentTypeCd) {
            tempUpdateOrBookNew = 'bookNew';
        }

        if (currentAppt.appointmentDate !== newAppt.appointmentDate) {
            tempUpdateOrBookNew = 'bookNew';
        }

        if (currentAppt.appointmentTime !== moment(newAppt.appointmentTime).format('HH:mm')) {
            tempUpdateOrBookNew = 'bookNew';
        }
        // if (currentAppt.appointmentTime !== newAppt.appointmentTime) {
        //     tempUpdateOrBookNew = 'bookNew';
        // }

        return tempUpdateOrBookNew;
    }

    handleUpdateAppointment = () => {
        let { listApptPara } = this.state;
        let { currentSelectedApptInfo, bookingData } = this.props;
        let updateApptPara = {};

        let tempUpdateOrBookNew = this.handleUpdateOrBookNew();
        if (tempUpdateOrBookNew === 'bookNew') {
            this.props.updateState({ updateOrBookNew: tempUpdateOrBookNew });
            this.handleBookClick();
        } else {
            tempUpdateOrBookNew = 'update';

            this.validateEncounter();

            updateApptPara = {
                appointmentId: currentSelectedApptInfo.appointmentId,
                remarkId: bookingData.encounterTypes[0].remarkId,
                memo: bookingData.encounterTypes[0].memo,
                version: currentSelectedApptInfo.version
            };

            this.props.submitUpdateAppointment(updateApptPara, listApptPara, tempUpdateOrBookNew);
            this.props.updateState({ updateOrBookNew: tempUpdateOrBookNew });
        }
        this.setState({
            bookingData: {},
            clinicCdEdit: true,
            encounterTypeCdEdit: true,
            subEncounterTypeCdEdit: true,
            caseTypeCdEdit: true,
            appointmentTypeCdEdit: true,
            appointmentDateEdit: true,
            appointmentTimeEdit: true,
            elapsedPeriodEdit: true,
            elapsedPeriodUnitEdit: true,
            searchLogicCdEdit: true,
            bookingUnitEdit: true,
            remarkIdEdit: true,
            memoEdit: true
        });
    }

    validateEncounter = () => {
        this.refs.encounterSelectField.validateCurrent();
        this.refs.subEncounterSelectField.validateCurrent();

        if (!this.refs.encounterSelectField.isValid || !this.refs.subEncounterSelectField.isValid) {
            this.handleSubmitError();
        }
    }

    setBookingData = () => {
        let bookingData = _.cloneDeep(this.props.bookingData);
        this.setState({ bookingData });
    }

    getCasePatientStatus = (curAppointment) => {
        const { patientInfo } = this.props;
        let patientStatus = AppointmentUtilities.getPatientStatusCd(curAppointment, patientInfo);
        return patientStatus;
    }

    getPatientStatusFlag = (curAppointment) => {
        let patientStatusFlag = 'N';
        this.props.getEncounterType({ clinicCd: '' }, (encounterList) => {
            patientStatusFlag = AppointmentUtilities.getPatientStatusFlag(encounterList, curAppointment);
            //  this.props.updateField({ patientStatusFlag: patientStatusFlag });
        });
        // this.setState({patientStatusFlag});
        this.setState({ patientStatusFlag });
        // return patientStatusFlag;
    }

    clearContactListSelected = () => {
        this.tableRef.clearSelected();
        this.setState({
            callerName: this.props.loginName,
            NotificationDate: moment(),
            NotificationTime: moment(),
            tel: this.props.patientInfo.phoneList.filter(item => item.phonePriority == 1)[0] && this.props.patientInfo.phoneList.filter(item => item.phonePriority == 1)[0].phoneNo || '',
            email: '',
            fax: '',
            note: '',
            contactType: 'Tel',
            currentSelectedContactInfo: null,
            currentSelectedContact: null,
            currentSingleSelectedContactInfo: null
        });
    }

    handleContactFieldChange = (name, e) => {
        let stateData = { ...this.state };
        stateData[name] = e.target.value;
        let reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
        if (name == 'email' && reg.test(e.target.value)) {
            return;
        }
        this.setState({ ...stateData });
    }
    handleContactDateFieldChange = (name, value) => {
        let stateData = { ...this.state };
        stateData[name] = value;
        if ((name === 'NotificationDate' || name === 'NotificationTime') && value) {

            if (name === 'NotificationDate' && !this.state['NotificationTime']) {
                if (moment(value).isAfter(moment(), 'day')) {
                    stateData['NotificationTime'] = moment(value).set({ hours: 0, minute: 0, second: 0 });
                } else if (moment(value).isSame(moment(), 'day')) {
                    stateData['NotificationTime'] = moment().set({ hours: 0, minute: 0, second: 0 });
                }
            }
        }
        this.setState({ ...stateData });
    }
    handleCotactUpdate = () => {

        if (this.state.currentSelectedContactInfo) {
            let params = {
                appointmentId: this.state.rowData.appointmentId,
                callerName: this.state.callerName,
                contactDesc: this.state.contactType == 'Tel' ? this.state.tel : this.state.contactType == 'Fax' ? this.state.fax : this.state.email,
                contactHistoryId: this.state.currentSelectedContactInfo.contactHistoryId,
                contactType: this.state.contactType,
                notes: this.state.note,
                notificationDtm: this.state.NotificationDate.set({
                    hour: this.state.NotificationTime.hour(),
                    minute: this.state.NotificationTime.minute(),
                    second: this.state.NotificationTime.second()
                }).valueOf(),
                statusCd: 'A',
                version: this.state.currentSelectedContactInfo.version
            };
            this.props.updateContatHistory(params, () => {
                this.props.listContatHistory(this.state.rowData.appointmentId, () => {
                    this.clearContactListSelected();

                });
            });
        } else {
            let params = {
                appointmentId: this.state.rowData.appointmentId,
                callerName: this.state.callerName,
                contactDesc: this.state.contactType == 'Tel' ? this.state.tel : this.state.contactType == 'Fax' ? this.state.fax : this.state.email,
                contactHistoryId: 0,
                contactType: this.state.contactType,
                notes: this.state.note,
                notificationDtm: this.state.NotificationDate.set({
                    hour: this.state.NotificationTime.hour(),
                    minute: this.state.NotificationTime.minute(),
                    second: this.state.NotificationTime.second()
                }).valueOf(),
                statusCd: 'A'
            };
            this.props.insertContatHistory(params, () => {
                this.props.listContatHistory(this.state.rowData.appointmentId, () => {
                    this.clearContactListSelected();
                });
            });
        }
    }

    render() {
        const { classes,
            clinicList,
            bookingData,
            confirmData,
            bookTimeSlotData,
            patientInfo,
            caseNoInfo,
            openEcsDialog,
            accessRights,
            ecsUserId,
            ecsLocCode,
            ecsServiceStatus,
            selectedPatientEcsStatus,
            showEcsBtnInBooking,
            appointmentInfo,
            defaultCaseTypeCd
        } = this.props;
        const { errorMessageList } = this.state;
        // const patientStatusFlag = this.getPatientStatusFlag(curBookingData);
        // const patientStatusFlag = 'Y';

        let remark = '';
        if (bookingData && bookingData.encounterTypes[0].remarkId) {
            remark = this.props.remarkCodeList.find(item => item.remarkId === bookingData.encounterTypes[0].remarkId);
        }
        // if (this.props.isWalkIn) {
        //     let walkInfo = {...this.props.walkInAttendanceInfo,patientStatus:caseNoInfo.patientStatus };
        //     this.handleWalkInInfoChange(walkInfo);
        // }

        return (
            <Grid container spacing={1} className={classes.root}>
                <Grid item xs={6}>
                    <HistoryGrid
                        ref="historyGrid"
                        appointmentList={this.props.appointmentList}
                        timeSlotList={this.props.timeSlotList}
                        openTimeSlotModal={this.props.openTimeSlotModal}
                        patientInfo={this.props.patientInfo}
                        bookingConfirmed={this.props.bookingConfirmed}
                        bookingData={this.props.bookingData}
                        bookTimeSlotData={this.props.bookTimeSlotData}
                        changeEncounterIndex={this.props.changeEncounterIndex}
                        getAppointmentReport={(...args) => this.props.getAppointmentReport(...args)}
                        listAppointment={(...args) => {

                            this.props.listAppointment(...args, () => {
                                if (this.props.appointmentList.totalNum) {
                                    let params = {
                                        ...args[0],
                                        allService: false,
                                        page: 1,
                                        pageSize: this.props.appointmentList.totalNum
                                    };
                                    this.props.listToalAppointment(
                                        params
                                    );
                                }
                            });
                        }}
                        listTimeSlot={(...args) => this.props.listTimeSlot(...args)}
                        updateState={(...args) => this.props.updateState(...args)}
                        updateField={(...args) => this.props.updateField(...args)}
                        waitingList={this.props.waitingList}
                        cancelAppointment={this.handleCancelAppointment}
                        editAppointment={this.handleEditAppointment}
                        isEditingAppt={this.props.isUpdating}
                        currentSelectedApptInfo={this.props.currentSelectedApptInfo}
                        isWalkIn={this.props.isWalkIn}
                        setBookingData={this.setBookingData}
                        openContactHistoryDialog={(args) => {
                            this.setState({
                                contactHistoryOpen: args.contactHistoryOpen,
                                rowData: args.rowData,
                                callerName: this.props.loginName,
                                NotificationDate: moment(),
                                NotificationTime: moment(),
                                tel: this.props.patientInfo.phoneList.filter(item => item.phonePriority == 1)[0] && this.props.patientInfo.phoneList.filter(item => item.phonePriority == 1)[0].phoneNo || '',
                                email: '',
                                fax: '',
                                note: '',
                                contactType: 'Tel'
                            });
                            this.props.listContatHistory(args.rowData.appointmentId);
                        }}
                    />
                </Grid>
                {
                    this.props.bookingConfirmed ?
                        <Grid xs={6} item container>
                            <ConfirmationWindow
                                id={'booking_confirmation'}
                                type={Enum.APPOINTMENT_TYPE.BOOKING}
                                patientInfo={patientInfo}
                                confirmationInfo={{ ...confirmData, remark: remark ? remark.description : '' }}
                            />
                        </Grid>
                        :
                        <Grid item container direction="column" xs={6}>
                            <Grid style={{ width: '100%', overflow: this.props.isWalkIn ? null : 'auto', overflowX: this.props.isWalkIn ? null : 'hidden', padding: '0 4px' }}>
                                <ValidatorForm ref="form" onSubmit={this.handleBooking} onError={this.handleSubmitError}>
                                    <Grid item container xs={this.props.isWalkIn ? 12 : 11}>
                                        <Typography className={classes.maintitleRoot}>Appointment Booking</Typography>
                                        <Grid item container alignItems="flex-end" justify="space-between">
                                            <Grid item container xs>
                                            </Grid>

                                            {
                                                this.props.isWalkIn ?
                                                    <CIMSButton
                                                        id={'btn_appointment_walkIn_booking'}
                                                        children={'Book and Attend'}
                                                        disabled={patientInfo && patientInfo.patientKey && !parseInt(patientInfo.deadInd) ? false : true}
                                                        onClick={() => {
                                                            this.props.openCommonCircular();
                                                            this.refs.form.submit();
                                                        }}
                                                    /> : null
                                            }
                                            {
                                                !this.props.isWalkIn ? (
                                                    this.props.isUpdating ? null :
                                                        <CIMSButton
                                                            id="btn_appointment_booking_booking"
                                                            variant="contained"
                                                            color="primary"
                                                            size="small"
                                                            style={{ marginRight: 0 }}
                                                            onClick={this.handleBookClick}
                                                            disabled={patientInfo && patientInfo.patientKey && !parseInt(patientInfo.deadInd) ? false : true}
                                                        >Book</CIMSButton>
                                                ) : null
                                            }
                                            {
                                                !this.props.isWalkIn ?
                                                    (
                                                        !this.props.isUpdating ?
                                                            null
                                                            :
                                                            <Grid>
                                                                <CIMSButton
                                                                    id="btn_appointment_booking_update_appointment"
                                                                    variant="contained"
                                                                    color="primary"
                                                                    size="small"
                                                                    style={{ marginRight: 0 }}
                                                                    disabled={((this.state.clinicCdEdit == true) && (this.state.encounterTypeCdEdit == true) && (this.state.subEncounterTypeCdEdit == true) &&
                                                                        (this.state.caseTypeCdEdit == true) && (this.state.appointmentTypeCdEdit == true) && (this.state.appointmentDateEdit == true) &&
                                                                        (this.state.appointmentTimeEdit == true) && (this.state.elapsedPeriodEdit == true) && (this.state.elapsedPeriodUnitEdit == true) &&
                                                                        (this.state.searchLogicCdEdit == true) && (this.state.bookingUnitEdit == true) && (this.state.remarkIdEdit == true) && (this.state.memoEdit == true))}
                                                                    onClick={this.handleUpdateAppointment}
                                                                    children={'Update'}
                                                                //style={{ display: this.props.isUpdating ? '' : 'none' }}
                                                                //disabled={patientInfo && patientInfo.patientKey && !parseInt(patientInfo.deadInd) ? false : true}
                                                                />
                                                                <CIMSButton
                                                                    id="btn_appointment_booking_update_appointment"
                                                                    variant="contained"
                                                                    color="primary"
                                                                    size="small"
                                                                    //style={{ marginRight: 0 }}
                                                                    onClick={() => {
                                                                        this.props.openCommonMessage({
                                                                            msgCode: '110235',
                                                                            btnActions: {
                                                                                btn1Click: () => {
                                                                                    this.props.cancelEditAppointment();
                                                                                    this.setState({
                                                                                        bookingData: {},
                                                                                        clinicCdEdit: true,
                                                                                        encounterTypeCdEdit: true,
                                                                                        subEncounterTypeCdEdit: true,
                                                                                        caseTypeCdEdit: true,
                                                                                        appointmentTypeCdEdit: true,
                                                                                        appointmentDateEdit: true,
                                                                                        appointmentTimeEdit: true,
                                                                                        elapsedPeriodEdit: true,
                                                                                        elapsedPeriodUnitEdit: true,
                                                                                        searchLogicCdEdit: true,
                                                                                        bookingUnitEdit: true,
                                                                                        remarkIdEdit: true,
                                                                                        memoEdit: true
                                                                                    });
                                                                                }
                                                                            }
                                                                        });
                                                                        this.props.updateState({ updateOrBookNew: '' });
                                                                    }}
                                                                    children={'Cancel'}
                                                                />
                                                            </Grid>
                                                    ) : null}
                                        </Grid>


                                        <SelectFieldValidator
                                            id="booking_select_appointment_booking_clinic"
                                            options={clinicList && clinicList.map(item => (
                                                { value: item.clinicCd, label: item.clinicName }
                                            ))}
                                            TextFieldProps={{
                                                variant: 'outlined',
                                                label: <>Clinic<RequiredIcon /></>
                                            }}
                                            value={bookingData.clinicCd}
                                            onChange={e => this.handleSelectFieldChange(e, 'clinicCd')}
                                            placeholder=""
                                            validators={[ValidatorEnum.required]}
                                            errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                            validatorListener={(...args) => this.validatorOtherListener(...args, 'clinicCd')}
                                            msgPosition="bottom"
                                        />
                                    </Grid>
                                    <Grid item container direction="column" className={classes.addEncounterContainer}>
                                        {
                                            bookingData && bookingData.encounterTypes && bookingData.encounterTypes.map((item, index) => (
                                                <Grid item container key={index}>
                                                    <Grid item container xs={this.props.isWalkIn ? 12 : 11}>
                                                        <CIMSFormLabel
                                                            fullWidth
                                                            labelText="Booking"
                                                            className={classes.addEncounterContainerItem}
                                                        >
                                                            <Grid item container direction="column" spacing={1}>
                                                                <Grid item container spacing={1}>
                                                                    <Grid item container xs={4} alignContent="center">
                                                                        <SelectFieldValidator
                                                                            id={'booking_select_appointment_booking_encounter_type_' + index}
                                                                            placeholder=""
                                                                            options={item.encounterTypeList && item.encounterTypeList.map(item => (
                                                                                { value: item.encounterTypeCd, label: item.encounterTypeCd, shortName: item.shortName }
                                                                            ))}
                                                                            TextFieldProps={{
                                                                                variant: 'outlined',
                                                                                label: <>Encounter<RequiredIcon /></>
                                                                            }}
                                                                            value={item.encounterTypeCd}
                                                                            onChange={e => this.handleEncounterSelectFieldChange(e, 'encounterTypeCd', index)}
                                                                            validators={[ValidatorEnum.required]}
                                                                            errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                                            validatorListener={(...args) => this.validatorListener(...args, 'Encounter Type', index)}
                                                                            notShowMsg
                                                                            ref={'encounterSelectField'}
                                                                        />
                                                                    </Grid>
                                                                    <Grid item container xs={3} alignContent="center">
                                                                        <SelectFieldValidator
                                                                            id={'booking_select_appointment_booking_sub_encounter_type_' + index}
                                                                            placeholder=""
                                                                            options={item.subEncounterTypeList && item.subEncounterTypeList.map(item => (
                                                                                { value: item.subEncounterTypeCd, label: item.subEncounterTypeCd, shortName: item.shortName }
                                                                            ))}
                                                                            TextFieldProps={{
                                                                                variant: 'outlined',
                                                                                label: <>Sub-encounter<RequiredIcon /></>
                                                                            }}
                                                                            value={item.subEncounterTypeCd}
                                                                            onChange={e => this.handleEncounterSelectFieldChange(e, 'subEncounterTypeCd', index)}
                                                                            validators={[ValidatorEnum.required]}
                                                                            errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                                            validatorListener={(...args) => this.validatorListener(...args, 'Sub Encounter Type', index)}
                                                                            notShowMsg
                                                                            ref={'subEncounterSelectField'}
                                                                        />
                                                                    </Grid>
                                                                    <Grid item container xs={5}>
                                                                        <CIMSFormLabel
                                                                            fullWidth
                                                                            labelText="Appointment Type"
                                                                            className={classes.formLabelContainer}
                                                                        >
                                                                            <Grid item container wrap="nowrap" spacing={1}>
                                                                                <Grid item xs={6}>
                                                                                    <SelectFieldValidator
                                                                                        id={'booking_select_appointment_booking_case_type_' + index}
                                                                                        options={Enum.APPOINTMENT_TYPE_PERFIX.map(item => (
                                                                                            { value: item.code, label: item.engDesc }
                                                                                        ))}
                                                                                        value={item.caseTypeCd || defaultCaseTypeCd}
                                                                                        placeholder=""
                                                                                        onChange={e => this.handleEncounterSelectFieldChange(e, 'caseTypeCd', index)}
                                                                                        validators={['required']}
                                                                                        errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                                                        validatorListener={(...args) => this.validatorListener(...args, 'Appointment Type First Select', index)}
                                                                                        notShowMsg
                                                                                    />
                                                                                </Grid>
                                                                                <Grid item xs={6}>
                                                                                    {
                                                                                        this.props.isWalkIn ?
                                                                                            ''
                                                                                            :
                                                                                            <SelectFieldValidator
                                                                                                id={'booking_select_appointment_booking_appointment_type_' + index}
                                                                                                options={this.state.quotaTypeList.map(item => (
                                                                                                    { value: item.code, label: item.engDesc }
                                                                                                ))}
                                                                                                value={item.appointmentTypeCd}
                                                                                                placeholder=""
                                                                                                onChange={e => this.handleEncounterSelectFieldChange(e, 'appointmentTypeCd', index)}
                                                                                                validators={['required']}
                                                                                                errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                                                                validatorListener={(...args) => this.validatorListener(...args, 'Appointment Type Second Select', index)}
                                                                                                notShowMsg
                                                                                            />
                                                                                    }
                                                                                </Grid>
                                                                            </Grid>
                                                                        </CIMSFormLabel>
                                                                    </Grid>
                                                                </Grid>
                                                                <Grid item container spacing={1}>
                                                                    <Grid item container xs={7}>
                                                                        <CIMSFormLabel
                                                                            fullWidth
                                                                            labelText="Date/Time"
                                                                            className={classes.formLabelContainer}
                                                                        >
                                                                            <Grid item container wrap="nowrap" spacing={1}>
                                                                                <Grid item container xs={7}>
                                                                                    <DateFieldValidator
                                                                                        style={{ width: 'inherit' }}
                                                                                        id={'booking_date_appointment_booking_encounter_date_' + index}
                                                                                        disablePast
                                                                                        value={item.appointmentDate}
                                                                                        placeholder=""
                                                                                        onChange={e => this.handleEncounterDateFieldChange(e, 'appointmentDate', index)}
                                                                                        disabled={this.props.isWalkIn}
                                                                                        isRequired={!item.elapsedPeriod}
                                                                                        validatorListener={(...args) => this.validatorListener(...args, 'Appointment Date', index)}
                                                                                        notShowMsg
                                                                                    />
                                                                                </Grid>
                                                                                <Grid item container xs={5}>
                                                                                    {
                                                                                        !this.props.isWalkIn ?
                                                                                            <TimeFieldValidator
                                                                                                id={'booking_time_appointment_booking_encounter_time_' + index}
                                                                                                helperText=""
                                                                                                // value={item.appointmentTime&&moment(item.appointmentTime, Enum.DATE_FORMAT_24_HOUR)}
                                                                                                value={item.appointmentTime}
                                                                                                placeholder=""
                                                                                                onChange={e => this.handleEncounterTimeOnChange(e, 'appointmentTime', index, item.appointmentDate)}
                                                                                                disabled={this.props.isWalkIn}
                                                                                                validators={item.elapsedPeriod ? [] : [ValidatorEnum.required]}
                                                                                                errorMessages={item.elapsedPeriod ? [] : [CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                                                                validatorListener={(...args) => this.validatorListener(...args, 'Appointment Time', index)}
                                                                                                notShowMsg
                                                                                            /> : null
                                                                                    }
                                                                                </Grid>
                                                                            </Grid>
                                                                        </CIMSFormLabel>
                                                                    </Grid>
                                                                    {
                                                                        !this.props.isWalkIn ?
                                                                            <Grid item container xs={5}>
                                                                                <CIMSFormLabel
                                                                                    fullWidth
                                                                                    labelText="Elapsed Period"
                                                                                    className={classes.formLabelContainer}
                                                                                >
                                                                                    <Grid item container wrap="nowrap" spacing={1}>
                                                                                        <Grid item xs={5}>
                                                                                            <TextFieldValidator
                                                                                                id={'booking_txt_appointment_booking_elapsed_period_num_' + index}
                                                                                                fullWidth
                                                                                                value={item.elapsedPeriod}
                                                                                                onChange={e => this.handleEncounterTextFieldChange(e, 'elapsedPeriod', index)}
                                                                                                validators={item.appointmentDate || item.appointmentTime ? [] : [ValidatorEnum.required, ValidatorEnum.isPositiveInteger]}
                                                                                                errorMessages={item.appointmentDate || item.appointmentTime ? [] : [CommonMessage.VALIDATION_NOTE_REQUIRED(), CommonMessage.VALIDATION_NOTE_POSITIVE_INTEGER()]}
                                                                                                validatorListener={(...args) => this.validatorListener(...args, 'Elapsed Period', index)}
                                                                                                notShowMsg
                                                                                                inputProps={{
                                                                                                    maxLength: 2
                                                                                                }}
                                                                                                disabled={this.props.isWalkIn}
                                                                                            />
                                                                                        </Grid>
                                                                                        <Grid item xs={7}>
                                                                                            <SelectFieldValidator
                                                                                                id={'booking_select_appointment_booking_elapsed_period_type_' + index}
                                                                                                options={Enum.ELAPSED_PERIOD_TYPE.map(item => (
                                                                                                    { value: item.code, label: item.engDesc }
                                                                                                ))}
                                                                                                fullWidth
                                                                                                placeholder=""
                                                                                                value={item.elapsedPeriodUnit}
                                                                                                onChange={e => this.handleEncounterSelectFieldChange(e, 'elapsedPeriodUnit', index)}
                                                                                                validators={item.elapsedPeriod ? [ValidatorEnum.required] : []}
                                                                                                errorMessages={item.elapsedPeriod ? [CommonMessage.VALIDATION_NOTE_REQUIRED()] : []}
                                                                                                validatorListener={(...args) => this.validatorListener(...args, 'Elapsed Period Unit', index)}
                                                                                                notShowMsg
                                                                                            />
                                                                                        </Grid>
                                                                                    </Grid>
                                                                                </CIMSFormLabel>
                                                                            </Grid>
                                                                            : null
                                                                    }
                                                                </Grid>
                                                                {
                                                                    !this.props.isWalkIn ?
                                                                        <>
                                                                            <Grid item container spacing={1}>
                                                                                <Grid item container xs={6}>
                                                                                    <SelectFieldValidator
                                                                                        id={'booking_select_appointment_booking_searchLogic_' + index}
                                                                                        TextFieldProps={{
                                                                                            variant: 'outlined',
                                                                                            label: <>Search Logic<RequiredIcon /></>
                                                                                        }}
                                                                                        placeholder="W (Whole day)"
                                                                                        value={item.searchLogicCd}
                                                                                        options={this.props.searchLogicList && this.props.searchLogicList.map(item => (
                                                                                            { value: item.slotSearchLogicCd, label: `${item.slotSearchLogicCd} (${item.description})` }
                                                                                        ))}
                                                                                        validators={[ValidatorEnum.required]}
                                                                                        errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                                                        validatorListener={(...args) => this.validatorListener(...args, 'Search Logic', index)}
                                                                                        onChange={e => this.handleEncounterSelectFieldChange(e, 'searchLogicCd', index)}
                                                                                        notShowMsg
                                                                                    />
                                                                                </Grid>
                                                                                <Grid item container xs={6}>
                                                                                    <TextFieldValidator
                                                                                        id={'booking_select_appointment_booking_booking_unit_' + index}
                                                                                        value={item.bookingUnit}
                                                                                        onChange={e => this.handleEncounterTextFieldChange(e, 'bookingUnit', index)}
                                                                                        onBlur={e => this.handleEncounterTextFieldOnBlur(e, 'bookingUnit', index)}
                                                                                        validators={[ValidatorEnum.isPositiveInteger]}
                                                                                        errorMessages={[CommonMessage.VALIDATION_NOTE_POSITIVE_INTEGER()]}
                                                                                        validatorListener={(...args) => this.validatorListener(...args, 'Booking Unit', index)}
                                                                                        notShowMsg
                                                                                        inputProps={{
                                                                                            maxLength: 1
                                                                                        }}
                                                                                        label={<>Booking Unit<RequiredIcon /></>}
                                                                                    />
                                                                                </Grid>
                                                                            </Grid>
                                                                            <Grid item container spacing={1}>
                                                                                <Grid item container>
                                                                                    <SelectFieldValidator
                                                                                        id={'booking_select_appointment_booking_remarkCode_' + index}
                                                                                        value={item.remarkId}
                                                                                        options={this.props.remarkCodeList && this.props.remarkCodeList.map(item => (
                                                                                            { value: item.remarkId, label: `${item.remarkCd} (${item.description})` }
                                                                                        ))}
                                                                                        TextFieldProps={{
                                                                                            variant: 'outlined',
                                                                                            label: 'Remark'
                                                                                        }}
                                                                                        addNullOption
                                                                                        onChange={e => this.handleEncounterSelectFieldChange(e, 'remarkId', index)}
                                                                                    />
                                                                                </Grid>
                                                                            </Grid>
                                                                            <Grid item container spacing={1}>
                                                                                <Grid item container>
                                                                                    <CIMSMultiTextField
                                                                                        id={'booking_select_appointment_booking_memo_' + index}
                                                                                        fullWidth
                                                                                        value={item.memo || bookingData.memo}
                                                                                        inputProps={{
                                                                                            maxLength: 500
                                                                                        }}
                                                                                        label="Memo"
                                                                                        calActualLength
                                                                                        rows="4"
                                                                                        onChange={e => this.handleEncounterTextFieldChange(e, 'memo', index)}
                                                                                    />
                                                                                </Grid>
                                                                            </Grid>
                                                                        </> : null
                                                                }
                                                                {
                                                                    errorMessageList[index] && errorMessageList[index].length > 0 ?
                                                                        <Grid item container style={{ marginTop: '10px' }} id={'booking_invalid_input_error_message'}>
                                                                            {
                                                                                errorMessageList[index].map((item, i) => (
                                                                                    <Grid item container key={i} justify="flex-start">
                                                                                        <Typography className={classes.errorFieldNameText}>{`${item.fieldName}: ${item.errMsg}`}</Typography>
                                                                                    </Grid>
                                                                                ))
                                                                            }
                                                                        </Grid> : null
                                                                }
                                                            </Grid>
                                                        </CIMSFormLabel>
                                                    </Grid>
                                                    {
                                                        !this.props.isWalkIn ?
                                                            <Grid item container xs={1} alignContent="center">
                                                                {
                                                                    bookingData && bookingData.encounterTypes && bookingData.encounterTypes.length > 1 ?
                                                                        <IconButton
                                                                            onClick={e => this.handleRemoveEncounter(e, index)}
                                                                            id="booking_appointment_btnRemove"
                                                                            className={classes.iconButton}
                                                                            color="secondary"
                                                                            title="Remove"
                                                                        >
                                                                            <RemoveCircle />
                                                                        </IconButton> : null
                                                                }
                                                                <IconButton
                                                                    onClick={this.handleAddEncounter}
                                                                    id="booking_appointment_btnAdd"
                                                                    title="Add"
                                                                    color="primary"
                                                                    className={classes.iconButton}
                                                                >
                                                                    <AddCircle />
                                                                </IconButton>
                                                            </Grid> : null
                                                    }
                                                </Grid>
                                            ))}
                                            {
                                                showEcsBtnInBooking?(
                                                    <Grid item container>
                                                    <Grid item container xs={this.props.isWalkIn ? 12 : 11}>

                                                    <Box width={1} pt={1}>
                                                    <Box display="flex" >
                                                        <Box pr={1}>
                                                        <CIMSButton
                                                            disabled={!EcsUtilities.isEcsEnable(
                                                        accessRights,
                                                        patientInfo.documentPairList.map(item => item.docTypeCd),
                                                        ecsUserId,
                                                        ecsLocCode,
                                                        false,
                                                        ecsServiceStatus ,
                                                        EcsUtilities.getProperHkicForEcs(patientInfo))}
                                                            style={{ padding: '0px', margin: '0px' }}
                                                            onClick={(e) => {
                                                            openEcsDialog({
                                                                docTypeCd: EcsUtilities.getProperDocTypeCdForEcs(patientInfo),
                                                                disableMajorKeys: true,
                                                                engSurname: patientInfo.engSurname,
                                                                engGivename: patientInfo.engGivename,
                                                                chineseName: patientInfo.chineseName,
                                                                cimsUser: ecsUserId,
                                                                locationCode: ecsLocCode,
                                                                patientKey: patientInfo.patientKey,
                                                                appointmentId: appointmentInfo ? appointmentInfo.appointmentId: null,
                                                                hkid: EcsUtilities.getProperHkicForEcs(patientInfo),
                                                                dob: patientInfo.dob,
                                                                exactDob:patientInfo.exactDobCd
                                                            },
                                                            null,
                                                            setEcsPatientStatus);
                                                        }}
                                                        >ECS</CIMSButton>
                                                        </Box>
                                                        <Box  flexGrow={1}>
                                                        <EcsResultTextField  ecsStore={selectedPatientEcsStatus} fullWidth ></EcsResultTextField>

                                                        </Box>
                                                    </Box>
                                                </Box>
                                                    </Grid>

                                                </Grid>
                                                ):(
                                                    null
                                                )
                                            }

                                    </Grid>
                                </ValidatorForm>
                            </Grid>
                            {this.props.isWalkIn === true
                                ? <Grid item container>
                                    <WalkInAttendanceInfo
                                        id={'walkIn_appointment_Info'}
                                        patientStatusList={this.props.patientStatusList}
                                        handleWalkInInfoChange={this.handleWalkInInfoChange}
                                        walkInInfo={this.props.walkInAttendanceInfo}
                                        // patientStatusFlag={patientStatusFlag}
                                        patientStatus={caseNoInfo.patientStatus}
                                    />
                                </Grid>
                                : null}
                        </Grid>
                }

                <CIMSDialog id="appointment_booking_dialog1" dialogTitle="Appointment Booking" open={this.props.openConfirmDialog && bookTimeSlotData.length === 1} dialogContentProps={{ style: { minWidth: 400 } }}>
                    <DialogContent id={'booking_appointment_booking_dialog1_description'} style={{ padding: 0 }}>
                        {/* {this.state.failSearchExactTime ? <Typography variant="subtitle2" className={classes.dialogErrorContent}>{this.state.failSearchReason}</Typography> : null} */}
                        {bookTimeSlotData && bookTimeSlotData[0] && (bookTimeSlotData[0].dateDiff > 0) ?
                            <Typography variant="subtitle2" className={classes.dialogErrorContent}>
                                {/* {`Available date is ${bookTimeSlotData[0].timeDiff} ${bookTimeSlotData[0].timeDiffUnit} later than given date`} */}
                                {`Available date is ${bookTimeSlotData[0].dateDiff} Day(s) later than given date`}
                            </Typography> : null}
                        {bookTimeSlotData && bookTimeSlotData[0] ?
                            <Typography component="div">
                                <Typography variant="subtitle1" className={classes.dialogContent}>
                                    {clinicList.find(item => item.clinicCd === bookingData.clinicCd).clinicName}
                                </Typography>
                                <Typography variant="subtitle1" className={classes.dialogContent}>
                                    {/* {bookTimeSlotData[0].encounterTypeDesc} */}
                                    {`${bookingData.encounterTypes[0].encounterTypeCd} - ${bookingData.encounterTypes[0].subEncounterTypeCd}`}
                                </Typography>
                                <Typography variant="subtitle1" className={classes.dialogContent}>
                                    {/* {`Date: ${moment(bookTimeSlotData[0].slotDate).format(Enum.DATE_FORMAT_EDMY_VALUE)} ${bookTimeSlotData[0].startTime}`} */}
                                    {
                                        AppointmentUtilities.combineApptDateAndTime(
                                            {
                                                appointmentDate: moment(bookTimeSlotData[0].slotDate).format(Enum.DATE_FORMAT_EDMY_VALUE),
                                                appointmentTime: bookTimeSlotData[0].startTime
                                            },
                                            null,
                                            Enum.TIME_FORMAT_12_HOUR_CLOCK)
                                    }
                                </Typography>
                            </Typography> : null
                        }
                    </DialogContent>
                    <DialogActions className={classes.dialogAction}>
                        <CIMSButton onClick={this.handleBookConfirm} id={'booking_appointment_booking_dialog1_confirm'} color="primary" disabled={this.props.handlingBooking}>Confirm</CIMSButton>
                        <CIMSButton onClick={(...args) => this.handleBookSearch(...args, 0)} color="primary" id={'booking_appointment_booking_dialog1_search'} style={{ display: this.props.openTimeSlotModal ? 'none' : '' }}>Search</CIMSButton>
                        <CIMSButton onClick={this.handleBookCancel} color="primary" id={'booking_appointment_booking_dialog1_cancel'}>Cancel</CIMSButton>
                    </DialogActions>
                </CIMSDialog>

                <CIMSPromptDialog
                    open={this.state.contactHistoryOpen}
                    id={'contactHistory'}
                    dialogTitle={'Contact History'}
                    paperStyl={this.props.classes.paper}
                    dialogContentText={
                        <div>
                            <Grid
                                container
                                item
                                alignItems="center"
                                wrap="nowrap"
                                className={classes.gridTitle}
                                xs={8}
                            >
                                <Grid container xs={8}>
                                    <Typography className={classes.maintitleRoot}>Appointment Date/Time: {' '}</Typography>
                                    <Typography className={classes.maintitleRoot} style={{ fontWeight: 'normal' }}>&nbsp;{`${moment(this.state.rowData.appointmentDate, Enum.DATE_FORMAT_EDMY_VALUE).format(Enum.DATE_FORMAT_EDMY_VALUE)} ${moment(this.state.rowData.appointmentTime, Enum.TIME_FORMAT_24_HOUR_CLOCK).format(Enum.TIME_FORMAT_24_HOUR_CLOCK)}`}</Typography>
                                </Grid>
                                <Grid container xs={4} justify={'flex-end'}>
                                    <CIMSButton
                                        id="booking_contactHistory_dialog_editBtn"
                                        classes={{ sizeSmall: classes.buttonRoot }}
                                        disabled={this.state.currentSelectedContact == null || this.state.currentSelectedContactInfo != null}
                                        onClick={() => {
                                            if (!this.state.currentSingleSelectedContactInfo) {
                                                return;
                                            }
                                            this.setState({
                                                currentSelectedContactInfo: this.state.currentSingleSelectedContactInfo,
                                                appointmentId: this.state.currentSingleSelectedContactInfo.appointmentId,
                                                callerName: this.state.currentSingleSelectedContactInfo.callerName,
                                                NotificationDate: moment(this.state.currentSingleSelectedContactInfo.notificationDtm, Enum.DATE_FORMAT_24_HOUR),
                                                NotificationTime: moment(this.state.currentSingleSelectedContactInfo.notificationDtm, Enum.DATE_FORMAT_24_HOUR),
                                                contactType: this.state.currentSingleSelectedContactInfo.contactType,
                                                note: this.state.currentSingleSelectedContactInfo.notes
                                            });

                                            if (this.state.currentSingleSelectedContactInfo.contactType == 'Tel') {

                                                this.setState({ tel: this.state.currentSingleSelectedContactInfo.contactDesc });
                                            } else if (this.state.currentSingleSelectedContactInfo.contactType == 'Fax') {
                                                this.setState({ fax: this.state.currentSingleSelectedContactInfo.contactDesc });
                                            } else if (this.state.currentSingleSelectedContactInfo.contactType == 'Mail') {
                                                this.setState({ email: this.state.currentSingleSelectedContactInfo.contactDesc });
                                            }
                                            this.setState({ currentSingleSelectedContactInfo: null });
                                        }}
                                    >Edit</CIMSButton>
                                    <CIMSButton
                                        id="booking_contactHistory_dialog_deleteBtn"
                                        classes={{ sizeSmall: classes.buttonRoot }}
                                        onClick={() => {
                                            if (this.state.currentSingleSelectedContactInfo) {
                                                let params = {
                                                    appointmentId: this.state.currentSingleSelectedContactInfo.appointmentId,
                                                    callerName: this.state.currentSingleSelectedContactInfo.callerName,
                                                    contactDesc: this.state.currentSingleSelectedContactInfo.contactDesc,
                                                    contactHistoryId: this.state.currentSingleSelectedContactInfo.contactHistoryId,
                                                    contactType: this.state.currentSingleSelectedContactInfo.contactType,
                                                    notes: this.state.currentSingleSelectedContactInfo.notes,
                                                    notificationDtm: this.state.currentSingleSelectedContactInfo.notificationDtm,
                                                    statusCd: 'D',
                                                    version: this.state.currentSingleSelectedContactInfo.version
                                                };
                                                this.props.updateContatHistory(params, () => {
                                                    this.props.listContatHistory(this.state.rowData.appointmentId);
                                                    this.setState({ currentSingleSelectedContactInfo: null });
                                                });
                                            }
                                        }}
                                        disabled={this.state.currentSelectedContact == null || this.state.currentSelectedContactInfo != null}
                                    >Delete</CIMSButton>
                                </Grid>

                            </Grid>

                            <Grid container spacing={1} className={classes.root}>
                                <Grid item container xs={8}>

                                    <Grid container className={classes.marginTop20}>
                                        <CIMSTable
                                            tableMaxHeight={450}
                                            id={'ContactHistory_table'}
                                            innerRef={ref => this.tableRef = ref}
                                            rows={this.state.tableRows}
                                            options={this.state.tableOptions}
                                            data={this.props.contactList}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid item container direction="column" xs={4}>
                                    <ValidatorForm ref="contact" onSubmit={this.handleCotactUpdate} >
                                        <Grid item container style={{ marginTop: 5, marginBottom: 20 }}>
                                            <TextFieldValidator
                                                id={'booking_select_appointment_booking_contactHistory_callerName'}
                                                value={this.state.callerName}
                                                onChange={e => this.handleContactFieldChange('callerName', e)}
                                                onBlur={e => this.handleContactFieldChange('callerName', e)}
                                                validators={[ValidatorEnum.isSpecialEnglish, ValidatorEnum.required]}
                                                errorMessages={[CommonMessage.VALIDATION_NOTE_SPECIAL_ENGLISH()]}
                                                msgPosition="bottom"
                                                disabled
                                                label={<>Caller Name</>}
                                            />
                                        </Grid>
                                        <Grid item container spacing={1} style={{ marginBottom: 20 }}>
                                            <Grid item container xs={6}>
                                                <DateFieldValidator
                                                    label={<>Notification Date<RequiredIcon /></>}
                                                    isRequired
                                                    style={{ width: 'inherit' }}
                                                    id={'booking_select_appointment_booking_contactHistory_notificationDate'}
                                                    value={this.state.NotificationDate}
                                                    placeholder=""
                                                    onChange={e => this.handleContactDateFieldChange('NotificationDate', e)}
                                                    validators={[ValidatorEnum.required]}
                                                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                    msgPosition="bottom"
                                                />


                                            </Grid>
                                            <Grid item container xs={6} >
                                                <TimeFieldValidator
                                                    label={<>Notification Time<RequiredIcon /></>}
                                                    isRequired
                                                    id={'booking_select_appointment_booking_contactHistory_notificationTime'}
                                                    helperText=""
                                                    value={this.state.NotificationTime}
                                                    placeholder=""
                                                    onChange={e => this.handleContactDateFieldChange('NotificationTime', e)}
                                                    validators={[ValidatorEnum.required]}
                                                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                    msgPosition="bottom"
                                                />
                                            </Grid>
                                        </Grid>
                                        <OutlinedRadioValidator
                                            id={'booking_select_appointment_booking_contactHistory_contactType'}
                                            labelText="Contact Type"
                                            isRequired
                                            value={this.state.contactType}
                                            onChange={e => this.handleContactFieldChange('contactType', e)}
                                            list={[{ label: 'Tel', value: 'Tel' }, { label: 'Fax', value: 'Fax' }, { label: 'Mail', value: 'Mail' }]
                                            }
                                            validators={[ValidatorEnum.required]}
                                            errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                            RadioGroupProps={{ className: classes.radioGroup }}
                                            style={{ marginBottom: 20 }}
                                        />
                                        {
                                            this.state.contactType === 'Tel' ?
                                                <Grid item container style={{ marginBottom: 20 }}>
                                                    <TextFieldValidator
                                                        type="number"
                                                        id={'booking_select_appointment_booking_contactHistory_tel'}
                                                        value={this.state.tel}
                                                        onChange={e => this.handleContactFieldChange('tel', e)}
                                                        onBlur={e => this.handleContactFieldChange('tel', e)}
                                                        validators={[ValidatorEnum.isNumber, ValidatorEnum.required]}
                                                        errorMessages={[CommonMessage.VALIDATION_NOTE_NUMBERFIELD(), CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                        msgPosition="bottom"
                                                        inputProps={{
                                                            maxLength: 15
                                                        }}
                                                        label={<>Tel<RequiredIcon /></>}
                                                    /></Grid> : null
                                        }
                                        {
                                            this.state.contactType === 'Fax' ? <Grid item container style={{ marginBottom: 20 }}>
                                                <TextFieldValidator
                                                    type="number"
                                                    id={'booking_select_appointment_booking_contactHistory_fax'}
                                                    value={this.state.fax}
                                                    onChange={e => this.handleContactFieldChange('fax', e)}
                                                    onBlur={e => this.handleContactFieldChange('fax', e)}
                                                    validators={[ValidatorEnum.isNumber, ValidatorEnum.required]}
                                                    errorMessages={[CommonMessage.VALIDATION_NOTE_NUMBERFIELD(), CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                    msgPosition="bottom"
                                                    inputProps={{
                                                        maxLength: 15
                                                    }}
                                                    label={<>Fax<RequiredIcon /></>}
                                                /></Grid> : null
                                        }
                                        {
                                            this.state.contactType === 'Mail' ? <Grid item container style={{ marginBottom: 20 }}>
                                                <TextFieldValidator
                                                    id={'booking_select_appointment_booking_contactHistory_mail'}
                                                    value={this.state.email}
                                                    onChange={e => this.handleContactFieldChange('email', e)}
                                                    onBlur={e => this.handleContactFieldChange('email', e)}
                                                    validators={[ValidatorEnum.required]}
                                                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                    msgPosition="bottom"
                                                    inputProps={{
                                                        maxLength: 20
                                                    }}
                                                    label={<>Mail Adress<RequiredIcon /></>}
                                                /> </Grid> : null
                                        }

                                        <Grid item container spacing={1} >
                                            <Grid item container>
                                                <CIMSMultiTextField
                                                    id={'booking_select_appointment_booking_contactHistory_note'}
                                                    fullWidth
                                                    value={this.state.note}
                                                    inputProps={{
                                                        maxLength: 166
                                                    }}
                                                    label="Notes"
                                                    calActualLength
                                                    rows="4"
                                                    onChange={e => this.handleContactFieldChange('note', e)}
                                                />
                                            </Grid>
                                        </Grid>
                                    </ValidatorForm>

                                </Grid>

                            </Grid>
                        </div>
                    }
                    buttonConfig={
                        this.state.currentSelectedContactInfo == null ?
                            [
                                {
                                    id: 'contactHistory_add',
                                    name: 'Add',
                                    onClick: () => {
                                        this.refs.contact.submit();

                                    }
                                },
                                {
                                    id: 'contactHistory_close',
                                    name: 'Close',
                                    onClick: () => {
                                        this.clearContactListSelected();
                                        this.refs.historyGrid.refreshListAppointment();
                                        this.setState({ contactHistoryOpen: false });
                                        this.props.clearContactList();
                                    }
                                }
                            ] : [
                                {
                                    id: 'contactHistory_save',
                                    name: 'Save',
                                    onClick: () => {
                                        this.refs.contact.submit();

                                    }
                                },
                                {
                                    id: 'contactHistory_cancel',
                                    name: 'Cancel',
                                    onClick: () => {

                                        this.props.openCommonMessage({
                                            msgCode: '110235',
                                            btnActions: {
                                                btn1Click: () => {
                                                    this.clearContactListSelected();
                                                }
                                            }
                                        });

                                    }
                                }
                            ]
                    }
                />
                <EditModeMiddleware componentName={AccessRightEnum.booking} when={!this.props.bookingConfirmed || this.props.isUpdating === true} />
            </Grid>
        );
    }
}

const mapStatetoProps = (state) => {
    return ({
        serviceCd: state.login.service.serviceCd,
        clinicCd: state.login.clinic.clinicCd,
        timeSlotList: state.bookingInformation.timeSlotList,
        openTimeSlotModal: state.bookingInformation.openTimeSlotModal,
        openConfirmDialog: state.bookingInformation.openConfirmDialog,
        bookingData: state.bookingInformation.bookingData,
        clinicList: state.bookingInformation.clinicList || null,
        bookTimeSlotData: state.bookingInformation.bookTimeSlotData,
        patientInfo: state.patient.patientInfo,
        appointmentInfo: state.patient.appointmentInfo,
        caseNoInfo: state.patient.caseNoInfo,
        bookingConfirmed: state.bookingInformation.bookingConfirmed,
        changeEncounterIndex: state.bookingInformation.changeEncounterIndex,
        appointmentList: state.bookingInformation.appointmentList,
        patientStatusList: state.attendance.patientStatusList,
        walkInAttendanceInfo: state.bookingInformation.walkInAttendanceInfo,
        clinicConfig: state.common.clinicConfig,
        defaultEncounterCd: state.bookingInformation.defaultEncounterCd,
        handlingBooking: state.bookingInformation.handlingBooking,
        waitingList: state.booking.waitingList,
        searchLogicList: state.bookingInformation.searchLogicList,
        remarkCodeList: state.bookingInformation.remarkCodeList,
        openApptCancelQueDialog: state.bookingInformation.openApptCancelQueDialog,
        currentSelectedApptInfo: state.bookingInformation.currentSelectedApptInfo,
        isUpdating: state.bookingInformation.isUpdating,
        confirmData: state.bookingInformation.confirmData,
        updateOrBookNew: state.bookingInformation.updateOrBookNew,
        contactList: state.bookingInformation.contactList,
        loginName: state.login.loginInfo.loginName,

        accessRights: state.login.accessRights,
        ecsUserId: state.login.loginInfo.ecsUserId,
        ecsLocCode: state.login.clinic.ecsLocCode,
        ecsServiceStatus: state.ecs.ecsServiceStatus,
        selectedPatientEcsStatus: state.ecs.selectedPatientEcsStatus,
        showEcsBtnInBooking: state.ecs.showEcsBtnInBooking,

        defaultCaseTypeCd: state.bookingInformation.defaultCaseTypeCd
    });
};

const mapDispatchtoProps = {
    resetInfoAll,
    appointmentBook,
    listTimeSlot,
    listAppointment,
    getEncounterTypeList,
    getClinicList,
    bookConfirm,
    bookConfirmWaiting,
    updateState,
    openCommonCircular,
    closeCommonCircular,
    getAppointmentReport,
    getPatientStatusList,
    walkInAttendance,
    updateField,
    listSeachLogic,
    listRemarkCode,
    cancelAppointment,
    editAppointment,
    cancelEditAppointment,
    submitUpdateAppointment,
    updatePatientState,
    openCommonMessage,
    openCaseNoDialog,
    getPatientAppointment,
    getPatientEncounter,
    updateCurTab,
    deleteSubTabs,
    selectCaseTrigger,
    getPatientPanelPatientById,
    getEncounterType,
    listContatHistory,
    insertContatHistory,
    updateContatHistory,
    getCodeList,
    refreshServiceStatus,
    clearContactList,
    openEcsDialog,
    listToalAppointment
};

export default connect(mapStatetoProps, mapDispatchtoProps)(withStyles(styles)(BookingInformation));