import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { Grid } from '@material-ui/core';
import {
    resetAll,
    destroyPage,
    updateField,
    fillingData,
    loadAppointmentBooking
} from '../../../store/actions/appointment/booking/bookingAnonymousAction';
import { getCodeList } from '../../../store/actions/patient/patientAction';
import * as BookingAnonymousType from '../../../store/actions/appointment/booking/bookingAnonymousActionType';
import AnonymousPatientBar from './component/anonymousPatientBar';
import BookingAnonymousInformation from './bookingAnonymousInformation';
// import CalendarView from './calendarView';
import _ from 'lodash';
import { initBookingData, initEncounterType } from '../../../constants/appointment/bookingInformation/bookingInformationConstants';
import { codeList } from '../../../constants/codeList';
import * as commonUtilities from '../../../utilities/commonUtilities';
import * as appointmentUtilities from '../../../utilities/appointmentUtilities';
import Enum from '../../../enums/enum';
import moment from 'moment';

const styles = () => ({
    root: {
        width: '100%',
        overflow: 'initial'
    }
});

class BookingAnonymous extends Component {

    UNSAFE_componentWillMount() {
        this.props.resetAll();
    }

    componentDidMount() {
        this.props.ensureDidMount();
        let codePara = [
            codeList.document_type
        ];
        this.props.getCodeList(codePara, BookingAnonymousType.PUT_GET_CODE_LIST);
        let where = { serviceCd: this.props.serviceCd, clinicCd: this.props.clinicCd };
        let defaultEncounterCd = commonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.DEFAULT_ENCOUNTER_CD, this.props.clinicConfig, where);
        this.props.updateField({ clinicValue: this.props.clinicCd, encounterTypeValue: defaultEncounterCd.configValue });
        let bookData = _.cloneDeep(initBookingData);
        if (this.props.location.state) {
            let params = _.cloneDeep(this.props.location.state);
            if (params.waitingListId) {
                let newEncounterType = _.cloneDeep(initEncounterType);
                newEncounterType.encounterTypeCd = params.encounterTypeCd;
                newEncounterType.appointmentDate = moment().format(Enum.DATE_FORMAT_EYMD_VALUE);
                newEncounterType.appointmentTime = moment().format(Enum.TIME_FORMAT_24_HOUR_CLOCK);
                let memoStrList = [];
                params.countryList ? memoStrList.push(params.countryList.replace(/,/g, ' | ')) : null;
                params.travelDtm ? memoStrList.push('Date:' + params.travelDtm) : null;
                params.remarks ? memoStrList.push('Remark:' + params.remarks): null;
                newEncounterType.memo = memoStrList.join(' | ');
                bookData.clinicCd = params.clinicCd;
                bookData.encounterTypes[0] = newEncounterType;
                params.showMakeAppointmentView = true;
                let engName = params.englishName.split(',');
                params.anonyomousBookingActiveInfo = {
                    patientKey: params.patientKey,
                    docTypeCd: params.docTypeCd,
                    docNO: params.hkidOrDocNo,
                    surname: engName.length > 0 ? engName[0] : '',
                    givenName: engName.length > 1 ? engName[1] : '',
                    countryCd: params.countryCd,
                    mobile: params.phoneNo,
                    isHKIDValid: params.docTypeCd === 'ID'
                };
                params.waitingList = {
                    version: params.version,
                    waitingListId: params.waitingListId
                };
                params.patientInfo = {
                    patientKey: params.patientKey,
                    engSurname: engName.length > 0 ? engName[0] : '',
                    engGivename: engName.length > 1 ? engName[1] : ''
                };
            } else {
                bookData = { ...bookData, ...params.bookData };
            }
            this.props.updateField(params);
        }

        //set elapsedPeriod when appointment date and time is null
        bookData = appointmentUtilities.getDefaultElapsedPeriodBookData(bookData);

        //default clinic code
        if (!bookData.clinicCd) {
            bookData.clinicCd = this.props.clinicCd;
        }

        this.props.loadAppointmentBooking(bookData);
    }

    componentWillUnmount() {
        this.props.destroyPage();
    }

    // prepareBookingData = (data) => {
    //     let bookData = _.cloneDeep(initBookingData);
    //     let newEncounterType = _.cloneDeep(initEncounterType);
    //     newEncounterType.encounterTypeCd = this.props.encounterTypeValue;
    //     newEncounterType.subEncounterTypeCd = data.subEncounterType;
    //     newEncounterType.appointmentDate = data.slotByDateDto.date;
    //     newEncounterType.appointmentTime = data.slotByDateDto.time || '';
    //     bookData.clinicCd = this.props.clinicValue;
    //     bookData.encounterTypes[0] = newEncounterType;
    //     return bookData;
    // }

    // calendarOnClick = (e, data) => {
    //     let fileData = {};
    //     let tempBookingData = this.prepareBookingData(data);
    //     fileData['bookData'] = tempBookingData;
    //     fileData['showMakeAppointmentView'] = true;
    //     this.props.updateField(fileData);
    // }

    handleAnonymousInfoChange = (patient) => {
        let field = {};
        field.anonyomousBookingActiveInfo = patient;
        this.props.updateField(field);
    }

    fillingPatientInfo = (fillingData) => {
        this.props.fillingData(fillingData);
    }

    render() {
        const { classes } = this.props;
        return (
            <Grid className={classes.root}>
                <AnonymousPatientBar
                    id={'bookingAnonymousPatientBar'}
                    codeList={this.props.codeList}
                    countryList={this.props.countryList}
                    patient={this.props.anonymousPatint}
                    anonymousInfoOnchange={this.handleAnonymousInfoChange}
                    fillingPatientInfo={this.fillingPatientInfo}
                />
                <BookingAnonymousInformation />
                {/* {
                    this.props.showMakeAppointmentView ?
                        <BookingAnonymousInformation bookData={this.props.bookData} />
                        :
                        <CalendarView
                            calendarOnClick={this.calendarOnClick}
                            requestData={(...args) => this.props.requestData(...args)}
                            updateField={e => this.props.updateField(e)}
                            calendarViewValue={this.props.calendarViewValue}
                            availableQuotaValue={this.props.availableQuotaValue}
                            showAppointmentRemarkValue={this.props.showAppointmentRemarkValue}
                            clinicValue={this.props.clinicValue}
                            encounterTypeValue={this.props.encounterTypeValue}
                            selectEncounterType={this.props.selectEncounterType}
                            subEncounterTypeValue={this.props.subEncounterTypeValue}
                            dateFrom={this.props.dateFrom}
                            dateTo={this.props.dateTo}
                            clinicListData={this.props.clinicListData}
                            encounterTypeListData={this.props.encounterTypeListData}
                            subEncounterTypeListData={this.props.subEncounterTypeListData}
                            calendarData={this.props.calendarData}
                            subEncounterTypeListKeyAndValue={this.props.subEncounterTypeListKeyAndValue}
                        />
                } */}
            </Grid>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        serviceCd: state.login.service.serviceCd,
        clinicCd: state.login.clinic.clinicCd,
        clinicConfig: state.common.clinicConfig,
        showMakeAppointmentView: state.bookingAnonymous.showMakeAppointmentView,
        calendarViewValue: state.bookingAnonymous.calendarViewValue,
        availableQuotaValue: state.bookingAnonymous.availableQuotaValue,
        showAppointmentRemarkValue: state.bookingAnonymous.showAppointmentRemarkValue,
        clinicValue: state.bookingAnonymous.clinicValue,
        encounterTypeValue: state.bookingAnonymous.encounterTypeValue,
        selectEncounterType: state.bookingAnonymous.selectEncounterType,
        subEncounterTypeValue: state.bookingAnonymous.subEncounterTypeValue,
        dateFrom: state.bookingAnonymous.dateFrom,
        dateTo: state.bookingAnonymous.dateTo,
        encounterTypeListData: state.bookingAnonymous.encounterTypeListData,
        subEncounterTypeListData: state.bookingAnonymous.subEncounterTypeListData,
        calendarData: state.bookingAnonymous.calendarData,
        subEncounterTypeListKeyAndValue: state.bookingAnonymous.subEncounterTypeListKeyAndValue,
        codeList: state.bookingAnonymous.codeList,
        anonymousPatint: state.bookingAnonymous.anonyomousBookingActiveInfo,
        bookData: state.bookingAnonymous.bookData,
        countryList: state.patient.countryList || []
    };
};

const mapDispatchToProps = {
    resetAll,
    destroyPage,
    updateField,
    getCodeList,
    fillingData,
    loadAppointmentBooking
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(BookingAnonymous));