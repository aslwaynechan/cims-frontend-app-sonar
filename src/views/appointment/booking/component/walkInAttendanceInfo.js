import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    Grid,
    Typography,
    RadioGroup,
    Radio,
    FormControlLabel,
    Checkbox
} from '@material-ui/core';
import ValidatorForm from '../../../../components/FormValidator/ValidatorForm';
import SelectFieldValidator from '../../../../components/FormValidator/SelectFieldValidator';
import TextFieldValidator from '../../../../components/FormValidator/TextFieldValidator';
import CommonRegex from '../../../../constants/commonRegex';
import * as CommonUtilities from '../../../../utilities/commonUtilities';

const styles = () => ({
    root: {
        width: '100%',
        marginTop: '8px'
    },
    maintitleRoot: {
        paddingTop: '6px',
        fontSize: '14pt',
        fontWeight: 600
    },
    infoContainerRoot: {
        flexWrap: 'wrap',
        //alignItems: 'flex-end',
        border: '1px solid rgba(0, 0, 0, 0.23)',
        padding: '16px 18px 16px 18px',
        borderRadius: '4px',
        marginBottom: '2px'
    },
    infoItemRoot: {
        flex: 1
    },
    radioBtn: {
        height: 26,
        paddingTop: 0,
        paddingBottom: 0,
        '&$radioBtnChecked': {
            height: 26,
            paddingTop: 0,
            paddingBottom: 0
        }
    }
});
class walkInAttendanceInfo extends Component {
    payment = [
        {
            'code': 'Cash',
            'engDesc': 'Cash'
        },
        {
            'code': 'Cheque',
            'engDesc': 'Cheque'
        },
        {
            'code': 'Octopus',
            'engDesc': 'Octopus'
        }
    ]
    updateInfo = (info) => {
        this.props.handleWalkInInfoChange(info);
    }

    handlePatientStatusChange = (e) => {
        let info = {
            ...this.props.walkInInfo,
            patientStatus: e.value
        };
        this.updateInfo(info);

    }

    changeRadio = (e, checked) => {
        let value = e.target.value;
        let info = { ...this.props.walkInInfo };
        if (checked) {
            info.paymentMeanCD = value;
            this.updateInfo(info);
        }
    };

    handleDiscNumberChange = (e) => {
        let reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
        let inputProps = this.refs.discNum.props.inputProps;
        let value = e.target.value;
        if (reg.test(value)) {
            return;
        }

        if (inputProps.maxLength && value.length > inputProps.maxLength) {
            value = value.slice(0, inputProps.maxLength);
        }

        let info = {
            ...this.props.walkInInfo,
            discNumber: value
        };
        this.updateInfo(info);
    }

    handleCbMSWChange = (e, checked) => {
        let info = { ...this.props.walkInInfo };
        if (checked) {
            info.mswWaiver = 1;
        } else {
            info.mswWaiver = 0;
        }
        this.updateInfo(info);
    }

    handleAmountChange = (e)=>{
        let info ={
            ...this.props.walkInInfo,
            amount:e.target.value
        };
         this.updateInfo(info);
    }

    render() {
        const { classes, patientStatusFlag, patientStatus } = this.props;
        let walkInInfo = this.props.walkInInfo;
        let patientStatusList = this.props.patientStatusList;
        return (
            <Grid className={classes.root}>
                <ValidatorForm
                    id={this.props.id + 'Form'}
                    ref={'form'}
                    onSubmit={() => { }}
                >
                    <Typography className={classes.maintitleRoot}>Attendance Information</Typography>
                    <Grid container item className={classes.infoContainerRoot}>
                        <Grid container spacing={1}>
                            <Grid item xs={4}>
                                <Grid container item >
                                    <Typography >{CommonUtilities.getPatientCall()} Status</Typography>
                                </Grid>
                                <Grid container item>
                                    <SelectFieldValidator
                                        id={this.props.id + 'patientStatus'}
                                        options={patientStatusList &&
                                            patientStatusList.map((item) => (
                                                { value: item.code, label: item.code }
                                            ))}
                                        placeholder={'None'}
                                        name={'patientStatus'}
                                        //isDisabled={patientStatusFlag === 'N'}
                                        onChange={this.handlePatientStatusChange}
                                        // value={patientStatusFlag === 'Y' ? patientStatus : ''}
                                        // isDisabled={patientStatusFlag !== 'Y'}
                                        value={walkInInfo.patientStatus}
                                    />
                                </Grid>
                            </Grid>
                            <Grid item xs={6}>
                                <Grid container item>
                                    <Typography>Payment Mean</Typography>
                                </Grid>
                                <Grid container item>
                                    <RadioGroup
                                        id={this.props.id + '_PaymentMeanGroup'}
                                        row
                                        style={{ flexWrap: 'nowrap', justifyContent: 'space-between', width: '100%' }}
                                        name="Payment Means"
                                        value={walkInInfo.paymentMeanCD || 'Cash'}
                                        onChange={(...arg) => this.changeRadio(...arg, 'paymentMeanCD')}
                                    >
                                        {this.payment.map((item, index) =>
                                            <FormControlLabel key={index} value={item.code}
                                                //disabled={comDisabled}
                                                label={item.engDesc}
                                                labelPlacement="end"
                                                id={this.props.id + '_' + item.code + '_radioLabel'}
                                                control={
                                                    <Radio
                                                        id={this.props.id + '_' + item.code + '_radio'}
                                                        color={'primary'}
                                                        classes={{
                                                            root: classes.radioBtn,
                                                            checked: classes.radioBtnChecked
                                                        }}
                                                    />}
                                            />
                                        )}
                                    </RadioGroup>
                                </Grid>
                            </Grid>
                            <Grid item xs={2}>
                                <Grid container item>
                                    <Typography>Amount</Typography>
                                </Grid>
                                <TextFieldValidator
                                    id={'txt_' + this.props.id + '_Amount'}
                                    variant={'standard'}
                                    value={walkInInfo.amount}
                                    // disabled
                                    onChange={(e) => this.handleAmountChange(e, 'amount')}
                                />
                            </Grid>

                        </Grid>
                        <Grid container spacing={1} style={{ alignItems: 'flex-end' }}>
                            <Grid item xs={4}>
                                <Grid container item>
                                    <Typography>Disc Number</Typography>
                                </Grid>
                                <TextFieldValidator
                                    id={'txt_' + this.props.id + '_Disc_Number'}
                                    variant={'outlined'}
                                    ref={'discNum'}
                                    value={walkInInfo.discNumber}
                                    onChange={this.handleDiscNumberChange}
                                    //inputProps={{width:'100%'}}
                                    style={{ width: '100%' }}
                                    inputProps={{
                                        maxLength: 10
                                    }}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                {/* <Grid item>
                                <Typography>Disc Number</Typography>
                            </Grid> */}
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            id={this.props.id + 'Msw_Waiver_CheckBox'}
                                            value={'1'}
                                            name={'MSW Waiver'}
                                            classes={{
                                                root: classes.radioBtn,
                                                checked: classes.radioBtnChecked
                                            }}
                                        />
                                    }
                                    label={'MSW Waiver'}
                                    checked={walkInInfo.mswWaiver === 1}// eslint-disable-line
                                    onChange={this.handleCbMSWChange}
                                // id={'sunLabel'}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                </ValidatorForm>
            </Grid>
        );
    }
}

export default withStyles(styles)(walkInAttendanceInfo);