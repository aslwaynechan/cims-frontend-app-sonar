import React, { Component } from 'react';
import {
    Grid,
    Table,
    TableBody,
    TableHead,
    TableRow,
    TableCell,
    ButtonBase
} from '@material-ui/core';
import moment from 'moment';
import _ from 'lodash';

class Calendar extends Component {

    UNSAFE_componentWillMount() {
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    montnView = (calendarData) => {
        let tableDatas = [];
        let tableData = {};
        let row = [];
        let rowLength = 7;
        let beginIsSat = false;
        let endIsSun = false;
        calendarData.map((item) => {
            tableData.title = item.subEncounterType;
            tableData.row = [];
            item.slotByDateDtos.map((cell, cellIndex) => {
                if (cellIndex === 6) {
                    beginIsSat = cell.date.substring(cell.date.length - 2) === '01';
                }
                if (cellIndex === item.slotByDateDtos.length - 6) {
                    endIsSun = cell.date.substring(cell.date.length - 2) === '01';
                }
                if (rowLength === 0) {
                    tableData.row.push(row);
                    row = [];
                    rowLength = 6;
                    row.push(cell);
                } else {
                    if (cellIndex === item.slotByDateDtos.length - 1) {
                        row.push(cell);
                        tableData.row.push(row);
                        row = [];
                        rowLength = 7;
                    } else {
                        row.push(cell);
                        rowLength--;
                    }
                }
                return cell;
            });
            tableDatas.push(_.cloneDeep(tableData));
            tableData = {};
            return item;
        });
        return (<Grid id={'calendarMontnView'} className="montnView" style={{ width: '100%', display: 'flex' }}>
            {tableDatas.map((item,index) => {
                return (
                    <Grid key={'montnViewRoom' + item.title} style={{ flex: 1, paddingLeft: 10, paddingRight: 1, minWidth: 460 }}>
                        <Table id={'calendarMontnViewTable'+index} style={{ tableLayout: 'fixed' }} >
                            <TableHead>
                                <TableRow style={{ height: 40 }}>
                                    <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}>{this.props.tableTitle[item.title]}</TableCell>
                                </TableRow>
                                <TableRow style={{ height: 40 }}>
                                    {this.props.showWeekend === true ? <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}>Sun</TableCell> : null}
                                    <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}>Mon</TableCell>
                                    <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}>Tue</TableCell>
                                    <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}>Wed</TableCell>
                                    <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}>Thu</TableCell>
                                    <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}>Fri</TableCell>
                                    {this.props.showWeekend === true ? <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}>Sat</TableCell> : null}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {item.row.map((row, rowIndex) => {
                                    if ((this.props.showWeekend !== true && beginIsSat && rowIndex === 0) || (this.props.showWeekend !== true && endIsSun && rowIndex === item.row.length - 1)) {
                                        return (null);
                                    } else {
                                        return (<TableRow key={'row' + rowIndex}>
                                            {row.map((cell, cellIndex) => {
                                                if (this.props.showWeekend === true || (cellIndex !== 0 && cellIndex !== 6)) {
                                                    return (
                                                        <TableCell key={cellIndex} align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px', height: 100 }}>
                                                            <ButtonBase
                                                                id={'calendarMontnViewTableCellButtonBaseRow' + rowIndex + 'Cell' + cellIndex}
                                                                disabled={cell.holiday || !cell.normalRemain || (cell.normalRemain === 0 && cell.urgentRemain === 0)}
                                                                style={{ width: '100%', height: '100%', display: 'block' }}
                                                                onClick={typeof this.props.onClick === 'function' ? (e) => { this.props.onClick(e, { subEncounterType: item.title, slotByDateDto: cell }); } : () => { }}
                                                            >
                                                                <Grid style={{ width: '100%', height: '100%', display: 'flex', flexDirection: 'column' }}>
                                                                    <Grid style={{ background: '#d0f0fb', textAlign: 'left', padding: 2 }}>{cell.date.substring(cell.date.length - 2) * 1}</Grid>
                                                                    {this.cellContent(cell)}
                                                                </Grid>
                                                            </ButtonBase>
                                                        </TableCell>
                                                    );
                                                }else{
                                                    return(null);
                                                }
                                            }
                                            )}
                                        </TableRow>);
                                    }
                                })}
                            </TableBody>
                        </Table>
                    </Grid>
                );
            }
            )}
        </Grid>);

    }
    cellContent = (cell) => {
        if (cell.holiday || (!cell.normalRemain&&cell.normalRemain!==0)){
            return (<Grid
                style={{
                    flex: 1,
                    display: 'flex',
                    justifyContent: 'center',
                    flexDirection: 'column',
                    wordWrap: 'break-word',
                    color: cell.holiday ? '#5c605e' : '#0a7cc9',
                    background: cell.holiday ? '#fed2d9' : '#ffffcc'
                }}
                    >
                {cell.holiday ? 'Holiday' : 'No Slot'}
            </Grid>);
        } else {
            return (
                <Grid
                    style={{
                        flex: 1,
                        display: 'flex',
                        justifyContent: 'center',
                        flexDirection: 'column',
                        color: !(cell.normalTotal === 0 && cell.urgentTolal === 0) && cell.normalRemain === 0 ? '#ffffff' : '#0a7cc9',
                        background: cell.normalTotal === 0 && cell.urgentTolal === 0 ? '#ffffcc' : cell.normalRemain === 0 ? '#bfbfbf' : '#ffffff'
                    }}
                >
                    <Grid style={{ wordWrap: 'break-word', display: this.props.availableQuotaValue.indexOf('normal') === -1 ? 'none' : null }}>{this.props.availableQuotaValue.indexOf('all') !== -1 ? cell.normalRemain + ' / ' + cell.normalTotal : cell.normalRemain}</Grid>
                    <Grid style={{ wordWrap: 'break-word', display: this.props.availableQuotaValue.indexOf('urgent') === -1 ? 'none' : null }}>{this.props.availableQuotaValue.indexOf('all') !== -1 ? cell.urgentRemain + ' / ' + cell.urgentTolal : cell.urgentRemain}</Grid>
                </Grid>
            );
        }
    }

    weekView = (calendarData) => {
        let tableDatas = [];
        let tableData = {};
        let rows = {};
        calendarData.map((item) => {
            tableData.title = item.subEncounterType;
            tableData.row = [];
            tableData.column = [];
            tableData.holidayColumn = [];
            tableData.noSlotColumn = [];
            item.slotByDateDtos.map((itemSlotDate, index) => {
                if (this.props.showWeekend === true || (index > 0 && index < item.slotByDateDtos.length - 1)) {
                    tableData.column.push(itemSlotDate.date);
                    if (itemSlotDate.holiday) {
                        tableData.holidayColumn.push(itemSlotDate.date);
                    } else if (!itemSlotDate.slotByHourDtos) {
                        tableData.noSlotColumn.push(itemSlotDate.date);
                    } else {
                        itemSlotDate.slotByHourDtos.map(itemSlotHour => {
                            if (!rows[itemSlotHour.startTime]) {
                                rows[itemSlotHour.startTime] = [];
                            }
                            itemSlotHour.date = itemSlotDate.date;
                            itemSlotHour.holiday = itemSlotDate.holiday;
                            rows[itemSlotHour.startTime].push(itemSlotHour);
                            return itemSlotHour;
                        });
                    }
                }
                return itemSlotDate;
            });
            tableData.row = Object.keys(rows).sort();
            tableData.rowData = rows;
            tableDatas.push(tableData);
            tableData = {};
            rows = {};
            return item;
        });
        return (<Grid id={'calendarWeekView'} className="weekView" style={{ width: '100%', display: 'flex' }}>
            {tableDatas.map((table,index) => {
                return (
                    <Grid key={'weekViewRoom' + table.title} style={{ flex: 1, paddingLeft: 10, paddingRight: 1, minWidth: 460 }}>
                        <Table id={'calendarWeekViewTable'+index} style={{ tableLayout: 'fixed' }} >
                            <TableHead>
                                <TableRow style={{ height: 40 }}>
                                    <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}>{this.props.tableTitle[table.title]}</TableCell>
                                </TableRow>
                                <TableRow style={{ height: 40 }}>
                                    <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}></TableCell>
                                    {table.column.map((tableHeadCell, tableHeadCellIndex) => {
                                        return (<TableCell key={'tableHeadCellIndex' + tableHeadCellIndex} align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}>{moment(tableHeadCell).format('ddd')}</TableCell>);
                                    })}
                                </TableRow>
                                <TableRow style={{ height: 'auto' }}>
                                    <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}></TableCell>
                                    {table.column.map((tableHeadCell, tableHeadCellIndex) => {
                                        return (<TableCell key={'tableHeadCellIndex' + tableHeadCellIndex} align="center" style={{ border: '1px solid #cccccc', padding: 2, fontSize: '16px', background: '#d0f0fb' }}>{moment(tableHeadCell).format('D')}</TableCell>);
                                    })}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.weekViewRow(table)}
                            </TableBody>
                        </Table>
                    </Grid>
                );
            }
            )}
        </Grid>);

    }
    weekViewRow = (table) => {
        if (table.row.length === 0) {
            table.row.push(null);
        }
        let row = table.row.map((row, rowIndex) => {
            return (
                <TableRow key={'row' + rowIndex} style={{ height: 40 }}>
                    <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}>{row || ''}</TableCell>
                    {table.column.map((column, columnIndex) => {
                        if (table.holidayColumn.indexOf(column) !== -1 || table.noSlotColumn.indexOf(column) !== -1) {
                            if (rowIndex === 0) {
                                return (
                                    <TableCell key={'columnIndex' + columnIndex} rowSpan={table.row.length} align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px', height: 47 }}>
                                        <ButtonBase
                                            id={'calendarWeekViewTableCellButtonBaseRow0Cell0'}
                                            disabled
                                            style={{ width: '100%', height: '100%', display: 'block' }}
                                        >
                                            <Grid style={{ width: '100%', height: '100%', display: 'flex' }}>
                                                {this.cellContent({ holiday: table.holidayColumn.indexOf(column) !== -1 })}
                                            </Grid>
                                        </ButtonBase>
                                    </TableCell>
                                );
                            }else{
                                return(null);
                            }
                        } else {
                            let cell = table.rowData[row].find(item => {
                                return item.date === column;
                            });
                            if (!cell) {
                                return (
                                    <TableCell key={'columnIndex' + columnIndex} align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px', height: 47 }}>
                                        <ButtonBase
                                            id={'calendarWeekViewTableCellButtonBaseRow0Cell0'}
                                            disabled
                                            style={{ width: '100%', height: '100%', display: 'block' }}
                                        >
                                            <Grid style={{ width: '100%', height: '100%', display: 'flex' }}>
                                                {this.cellContent({ holiday: table.holidayColumn.indexOf(column) !== -1 })}
                                            </Grid>
                                        </ButtonBase>
                                    </TableCell>
                                );
                            } else {
                                return (
                                    <TableCell key={'columnIndex' + columnIndex} align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px', height: 47 }}>
                                        <ButtonBase
                                            id={'calendarWeekViewTableCellButtonBaseRow0Cell0'}
                                            disabled={(cell.normalRemain === 0 && cell.urgentRemain === 0)}
                                            style={{ width: '100%', height: '100%', display: 'block' }}
                                            onClick={typeof this.props.onClick === 'function' ? (e) => {
                                                this.props.onClick(e, { subEncounterType: table.title, slotByDateDto: { date: cell.date, time: cell.startTime } });
                                            } : () => { }}
                                        >
                                            <Grid style={{ width: '100%', height: '100%', display: 'flex' }}>
                                                {this.cellContent(cell)}
                                            </Grid>
                                        </ButtonBase>
                                    </TableCell>
                                );
                            }

                        }
                    })}
                </TableRow>
            );
        });
        return (row);
    }
    dayView = (calendarData) => {
        return (<Grid className="dayView" style={{ width: '100%', display: 'flex' }}>
            {calendarData.map((item,index) => {
                return (
                    <Grid key={'dayViewRoom' + item.subEncounterType} style={{ flex: 1, paddingLeft: 10, paddingRight: 1, minWidth: 460 }}>
                        <Table id={'calendarDayViewTable'+index} style={{ tableLayout: 'fixed' }} >
                            <TableHead>
                                <TableRow style={{ height: 40 }}>
                                    <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px', width: 110 }}>{this.props.tableTitle[item.subEncounterType]}</TableCell>
                                </TableRow>
                                <TableRow style={{ height: 40 }}>
                                    <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}></TableCell>
                                    <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}>{moment(this.props.dateFrom).format('ddd')}</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                < TableRow style={{ height: 'auto' }} >
                                    <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px' }}>

                                    </TableCell>
                                    <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 2, fontSize: '16px', background: '#d0f0fb' }}>
                                        {moment(this.props.dateFrom).format('D')}
                                    </TableCell>

                                </ TableRow>
                                {!item.slotByDateDtos[0].slotByHourDtos || item.slotByDateDtos[0].slotByHourDtos.length <= 0 || item.slotByDateDtos[0].holiday ?
                                    < TableRow key={'row' + 0} >
                                        <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px', height: 47 }}>

                                        </TableCell>
                                        <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px', height: 47 }}>
                                            <ButtonBase
                                                id={'calendarDayViewTableCellButtonBaseRow0Cell1'}
                                                disabled
                                                style={{ width: '100%', height: '100%', display: 'block' }}
                                            >
                                                <Grid style={{ width: '100%', height: '100%', display: 'flex' }}>
                                                    <Grid
                                                        style={{
                                                            flex: 1,
                                                            display: 'flex',
                                                            justifyContent: 'center',
                                                            flexDirection: 'column',
                                                            wordWrap: 'break-word',
                                                            color: item.slotByDateDtos[0].holiday ? '#5c605e' : '#0a7cc9',
                                                            background: item.slotByDateDtos[0].holiday ? '#fed2d9' : '#ffffcc'
                                                        }}
                                                    >
                                                        {item.slotByDateDtos[0].holiday ? 'Holiday' : 'No Slot'}
                                                    </Grid>
                                                </Grid>
                                            </ButtonBase>
                                        </TableCell>
                                    </TableRow> : item.slotByDateDtos[0].slotByHourDtos.map((row, rowIndex) => {
                                        console.log('row',row);
                                        return (
                                            < TableRow key={'row' + rowIndex} >
                                                <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px', height: 47 }}>
                                                    {row.startTime}
                                                </TableCell>
                                                <TableCell align="center" style={{ border: '1px solid #cccccc', padding: 0, fontSize: '16px', height: 47 }}>
                                                    <ButtonBase
                                                        id={'calendarDayViewTableCellButtonBaseRow' + rowIndex + 'Cell1'}
                                                        disabled={(row.normalRemain === 0 && row.urgentRemain === 0)}
                                                        style={{ width: '100%', height: '100%', display: 'block' }}
                                                        onClick={typeof this.props.onClick === 'function' ? (e) => {
                                                            this.props.onClick(e, { subEncounterType: item.subEncounterType, slotByDateDto: { date: item.slotByDateDtos[0].date, time: row.startTime } });
                                                        } : () => { }}
                                                    >
                                                        <Grid style={{ width: '100%', height: '100%', display: 'flex'}}>
                                                            {this.cellContent({ holiday: item.slotByDateDtos[0].holiday, ...row })}
                                                        </Grid>
                                                    </ButtonBase>
                                                </TableCell>
                                            </TableRow>
                                        );
                                    })}
                            </TableBody>
                        </Table>
                    </Grid>);
            })}
        </Grid>);
    }

    render() {
        let viewType = this.props.viewType;
        let calendarData = this.props.calendarData;
        return (
            <Grid style={{ overflow: 'auto' }}>
                {viewType === 'M' ? this.montnView(calendarData) : viewType === 'W' ? this.weekView(calendarData) : this.dayView(calendarData)}
            </Grid>
        );
    }
}


export default Calendar;