import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    Grid
} from '@material-ui/core';
import ValidForm from '../../../../components/FormValidator/ValidatorForm';
import SelectFieldValidator from '../../../../components/FormValidator/SelectFieldValidator';
import TextFieldValidator from '../../../../components/FormValidator/TextFieldValidator';
import BookingEnum from '../../../../enums/appointment/booking/bookingEnum';
import ValidatorEnum from '../../../../enums/validatorEnum';
import CommonMessage from '../../../../constants/commonMessage';
import CommonRegex from '../../../../constants/commonRegex';
import * as RegUtil from '../../../../utilities/registrationUtilities';
import ValidatorForm from '../../../../components/FormValidator/ValidatorForm';
import _ from 'lodash';
import RegFieldLength from '../../../../enums/registration/regFieldLength';
import FieldConstant from '../../../../constants/fieldConstant';
import HKIDInput from '../../../compontent/hkidInput';
import * as PatientUtil from '../../../../utilities/patientUtilities';
import RequiredIcon from '../../../../components/InputLabel/RequiredIcon';
import CIMSFormLabel from '../../../../components/InputLabel/CIMSFormLabel';

const styles = () => ({
    root: {
        width: '100%',
        //borderLeft:'none'
        borderBottom: '1px solid #dcdcdc'
        //
    },
    formRoot: {
        width: '100%',
        display: 'flex'
    },
    containerRoot: {
        //width:'100%',
        display: 'flex'
    },
    itemRoot: {
        margin: 10,
        flex: 1
    },
    formLabelContainer: {
        paddingTop: 15,
        paddingBottom: 15
    }

});

class anonymousPatientBar extends Component {
    componentDidMount() {
        ValidatorForm.addValidationRule(ValidatorEnum.isHkid, (value) => {
            return RegUtil.checkHKID(value);
        });
    }

    invalidFieldList = [];

    updateAnonymousPatientInfo = (patient) => {
        this.props.anonymousInfoOnchange(patient);
    }

    fillingPatientInfo = (info) => {
        let emptyFieldCount = 0;
        let fillingData = {};
        for (let i in info) {
            if (info[i] === '' && i !== 'docNO') {
                emptyFieldCount++;
            }
        }
        if (info.docTypeCd === 'ID' && !info.isHKIDValid) {
            emptyFieldCount++;
        }

        if (emptyFieldCount === 0 && this.invalidFieldList.length === 0) {
            fillingData = null;
            let tempDocTypeCd = info.docNO !== '' ? info.docTypeCd : '';
            let patientInfo = {
                //docTypeCd: info.docTypeCd,
                docTypeCd: tempDocTypeCd,
                surname: info.surname.toUpperCase(),
                givenName: info.givenName.toUpperCase(),
                hkid: info.docTypeCd === 'ID' ? info.docNO.replace('(', '').replace(')', '').toUpperCase() : '',
                otherDocNo: info.docTypeCd === 'ID' ? '' : info.docNO ? info.docNO.toUpperCase() : '',
                mobile: info.mobile,
                countryCd: info.countryCd,
                deadInd: '0',
                patientKey: info.patientKey || '0'
            };
            fillingData = {
                patientInfo: patientInfo
            };
        }
        else {
            fillingData = {
                patientInfo: null
            };
        }
        this.props.fillingPatientInfo({ fillingData: fillingData });
    }

    handleDocTypeChange = (e) => {
        let patient = {
            ...this.props.patient,
            docTypeCd: e.value
        };
        this.updateAnonymousPatientInfo(patient);
    }

    handleInputsChange = (e, name) => {
        let reg = '';
        let value = e.target.value;

        if (name === 'mobile') {
            reg = new RegExp(CommonRegex.VALIDATION_REGEX_NOT_NUMBER);
        }
        else {
            reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
        }

        if (reg.test(value)) {
            return;
        }

        let patient = { ...this.props.patient };
        patient[name] = value;
        this.updateAnonymousPatientInfo(patient);
    }

    handleFocus = (e) => {
        let patient = { ...this.props.patient };
        if (patient.docTypeCd === 'ID') {
            patient.docNO = e.target.value.replace('(', '').replace(')', '');
        }
        this.updateAnonymousPatientInfo(patient);
    }

    handleDocNoBlur = (e) => {
        let patient = { ...this.props.patient };
        let value = e.target.value.replace('(', '').replace(')', '');
        if (!this.props.docTypeCd || this.props.docTypeCd === 'ID') {
            let promise = new Promise((resolve) => {
                resolve(RegUtil.checkHKID(value));
            });
            promise.then((result) => {
                if (result && this.props.patient.docNO) {
                    patient.docTypeCd = 'ID';
                    patient.docNO = RegUtil.hkidFormat(value);
                }
                patient.isHKIDValid = result;
                this.updateAnonymousPatientInfo(patient);
            });
        }
    }

    validatorListener = (isvalid, name) => {
        let patientInfo = { ...this.props.patient };
        if (!isvalid) {
            patientInfo[name] = '';
            if (this.invalidFieldList.indexOf(name) < 0) {
                this.invalidFieldList.push(name);
            }
        }
        else {
            let tempInvalidFieldList = [];
            this.invalidFieldList.forEach(f => {
                if (f !== name) {
                    tempInvalidFieldList.push(f);
                }
                this.invalidFieldList = tempInvalidFieldList;
            });
        }
        this.fillingPatientInfo(patientInfo);
    }

    handleCountryCdChange = (e) => {
        let patient = {
            ...this.props.patient,
            countryCd: e.value
        };
        this.refs.mobile.validateCurrent();
        this.updateAnonymousPatientInfo(patient);
    }

    handleDocNoChange = (value) => {
        let reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
        if (reg.test(value)) {
            return;
        }

        let inputProps = this.refs.docNO.props.inputProps;
        if (inputProps.maxLength && value.length > inputProps.maxLength) {
            value = value.slice(0, inputProps.maxLength);
        }
    }

    render() {
        const { classes, codeList, patient, id, countryList } = this.props;
        const countryCd = patient.countryCd;
        const isHKIDFormat = PatientUtil.isHKIDFormat(patient.docTypeCd);

        let hkidValidator = [];
        let hkidErrorMessages = [];
        if (isHKIDFormat) {
            hkidValidator.push(ValidatorEnum.isHkid);
            hkidErrorMessages.push(CommonMessage.VALIDATION_NOTE_HKIC_FORMAT_ERROR());
        }

        let phoneLength = RegFieldLength.CONTACT_PHONE_DEFAULT_MAX;
        let mobileValidRule = [ValidatorEnum.isNumber, ValidatorEnum.minStringLength(phoneLength), ValidatorEnum.maxStringLength(phoneLength)];
        let mobileErrMsg = [CommonMessage.VALIDATION_NOTE_NUMBERFIELD(), CommonMessage.VALIDATION_NOTE_PHONE_BELOWMINWIDTH(), CommonMessage.VALIDATION_NOTE_OVERMAXWIDTH()];

        if (countryCd !== FieldConstant.COUNTRY_CODE_DEFAULT_VALUE) {
            phoneLength = RegFieldLength.CONTACT_PHONE_OTHERS_MAX;
            mobileValidRule = [ValidatorEnum.isNumber];
            mobileErrMsg = [CommonMessage.VALIDATION_NOTE_NUMBERFIELD()];
        }
        let mobilephoneValidRule = _.cloneDeep(mobileValidRule);
        let mobilephoneErrMsg = _.cloneDeep(mobileErrMsg);

        mobilephoneValidRule.push(ValidatorEnum.phoneNo);
        mobilephoneErrMsg.push(CommonMessage.VALIDATION_NOTE_PHONE_NO());
        return (
            <Grid className={classes.root}>
                <Grid container >

                    <ValidForm className={classes.formRoot} onSubmit={() => { }} ref={'form'} id={this.props.id + 'AnonymousPatientInfoForm'}>
                        <Grid container direction={'row'}>
                            <Grid container item direction={'row'} className={classes.itemRoot} alignItems="flex-start">
                                <Grid container item direction={'row'} className={classes.itemRoot} >
                                    <SelectFieldValidator
                                        id={id + 'DocumentTypeSelectField'}
                                        TextFieldProps={{
                                            variant: 'outlined',
                                            label: <>{BookingEnum.ANONYMOUS_BANNER_LABEL.DOCUMENT_TYPE}</>
                                        }}
                                        // labelText={BookingEnum.ANONYMOUS_BANNER_LABEL.DOCUMENT_TYPE}
                                        className={classes.fieldRoot}
                                        fullWidth
                                        options={
                                            codeList &&
                                            codeList.doc_type &&
                                            codeList.doc_type.map((item) => (
                                                { value: item.code, label: item.engDesc }))}
                                        value={patient.docTypeCd}
                                        onChange={this.handleDocTypeChange}
                                        isDisabled={patient.patientKey ? true : false}
                                    />
                                </Grid>
                                <Grid container item direction={'row'} className={classes.itemRoot} >
                                    <HKIDInput
                                        isHKID={isHKIDFormat}
                                        id={id + 'DocumentNoTextField'}
                                        ref={'docNO'}
                                        label={<>{BookingEnum.ANONYMOUS_BANNER_LABEL.DOCUMENT_NO}</>}
                                        // labelText={BookingEnum.ANONYMOUS_BANNER_LABEL.DOCUMENT_NO}
                                        className={classes.fieldRoot}
                                        disabled={patient.patientKey ? true : false}
                                        //isRequired
                                        fullWidth
                                        upperCase
                                        validByBlur
                                        name={'docNO'}
                                        onChange={e => { this.handleInputsChange(e, 'docNO'); }}
                                        value={patient.docNO}
                                        validators={hkidValidator}
                                        errorMessages={hkidErrorMessages}
                                        msgPosition={'bottom'}
                                        variant="outlined"
                                        validatorListener={(isvalid) => this.validatorListener(isvalid, 'docNO')}
                                    />
                                </Grid>
                                <Grid container item direction={'row'} className={classes.itemRoot} >
                                    <TextFieldValidator
                                        id={id + 'SurnameTextField'}
                                        ref={'surname'}
                                        label={<>{BookingEnum.ANONYMOUS_BANNER_LABEL.SURNAME}<RequiredIcon /></>}
                                        // labelText={BookingEnum.ANONYMOUS_BANNER_LABEL.SURNAME}
                                        className={classes.fieldRoot}
                                        upperCase
                                        isRequired
                                        disabled={patient.patientKey ? true : false}
                                        fullWidth
                                        validByBlur
                                        onlyOneSpace
                                        name={'surname'}
                                        variant="outlined"
                                        onChange={e => { this.handleInputsChange(e, 'surname'); }}
                                        value={patient.surname}
                                        validators={[ValidatorEnum.required, ValidatorEnum.isSpecialEnglish]}
                                        errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED(), CommonMessage.VALIDATION_NOTE_SPECIAL_ENGLISH()]}
                                        warning={[
                                            ValidatorEnum.isEnglishWarningChar
                                        ]}
                                        warningMessages={[
                                            CommonMessage.WARNING_NOTE_SPECIAL_ENGLISH()
                                        ]}
                                        msgPosition={'bottom'}
                                        // eslint-disable-next-line
                                        inputProps={{ maxLength: 40 }}
                                        validatorListener={(isvalid) => this.validatorListener(isvalid, 'surname')}
                                        trim={'all'}
                                    />
                                </Grid>
                                <Grid container item direction={'row'} className={classes.itemRoot} >
                                    <TextFieldValidator
                                        id={id + 'GivenNameTextField'}
                                        ref={'givenName'}
                                        label={<>{BookingEnum.ANONYMOUS_BANNER_LABEL.GIVEN_NAME}<RequiredIcon /></>}
                                        // labelText={BookingEnum.ANONYMOUS_BANNER_LABEL.GIVEN_NAME}
                                        className={classes.fieldRoot}
                                        isRequired
                                        disabled={patient.patientKey ? true : false}
                                        fullWidth
                                        upperCase
                                        validByBlur
                                        onlyOneSpace
                                        name={'givenName'}
                                        variant="outlined"
                                        onChange={e => { this.handleInputsChange(e, 'givenName'); }}
                                        value={patient.givenName}
                                        validators={[ValidatorEnum.required, ValidatorEnum.isSpecialEnglish]}
                                        errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED(), CommonMessage.VALIDATION_NOTE_SPECIAL_ENGLISH()]}
                                        warning={[
                                            ValidatorEnum.isEnglishWarningChar
                                        ]}
                                        warningMessages={[
                                            CommonMessage.WARNING_NOTE_SPECIAL_ENGLISH()
                                        ]}
                                        msgPosition={'bottom'}
                                        // eslint-disable-next-line
                                        inputProps={{ maxLength: 40 }}
                                        validatorListener={(isvalid) => this.validatorListener(isvalid, 'givenName')}
                                        trim={'all'}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container item direction={'row'} className={classes.itemRoot} style={{ flex: 0.5 }}>
                                <Grid container direction="column" >
                                    {/* <Grid container item alignItems="baseline" spacing={1}>
                                        <Grid item style={{ fontWeight: 'bold' }}>
                                            <span>{BookingEnum.ANONYMOUS_BANNER_LABEL.MOBILE}</span>
                                            <span style={{ color: 'red' }}>*</span>
                                        </Grid>
                                    </Grid> */}
                                    <CIMSFormLabel
                                        // fullWidth
                                        // labelText={BookingEnum.ANONYMOUS_BANNER_LABEL.MOBILE}
                                        labelText={<>{BookingEnum.ANONYMOUS_BANNER_LABEL.MOBILE}<RequiredIcon /></>}
                                        className={classes.formLabelContainer}
                                    >
                                        <Grid container direction={'row'}>
                                            <Grid container style={{ width: 200 }}>
                                                <SelectFieldValidator
                                                    id={id + 'MobileCountrySelectField'}
                                                    ref={'countryCd'}
                                                    name={'countryCd'}
                                                    TextFieldProps={{
                                                        variant: 'outlined'
                                                    }}
                                                    isDisabled={patient.patientKey ? true : false}
                                                    options={countryList.map((item) => ({ value: item.countryCd, label: `${item.countryName || ''}(${item.dialingCd})` }))}
                                                    value={this.props.patient.countryCd}
                                                    onChange={this.handleCountryCdChange}
                                                //validatorListener={(isvalid) => this.validatorListener(isvalid, 'countryCd')}
                                                //labelText={BookingEnum.ANONYMOUS_BANNER_LABEL.MOBILE}
                                                />
                                            </Grid>
                                            <Grid container style={{ flex: 1 }}>
                                                <TextFieldValidator
                                                    id={id + 'MobileTextField'}
                                                    ref={'mobile'}
                                                    // label={<>{BookingEnum.ANONYMOUS_BANNER_LABEL.MOBILE}<RequiredIcon /></>}
                                                    //labelText={BookingEnum.ANONYMOUS_BANNER_LABEL.MOBILE}
                                                    className={classes.fieldRoot}
                                                    //isRequired
                                                    disabled={patient.patientKey ? true : false}
                                                    fullWidth
                                                    upperCase
                                                    validByBlur
                                                    name={'mobile'}
                                                    onChange={e => { this.handleInputsChange(e, 'mobile'); }}
                                                    value={patient.mobile}
                                                    validators={mobilephoneValidRule}
                                                    errorMessages={mobilephoneErrMsg}
                                                    msgPosition={'bottom'}
                                                    variant="outlined"
                                                    // eslint-disable-next-line
                                                    inputProps={{ maxLength: phoneLength }}
                                                    validatorListener={(isvalid) => this.validatorListener(isvalid, 'mobile')}
                                                //labelText={' '}
                                                />
                                            </Grid>

                                        </Grid>
                                    </CIMSFormLabel>
                                </Grid>
                            </Grid>
                        </Grid>
                    </ValidForm>

                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(anonymousPatientBar);