import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    Grid,
    FormControlLabel,
    Paper,
    Typography,
    Backdrop,
    IconButton,
    Link
} from '@material-ui/core';
import { CalendarTodayRounded, PlaylistAddCheck } from '@material-ui/icons';
import CIMSTable from '../../../../components/Table/CIMSTable';
import CIMSButton from '../../../../components/Buttons/CIMSButton';
import moment from 'moment';
import Enum from '../../../../enums/enum';
import * as appointmentUtilities from '../../../../utilities/appointmentUtilities';
import * as CommonUtilities from '../../../../utilities/commonUtilities';
import * as messageUtilities from '../../../../utilities/messageUtilities';
import _ from 'lodash';
import CIMSCheckBox from '../../../../components/CheckBox/CIMSCheckBox';

const sysRatio = CommonUtilities.getSystemRatio();
const unit = CommonUtilities.getResizeUnit(sysRatio);

const styles = (theme) => ({
    paperRoot: {
        backgroundColor: theme.palette.dialogBackground,
        padding: 4
    },
    iconBtnRoot: {
        padding: 0,
        marginLeft: 4,
        color: theme.palette.white
    },
    formlabelRoot: {
        marginLeft: 16
    },
    tableGrid: {
        backgroundColor: '#fff'
    },
    timeSlotBackdropRoot: {
        zIndex: 1200
    },
    timeslotModalRoot: {
        backgroundColor: '#fff',
        zIndex: 1201
    },
    confirmedPanel: {
        padding: '16px 18px',
        minHeight: 600 * unit
    },
    confirmedGrid: {
        borderBottom: '1px solid #b8bcb9',
        paddingTop: '10px'
    },
    customTableHeadRow: {
        fontWeight: 400,
        height: 40 * unit
    },
    customTableBodyCell: {
        fontSize: '14px'
    },
    highLineRowRoot: {
        '& td': {
            color: '#0579c8',
            fontStyle: 'italic'
        }
    },
    buttonRoot: {
        margin: 2,
        padding: 0,
        height: 35 * unit
    },
    tableTitle: {
        fontWeight: 500,
        color: theme.palette.white
    },
    tableContainer: {
        padding: 2
    },
    gridTitle: {
        padding: '4px 0px'
    },
    contactHistoryIconButton: {
        padding: 4
    }
});

class HistoryGrid extends Component {
    constructor(props) {
        super(props);

        this.state = {
            rows: appointmentUtilities.getBookingAppointmentListTableHeader(Enum.APPOINTMENT_LIST_TYPE.APPOINTMENT_HISTORY, this.contactHistoryRender),
            options: {
                rowExpand: true,
                headRowStyle: this.props.classes.customTableHeadRow,
                bodyCellStyle: this.props.classes.customTableBodyCell,
                customRowStyle: (rowData) => {
                    let className = '';
                    let currentAppt = this.props.currentSelectedApptInfo;
                    if (currentAppt && rowData.appointmentId === currentAppt.appointmentId) {
                        className = `${className} ${this.props.classes.highLineRowRoot}`;
                    }
                    return className;
                },
                rowsPerPage: 10,
                rowsPerPageOptions: [10],
                onSelectIdName: 'appointmentId',
                onSelectedRow: (rowId, rowData, selectedData) => {
                    const selected = selectedData.length === 0 ? null : selectedData[0];
                    this.setState({ currentSelectedAppt: selected });
                },
                onRowDoubleClick: (rowData) => {
                    if (moment(rowData.appointmentDate).isSameOrAfter(moment(), 'day') &&
                        rowData.statusCd === 'A' &&
                        !this.props.isEditingAppt &&
                        !this.props.isWalkIn &&
                        !this.props.bookingConfirmed) {
                        this.clearSelected();
                        this.handleEditAppointment(rowData);
                    }
                },
                isRowDisabled: (rowData) => {
                    return rowData.appointmentDate && moment(rowData.appointmentDate).isBefore(moment(), 'day');
                }
            },
            tsRows: [
                { name: 'datetime', label: 'Date/Time', width: 150 },
                { name: 'booked', label: 'Booked', width: 100 },
                { name: 'remain', label: 'Remaining Quota', width: 100 }
            ],
            tsOptions: {
                rowExpand: true,
                headRowStyle: this.props.classes.customTableHeadRow,
                bodyCellStyle: this.props.classes.customTableBodyCell,
                rowsPerPage: 10,
                rowsPerPageOptions: [10],
                actions: [
                    {
                        name: 'Choose',
                        onClick: (data) => {
                            let bookTimeSlot = _.cloneDeep(this.props.bookTimeSlotData);
                            const index = this.props.changeEncounterIndex;
                            if (bookTimeSlot[index] && data) {
                                // bookTimeSlot[index].slotDate = moment(data.slotDate, Enum.DATE_FORMAT_EDMY_VALUE).format('YYYY-MM-DD');
                                bookTimeSlot[index].slotDate = moment(data.slotDate, Enum.DATE_FORMAT_EDMY_VALUE).format(Enum.DATE_FORMAT_EYMD_VALUE);
                                bookTimeSlot[index].startTime = data.startTime;
                                bookTimeSlot[index].timeDiff = 0;
                                bookTimeSlot[index].timeDiffUnit = '';
                                const version = bookTimeSlot[index].list[0].version;
                                bookTimeSlot[index].list = [{
                                    slotId: data.slotId,
                                    version: version
                                }];
                            }
                            this.props.updateState({
                                bookTimeSlotData: bookTimeSlot,
                                openConfirmDialog: true,
                                openTimeSlotModal: false
                            });
                        },
                        icon: <CalendarTodayRounded />
                    }
                ]
            },
            allServiceChecked: false,
            currentSelectedAppt: null,
            newPage: null
        };
    }

    componentDidMount() {
        this.getPatientAppointmentList();
    }

    componentDidUpdate(prevProps) {
        if (JSON.stringify(this.props.patientInfo) !== JSON.stringify(prevProps.patientInfo) && this.props.patientInfo && this.props.patientInfo.patientKey) {
            this.getPatientAppointmentList();
        }
        if (this.props.patientInfo === null) {
            this.props.updateState({ bookingConfirmed: false });
        }
        if (this.props.bookingConfirmed === true && this.props.bookingConfirmed !== prevProps.bookingConfirmed) {
            this.getPatientAppointmentList();
        }
    }

    contactHistoryRender = (value, rowData) => {
        if (rowData.hasNotify) {
            return (
                <IconButton
                    color="primary"
                    id={'bookingInformation_historyGrid_editContactHistoryBtn_' + rowData.appointmentId}
                    className={this.props.classes.contactHistoryIconButton}
                    onClick={() => { this.openContactHistoryDialog(rowData); }}
                >
                    <PlaylistAddCheck />
                </IconButton>
            );
        }
        return (
            <Link
                id={'bookingInformation_historyGrid_addContactHistoryBtn_' + rowData.appointmentId}
                style={{ textDecoration: 'underline' }}
                onClick={() => { this.openContactHistoryDialog(rowData); }}
            >Add</Link>
        );
    }

    openContactHistoryDialog = (rowData) => {
        this.props.openContactHistoryDialog({ contactHistoryOpen: true, rowData });
    }

    getPatientAppointmentList = (page = 1) => {
        const { patientInfo } = this.props;
        const { options, allServiceChecked } = this.state;
        if (patientInfo && patientInfo.patientKey && patientInfo.patientKey > 0) {
            let params = {
                patientKey: patientInfo.patientKey,
                allService: allServiceChecked,
                page: page,
                pageSize: options.rowsPerPage
            };
            this.apptHistoryTableRef.updatePage(page - 1);
            this.props.listAppointment(params);
        }
        this.clearSelected();
    }

    clearSelected = () => {
        this.apptHistoryTableRef.clearSelected();
        this.setState({ currentSelectedAppt: null });
    }

    handleTimeSlotModalOpen = () => {
        let params = _.cloneDeep(this.props.bookingData);
        params.page = 1;
        params.pageSize = this.state.tsOptions.rowsPerPage;
        params = appointmentUtilities.handleBookingDataToParams(params);
        this.props.listTimeSlot(params);
    }

    handleTimeSlotModalClose = () => {
        this.props.updateState({ timeSlotList: [], openTimeSlotModal: false });
    }

    handleAllServiceOnChange = (event) => {
        this.setState({ allServiceChecked: event.currentTarget.checked }, () => {
            this.getPatientAppointmentList();
        });
    }

    handleOnChangePage = (event, newPage, rowPerPage, name) => {
        this.setState({ newPage });
        if (name === 'timeSlotTable') {
            let params = _.cloneDeep(this.props.bookingData);
            params.page = newPage + 1;
            params.pageSize = rowPerPage;
            params = appointmentUtilities.handleBookingDataToParams(params);
            this.props.listTimeSlot(params);
        } else if (name === 'appointmentTable') {
            this.getPatientAppointmentList(newPage + 1);
        }
    }

    handleOnChangeRowPerPage = (event, page, rowPerPage, name) => {
        if (name === 'timeSlotTable') {
            let params = _.cloneDeep(this.props.bookingData);
            params.page = page + 1;
            params.pageSize = rowPerPage;
            params = appointmentUtilities.handleBookingDataToParams(params);
            this.props.listTimeSlot(params);
        } else if (name === 'appointmentTable') {
            this.getPatientAppointmentList(page + 1);
        }
    }

    getAppointmentNodata = () => {
        const { patientInfo, appointmentList } = this.props;
        if (patientInfo) {
            if (parseInt(patientInfo.deadInd)) {
                return messageUtilities.getMessageDescriptionByMsgCode('110240').replace('%PATIENT_CALL%', CommonUtilities.getPatientCall());
            }
            if (!(appointmentList && appointmentList.appointmentDtos && appointmentList.appointmentDtos.length > 0)) {
                return messageUtilities.getMessageDescriptionByMsgCode('110241').replace('%PATIENT_CALL%', CommonUtilities.getPatientCall());
            }
            return null;
        } else {
            return messageUtilities.getMessageDescriptionByMsgCode('110242').replace('%PATIENT_CALL%', CommonUtilities.getPatientCall());
        }
    }

    printReportSingleSlip = (appointment) => {
        let reportParam = {
            allService: false,
            appointmentId: appointment.appointmentId,
            clinicCd: appointment.clinicCd,
            encounterTypeCd: appointment.encounterTypeCd,
            patientKey: this.props.patientInfo.patientKey,
            subEncounterTypeCd: appointment.subEncounterTypeCd
        };
        this.props.getAppointmentReport({ reportType: 'Single', reportParam });
    }
    printReportMultipleSlip = () => {
        let reportParam = {
            allService: this.state.allServiceChecked,
            appointmentId: this.props.appointmentList.appointmentDtos[0].appointmentId,
            clinicCd: this.props.appointmentList.appointmentDtos[0].clinicCd,
            encounterTypeCd: this.props.appointmentList.appointmentDtos[0].encounterTypeCd,
            patientKey: this.props.patientInfo.patientKey,
            subEncounterTypeCd: this.props.appointmentList.appointmentDtos[0].subEncounterTypeCd
        };
        this.props.getAppointmentReport({ reportType: 'Multiple', reportParam });
    }

    getListApptPara = () => {
        let para = {
            patientKey: this.props.patientInfo.patientKey,
            allService: this.state.allServiceChecked,
            page: this.apptHistoryTableRef.getPage() + 1,
            pageSize: this.apptHistoryTableRef.getPageSize()
        };
        return para;
    }

    markCurrentSelectedAppt = (data) => {
        let apptId = data.appointmentId;
        this.setState({ currentSelectedApptId: apptId });
    }

    handleCancelAppointment = (data) => {
        let listApptPara = this.getListApptPara();
        this.props.cancelAppointment(data, listApptPara);
    }

    handleEditAppointment = (data) => {
        let listApptPara = this.getListApptPara();
        this.props.editAppointment(data, listApptPara);
        this.props.setBookingData();
    }

    refreshListAppointment = () => {
        this.getPatientAppointmentList(this.state.newPage + 1);
    }

    render() {
        const { classes, patientInfo, timeSlotList, appointmentList, isEditingAppt, isWalkIn, bookingConfirmed } = this.props;
        const { currentSelectedAppt } = this.state;
        // const { bookingData } = this.props;
        // let hkidOrDocNo = '', engSurname = '', engGivename = '', phone = '';
        // if (patientInfo) {
        //     hkidOrDocNo = patientUtilities.getHkidOrDocNo(patientInfo);
        //     engSurname = patientInfo.engSurname || '';
        //     engGivename = patientInfo.engGivename || '';
        //     phone = patientInfo.phoneNo || '';
        // }
        let nodataMessage = this.getAppointmentNodata();
        return (
            <Paper className={classes.paperRoot}>
                <Grid
                    container
                    item
                    justify="space-around"
                    alignItems="center"
                    wrap="nowrap"
                    className={classes.gridTitle}
                    style={{ display: !this.props.openTimeSlotModal ? '' : 'none' }}
                >
                    <Grid style={{ flex: 1 }} item container alignItems="flex-end">
                        <Typography className={classes.tableTitle}>Appointment History</Typography>
                    </Grid>
                    <Grid item>
                        <FormControlLabel
                            className={classes.formlabelRoot}
                            control={
                                <CIMSCheckBox
                                    checked={this.state.allServiceChecked}
                                    onChange={this.handleAllServiceOnChange}
                                    color="primary"
                                    className={classes.iconBtnRoot}
                                />
                            }
                            label={<Typography className={classes.tableTitle}>All Services</Typography>}
                        />
                        <CIMSButton
                            id="booking_history_editBtn"
                            classes={{ sizeSmall: classes.buttonRoot }}
                            disabled={
                                !currentSelectedAppt ||
                                moment(currentSelectedAppt.appointmentDate).isBefore(moment(), 'day') ||
                                currentSelectedAppt.statusCd !== 'A' ||
                                isEditingAppt ||
                                isWalkIn ||
                                bookingConfirmed
                            }
                            onClick={() => this.handleEditAppointment(this.state.currentSelectedAppt)}
                        >Edit</CIMSButton>
                        <CIMSButton
                            id="booking_history_deleteBtn"
                            classes={{ sizeSmall: classes.buttonRoot }}
                            disabled={
                                !currentSelectedAppt ||
                                moment(currentSelectedAppt.appointmentDate).isBefore(moment(), 'day') ||
                                currentSelectedAppt.statusCd !== 'A' ||
                                isEditingAppt ||
                                isWalkIn ||
                                bookingConfirmed
                            }
                            onClick={() => this.handleCancelAppointment(this.state.currentSelectedAppt)}
                        >Delete</CIMSButton>
                        <CIMSButton
                            id="booking_history_printBtn"
                            classes={{ sizeSmall: classes.buttonRoot }}
                            disabled={this.props.isEditingAppt || !this.state.currentSelectedAppt}
                            onClick={() => this.printReportSingleSlip(this.state.currentSelectedAppt)}
                        >Print</CIMSButton>
                        <CIMSButton
                            id="booking_history_printAllBtn"
                            classes={{ sizeSmall: classes.buttonRoot }}
                            disabled={!patientInfo || !appointmentList || appointmentList.length <= 0}
                            onClick={this.printReportMultipleSlip}
                        >Print All</CIMSButton>
                    </Grid>
                </Grid>
                <Grid container className={classes.tableGrid} >
                    {/* <Grid item container style={{ display: !this.props.openTimeSlotModal && !this.props.bookingConfirmed ? '' : 'none' }}> */}
                    <Grid
                        item
                        container
                        className={classes.tableContainer}
                        style={{ display: !this.props.openTimeSlotModal ? '' : 'none' }}
                    >
                        <CIMSTable
                            innerRef={ref => this.apptHistoryTableRef = ref}
                            rows={this.state.rows}
                            data={patientInfo && patientInfo.deadInd === '0' && appointmentList ? appointmentList.appointmentDtos : null}
                            options={this.state.options}
                            remote
                            totalCount={patientInfo && appointmentList ? appointmentList.totalNum : 0}
                            nodataMessage={nodataMessage}
                            onChangePage={(...args) => this.handleOnChangePage(...args, 'appointmentTable')}
                            onChangeRowsPerPage={(...args) => this.handleOnChangeRowPerPage(...args, 'appointmentTable')}
                        />
                    </Grid>
                    {/* <Grid item container style={{ display: this.props.openTimeSlotModal && !this.props.bookingConfirmed ? '' : 'none' }}> */}
                    <Grid item container style={{ display: this.props.openTimeSlotModal ? '' : 'none' }}>
                        <Backdrop className={classes.timeSlotBackdropRoot} open={this.props.openTimeSlotModal} />
                        <Grid item container justify="flex-end" className={classes.timeslotModalRoot}>
                            <CIMSButton
                                id="btn_appointment_booking_timeslot_cancel"
                                classes={{ sizeSmall: classes.buttonRoot }}
                                onClick={this.handleTimeSlotModalClose}
                            >Cancel</CIMSButton>
                            <CIMSTable
                                rows={this.state.tsRows}
                                data={timeSlotList ? timeSlotList.slotForBookingDtos : null}
                                options={this.state.tsOptions}
                                remote
                                totalCount={timeSlotList ? timeSlotList.totalNum : 0}
                                onChangePage={(...args) => this.handleOnChangePage(...args, 'timeSlotTable')}
                                onChangeRowsPerPage={(...args) => this.handleOnChangeRowPerPage(...args, 'timeSlotTable')}
                            />
                        </Grid>
                    </Grid>
                    {/* <Grid item container direction="column" className={classes.confirmedPanel} style={{ display: this.props.bookingConfirmed && !this.props.openTimeSlotModal ? '' : 'none' }}>
                        <Typography variant="h6" style={{ textDecoration: 'underline' }}>Booking Confirmed</Typography>
                        {
                            patientInfo ?
                                <Grid item container className={classes.confirmedGrid}>
                                    <Grid item xs={4}><Typography variant="subtitle1">{CommonUtilities.getPatientCall()}</Typography></Grid>
                                    <Grid item container xs={8} direction="column">
                                        <Typography variant="body1">{hkidOrDocNo}</Typography>
                                        <Typography variant="body1">{`${engSurname}, ${engGivename}`}</Typography>
                                        <Typography variant="body1">{`Phone: ${phone}`}</Typography>
                                    </Grid>
                                </Grid> : null
                        }
                        {
                            bookingData && bookingData.encounterTypes.length > 0 ?
                                <Grid item container className={classes.confirmedGrid}>
                                    <Grid item xs={4}><Typography variant="subtitle1">Appointment</Typography></Grid>
                                    <Grid item container xs={8} direction="column">
                                        <Typography variant="body1">{bookingData.clinicCd}</Typography>
                                        <Typography variant="body1">{`${bookingData.encounterTypes[0].encounterTypeCd} - ${bookingData.encounterTypes[0].subEncounterTypeCd}`}</Typography>
                                        <Typography variant="body1">{`${moment(bookingData.encounterTypes[0].appointmentDate).format(Enum.DATE_FORMAT_EDMY_VALUE)} ${moment(bookingData.encounterTypes[0].appointmentTime).format('HH:mm')}`}</Typography>
                                    </Grid>
                                </Grid> : null
                        }
                    </Grid> */}
                </Grid>
            </Paper>
        );
    }
}

export default withStyles(styles)(HistoryGrid);