import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import {
    Grid,
    Typography,
    IconButton,
    DialogContent,
    DialogActions,
    // List,
    // ListItem,
    // ListItemText,
    FormHelperText
} from '@material-ui/core';
import CIMSButton from '../../../components/Buttons/CIMSButton';
import CIMSTypography from '../../../components/InputLabel/CIMSTypography';
import SelectFieldValidator from '../../../components/FormValidator/SelectFieldValidator';
import TextFieldValidator from '../../../components/FormValidator/TextFieldValidator';
import TimeFieldValidator from '../../../components/FormValidator/TimeFieldValidator';
import DateFieldValidator from '../../../components/FormValidator/DateFieldValidator';
import CIMSMultiTextField from '../../../components/TextField/CIMSMultiTextField';

import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import CIMSDialog from '../../../components/Dialog/CIMSDialog';
import { Add, Remove } from '@material-ui/icons';
import Enum from '../../../enums/enum';
import * as appointmentUtilities from '../../../utilities/appointmentUtilities';
import CIMSTable from '../../../components/Table/CIMSTable';
import CIMSPromptDialog from '../../../components/Dialog/CIMSPromptDialog';
import { CalendarTodayRounded } from '@material-ui/icons';
import {
    resetInfoAll,
    appointmentBook,
    listTimeSlot,
    getEncounterTypeList,
    getClinicList,
    bookConfirm,
    bookConfirmWaiting,
    updateState,
    updateField,
    listRemarkCode
} from '../../../store/actions/appointment/booking/bookingAnonymousAction';
import {
    openCommonCircular,
    closeCommonCircular
} from '../../../store/actions/common/commonAction';
import { openCommonMessage } from '../../../store/actions/message/messageAction';
import moment from 'moment';
import _ from 'lodash';
import CommonMessage from '../../../constants/commonMessage';
import CommonRegex from '../../../constants/commonRegex';
import ValidatorEnum from '../../../enums/validatorEnum';
import { initEncounterType } from '../../../constants/appointment/bookingInformation/bookingInformationConstants';
import * as commonUtilities from '../../../utilities/commonUtilities';
import EditModeMiddleware from '../../compontent/editModeMiddleware';
import { deleteTabs, updateCurTab } from '../../../store/actions/mainFrame/mainFrameAction';
import { getPatientList } from '../../../store/actions/patient/patientSpecFunc/patientSpecFuncAction';
import accessRightEnum from '../../../enums/accessRightEnum';
import { listSeachLogic } from '../../../store/actions/appointment/booking/bookingAction';
import * as CommonUtil from '../../../utilities/commonUtilities';
import * as AppointmentUtilities from '../../../utilities/appointmentUtilities';
import RequiredIcon from '../../../components/InputLabel/RequiredIcon';
import CIMSFormLabel from '../../../components/InputLabel/CIMSFormLabel';

const styles = theme => ({
    maintitleRoot: {
        paddingTop: '6px',
        fontSize: '14pt',
        fontWeight: 600
    },
    addEncounterContainer: {
        marginTop: '8px',
        marginBottom: '8px'
    },
    bookingContainerItem: {
        border: '1px solid rgba(0, 0, 0, 0.23)',
        padding: '0px 18px 16px 18px',
        borderRadius: '4px',
        marginBottom: '2px'
    },
    addButtonRoot: {
        borderRadius: '0%',
        padding: '0px'
    },
    dialogContent: {
        textAlign: 'center',
        padding: '0px 6px'
    },
    dialogErrorContent: {
        padding: '6px 6px',
        textAlign: 'center',
        backgroundColor: theme.palette.error.main,
        color: theme.palette.background.default
    },
    dialogAction: {
        justifyContent: 'center'
    },
    errorFieldNameText: {
        fontSize: '12px',
        wordBreak: 'break-word',
        color: '#fd0000'
    },
    customTableHeadCell: {
        fontWeight: 400
    },
    customTableBodyCell: {
        fontSize: '14px'
    },
    dialongContentRoot: {
        padding: '10px 0px'
    },
    formLabelContainer: {
        paddingTop: 15,
        paddingBottom: 15
    }
});

class BookingAnonymousInformation extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errorMessageList: [],
            clinicCd_valid: true,
            clinicCd_errMsg: '',
            tsRows: [
                { name: 'datetime', label: 'Date/Time', width: 150 },
                { name: 'booked', label: 'Booked', width: 100 },
                { name: 'remain', label: 'Remaining Quota', width: 100 }
            ],
            tsOptions: {
                rowExpand: true,
                headCellStyle: this.props.classes.customTableHeadCell,
                bodyCellStyle: this.props.classes.customTableBodyCell,
                customRowStyle: () => {
                    return this.props.classes.customTableRow;
                },
                rowsPerPage: 10,
                rowsPerPageOptions: [10],
                actions: [
                    {
                        name: 'Choose',
                        onClick: (data) => {
                            let bookTimeSlot = _.cloneDeep(this.props.bookTimeSlotData);
                            const index = this.props.changeEncounterIndex;
                            if (bookTimeSlot[index] && data) {
                                // bookTimeSlot[index].slotDate = moment(data.slotDate, Enum.DATE_FORMAT_EDMY_VALUE).format('YYYY-MM-DD');
                                bookTimeSlot[index].slotDate = moment(data.slotDate, Enum.DATE_FORMAT_EDMY_VALUE).format(Enum.DATE_FORMAT_EYMD_VALUE);
                                bookTimeSlot[index].startTime = data.startTime;
                                bookTimeSlot[index].timeDiff = 0;
                                bookTimeSlot[index].timeDiffUnit = '';
                                const version = bookTimeSlot[index].list[0].version;
                                bookTimeSlot[index].list = [{
                                    slotId: data.slotId,
                                    version: version
                                }];
                            }
                            this.props.updateState({
                                bookTimeSlotData: bookTimeSlot,
                                openConfirmDialog: true,
                                openTimeSlotModal: false,
                                showTimeSlotListDialog: false
                            });
                        },
                        icon: <CalendarTodayRounded />,
                        width: 150
                    }
                ],
                quotaTypeList: []
            }
        };
    }

    UNSAFE_componentWillMount() {

        const where = { serviceCd: this.props.serviceCd, clinicCd: this.props.clinicCd };
        const defaultQuotaDesc = CommonUtil.getPriorityConfig(Enum.CLINIC_CONFIGNAME.QUOTA_TYPE_DESC, this.props.clinicConfig, where);
        const quotaArr = defaultQuotaDesc.configValue ? defaultQuotaDesc.configValue.split('|') : null;
        let quotaTypeList = CommonUtil.transformToMap(quotaArr);
        quotaTypeList.push({
            code: 'U',
            engDesc: 'Urgent'
        });

        this.setState({ quotaTypeList });
    }

    componentDidMount() {
        this.props.resetInfoAll();
        this.props.getClinicList(this.props.serviceCd);
        this.props.listSeachLogic();
        this.props.listRemarkCode();
        this.loadDefaultEncounterCd();
        this.props.updateCurTab('F029', this.doClose);
    }

    componentWillUnmount() {
        this.props.resetInfoAll();
    }

    doClose = (callback) => {
        const { bookingConfirmed } = this.props;
        if (!bookingConfirmed) {
            this.props.openCommonMessage({
                msgCode: '110018',
                btnActions: {
                    btn1Click: () => {
                        callback(true);
                    }
                }
            });
        }
        else {
            callback(true);
        }

    }

    loadDefaultEncounterCd = () => {
        let where = { serviceCd: this.props.serviceCd, clinicCd: this.props.clinicCd };
        let defaultEncounterCd = commonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.DEFAULT_ENCOUNTER_CD, this.props.clinicConfig, where);
        this.props.updateState({ defaultEncounterCd: defaultEncounterCd.configValue });
    }

    handleBookClick = () => {
        this.props.openCommonCircular();
        this.refs.form.submit();
    }

    handleBooking = () => {
        this.props.closeCommonCircular();
        let params = _.cloneDeep(this.props.bookingData);
        params = appointmentUtilities.handleBookingDataToParams(params, 'N', this.props.openCommonMessage);
        if (!params) return;
        delete params.page;
        delete params.pageSize;
        if (params && params.encounterTypes) {
            //let errortime = [];
            // for (let i = 0; i < params.encounterTypes.length; i++) {
            //     let encounterType = params.encounterTypes[i];
            //     // if (encounterType.appointmentDate &&
            //     //     encounterType.appointmentTime &&
            //     //     appointmentUtilities.isExpireTime(encounterType.appointmentDate, `${encounterType.appointmentDate} ${encounterType.appointmentTime}`)) {
            //     //     errortime.push(i + 1);
            //     // }
            //     // if (encounterType.appointmentTime && encounterType.appointmentTime < moment().format('HH:mm')) {
            //     //     encounterType.appointmentTime = moment().format('HH:mm');
            //     // }
            //     // else if (!encounterType.appointmentTime) {
            //     //     errortime.push(i + 1);
            //     // }
            //     // if (encounterType.appointmentTime && encounterType.appointmentDate) {
            //     //     if (moment(encounterType.appointmentDate).format(Enum.DATE_FORMAT_EDMY_VALUE) === moment().format(Enum.DATE_FORMAT_EDMY_VALUE)) {
            //     //         if (encounterType.appointmentTime <= moment().format('HH:mm')) {
            //     //             encounterType.appointmentTime = moment().format('HH:mm');
            //     //         }
            //     //     }
            //     // }
            //     // else {
            //     //     errortime.push(i + 1);
            //     // }
            //     if (encounterType.appointmentDate) {
            //         if (moment(encounterType.appointmentDate).format(Enum.DATE_FORMAT_EDMY_VALUE) === moment().format(Enum.DATE_FORMAT_EDMY_VALUE)) {
            //             if (encounterType.appointmentTime <= moment().format('HH:mm')) {
            //                 encounterType.appointmentTime = moment().format('HH:mm');
            //             }
            //         }
            //         else if (moment(encounterType.appointmentDate).set({ hours: 0, minute: 0, second: 0 }) > moment().set({ hours: 0, minute: 0, second: 0 })) {
            //             if (!encounterType.appointmentTime) {
            //                 encounterType.appointmentTime = moment(encounterType.appointmentDate).format('HH:mm');
            //             }
            //         }
            //         else if (moment(encounterType.appointmentDate).set({ hours: 0, minute: 0, second: 0 }) < moment().set({ hours: 0, minute: 0, second: 0 })) {
            //             let fileData = {};
            //             fileData.bookFinishOpenDialog = true;
            //             fileData.bookFinishErrorMessage = 'Please do not choose expiration Date!';
            //             fileData.bookFinishErrorData = null;
            //             this.props.updateState(fileData);
            //             return;
            //         }
            //     }
            // }
            this.props.appointmentBook(params);
        }
    }

    handleBookConfirm = () => {
        let fileData = {};
        let patientInfo = this.props.patientInfo;
        let submitParams = {};
        this.props.updateState({ handlingBooking: true });
        if (!patientInfo) {
            this.props.openCommonMessage({
                msgCode: '110205',
                params: [{ name: 'PATIENT_CALL', value: commonUtilities.getPatientCall() }]
            });
            fileData.openConfirmDialog = false;
        } else if (parseInt(patientInfo.deadInd)) {
            this.props.openCommonMessage({
                msgCode: '110206',
                params: [{ name: 'PATIENT_CALL', value: commonUtilities.getPatientCall() }]
            });
            fileData.openConfirmDialog = false;
        } else {
            let bookingData = _.cloneDeep(this.props.bookingData);
            let bookTimeSlotList = _.cloneDeep(this.props.bookTimeSlotData);
            let params = [];

            for (let i = 0; i < bookingData.encounterTypes.length; i++) {
                let obj = {
                    appointmentDateVo: bookTimeSlotList[i].slotDate,
                    appointmentTime: bookTimeSlotList[i].startTime,
                    appointmentTypeCd: bookingData.encounterTypes[i].appointmentTypeCd,
                    bookingUnit: bookTimeSlotList[i].list.length,
                    caseTypeCd: bookingData.encounterTypes[i].caseTypeCd || 'N',
                    clinicCd: bookingData.clinicCd,
                    patientKey: patientInfo.patientKey === '0' ? '' : patientInfo.patientKey,
                    encounterTypeCd: bookingData.encounterTypes[i].encounterTypeCd,
                    subEncounterTypeCd: bookingData.encounterTypes[i].subEncounterTypeCd,
                    slots: bookTimeSlotList[i].list,
                    remarkId: bookingData.encounterTypes[i].remarkId,
                    memo: bookingData.encounterTypes[i].memo
                };
                params.push(obj);

                bookingData.encounterTypes[i].bookingUnit = obj.bookingUnit;
                bookingData.encounterTypes[i].appointmentDate = moment(bookTimeSlotList[i].slotDate);
                let timeSplit = bookTimeSlotList[i].startTime.split(':');
                bookingData.encounterTypes[i].appointmentTime = moment().set({ 'hours': timeSplit[0], 'minutes': timeSplit[1] });
                bookingData.encounterTypes[i].elapsedPeriod = '';
            }
            submitParams = {
                appointmentDtos: params
            };

            if (patientInfo.patientKey === '0') {
                let anonymouPatientInfo = {
                    docTypeCd: patientInfo.docTypeCd,
                    engGivename: patientInfo.givenName,
                    engSurname: patientInfo.surname,
                    hkid: patientInfo.hkid,
                    otherDocNo: patientInfo.otherDocNo,
                    patientKey: 0,
                    patientPhones: [
                        {
                            phoneNo: patientInfo.mobile,
                            countryCd: patientInfo.countryCd
                        }
                    ]
                };
                submitParams = {
                    appointmentDtos: params,
                    anonymousPatientDto: anonymouPatientInfo
                };
            }
            if (this.props.waitingList) {
                submitParams.waitingListDto = this.props.waitingList;
                this.props.bookConfirmWaiting(submitParams, bookingData, this.handleCloseTab);
            } else {
                this.props.bookConfirm(submitParams, bookingData, this.handleCloseTab);
            }
        }
        this.props.updateState(fileData);
    }

    handleBookSearch = (e, index) => {
        let params = _.cloneDeep(this.props.bookingData);
        params.page = 1;
        params.pageSize = 10;
        params = appointmentUtilities.handleBookingDataToParams(params, 'N');
        this.props.updateState({ openConfirmDialog: false, showTimeSlotListDialog: true, changeEncounterIndex: index, handlingBooking: false });
        this.props.listTimeSlot(params);
    }

    handleBookCancel = () => {
        this.props.updateState({ openConfirmDialog: false, handlingBooking: false });
    }

    handleRemoveEncounter = (e, index) => {
        let bookingData = _.cloneDeep(this.props.bookingData);
        if (bookingData.encounterTypes && bookingData.encounterTypes.length > 1) {
            bookingData.encounterTypes.splice(index, 1);
        }
        this.props.updateState({ bookingData });
        let { errorMessageList } = this.state;
        if (errorMessageList && errorMessageList[index]) {
            errorMessageList.splice(index, 1);
        }
        this.setState({ errorMessageList });
    }

    handleAddEncounter = () => {
        let bookingData = _.cloneDeep(this.props.bookingData);
        let newData = _.cloneDeep(initEncounterType);
        let defaultEncounterCd = this.props.defaultEncounterCd;
        newData.encounterTypeList = bookingData.encounterTypes[0].encounterTypeList;
        newData.subEncounterTypeList = bookingData.encounterTypes[0].subEncounterTypeList;
        bookingData.encounterTypes.push(newData);
        bookingData = appointmentUtilities.initBookData(bookingData, defaultEncounterCd);
        this.props.updateState({ bookingData });
    }

    handleSelectFieldChange = (e, name) => {
        let bookingData = _.cloneDeep(this.props.bookingData);
        bookingData[name] = e.value;
        if (name === 'clinicCd') {
            this.props.getEncounterTypeList(e.value, this.props.serviceCd, e.value, this.props.clinicConfig);
        }
        this.props.updateState({ bookingData });
    }

    handleTextFieldChange = (e, name) => {
        let bookingData = _.cloneDeep(this.props.bookingData);
        bookingData[name] = e.target.value;
        this.props.updateState({ bookingData });
    }

    updateEncounterField = (name, value, index) => {
        let bookingData = _.cloneDeep(this.props.bookingData);
        bookingData.encounterTypes[index][name] = value;
        if (name === 'encounterTypeCd') {
            let encounterSelected = bookingData.encounterTypes[index].encounterTypeList.find(item => item.encounterTypeCd === value);
            bookingData.encounterTypes[index].subEncounterTypeList = encounterSelected ? encounterSelected.subEncounterTypeList || [] : [];
            bookingData.encounterTypes[index].subEncounterTypeCd = '';
        }

        if ((name === 'appointmentDate' || name === 'appointmentTime') && value) {
            bookingData.encounterTypes[index]['elapsedPeriod'] = '';
            bookingData.encounterTypes[index]['elapsedPeriodUnit'] = '';
            if (name === 'appointmentDate' && !bookingData.encounterTypes[index]['appointmentTime']) {
                if (moment(value).isAfter(moment(), 'day')) {
                    bookingData.encounterTypes[index]['appointmentTime'] = moment(value).set({ hours: 0, minute: 0, second: 0 });
                } else if (moment(value).isSame(moment(), 'day')) {
                    bookingData.encounterTypes[index]['appointmentTime'] = moment().set({ hours: 0, minute: 0, second: 0 });
                }
            }
        }

        if (name === 'elapsedPeriod' && value) {
            bookingData.encounterTypes[index]['appointmentDate'] = null;
            bookingData.encounterTypes[index]['appointmentTime'] = null;
        }

        if (name === 'elapsedPeriodUnit' && value && !bookingData.encounterTypes[index]['elapsedPeriod']) {
            bookingData.encounterTypes[index][name] = '';
        }
        this.props.updateState({ bookingData }, index);
    }

    handleEncounterSelectFieldChange = (e, name, index) => {
        this.updateEncounterField(name, e.value, index);
    }

    handleEncounterDateFieldChange = (e, name, index) => {
        // if (name === 'appointmentTime') {
        //     if (e && moment(e).diff(moment()) < 0) {
        //         this.props.openCommonMessage({
        //             msgCode: '110207'
        //         });
        //         return;
        //     }
        // }
        this.updateEncounterField(name, e, index);
    }

    //eslint-disable-next-line
    handleEncounterTimeOnChange = (e, name, index, date) => {
        // if (date && e && date !== 'Invalid date' && e !== 'Invalid date') {
        //     if (appointmentUtilities.isExpireTime(date, e)) {
        //         this.props.openCommonMessage({
        //             msgCode: '110207'
        //         });
        //         return;
        //     }
        // }
        this.updateEncounterField(name, e, index);
    }

    handleEncounterTextFieldChange = (e, name, index) => {
        let value = e.target.value;
        if (name === 'elapsedPeriod' || name === 'bookingUnit') {
            let reg = new RegExp(CommonRegex.VALIDATION_REGEX_NOT_NUMBER);
            if (reg.test(value)) {
                return;
            }
        }
        if (name === 'memo') {
            // eslint-disable-next-line no-constant-condition
            while (true) {
                const byteSize = commonUtilities.getUTF8StringLength(e.target.value);
                if (byteSize <= e.target.maxLength) {
                    break;
                }
                e.target.value = e.target.value.substr(0, e.target.value.length - 1);
            }
        }
        this.updateEncounterField(name, e.target.value, index);
    }

    handleEncounterTextFieldOnBlur = (e, name, index) => {
        let value = e.target.value;
        if (name === 'bookingUnit' && !parseInt(value || 0)) {
            value = 1;
        }
        this.updateEncounterField(name, value, index);
    }

    handleSubmitError = () => {
        this.props.closeCommonCircular();
        // adandon: no need to show
        // this.props.openCommonMessage({
        //     msgCode: '110209'
        // });
    }

    validatorListener = (e, message, name, index) => {
        let { errorMessageList } = this.state;
        if (!errorMessageList[index]) {
            errorMessageList[index] = [];
        }
        let currentErrorList = errorMessageList[index];
        if (e && message === true) {
            currentErrorList = currentErrorList.filter((item) => {
                return item.fieldName !== name;
            });
        } else {
            let errorObj = currentErrorList.find(item => item.fieldName === name);
            if (errorObj) {
                errorObj.errMsg = message;
            } else {
                currentErrorList.push({ fieldName: name, errMsg: message });
            }
        }
        errorMessageList[index] = currentErrorList;
        this.setState({ errorMessageList });
    }

    validatorOtherListener = (isValid, message, name) => {
        if (isValid) {
            this.setState({ [name + '_valid']: true, [name + '_errMsg']: '' });
        } else {
            this.setState({ [name + '_valid']: false, [name + '_errMsg']: message });
        }
    }

    handleCloseTab = () => {
        if (this.props.bookingConfirmed) {
            // let params = {
            //     type: 'AL',
            //     dateFrom: moment().format('YYYY-MM-DD'),
            //     dateTo: moment().format('YYYY-MM-DD')
            // };
            // this.props.getPatientList(params);
            this.props.deleteTabs(accessRightEnum.bookingAnonymous);
        }
    }

    handleTimeSlotListOnClose = () => {
        this.props.updateState({ showTimeSlotListDialog: false, openTimeSlotModal: false });
    }

    handleOnChangePage = (event, newPage, rowPerPage, name) => {
        if (name === 'timeSlotTable') {
            let params = _.cloneDeep(this.props.bookingData);
            params.page = newPage + 1;
            params.pageSize = rowPerPage;
            params = appointmentUtilities.handleBookingDataToParams(params);
            this.props.listTimeSlot(params);
        } else if (name === 'appointmentTable') {
            if (this.props.patientInfo && this.props.patientInfo.patientKey) {
                let params = {
                    patientKey: this.props.patientInfo.patientKey,
                    allService: this.state.allServiceChecked,
                    page: newPage + 1,
                    pageSize: rowPerPage
                };
                this.props.listAppointment(params);
            }
        }
    }

    handleOnChangeRowPerPage = (event, page, rowPerPage, name) => {
        if (name === 'timeSlotTable') {
            let params = _.cloneDeep(this.props.bookingData);
            params.page = page + 1;
            params.pageSize = rowPerPage;
            params = appointmentUtilities.handleBookingDataToParams(params);
            this.props.listTimeSlot(params);
        } else if (name === 'appointmentTable') {
            if (this.props.patientInfo && this.props.patientInfo.patientKey) {
                let params = {
                    patientKey: this.props.patientInfo.patientKey,
                    allService: this.state.allServiceChecked,
                    page: page + 1,
                    pageSize: rowPerPage
                };
                this.props.listAppointment(params);
            }
        }
    }

    handleBackToCalendar = () => {
        this.props.updateField({ showMakeAppointmentView: false });
    }

    render() {
        const { classes, clinicList, bookingData, bookTimeSlotData, patientInfo, timeSlotList } = this.props;
        const { errorMessageList } = this.state;
        return (
            <Grid container spacing={1}>
                <Grid item container direction="column" xs={12}>
                    <ValidatorForm ref="form" onSubmit={this.handleBooking} onError={this.handleSubmitError}>
                        <Grid item container xs={12}>
                            <Typography className={classes.maintitleRoot}>Appointment Booking</Typography>
                            <Grid item container alignItems="flex-end" justify="space-between">
                                <Grid item container xs>
                                </Grid>
                                {/* <Grid item container xs>
                                <CIMSTypography variant="subtitle1" tooltip="Clinic">Clinic</CIMSTypography>
                                <FormHelperText error={!this.state.clinicCd_valid} style={{ paddingLeft: '5px' }}>{this.state.clinicCd_errMsg}</FormHelperText>
                            </Grid> */}
                                {/* <CIMSButton
                                id="btn_appointment_booking_esc"
                                onClick={this.handleBackToCalendar}
                                disabled={this.props.waitingList ? true : false}
                            >Esc</CIMSButton> */}
                                {
                                    this.props.bookingConfirmed ?
                                        null
                                        :
                                        <div>
                                            <CIMSButton
                                                id="bookingAnonymous_btn_appointment_booking_booking"
                                                variant="contained"
                                                color="primary"
                                                size="small"
                                                style={{ marginRight: 0 }}
                                                onClick={this.handleBookClick}
                                                disabled={patientInfo && patientInfo.patientKey && !parseInt(patientInfo.deadInd) ? false : true}
                                            >Book</CIMSButton>
                                            <CIMSButton
                                                id="bookingAnonymous_btn_appointment_booking_booking"
                                                variant="contained"
                                                color="primary"
                                                size="small"
                                                style={{ marginRight: 0 }}
                                                onClick={() => { this.props.deleteTabs(accessRightEnum.bookingAnonymous); }}
                                            // disabled={patientInfo && patientInfo.patientKey && !parseInt(patientInfo.deadInd) ? false : true}
                                            >Close</CIMSButton>
                                        </div>

                                }
                            </Grid>
                            <SelectFieldValidator
                                id="bookingAnonymous_select_appointment_booking_clinic"
                                options={clinicList && clinicList.map(item => (
                                    { value: item.clinicCd, label: item.clinicName }
                                ))}
                                TextFieldProps={{
                                    variant: 'outlined',
                                    label: <>Clinic</>
                                }}
                                value={bookingData.clinicCd}
                                onChange={e => this.handleSelectFieldChange(e, 'clinicCd')}
                                placeholder=""
                                validators={['required']}
                                errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                validatorListener={(...args) => this.validatorOtherListener(...args, 'clinicCd')}
                                notShowMsg
                            />
                            <Grid item container direction="column" className={classes.addEncounterContainer}>
                                {bookingData && bookingData.encounterTypes && bookingData.encounterTypes.map((item, index) => (
                                    <CIMSFormLabel
                                        key={index}
                                        // fullWidth
                                        // labelText="Booking"
                                        className={classes.bookingContainerItem}
                                    >
                                        <Grid item container direction="column" key={index} spacing={1}>
                                            <Grid item container justify="flex-end">
                                                <IconButton
                                                    id="bookingAnonymous_removeEncounter"
                                                    className={classes.addButtonRoot}
                                                    disabled={bookingData.encounterTypes.length < 2}
                                                    color={bookingData.encounterTypes.length < 2 ? 'default' : 'primary'}
                                                    onClick={e => this.handleRemoveEncounter(e, index)}
                                                >
                                                    <Remove color={bookingData.encounterTypes.length < 2 ? 'disabled' : 'primary'} />
                                                </IconButton>
                                            </Grid>

                                            <Grid item container spacing={1}>
                                                <Grid item container xs={1} alignContent="center">
                                                    {/* <CIMSTypography noWrap tooltip="Encounter Type">Encounter</CIMSTypography> */}
                                                    <SelectFieldValidator
                                                        id={'bookingAnonymous_select_appointment_booking_encounter_type_' + index}
                                                        placeholder=""
                                                        options={item.encounterTypeList && item.encounterTypeList.map(item => (
                                                            { value: item.encounterTypeCd, label: item.encounterTypeCd, shortName: item.shortName }
                                                        ))}
                                                        TextFieldProps={{
                                                            variant: 'outlined',
                                                            label: <>Encounter<RequiredIcon /></>
                                                        }}
                                                        value={item.encounterTypeCd}
                                                        onChange={e => this.handleEncounterSelectFieldChange(e, 'encounterTypeCd', index)}
                                                        validators={['required']}
                                                        errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                        validatorListener={(...args) => this.validatorListener(...args, 'Encounter Type', index)}
                                                        notShowMsg
                                                    />
                                                </Grid>
                                                <Grid item container xs={2} alignContent="center">
                                                    {/* <CIMSTypography noWrap tooltip="Sub Encounter Type">Sub-encounter</CIMSTypography> */}
                                                    <SelectFieldValidator
                                                        id={'bookingAnonymous_select_appointment_booking_sub_encounter_type_' + index}
                                                        placeholder=""
                                                        options={item.subEncounterTypeList && item.subEncounterTypeList.map(item => (
                                                            { value: item.subEncounterTypeCd, label: item.subEncounterTypeCd, shortName: item.shortName }
                                                        ))}
                                                        TextFieldProps={{
                                                            variant: 'outlined',
                                                            label: <>Sub-encounter<RequiredIcon /></>
                                                        }}
                                                        value={item.subEncounterTypeCd}
                                                        onChange={e => this.handleEncounterSelectFieldChange(e, 'subEncounterTypeCd', index)}
                                                        validators={['required']}
                                                        errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                        validatorListener={(...args) => this.validatorListener(...args, 'Sub Encounter Type', index)}
                                                        notShowMsg
                                                    />
                                                </Grid>
                                                <Grid item container xs={3}>
                                                    {/* <CIMSTypography noWrap tooltip="Appointment Type">Appointment Type</CIMSTypography> */}
                                                    <CIMSFormLabel
                                                        fullWidth
                                                        labelText={<>{'Appointment Type'}<RequiredIcon /></>}
                                                        className={classes.formLabelContainer}
                                                    >
                                                        <Grid item container wrap="nowrap">
                                                            <Grid item xs={5} style={{ paddingRight: 5 }}>
                                                                <SelectFieldValidator
                                                                    id={'bookingAnonymous_select_appointment_booking_case_type_' + index}
                                                                    options={Enum.APPOINTMENT_TYPE_PERFIX.map(item => (
                                                                        { value: item.code, label: item.engDesc }
                                                                    ))}
                                                                    value={item.caseTypeCd || 'N'}
                                                                    placeholder=""
                                                                    onChange={e => this.handleEncounterSelectFieldChange(e, 'caseTypeCd', index)}
                                                                    validators={['required']}
                                                                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                                    validatorListener={(...args) => this.validatorListener(...args, 'Appointment Type First Select', index)}
                                                                    notShowMsg
                                                                />
                                                            </Grid>
                                                            <Grid item xs={7}>
                                                                <SelectFieldValidator
                                                                    id={'bookingAnonymous_select_appointment_booking_appointment_type_' + index}
                                                                    options={this.state.quotaTypeList.map(item => (
                                                                        { value: item.code, label: item.engDesc }
                                                                    ))}
                                                                    value={item.appointmentTypeCd}
                                                                    placeholder=""
                                                                    onChange={e => this.handleEncounterSelectFieldChange(e, 'appointmentTypeCd', index)}
                                                                    validators={['required']}
                                                                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                                    validatorListener={(...args) => this.validatorListener(...args, 'Appointment Type Second Select', index)}
                                                                    notShowMsg
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                    </CIMSFormLabel>
                                                </Grid>

                                                <Grid item container xs={3}>
                                                    {/* <CIMSTypography noWrap tooltip="Date/Time">Date/Time</CIMSTypography> */}
                                                    <CIMSFormLabel
                                                        fullWidth
                                                        labelText="Date/Time"
                                                        className={classes.formLabelContainer}
                                                    >
                                                        <Grid item container wrap="nowrap">
                                                            <Grid item container xs={7} style={{ paddingRight: 5 }}>
                                                                <DateFieldValidator
                                                                    style={{ width: 'inherit' }}
                                                                    id={'bookingAnonymous_date_appointment_booking_encounter_date_' + index}
                                                                    disablePast
                                                                    value={item.appointmentDate}
                                                                    placeholder=""
                                                                    onChange={e => this.handleEncounterDateFieldChange(e, 'appointmentDate', index)}
                                                                    isRequired={!item.elapsedPeriod}
                                                                    validatorListener={(...args) => this.validatorListener(...args, 'Appointment Date', index)}
                                                                    notShowMsg
                                                                />
                                                            </Grid>
                                                            <Grid item container xs={5}>
                                                                <TimeFieldValidator
                                                                    id={'bookingAnonymous_time_appointment_booking_encounter_time_' + index}
                                                                    value={item.appointmentTime}
                                                                    helperText=""
                                                                    placeholder=""
                                                                    onChange={e => this.handleEncounterTimeOnChange(e, 'appointmentTime', index, item.appointmentDate)}
                                                                    validators={item.elapsedPeriod ? [] : [ValidatorEnum.required]}
                                                                    errorMessages={item.elapsedPeriod ? [] : [CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                                    validatorListener={(...args) => this.validatorListener(...args, 'Appointment Time', index)}
                                                                    notShowMsg
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                    </CIMSFormLabel>
                                                </Grid>
                                                <Grid item container xs={3}>
                                                    {/* <CIMSTypography tooltip="Elapsed Period">Elapsed Period</CIMSTypography> */}
                                                    <CIMSFormLabel
                                                        fullWidth
                                                        labelText={<>{'Elapsed Period'}</>}
                                                        className={classes.formLabelContainer}
                                                    >
                                                        <Grid item container wrap="nowrap" spacing={1}>
                                                            <Grid item xs={6}>
                                                                <TextFieldValidator
                                                                    id={'bookingAnonymous_txt_appointment_booking_elapsed_period_num_' + index}
                                                                    fullWidth
                                                                    value={item.elapsedPeriod}
                                                                    // label={<>{'Elapsed Period'}</>}
                                                                    onChange={e => this.handleEncounterTextFieldChange(e, 'elapsedPeriod', index)}
                                                                    validators={item.appointmentDate || item.appointmentTime ? [] : [ValidatorEnum.required, ValidatorEnum.isPositiveInteger]}
                                                                    errorMessages={item.appointmentDate || item.appointmentTime ? [] : [CommonMessage.VALIDATION_NOTE_REQUIRED(), CommonMessage.VALIDATION_NOTE_POSITIVE_INTEGER()]}
                                                                    validatorListener={(...args) => this.validatorListener(...args, 'Elapsed Period', index)}
                                                                    notShowMsg
                                                                    inputProps={{
                                                                        maxLength: 2
                                                                    }}
                                                                />
                                                            </Grid>
                                                            <Grid item xs={6}>
                                                                <SelectFieldValidator
                                                                    id={'bookingAnonymous_select_appointment_booking_elapsed_period_type_' + index}
                                                                    options={Enum.ELAPSED_PERIOD_TYPE.map(item => (
                                                                        { value: item.code, label: item.engDesc }
                                                                    ))}
                                                                    fullWidth
                                                                    placeholder=""
                                                                    value={item.elapsedPeriodUnit}
                                                                    onChange={e => this.handleEncounterSelectFieldChange(e, 'elapsedPeriodUnit', index)}
                                                                    validators={item.elapsedPeriod ? [ValidatorEnum.required] : []}
                                                                    errorMessages={item.elapsedPeriod ? [CommonMessage.VALIDATION_NOTE_REQUIRED()] : []}
                                                                    validatorListener={(...args) => this.validatorListener(...args, 'Elapsed Period Unit', index)}
                                                                    notShowMsg
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                    </CIMSFormLabel>
                                                </Grid>

                                            </Grid>
                                            <Grid item container spacing={1}>

                                                <Grid item container xs={3}>
                                                    {/* <CIMSTypography tooltip="Search Logic">Search Logic</CIMSTypography> */}
                                                    <SelectFieldValidator
                                                        // id={'select_appointment_booking_session_' + index}
                                                        // options={Enum.SESSION.map(item => (
                                                        //     { value: item.code, label: item.engDesc }
                                                        // ))}
                                                        // fullWidth
                                                        // placeholder=""
                                                        // value={item.session}
                                                        // onChange={e => this.handleEncounterSelectFieldChange(e, 'session', index)}
                                                        id={'bookingAnonymous_select_appointment_booking_searchLogic_' + index}
                                                        fullWidth
                                                        TextFieldProps={{
                                                            variant: 'outlined',
                                                            label: <>Search Logic<RequiredIcon /></>
                                                        }}
                                                        placeholder="W (Whole day)"
                                                        value={item.searchLogicCd}
                                                        options={this.props.searchLogicList && this.props.searchLogicList.map(item => (
                                                            { value: item.slotSearchLogicCd, label: `${item.slotSearchLogicCd} (${item.description})` }
                                                        ))}
                                                        //onBlur={e => this.matchUserSearchLogic(e.target.value, 'searchLogic', index)}
                                                        onChange={e => this.handleEncounterSelectFieldChange(e, 'searchLogicCd', index)}
                                                    />
                                                </Grid>
                                                <Grid item container xs={3}>
                                                    {/* <CIMSTypography tooltip="Booking Unit">Booking Unit</CIMSTypography> */}
                                                    <TextFieldValidator
                                                        id={'bookingAnonymous_select_appointment_booking_booking_unit_' + index}
                                                        fullWidth
                                                        value={item.bookingUnit}
                                                        label={<>{'Booking Unit'}<RequiredIcon /></>}
                                                        onChange={e => this.handleEncounterTextFieldChange(e, 'bookingUnit', index)}
                                                        onBlur={e => this.handleEncounterTextFieldOnBlur(e, 'bookingUnit', index)}
                                                        validators={['isPositiveInteger']}
                                                        errorMessages={[CommonMessage.VALIDATION_NOTE_POSITIVE_INTEGER()]}
                                                        validatorListener={(...args) => this.validatorListener(...args, 'Booking Unit', index)}
                                                        notShowMsg
                                                        inputProps={{
                                                            maxLength: 1
                                                        }}
                                                    />
                                                </Grid>
                                                <Grid item container xs={6}>
                                                    {/* <CIMSTypography tooltip="Remark">Remark</CIMSTypography> */}
                                                    <SelectFieldValidator
                                                        id={'bookingAnonymous_select_appointment_booking_remarkCode_' + index}
                                                        fullWidth
                                                        TextFieldProps={{
                                                            variant: 'outlined',
                                                            label: <>Remark</>
                                                        }}
                                                        value={item.remarkId}
                                                        options={this.props.remarkCodeList && this.props.remarkCodeList.map(item => (
                                                            { value: item.remarkId, label: `${item.remarkCd} (${item.description})` }
                                                        ))}
                                                        onChange={e => this.handleEncounterSelectFieldChange(e, 'remarkId', index)}
                                                        isDisabled={this.props.bookingConfirmed}
                                                    />
                                                </Grid>
                                            </Grid>
                                            <Grid item container spacing={1}>
                                                <Grid item container xs={12}>
                                                    {/* <CIMSTypography tooltip="Memo">Memo</CIMSTypography> */}
                                                    <CIMSMultiTextField
                                                        id={'bookingAnonymous_select_appointment_booking_memo_' + index}
                                                        fullWidth
                                                        label={'Memo'}
                                                        value={item.memo}
                                                        inputProps={{
                                                            maxLength: 500
                                                        }}
                                                        rows="2"
                                                        onChange={e => this.handleEncounterTextFieldChange(e, 'memo', index)}
                                                    />
                                                </Grid>
                                            </Grid>
                                            {
                                                errorMessageList[index] && errorMessageList[index].length > 0 ?
                                                    <Grid item container style={{ marginTop: '10px' }} id={'bookingAnonymous_invalid_input_error_message'}>
                                                        {
                                                            errorMessageList[index].map((item, i) => (
                                                                <Grid item container key={i} justify="flex-start">
                                                                    <Typography className={classes.errorFieldNameText}>{`${item.fieldName}: ${item.errMsg}`}</Typography>
                                                                </Grid>
                                                            ))
                                                        }
                                                    </Grid> : null
                                            }
                                        </Grid>
                                    </CIMSFormLabel>
                                ))}
                                {/* <Grid item container>
                                <IconButton
                                    className={classes.addButtonRoot}
                                    color="primary"
                                    onClick={this.handleAddEncounter}
                                    disabled={this.props.waitingList ? true : false}
                                >
                                    <Add color="primary" />
                                    <Typography>Please Click to add more Encounter Type</Typography>
                                </IconButton>
                            </Grid> */}
                            </Grid>
                            {/* <Grid item container justify="flex-start" spacing={2}>
                            <Grid item container direction="column" xs={4}>
                                <CIMSTypography variant="subtitle1" tooltip="No. of follow-up appts">No. of follow-up appts</CIMSTypography>
                                <TextFieldValidator
                                    id="bookingAnonymous_txt_appointment_booking_no_of_followup"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item container direction="column" xs={4}>
                                <CIMSTypography variant="subtitle1" tooltip="Appt. Interval">Appt. Interval</CIMSTypography>
                                <Grid item container spacing={1}>
                                    <Grid item xs={4}>
                                        <TextFieldValidator
                                            id="bookingAnonymous_txt_appointment_booking_appt"
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid item xs={8}>
                                        <SelectFieldValidator
                                            id="bookingAnonymous_select_appointment_booking_appt_type"
                                            placeholder=""
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item container>
                            <CIMSTypography variant="subtitle1" tooltip="Remark">Remark</CIMSTypography>
                            <TextFieldValidator
                                id="bookingAnonymous_txt_appointment_booking_remark"
                                fullWidth
                                value={bookingData.remark || ''}
                                onChange={(e) => { this.handleTextFieldChange(e, 'remark'); }}
                                inputProps={{
                                    maxLength: 165
                                }}
                            />
                        </Grid> */}
                        </Grid>
                    </ValidatorForm>
                </Grid>
                <CIMSDialog id="bookingAnonymous_appointment_booking_dialog1" dialogTitle="Appointment Booking" open={this.props.openConfirmDialog && bookTimeSlotData.length === 1} dialogContentProps={{ style: { minWidth: 400 } }}>
                    <DialogContent id={'appointment_booking_dialog1_description'} style={{ padding: 0 }}>
                        {/* {bookTimeSlotData && bookTimeSlotData[0] && bookTimeSlotData[0].timeDiff > 0 ? <Typography variant="subtitle2" className={classes.dialogErrorContent}>{`Available date is ${bookTimeSlotData[0].timeDiff} ${bookTimeSlotData[0].timeDiffUnit} later than given date`}</Typography> : null} */}
                        {bookTimeSlotData && bookTimeSlotData[0] && (bookTimeSlotData[0].dateDiff > 0) ?
                            <Typography variant="subtitle2" className={classes.dialogErrorContent}>
                                {/* {`Available date is ${bookTimeSlotData[0].timeDiff} ${bookTimeSlotData[0].timeDiffUnit} later than given date`} */}
                                {`Available date is ${bookTimeSlotData[0].dateDiff} Day(s) later than given date`}
                            </Typography> : null}
                        {bookTimeSlotData && bookTimeSlotData[0] ?
                            <Typography component="div">
                                <Typography variant="subtitle1" className={classes.dialogContent}>
                                    {clinicList.find(item => item.clinicCd === bookingData.clinicCd).clinicName}
                                </Typography>
                                <Typography variant="subtitle1" className={classes.dialogContent}>
                                    {/* {bookTimeSlotData[0].encounterTypeDesc} */}
                                    {`${bookingData.encounterTypes[0].encounterTypeCd} - ${bookingData.encounterTypes[0].subEncounterTypeCd}`}
                                </Typography>
                                <Typography variant="subtitle1" className={classes.dialogContent}>
                                    {/* {`Date: ${moment(bookTimeSlotData[0].slotDate).format(Enum.DATE_FORMAT_EDMY_VALUE)} ${bookTimeSlotData[0].startTime}`} */}
                                    {
                                        AppointmentUtilities.combineApptDateAndTime(
                                            {
                                                appointmentDate: moment(bookTimeSlotData[0].slotDate).format(Enum.DATE_FORMAT_EDMY_VALUE),
                                                appointmentTime: bookTimeSlotData[0].startTime
                                            },
                                            null,
                                            Enum.TIME_FORMAT_12_HOUR_CLOCK)
                                    }
                                </Typography>
                            </Typography> : null
                        }
                    </DialogContent>
                    <DialogActions className={classes.dialogAction}>
                        <CIMSButton onClick={this.handleBookConfirm} id={'bookingAnonymous_appointment_booking_dialog1_confirm'} color="primary" disabled={this.props.handlingBooking}>Confirm</CIMSButton>
                        <CIMSButton onClick={(...args) => this.handleBookSearch(...args, 0)} color="primary" id={'bookingAnonymous_appointment_booking_dialog1_search'} style={{ display: this.props.openTimeSlotModal ? 'none' : '' }}>Search</CIMSButton>
                        <CIMSButton onClick={this.handleBookCancel} color="primary" id={'bookingAnonymous_appointment_booking_dialog1_cancel'}>Cancel</CIMSButton>
                    </DialogActions>
                </CIMSDialog>
                {/* <CIMSDialog id="appointment_booking_dialog2" dialogTitle="Appointment Booking" open={this.props.openConfirmDialog && bookingData.encounterTypes.length > 1} dialogContentProps={{ style: { minWidth: 400 } }}>
                    <DialogContent id={'appointment_booking_dialog1_description'} style={{ padding: 0 }}>
                        <Typography variant="h6">{'Please click <Confirm> to book the appointment'}</Typography>
                        <List dense>
                            {
                                bookingData.encounterTypes.map((item, index) => (
                                    <ListItem key={index}>
                                        <ListItemText
                                            primary={`${item.encounterTypeList.find(i => i.encounterTypeCd === item.encounterTypeCd).shortName} ${item.subEncounterTypeList.find(i => i.subEncounterTypeCd === item.subEncounterTypeCd).shortName}`}
                                        />
                                        <ListItemText
                                            primary={`${moment(item.appointmentDate).format(Enum.DATE_FORMAT_EDMY_VALUE)} ${moment(item.appointmentTime).format('HH:mm')}`}
                                        />
                                        <ListItemSecondaryAction>
                                            <IconButton edge="end" onClick={this.handleBookSearch}>
                                                <Search />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                ))
                            }
                        </List>
                    </DialogContent>
                    <DialogActions className={classes.dialogAction}>
                        <CIMSButton onClick={this.handleBookConfirm} id={'appointment_booking_dialog2_confirm'} color="primary">Confirm</CIMSButton>
                        <CIMSButton onClick={this.handleBookCancel} color="primary" id={'appointment_booking_dialog2_cancel'}>Cancel</CIMSButton>
                    </DialogActions>
                </CIMSDialog> */}
                <CIMSPromptDialog
                    id={'bookingAnonymous_appointmentBookingTimeSlotListDialog'}
                    dialogTitle={'Search Appointment Booking'}
                    dialogContentText={
                        <CIMSTable
                            id={'bookingAnonymous_appointmentBookingTimeSlotListDialogTimeSlotListTable'}
                            rows={this.state.tsRows}
                            data={timeSlotList ? timeSlotList.slotForBookingDtos : null}
                            options={this.state.tsOptions}
                            // tableStyles={{
                            //     minHeight: 400
                            // }}
                            remote
                            totalCount={timeSlotList ? timeSlotList.totalNum : 0}
                            onChangePage={(...args) => this.handleOnChangePage(...args, 'timeSlotTable')}
                            onChangeRowsPerPage={(...args) => this.handleOnChangeRowPerPage(...args, 'timeSlotTable')}
                        />
                    }
                    open={this.props.showTimeSlotList}
                    buttonConfig={
                        [
                            {
                                id: 'bookingAnonymous_appointmentBookingTimeSlotListDialogCancelButton',
                                name: 'Cancel',
                                onClick: this.handleTimeSlotListOnClose
                            }
                        ]
                    }
                />
                <EditModeMiddleware componentName={accessRightEnum.bookingAnonymous} when={!this.props.bookingConfirmed} />
            </Grid >
        );
    }
}

const mapStatetoProps = (state) => {
    return {
        serviceCd: state.login.service.serviceCd,
        clinicCd: state.login.clinic.clinicCd,
        patientInfo: state.bookingAnonymous.patientInfo,
        timeSlotList: state.bookingAnonymousInformation.timeSlotList,
        openTimeSlotModal: state.bookingAnonymousInformation.openTimeSlotModal,
        openConfirmDialog: state.bookingAnonymousInformation.openConfirmDialog,
        bookingData: state.bookingAnonymousInformation.bookingData,
        clinicList: state.bookingAnonymousInformation.clinicList || null,
        bookTimeSlotData: state.bookingAnonymousInformation.bookTimeSlotData,
        bookingConfirmed: state.bookingAnonymousInformation.bookingConfirmed,
        showTimeSlotList: state.bookingAnonymousInformation.showTimeSlotListDialog,
        changeEncounterIndex: state.bookingAnonymousInformation.changeEncounterIndex,
        clinicConfig: state.common.clinicConfig,
        defaultEncounterCd: state.bookingAnonymousInformation.defaultEncounterCd,
        handlingBooking: state.bookingAnonymousInformation.handlingBooking,
        waitingList: state.bookingAnonymous.waitingList,
        searchLogicList: state.bookingInformation.searchLogicList,
        remarkCodeList: state.bookingAnonymousInformation.remarkCodeList
    };
};

const mapDispatchtoProps = {
    resetInfoAll,
    appointmentBook,
    listTimeSlot,
    getEncounterTypeList,
    getClinicList,
    bookConfirm,
    bookConfirmWaiting,
    updateState,
    updateField,
    openCommonCircular,
    closeCommonCircular,
    deleteTabs,
    listSeachLogic,
    getPatientList,
    openCommonMessage,
    listRemarkCode,
    updateCurTab
};

export default connect(mapStatetoProps, mapDispatchtoProps)(withStyles(styles)(BookingAnonymousInformation));