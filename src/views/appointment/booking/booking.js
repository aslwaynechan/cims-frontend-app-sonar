import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { Grid } from '@material-ui/core';
import {
    resetAll,
    destroyPage,
    updateField,
    loadAppointmentBooking
} from '../../../store/actions/appointment/booking/bookingAction';
import BookingInformation from './bookingInformation';
// import CalendarView from './calendarView';
import _ from 'lodash';
import { initBookingData, initEncounterType } from '../../../constants/appointment/bookingInformation/bookingInformationConstants';
import * as commonUtilities from '../../../utilities/commonUtilities';
import * as appointmentUtilities from '../../../utilities/appointmentUtilities';
import Enum from '../../../enums/enum';
import moment from 'moment';

import {ecsSelector, patientKeySelector, patientInfoSelector} from '../../../store/selectors/ecsSelectors';

const styles = () => ({
    root: {
        width: '100%',
        overflow: 'initial'
    }
});

class Booking extends Component {

    componentDidMount() {
        this.props.ensureDidMount();
        let where = { serviceCd: this.props.serviceCd, clinicCd: this.props.clinicCd };
        let defaultEncounterCd = commonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.DEFAULT_ENCOUNTER_CD, this.props.clinicConfig, where);
        this.props.updateField({ clinicValue: this.props.clinicCd, encounterTypeValue: defaultEncounterCd.configValue });
        let bookData = _.cloneDeep(initBookingData);

        if (this.props.location.state) {
            let params = _.cloneDeep(this.props.location.state);
            if (params.waitingListId) {
                let newEncounterType = _.cloneDeep(initEncounterType);
                newEncounterType.encounterTypeCd = params.encounterTypeCd;
                newEncounterType.appointmentDate = moment().format(Enum.DATE_FORMAT_EYMD_VALUE);
                newEncounterType.appointmentTime = moment().format(Enum.TIME_FORMAT_24_HOUR_CLOCK);
                let memoStrList = [];
                params.countryList ? memoStrList.push(params.countryList.replace(/,/g, ' | ')) : null;
                params.travelDtm ? memoStrList.push('Date:' + params.travelDtm) : null;
                params.remarks ? memoStrList.push('Remark:' + params.remarks) : null;
                bookData.memo = memoStrList.join(' | ');
                bookData.clinicCd = params.clinicCd;
                bookData.encounterTypes[0] = newEncounterType;
                params.showMakeAppointmentView = true;
                params.waitingList = {
                    version: params.version,
                    waitingListId: params.waitingListId
                };
            } else {
                bookData = { ...bookData, ...params.bookData };
            }
            if (!bookData.clinicCd) {
                bookData.clinicCd = this.props.clinicCd;
            }
            this.props.updateField(params);
        }


        //set elapsedPeriod when appointment date and time is null
        bookData = appointmentUtilities.getDefaultElapsedPeriodBookData(bookData);

        //default walkin date
        if (this.props.isWalkIn) {
            bookData.encounterTypes[0].appointmentDate = moment();
            bookData.encounterTypes[0].appoinementTime = moment().set({ hour: 0, minute: 0, second: 0 });
        }

        //default clinic code
        if (!bookData.clinicCd) {
            bookData.clinicCd = this.props.clinicCd;
        }

        this.props.loadAppointmentBooking(bookData);

    }

    componentWillUnmount() {
        this.props.resetAll();
        this.props.destroyPage();
    }

    // prepareBookingData = (data) => {
    //     let bookData = _.cloneDeep(initBookingData);
    //     let newEncounterType = _.cloneDeep(initEncounterType);
    //     newEncounterType.encounterTypeCd = this.props.encounterTypeValue;
    //     newEncounterType.subEncounterTypeCd = data.subEncounterType;
    //     newEncounterType.appointmentDate = data.slotByDateDto.date;
    //     newEncounterType.appointmentTime = data.slotByDateDto.time || '';
    //     bookData.clinicCd = this.props.clinicValue;
    //     bookData.encounterTypes[0] = newEncounterType;

    //     return bookData;
    // }

    // calendarOnClick = (e, data) => {
    //     let fieldData = {};
    //     let tempBookingData = this.prepareBookingData(data);
    //     fieldData['bookData'] = tempBookingData;
    //     fieldData['showMakeAppointmentView'] = true;
    //     this.props.updateField(fieldData);
    // }

    render() {
        const { classes, ecs } = this.props;
        return (
            <Grid className={classes.root}>
                {/* {
                    this.props.showMakeAppointmentView ?
                        <BookingInformation bookData={this.props.bookData} isWalkIn={this.props.isWalkIn} encounterTypeValue={this.props.encounterTypeValue} />
                        :
                        <CalendarView
                            calendarOnClick={this.calendarOnClick}
                            requestData={(...args) => this.props.requestData(...args)}
                            updateField={e => this.props.updateField(e)}
                            calendarViewValue={this.props.calendarViewValue}
                            availableQuotaValue={this.props.availableQuotaValue}
                            showAppointmentRemarkValue={this.props.showAppointmentRemarkValue}
                            clinicValue={this.props.clinicValue}
                            encounterTypeValue={this.props.encounterTypeValue}
                            selectEncounterType={this.props.selectEncounterType}
                            subEncounterTypeValue={this.props.subEncounterTypeValue}
                            dateFrom={this.props.dateFrom}
                            dateTo={this.props.dateTo}
                            clinicListData={this.props.clinicListData}
                            encounterTypeListData={this.props.encounterTypeListData}
                            subEncounterTypeListData={this.props.subEncounterTypeListData}
                            calendarData={this.props.calendarData}
                            subEncounterTypeListKeyAndValue={this.props.subEncounterTypeListKeyAndValue}
                        />
                } */}
                <BookingInformation isWalkIn={this.props.isWalkIn} encounterTypeValue={this.props.encounterTypeValue}/>
            </Grid>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        serviceCd: state.login.service.serviceCd,
        clinicCd: state.login.clinic.clinicCd,
        clinicConfig: state.common.clinicConfig,
        showMakeAppointmentView: state.booking.showMakeAppointmentView,
        calendarViewValue: state.booking.calendarViewValue,
        availableQuotaValue: state.booking.availableQuotaValue,
        showAppointmentRemarkValue: state.booking.showAppointmentRemarkValue,
        clinicValue: state.booking.clinicValue,
        encounterTypeValue: state.booking.encounterTypeValue,
        selectEncounterType: state.booking.selectEncounterType,
        subEncounterTypeValue: state.booking.subEncounterTypeValue,
        dateFrom: state.booking.dateFrom,
        dateTo: state.booking.dateTo,
        encounterTypeListData: state.booking.encounterTypeListData,
        subEncounterTypeListData: state.booking.subEncounterTypeListData,
        calendarData: state.booking.calendarData,
        subEncounterTypeListKeyAndValue: state.booking.subEncounterTypeListKeyAndValue,
        isWalkIn: state.booking.isWalkIn,
        caseNoInfo: state.patient.caseNoInfo
    };
};

const mapDispatchToProps = {
    resetAll,
    destroyPage,
    updateField,
    loadAppointmentBooking
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Booking));
