import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import {
    Checkbox,
    Grid,
    FormControl,
    FormControlLabel,
    FormGroup,
    FormLabel,
    RadioGroup,
    Radio,
    IconButton
} from '@material-ui/core';
import { KeyboardArrowRight, KeyboardArrowLeft } from '@material-ui/icons';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import SelectFieldValidator from '../../../components/FormValidator/SelectFieldValidator';
import Calendar from './component/calendar';
import moment from 'moment';
import _ from 'lodash';
import Enum from '../../../enums/enum';

const styles = () => ({
    root: {
        padding: 16,
        '&:last-child': { paddingBottom: 24 },
        display: 'flex'
    },
    formControl: {
        marginBottom: 20,
        width: '100%'
    },
    formLabel: {
        marginBottom: 10,
        fontWeight: 'bold'
    },
    formGroup: {
        overflowY: 'hidden',
        border: '1px solid #cccccc',
        padding: 6
    },
    checkboxFormGroup: {
        overflow: 'hidden',
        border: '1px solid #cccccc',
        paddingLeft: 6,
        height: 160
    },
    checkboxGroup: {
        overflow: 'auto'
    },
    formControlLabel: {
        margin: 0,
        width: '100%'
    },
    checkboxLabel: {
        width: 182,
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis'
    },
    shortNameLable: {
        paddingLeft: 14,
        color: '#404040'
    }
});

class CalendarView extends Component{

    getCalendarData = (newData) => {
        let fileData = newData || {};
        let submitData = {
            clinicCd: fileData.clinicValue || this.props.clinicValue,
            dateFrom: fileData.dateFrom || this.props.dateFrom,
            dateTo: fileData.dateTo || this.props.dateTo,
            encounterTypeCd: fileData.encounterTypeValue || this.props.encounterTypeValue,
            subEncounterTypeCds: fileData.subEncounterTypeValue || this.props.subEncounterTypeValue,
            viewType: fileData.calendarViewValue || this.props.calendarViewValue
        };
        if (submitData.subEncounterTypeCds.length > 0) {
            if (submitData.viewType === 'M') {
                // submitData.dateFrom = moment(submitData.dateFrom).startOf('week').format('YYYY-MM-DD');
                // submitData.dateTo = moment(submitData.dateTo).endOf('week').format('YYYY-MM-DD');
                submitData.dateFrom = moment(submitData.dateFrom).startOf('week').format(Enum.DATE_FORMAT_EYMD_VALUE);
                submitData.dateTo = moment(submitData.dateTo).endOf('week').format(Enum.DATE_FORMAT_EYMD_VALUE);
            } else {
                // submitData.dateFrom = moment(submitData.dateFrom).format('YYYY-MM-DD');
                // submitData.dateTo = moment(submitData.dateTo).format('YYYY-MM-DD');
                submitData.dateFrom = moment(submitData.dateFrom).format(Enum.DATE_FORMAT_EYMD_VALUE);
                submitData.dateTo = moment(submitData.dateTo).format(Enum.DATE_FORMAT_EYMD_VALUE);
            }
            this.props.requestData('calendarData', submitData, fileData);
        } else {
            fileData['calendarData'] = [];
            this.props.updateField(fileData);
        }
    }
    calendarViewValueOnChange = e => {
        let fileData = {};
        fileData[e.target.name] = e.target.value;
        let dateType = 'D';
        switch (e.target.value) {
            case 'M':
                dateType = 'month';
                break;
            case 'W':
                dateType = 'week';
                break;
            default:
                dateType = 'day';
                break;
        }
        fileData['dateFrom'] = moment().startOf(dateType);
        fileData['dateTo'] = moment().endOf(dateType);
        this.getCalendarData(fileData);
    }
    fileValueOnChange = (name, value) => {
        let fileData = {};
        fileData[name] = value;
        this.props.updateField(fileData);
    }
    checkboxOnChange = (name, value) => {
        let fileData = {};
        fileData[name] = _.cloneDeep(this.props[name] || []);
        let valueIndex = fileData[name].indexOf(value);
        if (valueIndex === -1) {
            fileData[name].push(value);
        } else {
            fileData[name].splice(valueIndex, 1);
        }
        if (name === 'availableQuotaValue') {
            if (fileData[name].indexOf('urgent') === -1 && fileData[name].indexOf('normal') === -1) {
                fileData[name] = [];
            }
            this.props.updateField(fileData);
        } else {
            this.getCalendarData(fileData);
        }
    }
    clinicValueOnChange = (e) => {
        let fileData = {};
        fileData.clinicValue = e.value;
        fileData.encounterTypeValue = null;
        fileData.subEncounterTypeListData = [];
        fileData.subEncounterTypeValue = [];
        fileData.selectEncounterType = {};
        this.props.updateField(fileData);
    }
    encounterTypeValueOnChange = (e) => {
        let keyAndValue = {};
        let fileData = {};
        fileData.encounterTypeValue = e.value;
        fileData.subEncounterTypeListData = e.rowData.subEncounterTypeList.map(item => { keyAndValue[item.subEncounterTypeCd] = item.shortName; return item; });
        fileData.subEncounterTypeListKeyAndValue = keyAndValue;
        fileData.subEncounterTypeValue = [];
        fileData.selectEncounterType = { ...e.rowData };
        this.props.updateField(fileData);
    }
    subtractDate = () => {
        let fileData = {};
        fileData.dateFrom = moment(this.props.dateFrom).subtract(1, this.props.calendarViewValue === 'D' ? 'days' : this.props.calendarViewValue);
        fileData.dateTo = moment(this.props.dateTo).subtract(1, this.props.calendarViewValue === 'D' ? 'days' : this.props.calendarViewValue);
        this.getCalendarData(fileData);
    }
    addDate = () => {
        let fileData = {};
        fileData.dateFrom = moment(this.props.dateFrom).add(1, this.props.calendarViewValue === 'D' ? 'days' : this.props.calendarViewValue);
        fileData.dateTo = moment(this.props.dateTo).add(1, this.props.calendarViewValue === 'D' ? 'days' : this.props.calendarViewValue);
        this.getCalendarData(fileData);
    }

    render(){
        const { classes } = this.props;
        return(
            <Grid className={classes.root}>
                    <Grid style={{ width: 240 }}>
                        <ValidatorForm
                            id="bookingCalendarForm"
                            ref="form"
                            onSubmit={() => { }}
                        >
                            <FormControl component="fieldset" className={classes.formControl}>
                                <FormLabel component="legend" className={classes.formLabel}>Calendar View</FormLabel>
                                <RadioGroup
                                    id={'bookingCalendarTypeRadioGroup'}
                                    aria-label="Calendar View"
                                    name="calendarViewValue"
                                    className={classes.formGroup}
                                    value={this.props.calendarViewValue}
                                    onChange={this.calendarViewValueOnChange}
                                >
                                    <FormControlLabel id={'bookingCalendarTypeRadioGroupRadioDay'} className={classes.formControlLabel} value="D" control={<Radio style={{ padding: 4 }} />} label="Day" />
                                    <FormControlLabel id={'bookingCalendarTypeRadioGroupRadioWeek'} className={classes.formControlLabel} value="W" control={<Radio style={{ padding: 4 }} />} label="Week" />
                                    <FormControlLabel id={'bookingCalendarTypeRadioGroupRadioMonth'} className={classes.formControlLabel} value="M" control={<Radio style={{ padding: 4 }} />} label="Month" />
                                </RadioGroup>
                            </FormControl>
                            <FormControl component="fieldset" className={classes.formControl}>
                                <FormLabel component="legend" className={classes.formLabel} >Available Quota</FormLabel>
                                <FormGroup id={'bookingAvailableQuotaCheckboxGroup'} className={classes.formGroup}>
                                    <FormControlLabel
                                        id={'bookingAvailableQuotaCheckboxGroupCheckboxNormal'}
                                        className={classes.formControlLabel}
                                        control={<Checkbox style={{ padding: 4 }} checked={this.props.availableQuotaValue.indexOf('normal') !== -1} onChange={() => { this.checkboxOnChange('availableQuotaValue', 'normal'); }} value="normal" />}
                                        label="Normal"
                                    />
                                    <FormControlLabel
                                        id={'bookingAvailableQuotaCheckboxGroupCheckboxUrgent'}
                                        className={classes.formControlLabel}
                                        control={<Checkbox style={{ padding: 4 }} checked={this.props.availableQuotaValue.indexOf('urgent') !== -1} onChange={() => { this.checkboxOnChange('availableQuotaValue', 'urgent'); }} value="urgent" />}
                                        label="Urgent"
                                    />
                                    <FormControlLabel
                                        id={'bookingAvailableQuotaCheckboxGroupCheckboxTotalQuota'}
                                        className={classes.formControlLabel}
                                        control={
                                            <Checkbox disabled={this.props.availableQuotaValue.length === 0} style={{ padding: 4 }} checked={this.props.availableQuotaValue.indexOf('all') !== -1} onChange={() => { this.checkboxOnChange('availableQuotaValue', 'all'); }} value="all" />
                                        }
                                        label="Total Quota"
                                    />
                                </FormGroup>
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <FormControlLabel
                                    id={'bookingShowAppointmentRemarkCheckbox'}
                                    className={classes.formControlLabel}
                                    style={{ width: '100%', borderBottom: '6px solid #d0f0fb' }}
                                    control={
                                        <Checkbox style={{ padding: '4px 4px 4px 0' }} checked={this.props.showAppointmentRemarkValue} onChange={() => { this.fileValueOnChange('showAppointmentRemarkValue', !this.props.showAppointmentRemarkValue); }} value="antoine" />
                                    }
                                    label="Show Appointment Remark"
                                />
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <FormLabel component="legend" className={classes.formLabel}>Clinic</FormLabel>
                                <SelectFieldValidator
                                    id={'bookingClinicSelectField'}
                                    options={this.props.clinicListData.map((item) => ({ value: item.code, label: item.engDesc }))}
                                    msgPosition="bottom"
                                    value={this.props.clinicValue}
                                    onChange={this.clinicValueOnChange}
                                />
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <FormLabel component="legend" className={classes.formLabel}>Encounter</FormLabel>
                                <SelectFieldValidator
                                    id={'bookingEncounterTypeSelectField'}
                                    options={this.props.encounterTypeListData.map((item) => ({ value: item.encounterTypeCd, label: item.encounterTypeCd, rowData: item }))}
                                    msgPosition="bottom"
                                    value={this.props.encounterTypeValue}
                                    onChange={this.encounterTypeValueOnChange}
                                />
                                <Grid className={classes.shortNameLable}>
                                    {this.props.selectEncounterType.shortName}
                                </Grid>
                            </FormControl>
                            <FormControl component="fieldset" className={classes.formControl}>
                                <FormLabel component="legend" className={classes.formLabel} >Sub-encounter</FormLabel>
                                {this.props.subEncounterTypeListData && this.props.subEncounterTypeListData.length > 0 ?
                                    <FormGroup id={'bookingSubEncounterTypeCheckboxGroup'} className={classes.checkboxFormGroup}>
                                        <Grid className={classes.checkboxGroup}>
                                            {this.props.subEncounterTypeListData.map((item, index) => (
                                                <FormControlLabel
                                                    key={index}
                                                    id={'bookingSubEncounterTypeCheckboxGroupCheckbox' + index}
                                                    className={classes.formControlLabel}
                                                    control={<Checkbox style={{ padding: 4 }} checked={this.props.subEncounterTypeValue.indexOf(item.subEncounterTypeCd) !== -1} onChange={() => { this.checkboxOnChange('subEncounterTypeValue', item.subEncounterTypeCd); }} value={item.subEncounterTypeCd} />}
                                                    label={<div className={classes.checkboxLabel}>{item.subEncounterTypeCd + ' ' + item.shortName}</div>}
                                                />
                                            ))
                                            }
                                        </Grid>
                                    </FormGroup> : null
                                }
                            </FormControl>

                        </ValidatorForm>
                    </Grid>
                    <Grid style={{ flex: '1', overflowX: 'hidden' }}>
                        <Grid>
                            <IconButton
                                id={'bookingSubtractDateIconButton'}
                                color="primary"
                                aria-label="keyboard_arrow_left"
                                onClick={this.subtractDate}
                            >
                                <KeyboardArrowLeft />
                            </IconButton>
                            {
                                //MMM-D-YYYY
                                this.props.calendarViewValue === 'M' ? moment(this.props.dateFrom).format('MMM-YYYY') :
                                    this.props.calendarViewValue === 'W' ? moment(this.props.dateFrom).format('MMM-D-YYYY') + ' ~ ' + moment(this.props.dateTo).format('MMM-D-YYYY') :
                                        moment(this.props.dateFrom).format('MMM-D-YYYY')
                            }
                            <IconButton
                                id={'bookingAddDateIconButton'}
                                color="primary"
                                aria-label="keyboard_arrow_right"
                                onClick={this.addDate}
                            >
                                <KeyboardArrowRight />
                            </IconButton>
                        </Grid>
                        <Grid>
                            <Calendar
                                viewType={this.props.calendarViewValue}
                                showWeekend={false}
                                calendarData={this.props.calendarData}
                                availableQuotaValue={this.props.availableQuotaValue}
                                onClick={this.props.calendarOnClick}
                                dateFrom={this.props.dateFrom}
                                dateTo={this.props.dateTo}
                                tableTitle={this.props.subEncounterTypeListKeyAndValue}
                            />
                        </Grid>
                    </Grid>
                </Grid>
        );
    }
}

export default withStyles(styles)(CalendarView);