import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import {
    Grid
} from '@material-ui/core';
import SearchPanel from './compontent/searchPanel';
import CIMSTable from '../../../components/Table/CIMSTable';
import Enum from '../../../enums/enum';
import SearchDialog from './compontent/searchDialog';
import EditWaitingDialog from './compontent/editWaitingDialog';
import moment from 'moment';
import {
    updateField,
    searchWaitingList,
    initiPage,
    cancelSearch,
    addWaiting,
    getWaiting,
    deleteWaiting,
    resetAll
} from '../../../store/actions/appointment/waitingList/waitingListAction';
import AccessRightEnum from '../../../enums/accessRightEnum';
import {
    resetAll as resetPatient,
    getPatientById,
    getPatientCaseNo
} from '../../../store/actions/patient/patientAction';
import {
    skipTab,
    cleanSubTabs
} from '../../../store/actions/mainFrame/mainFrameAction';
import { selectCaseTrigger, openCaseNoDialog } from '../../../store/actions/caseNo/caseNoAction';
import * as removeIllegalCharUtilities from '../../../utilities/removeIllegalCharUtilities';
import * as AppointmentUtilities from '../../../utilities/appointmentUtilities';
import { openCommonMessage } from '../../../store/actions/message/messageAction';
import * as commonUtilities from '../../../utilities/commonUtilities';
import * as RegUtil from '../../../utilities/registrationUtilities';
import CIMSButtonGroup from '../../../components/Buttons/CIMSButtonGroup';

class WaitingList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedItems: [],
            tableRows: AppointmentUtilities.get_WaitingList_TableRow_ByServiceCd(this.props.listConfig, this.customDocTypeRender),
            tableOptions: {
                rowExpand: true,
                headCellStyle: this.props.classes.customTableHeadCell,
                bodyCellStyle: this.props.classes.customTableBodyCell,
                customRowStyle: (rowData) => {
                    let classname = '';
                    // if (rowData.genderCd === Enum.GENDER_MALE_VALUE) {
                    //     classname = this.props.classes.maleRowRoot;
                    // } else if (rowData.genderCd === Enum.GENDER_FEMALE_VALUE) {
                    //     classname = this.props.classes.femaleRowRoot;
                    // } else {
                    //     classname = this.props.classes.unknownRowRoot;
                    // }
                    if (rowData.isHighLine) {
                        classname = this.props.classes.highLineRowRoot;
                    }

                    return classname;
                },
                rowsPerPage: this.props.pageSize,
                rowsPerPageOptions: [10, 15, 20],
                onSelectIdName: 'waitingListId',
                onSelectedRow: (rowId, rowData, selectedData) => {
                    this.selectTableItem(selectedData);
                }
            }
        };
    }

    componentDidMount() {
        this.props.ensureDidMount();
        this.props.initiPage({ 'serviceCd': this.props.serviceCd });
        this.searchWaitingList({});
        this.tableRef.setDividerScale(window.sessionStorage.getItem('waitingList_tableScale') || '40');
    }

    shouldComponentUpdate(nextP) {
        if (nextP.tabsActiveKey !== this.props.tabsActiveKey && nextP.tabsActiveKey === AccessRightEnum.waitingList) {
            this.searchWaitingList({});
            return false;
        }
        return true;
    }

    componentWillUnmount() {
        this.props.resetAll();
        if (this.props.isLoginSuccess) {
            window.sessionStorage.setItem('waitingList_tableScale', this.tableRef.getDividerScale());
        }
    }

    selectTableItem = (selected) => {
        let selectedItem = [];
        if (selected && selected.length > 0)
            selectedItem.push(selected[0]);
        this.setState({ selectedItems: selectedItem });
    }

    customDocTypeRender = (value) => {
        let docType = this.props.docTypeList.find(item => { return item.code === value; });
        return docType && docType.engDesc;
    }

    updatePage = (page) => {
        this.tableRef.updatePage(page);
    }

    searchWaitingList = (fields = {}, callback = null) => {
        let params = {
            docTypeCd: fields.docType || this.props.docType,
            encounterTypeCd: fields.encounterType || this.props.encounterType,
            engGivename: fields.engGivenname || this.props.engGivenname,
            engSurname: fields.engSurname || this.props.engSurname,
            hkid: fields.HKID || this.props.HKID,
            otherDocNo: fields.HKID || this.props.HKID,
            sortBy: fields.sortBy || this.props.sortBy,
            sortType: (fields.sortType || this.props.sortType).toUpperCase(),
            page: fields.page || this.props.page,
            pageSize: fields.pageSize || this.props.pageSize,
            phoneNo: fields.phone || this.props.phone,
            dateFrom: fields.dateFrom || this.props.dateFrom,
            dateTo: fields.dateTo || this.props.dateTo,
            statusCd: fields.status || this.props.status
        };
        params.hkid = params.hkid.replace('(', '').replace(')', '');
        params.encounterTypeCd = params.encounterTypeCd === '*ALL' ? null : params.encounterTypeCd;
        params.dateFrom = moment(params.dateFrom).format(Enum.DATE_FORMAT_EYMD_VALUE);
        params.dateTo = moment(params.dateTo).format(Enum.DATE_FORMAT_EYMD_VALUE);
        params.statusCd = params.statusCd === '*ALL' ? null : params.statusCd;
        params.docTypeCd = params.docTypeCd === '*ALL' ? null : params.docTypeCd;
        this.setState({ selectedItems: [] });
        this.tableRef.clearSelected();
        // if (reset) {
        //     this.tableRef.updatePage(0);
        //     params.page = 1;
        // }
        this.props.searchWaitingList(params, fields, callback);
    }

    searchPanelOnChange = (event, name, fieldType) => {
        let fields = {};
        const { dateFrom, dateTo } = this.props;
        switch (fieldType) {
            case 'DateField':
                fields[name] = event || moment();
                if (fields[name] && moment(fields[name]).isValid()) {
                    if (name === 'dateFrom' && moment(dateTo).isBefore(moment(fields[name]))) {
                        fields['dateTo'] = fields[name];
                    }
                    if (name === 'dateTo' && moment(dateFrom).isAfter(moment(fields[name]))) {
                        fields['dateFrom'] = fields[name];
                    }
                    if (!moment(fields.dateFrom || dateFrom).isValid() ||
                        !moment(fields.dateTo || dateTo).isValid()) {
                        this.props.updateField(fields);
                        return;
                    }
                } else {
                    this.props.updateField(fields);
                    return;
                }
                break;
            case 'SelectField':
                fields[name] = event.value;
                break;
            // case 'TextField':
            //     fields[name] = name === 'engSurname'||name === 'engGivenname'?event.target.value.toUpperCase():event.target.value;
            //     break;
            default:
                break;
        }
        fields.page = 1;
        this.searchWaitingList(fields,this.updatePage);
    }


    searchDialogOnChange = (event, name, fieldType) => {
        let fields = {};
        switch (fieldType) {
            case 'DateField':
                fields[name] = event;
                break;
            case 'SelectField':
                if (name === 'docType') {
                    if (event.value === '*ALL') {
                        fields['HKID'] = '';
                    }
                }
                fields[name] = event.value;
                break;
            case 'TextField':
                if (name === 'phone') {
                    fields[name] = removeIllegalCharUtilities.nonNumber(event.target.value.toUpperCase(), 8);
                } else {
                    fields[name] = event.target.value.toUpperCase();
                }
                break;
            default:
                break;
        }
        this.props.updateField(fields);
    }

    showSearchDialog = () => {
        this.props.updateField({ 'openSearchDialog': true });
    }

    searchDialogSearch = () => {
        this.searchWaitingList({}, this.updatePage);
        this.props.cancelSearch();
    }
    searchDialogCancel = () => {
        this.props.cancelSearch();
    }

    //eslint-disable-next-line
    onChangePage = (event, newPage, rowPerPage) => {
        let fields = { page: newPage + 1 };
        this.searchWaitingList(fields);
    }

    onChangeRowsPerPage = (event, newPage, rowPerPage) => {
        let fields = { page: newPage + 1, pageSize: rowPerPage };
        this.searchWaitingList(fields);
    }

    headCellOnClick = (e, data) => {
        let fields = { sortBy: data.name };
        fields.sortType = this.props.sortBy === data.name && this.props.sortType === 'asc' ? 'desc' : 'asc';
        this.searchWaitingList(fields, this.updatePage);
    }

    addWaiting = () => {
        this.props.addWaiting();
    }
    editWaiting = (data) => {
        this.props.getWaiting({ longId: data.waitingListId });
    }

    // handleHkicFocus = (e, fieldName) => {
    //     let fields = {};
    //     fields[fieldName] = e.target.value.replace('(', '').replace(')', '');
    //     this.props.updateField(fields);
    // }

    handleHkic = (value, fieldName) => {
        let fields = {};
        value = value.replace('(', '').replace(')', '');
        let promise = new Promise((resolve) => {
            resolve(RegUtil.checkHKID(value));
        });
        promise.then((result) => {
            if (result) {
                fields[fieldName] = RegUtil.hkidFormat(value);
            }
            this.props.updateField(fields);
        });
    }

    deleteWaiting = (data) => {
        let submitData = {
            waitingListId: data.waitingListId,
            version: data.version
        };
        this.props.deleteWaiting(submitData, this.searchWaitingList);
        this.setState({ selectedItems: [] });
        this.tableRef.clearSelected();
    }

    booking = (data) => {
        let regOrSumTabsIndex = this.props.tabs.findIndex((item) => { return item.label == 'Registration' || item.label == 'Patient Summary'; });
        let patientCall = commonUtilities.getPatientCall();
        if (regOrSumTabsIndex != -1) {
            this.props.openCommonMessage({
                msgObj: {
                    description: `Please complete the tasks for current patient before ${patientCall} to Registration.`,
                    header: 'Application Suggestion',
                    btn1Caption:'OK'
                }
            });
            return;
        }
        if (data.patientKey && data.patientKey > 0) {
            if (this.props.patientInfo) {
                this.props.openCommonMessage({
                    msgCode: '110244',
                    params: [{ name: 'PATIENT_CALL', value: commonUtilities.getPatientCall() }],
                    btnActions: {
                        btn1Click: () => {
                            this.searchWaitingList({});
                        }
                    }
                });
            } else {
                this.props.cleanSubTabs();
                this.props.resetPatient();
                // let callBack = (patient) => {
                //     const activeCaseList = (patient.caseList && patient.caseList.filter(item => item.statusCd === Enum.CASE_STATUS.ACTIVE) || []);
                //     if(activeCaseList.length === 1){
                //         this.props.getPatientCaseNo(patient.caseList, activeCaseList[0]['caseNo']);
                //         this.props.skipTab(AccessRightEnum.booking, data, true);
                //     } else if(activeCaseList.length > 1) {
                //         this.props.selectCaseTrigger({
                //             trigger: true,
                //             caseSelectCallBack: () => {
                //                 this.props.skipTab(AccessRightEnum.booking, data, true);
                //             }
                //         });
                //     } else {
                //         this.props.openCaseNoDialog({
                //             caseDialogStatus: Enum.CASE_DIALOG_STATUS.CREATE,
                //             caseNoForm: { patientKey: patient.patientKey },
                //             caseCallBack: (data) => {
                //                 if (data && data.caseNo) {
                //                     this.props.getPatientById(patient.patientKey, null, data.caseNo);
                //                     this.props.skipTab(AccessRightEnum.booking, data, true);
                //                 }
                //             }
                //         });
                //     }
                // };
                let callBack = () => {
                    this.props.skipTab(AccessRightEnum.booking, data, true);
                };
                this.props.getPatientById(data.patientKey, null, null, callBack);
            }
        } else {
            if (this.props.patientInfo) {
                this.props.openCommonMessage({
                    msgCode: '110244',
                    params: [{ name: 'PATIENT_CALL', value: commonUtilities.getPatientCall() }],
                    btnActions: {
                        btn1Click: () => {
                            this.searchWaitingList({});
                        }
                    }
                });
            } else {
                this.props.skipTab(AccessRightEnum.bookingAnonymous, data);
            }
        }
    }

    render() {
        const { selectedItems } = this.state;
        return (
            <Grid id="waitingList">
                <SearchPanel
                    id="waitingListSearchPanel"
                    encounterTypeList={this.props.encounterTypeList}
                    statusOption={Enum.WAITING_LIST_STATUS_LIST}
                    dateFrom={this.props.dateFrom}
                    dateTo={this.props.dateTo}
                    encounterType={this.props.encounterType}
                    status={this.props.status}
                    updateField={this.searchPanelOnChange}
                    btnSearch={this.showSearchDialog}
                    btnAdd={this.addWaiting}
                />
                <Grid style={{ marginTop: 10 }}>
                    <CIMSTable
                        id="waitingListCIMSTable"
                        rows={this.state.tableRows}
                        data={this.props.waitingList.waitingListInnerDto || []}
                        options={this.state.tableOptions}
                        orderDirection={this.props.sortType}
                        orderBy={this.props.sortBy}
                        onRequestSort={this.headCellOnClick}
                        remote
                        totalCount={this.props.waitingList.totalNum || 0}
                        onChangePage={this.onChangePage}
                        onChangeRowsPerPage={this.onChangeRowsPerPage}
                        innerRef={ref => this.tableRef = ref}
                    />
                </Grid>
                <CIMSButtonGroup
                    buttonConfig={
                        [
                            {
                                id: 'waitingListBookingButton',
                                name: 'Booking',
                                disabled: selectedItems.length === 0 || selectedItems[0].statusCd !== '0',
                                onClick: () => this.booking(selectedItems[0])
                            },
                            {
                                id: 'waitingListEditWaitingButton',
                                name: 'Edit',
                                disabled: selectedItems.length === 0 || selectedItems[0].statusCd !== '0',
                                onClick: () => this.editWaiting(selectedItems[0])
                            },
                            {
                                id: 'waitingListDeleteWaitingButton',
                                name: 'Delete',
                                disabled: selectedItems.length === 0 || selectedItems[0].statusCd !== '0',
                                onClick: () => {
                                    this.props.openCommonMessage({
                                        msgCode: '110245',
                                        btnActions: {
                                            btn1Click: () => {
                                                this.deleteWaiting(selectedItems[0]);
                                            }
                                        }
                                    });
                                }

                            }
                        ]
                    }
                />

                <SearchDialog
                    id="waitingListSearchDialog"
                    updateField={this.searchDialogOnChange}
                    btnSearch={this.searchDialogSearch}
                    btnCancel={this.searchDialogCancel}
                />
                <EditWaitingDialog
                    id="waitingListEditWaitingDialog"
                    searchWaitingList={this.searchWaitingList}
                    handleHkic={this.handleHkic}
                />
            </Grid>
        );
    }
}

const style = () => ({
    highLineRowRoot: {
        '& td': {
            color: '#0579c8',
            fontStyle: 'italic'
        }
    }
});
const mapStateToProps = (state) => {
    return {
        tabsActiveKey: state.mainFrame.tabsActiveKey,
        patientInfo: state.bookingAnonymous.patientInfo,
        serviceCd: state.login.service.serviceCd,
        dateFrom: state.waitingList.dateFrom,
        dateTo: state.waitingList.dateTo,
        encounterType: state.waitingList.encounterType,
        status: state.waitingList.status,
        encounterTypeList: state.waitingList.encounterTypeList,
        docTypeList: state.waitingList.docTypeList,
        docType: state.waitingList.docType,
        HKID: state.waitingList.HKID,
        engSurname: state.waitingList.engSurname,
        engGivenname: state.waitingList.engGivenname,
        phone: state.waitingList.phone,
        openSearchDialog: state.waitingList.openSearchDialog,
        waitingList: state.waitingList.waitingList,
        page: state.waitingList.page,
        pageSize: state.waitingList.pageSize,
        sortBy: state.waitingList.sortBy,
        sortType: state.waitingList.sortType,
        listConfig: state.common.listConfig,
        isLoginSuccess: state.login.isLoginSuccess,
        tabs: state.mainFrame.tabs
    };
};

const mapDispatchToProps = {
    updateField,
    searchWaitingList,
    initiPage,
    cancelSearch,
    addWaiting,
    getWaiting,
    deleteWaiting,
    getPatientById,
    skipTab,
    resetAll,
    openCommonMessage,
    resetPatient,
    cleanSubTabs,
    selectCaseTrigger,
    openCaseNoDialog,
    getPatientCaseNo
};
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(style)(WaitingList));