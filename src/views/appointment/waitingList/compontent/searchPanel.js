import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    Grid
} from '@material-ui/core';
import ValidatorForm from '../../../../components/FormValidator/ValidatorForm';
import DateFieldValidator from '../../../../components/FormValidator/DateFieldValidator';
import SelectFieldValidator from '../../../../components/FormValidator/SelectFieldValidator';
import CIMSButton from '../../../../components/Buttons/CIMSButton';

class SearchPanel extends Component {
    shouldComponentUpdate(nextProps) {
        return  nextProps.dateFrom!==this.props.dateFrom||
                nextProps.dateTo!==this.props.dateTo||
                nextProps.encounterTypeList!==this.props.encounterTypeList||
                nextProps.encounterType!==this.props.encounterType||
                nextProps.status!==this.props.status||
                nextProps.updateField!==this.props.updateField||
                nextProps.btnSearch!==this.props.btnSearch||
                nextProps.statusOption!==this.props.statusOption||
                nextProps.btnAdd!==this.props.btnAdd;
    }
    render() {
        const {
            id,
            classes,
            dateFrom,
            dateTo,
            encounterTypeList,
            encounterType,
            status,
            updateField,
            btnSearch,
            statusOption,
            btnAdd
        } = this.props;
        return (
            <ValidatorForm
                id={id+'Form'}
                ref={'form'}
                onSubmit={() => { }}
                className={classes.form}
            >
                <Grid className={classes.fieldPanel}>
                    <Grid className={classes.field}>
                        <DateFieldValidator
                            id={id+'DateFromDateField'}
                            labelText="From (Request Date)"
                            isRequired
                            value={dateFrom}
                            onChange={(e) => updateField(e, 'dateFrom', 'DateField')}
                        />
                    </Grid>
                    <Grid className={classes.field}>
                        <DateFieldValidator
                            id={id+'DateToDateField'}
                            labelText="To (Request Date)"
                            isRequired
                            value={dateTo}
                            onChange={(e) => updateField(e, 'dateTo', 'DateField')}
                        />
                    </Grid>
                    <Grid className={classes.field}>
                        <SelectFieldValidator
                            id={id+'EncounterSelectField'}
                            labelText="Encounter Type"
                            options={encounterTypeList &&
                                [
                                    { value: '*ALL', label: '*All' },
                                    ...encounterTypeList.map(item => ({ value: item.encounterTypeCd, label: item.encounterTypeCd }))
                                ]
                            }
                            value={encounterType}
                            onChange={(e) => updateField(e, 'encounterType', 'SelectField')}
                        />
                    </Grid>
                    <Grid className={classes.field}>
                        <SelectFieldValidator
                            id={id+'StatusSelectField'}
                            labelText="Status"
                            options={statusOption}
                            value={status}
                            onChange={(e) => updateField(e, 'status', 'SelectField')}
                        />
                    </Grid>
                </Grid >
                <Grid className={classes.buttonPanel}>
                    <CIMSButton
                        id={id+'SearchCIMSButton'}
                        onClick={btnSearch}
                    >
                        Search
                    </CIMSButton>
                    <CIMSButton
                        id={id+'AddCIMSButton'}
                        onClick={btnAdd}
                    >
                        Add
                    </CIMSButton>
                    <CIMSButton
                        id={id+'ExitCIMSButton'}
                    >
                        Exit
                    </CIMSButton>
                </Grid>
            </ValidatorForm>
        );
    }
}

const style = () => ({
    form: {
        display: 'flex'
    },
    fieldPanel: {
        flex: 1,
        display: 'flex'
    },
    field: {
        flex: 1,
        marginRight: 4
    },
    buttonPanel: {
        paddingTop: 14
    }

});
export default withStyles(style)(SearchPanel);