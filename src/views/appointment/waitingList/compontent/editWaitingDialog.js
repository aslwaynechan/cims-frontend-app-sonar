import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import {
    Grid
} from '@material-ui/core';
import {
    updateField,
    cancelEditWaiting,
    getEncounterList,
    searchPatientList,
    getPatient,
    saveWaiting,
    resetAll
} from '../../../../store/actions/appointment/waitingList/waitingListAction';
import CIMSPromptDialog from '../../../../components/Dialog/CIMSPromptDialog';
import ValidatorForm from '../../../../components/FormValidator/ValidatorForm';
import SelectFieldValidator from '../../../../components/FormValidator/SelectFieldValidator';
import TextFieldValidator from '../../../../components/FormValidator/TextFieldValidator';
import DateFieldValidator from '../../../../components/FormValidator/DateFieldValidator';
import ValidatorEnum from '../../../../enums/validatorEnum';
import SearchInput from '../../../compontent/searchInput';
import HKIDInput from '../../../compontent/hkidInput';
import * as RegUtil from '../../../../utilities/registrationUtilities';
import EditModeMiddleware from '../../../compontent/editModeMiddleware';
import moment from 'moment';
import RegFieldLength from '../../../../enums/registration/regFieldLength';
import * as CommonUtilities from '../../../../utilities/commonUtilities';
import Enum from '../../../../enums/enum';
import _ from 'lodash';
import CommonMessage from '../../../../constants/commonMessage';
import accessRightEnum from '../../../../enums/accessRightEnum';
import * as PatientUtil from '../../../../utilities/patientUtilities';
import RequiredIcon from '../../../../components/InputLabel/RequiredIcon';

class EditWaitingDialog extends Component {
    componentDidMount() {
        ValidatorForm.addValidationRule(ValidatorEnum.isHkid, (value) => {
            return RegUtil.checkHKID(value);
        });
        ValidatorForm.addValidationRule(ValidatorEnum.isExpiryDate, (value) => {
            return !value || moment(value).isSameOrAfter(moment().startOf('day'));
        });
    }

    UNSAFE_componentWillUpdate(nexp) {
        if (nexp.waiting !== this.props.waiting) {
            if (this.refs.form) {
                this.refs.form.resetValidations();
            }
            if (nexp.waitingHkid) {
                this.props.handleHkic(nexp.waitingHkid, 'waitingHkid');
            }
        }
        if (!nexp.waitingClinicCd && nexp.editWaitingStatus !== 'I') {
            let where = { serviceCd: nexp.serviceCd, clinicCd: nexp.clinicCd };
            let defaultEncounterCd = CommonUtilities.getPriorityConfig(Enum.CLINIC_CONFIGNAME.DEFAULT_ENCOUNTER_CD, this.props.clinicConfig, where);
            this.waitingClinicCdOnChange({ value: nexp.clinicCd }, defaultEncounterCd.configValue);
        }
    }


    waitingSubmit = () => {
        let submitData = this.props.waiting || {};

        //patient info
        if (this.props.waiting.patientKey && this.props.waiting.patientKey > 0) {
            submitData.anonymousPatientDto = null;
        } else {
            let patientPhones = this.props.waitingPatientPhones;
            if (patientPhones[1].phoneNo.trim() === '') {
                patientPhones = [patientPhones[0]];
            }
            submitData.anonymousPatientDto = {
                ...this.props.waiting.anonymousPatientDto,
                docTypeCd: this.props.waitingDocTypeCd,
                engGivename: this.props.waitingEngGivenname,
                engSurname: this.props.waitingEngSurname,
                hkid: this.props.waitingHkid.replace('(', '').replace(')', ''),
                otherDocNo: this.props.waitingOtherDocNo,
                patientPhones
            };
        }

        //waiting info
        submitData.clinicCd = this.props.waitingClinicCd;
        submitData.countryCdList = this.props.waitingCountryCdList.join('|');
        submitData.encounterTypeCd = this.props.waitingEncounterTypeCd;
        submitData.remarks = this.props.waitingRemarks;
        // submitData.travelDtm = this.props.waitingTravelDate ? moment(this.props.waitingTravelDate).format('YYYY-MM-DD') : null;
        submitData.travelDtm = this.props.waitingTravelDate ? moment(this.props.waitingTravelDate).format(Enum.DATE_FORMAT_EYMD_VALUE) : null;
        this.props.saveWaiting({ statu: this.props.editWaitingStatus, data: submitData }, this.props.searchWaitingList);
    }

    cancelEditWaiting = () => {
        this.props.cancelEditWaiting();
    }

    waitingFieldOnChange = (e, fieldName, fieldType) => {
        if (fieldType) {
            switch (fieldType) {
                case 'TextField':
                    {
                        let fields = {};
                        if (fieldName !== 'waitingRemarks') {
                            fields[fieldName] = e.target.value.toUpperCase();
                        } else {
                            fields[fieldName] = e.target.value;
                        }
                        this.props.updateField(fields);
                    }
                    break;
                case 'SelectField':
                    {
                        let fields = {};
                        if (fieldName === 'waitingCountryCdList') {
                            if (this.props.waitingCountryCdList.length < 5 || e.length < 5) {
                                let waitingCountryCdList = [];
                                let countryCdList = e || [];
                                console.log(countryCdList);
                                countryCdList.forEach(item => {
                                    waitingCountryCdList.push(item.value);
                                });
                                fields[fieldName] = waitingCountryCdList;
                            }
                        } else {
                            fields[fieldName] = e.value;
                        }
                        this.props.updateField(fields);
                    }

                    break;
                case 'DateField':
                    {
                        let fields = {};
                        fields[fieldName] = e;
                        this.props.updateField(fields);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    waitingPatientPhonesOnChange = (e, phoneField, phoneIndex) => {
        let waitingPatientPhones = _.cloneDeep(this.props.waitingPatientPhones);
        if (phoneField === 'countryCd') {
            waitingPatientPhones[phoneIndex].countryCd = e.value;
        } else {
            waitingPatientPhones[phoneIndex].phoneNo = e.target.value.replace(/[^\d]/g, '');
        }
        this.props.updateField({ waitingPatientPhones });
    }

    waitingClinicCdOnChange = (e, encounterTypeCd) => {
        this.props.getEncounterList({ clinicCd: e.value }, { waitingEncounterTypeCd: encounterTypeCd });
    }

    searchPatientList = (value) => {
        const params = { searchString: value };
        this.props.searchPatientList(params);
    }

    handleItemSelected = (item) => {
        this.props.getPatient(item.patientKey);
    }
    render() {
        const {
            id,
            classes,
            editWaitingStatus,
            docTypeList,
            waiting,
            waitingHkid,
            waitingDocTypeCd,
            waitingOtherDocNo,
            waitingEngSurname,
            waitingEngGivenname,
            waitingPatientPhones,
            waitingClinicCd,
            waitingEncounterTypeList,
            waitingEncounterTypeCd,
            waitingTravelDate,
            waitingCountryCdList,
            waitingRemarks,
            clinicList,
            countryList
        } = this.props;
        let docTypeListNotID = docTypeList.filter(item => { return item.code !== 'ID'; });
        let hkidValidator = [];
        let hkidErrorMessages = [];

        const isHKIDFormat = PatientUtil.isHKIDFormat(waitingDocTypeCd);
        if (isHKIDFormat) {
            hkidValidator.push(ValidatorEnum.isHkid);
            hkidErrorMessages.push(CommonMessage.VALIDATION_NOTE_HKIC_FORMAT_ERROR());
        }

        const isShowDepartureDate = this.props.listConfig.WAITING_LIST && this.props.listConfig.WAITING_LIST.findIndex(item => item.labelCd === 'travelDtm') > -1;
        const isShowDestination = this.props.listConfig.WAITING_LIST && this.props.listConfig.WAITING_LIST.findIndex(item => item.labelCd === 'countryList') > -1;
        return (
            <CIMSPromptDialog
                id={id}
                dialogTitle={editWaitingStatus === 'E' ? 'Edit Waiting' : 'Add Waiting'}
                dialogContentText={
                    <Grid>
                        {editWaitingStatus !== 'E' ?
                            <Grid className={classes.formRowSearchRow}>
                                <SearchInput
                                    id={id + 'SearchPatientSearchInput'}
                                    displayField={['hkidOrDocno', 'engFullName', 'phoneAndCountry']}
                                    inputPlaceHolder={`Search by Case No/ ID/ Phone Number${CommonUtilities.getNameSearchCall()}`}
                                    onChange={this.searchPatientList}
                                    dataList={this.props.patientList}
                                    onSelectItem={this.handleItemSelected}
                                    upperCase
                                />
                            </Grid> : null
                        }

                        <ValidatorForm
                            id={id + 'Form'}
                            ref={'form'}
                            onSubmit={this.waitingSubmit}
                            className={classes.from}
                        >
                            <Grid>
                                <Grid className={classes.formRow}>
                                    <Grid className={classes.field}>
                                        <HKIDInput
                                            isHKID
                                            variant="outlined"
                                            disabled={waiting.patientKey && waiting.patientKey > 0}
                                            id={id + 'WaitingHkidTextField'}
                                            label="HKIC"
                                            msgPosition="bottom"
                                            value={waitingHkid}
                                            validByBlur
                                            validators={waiting.patientKey > 0 ? [] : [ValidatorEnum.isHkid]}
                                            errorMessages={waiting.patientKey > 0 ? [] : [CommonMessage.VALIDATION_NOTE_HKIC_FORMAT_ERROR()]}
                                            onChange={(e) => this.waitingFieldOnChange(e, 'waitingHkid', 'TextField')}
                                        />
                                    </Grid>
                                    <Grid className={classes.field}>
                                    </Grid>
                                </Grid>
                                <Grid className={classes.formRow}>
                                    <Grid className={classes.field}>
                                        <SelectFieldValidator
                                            id={id + 'WaitingDocTypeCdSelectField'}
                                            TextFieldProps={{
                                                variant: 'outlined',
                                                label: 'Document Type'
                                            }}
                                            fullWidth
                                            addNullOption
                                            options={docTypeListNotID.map(item => ({ value: item.code, label: item.engDesc }))}
                                            value={waitingDocTypeCd}
                                            isDisabled={waiting.patientKey && waiting.patientKey > 0}
                                            onChange={(e) => this.waitingFieldOnChange(e, 'waitingDocTypeCd', 'SelectField')}
                                        />
                                    </Grid>
                                    <Grid className={classes.field}>
                                        <HKIDInput
                                            isHKID={isHKIDFormat}
                                            variant="outlined"
                                            id={id + 'WaitingOtherDocNoTextField'}
                                            label="Document Number"
                                            msgPosition="bottom"
                                            fullWidth
                                            value={waitingOtherDocNo}
                                            inputProps={{ maxLength: 30 }}
                                            disabled={waiting.patientKey && waiting.patientKey > 0}
                                            validByBlur
                                            validators={hkidValidator}
                                            errorMessages={hkidErrorMessages}
                                            onChange={(e) => this.waitingFieldOnChange(e, 'waitingOtherDocNo', 'TextField')}
                                        />
                                    </Grid>
                                </Grid>

                                <Grid className={classes.formRow}>
                                    <Grid className={classes.field}>
                                        <TextFieldValidator
                                            id={id + 'WaitingEngSurnameTextField'}
                                            label={<>Surname<RequiredIcon /></>}
                                            fullWidth
                                            onlyOneSpace
                                            value={waitingEngSurname}
                                            variant="outlined"
                                            disabled={waiting.patientKey && waiting.patientKey > 0}
                                            msgPosition={'bottom'}
                                            inputProps={{
                                                maxLength: 40
                                            }}
                                            validByBlur
                                            validators={waiting.patientKey > 0 ? [] : [
                                                ValidatorEnum.required,
                                                ValidatorEnum.isSpecialEnglish
                                            ]}
                                            errorMessages={waiting.patientKey > 0 ? [] : [
                                                CommonMessage.VALIDATION_NOTE_REQUIRED(),
                                                CommonMessage.VALIDATION_NOTE_SPECIAL_ENGLISH()
                                            ]}
                                            warning={waiting.patientKey > 0 ? [] : [
                                                ValidatorEnum.isEnglishWarningChar
                                            ]}
                                            warningMessages={waiting.patientKey > 0 ? [] : [
                                                CommonMessage.WARNING_NOTE_SPECIAL_ENGLISH()
                                            ]}
                                            onChange={(e) => this.waitingFieldOnChange(e, 'waitingEngSurname', 'TextField')}
                                            trim={'all'}
                                        />
                                    </Grid>
                                    <Grid className={classes.field}>
                                        <TextFieldValidator
                                            id={id + 'WaitingEngGivennameTextField'}
                                            label={<>Given Name<RequiredIcon /></>}
                                            fullWidth
                                            onlyOneSpace
                                            value={waitingEngGivenname}
                                            variant="outlined"
                                            disabled={waiting.patientKey && waiting.patientKey > 0}
                                            msgPosition={'bottom'}
                                            inputProps={{
                                                maxLength: 40
                                            }}
                                            validByBlur
                                            validators={waiting.patientKey > 0 ? [] : [
                                                ValidatorEnum.required,
                                                ValidatorEnum.isSpecialEnglish
                                            ]}
                                            errorMessages={waiting.patientKey > 0 ? [] : [
                                                CommonMessage.VALIDATION_NOTE_REQUIRED(),
                                                CommonMessage.VALIDATION_NOTE_SPECIAL_ENGLISH()
                                            ]}
                                            warning={waiting.patientKey > 0 ? [] : [
                                                ValidatorEnum.isEnglishWarningChar
                                            ]}
                                            warningMessages={waiting.patientKey > 0 ? [] : [
                                                CommonMessage.WARNING_NOTE_SPECIAL_ENGLISH()
                                            ]}
                                            onChange={(e) => this.waitingFieldOnChange(e, 'waitingEngGivenname', 'TextField')}
                                            trim={'all'}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid className={classes.formRow}>
                                    <Grid className={classes.field}>
                                        <Grid className={classes.phoneField}>
                                            <Grid className={classes.phoneSelectField}>
                                                <SelectFieldValidator
                                                    id={id + 'ContactPhone_1_SelectField'}
                                                    TextFieldProps={{
                                                        variant: 'outlined',
                                                        label: <>Contact Phone 1<RequiredIcon /></>
                                                    }}
                                                    msgPosition="bottom"
                                                    fullWidth
                                                    options={countryList.map((item) => ({ value: item.countryCd, label: `${item.countryName || ''}(${item.dialingCd})` }))}
                                                    value={waitingPatientPhones.length > 0 ? waitingPatientPhones[0].countryCd : ''}
                                                    isDisabled={waiting.patientKey && waiting.patientKey > 0}
                                                    onChange={(e) => this.waitingPatientPhonesOnChange(e, 'countryCd', 0)}
                                                />
                                            </Grid>
                                            <Grid className={classes.phoneTextField}>
                                                <TextFieldValidator
                                                    id={id + 'ContactPhone_1_TextField'}
                                                    fullWidth
                                                    value={waitingPatientPhones.length > 0 ? waitingPatientPhones[0].phoneNo : ''}
                                                    msgPosition={'bottom'}
                                                    variant="outlined"
                                                    inputProps={{
                                                        maxLength: waitingPatientPhones[0].countryCd === 'HKG' ? RegFieldLength.CONTACT_PHONE_DEFAULT_MAX : RegFieldLength.CONTACT_PHONE_OTHERS_MAX
                                                    }}
                                                    validators={waiting.patientKey > 0 ? [] : waitingPatientPhones[0].countryCd === 'HKG' ? [
                                                        ValidatorEnum.required,
                                                        ValidatorEnum.hkPhoneNo
                                                    ] : [
                                                            ValidatorEnum.required,
                                                            ValidatorEnum.phoneNo
                                                        ]
                                                    }
                                                    errorMessages={waiting.patientKey > 0 ? [] :
                                                        waitingPatientPhones[0].countryCd === 'HKG' ? [
                                                            CommonMessage.VALIDATION_NOTE_REQUIRED(),
                                                            CommonMessage.VALIDATION_NOTE_HK_PHONE_NO()
                                                        ] : [
                                                                CommonMessage.VALIDATION_NOTE_REQUIRED(),
                                                                CommonMessage.VALIDATION_NOTE_PHONE_NO()]
                                                    }
                                                    validByBlur
                                                    disabled={waiting.patientKey && waiting.patientKey > 0}
                                                    onChange={(e) => this.waitingPatientPhonesOnChange(e, 'phoneNo', 0)}
                                                />
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid className={classes.field}>
                                        <Grid className={classes.phoneField}>
                                            <Grid className={classes.phoneSelectField}>
                                                <SelectFieldValidator
                                                    id={id + 'ContactPhone_2_SelectField'}
                                                    TextFieldProps={{
                                                        variant: 'outlined',
                                                        label: 'Contact Phone 2'
                                                    }}
                                                    variant="outlined"
                                                    msgPosition="bottom"
                                                    fullWidth
                                                    options={countryList.map((item) => ({ value: item.countryCd, label: `${item.countryName || ''}(${item.dialingCd})` }))}
                                                    isDisabled={waitingPatientPhones.length < 1 || (waiting.patientKey && waiting.patientKey > 0)}
                                                    value={waitingPatientPhones.length > 1 ? waitingPatientPhones[1].countryCd : ''}
                                                    onChange={(e) => this.waitingPatientPhonesOnChange(e, 'countryCd', 1)}
                                                />
                                            </Grid>
                                            <Grid className={classes.phoneTextField}>
                                                <TextFieldValidator
                                                    id={id + 'ContactPhone_2_TextField'}
                                                    fullWidth
                                                    inputProps={{
                                                        maxLength: waitingPatientPhones[1].countryCd === 'HKG' ? RegFieldLength.CONTACT_PHONE_DEFAULT_MAX : RegFieldLength.CONTACT_PHONE_OTHERS_MAX
                                                    }}
                                                    variant="outlined"
                                                    disabled={waitingPatientPhones.length < 1 || (waiting.patientKey && waiting.patientKey > 0)}
                                                    value={waitingPatientPhones.length > 1 ? waitingPatientPhones[1].phoneNo : ''}
                                                    msgPosition={'bottom'}
                                                    validators={waiting.patientKey > 0 ? [] : waitingPatientPhones[1].countryCd === 'HKG' ? [ValidatorEnum.hkPhoneNo] : [ValidatorEnum.phoneNo]}
                                                    errorMessages={waiting.patientKey > 0 ? [] :
                                                        waitingPatientPhones[0].countryCd === 'HKG' ? [CommonMessage.VALIDATION_NOTE_HK_PHONE_NO()] : [CommonMessage.VALIDATION_NOTE_PHONE_NO()]
                                                    }
                                                    validByBlur
                                                    onChange={(e) => this.waitingPatientPhonesOnChange(e, 'phoneNo', 1)}
                                                />
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>

                                <Grid className={classes.formRow}>
                                    <Grid className={classes.field}>
                                        <SelectFieldValidator
                                            id={id + 'ClinicSelectField'}
                                            TextFieldProps={{
                                                variant: 'outlined',
                                                label: <>Clinic<RequiredIcon /></>
                                            }}
                                            fullWidth
                                            options={clinicList.map((item) => ({ value: item.clinicCd, label: item.clinicName }))}
                                            value={waitingClinicCd}
                                            msgPosition={'bottom'}
                                            validators={[ValidatorEnum.required]}
                                            errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                            onChange={(e) => this.waitingClinicCdOnChange(e)}
                                        />
                                    </Grid>
                                    <Grid className={classes.field}>
                                        <SelectFieldValidator
                                            id={id + 'EncounterSelectField'}
                                            TextFieldProps={{
                                                variant: 'outlined',
                                                label: <>Encounter<RequiredIcon /></>
                                            }}
                                            fullWidth
                                            options={waitingEncounterTypeList.map(item => ({ value: item.encounterTypeCd, label: item.encounterTypeCd }))}
                                            value={waitingEncounterTypeCd}
                                            msgPosition={'bottom'}
                                            validators={[ValidatorEnum.required]}
                                            errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                            onChange={(e) => this.waitingFieldOnChange(e, 'waitingEncounterTypeCd', 'SelectField')}
                                        />
                                    </Grid>
                                </Grid>

                                {
                                    isShowDepartureDate ?
                                        <Grid className={classes.formRow}>
                                            <Grid className={classes.field}>
                                                <DateFieldValidator
                                                    id={id + 'TravelDateDateField'}
                                                    label="Departure Date"
                                                    value={waitingTravelDate}
                                                    disablePast
                                                    onChange={(e) => this.waitingFieldOnChange(e, 'waitingTravelDate', 'DateField')}
                                                />
                                            </Grid>
                                            <Grid className={classes.field}></Grid>
                                        </Grid> : null
                                }

                                {
                                    isShowDestination ?
                                        <Grid className={classes.formRow}>
                                            <Grid className={classes.field}>
                                                <SelectFieldValidator
                                                    id={id + 'CountrySelectField'}
                                                    TextFieldProps={{
                                                        variant: 'outlined',
                                                        label: <>Destination<RequiredIcon /></>
                                                    }}
                                                    isMulti
                                                    fullWidth
                                                    options={countryList.map((item) => ({ value: item.countryCd, label: item.countryName, nationality: item.nationality }))}
                                                    value={waitingCountryCdList}
                                                    msgPosition="bottom"
                                                    validators={[ValidatorEnum.required]}
                                                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                    onChange={(e) => this.waitingFieldOnChange(e, 'waitingCountryCdList', 'SelectField')}
                                                />
                                            </Grid>
                                        </Grid> : null
                                }

                                <Grid className={classes.formRow}>
                                    <Grid className={classes.field}>
                                        <TextFieldValidator
                                            id={id + 'RemarkTextField'}
                                            label="Remark"
                                            msgPosition="bottom"
                                            fullWidth
                                            value={waitingRemarks}
                                            inputProps={{ maxLength: 150 }}
                                            // validators={docType !== '*ALL' ? [ValidatorEnum.required] : []}
                                            // errorMessages={docType !== '*ALL' ? [CommonMessage.VALIDATION_NOTE_REQUIRED()] : []}
                                            onChange={(e) => this.waitingFieldOnChange(e, 'waitingRemarks', 'TextField')}
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </ValidatorForm>
                        <EditModeMiddleware componentName={accessRightEnum.waitingList} when={editWaitingStatus !== 'I'} />
                    </Grid>
                }
                open={editWaitingStatus !== 'I'}
                buttonConfig={
                    [
                        {
                            id: id + 'SubmitButton',
                            name: 'Submit',
                            onClick: () => { this.refs.form.submit(); }
                        },

                        {
                            id: id + 'CancelButton',
                            name: 'Cancel',
                            onClick: this.cancelEditWaiting
                        }
                    ]
                }
            />
        );
    }
}

const style = (theme) => ({
    formRowSearchRow: {
        marginBottom: 19
    },
    from: {
        minWidth: 852,
        minHeight: 420
    },
    formRow: {
        display: 'flex',
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1)
        // alignItems: 'flex-end'
    },
    field: {
        flex: 1,
        margin: '0 4px'
    },
    phoneField: {
        display: 'flex'
    },
    phoneSelectField: {
        width: 200
    },
    phoneTextField: {
        flex: 1,
        marginLeft: 4
    }
});

const mapStateToProps = (state) => {
    return {
        serviceCd: state.login.service.serviceCd,
        clinicCd: state.login.clinic.clinicCd,
        clinicConfig: state.common.clinicConfig,
        listConfig: state.common.listConfig,
        editWaitingStatus: state.waitingList.editWaitingStatus,
        docTypeList: state.waitingList.docTypeList,
        waiting: state.waitingList.waiting,
        waitingHkid: state.waitingList.waitingHkid,
        waitingDocTypeCd: state.waitingList.waitingDocTypeCd,
        waitingOtherDocNo: state.waitingList.waitingOtherDocNo,
        waitingEngSurname: state.waitingList.waitingEngSurname,
        waitingEngGivenname: state.waitingList.waitingEngGivenname,
        waitingPatientPhones: state.waitingList.waitingPatientPhones,
        waitingClinicCd: state.waitingList.waitingClinicCd,
        waitingEncounterTypeList: state.waitingList.waitingEncounterTypeList,
        waitingEncounterTypeCd: state.waitingList.waitingEncounterTypeCd,
        waitingTravelDate: state.waitingList.waitingTravelDate,
        waitingCountryCdList: state.waitingList.waitingCountryCdList,
        waitingRemarks: state.waitingList.waitingRemarks,
        clinicList: state.waitingList.clinicList,
        countryList: state.patient.countryList || [],
        patientList: state.waitingList.patientList
    };
};

const mapDispatchToProps = {
    updateField,
    cancelEditWaiting,
    getEncounterList,
    searchPatientList,
    getPatient,
    saveWaiting,
    resetAll
};
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(style)(EditWaitingDialog));