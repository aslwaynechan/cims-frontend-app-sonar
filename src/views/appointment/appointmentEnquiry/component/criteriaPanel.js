import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import CIMSSelect from '../../../../components/Select/CIMSSelect';
import CIMSDatePicker from '../../../../components/DatePicker/CIMSDatePicker';
// import { statusList } from '../../../../enums/appointment/attendance/attendanceStatus';
import Enum from '../../../../enums/enum';
import {
    getEncounterType,
    openCommonCircular,
    closeCommonCircular
} from '../../../../store/actions/common/commonAction';
import moment from 'moment';
import CIMSButtonGroup from '../../../../components/Buttons/CIMSButtonGroup';
import ApptEnum from '../enum/apptEnquiryEnum';
import {
    fetchEnquiryResult,
    printApptReport
} from '../../../../store/actions/appointment/apptEnquiry/apptEnquiryAction';
import { initSubEncounterList } from '../../../../utilities/commonUtilities';

const styles = (theme) => ({
    root: {
        width: '100%',
        display: 'flex',
        height: '100%',
        flexFlow: 'row'
    },
    paddingLeft: {
        paddingLeft: theme.spacing(2)
    }
});

class CriteriaPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            curEnquiryParam: null,
            curEncounterList: [],
            curSubEncounterList: [],
            curClinicList: []
        };
    }

    componentDidMount() {
        this.initCriterialInfo();
    }

    initCriterialInfo = () => {
        const { serviceCd, clinicCd, encounterList, clinicList } = this.props;
        let tempInfo = { ...this.props.criteriaInfo };
        let subEncounterList = initSubEncounterList(encounterList);
        let tempClinicList = [];

        tempInfo.clinicCd = clinicCd;

        clinicList.forEach(clinic => {
            if (clinic.serviceCd === serviceCd) {
                tempClinicList.push(clinic);
            }
        });

        this.props.handleCriteriaChange({ criteria: tempInfo });
        this.setState({
            curEncounterList: encounterList,
            curSubEncounterList: subEncounterList,
            curClinicList: tempClinicList
        });
    }

    handleInputChange = (name, value) => {
        let tempInfo = { ...this.props.criteriaInfo };
        let dateFrom = tempInfo.dateFrom;
        let type = tempInfo.type;
        tempInfo[name] = value;

        if (name === 'dateFrom') {
            if (moment(tempInfo.dateTo).isBefore(moment(value))) {
                tempInfo.dateTo = value;
            }
            if (value === null) {
                tempInfo.dateFrom = moment();
            }
            if (value > moment().endOf('days') || type === ApptEnum.APPT_TYPE.DEFAULTER_TRACING) {
                tempInfo.status = 'N';
            }
            else {
                tempInfo.status = 'ALL';
            }
        }

        if (name === 'dateTo') {
            if (moment(dateFrom).isAfter(moment(value))) {
                tempInfo.dateFrom = value;
            }
            if (value === null) {
                tempInfo.dateTo = moment();
            }
            if (dateFrom > moment().endOf('days') || type === ApptEnum.APPT_TYPE.DEFAULTER_TRACING) {
                tempInfo.status = 'N';
            }
            else {
                tempInfo.status = 'ALL';
            }
        }

        if (name === 'type') {
            if (value === ApptEnum.APPT_TYPE.DEFAULTER_TRACING || dateFrom >= moment().endOf('days')) {
                tempInfo.status = 'N';
            }
            else {
                tempInfo.status = 'ALL';
            }
            this.props.changeTableHeader(value);
            this.props.handleCriteriaChange({ enquiryResult: [] });
            tempInfo.encounterCd = 'ALL';
            tempInfo.subEncounterCd = 'ALL';

        }

        if (name === 'clinicCd') {
            this.props.getEncounterType({ clinicCd: value });
            tempInfo.encounterCd = 'ALL';
            tempInfo.subEncounterCd = 'ALL';
            if (tempInfo.type === ApptEnum.APPT_TYPE.APPT_LIST && dateFrom <= moment().endOf('days')) {
                tempInfo.status = 'ALL';
            }

        }

        if (name === 'encounterCd') {
            this.filterCounter(name, value);
            const subEncounterCd = tempInfo.subEncounterCd;
            // tempInfo.subEncounterCd = 'ALL';
            if (value === 'ALL' || subEncounterCd === 'ALL') {
                tempInfo.subEncounterCd = 'ALL';
            }
            else {
                let subEncounter = this.state.curSubEncounterList.find(item => item.subEncounterTypeCd === subEncounterCd);
                let encounterIdx = subEncounter.parentGp.findIndex(item => item.encounterTypeCd === value);
                tempInfo.subEncounterCd = encounterIdx > -1 ? tempInfo.subEncounterCd : 'ALL';
            }
        }

        if (name === 'subEncounterCd') {
            this.filterCounter(name, value);
        }

        this.props.handleCriteriaChange({ criteria: tempInfo });
    }

    handleSearch = () => {
        const { criteriaInfo } = this.props;
        let param = {
            attnStatusCd: criteriaInfo.status === 'ALL' ? '' : criteriaInfo.status,
            clinicCd: criteriaInfo.clinicCd,
            encounter: criteriaInfo.encounterCd === 'ALL' ? '' : criteriaInfo.encounterCd,
            subEncounter: criteriaInfo.subEncounterCd === 'ALL' ? '' : criteriaInfo.subEncounterCd,
            from: moment(criteriaInfo.dateFrom).format(Enum.DATE_FORMAT_EYMD_VALUE),
            to: moment(criteriaInfo.dateTo).format(Enum.DATE_FORMAT_EYMD_VALUE),
            reportType: criteriaInfo.type
        };
        this.setState({ curEnquiryParam: param });
        this.props.fetchEnquiryResult(param);
    }

    handlePrint = () => {
        let { curEnquiryParam } = this.state;
        if (!curEnquiryParam || this.props.enquiryResult.length === 0) {
            return;
        }
        this.props.openCommonCircular();
        this.props.printApptReport(curEnquiryParam,
            () => {
                this.props.closeCommonCircular();
            }
        );
    }

    filterCounter = (name, value) => {
        const { encounterList } = this.props;
        let tempSubEncounterList = [];
        const allSubEncounterList = initSubEncounterList(encounterList);

        if (name === 'encounterCd') {
            allSubEncounterList.forEach(subEncounter => {
                let encounterIdx = subEncounter.parentGp.findIndex(item => item.encounterTypeCd === value);
                if (encounterIdx > -1) {
                    tempSubEncounterList.push(subEncounter);
                }
            });
            this.setState({
                curSubEncounterList: tempSubEncounterList
            });
            if (value === 'ALL') {
                this.setState({
                    curEncounterList: encounterList,
                    curSubEncounterList: allSubEncounterList
                });
            }
        }
        else {
            let curEncounterList = [];
            let selectedSubEncounter = allSubEncounterList.find(item => item.subEncounterTypeCd === value);

            if (selectedSubEncounter && selectedSubEncounter.parentGp) {
                selectedSubEncounter.parentGp.forEach(parent => {
                    curEncounterList.push(parent);
                });
                this.setState({ curEncounterList });
            }
        }
    }

    render() {
        const { id, criteriaInfo, classes, enquiryResult } = this.props;
        const apptTypeList = ApptEnum.APPT_TYPE_LIST;
        const { curEncounterList, curSubEncounterList, curClinicList } = this.state;

        return (
            <Grid container className={classes.root} spacing={2}>
                <Grid item xs={10}>
                    <Grid item container xs={12} direction={'row'} spacing={4}>
                        <Grid item xs={5}>
                            <CIMSSelect
                                id={`${id}_appointment_type_select`}
                                TextFieldProps={{
                                    variant: 'outlined',
                                    label: 'Report Type'
                                }}
                                value={criteriaInfo.type}
                                options={apptTypeList && apptTypeList.map(item => ({ value: item.value, label: item.type }))}
                                onChange={(e) => this.handleInputChange('type', e.value)}
                            />
                        </Grid>
                        <Grid item xs={5}>
                            <CIMSSelect
                                id={`${id}_clinic_select`}
                                TextFieldProps={{
                                    variant: 'outlined',
                                    label: 'Clinic'
                                }}
                                value={criteriaInfo.clinicCd}
                                options={curClinicList && curClinicList.map(item => ({ value: item.clinicCd, label: item.clinicName }))}
                                onChange={(e) => this.handleInputChange('clinicCd', e.value)}
                            />
                        </Grid>

                    </Grid>
                    <Grid item container xs={12} direction={'row'} spacing={4}>
                        <Grid item container xs={5} direction={'row'}>
                            <Grid item xs={6} >
                                <CIMSDatePicker
                                    id={`${id}_date_from_date_picker`}
                                    label={'Date From'}
                                    value={criteriaInfo.dateFrom}
                                    onChange={(e) => this.handleInputChange('dateFrom', e)}
                                />
                            </Grid>
                            <Grid item xs={6} className={classes.paddingLeft}>
                                <CIMSDatePicker
                                    id={`${id}_date_to_date_picker`}
                                    label={'Date To'}
                                    value={criteriaInfo.dateTo}
                                    onChange={(e) => this.handleInputChange('dateTo', e)}
                                />
                            </Grid>
                        </Grid>
                        <Grid item container xs={5} direction={'row'}>
                            <Grid item xs={4}>
                                <CIMSSelect
                                    id={`${id}_encounter_select`}
                                    TextFieldProps={{
                                        variant: 'outlined',
                                        label: 'Encounter'
                                    }}
                                    options={
                                        [
                                            { value: 'ALL', label: 'All' }, ...curEncounterList &&
                                            curEncounterList.map(item => ({ value: item.encounterTypeCd, label: item.encounterTypeCd }))
                                        ]
                                    }
                                    value={criteriaInfo.encounterCd}
                                    onChange={(e) => this.handleInputChange('encounterCd', e.value)}
                                />
                            </Grid>

                            <Grid item xs={4} className={classes.paddingLeft}>
                                <CIMSSelect
                                    id={`${id}_sub_encounter_select`}
                                    TextFieldProps={{
                                        variant: 'outlined',
                                        label: 'Sub-encounter'
                                    }}
                                    options={
                                        [
                                            { value: 'ALL', label: 'All' }, ...curSubEncounterList &&
                                            curSubEncounterList.map(item => ({ value: item.subEncounterTypeCd, label: item.subEncounterTypeCd }
                                            ))
                                        ]
                                    }
                                    value={criteriaInfo.subEncounterCd}
                                    onChange={(e) => this.handleInputChange('subEncounterCd', e.value)}
                                />
                            </Grid>

                            <Grid item xs={4} className={classes.paddingLeft}>
                                <CIMSSelect
                                    id={`${id}_status_select`}
                                    TextFieldProps={{
                                        variant: 'outlined',
                                        label: 'Status'
                                    }}
                                    options={[
                                        { value: 'ALL', label: 'All' },
                                        ...Enum.ATTENDANCE_STATUS_LIST &&
                                        Enum.ATTENDANCE_STATUS_LIST.map((item) => (
                                            { value: item.value, label: item.label }
                                        ))
                                    ]}
                                    value={criteriaInfo.status}
                                    onChange={(e) => this.handleInputChange('status', e.value)}
                                    isDisabled={criteriaInfo.type === ApptEnum.APPT_TYPE.DEFAULTER_TRACING || criteriaInfo.dateFrom > moment().endOf('days')}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item container xs={2} alignItems={'center'} justify={'flex-end'} style={{ paddingRight: 0 }}>
                    <CIMSButtonGroup
                        customStyle={{
                            position: 'unset',
                            right: 0,
                            width: 'auto',
                            zIndex: 999,
                            padding: 0
                        }}
                        buttonConfig={
                            [
                                {
                                    id: `${id}_appointment_enquiry_search_button`,
                                    name: 'Search',
                                    onClick: this.handleSearch
                                },
                                {
                                    id: `${id}_appointment_enquiry_print_button`,
                                    name: 'Print',
                                    onClick: this.handlePrint,
                                    disabled: (enquiryResult.length === 0)
                                }
                            ]
                        }
                    />
                    {/* <CIMSButton
                        id={`${id}_appointment_enquiry_search_button`}
                        children={'Search'}
                        onClick={this.handleSearch}
                    />
                    <CIMSButton
                        id={`${id}_appointment_enquiry_print_button`}
                        children={'Print'}
                        onClick={this.handlePrint}
                        disabled={enquiryResult.length === 0}
                    /> */}
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        encounterList: state.common.encounterTypeList,
        enquiryResult: state.apptEnquiry.enquiryResult
    };
};

const mapDispatchToProps = {
    getEncounterType,
    fetchEnquiryResult,
    printApptReport,
    openCommonCircular,
    closeCommonCircular
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(CriteriaPanel));