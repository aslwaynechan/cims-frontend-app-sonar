const APPT_ENUM = {
    APPT_TYPE_LIST: [
        {
            type: 'Appointment List',
            value: 'A'
        },
        {
            type: 'Defaulter Tracing Report',
            value: 'D'
        }
    ],
    APPT_TYPE: {
        /**
        * Appointment List report
        */
        APPT_LIST: 'A',
        /**
         * Defaulter Tracing report
         */
        DEFAULTER_TRACING: 'D'
    },
    APPT_LIST_HEADER: [
        {
            labelCd: 'appointmentTime',
            labelName: 'Date/Time',
            labelLength: 105,
            site: '1'
        },
        {
            labelCd: 'patientName',
            labelName: 'Name',
            labelLength: 100,
            site: '1'
        },
        {
            labelCd: 'caseNo',
            labelName: 'Case No.',
            labelLength: 100,
            site: '1'
        },
        {
            labelCd: 'hkid',
            labelLength: 100,
            labelName: 'HKIC/Doc.No',
            site: '1'
        },
        {
            labelCd: 'age',
            labelName: 'Age',
            labelLength: 50,
            site: '1'
        },
        {
            labelCd: 'genderCd',
            labelName: 'Sex',
            labelLength: 70,
            site: '1'
        },
        {
            labelCd: 'phoneNo',
            labelName: 'Mobile No.',
            labelLength: 100,
            site: '1'
        },
        {
            labelCd: 'encounter',
            labelName: 'Encounter',
            labelLength: 100,
            site: '0'
        },
        {
            labelCd: 'subEncounter',
            labelName: 'Sub-encounter',
            labelLength: 120,
            site: '0'
        },
        {
            labelCd: 'attnStatusCd',
            labelName: 'Attn.',
            labelLength: 50,
            site: '0'
        },
        {
            labelCd: 'caseTypeCd',
            labelName: 'Type',
            labelLength: 50,
            site: '0'
        },

        {
            labelCd: 'remarkAndMemo',
            labelName: 'Remark/Memo',
            labelLength: 220,
            site: '0'
        }
    ],
    DEFAULTER_TRANCING_HEADER: [
        {
            labelCd: 'patientName',
            labelName: 'Name',
            labelLength: 200,
            site: '1'
        },
        {
            labelCd: 'caseNo',
            labelName: 'Case No.',
            labelLength: 100,
            site: '1'
        },
        {
            labelCd: 'hkid',
            labelLength: 100,
            labelName: 'HKIC/Doc.No',
            site: '1'
        },
        {
            labelCd: 'genderCd',
            labelName: 'Sex',
            labelLength: 70,
            site: '1'
        },
        {
            labelCd: 'age',
            labelName: 'Age',
            labelLength: 50,
            site: '1'
        },
        {
            labelCd: 'phoneNo',
            labelName: 'Mobile No.',
            labelLength: 100,
            site: '1'
        },
        {
            labelCd: 'defaulterNumber',
            labelName: 'Defaulter Number',
            labelLength: 70,
            site: '0'
        }
    ]

};


export default APPT_ENUM;