import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core';
import moment from 'moment';
import CIMSPromptDialog from '../../components/Dialog/CIMSPromptDialog';
import Enum from '../../enums/enum';
import { getPatientCaseNo, resetAll } from '../../store/actions/patient/patientAction';
import { cleanSubTabs } from '../../store/actions/mainFrame/mainFrameAction';
import { selectCaseTrigger, updateState } from '../../store/actions/caseNo/caseNoAction';
import * as caseNoUtilities from '../../utilities/caseNoUtilities';
import AutoScrollTable from '../../components/Table/AutoScrollTable';

const styles = () => ({
    container: {
        maxWidth: 1000
    }
});

class CaseNoSelectDialog extends Component {

    state = {
        columns: [
            {
                label: 'Case No.',name: 'caseNo', width: 150,
                customBodyRender: (value) => {
                    return caseNoUtilities.getFormatCaseNo(value);
                }
            },
            { label: 'Encounter', name: 'encounterTypeCd', width: 100 },
            { label: 'Sub-encounter', name: 'subEncounterTypeCd', width: 150 },
            { label: 'Patient Status', name: 'patientStatus', width: 150 },
            { label: 'Case Reference', name: 'caseReference', width: 150 },
            { label: 'Owner Clinic', name: 'ownerClinicCd', width: 120 },
            {
                label: 'Registration Date', name: 'regDtm', width: 150,
                customBodyRender: (value) => {
                    return value && moment(value).format(Enum.DATE_FORMAT_EDMY_VALUE);
                }
            },
            { label: 'Remark', name: 'remark', width: 150 }
        ],
        selectIndex: []
    }

    componentDidMount() {
        // let { caseNoInfo, patientInfo } = this.props;
        // if (!caseNoInfo.caseNo) {
        //     if (patientInfo && patientInfo.caseList) {
        //         const caseActiveDtos = patientInfo.caseList.filter(item => item.statusCd === Enum.CASE_STATUS.ACTIVE);
        //         if (caseActiveDtos.length === 1) {
        //             caseNoInfo.caseNo = caseActiveDtos[0]['caseNo'];
        //             this.props.getPatientCaseNo(patientInfo.caseList, caseNoInfo.caseNo);
        //             this.props.selectCaseTrigger({ tigger: false });
        //             this.props.caseSelectCallBack && this.props.caseSelectCallBack(true);
        //         }
        //     }
        // }
    }

    componentWillUnmount() {
        this.props.updateState({ openSelectCase: false, caseSelectCallBack: null });
    }

    handleTableRowClick = (e, row, index) => {
        if (this.state.selectIndex.indexOf(index) > -1) {
            this.setState({ selectIndex: [] });
        } else {
            this.setState({ selectIndex: [index] });
        }
    }

    handleTableRowDbClick = (e, row, index) => {
        const { patientInfo } = this.props;
        let selectCaseNo = patientInfo.caseList[index].caseNo;
        this.props.getPatientCaseNo(patientInfo.caseList, selectCaseNo);
        this.props.selectCaseTrigger({ tigger: false });
        this.props.caseSelectCallBack && this.props.caseSelectCallBack(true);
    }

    render() {
        const { classes, patientInfo, openSelectCase } = this.props;
        return (
            <CIMSPromptDialog
                id="indexPatient_selectCaseNoDialog"
                dialogTitle="Select CaseNo"
                dialogContentText={
                    <AutoScrollTable
                        id="caseNoSelectDialog_scrollTable"
                        columns={this.state.columns}
                        store={patientInfo && patientInfo.caseList && patientInfo.caseList.filter(item => item.statusCd === Enum.CASE_STATUS.ACTIVE)}
                        selectIndex={this.state.selectIndex}
                        handleRowClick={this.handleTableRowClick}
                        handleRowDbClick={this.handleTableRowDbClick}
                        classes={{
                            container: classes.container
                        }}
                    />
                }
                open={openSelectCase}
                buttonConfig={
                    [
                        {
                            id: 'indexPatient_selectCaseNoDialog_okBtn',
                            name: 'OK',
                            disabled: this.state.selectIndex.length === 0,
                            onClick: () => {
                                let selectCaseNo = patientInfo.caseList[this.state.selectIndex[0]].caseNo;
                                this.props.getPatientCaseNo(patientInfo.caseList, selectCaseNo);
                                this.props.selectCaseTrigger({ tigger: false });
                                this.props.caseSelectCallBack && this.props.caseSelectCallBack(true);
                            }
                        },
                        {
                            id: 'indexPatient_selectCaseNoDialog_cancelBtn',
                            name: 'Cancel',
                            onClick: () => {
                                this.props.cleanSubTabs();
                                this.props.resetAll();
                                this.props.selectCaseTrigger({ tigger: false });
                            }
                        }
                    ]
                }
            />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        patientInfo: state.patient.patientInfo,
        caseNoInfo: state.patient.caseNoInfo,
        openSelectCase: state.caseNo.openSelectCase,
        caseSelectCallBack: state.caseNo.caseSelectCallBack
    };
};

const mapDispatchToProps = {
    getPatientCaseNo,
    resetAll,
    cleanSubTabs,
    selectCaseTrigger,
    updateState
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(CaseNoSelectDialog));