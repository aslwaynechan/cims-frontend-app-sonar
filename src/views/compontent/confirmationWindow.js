import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
    Grid,
    Typography
} from '@material-ui/core';
import * as AppointmentUtilities from '../../utilities/appointmentUtilities';
import * as CaseUtilities from '../../utilities/caseNoUtilities';
import Enum from '../../enums/enum';

const styles = theme => ({
    confirmationInfo: {
        height: '100%',
        backgroundColor: theme.palette.primary.main,
        position: 'relative'
    },
    confirmationInfoItem: {
        width: '100%',
        fontSize: '2.5em',
        color: theme.palette.white,
        textAlign: 'center',
        padding: '0px 8px'
    },
    caseContent: {
        fontSize: '1.25em',
        position: 'absolute',
        bottom: '40px',
        right: '70px',
        padding: '13px 25px',
        background: '#d1d11c',
        fontWeight: 'bolder'
    }
});

class ConfirmationWindow extends React.Component {

    render() {
        const { classes, patientInfo, confirmationInfo, id, type } = this.props;
        let hkid;
        let hkidNum;
        if (patientInfo && patientInfo.hkid) {
            hkid = patientInfo.hkid.substring(0, patientInfo.hkid.length - 1);
            hkidNum = patientInfo.hkid.substring(patientInfo.hkid.length - 1);
        } else if (patientInfo && patientInfo.otherDocNo) {
            hkid = patientInfo.otherDocNo;
            hkidNum = patientInfo.docTypeCd;
        }

        return (
            <Grid item container direction="column" xs={12} justify="center" className={classes.confirmationInfo}>
                <Typography
                    id={`${id}_doc_no`}
                    className={classes.confirmationInfoItem}
                >
                    {`${hkid}(${hkidNum})`}
                </Typography >
                {/* {
                    type ? <Typography className={classes.confirmationInfoItem}> {type}</Typography> : null
                } */}
                {/* <Typography className={classes.confirmationInfoItem}>Take Attendance</Typography> */}

                <Typography
                    id={`${id}_english_name`}
                    className={classes.confirmationInfoItem}
                >
                    {`${patientInfo.engSurname}, ${patientInfo.engGivename}`}
                </Typography>

                {
                    patientInfo.nameChi ?
                        <Typography
                            id={`${id}_chinese_name`}
                            className={classes.confirmationInfoItem}
                        >
                            {`(${patientInfo.nameChi})`}
                        </Typography>
                        : null
                }

                <Typography
                    id={`${id}_case_no`}
                    className={classes.confirmationInfoItem}
                >
                    {confirmationInfo && `${CaseUtilities.getFormatCaseNo(confirmationInfo.caseNo)}`}
                </Typography>

                <Typography
                    id={`${id}_encounter_and_sub_encounter`}
                    className={classes.confirmationInfoItem}
                >
                    {confirmationInfo && `${confirmationInfo.encounterTypeCd} - ${confirmationInfo.subEncounterTypeCd}`}
                </Typography>

                <Typography
                    id={`${id}_appointment_date_and_time`}
                    className={classes.confirmationInfoItem}
                >
                    {
                        confirmationInfo && AppointmentUtilities.combineApptDateAndTime(confirmationInfo, Enum.DATE_FORMAT_EDMY_VALUE, Enum.TIME_FORMAT_12_HOUR_CLOCK)
                    }
                </Typography>

                {
                    confirmationInfo && confirmationInfo.patientStatusCd ?
                        <Typography
                            id={`${id}_patient_status`}
                            className={classes.confirmationInfoItem}
                        >
                            {/* {`Status(${this.props.patientStatus})`} */}
                            {`Status (${confirmationInfo.patientStatusCd})`}
                        </Typography> : null
                }

                {
                    confirmationInfo && confirmationInfo.amount ?
                        <Typography
                            id={`${id}_amount`}
                            className={classes.confirmationInfoItem}
                        >
                            {`Amt: $${confirmationInfo.amount}`}
                        </Typography> : null
                }

                {
                    confirmationInfo && confirmationInfo.paymentMeanCD ?
                        <Typography
                            id={`${id}_pay_means`}
                            className={classes.confirmationInfoItem}
                        >
                            {`Pay Means: ${confirmationInfo.paymentMeanCD}`}
                        </Typography> : null
                }

                {
                    type === Enum.APPOINTMENT_TYPE.BOOKING && confirmationInfo && confirmationInfo.remarkId ?
                        <Typography
                            id={`${id}_remark`}
                            className={classes.confirmationInfoItem}
                            style={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}
                        >
                            {`Remark : ${confirmationInfo.remark}`}
                        </Typography>
                        : null
                }
                {
                    type === Enum.APPOINTMENT_TYPE.BOOKING && confirmationInfo && confirmationInfo.memo ?
                        <Typography
                            id={`${id}_remark`}
                            className={classes.confirmationInfoItem}
                            style={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}
                        >
                            {`Memo : ${confirmationInfo.memo}`}
                        </Typography>
                        : null
                }
                {
                    (type === Enum.APPOINTMENT_TYPE.BOOKING || type === Enum.APPOINTMENT_TYPE.WALK_IN) && confirmationInfo && confirmationInfo.caseTypeCd ?

                        <div
                            id={`${id}_caseType`}
                            className={classes.caseContent}
                        >
                            {confirmationInfo.caseTypeCd == 'N' ? 'New Case' : 'Old Case'}
                        </div>

                        : null
                }
            </Grid>
        );
    }
}
export default withStyles(styles)(ConfirmationWindow);