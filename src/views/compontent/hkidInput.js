import React, { Component } from 'react';
import TextFieldValidator from '../../components/FormValidator/TextFieldValidator';
import _ from 'lodash';

export default class HKIDInput extends Component {

    componentDidMount() {
        if (this.props.isHKID && this.props.value && this.props.onChange) {
            const val = this.getFormatHKID(this.props.value);
            this.props.onChange({ target: { value: val } });
        }
    }

    UNSAFE_componentWillUpdate(nextProps) {
        if (this.props.isHKID !== nextProps.isHKID && nextProps.value) {
            const val = nextProps.isHKID ? this.getFormatHKID(nextProps.value) : nextProps.value.replace('(', '').replace(')', '');
            if (this.props.onChange) {
                this.props.onChange({ target: { value: val } });
            }
        }
    }

    getFormatHKID(value) {
        let val = _.cloneDeep(value);
        val = val.replace('(', '').replace(')', '');
        const len = val.length;
        val = val.substr(0, len - 1) + '(' + val.substr(len - 1) + ')';
        return val;
    }

    handleOnFocus = (e) => {
        if (e.target.value && this.props.isHKID) {
            e.target.value = e.target.value.replace('(', '').replace(')', '');
        }
        if (this.props.onChange) {
            this.props.onChange(e);
        }
        if (this.props.onFocus) {
            this.props.onFocus(e);
        }
    }

    handleOnBlur = (e) => {
        if (e.target.value) {
            e.target.value = e.target.value.replace(/\s+/g, '');
            if (this.props.isHKID && e.target.value.length >= 8) {
                e.target.value = e.target.value.replace('(', '').replace(')', '');
                const len = e.target.value.length;
                e.target.value = e.target.value.substr(0, len - 1) + '(' + e.target.value.substr(len - 1) + ')';
            } else {
                e.target.value = e.target.value.replace('(', '').replace(')', '');
            }
        }
        if (this.props.onChange) {
            this.props.onChange(e);
        }
        if (this.props.onBlur) {
            this.props.onBlur(e);
        }
    }

    render() {
        //eslint-disable-next-line
        const { isHKID, onBlur, onFocus, inputProps, ...rest } = this.props;
        let maxLength = isHKID ? 9 : 30;
        if(inputProps && inputProps.maxLength){
            maxLength = inputProps.maxLength;
        }
        return (
            <TextFieldValidator
                upperCase
                fullWidth
                onBlur={this.handleOnBlur}
                onFocus={this.handleOnFocus}
                inputProps={{
                    ...inputProps,
                    maxLength: maxLength
                }}
                {...rest}
            />
        );
    }
}