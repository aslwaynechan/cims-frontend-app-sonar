import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Link, FilledInput, Typography, FormControlLabel, Box, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import moment from 'moment';
import _ from 'lodash';

import CIMSInputLabel from '../../components/InputLabel/CIMSInputLabel';
import CIMSCheckBox from '../../components/CheckBox/CIMSCheckBox';
import Enum from '../../enums/enum';
import { patientSocialDataBasic } from '../../constants/registration/registrationConstants';
import AutoScrollTable from '../../components/Table/AutoScrollTable';
import ButtonStatusEnum from '../../enums/registration/buttonStatusEnum';
import FieldConstant from '../../constants/fieldConstant';
import accessRightEnum from '../../enums/accessRightEnum';

import { updateState, getPatientById as getRegPatientById, resetAll } from '../../store/actions/registration/registrationAction';
import { openCommonCircular, closeCommonCircular } from '../../store/actions/common/commonAction';
import { skipTab } from '../../store/actions/mainFrame/mainFrameAction';
import {
    getPatientById as getPatientPanelPatientById,
    updateState as updatePatient
} from '../../store/actions/patient/patientAction';
import { updateField as updateBookingField } from '../../store/actions/appointment/booking/bookingAction';
import { openCommonMessage } from '../../store/actions/message/messageAction';
import { openCaseNoDialog, selectCaseTrigger } from '../../store/actions/caseNo/caseNoAction';
import { deleteTabs } from '../../store/actions/mainFrame/mainFrameAction';
// import {getMessageDescriptionByMsgCode} from '../../utilities/messageUtilities';
import CIMSButtonGroup from '../../components/Buttons/CIMSButtonGroup';
import * as caseNoUtilities from '../../utilities/caseNoUtilities';

import CIMSButton from '../../components/Buttons/CIMSButton';
import {ecsSelector, ocsssSelector, mwecsSelector, summaryPatientInfoSelector, summaryPatientKeySelector} from '../../store/selectors/ecsSelectors';
import {openMwecsDialog,openEcsDialog,openOcsssDialog, setMwecsPatientStatusInPatientSummary,checkOcsss, setOcsssPatientStatusInPatientSummary, setEcsPatientStatusInPatientSummary} from '../../store/actions/ECS/ecsAction';
import * as EcsUtilities from '../../utilities/ecsUtilities';
import EcsResultTextField from 'components/ECS/Ecs/EcsResultTextField';
import OcsssResultTextField from 'components/ECS/Ocsss/OcsssResultTextField';

import MwecsResultTextField from 'components/ECS/Mwecs/MwecsResultTextField';
import MwecsMessageIdTextField from 'components/ECS/Mwecs/MwecsMessageIdTextField';

//eslint-disable-next-line
const useStyle = (theme) => ({
    root: {
    },
    container: {
        minHeight: 180,
        padding: '10px 15px 0px'
    },
    gridPadding: {
        padding: theme.spacing(0.2)
    },
    borderTop: {
        borderTopColor: theme.palette.primary.main,
        borderTopStyle: 'solid',
        borderTopWidth: 1
    },
    headerTop: {
        justifyContent: 'space-between',
        alignItems: 'baseline',
        marginBottom: 5
    },
    contactPersonContainer: {
        //border: '1px solid #c3c3c3',
        borderRadius: 4,
        marginBottom: 5
    },
    contactPersonScrollGrid: {
        overflowY: 'auto',
        overflowX: 'hidden'
    },
    tableContainer: {
        height: 'auto',
        maxHeight: 140
    },
    tableRowRoot: {
        height: 'unset'
    },
    formControlLabelRoot: {
        marginLeft: 11
    },
    formControlLabelLabel: {
        color: theme.palette.primary.main,
        fontWeight: 'bold'
    },
    checkBox: {
        padding: 0
    }
});

const StyledFilledInput = withStyles({
    // root: {
    //     height: 26
    // },
    input: {
        padding: '0px 14px',
        textOverflow: 'ellipsis'
    }
})((props) => {
    return (
        <FilledInput title={props.value || ''} {...props} />
    );
});

class ReviewInformation extends Component {

    state = {
        selectIndex: [],
        selectedCaseNo: '',
        isShowClosedCase: false
    }

    componentDidMount() {
        const caseActiveDtos = this.props.patientById.caseList && this.props.patientById.caseList.filter(item => item.statusCd === Enum.CASE_STATUS.ACTIVE);
        if (this.props.isSaveSuccess && this.props.isAutoGen === 'Y' && caseActiveDtos.length == 0) {
            this.props.openCaseNoDialog({
                caseDialogStatus: Enum.CASE_DIALOG_STATUS.CREATE,
                caseNoForm: { patientKey: this.props.patientById.patientKey },
                caseCallBack: () => { this.props.getRegPatientById(this.props.patientById.patientKey); }
            });
        }
    }

    handleOnEdit = (e, index) => {
        this.props.openCommonCircular();
        setTimeout(() => {
            this.props.updateState({ isOpenReview: false, patientOperationStatus: ButtonStatusEnum.EDIT });
            this.props.onChangeStep(index, () => { this.props.closeCommonCircular(); });
        }, 10);
    }

    handleClose = () => {
        this.props.updateState({ isOpenReview: false });
        this.props.onChangeStep(0);
        this.props.resetAll();
    }

    transformToGroupCd = () => {
        let defaultLangGroup = '';
        const { patientBaseInfo, languageData } = this.props;
        const langGroupList = languageData.codeLangGroupDtos;
        patientBaseInfo.preferredLangCd && langGroupList && langGroupList.forEach((item) => {
            let filterPreferredLangList = languageData && languageData.codePreferredLangMap && languageData.codePreferredLangMap[item.langGroupCd] && languageData.codePreferredLangMap[item.langGroupCd].filter(itemObj => itemObj.preferredLangCd == patientBaseInfo.preferredLangCd);
            if (filterPreferredLangList && filterPreferredLangList.length == 1) {
                defaultLangGroup = filterPreferredLangList && filterPreferredLangList[0] && filterPreferredLangList[0].langGroupCd;
            } else if (filterPreferredLangList && filterPreferredLangList.length == 2) {
                defaultLangGroup = filterPreferredLangList && filterPreferredLangList[1] && filterPreferredLangList[1].langGroupCd;

            }
        });
        return defaultLangGroup || (langGroupList && langGroupList[0] && langGroupList[0].langGroupCd);
    }

    getPersonalInfo = () => {
        const info = this.props.patientBaseInfo;
        const codeList = this.props.registerCodeList;
        const countryList = this.props.countryList;
        if (!info) return [];
        const docType = codeList && codeList.doc_type && codeList.doc_type.find(item => item.code === info.docTypeCd);
        const sex = codeList && codeList.gender && codeList.gender.find(item => item.code === info.genderCd);
        const counrty = countryList.find(item => item.countryCd === info.birthPlaceCd);
        const { languageData } = this.props;
        const langGroupList = languageData.codeLangGroupDtos;
        let defaultLangGroup = this.transformToGroupCd();
        const langTypeList = langGroupList && langGroupList.filter(item => item.langGroupCd == ((info.langGroup != 'E' && info.langGroup) || (defaultLangGroup) || (langGroupList && langGroupList[0].langGroupCd)));
        const preferredLangList = languageData.codePreferredLangMap && languageData.codePreferredLangMap[(info.langGroup != 'E' && info.langGroup) || (defaultLangGroup) || (langGroupList && langGroupList[0].langGroupCd)];
        const langList = preferredLangList && preferredLangList.filter(item => item.preferredLangCd ==
            ((info.preferredLangCd != 'E' && info.preferredLangCd)));
        return [
            { label: 'English Name:', value: `${info.engSurname} ${info.engGivename}` },
            { label: 'Chinese Name:', value: info.nameChi },
            { label: 'Doc. Type:', value: info.hkid ? 'HKID' : (docType && docType.engDesc) },
            { label: 'Doc. No.:', value: info.hkid ? info.hkid : info.otherDocNo },
            // { label: 'DOB(DMY):', value: moment(info.dob).format('DD-MMM-YYYY') },
            { label: 'DOB(DMY):', value: moment(info.dob).format(Enum.DATE_FORMAT_EDMY_VALUE) },
            { label: 'Sex:', value: sex && sex.engDesc },
            { label: 'Birth Place:', value: counrty ? counrty.countryName : info.birthPlaceCd },
            { label: 'Nationality:', value: info.nationality },
            { label: 'Translation Language:', value: langTypeList && langTypeList[0] && (langTypeList[0].engPreferredLang + (langTypeList[0].tchPreferredLang ?(' ' +  langTypeList[0].tchPreferredLang) : '')) },
            {
                label: 'Preferred Language:', value: (langList && langList[0] && (langList[0].engLangCategory + (langList[0].tcnLangCategory ? (' ' + langList[0].tcnLangCategory) : '') + (langList[0].remark ? (' (' + langList[0].remark + ')') : ''))) ||
                    (preferredLangList && preferredLangList[0] && (preferredLangList[0].engLangCategory + (preferredLangList[0].tcnLangCategory ? (' ' + preferredLangList[0].tcnLangCategory) : '') + (preferredLangList[0].remark ? (' (' + preferredLangList[0].remark + ')') : '')))
            },
            { label: 'Phone:', value: info.phone && `${info.phone.countryCd === FieldConstant.COUNTRY_CODE_DEFAULT_VALUE ? '' : '+' + info.phone.countryCd + ' '}${info.phone.phoneNo || ''}` }];
    }


    getContactPerson = () => {
        const info = this.props.contactPersonList;
        const codeList = this.props.registerCodeList;
        if (!info || info.length === 0) return [];
        const result = [];
        for (let i = 0; i < info.length; i++) {
            const relationship = codeList && codeList.relationship && codeList.relationship.find(item => item.code === info[i].relationshipCd);
            result.push({
                name: `${info[i].engSurname} ${info[i].engGivename}`,
                nameChi: info[i].nameChi,
                relationshipCd: relationship && relationship.engDesc,
                email: info[i].emailAddress
            });
        }
        return result;
    }

    getContactInfo = () => {
        const info = this.props.patientContactInfo;
        const phones = this.props.phoneList;
        if (!info) return [];
        const officePhone = phones && phones.find(item => item.phoneTypeCd === Enum.PHONE_TYPE_OFFICE_PHONE);
        const homePhone = phones && phones.find(item => item.phoneTypeCd === Enum.PHONE_TYPE_HOME_PHONE);
        const contactMean = Enum.CONTACT_MEAN_LIST.find(item => item.code === info.communicationMeansCd);
        const communicateLangCd = Enum.LANGUAGE_LIST.find(item => item.code === info.communicateLangCd);
        return [
            { label: 'Office Phone:', value: officePhone && officePhone.phoneNo && `${officePhone.countryCd === FieldConstant.COUNTRY_CODE_DEFAULT_VALUE ? '' : '+' + officePhone.countryCd + ' '}${officePhone.phoneNo || ''}` },
            { label: 'Home Phone:', value: homePhone && homePhone.phoneNo && `${homePhone.countryCd === FieldConstant.COUNTRY_CODE_DEFAULT_VALUE ? '' : '+' + homePhone.countryCd + ' '}${homePhone.phoneNo || ''}` },
            { label: 'Email:', value: info.emailAddress || '' },
            { label: 'Communication Means:', value: contactMean && contactMean.engDesc },
            { label: 'Communication Language:', value: communicateLangCd && communicateLangCd.engDesc },
            { label: 'PostalBox No.:', value: info.postOfficeBoxNo || '' },
            { label: 'PostalBox Name:', value: info.postOfficeName || '' },
            { label: 'PostalBox Region:', value: info.postOfficeRegion || '' }
        ];
    }

    getContactAddress = () => {
        const info = this.props.addressList;
        const codeList = this.props.registerCodeList;
        if (!info || info.length === 0) return [];
        const result = [];
        for (let i = 0; i < info.length; i++) {
            const district = codeList && codeList.district && codeList.district.find(item => item.code === info[i].districtCd);
            const subDistrict = codeList && codeList.sub_district && codeList.sub_district.find(item => item.code === info[i].subDistrictCd);
            const label = info[i].addressTypeCd === Enum.PATIENT_CORRESPONDENCE_ADDRESS_TYPE ? 'Correspondence Addr.:' : 'Residential Addr.:';
            result.push({
                label: label,
                value: `${info[i].room || ''} ${info[i].floor || ''} ${info[i].block || ''} ${info[i].building || ''} ${info[i].estate || ''} ${info[i].streetNo || ''} ${info[i].streetName || ''} ${(district && district.engDesc) || ''} ${(subDistrict && subDistrict.engDesc) || ''}`
            });
        }
        return result;
    }

    getSocialData = () => {
        const info = this.props.patientSocialData;
        const codeList = this.props.registerCodeList;
        if (!info || JSON.stringify(info) === JSON.stringify(patientSocialDataBasic)) return [];
        const ethnicity = codeList && codeList.ethnicity && codeList.ethnicity.find(item => item.code === info.ethnicityCd);
        const maritalStatus = codeList && codeList.marital_status && codeList.marital_status.find(item => item.code === info.maritalStatusCd);
        const religion = codeList && codeList.religion && codeList.religion.find(item => item.code === info.religionCd);
        const occupation = codeList && codeList.occupation && codeList.occupation.find(item => item.code === info.occupationCd);
        // const translationLang = codeList && codeList.translation_lang && codeList.translation_lang.find(item => item.code === info.translationLangCd);
        const eduLevel = codeList && codeList.edu_level && codeList.edu_level.find(item => item.code === info.eduLevelCd);
        const govDpt = codeList && codeList.gov_dpt && codeList.gov_dpt.find(item => item.code === info.govDptCd);
        return [
            { label: 'Ethnicity:', value: ethnicity && ethnicity.engDesc },
            { label: 'Marital Status:', value: maritalStatus && maritalStatus.engDesc },
            { label: 'Religion:', value: religion && religion.engDesc },
            { label: 'Occupation:', value: occupation && occupation.engDesc },
            // { label: 'Translation Language:', value: translationLang && translationLang.engDesc },
            { label: 'Education Level:', value: eduLevel && eduLevel.engDesc },
            { label: 'Gov. Department:', value: govDpt && govDpt.engDesc },
            { label: 'Rank:', value: info.rank },
            { label: 'Remarks:', value: info.remarks }
        ];
    }

    handleSkipToBooking = () => {
        const { patientById, patientInfo } = this.props;
        if (patientById && patientById.patientKey) {
            const patientKey = patientById.patientKey;
            if (patientInfo && patientInfo.patientKey !== patientKey) {
                this.props.openCommonMessage({ msgCode: '110136' });
                return false;
            }

            const caseActiveDtos = patientById.caseList && patientById.caseList.filter(item => item.statusCd === Enum.CASE_STATUS.ACTIVE);
            if (caseActiveDtos.length === 0) {
                this.props.openCaseNoDialog({
                    caseDialogStatus: Enum.CASE_DIALOG_STATUS.CREATE,
                    caseNoForm: { patientKey: patientKey },
                    caseCallBack: (data) => {
                        if (data && data.caseNo) {
                            this.props.getPatientPanelPatientById(patientKey, null, data.caseNo);
                            this.props.skipTab(accessRightEnum.booking, null, true);
                            this.props.deleteTabs(accessRightEnum.registration);
                        }
                    }
                });
            } else if (caseActiveDtos.length === 1) {
                this.props.getPatientPanelPatientById(patientKey, null, caseActiveDtos[0].caseNo);
                this.props.skipTab(accessRightEnum.booking, null, true);
                this.props.deleteTabs(accessRightEnum.registration);
            } else {
                let callBack = () => {
                    this.props.selectCaseTrigger({
                        trigger: true,
                        caseSelectCallBack: () => {
                            this.props.skipTab(accessRightEnum.booking, null, true);
                            this.props.deleteTabs(accessRightEnum.registration);
                        }
                    });
                };
                this.props.getPatientPanelPatientById(patientKey, null, null, callBack);
            }
        }
    }

    handleSkipToWalkIn = () => {
        const { patientById, patientInfo } = this.props;
        if (patientById && patientById.patientKey) {
            const patientKey = patientById.patientKey;
            if (patientInfo && patientInfo.patientKey !== patientKey) {
                this.props.openCommonMessage({ msgCode: '110136' });
                return false;
            }
            this.props.updateBookingField({ isWalkIn: true, showMakeAppointmentView: true });
            const caseActiveDtos = patientById.caseList && patientById.caseList.filter(item => item.statusCd === Enum.CASE_STATUS.ACTIVE);
            if (caseActiveDtos.length === 1) {
                this.props.getPatientPanelPatientById(patientKey, null, caseActiveDtos[0].caseNo);
                this.props.skipTab(accessRightEnum.booking, null, true);
                this.props.deleteTabs(accessRightEnum.registration);
            } else if (caseActiveDtos.length > 1) {
                let callBack = () => {
                    this.props.selectCaseTrigger({
                        trigger: true,
                        caseSelectCallBack: () => {
                            this.props.skipTab(accessRightEnum.booking, null, true);
                            this.props.deleteTabs(accessRightEnum.registration);
                        }
                    });
                };
                this.props.getPatientPanelPatientById(patientKey, null, null, callBack);
            } else {
                this.props.getPatientPanelPatientById(patientKey);
                this.props.skipTab(accessRightEnum.booking, null, true);
                this.props.deleteTabs(accessRightEnum.registration);
            }
        }
    }

    handleTableRowClick = (e, row, index) => {
        if (this.state.selectIndex.indexOf(index) > -1) {
            this.setState({ selectIndex: [], selectedCaseNo: '' });
        } else {
            this.setState({ selectIndex: [index], selectedCaseNo: row.caseNo });
        }
    }

    render() {
        const { classes, patientById , ecs, ecsResult, ocsss, ocsssResult, mwecs, mwecsResult,  openOcsssDialog, openMwecsDialog, openEcsDialog, checkOcsss} = this.props;
        const personalInfo = this.getPersonalInfo();
        const contactPerson = this.getContactPerson();
        const contactInfo = this.getContactInfo();
        const contactAddress = this.getContactAddress();
        const socialData = this.getSocialData();
        return (
            <Grid container className={classes.root}>

                {/* Personal Particulars */}
                <Grid item xs={6} className={classes.container}>
                    <Grid item container className={classes.borderTop}>
                        <Grid item container className={classes.headerTop}>
                            <CIMSInputLabel>Personal Particulars</CIMSInputLabel>
                            <Link
                                id={'registration_reviewInformation_personalInfo_btnEdit'}
                                component="button"
                                variant="body2"
                                onClick={e => this.handleOnEdit(e, 0)}
                            >Edit</Link>
                        </Grid>
                        <Grid item container>
                            {
                                personalInfo && personalInfo.map((item, index) => {
                                    return (
                                        <Grid item container className={classes.gridPadding} alignItems="baseline" xs={6} key={index}>
                                            <Grid item xs={5}><Typography variant="subtitle2">{item.label}</Typography></Grid>
                                            <Grid item xs={7}>
                                                <StyledFilledInput
                                                    id={`registration_reviewInformation_personalInfo_${index}`}
                                                    disabled
                                                    value={item.value || ''}
                                                />
                                            </Grid>
                                        </Grid>
                                    );
                                })
                            }
                        </Grid>
                    </Grid>
                </Grid>

                {/* Contact Person */}
                <Grid item xs={6} className={classes.container}>
                    <Grid item container className={classes.borderTop}>
                        <Grid item container className={classes.headerTop}>
                            <CIMSInputLabel>Contact Person</CIMSInputLabel>
                            <Link
                                id={'registration_reviewInformation_contactPerson_btnEdit'}
                                component="button"
                                variant="body2"
                                onClick={e => this.handleOnEdit(e, 2)}
                            >Edit</Link>
                        </Grid>
                        <Grid container>
                            {
                                contactPerson && contactPerson.length > 0 ?
                                    <>
                                        {
                                            contactPerson && contactPerson.map((item, index) => {
                                                return (
                                                    <Grid item container className={classes.contactPersonContainer} key={index}>
                                                        <Grid item container className={classes.gridPadding} xs={6} alignItems="baseline">
                                                            <Grid item xs={5}><Typography variant="subtitle2">Name</Typography></Grid>
                                                            <Grid item xs={7}>
                                                                <StyledFilledInput
                                                                    id={`registration_reviewInformation_contactPerson_name_${index}`}
                                                                    disabled
                                                                    value={item.name || ''}
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                        <Grid item container className={classes.gridPadding} xs={6} alignItems="baseline">
                                                            <Grid item xs={5}><Typography variant="subtitle2">Chinese Name</Typography></Grid>
                                                            <Grid item xs={7}>
                                                                <StyledFilledInput
                                                                    id={`registration_reviewInformation_contactPerson_nameChi_${index}`}
                                                                    disabled
                                                                    value={item.nameChi || ''}
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                        <Grid item container className={classes.gridPadding} xs={6} alignItems="baseline">
                                                            <Grid item xs={5}><Typography variant="subtitle2">Relationship</Typography></Grid>
                                                            <Grid item xs={7}>
                                                                <StyledFilledInput
                                                                    id={`registration_reviewInformation_contactPerson_relationShip_${index}`}
                                                                    disabled
                                                                    value={item.relationshipCd || ''}
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                        <Grid item container className={classes.gridPadding} xs={6} alignItems="baseline">
                                                            <Grid item xs={5}><Typography variant="subtitle2">Email</Typography></Grid>
                                                            <Grid item xs={7}>
                                                                <StyledFilledInput
                                                                    id={`registration_reviewInformation_contactPerson_email_${index}`}
                                                                    disabled
                                                                    value={item.email || ''}
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                );
                                            })
                                        }
                                    </> : <Grid item container justify="center" alignItems="center"><Typography variant="h3">BLANK</Typography></Grid>
                            }
                        </Grid>
                    </Grid>
                </Grid>

                {/* Contact Information */}
                <Grid item xs={6} className={classes.container}>
                    <Grid item container className={classes.borderTop}>
                        <Grid item container className={classes.headerTop}>
                            <CIMSInputLabel>Contact Information</CIMSInputLabel>
                            <Link
                                id={'registration_reviewInformation_contactInformation_btnEdit'}
                                component="button"
                                variant="body2"
                                onClick={e => this.handleOnEdit(e, 1)}
                            >Edit</Link>
                        </Grid>
                        <Grid item container>
                            {
                                contactInfo && contactInfo.map((item, index) => {
                                    return (
                                        <Grid item container className={classes.gridPadding} xs={6} alignItems="baseline" key={index}>
                                            <Grid item xs={5}><Typography variant="subtitle2">{item.label}</Typography></Grid>
                                            <Grid item xs={7} >
                                                <StyledFilledInput
                                                    id={`registration_reviewInformation_contactInformation_${index}`}
                                                    disabled
                                                    value={item.value || ''}
                                                />
                                            </Grid>
                                        </Grid>
                                    );
                                })
                            }
                            {
                                contactAddress && contactAddress.map((item, index) => {
                                    return (
                                        <Grid item container className={classes.gridPadding} alignItems="baseline" key={index}>
                                            <Grid item xs={2}><Typography variant="subtitle2">{item.label}</Typography></Grid>
                                            <Grid item xs={10}>
                                                <Grid item xs={1}></Grid>
                                                <Grid item xs={11} style={{ marginLeft: 35, marginRight: 40 }}>
                                                    <StyledFilledInput
                                                        id={`registration_reviewInformation_contactAddress_${index}`}
                                                        fullWidth
                                                        disabled
                                                        value={item.value || ''}
                                                    />
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    );
                                })
                            }
                        </Grid>
                    </Grid>
                </Grid>

                {/* Social Data */}
                <Grid item xs={6} className={classes.container}>
                    <Grid item container className={classes.borderTop}>
                        <Grid item container className={classes.headerTop}>
                            <CIMSInputLabel>Social Data</CIMSInputLabel>
                            <Link
                                id={'registration_reviewInformation_socialData_btnEdit'}
                                component="button"
                                variant="body2"
                                onClick={e => this.handleOnEdit(e, 3)}
                            >Edit</Link>
                        </Grid>
                        <Grid item container>
                            {
                                socialData && socialData.length > 0 ?
                                    <>
                                        {
                                            socialData && socialData.map((item, index) => {
                                                if (item.label === 'Remarks:') {
                                                    return (
                                                        <Grid item container className={classes.gridPadding} xs={12} alignItems="baseline" key={index}>
                                                            <Grid item xs={2}><Typography variant="subtitle2">{item.label}</Typography></Grid>
                                                            <Grid item xs={9} style={{ marginLeft: 35, maxWidth: '73%' }}>
                                                                <StyledFilledInput
                                                                    id={`registration_reviewInformation_socialData_${index}`}
                                                                    fullWidth
                                                                    disabled
                                                                    value={item.value || ''}
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                    );
                                                }
                                                return (
                                                    <Grid item container className={classes.gridPadding} xs={6} alignItems="baseline" key={index}>
                                                        <Grid item xs={5}><Typography variant="subtitle2">{item.label}</Typography></Grid>
                                                        <Grid item xs={7}>
                                                            <StyledFilledInput
                                                                id={`registration_reviewInformation_socialData_${index}`}
                                                                disabled
                                                                value={item.value || ''}
                                                            />
                                                        </Grid>
                                                    </Grid>
                                                );
                                            })
                                        }
                                    </> : <Grid item container justify="center" alignItems="center"><Typography variant="h3">BLANK</Typography></Grid>
                            }
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item xs={6}>
                <Box display="flex" width={1}  pt={1} >
                        <Box display="flex" width={1}>
                        <Box pr={1}>
                        <CIMSButton
                            disabled={!EcsUtilities.isMwecsEnable(
                                mwecs.accessRights,
                                mwecs.serviceStatus)}
                            style={{ padding: '0px', margin: '0px' }}
                            onClick={(e) => {
                                openMwecsDialog({
                                    idType: mwecs.idType,
                                    idNum: mwecs.idNum,
                                    patientKey: mwecs.patientKey
                                }, null, setMwecsPatientStatusInPatientSummary);
                            }}
                        >MWECS</CIMSButton>
                    </Box>
                    <Box pr={1} flexGrow={1}>
                        <MwecsResultTextField  mwecsStore={mwecsResult} fullWidth ></MwecsResultTextField>
                    </Box>
                    <Box pr={1} flexGrow={1}>
                        <MwecsMessageIdTextField  mwecsStore={mwecsResult} fullWidth ></MwecsMessageIdTextField>
                    </Box>
                        </Box>

                </Box>
                <Box display="flex" width={1}  pt={1}>
                    <Box display="flex" width={1} pr={1}>
                        <Box pr={1}>
                        <CIMSButton
                            disabled={!EcsUtilities.isEcsEnable(
                            ecs.accessRights,
                            ecs.docTypeCds,
                            ecs.ecsUserId,
                            ecs.ecsLocCode,
                            false,
                            ecs.ecsServiceStatus ,
                            ecs.hkicForEcs)}
                            style={{ padding: '0px', margin: '0px' }}
                            onClick={(e) => {
                                openEcsDialog({
                                    docTypeCd:ecs.docTypeCd,
                                    disableMajorKeys: true,
                                    engSurname: ecs.engSurname,
                                    engGivename: ecs.engGivename,
                                    chineseName: ecs.chineseName,
                                    cimsUser: ecs.ecsUserId,
                                    locationCode: ecs.ecsLocCode,
                                    patientKey: ecs.patientKey,
                                    hkid: ecs.hkicForEcs,
                                    dob: ecs.dob,
                                    exactDob:ecs.exactDobCd
                                },
                                null,
                                setEcsPatientStatusInPatientSummary);
                            }}
                        >ECS</CIMSButton>
                        </Box>
                        <Box  flexGrow={1}>
                            <EcsResultTextField  ecsStore={ecsResult} fullWidth ></EcsResultTextField>
                        </Box>
                    </Box>
                    <Box display="flex" width={1}>
                    <Box pr={1}>
                    <CIMSButton
                        style={{ padding: '0px', margin: '0px' }}
                        disabled={!EcsUtilities.isOcsssEnable(
                            ocsss.accessRights,
                            ocsss.docTypeCds,
                            ocsss.ocsssServiceStatus,
                            ocsss.hkicForEcs)
                            || ocsssResult.isValid
                        }
                        onClick={(e) => {
                            openOcsssDialog({
                                hkid: ocsss.hkicForEcs,
                                patientKey: ocsss.patientKey
                            },null, setOcsssPatientStatusInPatientSummary, checkOcsss);
                        }}
                    >OCSSS</CIMSButton>

                    </Box>
                    <Box  flexGrow={1} pr={1}>
                        <OcsssResultTextField  ocsssStore={ocsssResult} fullWidth ></OcsssResultTextField>
                    </Box>
                    </Box>

            </Box>

                </Grid>

                {/* Case no */}
                <Grid item xs={6} className={classes.container}>
                    <Grid item container className={classes.borderTop}>
                        <Grid item container className={classes.headerTop}>
                            <Box display="flex">
                                <CIMSInputLabel>Case</CIMSInputLabel>
                                <FormControlLabel
                                    id="registration_reviewInformation_showClosedLabel"
                                    value={this.state.isShowClosedCase}
                                    label="Show Closed Case"
                                    classes={{
                                        root: classes.formControlLabelRoot,
                                        label: classes.formControlLabelLabel
                                    }}
                                    onChange={e => this.setState({ isShowClosedCase: e.target.checked })}
                                    control={
                                        <CIMSCheckBox
                                            color="primary"
                                            id="registration_reviewInformation_showClosed"
                                            classes={{
                                                root: classes.checkBox
                                            }}
                                        />
                                    }
                                />
                            </Box>
                            <Grid item>
                                <Link
                                    id={'registration_reviewInformation_caseNo_createBtn'}
                                    component="button"
                                    variant="body2"
                                    onClick={() => {
                                        let params = {
                                            caseDialogStatus: Enum.CASE_DIALOG_STATUS.CREATE,
                                            caseNoForm: { patientKey: patientById.patientKey },
                                            caseCallBack: () => { this.props.getRegPatientById(patientById.patientKey); }
                                        };
                                        this.props.openCaseNoDialog(params);
                                    }}
                                >Create Case</Link>
                                <Typography component="span" variant="body2"> | </Typography>
                                <Link
                                    id={'registration_reviewInformation_caseNo_editBtn'}
                                    component="button"
                                    variant="body2"
                                    underline={!this.state.selectedCaseNo ? 'none' : 'hover'}
                                    style={{ color: !this.state.selectedCaseNo ? 'gray' : '#0579c8' }}
                                    onClick={() => {
                                        if (this.state.selectedCaseNo) {
                                            console.log(this.state.selectedCaseNo);
                                            let caseNoForm = _.cloneDeep(patientById.caseList.find(item => item.caseNo === this.state.selectedCaseNo));
                                            caseNoForm.patientKey = patientById.patientKey;
                                            caseNoForm.casePrefixCd = caseNoForm.caseNo.substr(0, 4);
                                            let params = {
                                                caseDialogStatus: Enum.CASE_DIALOG_STATUS.EDIT,
                                                caseNoForm: caseNoForm,
                                                caseCallBack: () => {
                                                    this.setState({ selectIndex: [], selectedCaseNo: '' });
                                                    this.props.getRegPatientById(patientById.patientKey);
                                                }
                                            };
                                            this.props.openCaseNoDialog(params);
                                        }
                                    }}
                                >Edit</Link>
                                <Typography component="span" variant="body2"> | </Typography>
                                <Link
                                    id={'registration_reviewInformation_caseNo_printBtn'}
                                    component="button"
                                    variant="body2"
                                    underline={!this.state.selectedCaseNo ? 'none' : 'hover'}
                                    style={{ color: !this.state.selectedCaseNo ? 'gray' : '#0579c8' }}
                                    onClick={() => { }}
                                >Print Label</Link>
                            </Grid>
                        </Grid>
                        <Grid item container>
                            <AutoScrollTable
                                id="registration_reviewInformation_scrollTable"
                                columns={[
                                    { label: 'Case Status', name: 'statusCd', width: '20%', customBodyRender: (value) => { return caseNoUtilities.getCaseNoStatus(value); } },
                                    { label: 'Case No.', name: 'caseNo', width: '30%', customBodyRender: (value) => { return caseNoUtilities.getFormatCaseNo(value); } },
                                    { label: 'Case Reference', name: 'caseReference', width: '50%' }
                                ]}
                                store={this.state.isShowClosedCase ? patientById.caseList : patientById.caseList.filter(item => item.statusCd === Enum.CASE_STATUS.ACTIVE)}
                                classes={{
                                    container: classes.tableContainer,
                                    tableRowRoot: classes.tableRowRoot
                                }}
                                selectIndex={this.state.selectIndex}
                                handleRowClick={this.handleTableRowClick}
                            />
                        </Grid>
                    </Grid>
                </Grid>

                <CIMSButtonGroup
                    buttonConfig={
                        [
                            {
                                id: 'RegisterationReviewInformationBookButton',
                                name: 'Booking',
                                onClick: this.handleSkipToBooking
                            },
                            {
                                id: 'RegisterationReviewInformationWalkInButton',
                                name: 'Walk-in',
                                onClick: this.handleSkipToWalkIn
                            }
                        ]
                    }
                    isLeft
                />

                <CIMSButtonGroup
                    buttonConfig={
                        [
                            {
                                id: 'RegisterationReviewInformationEditButton',
                                name: 'Edit',
                                onClick: (e) => this.handleOnEdit(e, 0)
                            },
                            {
                                id: 'RegisterationReviewInformationCancelButton',
                                name: 'Cancel',
                                onClick: this.handleClose
                            }
                        ]
                    }
                />
            </Grid>
        );
    }
}
const mapStateToProps = state => {
    return {
        registerCodeList: state.registration.codeList,
        patientBaseInfo: state.registration.patientBaseInfo,
        patientContactInfo: state.registration.patientContactInfo,
        patientSocialData: state.registration.patientSocialData,
        contactPersonList: state.registration.contactPersonList,
        addressList: state.registration.addressList,
        phoneList: state.registration.phoneList,
        countryList: state.patient.countryList || [],
        patientById: state.registration.patientById,
        patientInfo: state.patient.patientInfo,
        caseNoInfo: state.patient.caseNoInfo,
        isSaveSuccess: state.registration.isSaveSuccess,
        isAutoGen: state.caseNo.isAutoGen,
        languageData: state.patient.languageData,
        ecs: ecsSelector(state, summaryPatientInfoSelector ,summaryPatientKeySelector),
        ocsss: ocsssSelector(state, summaryPatientInfoSelector ,summaryPatientKeySelector),
        mwecs: mwecsSelector(state, summaryPatientInfoSelector ,summaryPatientKeySelector),
        ecsResult: state.ecs.patientSummaryEcsStatus,
        ocsssResult: state.ecs.patientSummaryOcsssStatus,
        mwecsResult: state.ecs.patientSummaryMwecsStatus
    };
};

const mapDispatchToProps = {
    updateState,
    openCommonCircular,
    closeCommonCircular,
    skipTab,
    resetAll,
    getPatientPanelPatientById,
    getRegPatientById,
    updateBookingField,
    openCommonMessage,
    openCaseNoDialog,
    updatePatient,
    selectCaseTrigger,
    openMwecsDialog,
    openEcsDialog,
    openOcsssDialog,
    checkOcsss,
    deleteTabs
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(useStyle)(ReviewInformation));