import React, { Component, useEffect } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import memoize from 'memoize-one';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { IconButton, Grid, Typography, FormControl, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, ExpansionPanelActions } from '@material-ui/core';
import { Add, ExpandMore } from '@material-ui/icons';

import CIMSButton from '../../components/Buttons/CIMSButton';
// import TextFieldValidator from '../../components/FormValidator/TextFieldValidator';
import SelectFieldValidator from '../../components/FormValidator/SelectFieldValidator';
import ConPhoneFiled from './component/conPhoneField';
import ValidatorEnum from '../../enums/validatorEnum';
import CommonMessage from '../../constants/commonMessage';
import RegFieldName from '../../enums/registration/regFieldName';
import Enum from '../../enums/enum';
import { contactPersonList, patientContactPhonesBasic } from '../../constants/registration/registrationConstants';
import RequiredIcon from '../../components/InputLabel/RequiredIcon';
import DelayInput from '../compontent/delayInput';

import { openCommonCircular, closeCommonCircular } from '../../store/actions/common/commonAction';
import { updateState } from '../../store/actions/registration/registrationAction';
import { openCommonMessage } from '../../store/actions/message/messageAction';

//eslint-disable-next-line
const useStyle3 = makeStyles(theme => ({
    form_input: {
        width: '100%'
    }
}));

const StyledFormControl = withStyles({
    root: {
        marginTop: 0
    }
})(FormControl);

function ContactPersonItem(props) {

    const addContactPerPhone = (type) => {
        let pcp = _.cloneDeep(patientContactPhonesBasic);
        pcp.phoneTypeCd = type;
        return pcp;
    };

    const initContactPerPhone = () => {
        let phoneList = [];
        phoneList.push(addContactPerPhone(Enum.PHONE_TYPE_MOBILE_PHONE));
        phoneList.push(addContactPerPhone(Enum.PHONE_TYPE_OTHER_PHONE));
        return phoneList;
    };

    useEffect(() => {
        //component did mount
        let contactPers = _.cloneDeep(props.contactPersonList);
        let contact = contactPers[props.index];
        let { contactPhoneList } = contact;
        if (!contactPhoneList) contactPhoneList = [];
        if (contactPhoneList.length <= 0) {
            let phoneList = initContactPerPhone();
            contactPhoneList.push(...phoneList);
            props.updateState({ contactPersonList: contactPers });
        }
        return () => {
            //component unmount
        };
        //eslint-disable-next-line
    }, []);

    const mobileFilter = memoize((list) => list && list.find(item => item.phoneTypeCd === Enum.PHONE_TYPE_MOBILE_PHONE));
    const otherFilter = memoize((list) => list && list.find(item => item.phoneTypeCd === Enum.PHONE_TYPE_OTHER_PHONE));

    const handleOnChange = (value, name) => {
        props.change(props.index, value, name);
    };

    const handleOnChangePhone = (name, value, type) => {
        let contactPers = _.cloneDeep(props.contactPersonList);
        let contact = contactPers[props.index];
        let contactPhoneList = contact.contactPhoneList;
        if (!contactPhoneList) contactPhoneList = [];
        let patiendConPhone = contactPhoneList.find(item => item.phoneTypeCd === type);
        if (!patiendConPhone) {
            let pcp = _.cloneDeep(patientContactPhonesBasic);
            pcp.phoneTypeCd = type;
            pcp[name] = value;
            contactPhoneList.push(pcp);
        } else {
            patiendConPhone[name] = value;
        }
        props.updateState({ contactPersonList: contactPers });
    };

    const classes = useStyle3();
    const { contact, comDisabled, registerCodeList, id,countryList} = props;
    const contact_Person_Mobile_Phone = mobileFilter(contact.contactPhoneList);
    const contact_Person_Other_Phone = otherFilter(contact.contactPhoneList);

    return (
        <Grid container spacing={1}>
            <Grid item xs={6}>
                <StyledFormControl fullWidth>
                    <DelayInput
                        id={id + '_surName'}
                        validByBlur
                        upperCase
                        onlyOneSpace
                        // labelText="Surname"
                        // isRequired
                        label={<>Surname<RequiredIcon /></>}
                        //InputLabelProps={{ shrink: true }}
                        variant="outlined"
                        msgPosition="bottom"
                        /* eslint-disable */
                        InputProps={{ className: classes.form_input }}
                        disabled={comDisabled}
                        inputProps={{ maxLength: 40 }}
                        /* eslint-enable */
                        value={contact.engSurname}
                        onChange={e => handleOnChange(e.target.value, RegFieldName.CONTACT_PERSON_ENGLISH_SURNAME)}
                        name={RegFieldName.CONTACT_PERSON_ENGLISH_SURNAME}
                        validators={[
                            ValidatorEnum.required,
                            ValidatorEnum.isSpecialEnglish
                        ]}
                        errorMessages={[
                            CommonMessage.VALIDATION_NOTE_REQUIRED(),
                            CommonMessage.VALIDATION_NOTE_SPECIAL_ENGLISH()
                        ]}
                        warning={[
                            ValidatorEnum.isEnglishWarningChar
                        ]}
                        warningMessages={[
                            CommonMessage.WARNING_NOTE_SPECIAL_ENGLISH()
                        ]}
                        trim={'all'}
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={6}>
                <StyledFormControl fullWidth>
                    <DelayInput
                        id={id + '_givenName'}
                        validByBlur
                        upperCase
                        onlyOneSpace
                        // labelText="Given Name"
                        // isRequired
                        label={<>Given Name<RequiredIcon /></>}
                        variant="outlined"
                        //InputLabelProps={{ shrink: true }}
                        msgPosition="bottom"
                        /* eslint-disable */
                        InputProps={{ className: classes.form_input }}
                        disabled={comDisabled}
                        inputProps={{ maxLength: 40 }}
                        /* eslint-enable */
                        value={contact.engGivename}
                        onChange={e => handleOnChange(e.target.value, RegFieldName.CONTACT_PERSON_ENGLISH_GIVENNAME)}
                        name={RegFieldName.CONTACT_PERSON_ENGLISH_GIVENNAME}
                        validators={[
                            ValidatorEnum.required,
                            ValidatorEnum.isSpecialEnglish
                        ]}
                        errorMessages={[
                            CommonMessage.VALIDATION_NOTE_REQUIRED(),
                            CommonMessage.VALIDATION_NOTE_SPECIAL_ENGLISH()
                        ]}
                        warning={[
                            ValidatorEnum.isEnglishWarningChar
                        ]}
                        warningMessages={[
                            CommonMessage.WARNING_NOTE_SPECIAL_ENGLISH()
                        ]}
                        trim={'all'}
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={6}>
                <StyledFormControl fullWidth>
                    <DelayInput
                        id={id + '_chineseName'}
                        // labelText="中文姓名"
                        // isRequired
                        label={<>中文姓名<RequiredIcon /></>}
                        variant="outlined"
                        //InputLabelProps={{ shrink: true }}
                        msgPosition="bottom"
                        /* eslint-disable */
                        InputProps={{ className: classes.form_input }}
                        disabled={comDisabled}
                        inputProps={{ maxLength: 20 }}
                        /* eslint-enable */
                        value={contact.nameChi}
                        onChange={e => handleOnChange(e.target.value, RegFieldName.CONTACT_PERSON_CHINESE_NAME)}
                        name={RegFieldName.CONTACT_PERSON_CHINESE_NAME}
                        validByBlur
                        validators={[ValidatorEnum.required, ValidatorEnum.isChinese]}
                        errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED(), CommonMessage.VALIDATION_NOTE_CHINESEFIELD()]}
                        trim={'all'}
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={6}>
                <StyledFormControl fullWidth>
                    <SelectFieldValidator
                        id={id + '_relationship'}
                        options={registerCodeList &&
                            registerCodeList.relationship &&
                            registerCodeList.relationship.map((item) => (
                                { value: item.code, label: item.engDesc }))}
                        value={contact.relationshipCd}
                        onChange={(e) => handleOnChange(e.value, RegFieldName.CONTACT_PERSON_RELATIONSHIP)}
                        isDisabled={comDisabled}
                        validByBlur
                        // isRequired
                        // labelText="Relationship"
                        TextFieldProps={{
                            className: classes.form_input,
                            variant: 'outlined',
                            label: <>Relationship<RequiredIcon /></>
                            //InputLabelProps: { shrink: true }
                        }}
                        msgPosition="bottom"
                        validators={[ValidatorEnum.required]}
                        errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={6}>
                <StyledFormControl fullWidth>
                    <ConPhoneFiled
                        id={id + '_mobilePhone'}
                        labelText="Mobile Phone"
                        comDisabled={comDisabled}
                        phone={contact_Person_Mobile_Phone}
                        countryOptions={countryList}
                        onChange={e => handleOnChangePhone(e.target.name, e.target.value, Enum.PHONE_TYPE_MOBILE_PHONE)}
                        onSelectChange={e => handleOnChangePhone(RegFieldName.CONTACT_PERSON_PHONE_COUNTRY, e.value, Enum.PHONE_TYPE_MOBILE_PHONE)}
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={6}>
                <StyledFormControl fullWidth>
                    <ConPhoneFiled
                        id={id + '_otherPhone'}
                        labelText="Other Phone"
                        comDisabled={comDisabled}
                        phone={contact_Person_Other_Phone}
                        countryOptions={countryList}
                        onChange={e => handleOnChangePhone(e.target.name, e.target.value, Enum.PHONE_TYPE_OTHER_PHONE)}
                        onSelectChange={e => handleOnChangePhone(RegFieldName.CONTACT_PERSON_PHONE_COUNTRY, e.value, Enum.PHONE_TYPE_OTHER_PHONE)}
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={4}>
                <StyledFormControl fullWidth>
                    <DelayInput
                        id={id + '_email'}
                        validByBlur
                        // labelText="Email"
                        // isRequired
                        label={<>Email<RequiredIcon /></>}
                        //InputLabelProps={{ shrink: true }}
                        variant="outlined"
                        msgPosition="bottom"
                        /* eslint-disable */
                        InputProps={{ className: classes.form_input }}
                        disabled={comDisabled}
                        inputProps={{ maxLength: 80 }}
                        /* eslint-enable */
                        value={contact.emailAddress}
                        onChange={e => handleOnChange(e.target.value, RegFieldName.CONTACT_PERSON_EMAIL)}
                        type="email"
                        name={RegFieldName.CONTACT_PERSON_EMAIL}
                        validators={[
                            ValidatorEnum.required,
                            ValidatorEnum.isEmail
                        ]}
                        errorMessages={[
                            CommonMessage.VALIDATION_NOTE_REQUIRED(),
                            CommonMessage.VALIDATION_NOTE_EMAILFIELD()
                        ]}
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={4}>
                <StyledFormControl fullWidth>
                    <DelayInput
                        id={id + '_roomFlat'}
                        // labelText="Room/Flat"
                        label="Room/Flat"
                        variant="outlined"
                        //InputLabelProps={{ shrink: true }}
                        /* eslint-disable */
                        InputProps={{ className: classes.form_input }}
                        disabled={comDisabled}
                        inputProps={{ maxLength: 65 }}
                        /* eslint-enable */
                        value={contact.room}
                        onChange={e => handleOnChange(e.target.value, RegFieldName.CONTACT_PERSON_ROOM)}
                        name={RegFieldName.CONTACT_PERSON_ROOM}
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={4}>
                <StyledFormControl fullWidth>
                    <DelayInput
                        id={id + '_floor'}
                        // labelText="Floor"
                        label="Floor"
                        variant="outlined"
                        //InputLabelProps={{ shrink: true }}
                        /* eslint-disable */
                        InputProps={{ className: classes.form_input }}
                        disabled={comDisabled}
                        inputProps={{ maxLength: 25 }}
                        /* eslint-enable */
                        value={contact.floor}
                        onChange={e => handleOnChange(e.target.value, RegFieldName.CONTACT_PERSON_FLOOR)}
                        name={RegFieldName.CONTACT_PERSON_FLOOR}
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={4}>
                <StyledFormControl fullWidth>
                    <DelayInput
                        id={id + '_block'}
                        // labelText="Block"
                        label="Block"
                        variant="outlined"
                        //InputLabelProps={{ shrink: true }}
                        /* eslint-disable */
                        InputProps={{ className: classes.form_input }}
                        disabled={comDisabled}
                        inputProps={{ maxLength: 50 }}
                        /* eslint-enable */
                        value={contact.block}
                        onChange={e => handleOnChange(e.target.value, RegFieldName.CONTACT_PERSON_BLOCK)}
                        name={RegFieldName.CONTACT_PERSON_BLOCK}
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={4}>
                <StyledFormControl fullWidth>
                    <DelayInput
                        id={id + '_building'}
                        // labelText="Building"
                        label="Building"
                        variant="outlined"
                        //InputLabelProps={{ shrink: true }}
                        /* eslint-disable */
                        InputProps={{ className: classes.form_input }}
                        disabled={comDisabled}
                        inputProps={{ maxLength: 415 }}
                        /* eslint-enable */
                        value={contact.building}
                        onChange={e => handleOnChange(e.target.value, RegFieldName.CONTACT_PERSON_BUILDING)}
                        name={RegFieldName.CONTACT_PERSON_BUILDING}
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={4}>
                <StyledFormControl fullWidth>
                    <DelayInput
                        id={id + '_estateLot'}
                        // labelText="Estate/Lot"
                        label="Estate/Lot"
                        variant="outlined"
                        //InputLabelProps={{ shrink: true }}
                        /* eslint-disable */
                        InputProps={{ className: classes.form_input }}
                        disabled={comDisabled}
                        inputProps={{ maxLength: 764 }}
                        /* eslint-enable */
                        value={contact.estate}
                        onChange={e => handleOnChange(e.target.value, RegFieldName.CONTACT_PERSON_ESTATELOT)}
                        name={RegFieldName.CONTACT_PERSON_ESTATELOT}
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={4}>
                <StyledFormControl fullWidth>
                    <DelayInput
                        id={id + '_streetNo'}
                        // labelText="Street No."
                        label="Street No."
                        variant="outlined"
                        //InputLabelProps={{ shrink: true }}
                        /* eslint-disable */
                        InputProps={{ className: classes.form_input }}
                        disabled={comDisabled}
                        inputProps={{ maxLength: 51 }}
                        /* eslint-enable */
                        value={contact.streetNo}
                        onChange={e => handleOnChange(e.target.value, RegFieldName.CONTACT_PERSON_STREET_NO)}
                        name={RegFieldName.CONTACT_PERSON_STREET_NO}
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={4}>
                <StyledFormControl fullWidth>
                    <DelayInput
                        id={id + '_streetRoad'}
                        // labelText="Street/Road."
                        label="Street/Road."
                        variant="outlined"
                        //InputLabelProps={{ shrink: true }}
                        /* eslint-disable */
                        InputProps={{ className: classes.form_input }}
                        disabled={comDisabled}
                        inputProps={{ maxLength: 384 }}
                        /* eslint-enable */
                        value={contact.streetName}
                        onChange={e => handleOnChange(e.target.value, RegFieldName.CONTACT_PERSON_STREET)}
                        name={RegFieldName.CONTACT_PERSON_STREET}
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={4}>
                <StyledFormControl fullWidth>
                    <DelayInput
                        id={id + '_region'}
                        // labelText="Region"
                        label="Region"
                        variant="outlined"
                        //InputLabelProps={{ shrink: true }}
                        /* eslint-disable */
                        InputProps={{ className: classes.form_input }}
                        disabled={comDisabled}
                        inputProps={{ maxLength: 50 }}
                        /* eslint-enable */
                        value={contact.region}
                        onChange={e => handleOnChange(e.target.value, RegFieldName.CONTACT_PERSON_REGION)}
                        name={RegFieldName.CONTACT_PERSON_REGION}
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={4}>
                <StyledFormControl fullWidth>
                    <SelectFieldValidator
                        id={id + '_district'}
                        options={registerCodeList &&
                            registerCodeList.district &&
                            registerCodeList.district.map((item) => (
                                { value: item.code, label: item.engDesc }))}
                        value={contact.districtCd}
                        onChange={(e) => handleOnChange(e.value, RegFieldName.CONTACT_PERSON_DISTRICT)}
                        isDisabled={comDisabled}
                        // labelText="District"
                        TextFieldProps={{
                            className: classes.form_input,
                            variant: 'outlined',
                            label: 'District'
                            //InputLabelProps: { shrink: true }
                        }}
                        addNullOption
                    />
                </StyledFormControl>
            </Grid>
            <Grid item xs={4}>
                <StyledFormControl fullWidth>
                    <SelectFieldValidator
                        id={id + '_subDistrict'}
                        options={registerCodeList &&
                            registerCodeList.sub_district &&
                            registerCodeList.sub_district.map((item) => (
                                { value: item.code, label: item.engDesc }))}
                        value={contact.subDistrictCd}
                        onChange={(e) => handleOnChange(e.value, RegFieldName.CONTACT_PERSON_SUB_DISTRICT)}
                        isDisabled={comDisabled}
                        // labelText="Sub District"
                        TextFieldProps={{
                            className: classes.form_input,
                            variant: 'outlined',
                            label: 'Sub District'
                            //InputLabelProps: { shrink: true }
                        }}
                        addNullOption
                    />
                </StyledFormControl>
            </Grid>
        </Grid>
    );
}

//eslint-disable-next-line
const styles = theme => ({
    root: {
        marginTop: 10
    },
    add_icon: {
        verticalAlign: 'bottom'
    },
    button: {
        borderRadius: '0%',
        width: '80%',
        padding: 0
    },
    expandButton: {
        padding: 0
    },
    expandDetail: {
        padding: '1px 24px'
    }
});

const StyledExpansionPanel = withStyles({
    root: {
        width: '80%',
        '&$expanded': {
            margin: '5px 0px'
        }
    },
    expanded: {}
})(ExpansionPanel);

const StyledExpansionPanelSummary = withStyles({
    root: {
        minHeight: 30,
        '&$expanded': {
            minHeight: 30
        }
    },
    expanded: {},
    content: {
        margin: 0,
        '&$expanded': {
            margin: 0
        }
    },
    expandIcon: {
        padding: 0
    }
})(ExpansionPanelSummary);

class ContactPerson extends Component {

    constructor(props) {
        super(props);
        this.state = {
            expanded: -1,
            contactIndex: -1
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextState !== this.state ||
            nextProps.comDisabled !== this.props.comDisabled ||
            nextProps.contactPersonList !== this.props.contactPersonList) {
            return true;
        } else {
            return false;
        }
    }

    addContact = () => {
        if (this.props.contactPersonList && this.props.contactPersonList.length === 0) {
            this.addContactPers();
        } else {
            this.props.openCommonCircular();
            const valid = this.props.checkContactValid(true);
            valid.then((result) => {
                if (result) {
                    this.addContactPers();
                } else {
                    this.openLastExpand();
                }
                this.props.closeCommonCircular();
            });
        }
    };

    addContactPers = () => {
        let contactPers = _.cloneDeep(this.props.contactPersonList);
        let contact = _.cloneDeep(contactPersonList);
        contact.displaySeq = contactPers.length;
        contactPers.push(contact);
        this.props.updateState({ contactPersonList: contactPers });
        this.setState({ expanded: contactPers.length - 1 });
    }

    changeContact = (index, value, name) => {
        let contactPers = _.cloneDeep(this.props.contactPersonList);
        contactPers[index][name] = value;
        this.props.updateState({ contactPersonList: contactPers });
    };

    deleteContactPer = () => {
        const index = this.state.contactIndex;
        let contactPers = _.cloneDeep(this.props.contactPersonList);
        contactPers.splice(index, 1);
        this.props.updateState({ contactPersonList: contactPers });
        this.setState({ expanded: this.state.expanded - 1 });
        // this.props.checkContactValid(false);
    };

    handleChangeExpand = (e, isExpanded, index) => {
        // let expanded = this.state.expanded;
        // if (expanded.indexOf(index) > -1) {
        //     expanded = expanded.replace(index, '');
        // } else {
        //     expanded = expanded + index;
        // }
        // this.setState({ expanded });
        if (this.props.comDisabled) {
            this.setState({ expanded: isExpanded ? index : false });
        } else {
            this.props.openCommonCircular();
            const valid = this.props.checkContactValid(true);
            valid.then((result) => {
                if (result) {
                    this.setState({ expanded: isExpanded ? index : false });
                }
                this.props.closeCommonCircular();
            });
        }
    }

    openLastExpand = () => {
        // let str = '';
        // for (let i = 0; i < this.props.contactPersonList.length; i++) {
        //     str = str + i;
        // }
        // this.setState({ expanded: str });
        this.setState({ expanded: this.props.contactPersonList.length - 1 });
    }

    openDialog = (e, index) => {
        this.props.openCommonMessage({
            msgCode: '110105',
            btnActions: {
                btn1Click: this.deleteContactPer
            }
        });
        this.setState({ contactIndex: index });
    }

    render() {
        const { classes, contactPersonList, comDisabled,countryList } = this.props;
        const id = 'registration_contactPerson';
        return (
            <Grid container justify="center" className={classes.root}>
                {
                    contactPersonList &&
                        contactPersonList.length > 0
                        ? contactPersonList.map((item, index) => (
                            <StyledExpansionPanel
                                key={index}
                                expanded={this.state.expanded === index ? true : false}
                                onChange={(...args) => this.handleChangeExpand(...args, index)}
                            >
                                <StyledExpansionPanelSummary
                                    id={id + '_personExpanSummary_' + index}
                                    expandIcon={<ExpandMore />}
                                >
                                    <Typography id={id + '_personTitle_' + index} variant="subtitle1">{index === 0 ? 'First Contact Person' : 'Other Contact Person'}</Typography>
                                </StyledExpansionPanelSummary>
                                <ExpansionPanelDetails className={classes.expandDetail}>
                                    <ContactPersonItem
                                        id={id + '_person' + index}
                                        key={index}
                                        index={index}
                                        contact={item}
                                        change={this.changeContact}
                                        updateState={e => this.props.updateState(e)}
                                        registerCodeList={this.props.registerCodeList}
                                        contactPersonList={this.props.contactPersonList}
                                        comDisabled={comDisabled}
                                        countryList={countryList}
                                    />
                                </ExpansionPanelDetails>
                                <ExpansionPanelActions>
                                    <CIMSButton
                                        id={id + '_personRemove' + index}
                                        size="small"
                                        disabled={comDisabled}
                                        onClick={e => this.openDialog(e, index)}
                                    >Remove</CIMSButton>
                                </ExpansionPanelActions>
                            </StyledExpansionPanel>))
                        : null}

                <IconButton
                    id={id + '_addContactPersonBtn'}
                    className={classes.button}
                    color="primary"
                    onClick={this.addContact}
                    disabled={comDisabled}
                >
                    <Add className={classes.add_icon} color={comDisabled ? 'disabled' : 'primary'} />
                    <b>ADD MORE CONTACT PERSON</b>
                </IconButton>
            </Grid>
        );
    }
}




const mapStateToProps = state => {
    return {
        registerCodeList: state.registration.codeList,
        contactPersonList: state.registration.contactPersonList,
        countryList: state.patient.countryList || []
    };
};

const dispatchProps = {
    updateState,
    openCommonCircular,
    closeCommonCircular,
    openCommonMessage
};

export default connect(mapStateToProps, dispatchProps)(withStyles(styles)(ContactPerson));