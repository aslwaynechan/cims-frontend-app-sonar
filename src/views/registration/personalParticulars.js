import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import _ from 'lodash';
import {
    Grid,
    FormControl,
    FormControlLabel,
    Box
} from '@material-ui/core';
import memoize from 'memoize-one';

import TextFieldValidator from '../../components/FormValidator/TextFieldValidator';
import SelectFieldValidator from '../../components/FormValidator/SelectFieldValidator';
import OutlinedRadioValidator from '../../components/FormValidator/OutlinedRadioValidator';
import ValidatorEnum from '../../enums/validatorEnum';
import CommonMessage from '../../constants/commonMessage';
import RegFieldName from '../../enums/registration/regFieldName';
import HKIDInput from '../compontent/hkidInput';
import RegChCodeField from './component/regChCodeField';
import RegDateBirthField from './component/regDateBirthField';
import NormalPhoneField from './component/normalPhoneField';
import ButtonStatusEnum from '../../enums/registration/buttonStatusEnum';
import RequiredIcon from '../../components/InputLabel/RequiredIcon';
import DelayInput from '../compontent/delayInput';
import * as CommonUtilities from '../../utilities/commonUtilities';
import CIMSCheckBox from '../../components/CheckBox/CIMSCheckBox';
import {
    updateState,
    searchPatient,
    findCharByCcCode,
    updateCcCode
} from '../../store/actions/registration/registrationAction';
import { openCommonCircular, closeCommonCircular } from '../../store/actions/common/commonAction';
import {
    checkOcsss,
    checkEcs,
    setEcsPatientStatus,
    resetEcsPatientStatus,
    resetOcsssPatientStatus,
    completeRegPageReset,
    openEcsDialog,
    openOcsssDialog,
    setEcsPatientStatusInRegPage,
    setOcsssPatientStatusInRegPage,
    regPageKeyFieldOnBlur
} from '../../store/actions/ECS/ecsAction';
import * as EcsActionType from '../../store/actions/ECS/ecsActionType';
import * as PatientUtil from '../../utilities/patientUtilities';
import {
    initSelectedPatientEcsStatus,
    initSelectedPatientOcsssStatus
} from '../../store/reducers/ECS/ecsReducer';
import EcsResultTextField from '../../components/ECS/Ecs/EcsResultTextField';
import OcsssResultTextField from '../../components/ECS/Ocsss/OcsssResultTextField';
import * as EcsUtilities from '../../utilities/ecsUtilities';
import { openCommonMessage } from '../../store/actions/message/messageAction';
import {ecsSelector, ocsssSelector, regPatientInfoSelector, regPatientKeySelector} from '../../store/selectors/ecsSelectors';
import CIMSButton from '../../components/Buttons/CIMSButton';

const sysRatio = CommonUtilities.getSystemRatio();
const unit = CommonUtilities.getResizeUnit(sysRatio);
//eslint-disable-next-line
const styles = (theme) => ({
    //...theme,
    //spacing: theme.spacing,
    root: {
        paddingTop: 10,
        width: '90%'
    },
    grid: {
        paddingTop: 4,
        paddingBottom: 4,
        //padding: 20,
        justifyContent: 'center'
    },
    form_input: {
        width: '100%'
    },
    radioGroup: {
        //height: 36
        height: 39 * unit
    },
    checkBoxRoot: {
        height: 'auto'
    }
});

class PersonalParticulars extends React.Component {

    static getDerivedStateFromProps(nextProps) {
        if (!nextProps.comDisabled) {
            let isHkidRequired = true, isDocNumRequired = true;
            if (nextProps.patientBaseInfo.hkid) {
                isHkidRequired = true;
                isDocNumRequired = false;
            } else if (!nextProps.patientBaseInfo.hkid && nextProps.patientBaseInfo.otherDocNo) {
                isHkidRequired = false;
                isDocNumRequired = true;
            }

            if (nextProps.patientBaseInfo.otherDocNo || nextProps.patientBaseInfo.docTypeCd) {
                isDocNumRequired = true;
            }
            return { isHkidRequired, isDocNumRequired };
        }
        return { isHkidRequired: true, isDocNumRequired: true };
    }

    state = {
        isHkidRequired: true,
        isDocNumRequired: true,
        ecsLocalStore: _.cloneDeep(initSelectedPatientEcsStatus),
        ocsssLocalStore: _.cloneDeep(initSelectedPatientOcsssStatus),
        patientBaseInfoSnapshot: {}
    }

    componentDidMount() {
        const hkidDom = document.getElementById('registration_personalParticulars_hkid');
        hkidDom && hkidDom.focus();
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state !== nextState ||
            nextProps.ecsResetFlag ||
            nextProps.patientBaseInfo !== this.props.patientBaseInfo ||
            nextProps.patientOperationStatus !== this.props.patientOperationStatus ||
            nextProps.registerCodeList !== this.props.registerCodeList) {
            return true;
        }
        return false;
    }

    filterDocTypeList = memoize((list) => {
        let codeList = list && list.doc_type;
        if (!codeList) return [];
        return codeList.filter(item => item.code !== 'ID');
    });

    chineseNameOnChange = (e) => {
        let patient = _.cloneDeep(this.props.patientBaseInfo);
        if (patient.nameChi === e.target.value) {
            return;
        }
        patient.nameChi = e.target.value;
        for (let i = 5; i >= 0; i--) {
            let name = 'ccCode' + (i + 1);
            patient[name] = '';
        }
        this.props.updateState({ patientBaseInfo: patient, ccCodeChiChar: [] });

    }
    handleOnChange = (value, name) => {
        let patient = _.cloneDeep(this.props.patientBaseInfo);
        patient[name] = value;
        this.props.updateState({ patientBaseInfo: patient });
        // handle preferredLanguage
        if (name === 'langGroup') {
            const preferredLangList = this.props.languageData && this.props.languageData.codePreferredLangMap && this.props.languageData.codePreferredLangMap[value];
            patient['preferredLangCd'] = preferredLangList[0] && preferredLangList[0].preferredLangCd;
            this.props.updateState({ patientBaseInfo: patient });
        }

    }

    handleOnChangeBirthPlace = (e, name) => {
        let patient = _.cloneDeep(this.props.patientBaseInfo);
        patient[name] = e.value;
        if (e.nationality) {
            patient['nationality'] = e.nationality;
        }
        this.props.updateState({ patientBaseInfo: patient });
    }

    handleOnChangePhone = (value, name) => {
        let patient = _.cloneDeep(this.props.patientBaseInfo);
        patient.phone[name] = value;
        this.props.updateState({ patientBaseInfo: patient });
    }

    handleOnDocTypeBlur = (e) => {
        this.handleEcsChange(e);
    }

    handleOnDocNoBlur = (e) => {
        this.handleEcsChange(e);
    }

    handleEcsChange = (e) => {
        this.props.regPageKeyFieldOnBlur();
    }

    handlePinBlur = (e) => {
        if (e.target.value && this.props.comDisabled) {
            // const searchString = e.target.value.replace('(', '').replace(')', '').replace(' ', '');
            const searchString = e.target.value;
            this.props.searchPatient(searchString);
        }
        this.handleEcsChange();
    }

    handleKeyDown = (e) => {
        if (e.keyCode === 13) {
            if (e.target.value && this.props.comDisabled) {
                this.hkid.blur();
                this.handleEcsChange();
            }
        }
    }

    getValidator = (name) => {
        let validators = [];
        const isHKIDFormat = PatientUtil.isHKIDFormat(this.props.patientBaseInfo.docTypeCd);
        if (name === RegFieldName.HKID) {
            if (this.state.isHkidRequired) {
                validators.push(ValidatorEnum.required);
            }
            if (!this.props.comDisabled) {
                validators.push('isHkid');
            }
            return validators;
        }
        if (name === RegFieldName.DOCUMENT_TYPE) {
            if (this.state.isDocNumRequired) {
                validators.push(ValidatorEnum.required);
            }
            return validators;
        }
        if (RegFieldName.DOCUMENT_NUMBER) {
            if (this.state.isDocNumRequired) {
                validators.push(ValidatorEnum.required);
            }
            if (isHKIDFormat) {
                validators.push(ValidatorEnum.isHkid);
            }
            return validators;
        }
    }

    getErrorMessage = (name) => {
        let errorMessages = [];
        const isHKIDFormat = PatientUtil.isHKIDFormat(this.props.patientBaseInfo.docTypeCd);
        if (name === RegFieldName.HKID) {
            if (this.state.isHkidRequired) {
                errorMessages.push(CommonMessage.VALIDATION_NOTE_REQUIRED());
            }
            if (!this.props.comDisabled) {
                errorMessages.push(CommonMessage.VALIDATION_NOTE_HKIC_FORMAT_ERROR());
            }
            return errorMessages;
        }
        if (name === RegFieldName.DOCUMENT_TYPE) {
            if (this.state.isDocNumRequired) {
                errorMessages.push(CommonMessage.VALIDATION_NOTE_REQUIRED());
            }
            return errorMessages;
        }
        if (RegFieldName.DOCUMENT_NUMBER) {
            if (this.state.isDocNumRequired) {
                errorMessages.push(CommonMessage.VALIDATION_NOTE_REQUIRED());
            }
            if (isHKIDFormat) {
                errorMessages.push(CommonMessage.VALIDATION_NOTE_HKIC_FORMAT_ERROR());
            }
            return errorMessages;
        }
    }

    resetChineseNameFieldValid = () => {
        if (this.chineseNameField) {
            this.chineseNameField.makeValid();
        }
    }

    transformToGroupCd = () => {
        let defaultLangGroup = '';
        const { patientBaseInfo, languageData } = this.props;
        const langGroupList = languageData.codeLangGroupDtos;
        patientBaseInfo.preferredLangCd && langGroupList.forEach((item, index) => {
            let filterPreferredLangList = languageData && languageData.codePreferredLangMap && languageData.codePreferredLangMap[item.langGroupCd] && languageData.codePreferredLangMap[item.langGroupCd].filter(itemObj => itemObj.preferredLangCd == patientBaseInfo.preferredLangCd);
            if (filterPreferredLangList && filterPreferredLangList.length == 1) {
                defaultLangGroup = filterPreferredLangList && filterPreferredLangList[0] && filterPreferredLangList[0].langGroupCd;
            } else if (filterPreferredLangList && filterPreferredLangList.length == 2) {
                defaultLangGroup = filterPreferredLangList && filterPreferredLangList[1] && filterPreferredLangList[1].langGroupCd;

            }
        });
        return defaultLangGroup;
    }


    render() {

        const {
            checkOcsss,
            classes,
            registerCodeList,
            patientBaseInfo,
            comDisabled,
            countryList,
            nationalityList,
            accessRights,
            patientOperationStatus,
            languageData,
            ecs,
            ocsss,
            openEcsDialog,
            openOcsssDialog,
            ecsResult,
            ocsssResult } = this.props;
        const { isHkidRequired, isDocNumRequired } = this.state;
        const docTypeList = this.filterDocTypeList(registerCodeList);
        const id = 'registration_personalParticulars';
        const hkicForEcs = EcsUtilities.getProperHkicForEcs(patientBaseInfo);
        const langGroupList = languageData.codeLangGroupDtos;
        let defaultLangGroup = this.transformToGroupCd();
        const preferredLangList = languageData.codePreferredLangMap && languageData.codePreferredLangMap[(patientBaseInfo.langGroup != 'E' && patientBaseInfo.langGroup) || defaultLangGroup || (langGroupList && langGroupList[0].langGroupCd)];
        console.log(patientOperationStatus);
        return (
            <Grid container justify="center">
                <Grid item container className={classes.root} spacing={2}>
                    <Grid item container xs={6} className={classes.grid}>
                        <FormControl className={classes.form_input}>
                            <HKIDInput
                                id={id + '_hkid'}
                                isHKID={!comDisabled}
                                variant="outlined"
                                msgPosition="bottom"
                                label={comDisabled ? <>PIN<RequiredIcon /></> : <>HKID{isHkidRequired ? <RequiredIcon /> : null}</>}
                                placeholder={comDisabled ? `Search by Case No/ ${CommonUtilities.getPatientCall()} ID/ HKID/ Document Number${CommonUtilities.getNameSearchCall()}` : ''}
                                name={RegFieldName.HKID}
                                value={patientBaseInfo.hkid}
                                validByBlur
                                validators={this.getValidator(RegFieldName.HKID)}
                                errorMessages={this.getErrorMessage(RegFieldName.HKID)}
                                onChange={e => this.handleOnChange(e.target.value, RegFieldName.HKID)}
                                onBlur={this.handlePinBlur}
                                onKeyDown={this.handleKeyDown}
                                disabled={this.props.patientOperationStatus === ButtonStatusEnum.DATA_SELECTED}
                                inputRef={ref => this.hkid = ref}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item container xs={6} className={classes.grid}>
                        <FormControl className={classes.form_input}>
                            {
                                patientBaseInfo.ehrPatientId ?
                                    <Grid item container alignContent="center">
                                        <Grid item xs={8}>
                                            <TextFieldValidator
                                                id={id + '_ehrNumber'}
                                                // labelText="eHR Number"
                                                label="eHR Number"
                                                // InputLabelProps={{ shrink: true }}
                                                variant="outlined"
                                                InputProps={{
                                                    readOnly: true
                                                }}
                                                value={patientBaseInfo.ehrPatientId}
                                            />
                                        </Grid>
                                        <Grid item container xs={4} justify="center">
                                            <FormControlLabel
                                                control={
                                                    <CIMSCheckBox
                                                        checked={patientBaseInfo.problemPmi}
                                                        onChange={e => this.handleOnChange(e.target.checked, 'problemPmi')}
                                                        value="problemPmi"
                                                        color="secondary"
                                                        // disabled={comDisabled}
                                                        disabled
                                                        className={classes.checkBoxRoot}
                                                    />
                                                }
                                                label="Problem PMI"
                                            />
                                        </Grid>
                                    </Grid>
                                    : null
                            }
                        </FormControl>
                    </Grid>
                    <Grid item container xs={6} className={classes.grid}>
                        <FormControl className={classes.form_input}>
                            <SelectFieldValidator
                                id={id + '_documentType'}
                                isDisabled={comDisabled}
                                TextFieldProps={{
                                    variant: 'outlined',
                                    label: <>Document Type{isDocNumRequired ? <RequiredIcon /> : null}</>
                                }}
                                options={
                                    docTypeList.map((item) => (
                                        { value: item.code, label: item.engDesc }))}
                                value={patientBaseInfo.docTypeCd}
                                msgPosition="bottom"
                                addNullOption
                                onBlur={this.handleOnDocTypeBlur}
                                validators={this.getValidator(RegFieldName.DOCUMENT_TYPE)}
                                errorMessages={this.getErrorMessage(RegFieldName.DOCUMENT_TYPE)}
                                onChange={e => this.handleOnChange(e.value, RegFieldName.DOCUMENT_TYPE)}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item container xs={6} className={classes.grid}>
                        <FormControl className={classes.form_input}>
                            <DelayInput
                                id={id + '_documentNumber'}
                                // isRequired={isDocNumRequired}
                                // labelText="Document No."
                                label={<>Document No.{isDocNumRequired ? <RequiredIcon /> : null}</>}
                                // InputLabelProps={{ shrink: true }}
                                variant="outlined"
                                upperCase
                                validByBlur
                                disabled={comDisabled}
                                // eslint-disable-next-line
                                inputProps={{ maxLength: 30 }}
                                name={RegFieldName.DOCUMENT_NUMBER}
                                value={patientBaseInfo.otherDocNo}
                                msgPosition="bottom"
                                onBlur={this.handleOnDocNoBlur}
                                validators={this.getValidator(RegFieldName.DOCUMENT_NUMBER)}
                                errorMessages={this.getErrorMessage(RegFieldName.DOCUMENT_NUMBER)}
                                onChange={e => this.handleOnChange(e.target.value, RegFieldName.DOCUMENT_NUMBER)}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item container xs={6} className={classes.grid}>
                        <FormControl className={classes.form_input}>
                            <TextFieldValidator
                                id={id + '_surName'}
                                name={RegFieldName.ENGLISH_SURNAME}
                                value={patientBaseInfo.engSurname}
                                validByBlur
                                upperCase
                                disabled={comDisabled}
                                // isRequired
                                // labelText="Surname"
                                variant="outlined"
                                label={<>Surname<RequiredIcon /></>}
                                // InputLabelProps={{ shrink: true }}
                                onlyOneSpace
                                // eslint-disable-next-line
                                inputProps={{ maxLength: 40 }}
                                msgPosition="bottom"
                                validators={[
                                    ValidatorEnum.required,
                                    ValidatorEnum.isSpecialEnglish
                                ]}
                                errorMessages={[
                                    CommonMessage.VALIDATION_NOTE_REQUIRED(),
                                    CommonMessage.VALIDATION_NOTE_SPECIAL_ENGLISH()
                                ]}
                                warning={[
                                    ValidatorEnum.isEnglishWarningChar
                                ]}
                                warningMessages={[
                                    CommonMessage.WARNING_NOTE_SPECIAL_ENGLISH()
                                ]}
                                onChange={e => this.handleOnChange(e.target.value, RegFieldName.ENGLISH_SURNAME)}
                                onBlur={e => this.handleOnChange(e.target.value, RegFieldName.ENGLISH_SURNAME)}
                                trim={'all'}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item container xs={6} className={classes.grid}>
                        <FormControl className={classes.form_input}>
                            <DelayInput
                                id={id + '_givenName'}
                                validByBlur
                                upperCase
                                // isRequired
                                // labelText="Given Name"
                                variant="outlined"
                                label={<>Given Name<RequiredIcon /></>}
                                // InputLabelProps={{ shrink: true }}
                                onlyOneSpace
                                disabled={comDisabled}
                                // eslint-disable-next-line
                                inputProps={{ maxLength: 40 }}
                                value={patientBaseInfo.engGivename}
                                name={RegFieldName.ENGLISH_GIVENNAME}
                                msgPosition="bottom"
                                validators={[
                                    ValidatorEnum.required,
                                    ValidatorEnum.isSpecialEnglish
                                ]}
                                errorMessages={[
                                    CommonMessage.VALIDATION_NOTE_REQUIRED(),
                                    CommonMessage.VALIDATION_NOTE_SPECIAL_ENGLISH()
                                ]}
                                warning={[
                                    ValidatorEnum.isEnglishWarningChar
                                ]}
                                warningMessages={[
                                    CommonMessage.WARNING_NOTE_SPECIAL_ENGLISH()
                                ]}
                                onChange={e => this.handleOnChange(e.target.value, RegFieldName.ENGLISH_GIVENNAME)}
                                trim={'all'}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item container xs={6} className={classes.grid}>
                        <FormControl className={classes.form_input}>
                            <RegChCodeField
                                id={id + '_chinaCode'}
                                resetChineseNameFieldValid={this.resetChineseNameFieldValid}
                                comDisabled={comDisabled}
                            // ccCode1={patientBaseInfo.ccCode1}
                            // ccCode2={patientBaseInfo.ccCode2}
                            // ccCode3={patientBaseInfo.ccCode3}
                            // ccCode4={patientBaseInfo.ccCode4}
                            // ccCode5={patientBaseInfo.ccCode5}
                            // ccCode6={patientBaseInfo.ccCode6}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item container xs={6} className={classes.grid}>
                        <FormControl className={classes.form_input}>
                            <DelayInput
                                id={id + '_otherName'}
                                validByBlur
                                upperCase
                                // labelText="Other Name"
                                variant="outlined"
                                label={<>Other Name</>}
                                // InputLabelProps={{ shrink: true }}
                                disabled={comDisabled}
                                // eslint-disable-next-line
                                inputProps={{ maxLength: 20 }}
                                value={patientBaseInfo.otherName}
                                validators={[
                                    ValidatorEnum.isSpecialEnglish
                                ]}
                                errorMessages={[
                                    CommonMessage.VALIDATION_NOTE_SPECIAL_ENGLISH()
                                ]}
                                warning={[
                                    ValidatorEnum.isEnglishWarningChar
                                ]}
                                warningMessages={[
                                    CommonMessage.WARNING_NOTE_SPECIAL_ENGLISH()
                                ]}
                                onChange={e => this.handleOnChange(e.target.value, RegFieldName.OTHER_NAME)}
                                name={RegFieldName.OTHER_NAME}
                                msgPosition="bottom"
                                trim={'all'}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item container xs={6} className={classes.grid}>
                        <FormControl className={classes.form_input}>
                            <TextFieldValidator
                                ref={e => this.chineseNameField = e}
                                id={id + '_chineseName'}
                                // labelText="中文姓名"
                                variant="outlined"
                                label="中文姓名"
                                // InputLabelProps={{ shrink: true }}
                                disabled={comDisabled}
                                validByBlur
                                // eslint-disable-next-line
                                inputProps={{ maxLength: 20 }}
                                value={patientBaseInfo.nameChi}
                                onChange={this.chineseNameOnChange}
                                name={RegFieldName.CHINESE_NAME}
                                msgPosition="bottom"
                                validators={[ValidatorEnum.isChinese]}
                                errorMessages={[CommonMessage.VALIDATION_NOTE_CHINESEFIELD()]}
                                trim={'all'}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item container xs={6} className={classes.grid}>
                        <FormControl className={classes.form_input}>
                            <SelectFieldValidator
                                id={id + '_birthPlace'}
                                options={countryList.map((item) => (
                                    { value: item.countryCd, label: item.countryName, nationality: item.nationality }))
                                }
                                value={patientBaseInfo.birthPlaceCd}
                                onChange={e => this.handleOnChangeBirthPlace(e, RegFieldName.BIRTH_PLACE)}
                                addNullOption
                                isDisabled={comDisabled}
                                // labelText="Birth Place"
                                TextFieldProps={{
                                    variant: 'outlined',
                                    label: 'Birth Place'
                                    // InputLabelProps: { shrink: true }
                                }}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item container xs={6} className={classes.grid}>
                        <FormControl className={classes.form_input}>
                            <RegDateBirthField
                                id={id + '_birthField'}
                                onChange={(value, name) => this.handleOnChange(value, name)}
                                dobValue={patientBaseInfo.dob}
                                exact_dobValue={patientBaseInfo.exactDobCd}
                                exact_dobList={registerCodeList ? registerCodeList.exact_dob : null}
                                comDisabled={comDisabled}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item container xs={6} className={classes.grid}>
                        <OutlinedRadioValidator
                            id={id + '_genderCd'}
                            name="Sex"
                            labelText="Sex"
                            isRequired
                            value={patientBaseInfo.genderCd}
                            onChange={e => this.handleOnChange(e.target.value, RegFieldName.GENDER)}
                            list={
                                registerCodeList &&
                                registerCodeList.gender &&
                                registerCodeList.gender.map(item => ({ label: item.engDesc, value: item.code }))
                            }
                            disabled={comDisabled}
                            validators={[ValidatorEnum.required]}
                            errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                            RadioGroupProps={{ className: classes.radioGroup }}
                        />
                    </Grid>
                    <Grid item container xs={6} className={classes.grid}>
                        <FormControl className={classes.form_input}>
                            <NormalPhoneField
                                id={id + '_basePhone'}
                                labelText="Phone"
                                isRequired
                                comDisabled={comDisabled}
                                phone={patientBaseInfo.phone}
                                countryOptions={countryList}
                                onChange={(value, name) => this.handleOnChangePhone(value, name)}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item container xs={6} className={classes.grid}>
                        <FormControl className={classes.form_input}>
                            <SelectFieldValidator
                                id={id + '_nationality'}
                                options={nationalityList.map((item) => (
                                    { value: item, label: item }))
                                }
                                value={patientBaseInfo.nationality}
                                addNullOption
                                onChange={e => this.handleOnChange(e.value, 'nationality')}
                                isDisabled={comDisabled}
                                TextFieldProps={{
                                    variant: 'outlined',
                                    label: 'Nationality'
                                }}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={6} className={classes.grid}>
                        <FormControl className={classes.form_input}>
                            <SelectFieldValidator
                                id={id + '_translationLanguage'}
                                options={
                                    langGroupList &&
                                    langGroupList.map((item) => (
                                        { value: item.langGroupCd, label: ((item.engPreferredLang ? item.engPreferredLang : '') + ' ') + (item.tchPreferredLang ? item.tchPreferredLang : '') }))}
                                value={(patientBaseInfo.langGroup != 'E' && patientBaseInfo.langGroup) || (defaultLangGroup) || (langGroupList && langGroupList[0].langGroupCd)}
                                onChange={(e) => this.handleOnChange(e.value, 'langGroup')}
                                validators={[ValidatorEnum.required]}
                                isDisabled={comDisabled}
                                TextFieldProps={{
                                    variant: 'outlined',
                                    label: <>Translation Language<RequiredIcon /></>

                                }}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item container xs={6} className={classes.grid}>
                        <FormControl className={classes.form_input}>
                            <SelectFieldValidator
                                id={id + '_preferredLanguage'}
                                options={preferredLangList && preferredLangList.map((item) => (
                                    { value: item.preferredLangCd, label: ((item.engLangCategory ? item.engLangCategory : '') + ' ') + (item.tcnLangCategory ? item.tcnLangCategory : '') + (item.remark ? ' (' + item.remark + ')' : '') }))
                                }
                                value={(patientBaseInfo.preferredLangCd != 'E' && patientBaseInfo.preferredLangCd) || (preferredLangList && preferredLangList[0] && preferredLangList[0].preferredLangCd)}
                                onChange={e => this.handleOnChange(e.value, 'preferredLangCd')}
                                validators={[ValidatorEnum.required]}
                                isDisabled={comDisabled}
                                TextFieldProps={{
                                    variant: 'outlined',
                                    label: <>Preferred Language<RequiredIcon /></>
                                }}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item container xs={6}>
                    <Box display="flex" width={1} >
                    <Box display="flex" width={1} pr={1}>
                        <Box pr={1}>
                        <CIMSButton
                            id={id + '_ecsBtn'}
                            disabled={!EcsUtilities.isEcsEnable(
                            accessRights,
                            ecs.docTypeCds,
                            ecs.ecsUserId,
                            ecs.ecsLocCode,
                            //patientOperationStatus === ButtonStatusEnum.SEARCH || patientOperationStatus === ButtonStatusEnum.VIEW,
                            true,
                            ecs.ecsServiceStatus ,
                            ecs.hkicForEcs)}
                            style={{ padding: '0px', margin: '0px' }}
                            onClick={(e) => {
                                openEcsDialog({
                                    docTypeCd:ecs.docTypeCd,
                                    disableMajorKeys: patientOperationStatus === ButtonStatusEnum.DATA_SELECTED || patientOperationStatus === ButtonStatusEnum.ADD || patientOperationStatus === ButtonStatusEnum.EDIT,
                                    engSurname: ecs.engSurname,
                                    engGivename: ecs.engGivename,
                                    chineseName: ecs.chineseName,
                                    cimsUser: ecs.ecsUserId,
                                    locationCode: ecs.ecsLocCode,
                                    patientKey: ecs.patientKey,
                                    hkid: ecs.hkicForEcs,
                                    dob: ecs.dob,
                                    exactDob:ecs.exactDobCd
                                },
                                null,
                                setEcsPatientStatusInRegPage);
                            }}
                        >ECS</CIMSButton>
                        </Box>
                        <Box  flexGrow={1}>
                            <EcsResultTextField id={id + '_ecsResultTxt'}  ecsStore={ecsResult} fullWidth ></EcsResultTextField>
                        </Box>
                    </Box>
                    <Box display="flex" width={1}>
                    <Box pr={1}>
                    <CIMSButton
                        id={id + '_ocsssBtn'}
                        style={{ padding: '0px', margin: '0px' }}
                        disabled={!EcsUtilities.isOcsssEnable(
                            accessRights,
                            ocsss.docTypeCds,
                            ocsss.ocsssServiceStatus,
                            ocsss.hkicForEcs) || patientOperationStatus === ButtonStatusEnum.SEARCH
                            || patientOperationStatus === ButtonStatusEnum.VIEW
                            || ocsssResult.isValid
                        }
                        onClick={(e) => {
                            openOcsssDialog({hkid:ocsss.hkicForEcs,
                                patientKey: ocsss.patientKey},null, setOcsssPatientStatusInRegPage, checkOcsss);
                        }}
                    >OCSSS</CIMSButton>

                    </Box>
                    <Box  flexGrow={1}>
                        <OcsssResultTextField id={id + '_ocsssResultTxt'} ocsssStore={ocsssResult} fullWidth ></OcsssResultTextField>
                    </Box>
                    </Box>

            </Box>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = state => {
    return {
        loginInfo: state.login.loginInfo,
        clinicInfo: state.login.clinic,
        accessRights: state.login.accessRights,
        patientOperationStatus: state.registration.patientOperationStatus,
        registerCodeList: state.registration.codeList,
        nationalityList: state.patient.nationalityList || [],
        countryList: state.patient.countryList || [],
        patientBaseInfo: state.registration.patientBaseInfo,
        languageData: state.patient.languageData,
        patientById: state.registration.patientById,
        ecs: ecsSelector(state, regPatientInfoSelector ,regPatientKeySelector),
        ocsss: ocsssSelector(state, regPatientInfoSelector ,regPatientKeySelector),
        ecsResetFlag: state.ecs.shouldRegPageClearResult,
        ocsssServiceStatus: state.ecs.ocsssServiceStatus,
        ecsResult: state.ecs.regSummaryEcsStatus,
        ocsssResult: state.ecs.regSummaryOcsssStatus
    };
};

const mapDispatchToProps = {
    updateState,
    searchPatient,
    findCharByCcCode,
    updateCcCode,
    openCommonCircular,
    closeCommonCircular,
    checkEcs,
    checkOcsss,
    setEcsPatientStatus,
    resetEcsPatientStatus,
    resetOcsssPatientStatus,
    openCommonMessage,
    completeRegPageReset,
    openEcsDialog,
    openOcsssDialog,
    regPageKeyFieldOnBlur
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(PersonalParticulars));