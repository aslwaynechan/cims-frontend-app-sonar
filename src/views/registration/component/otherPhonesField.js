import React from 'react';
import {
    Grid,
    IconButton
} from '@material-ui/core';
import { RemoveCircle, AddCircle } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
import _ from 'lodash';
import { patientPhonesBasic } from '../../../constants/registration/registrationConstants';
import NormalPhoneField from './normalPhoneField';
import Enum from '../../../enums/enum';

class OtherPhonesField extends React.Component {

    handleDelete = (e, data) => {
        let otherPhones = _.cloneDeep(this.props.otherPhones);
        let deletePhoneIndex = otherPhones.findIndex(item => item.phoneId === data.phoneId && item.phoneTypeCd === data.phoneTypeCd
            && item.phoneNo === data.phoneNo && item.countryCd === data.countryCd && item.phonePriority === 0);
        otherPhones.splice(deletePhoneIndex, 1);
        this.props.onChange(otherPhones);
    }

    handleAdd = () => {
        let otherPhones = _.cloneDeep(this.props.otherPhones);
        let basicPhone = _.cloneDeep(patientPhonesBasic);
        basicPhone.phoneTypeCd = Enum.PHONE_TYPE_OTHER_PHONE;
        otherPhones.push(basicPhone);
        this.props.onChange(otherPhones);
    }

    handleOnChange = (value, name, index) => {
        let otherPhones = _.cloneDeep(this.props.otherPhones);
        otherPhones[index][name] = value;
        this.props.onChange(otherPhones);
    }

    render() {
        const {
            classes,
            phoneCountryList,
            comDisabled,
            otherPhones,
            atLeastOne
        } = this.props;
        return (
            <Grid container>
                {
                    otherPhones && otherPhones.map((item, index) => {
                        return (
                            <Grid item container wrap="nowrap" key={index}>
                                <Grid item container xs={10} className={classes.paddingGrid}>
                                    <NormalPhoneField
                                        id="registration_contactInformation_otherPhone"
                                        labelText="Phone"
                                        comDisabled={comDisabled}
                                        phone={item}
                                        isRequired={atLeastOne && index === 0}
                                        countryOptions={phoneCountryList}
                                        onChange={(value, name) => this.handleOnChange(value, name, index)}
                                    />
                                </Grid>
                                <Grid item container xs={2} justify="flex-start" wrap="nowrap">
                                    {
                                        otherPhones.length - 1 > 0 ?
                                            <IconButton
                                                onClick={e => this.handleDelete(e, item)}
                                                disabled={comDisabled}
                                                id={`registration_contactInformation_removeBtn_${index}`}
                                                className={classes.iconButton}
                                                title="Remove"
                                            >
                                                <RemoveCircle />
                                            </IconButton> : null
                                    }
                                    {
                                        index === otherPhones.length - 1 && otherPhones.length < 3 ?
                                            <IconButton
                                                onClick={this.handleAdd}
                                                disabled={comDisabled}
                                                id={`registration_contactInformation_addBtn_${index}`}
                                                className={classes.iconButton}
                                                title="Add"
                                                color="primary"
                                            >
                                                <AddCircle />
                                            </IconButton> : null
                                    }
                                </Grid>
                            </Grid>
                        );
                    })
                }
            </Grid>
        );
    }
}

const style = () => ({
    paddingGrid: {
        paddingBottom: 10
    },
    iconButton: {
        padding: 0,
        borderRadius: '15%',
        width: 40,
        paddingBottom: 10
    }
});

export default withStyles(style)(OtherPhonesField);