import React from 'react';
import SelectFieldValidator from '../../../components/FormValidator/SelectFieldValidator';
import DelayInput from '../../compontent/delayInput';
import FieldConstant from '../../../constants/fieldConstant';
import CommonMessage from '../../../constants/commonMessage';
import RegFieldName from '../../../enums/registration/regFieldName';
import RegFieldLength from '../../../enums/registration/regFieldLength';
import {
    FormControl,
    FormHelperText,
    Grid
} from '@material-ui/core';
// import FormInputLabel from '../../compontent/label/formInputLabel';
import ValidatorEnum from '../../../enums/validatorEnum';

class ConPhoneFiled extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            countryCd_Valid: true,
            phoneNo_Valid: true,
            errorMessage: {
                countryCd_errMsg: '',
                phoneNo_errMsg: ''
            },
            phoneMaxLength: RegFieldLength.CONTACT_PHONE_DEFAULT_MAX
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.state !== nextState ||
            this.props.phone !== nextProps.phone ||
            this.props.labelText !== nextProps.labelText ||
            this.props.comDisabled !== nextProps.comDisabled ||
            this.props.countryOptions !== nextProps.countryOptions;
    }

    validatorListener = (isvalid, errMsg, name) => {
        let { errorMessage } = this.state;
        if (!isvalid && errMsg) {
            errorMessage[name + '_errMsg'] = errMsg;
        } else {
            errorMessage[name + '_errMsg'] = '';
        }
        this.setState({ [name + '_Valid']: isvalid, errorMessage });
    }

    handleOnChange = (e) => {
        if (this.props.onChange) {
            this.props.onChange(e);
        }
    }

    handleOnChangeSelect = (e) => {
        if (e.value !== FieldConstant.COUNTRY_CODE_DEFAULT_VALUE) {
            this.setState({ phoneMaxLength: RegFieldLength.CONTACT_PHONE_OTHERS_MAX });
        } else {
            this.setState({ phoneMaxLength: RegFieldLength.CONTACT_PHONE_DEFAULT_MAX });
        }
        if (this.props.onSelectChange) {
            this.props.onSelectChange(e);
        }
    }

    render() {
        const { comDisabled, phone, countryOptions, labelText, id } = this.props;
        const { errorMessage } = this.state;
        let errorMsg = errorMessage.countryCd_errMsg || errorMessage.phoneNo_errMsg;
        let validators = [ValidatorEnum.isNumber, ValidatorEnum.phoneNo], errMsgs = [CommonMessage.VALIDATION_NOTE_NUMBERFIELD(), CommonMessage.VALIDATION_NOTE_PHONE_NO()];
        if (phone && phone.countryCd === FieldConstant.COUNTRY_CODE_DEFAULT_VALUE) {
            validators = validators.concat([ValidatorEnum.minStringLength(RegFieldLength.CONTACT_PHONE_DEFAULT_MAX), ValidatorEnum.maxStringLength(RegFieldLength.CONTACT_PHONE_DEFAULT_MAX)]);
            errMsgs = errMsgs.concat([CommonMessage.VALIDATION_NOTE_PHONE_BELOWMINWIDTH(), CommonMessage.VALIDATION_NOTE_OVERMAXWIDTH()]);
        }
        return (
            <FormControl fullWidth style={{ display: 'flex', flexDirection: 'column' }}>
                {/* <Grid container item alignItems="baseline" spacing={1}>
                    <Grid item>
                        <FormInputLabel>{labelText}</FormInputLabel>
                    </Grid>
                    <Grid item>
                        <FormHelperText style={{ marginTop: 0 }} error id={id + '_errorMsg'}>
                            {
                                this.state.country_Valid && this.state.phone_Valid ? '' : errorMsg
                            }
                        </FormHelperText>
                    </Grid>
                </Grid> */}
                <Grid container spacing={1}>
                    <Grid item xs={5}>
                        <SelectFieldValidator
                            id={id + '_country'}
                            options={countryOptions && countryOptions.map((item) => (
                                { value: item.countryCd, label: `${item.countryName || ''}(${item.dialingCd})` }
                            ))}
                            isDisabled={this.props.comDisabled}
                            value={phone ? phone.countryCd : FieldConstant.COUNTRY_CODE_DEFAULT_VALUE}
                            onChange={this.handleOnChangeSelect}
                            validators={phone && phone.phoneNo ? [ValidatorEnum.required] : null}
                            errorMessages={phone && phone.phoneNo ? [CommonMessage.VALIDATION_NOTE_REQUIRED()] : null}
                            // validatorListener={(...args) => this.validatorListener(...args, 'countryCd')}
                            // notShowMsg
                            msgPosition="bottom"
                            TextFieldProps={{
                                variant: 'outlined',
                                label: 'Country Code'
                                //InputLabelProps: { shrink: true }
                            }}
                        />
                    </Grid>
                    <Grid item xs={7}>
                        <DelayInput
                            id={id + '_phoneNo'}
                            name={RegFieldName.CONTACT_PHONE_NO}
                            inputProps={{
                                maxLength: this.state.phoneMaxLength
                            }}
                            type="number"
                            disabled={comDisabled}
                            value={phone ? phone.phoneNo : ''}
                            onChange={this.handleOnChange}
                            validByBlur
                            validators={validators}
                            errorMessages={errMsgs}
                            // notShowMsg
                            msgPosition="bottom"
                            // validatorListener={(...args) => this.validatorListener(...args, 'phoneNo')}
                            label={labelText}
                            // InputLabelProps={{ shrink: true }}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
                {
                    this.state.countryCd_Valid && this.state.phoneNo_Valid ?
                        null :
                        <Grid item>
                            <FormHelperText style={{ marginTop: 0 }} error id={id + '_errorMsg'}>
                                {errorMsg}
                            </FormHelperText>
                        </Grid>
                }
            </FormControl>
        );
    }

}

export default ConPhoneFiled;