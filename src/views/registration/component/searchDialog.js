import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DialogContent, DialogActions, Typography } from '@material-ui/core';

import CIMSTable from '../../../components/Table/CIMSTable';
import CIMSDialog from '../../../components/Dialog/CIMSDialog';
import CIMSButton from '../../../components/Buttons/CIMSButton';
import * as CommonUtilities from '../../../utilities/commonUtilities';
import * as PatientUtil from '../../../utilities/patientUtilities';

import moment from 'moment';

class SearchDialog extends Component {

    state = {
        rows: [
            {
                name: 'hkidOrDoc', label: 'HKID/Doc No', width: 120,
                customBodyRender: (value, rowData) => PatientUtil.getHkidOrDocNo(rowData)
            },
            {
                name: 'engName', label: 'English Name', width: 120,
                customBodyRender: (value, rowData) => `${rowData.engSurname} ${rowData.engGivename}`
            },
            { name: 'nameChi', label: 'Chinese Name', width: 120 },
            {
                name: 'dob', label: 'DOB', width: 120,
                customBodyRender: (value) => value ? moment(value).format('DD MMM YYYY') : ''
            },
            { name: 'phoneNo', label: 'Phone', width: 120 },
            {
                name: 'action', label: '', align: 'center', width: 120,
                customBodyRender: (value, rowData) => {
                    return (
                        <CIMSButton
                            id={'registration_dialogview_' + rowData.patientKey}
                            style={{ margin: 0 }}
                            onClick={() => this.props.clickView(rowData.patientKey)}
                        >View</CIMSButton>
                    );
                }
            }
        ],
        options: {
            rowExpand: true,
            rowsPerPage: 10,
            rowsPerPageOptions: [10, 15, 20]
        }
    }

    handleOnChangePage = (event, newPage, rowPerPage) => {
        if (this.props.searchPatient && this.props.searchString) {
            this.props.searchPatient(this.props.searchString, newPage + 1, rowPerPage);
        }
    }

    handleOnChangeRowPerPage = (event, page, newRowPerPage) => {
        if (this.props.searchPatient && this.props.searchString) {
            this.props.searchPatient(this.props.searchString, page + 1, newRowPerPage);
        }
    }

    render() {
        return (
            <CIMSDialog
                id="registration_searchDialog"
                open={this.props.open}
                dialogTitle="Registration"
            >
                <DialogContent id="registration_searchDialogContent">
                    <Typography variant="subtitle1">Please select {CommonUtilities.getPatientCall()} below:</Typography>
                    <CIMSTable
                        rows={this.state.rows}
                        data={this.props.data && this.props.data.patientSearchDtos}
                        options={this.state.options}
                        remote
                        totalCount={this.props.data ? this.props.data.total : 0}
                        onChangePage={this.handleOnChangePage}
                        onChangeRowsPerPage={this.handleOnChangeRowPerPage}
                    />
                </DialogContent>
                <DialogActions>
                    <CIMSButton
                        id="registration_btnCloseSearchDialog"
                        onClick={() => this.props.closeDialog()}
                    >Close</CIMSButton>
                </DialogActions>
            </CIMSDialog>
        );
    }
}

SearchDialog.propTypes = {
    closeDialog: PropTypes.func.isRequired,
    clickView: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    data: PropTypes.any.isRequired
};

export default SearchDialog;