import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { DialogContent, DialogActions, InputAdornment, Grid, IconButton } from '@material-ui/core';
import { AccountBox, Lock, Visibility, VisibilityOff } from '@material-ui/icons';
import CIMSFormLabel from '../../../components/InputLabel/CIMSFormLabel';
import CIMSButton from '../../../components/Buttons/CIMSButton';
import TextFieldValidator from '../../../components/FormValidator/TextFieldValidator';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import ValidatorEnum from '../../../enums/validatorEnum';
import CommonMessage from '../../../constants/commonMessage';

import moment from 'moment';
import NewAprrovalDialog from './newAprrovalDialog';
import Enum from '../../../enums/enum';
import accessRightEnum from '../../../enums/accessRightEnum';

const styles = () => ({
    gridMargin20: {
        marginBottom: 20
    },
    textfield: {
        marginBottom: 20,
        marginTop: 20
    }
});

const useStyle = makeStyles(theme => ({
    informationBar: {
        height: 40
    },
    complexBar: {
        // backgroundColor: `${theme.palette.primary.main}55`
        backgroundColor: '#DAE3F3'
    },
    grid: {
        paddingLeft: 8,
        paddingRight: 8
    },
    headGrid: {
        textAlign: 'right',
        fontWeight: 'bold',
        paddingRight: 30
    },
    boldGrid: {
        fontWeight: 'bold'
    },
    differentGrid: {
        // color: theme.palette.secondary.main
        color: '#FD0000'
    }
}));

function InformationBar(props) {
    const classes = useStyle();
    return (
        <Grid
            container
            className={classNames({
                [classes.informationBar]: true,
                [classes.complexBar]: props.complex
            })}
            alignContent="center"
        >
            <Grid item xs={4}
                className={classNames({
                    [classes.headGrid]: true,
                    [classes.grid]: true
                })}
            >{props.title}</Grid>
            <Grid item xs={4}
                className={classNames({
                    [classes.grid]: true,
                    [classes.boldGrid]: props.head
                })}
            >{props.content1}</Grid>
            <Grid item xs={4}
                className={classNames({
                    [classes.grid]: true,
                    [classes.boldGrid]: props.head,
                    [classes.differentGrid]: props.head == true ? false : (props.content1 !== props.content2)
                })}
            >{props.content2}</Grid>
        </Grid>
    );
}

class ApprovalDialog extends Component {
    constructor(props){
        super(props);
    }
    state = {
        showPassword: false
    }

    handleMouseDownPassword = () => {
        this.setState({ showPassword: true });
    }

    handleMouseUpPassword = () => {
        this.setState({ showPassword: false });
    }

    handleMouseLeavePassword = () => {
        this.setState({ showPassword: false });
    }

    handleConfirm = () => {
        if (this.props.isUserHaveAccess) {
            this.props.confirm();
        } else {
            const formvalid = this.refs.approvalForm.isFormValid(false);
            formvalid.then(result => {
                if (result) {
                    this.props.confirm();
                }
            });
        }
    }

    render() {
        const {
            classes,
            open,
            cancel,
            approverName,
            approverPassword,
            existingInfo,
            newInfo,
            onChange,
            isUserHaveAccess
        } = this.props;
        return (
            <NewAprrovalDialog
                id="registration_approvalDialog"
                open={open}
                dialogTitle="Supervisor Confirm"
            >
                <DialogContent id="registration_approvalDialogContent" style={{ maxHeight: 555 }}>
                    <Grid container className={classes.gridMargin20}>
                        <InformationBar
                            head
                            title=""
                            content1="Existing Information"
                            content2="New Information"
                        />
                        <InformationBar
                            complex
                            title="HKID"
                            content1={existingInfo.hkid}
                            content2={newInfo.hkid}
                        />
                        <InformationBar
                            title="English Name"
                            content1={`${existingInfo.engSurname}, ${existingInfo.engGivename}`}
                            content2={`${newInfo.engSurname}, ${newInfo.engGivename}`}
                        />
                        <InformationBar
                            complex
                            title="Chinese Name"
                            content1={existingInfo.nameChi}
                            content2={newInfo.nameChi}
                        />
                        <InformationBar
                            title="Sex"
                            content1={existingInfo.genderCd}
                            content2={newInfo.genderCd}
                        />
                        <InformationBar
                            complex
                            title="DOB"
                            content1={existingInfo.dob && moment(existingInfo.dob).format(Enum.DATE_FORMAT_EDMY_VALUE)}
                            content2={newInfo.dob && moment(newInfo.dob).format(Enum.DATE_FORMAT_EDMY_VALUE)}
                        />
                    </Grid>
                    {
                        isUserHaveAccess ?
                            null :
                            <Grid container>
                                <CIMSFormLabel
                                    fullWidth
                                    labelText="Approver"
                                >
                                    <ValidatorForm ref="approvalForm">
                                        <Grid item container alignItems="center" className={classes.textfield}>
                                            <Grid item container justify="center" xs={1}><AccountBox /></Grid>
                                            <Grid item xs={11}>
                                                <TextFieldValidator
                                                    fullWidth
                                                    upperCase
                                                    onChange={e => onChange(e.target.value, 'loginName')}
                                                    value={approverName}
                                                    name={'approverName'}
                                                    id={'registration_approverName'}
                                                    label="Login ID"
                                                    autoFocus
                                                    msgPosition="bottom"
                                                    validators={[ValidatorEnum.required]}
                                                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                />
                                            </Grid>
                                        </Grid>
                                        <Grid item container alignItems="center" className={classes.textfield}>
                                            <Grid item container justify="center" xs={1}><Lock /></Grid>
                                            <Grid item xs={11}>
                                                <TextFieldValidator
                                                    fullWidth
                                                    onPaste={() => { window.event.returnValue = false; }}
                                                    onContextMenu={() => { window.event.returnValue = false; }}
                                                    onCopy={() => { window.event.returnValue = false; }}
                                                    onCut={() => { window.event.returnValue = false; }}
                                                    onChange={e => onChange(e.target.value, 'password')}
                                                    type={this.state.showPassword ? 'text' : 'password'}
                                                    value={approverPassword}
                                                    name={'approverPassword'}
                                                    id={'registration_approverPassword'}
                                                    label="Password"
                                                    msgPosition="bottom"
                                                    validators={[ValidatorEnum.required]}
                                                    errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                                    InputProps={{
                                                        endAdornment:
                                                            <InputAdornment position="end">
                                                                <IconButton
                                                                    aria-label="toggle password visibility"
                                                                    id="registration_passwordVisibility"
                                                                    onMouseDown={this.handleMouseDownPassword}
                                                                    onMouseUp={this.handleMouseUpPassword}
                                                                    onMouseLeave={this.handleMouseLeavePassword}
                                                                >
                                                                    {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                                                                </IconButton>
                                                            </InputAdornment>
                                                    }}
                                                />
                                            </Grid>
                                        </Grid>
                                    </ValidatorForm>
                                </CIMSFormLabel>
                            </Grid>
                    }
                </DialogContent>
                <DialogActions style={{ justifyContent: 'center', padding: 0 }}>
                    <CIMSButton
                        style={{ minWidth: '140px', height: '38px' }}
                        id="registration_btnConfirmApprovalDialog"
                        onClick={this.handleConfirm}
                    >Confirm</CIMSButton>
                    <CIMSButton
                        style={{ minWidth: '140px', height: '38px' }}
                        id="registration_btnCancelApprovalDialog"
                        onClick={() => cancel()}
                    >Cancel</CIMSButton>
                </DialogActions>
            </NewAprrovalDialog>
        );
    }
}

ApprovalDialog.propTypes = {
};

export default withStyles(styles)(ApprovalDialog);