import React from 'react';
import SelectFieldValidator from '../../../components/FormValidator/SelectFieldValidator';
import DelayInput from '../../compontent/delayInput';
import RequiredIcon from '../../../components/InputLabel/RequiredIcon';
import FieldConstant from '../../../constants/fieldConstant';
import CommonMessage from '../../../constants/commonMessage';
import RegFieldLength from '../../../enums/registration/regFieldLength';
import {
    FormControl,
    // FormHelperText,
    Grid
} from '@material-ui/core';
// import FormInputLabel from '../../compontent/label/formInputLabel';
import ValidatorEnum from '../../../enums/validatorEnum';
import Enum from '../../../enums/enum';

class NormalPhoneField extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            phoneTypeCd_Valid: true,
            countryCd_Valid: true,
            phoneNo_Valid: true,
            errorMessage: {
                phoneTypeCd_errMsg: '',
                countryCd_errMsg: '',
                phoneNo_errMsg: ''
            },
            phoneMaxLength: RegFieldLength.CONTACT_PHONE_DEFAULT_MAX
        };
    }

    componentDidMount() {
        if(this.props.phone){
            if (this.props.phone.countryCd !== FieldConstant.COUNTRY_CODE_DEFAULT_VALUE) {
                this.setState({ phoneMaxLength: RegFieldLength.CONTACT_PHONE_OTHERS_MAX });
            } else {
                this.setState({ phoneMaxLength: RegFieldLength.CONTACT_PHONE_DEFAULT_MAX });
            }
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.state !== nextState ||
            this.props.isRequired !== nextProps.isRequired ||
            this.props.phone !== nextProps.phone ||
            this.props.labelText !== nextProps.labelText ||
            this.props.comDisabled !== nextProps.comDisabled ||
            this.props.countryOptions !== nextProps.countryOptions;
    }

    UNSAFE_componentWillUpdate(nextProps){
        if(nextProps.phone && nextProps.phone.countryCd !== this.props.phone.countryCd){
            if (nextProps.phone.countryCd !== FieldConstant.COUNTRY_CODE_DEFAULT_VALUE) {
                this.setState({ phoneMaxLength: RegFieldLength.CONTACT_PHONE_OTHERS_MAX });
            } else {
                this.setState({ phoneMaxLength: RegFieldLength.CONTACT_PHONE_DEFAULT_MAX });
            }
        }
    }

    // validatorListener = (isvalid, errMsg, name) => {
    //     let { errorMessage } = this.state;
    //     if (!isvalid && errMsg) {
    //         errorMessage[name + '_errMsg'] = errMsg;
    //     } else {
    //         errorMessage[name + '_errMsg'] = '';
    //     }
    //     this.setState({ [name + '_Valid']: isvalid, errorMessage });
    // }

    handleOnChange = (value, name) => {
        // if (name === 'countryCd') {
        //     if (value !== FieldConstant.COUNTRY_CODE_DEFAULT_VALUE) {
        //         this.setState({ phoneMaxLength: RegFieldLength.CONTACT_PHONE_OTHERS_MAX });
        //     } else {
        //         this.setState({ phoneMaxLength: RegFieldLength.CONTACT_PHONE_DEFAULT_MAX });
        //     }
        // }
        if (this.props.onChange) {
            this.props.onChange(value, name);
        }
    }

    render() {
        const { comDisabled, phone, countryOptions, isRequired, id } = this.props;
        // const { labelText } = this.props;
        // const { errorMessage } = this.state;
        // let errorMsg = errorMessage.phoneTypeCd_errMsg || errorMessage.countryCd_errMsg || errorMessage.phoneNo_errMsg;
        let
            validators = [ValidatorEnum.phoneNo],
            errMsgs = [CommonMessage.VALIDATION_NOTE_PHONE_NO()];
        if (phone && phone.countryCd === FieldConstant.COUNTRY_CODE_DEFAULT_VALUE) {
            validators = validators.concat([ValidatorEnum.minStringLength(RegFieldLength.CONTACT_PHONE_DEFAULT_MAX), ValidatorEnum.maxStringLength(RegFieldLength.CONTACT_PHONE_DEFAULT_MAX)]);
            errMsgs = errMsgs.concat([CommonMessage.VALIDATION_NOTE_PHONE_BELOWMINWIDTH(), CommonMessage.VALIDATION_NOTE_OVERMAXWIDTH()]);
        }
        if (isRequired) {
            validators = validators.concat([ValidatorEnum.required]);
            errMsgs = errMsgs.concat([CommonMessage.VALIDATION_NOTE_REQUIRED()]);
        }
        return (
            <FormControl fullWidth style={{ display: 'flex', flexDirection: 'column' }}>
                {/* <Grid container item alignItems="baseline" spacing={1}>
                    <Grid item>
                        <FormInputLabel>{labelText}{isRequired ? <RequiredIcon /> : null}</FormInputLabel>
                    </Grid>
                    <Grid item>
                        <FormHelperText style={{ marginTop: 0 }} error id={id + '_errorMsg'}>
                            {
                                this.state.phoneTypeCd_Valid &&
                                    this.state.country_Valid &&
                                    this.state.phone_Valid ? '' : errorMsg
                            }
                        </FormHelperText>
                    </Grid>
                </Grid> */}
                <Grid container spacing={1}>
                    <Grid item xs={3}>
                        <SelectFieldValidator
                            id={id + '_phoneTypeCd'}
                            options={Enum.PHONE_DROPDOWN_LIST.map((item) => (
                                { value: item.value, label: item.label }
                            ))}
                            isDisabled={this.props.comDisabled}
                            value={phone ? phone.phoneTypeCd : Enum.PHONE_TYPE_MOBILE_PHONE}
                            onChange={e => this.handleOnChange(e.value, 'phoneTypeCd')}
                            validators={[ValidatorEnum.required]}
                            errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                            // validatorListener={(...args) => this.validatorListener(...args, 'phoneTypeCd')}
                            // notShowMsg
                            msgPosition="bottom"
                            TextFieldProps={{
                                variant: 'outlined',
                                label: <>Type{isRequired?<RequiredIcon />:null}</>
                                // InputLabelProps: { shrink: true }
                            }}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <SelectFieldValidator
                            id={id + '_country'}
                            options={countryOptions && countryOptions.map((item) => (
                                { value: item.countryCd, label: `${item.countryName || ''}(${item.dialingCd})` }
                            ))}
                            isDisabled={this.props.comDisabled}
                            value={phone ? phone.countryCd : FieldConstant.COUNTRY_CODE_DEFAULT_VALUE}
                            onChange={e => this.handleOnChange(e.value, 'countryCd')}
                            validators={phone && phone.phoneNo ? [ValidatorEnum.required] : null}
                            errorMessages={phone && phone.phoneNo ? [CommonMessage.VALIDATION_NOTE_REQUIRED()] : null}
                            // validatorListener={(...args) => this.validatorListener(...args, 'countryCd')}
                            // notShowMsg
                            msgPosition="bottom"
                            TextFieldProps={{
                                variant: 'outlined',
                                label: <>Country Code{isRequired?<RequiredIcon />:null}</>
                                // InputLabelProps: { shrink: true }
                            }}
                        />
                    </Grid>
                    <Grid item xs={5}>
                        <DelayInput
                            id={id + '_phoneNo'}
                            name="phoneNo"
                            type="number"
                            inputProps={{ maxLength: this.state.phoneMaxLength }}
                            disabled={comDisabled}
                            value={phone ? phone.phoneNo : ''}
                            onChange={e => this.handleOnChange(e.target.value, 'phoneNo')}
                            // notShowMsg
                            validByBlur
                            msgPosition="bottom"
                            validators={validators}
                            errorMessages={errMsgs}
                            // validatorListener={(...args) => this.validatorListener(...args, 'phoneNo')}
                            label={<>Phone{isRequired?<RequiredIcon />:null}</>}
                            // InputLabelProps={{ shrink: true }}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
                {/* {
                    (!this.state.phoneTypeCd_Valid ||
                        !this.state.country_Valid ||
                        !this.state.phone_Valid) && errorMsg ?
                        <Grid container item>
                            <FormHelperText style={{ marginTop: 0 }} error id={id + '_errorMsg'}>
                                {errorMsg}
                            </FormHelperText>
                        </Grid> : null
                } */}
            </FormControl>
        );
    }

}

export default NormalPhoneField;