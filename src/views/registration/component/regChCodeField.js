import React from 'react';
import { connect } from 'react-redux';
import RegFieldName from '../../../enums/registration/regFieldName';
import {
    FormControl,
    Grid
} from '@material-ui/core';
import RegFieldLength from '../../../enums/registration/regFieldLength';
// import FormInputLabel from '../../compontent/label/formInputLabel';
import TextFieldValidator from '../../../components/FormValidator/TextFieldValidator';
import {
    updateState,
    searchPatient,
    findCharByCcCode,
    updateCcCode,
    updateChiChar
} from '../../../store/actions/registration/registrationAction';

class RegChCodeField extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            backspaceOrEnterPre:-1
        };
    }

    shouldComponentUpdate(nextProps, nextState) {

        if(this.state !== nextState ||
            this.props.comDisabled !== nextProps.comDisabled ||
            this.props.ccCode1 !== nextProps.ccCode1 ||
            this.props.ccCode2 !== nextProps.ccCode2 ||
            this.props.ccCode3 !== nextProps.ccCode3 ||
            this.props.ccCode4 !== nextProps.ccCode4 ||
            this.props.ccCode5 !== nextProps.ccCode5 ||
            this.props.ccCode6 !== nextProps.ccCode6
            ){
                if(document.activeElement.id.indexOf(nextProps.id)>-1){
                    //Adjust focus when entering ccCode
                    if(nextState.backspaceOrEnterPre>=0){
                        //focus is changed cause by backspace Or Enter
                        this.cmps[nextState.backspaceOrEnterPre].focus();
                        nextState.backspaceOrEnterPre=-1;
                    }else{
                        //When the data changes, the focus stays at the stop position
                        for(let i=0;i<this.cmps.length;i++){
                            let ccCode = (nextProps[`ccCode${i+1}`] || '').toString();
                            let item = this.cmps[i];
                            if(ccCode.length<RegFieldLength.CHINESE_CODE_MAX){
                                if(document.activeElement.id!==item.id){
                                    this.lock.isAutoBlur=true;
                                    item.focus();
                                }
                                i=6;
                            }
                        }
                    }
                }
                return true;
            }else{
                return false;
            }
    }


    cmps = []
    lock={
        isAutoBlur:false //static lock,input wont tirgger onBlur anymore after it lose focus
    }

    handleOnChange = (e,index) => {
        this.props.resetChineseNameFieldValid();//clear Chinese name validation
        let value =e.target.value;
        if((index!==5||value.length<=4)&&value!==this.props[`ccCode${index+1}`]){
            let maxLength = RegFieldLength.CHINESE_CODE_MAX;
            let ccCodeList = [];
            ccCodeList.push({index:index,code:value.slice(0, maxLength)});
            if (value.length >= maxLength) {
                let newValue = value.slice(0, maxLength);
                this.props.findCharByCcCode(newValue,index);
                if(index<5){
                    ccCodeList.push({index:index+1,code:value.slice(maxLength, 2*maxLength)});
                }
            }
            this.props.updateCcCode(ccCodeList);
        }
    }

    onKeyDown = (e,index)=>{
        if(e.keyCode===13){
            if(index<5){
                this.setState({backspaceOrEnterPre:index+1});
            }else{
                this.cmps[index].blur();
            }
        }else if(e.keyCode===8&&index>0&&!this.props['ccCode' + (index + 1)]){
            this.setState({backspaceOrEnterPre:index-1});
        }
    }

    onBlur = (e,index)=>{
        if(!this.lock.isAutoBlur){
            let value = e.target.value.slice(0, RegFieldLength.CHINESE_CODE_MAX);
            if(value.length>0&&value.length<RegFieldLength.CHINESE_CODE_MAX){
                let length = RegFieldLength.CHINESE_CODE_MAX - value.length;
                for(let i = 0;i<length;i++){
                    value = '0'+value;
                }
                let ccCodeList = [];
                ccCodeList.push({index:index,code:value});
                this.props.findCharByCcCode(value,index);
                this.props.updateCcCode(ccCodeList);
            }
        }else{
            this.lock.isAutoBlur = false;
        }
    }

    render() {
        const { comDisabled, id } = this.props;
        const textFieldProps=[
            {
                name: RegFieldName.CHINESE_CODE1,
                value: this.props.ccCode1
            },
            {
                name: RegFieldName.CHINESE_CODE2,
                value: this.props.ccCode2
            },
            {
                name: RegFieldName.CHINESE_CODE3,
                value: this.props.ccCode3
            },
            {
                name: RegFieldName.CHINESE_CODE4,
                value: this.props.ccCode4
            },
            {
                name: RegFieldName.CHINESE_CODE5,
                value: this.props.ccCode5
            },
            {
                name: RegFieldName.CHINESE_CODE6,
                value: this.props.ccCode6
            }
        ];
        return (
            <FormControl fullWidth>
                <Grid container item justify="space-between" spacing={1}>
                    {
                        textFieldProps.map((item, index) => (
                            <Grid item xs={2} key={index}>
                                <TextFieldValidator
                                    id={id + '_' + item.name}
                                    type="number"
                                    inputProps={{
                                        ref:e =>this.cmps[index] = e
                                    }}
                                    variant="outlined"
                                    label={'中文電碼' + (index + 1)}
                                    InputLabelProps={{ shrink: true }}
                                    disabled={comDisabled||(index>0&&!textFieldProps[index-1].value)}
                                    value={item.value || ''}
                                    name={item.name}
                                    onChange={e=>this.handleOnChange(e,index)}
                                    onKeyDown={e=>this.onKeyDown(e,index)}
                                    onBlur={e=>this.onBlur(e,index)}
                                />
                            </Grid>
                        ))
                    }
                </Grid>
            </FormControl>
        );
    }
}

const mapStateToProps = state => {
    return {
        ccCode1: state.registration.patientBaseInfo.ccCode1,
        ccCode2: state.registration.patientBaseInfo.ccCode2,
        ccCode3: state.registration.patientBaseInfo.ccCode3,
        ccCode4: state.registration.patientBaseInfo.ccCode4,
        ccCode5: state.registration.patientBaseInfo.ccCode5,
        ccCode6: state.registration.patientBaseInfo.ccCode6
    };
};

const mapDispatchToProps = {
    updateState,
    searchPatient,
    findCharByCcCode,
    updateCcCode,
    updateChiChar
};

export default connect(mapStateToProps, mapDispatchToProps)(RegChCodeField);