import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import {
  Grid,
  Stepper,
  Step,
  StepButton
} from '@material-ui/core';
import _ from 'lodash';
import moment from 'moment';

import CIMSButtonGroup from '../../components/Buttons/CIMSButtonGroup';
import ValidatorForm from '../../components/FormValidator/ValidatorForm';
import ButtonStatusEnum from '../../enums/registration/buttonStatusEnum';
import accessRightEnum from '../../enums/accessRightEnum';
import { codeList } from '../../constants/codeList';
import * as RegistrationType from '../../store/actions/registration/registrationActionType';
import * as EcsActionType from '../../store/actions/ECS/ecsActionType';
import * as RegUtil from '../../utilities/registrationUtilities';
import PersonalParticulars from './personalParticulars';
import ContactInformation from './contactInformation';
import ContactPerson from './contactPerson';
import SocialData from './socialData';
import ReviewInformation from './reviewInformation';
import SearchDialog from './component/searchDialog';
import EditModeMiddleware from '../compontent/editModeMiddleware';
import ApprovalDialog from './component/approvalDialog';

import { getCodeList, getLanguageList } from '../../store/actions/patient/patientAction';
import {
  resetAll,
  updatePatientOperateStatus,
  getPatientById,
  updatePatient,
  registerPatient,
  updateState,
  searchPatient
} from '../../store/actions/registration/registrationAction';
import { openCommonMessage } from '../../store/actions/message/messageAction';
import { openCommonCircular, closeCommonCircular } from '../../store/actions/common/commonAction';
import { updateTabLabel } from '../../store/actions/mainFrame/mainFrameAction';
import * as commonUtilities from '../../utilities/commonUtilities';
import { refreshServiceStatus } from '../../store/actions/ECS/ecsAction';
import * as constants from '../../constants/registration/registrationConstants';
import Enum from '../../enums/enum';
import {resetAll as resetPatient } from '../../store/actions/patient/patientAction';

const useStyle1 = () => ({
  root: {
    display: 'flex',
    flexFlow: 'column',
    height: 'calc(100vh - 106px)'
  },
  stepperGrid: {
    display: 'flex',
    justifyContent: 'center'
  },
  stepper: {
    paddingBottom: 15,
    paddingTop: 5,
    width: '80%'
  },
  content: {
    flex: 1,
    overflowY: 'auto'
  },
  buttonGroup: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  button: {
    minWidth: 120
  },
  hidden: {
    display: 'none'
  }
});

const sysRatio = commonUtilities.getSystemRatio();
const unit = commonUtilities.getResizeUnit(sysRatio);
const customTheme = (theme) => createMuiTheme({
  ...theme,
  spacing: (18 * unit)
});

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      steps: ['Personal Particulars', 'Contact Information', 'Contact Person', 'Social Data'],
      openApproval: false,
      loginName: '',
      password: '',
      activeStep: 0,
      comDisabled: true
    };
  }

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.patientOperationStatus === ButtonStatusEnum.EDIT || nextProps.patientOperationStatus === ButtonStatusEnum.ADD) {
      return { comDisabled: false };
    } else {
      return { comDisabled: true };
    }
  }

  componentDidMount() {
    this.props.ensureDidMount();
    let params = [
      codeList.gender,
      codeList.exact_date_of_birth,
      codeList.district,
      codeList.sub_district,
      codeList.document_type,
      codeList.edu_level,
      codeList.ethnicity,
      codeList.gov_department,
      codeList.marital_status,
      codeList.occupation,
      codeList.relationship,
      codeList.religion,
      codeList.translation_lang
    ];
    this.props.getCodeList(params, [RegistrationType.PUT_GET_CODE_LIST,EcsActionType.PUT_GET_CODE_LIST]);

    this.props.getLanguageList();

    this.props.refreshServiceStatus();
    ValidatorForm.addValidationRule('isChinese', (value) => {
      return RegUtil.checkChinese(value);
    });
    ValidatorForm.addValidationRule('isHkid', (value) => {
      return RegUtil.checkHKID(value);
    });

    this.props.resetAll();

    //redirect from other page by params
    if (this.props.location.state) {
      const params = this.props.location.state;
      //redirect from patientlist searchPatient
      if (params.isReviews && params.patientKey) {
        this.props.getPatientById(params.patientKey);
        this.props.updateState({
          isOpenReview: true
        });
      }
      //redirect from patientlist linkpatient
      else {
        let patient = _.cloneDeep(this.props.patientBaseInfo);
        patient.docTypeCd = params.docTypeCd || '';
        patient.engSurname = params.engSurname || '';
        patient.engGivename = params.engGivename || '';
        if (params.phoneNo) {
          patient.phone.phoneNo = params.phoneNo || '';
        }
        if (params.docTypeCd && params.docTypeCd === 'ID') {
          patient.hkid = params.hkidOrDocNum || '';
          patient.docTypeCd = '';
        } else {
          patient.otherDocNo = params.hkidOrDocNum || '';
        }

        this.props.updateState({
          patientBaseInfo: patient,
          patientOperationStatus: ButtonStatusEnum.ADD
        });
        this.resetAllValidation();
      }
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isOpenReview !== this.props.isOpenReview) {
      this.props.updateTabLabel(accessRightEnum.registration, this.props.isOpenReview ? 'Patient Summary' : 'Registration');
    }
    if (this.props.patientOperationStatus === ButtonStatusEnum.EDIT || this.props.patientOperationStatus === ButtonStatusEnum.ADD) {
      this.props.updateState({ isSaveSuccess: false });
    }
  }

  componentWillUnmount() {
    this.props.resetAll();
  }


  handleStep = (step, callback) => {
    if (this.state.activeStep === 0 && !this.state.comDisabled) {
      const personalValid = this.refs.personalForm.isFormValid(false);
      personalValid.then(result => {
        if (result) {
          this.setState({ activeStep: step });
        }
      });
    } else if (this.state.activeStep === 1 && !this.state.comDisabled) {
      const contactValid = this.refs.contactInfoForm.isFormValid(false);
      contactValid.then(result => {
        if (result) {
          this.setState({ activeStep: step });
        }
      });
    } else if (this.state.activeStep === 2 && !this.state.comDisabled) {
      const contactPerValid = this.refs.contactPersonForm.isFormValid(false);
      contactPerValid.then(result => {
        if (result) {
          this.setState({ activeStep: step });
        }
      });
    } else {
      this.setState({ activeStep: step });
    }
    callback && callback();
  }

  btnBackOnClick = () => {
    if (this.state.activeStep === 1 && !this.state.comDisabled) {
      const contactValid = this.refs.contactInfoForm.isFormValid(false);
      contactValid.then(result => {
        if (result) {
          this.backStepAction();
        }
      });
    } else if (this.state.activeStep === 2 && !this.state.comDisabled) {
      const contactPerValid = this.refs.contactPersonForm.isFormValid(false);
      contactPerValid.then(result => {
        if (result) {
          this.backStepAction();
        }
      });
    } else {
      this.backStepAction();
    }
  }

  backStepAction() {
    if (this.state.activeStep > 0) {
      const activeStep = this.state.activeStep - 1;
      this.setState({ activeStep: activeStep });
    }
  }

  btnNextOnClick = (e) => {
    e.preventDefault();
    if (this.state.activeStep === 0 && !this.state.comDisabled) {
      const personalValid = this.refs.personalForm.isFormValid(false);
      personalValid.then(result => {
        if (result) {
          this.nextStepAction();
        }
      });
    } else if (this.state.activeStep === 1 && !this.state.comDisabled) {
      const contactValid = this.refs.contactInfoForm.isFormValid(false);
      contactValid.then(result => {
        if (result) {
          this.nextStepAction();
        }
      });
    } else if (this.state.activeStep === 2 && !this.state.comDisabled) {
      const contactPerValid = this.refs.contactPersonForm.isFormValid(false);
      contactPerValid.then(result => {
        if (result) {
          this.nextStepAction();
        }
      });
    } else {
      this.nextStepAction();
    }
  }

  nextStepAction = () => {
    if (this.state.activeStep < this.state.steps.length - 1) {
      const activeStep = this.state.activeStep + 1;
      this.setState({ activeStep: activeStep });
    }
  }

  btnSaveOnClick = (e) => {
    e.preventDefault();
    this.props.openCommonCircular();
    const baseValid = this.refs.personalForm.isFormValid(false);
    const contactValid = this.refs.contactInfoForm.isFormValid(false);
    const contactPerValid = this.refs.contactPersonForm.isFormValid(false);
    let isValid = true;
    baseValid.then(result => {
      if (!result) {
        isValid = false;
        this.setState({ activeStep: 0 });
      }
    }).then(() => {
      contactValid.then(result => {
        if (!result) {
          isValid = false;
          this.setState({ activeStep: 1 });
        }
      });
    }).then(() => {
      contactPerValid.then(result => {
        if (!result) {
          isValid = false;
          this.setState({ activeStep: 2 });
        }
      });
    }).then(() => {
      if (isValid) {
        const { patientOperationStatus, patientBaseInfo, patientById } = this.props;
        if (patientOperationStatus === ButtonStatusEnum.EDIT && (
          (patientBaseInfo.hkid || '') !== (patientById.hkid || '') ||
          (patientBaseInfo.engSurname || '') !== (patientById.engSurname || '') ||
          (patientBaseInfo.engGivename || '') !== (patientById.engGivename || '') ||
          (patientBaseInfo.nameChi || '') !== (patientById.nameChi || '') ||
          (patientBaseInfo.genderCd || '') !== (patientById.genderCd || '') ||
          // moment(patientBaseInfo.dob).format('YYYY-MM-DD') !== moment(patientById.dob).format('YYYY-MM-DD'
          moment(patientBaseInfo.dob).format(Enum.DATE_FORMAT_EYMD_VALUE) !== moment(patientById.dob).format(Enum.DATE_FORMAT_EYMD_VALUE))) {
          this.setState({ openApproval: true });
          this.props.closeCommonCircular();
        } else {
          this.doRegister();
        }
      } else {
        this.props.closeCommonCircular();
        this.props.openCommonMessage({ msgCode: '110101' });
      }
    });
  }

  transformToGroupCd = () => {
    let defaultLangGroup = '';
    const { patientBaseInfo, languageData } = this.props;
    const langGroupList = languageData.codeLangGroupDtos;
    langGroupList.forEach((item) => {
      let filterPreferredLangList = languageData && languageData.codePreferredLangMap && languageData.codePreferredLangMap[item.langGroupCd] && languageData.codePreferredLangMap[item.langGroupCd].filter(itemObj => itemObj.preferredLangCd == patientBaseInfo.preferredLangCd);
      if (filterPreferredLangList && filterPreferredLangList.length == 1) {
        defaultLangGroup = filterPreferredLangList && filterPreferredLangList[0] && filterPreferredLangList[0].langGroupCd;
      } else if (filterPreferredLangList && filterPreferredLangList.length == 2) {
        defaultLangGroup = filterPreferredLangList && filterPreferredLangList[1] && filterPreferredLangList[1].langGroupCd;

      }
    });
    return defaultLangGroup || langGroupList[0].langGroupCd;
  }

  doRegister = () => {
    let patientParams = _.cloneDeep(this.props.patientById);

    //handle phone
    let phones = _.cloneDeep(this.props.phoneList);
    let baseInfo = _.cloneDeep(this.props.patientBaseInfo);
    if (phones.length === 1 && !phones[0].phoneNo) {
      phones = [];
    }
    phones.unshift(baseInfo.phone);
    patientParams.phoneList = _.cloneDeep(phones);
    delete baseInfo.phone;

    patientParams = _.assign(patientParams, baseInfo);
    patientParams = _.assign(patientParams, this.props.patientContactInfo);
    patientParams = _.assign(patientParams, this.props.patientSocialData);
    patientParams.contactPersonList = _.cloneDeep(this.props.contactPersonList);
    patientParams.addressList = _.cloneDeep(this.props.addressList);


    //handle hkid and other document
    if (patientParams.hkid) {
      patientParams.hkid = patientParams.hkid.replace('(', '').replace(')', '');
      let hkidDto = patientParams.documentPairList.find(item => item.docTypeCd === Enum.DOC_TYPE.HKID_ID);
      if (hkidDto) {
        hkidDto.docNo = patientParams.hkid;
      } else {
        hkidDto = _.cloneDeep(constants.patientDocumentPair);
        hkidDto.docNo = patientParams.hkid;
        hkidDto.docTypeCd = Enum.DOC_TYPE.HKID_ID;
        hkidDto.patientKey = patientParams.patientKey;
        hkidDto.serviceCd = this.props.serviceCd;
        patientParams.documentPairList.push(hkidDto);
      }
    } else {
      patientParams.documentPairList = patientParams.documentPairList.filter(item => item.docTypeCd !== Enum.DOC_TYPE.HKID_ID);
    }

    if (patientParams.otherDocNo) {
      let otherDocDto = patientParams.documentPairList.find(item => item.docTypeCd === patientParams.docTypeCd);
      if (otherDocDto) {
        otherDocDto.docNo = patientParams.otherDocNo;
      } else {
        otherDocDto = _.cloneDeep(constants.patientDocumentPair);
        otherDocDto.docNo = patientParams.otherDocNo;
        otherDocDto.docTypeCd = patientParams.docTypeCd;
        otherDocDto.patientKey = patientParams.patientKey;
        otherDocDto.serviceCd = this.props.serviceCd;
        patientParams.documentPairList.push(otherDocDto);
      }
    } else {
      //at present, only delete the first other document
      const otherDocNoIndex = patientParams.documentPairList.findIndex(item => item.docTypeCd !== Enum.DOC_TYPE.HKID_ID);
      if (otherDocNoIndex > -1) {
        patientParams.documentPairList.splice(otherDocNoIndex, 1);
      }
    }

    //delete data field
    delete patientParams.hkid;
    delete patientParams.otherDocNo;
    delete patientParams.docTypeCd;

    patientParams.dob = RegUtil.getDateByFormat(patientParams.exactDobCd, patientParams.dob).format(Enum.DATE_FORMAT_EYMD_VALUE);

    const preferredLangList = this.props.languageData && this.props.languageData.codePreferredLangMap && this.props.languageData.codePreferredLangMap[this.transformToGroupCd()];

    let params = {
      ...patientParams,
      preferredLangCd: (this.props.patientBaseInfo.preferredLangCd != 'E' && this.props.patientBaseInfo.preferredLangCd) || (preferredLangList && preferredLangList[0] && preferredLangList[0].preferredLangCd)
    };

    if (this.props.patientOperationStatus === ButtonStatusEnum.EDIT) {
      params.userId = 'admin';
      params.clinicCd = 'A2';
      params.loginName = this.state.loginName;
      params.password = this.state.password;

      this.props.updatePatient(params, () => { this.props.resetPatient(); });
    } else if (this.props.patientOperationStatus === ButtonStatusEnum.ADD) {

      this.props.registerPatient(params, () => {this.props.resetPatient(); });
    }
  }

  btnEditOnClick = () => {
    this.props.updatePatientOperateStatus(ButtonStatusEnum.EDIT);
  }

  btnCancelOnClick = () => {
    this.setState({ activeStep: 0 });
    this.resetAllValidation();
    this.props.resetAll();
  }

  resetAllValidation = () => {
    this.refs.personalForm.resetValidations();
    this.refs.contactInfoForm.resetValidations();
    this.refs.contactPersonForm.resetValidations();
  }

  checkContactPersonValid = (isIncludeRequired) => {
    return this.refs.contactPersonForm.isFormValid(false, isIncludeRequired);
  }

  handleConfirmApproval = () => {
    this.doRegister();
    this.setState({ openApproval: false, loginName: '', password: '' });
  }

  render() {
    const { activeStep, steps, comDisabled } = this.state;
    const { classes, patientOperationStatus, patientBaseInfo, patientById, isOpenReview, accessRights } = this.props;
    const isDisplayEditBtn = patientOperationStatus === ButtonStatusEnum.DATA_SELECTED ? true : false;
    commonUtilities.getSystemRatio();
    return (
      <MuiThemeProvider theme={customTheme}>
        {
          isOpenReview ?
            (patientById.patientKey ? <ReviewInformation onChangeStep={this.handleStep} /> : null)
            :
            <Grid className={classes.root}>
              <Grid className={classes.stepperGrid}>
                <Stepper className={classes.stepper} nonLinear activeStep={activeStep}>
                  {steps.map((label, index) => (
                    <Step key={label}>
                      <StepButton
                          onClick={() => { this.handleStep(index); }}
                          completed={index < activeStep}
                          tabIndex={-1}
                      >
                        {label}
                        {index === 0 ? <span style={{ color: 'red' }}>*</span> : null}
                      </StepButton>
                    </Step>
                  ))}
                </Stepper>
              </Grid>
              <Grid className={classes.content}>
                <ValidatorForm className={activeStep !== 0 ? classes.hidden : null} ref="personalForm">
                  <PersonalParticulars comDisabled={comDisabled} />
                </ValidatorForm>
                <ValidatorForm className={activeStep !== 1 ? classes.hidden : null} ref="contactInfoForm">
                  <MuiThemeProvider theme={customTheme}>
                    <ContactInformation comDisabled={comDisabled} />
                  </MuiThemeProvider>
                </ValidatorForm>
                <ValidatorForm className={activeStep !== 2 ? classes.hidden : null} ref="contactPersonForm">
                  <MuiThemeProvider theme={customTheme}>
                    <ContactPerson comDisabled={comDisabled} checkContactValid={this.checkContactPersonValid} />
                  </MuiThemeProvider>
                </ValidatorForm>
                <ValidatorForm className={activeStep !== 3 ? classes.hidden : null} ref="socialDataForm">
                  <MuiThemeProvider theme={customTheme}>
                    <SocialData comDisabled={comDisabled} />
                  </MuiThemeProvider>

                </ValidatorForm>
              </Grid>
              <CIMSButtonGroup
                  buttonConfig={
                  [
                    {
                      id: 'registration_btnBack',
                      name: 'Back',
                      className: `${activeStep === 0 ? classes.hidden : null}`,
                      onClick: this.btnBackOnClick
                    },
                    {
                      id: 'registration_btnNext',
                      name: 'Next',
                      className: `${activeStep >= steps.length - 1 ? classes.hidden : null}`,
                      onClick: this.btnNextOnClick,
                      disabled: comDisabled && !isDisplayEditBtn
                    },
                    {
                      id: 'registration_btnSave',
                      name: 'Save',
                      disabled: comDisabled,
                      onClick: this.btnSaveOnClick
                    },
                    {
                      id: 'registration_btnEdit',
                      name: 'Edit',
                      disabled: !isDisplayEditBtn,
                      onClick: this.btnEditOnClick
                    },
                    {
                      id: 'registration_btnCancel',
                      name: 'Cancel',
                      disabled: comDisabled && !isDisplayEditBtn,
                      onClick: this.btnCancelOnClick
                    }
                  ]
                }
              />
              <SearchDialog
                  open={patientOperationStatus === ButtonStatusEnum.SEARCH}
                  data={this.props.patientList}
                  clickView={(key) => this.props.getPatientById(key)}
                  closeDialog={() => this.props.updatePatientOperateStatus(ButtonStatusEnum.VIEW)}
                  searchPatient={(...args) => this.props.searchPatient(...args)}
                  searchString={this.props.patientBaseInfo.hkid}
              />
              <ApprovalDialog
                  open={this.state.openApproval}
                  confirm={this.handleConfirmApproval}
                  cancel={() => this.setState({ openApproval: false, loginName: '', password: '' })}
                  approverName={this.state.loginName}
                  approverPassword={this.state.password}
                  onChange={(value, name) => this.setState({ [name]: value })}
                  existingInfo={patientById}
                  newInfo={patientBaseInfo}
                  isUserHaveAccess={accessRights && accessRights.findIndex(item => item.name === accessRightEnum.changePatientMajorKey) > -1}
              />
            </Grid>
        }
        <EditModeMiddleware componentName={accessRightEnum.registration} when={!comDisabled} />
      </MuiThemeProvider>
    );
  }
}

function mapStateToProps(state) {
  return {
    patientOperationStatus: state.registration.patientOperationStatus,
    isOpenPatientDialog: state.registration.isOpenPatientDialog,
    patientErrorMessage: state.registration.patientErrorMessage,
    patientErrorData: state.registration.patientErrorData,
    patientList: state.registration.patientList,
    patientBaseInfo: state.registration.patientBaseInfo,
    patientContactInfo: state.registration.patientContactInfo,
    patientSocialData: state.registration.patientSocialData,
    contactPersonList: state.registration.contactPersonList,
    addressList: state.registration.addressList,
    phoneList: state.registration.phoneList,
    patientById: state.registration.patientById,
    isOpenReview: state.registration.isOpenReview,
    languageData: state.patient.languageData,
    serviceCd: state.login.service.serviceCd,
    accessRights: state.login.accessRights
  };
}

const dispatchProps = {
  getCodeList,
  resetAll,
  updatePatientOperateStatus,
  getPatientById,
  openCommonCircular,
  closeCommonCircular,
  updatePatient,
  registerPatient,
  updateState,
  searchPatient,
  openCommonMessage,
  updateTabLabel,
  getLanguageList,
  refreshServiceStatus,
  resetPatient
};

export default connect(mapStateToProps, dispatchProps)(withStyles(useStyle1)(Registration));
