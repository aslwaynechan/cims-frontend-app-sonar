import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Remove, Add } from '@material-ui/icons';
import {
  Grid,
  FormControlLabel,
  IconButton,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails
} from '@material-ui/core';
import _ from 'lodash';
import { patientAddressBasic } from '../../constants/registration/registrationConstants';
import { updateState } from '../../store/actions/registration/registrationAction';
import DelayInput from '../compontent/delayInput';
import SelectFieldValidator from '../../components/FormValidator/SelectFieldValidator';
import OutlinedRadioValidator from '../../components/FormValidator/OutlinedRadioValidator';
import OutlinedCheckBoxValidator from '../../components/FormValidator/OutlinedCheckBoxValidator';
import CommonMessage from '../../constants/commonMessage';
import RegFieldName from '../../enums/registration/regFieldName';
import Enum from '../../enums/enum';
import ValidatorEnum from '../../enums/validatorEnum';
import OtherPhonesField from './component/otherPhonesField';
import memoize from 'memoize-one';
import RequiredIcon from '../../components/InputLabel/RequiredIcon';
import CIMSCheckBox from '../../components/CheckBox/CIMSCheckBox';
import { openADISearchDialog } from '../../utilities/registrationUtilities';
import CIMSButton from '../../components/Buttons/CIMSButton';
import {
  openCommonCircularDialog,
  closeCommonCircularDialog
} from '../../store/actions/common/commonAction';

const fieldLabel = {
  CONTACT_EMAIL: 'Email',
  CONTACT_ROOM: 'Room/Flat',
  CONTACT_FLOOR: 'Floor',
  CONTACT_BLOCK: 'Block',
  CONTACT_BUILDING: 'Building',
  CONTACT_ESTATELOT: 'Estate/Village',
  CONTACT_STREET_NO: 'Street No.',
  CONTACT_STREET: 'Street/Road',
  CONTACT_POSTOFFICE_BOXNO: 'Post Office Box No.',
  CONTACT_POSTOFFICE_NAME: 'Post Office Name',
  CONTACT_POSTOFFICE_REGION: 'Region',
  CONTACT_CORRESPONDENCE_DISTRICT: 'District',
  CONTACT_CORRESPONDENCE_SUB_DISTRICT: 'Sub District',
  CONTACT_RESIDENTIAL_DISTRICT: 'District',
  CONTACT_RESIDENTIAL_SUB_DISTRICT: 'Sub District'
};

const regionList = [{
  value: 'HK',
  engDesc: 'Hong Kong',
  chiDesc: '香港'
},
{
  value: 'KLN',
  engDesc: 'Kowloon',
  chiDesc: '九龍'
},
{
  value: 'NT',
  engDesc: 'New Territories',
  chiDesc: '新界'
}];

const adiURL = 'http://localhost:8080/ADIWebClient/ADIWebClient.view';

const addressInputs = [
  {
    label: fieldLabel.CONTACT_ROOM,
    isRequire: false,
    name: RegFieldName.CONTACT_ROOM,
    maxLength: 30
  },
  {
    label: fieldLabel.CONTACT_FLOOR,
    isRequire: false,
    name: RegFieldName.CONTACT_FLOOR,
    maxLength: 30
  },
  {
    label: fieldLabel.CONTACT_BLOCK,
    isRequire: false,
    name: RegFieldName.CONTACT_BLOCK,
    maxLength: 30
  },
  {
    label: fieldLabel.CONTACT_BUILDING,
    isRequire: false,
    name: RegFieldName.CONTACT_BUILDING,
    maxLength: 150
  },
  {
    label: fieldLabel.CONTACT_ESTATELOT,
    isRequire: false,
    name: RegFieldName.CONTACT_ESTATELOT,
    maxLength: 400
  },
  {
    label: fieldLabel.CONTACT_STREET_NO,
    isRequire: false,
    name: RegFieldName.CONTACT_STREET_NO,
    maxLength: 30
  },
  {
    label: fieldLabel.CONTACT_STREET,
    isRequire: false,
    name: RegFieldName.CONTACT_STREET,
    maxLength: 300
  }
];

class ContactInformation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: '',
      corADI: {
        lang: 'en_US',
        unit: '',
        floor: '',
        block: '',
        building: '',
        estate: '',
        streetNo: '',
        streetName: '',
        subDistrict: '',
        district: '',
        region: '',
        callbackURL: `${window.location.origin}/#/adiCallback`
      },
      resADI: {
        lang: 'en_US',
        unit: '',
        floor: '',
        block: '',
        building: '',
        estate: '',
        streetNo: '',
        streetName: '',
        subDistrict: '',
        district: '',
        region: '',
        callbackURL: `${window.location.origin}/#/adiCallback`
      }
    };
  }

  corAddressFilter = memoize((list) => list && list.find(item => item.addressTypeCd === Enum.PATIENT_CORRESPONDENCE_ADDRESS_TYPE));
  resAddressFilter = memoize((list) => list && list.find(item => item.addressTypeCd === Enum.PATIENT_RESIDENTIAL_ADDRESS_TYPE));
  postOfficeValidFilter = memoize((communicationMeansCd) => {
    let validator = [];
    let errorMsg = [];
    if (communicationMeansCd.indexOf(Enum.CONTACT_MEAN_POSTALMAIL) > -1) {
      validator.push(ValidatorEnum.required);
      errorMsg.push(CommonMessage.VALIDATION_NOTE_REQUIRED());
    }
    return { validator, errorMsg };
  });
  filterPhones = memoize(list => (list && list.filter(item => item.phonePriority !== 1)) || []);

  handleChangeInfo = (value, name) => {
    let patientContactInfo = _.cloneDeep(this.props.patientContactInfo);
    patientContactInfo[name] = value;
    this.props.updateState({ patientContactInfo: patientContactInfo });
    if (name === 'communicationMeansCd' && value.indexOf(Enum.CONTACT_MEAN_POSTALMAIL) > -1) {
      this.setState({ expanded: 'panel3' });
    }
  }

  handleChangeOtherPhones = (otherPhones) => {
    this.props.updateState({ phoneList: otherPhones });
  }

  setAddressState = (value, type, name) => {
    if (type === Enum.PATIENT_CORRESPONDENCE_ADDRESS_TYPE) {
      this.setState(prevState => ({
        corADI: {
          ...prevState.corADI,
          [name]: value
        }
      }));
    } else {
      this.setState(prevState => ({
        resADI: {
          ...prevState.resADI,
          [name]: value
        }
      }));
    }
  }

  handleChangeAddress = (value, type, name) => {
    let addressList = _.cloneDeep(this.props.addressList);
    let patientAddressObj = addressList.find(item => item.addressTypeCd === type);
    if (name === 'addressLanguageCd') {
      if (!value.target.checked) {
        value = Enum.PATIENT_ADDRESS_ENGLISH_LANUAGE;
        if (type === Enum.PATIENT_CORRESPONDENCE_ADDRESS_TYPE) {
          this.setState(prevState => ({
            corADI: { // object that we want to update
              ...prevState.corADI, // keep all other key-value pairs
              lang: 'en_US' // update the value of specific key
            }
          }));
        } else {
          this.setState(prevState => ({
            resADI: {
              ...prevState.resADI,
              lang: 'en_US'
            }
          }));
        }
      } else {
        value = value.target.value;
        if (type === Enum.PATIENT_CORRESPONDENCE_ADDRESS_TYPE) {
          this.setState(prevState => ({
            corADI: {
              ...prevState.corADI,
              lang: 'zh_HK'
            }
          }));
        } else {
          this.setState(prevState => ({
            resADI: {
              ...prevState.resADI,
              lang: 'zh_HK'
            }
          }));
        }
      }
    } else if (name === 'room') {
      this.setAddressState(value, type, 'unit');
    } else {
      this.setAddressState(value, type, name);
    }
    if (patientAddressObj) {
      patientAddressObj[name] = value;
    } else {
      let pab = _.cloneDeep(patientAddressBasic);
      pab.addressTypeCd = type;
      pab[name] = value;
      addressList.push(pab);
    }
    this.props.updateState({ addressList: addressList });
  }

  copyAddressFromCorToRes = (e, checked) => {
    this.props.updateState({ contactInfoSaveAbove: checked });
    let addressList = _.cloneDeep(this.props.addressList);
    let corAddress = addressList.find(item => item.addressTypeCd === Enum.PATIENT_CORRESPONDENCE_ADDRESS_TYPE);
    let resAddress = addressList.find(item => item.addressTypeCd === Enum.PATIENT_RESIDENTIAL_ADDRESS_TYPE);
    if (checked) {
      resAddress.addressTypeCd = Enum.PATIENT_RESIDENTIAL_ADDRESS_TYPE;
      resAddress.addressLanguageCd = corAddress.addressLanguageCd;
      resAddress.block = corAddress.block;
      resAddress.building = corAddress.building;
      resAddress.buildingCsuId = corAddress.buildingCsuId;
      resAddress.districtCd = corAddress.districtCd;
      resAddress.estate = corAddress.estate;
      resAddress.floor = corAddress.floor;
      resAddress.room = corAddress.room;
      resAddress.streetName = corAddress.streetName;
      resAddress.streetNo = corAddress.streetNo;
      resAddress.subDistrictCd = corAddress.subDistrictCd;
      resAddress.region = corAddress.region;
    } else {
      resAddress.addressTypeCd = Enum.PATIENT_RESIDENTIAL_ADDRESS_TYPE;
      resAddress.addressLanguageCd = Enum.PATIENT_ADDRESS_ENGLISH_LANUAGE;
      resAddress.block = '';
      resAddress.building = '';
      resAddress.buildingCsuId = '';
      resAddress.districtCd = '';
      resAddress.estate = '';
      resAddress.floor = '';
      resAddress.room = '';
      resAddress.streetName = '';
      resAddress.streetNo = '';
      resAddress.subDistrictCd = '';
      resAddress.region = '';
    }
    this.props.updateState({ addressList: addressList });
  }

  handleExpanChange = (expanded) => {
    this.setState({
      expanded: this.state.expanded === expanded ? '' : expanded
    });
  }

  onClickSearchBtn = (addressTypeCd, districtCdList) => {
    let addressList = _.cloneDeep(this.props.addressList);
    let corAddress = addressList.find(item => item.addressTypeCd === Enum.PATIENT_CORRESPONDENCE_ADDRESS_TYPE);
    let resAddress = addressList.find(item => item.addressTypeCd === Enum.PATIENT_RESIDENTIAL_ADDRESS_TYPE);
    let adiSring = '';
    if (addressTypeCd === Enum.PATIENT_CORRESPONDENCE_ADDRESS_TYPE) {
      adiSring = JSON.stringify(this.state.corADI);
    } else if (addressTypeCd === Enum.PATIENT_RESIDENTIAL_ADDRESS_TYPE) {
      adiSring = JSON.stringify(this.state.resADI);
    }
    openADISearchDialog(`/#/adi/${encodeURIComponent(adiURL)}/${encodeURIComponent(adiSring)}`,
      (result) => {
        this.props.closeCommonCircularDialog();
        if (result.status === 'success') {
          if (addressTypeCd === Enum.PATIENT_CORRESPONDENCE_ADDRESS_TYPE) {
            this.setState({
              corADIResult: result
            });
            corAddress.room = result.unit;
            corAddress.floor = result.floor;
            corAddress.block = result.block;
            corAddress.building = result.building;
            corAddress.estate = result.estate;
            corAddress.streetNo = result.streetNo;
            corAddress.streetName = result.streetName;
            if (districtCdList && districtCdList.district) {
              if (this.state.corADI.lang === 'en_US' && districtCdList.district.find(item => item.engDesc.toUpperCase() === result.district.toUpperCase())) {
                corAddress.districtCd = districtCdList.district.find(item => item.engDesc.toUpperCase() === result.district.toUpperCase()).code;
              } else if (this.state.corADI.lang === 'zh_HK' && districtCdList.district.find(item => item.chiDesc === result.district)) {
                corAddress.districtCd = districtCdList.district.find(item => item.chiDesc === result.district).code;
              }
            }
            if (districtCdList && districtCdList.sub_district) {
              if (this.state.corADI.lang === 'en_US' && districtCdList.sub_district.find(item => item.engDesc.toUpperCase() === result.subDistrict.toUpperCase())) {
                corAddress.subDistrictCd = districtCdList.sub_district.find(item => item.engDesc.toUpperCase() === result.subDistrict.toUpperCase()).code;
              } else if (this.state.corADI.lang === 'zh_HK' && districtCdList.sub_district.find(item => item.chiDesc === result.subDistrict)) {
                corAddress.subDistrictCd = districtCdList.sub_district.find(item => item.chiDesc === result.subDistrict).code;
              }
            }
            corAddress.region = result.region;
            corAddress.buildingCsuId = result.buildingCsuId;
          } else if (addressTypeCd === Enum.PATIENT_RESIDENTIAL_ADDRESS_TYPE) {
            this.setState({
              resADIResult: result
            });
            resAddress.room = result.unit;
            resAddress.floor = result.floor;
            resAddress.block = result.block;
            resAddress.building = result.building;
            resAddress.estate = result.estate;
            resAddress.streetNo = result.streetNo;
            resAddress.streetName = result.streetName;
            if (districtCdList && districtCdList.district) {
              if (this.state.resADI.lang === 'en_US' && districtCdList.district.find(item => item.engDesc.toUpperCase() === result.district.toUpperCase())) {
                resAddress.districtCd = districtCdList.district.find(item => item.engDesc.toUpperCase() === result.district.toUpperCase()).code;
              } else if (this.state.resADI.lang === 'zh_HK' && districtCdList.district.find(item => item.chiDesc === result.district)) {
                resAddress.districtCd = districtCdList.district.find(item => item.chiDesc === result.district).code;
              }
            }
            if (districtCdList && districtCdList.sub_district) {
              if (this.state.resADI.lang === 'en_US' && districtCdList.sub_district.find(item => item.engDesc.toUpperCase() === result.subDistrict.toUpperCase())) {
                resAddress.subDistrictCd = districtCdList.sub_district.find(item => item.engDesc.toUpperCase() === result.subDistrict.toUpperCase()).code;
              } else if (this.state.resADI.lang === 'zh_HK' && districtCdList.sub_district.find(item => item.chiDesc === result.subDistrict)) {
                resAddress.subDistrictCd = districtCdList.sub_district.find(item => item.chiDesc === result.subDistrict).code;
              }
            }
            resAddress.region = result.region;
            resAddress.buildingCsuId = result.buildingCsuId;
          }
          this.props.updateState({
            addressList: addressList
          });
        }
      },
      () => {
        this.props.closeCommonCircularDialog();
      }
    );
  }

  render() {
    const { classes, patientContactInfo, registerCodeList, comDisabled, addressList, phoneList, countryList } = this.props;
    const cor_address = this.corAddressFilter(addressList);
    const res_address = this.resAddressFilter(addressList);

    const corChiAddress = cor_address ? cor_address.addressLanguageCd === Enum.PATIENT_ADDRESS_CHINESE_LANGUAGE : false;
    const resChiAddress = res_address ? res_address.addressLanguageCd === Enum.PATIENT_ADDRESS_CHINESE_LANGUAGE : false;

    const postOfficeValids = this.postOfficeValidFilter(patientContactInfo.communicationMeansCd);
    const postOfficeValidator = postOfficeValids.validator, postOfficeErrMsg = postOfficeValids.errorMsg;

    const id = 'registration_contactInformation';
    return (
      <Grid container className={classes.root}>
        <Grid container className={classes.grid}>
          <Grid item xs={6}>
            <OtherPhonesField
                id={id + '_phoneField'}
                comDisabled={comDisabled}
                phoneCountryList={countryList}
                otherPhones={phoneList}
                onChange={this.handleChangeOtherPhones}
            />
          </Grid>
          <Grid item container xs={4}>
            <Grid item container className={classes.paddingRightGrid}>
              <DelayInput
                  id={id + '_email'}
                  validByBlur
                  msgPosition="bottom"
                  disabled={comDisabled}
                  inputProps={{ maxLength: 80 }}
                  value={patientContactInfo.emailAddress}
                  onChange={e => this.handleChangeInfo(e.target.value, 'emailAddress')}
                  label={<>{fieldLabel.CONTACT_EMAIL}{patientContactInfo.communicationMeansCd.indexOf(Enum.CONTACT_MEAN_EMAIL) > -1 ? <RequiredIcon /> : null}</>}
                  variant="outlined"
                  type="email"
                  validators={patientContactInfo.communicationMeansCd.indexOf(Enum.CONTACT_MEAN_EMAIL) > -1 ? [ValidatorEnum.required, ValidatorEnum.isEmail] : [ValidatorEnum.isEmail]}
                  errorMessages={patientContactInfo.communicationMeansCd.indexOf(Enum.CONTACT_MEAN_EMAIL) > -1 ? [CommonMessage.VALIDATION_NOTE_REQUIRED(), CommonMessage.VALIDATION_NOTE_EMAILFIELD()] : [CommonMessage.VALIDATION_NOTE_EMAILFIELD()]}
                  ref="email"
              />
            </Grid>
            <Grid item container className={classes.paddingRightGrid}>
              <OutlinedCheckBoxValidator
                  id={id + '_contactMean'}
                  labelText="Communication Means"
                  value={patientContactInfo.communicationMeansCd}
                  onChange={(e, cbList) => {
                  let means = '';
                  for (let i = 0; i < cbList.length; i++) {
                    if (cbList[i].checked) {
                      means += cbList[i].value;
                    }
                  }
                  this.handleChangeInfo(means, 'communicationMeansCd');
                }}
                  list={Enum.CONTACT_MEAN_LIST.map(item => ({ label: item.engDesc, value: item.code }))}
                  FormGroupProps={{ className: classes.radioGroup }}
                  disabled={comDisabled}
              />
            </Grid>
            <Grid item container className={classes.paddingRightGrid}>
              <OutlinedRadioValidator
                  id={id + '_languagePreferred'}
                  name="languagePreferred"
                  labelText="Communication Language"
                  isRequired
                  value={patientContactInfo.communicateLangCd}
                  onChange={e => this.handleChangeInfo(e.target.value, 'communicateLangCd')}
                  list={Enum.LANGUAGE_LIST.map(item => ({ label: item.engDesc, value: item.code }))}
                  RadioGroupProps={{ className: classes.radioGroup }}
                  disabled={comDisabled}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid container className={classes.grid}>
          <Grid item xs={10}>
            <StyledExpansionPanel square expanded={this.state.expanded === 'panel1'} onChange={() => this.handleExpanChange('panel1')}>
              <StyledExpansionPanelSummary
                  aria-controls="panel1d-content"
                  id={id + 'panel1d-header'}
              >
                <IconButton
                    id={id + '_correspondenceAddress_btn'}
                    className={classes.close_icon}
                    color={'primary'}
                    fontSize="small"
                >
                  {this.state.expanded === 'panel1' ? <Remove /> : <Add />}
                  <b>Correspondence Address</b>
                </IconButton>
              </StyledExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Grid container spacing={1}>
                  <Grid item xs={4}>
                    <FormControlLabel
                        id={id + '_chineseAddress_radioLabel1'}
                        style={{ textAlign: 'center' }}
                        checked={corChiAddress}
                        onChange={e => this.handleChangeAddress(e, Enum.PATIENT_CORRESPONDENCE_ADDRESS_TYPE, 'addressLanguageCd')}
                        label="Chinese Address"
                        disabled={comDisabled}
                        value={Enum.PATIENT_ADDRESS_CHINESE_LANGUAGE}
                        control={
                        <CIMSCheckBox
                            color="primary"
                            id={id + '_chineseAddress_radio1'}
                            classes={{ root: classes.radioBtn }}
                        />
                      }
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <CIMSButton
                        id="btn_cor_address_lookup"
                        onClick={() => {
                          this.props.openCommonCircularDialog();
                          this.onClickSearchBtn(Enum.PATIENT_CORRESPONDENCE_ADDRESS_TYPE, registerCodeList);
                        }}
                        disabled={comDisabled}
                    >
                      Address Lookup
                    </CIMSButton>
                  </Grid>
                  <Grid item xs={4}></Grid>
                  {
                    addressInputs.map(item => {
                      return (
                        <Grid item xs={4} key={item.name}>
                          <DelayInput
                              id={id + '_cor_' + item.name}
                              disabled={comDisabled}
                              inputProps={{ maxLength: item.maxLength }}
                              value={cor_address ? cor_address[item.name] : ''}
                              onChange={e => this.handleChangeAddress(e.target.value, Enum.PATIENT_CORRESPONDENCE_ADDRESS_TYPE, item.name)}
                              name={item.name}
                            // labelText={item.label}
                              label={item.label}
                            // InputLabelProps={{ shrink: true }}
                              variant="outlined"
                          />
                        </Grid>
                      );
                    })
                  }
                  <Grid item xs={4}>
                    <SelectFieldValidator
                        id={id + '_cor_districtCd'}
                        options={
                        registerCodeList &&
                        registerCodeList.district &&
                        registerCodeList.district.map((item) => (
                          { value: item.code, label: item.engDesc }))}
                        value={cor_address ? cor_address.districtCd : ''}
                        onChange={e => this.handleChangeAddress(e.value, Enum.PATIENT_CORRESPONDENCE_ADDRESS_TYPE, RegFieldName.CONTACT_CORRESPONDENCE_DISTRICT)}
                      // labelText={fieldLabel.CONTACT_CORRESPONDENCE_DISTRICT}
                        TextFieldProps={{
                        variant: 'outlined',
                        label: fieldLabel.CONTACT_CORRESPONDENCE_DISTRICT
                        // InputLabelProps: { shrink: true }
                      }}
                        isDisabled={this.props.comDisabled}
                        addNullOption
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <SelectFieldValidator
                        id={id + '_cor_subDistrictCd'}
                        options={
                        registerCodeList &&
                        registerCodeList.sub_district &&
                        registerCodeList.sub_district.map((item) => (
                          { value: item.code, label: item.engDesc }))}
                        value={cor_address ? cor_address.subDistrictCd : ''}
                        onChange={e => this.handleChangeAddress(e.value, Enum.PATIENT_CORRESPONDENCE_ADDRESS_TYPE, RegFieldName.CONTACT_CORRESPONDENCE_SUB_DISTRICT)}
                      // labelText={fieldLabel.CONTACT_CORRESPONDENCE_SUB_DISTRICT}
                        TextFieldProps={{
                        variant: 'outlined',
                        label: fieldLabel.CONTACT_CORRESPONDENCE_SUB_DISTRICT
                        // InputLabelProps: { shrink: true }
                      }}
                        isDisabled={comDisabled}
                        addNullOption
                    />
                  </Grid>
                </Grid>
              </ExpansionPanelDetails>
            </StyledExpansionPanel>
            <StyledExpansionPanel square expanded={this.state.expanded === 'panel2'} onChange={() => this.handleExpanChange('panel2')}>
              <StyledExpansionPanelSummary
                  aria-controls="panel2d-content"
                  id={id + 'panel2d-header'}
              >
                <IconButton
                    id={id + '_residentialAddress_btn'}
                    className={classes.close_icon}
                    color={'primary'}
                    fontSize="small"
                >
                  {this.state.expanded === 'panel2' ? <Remove /> : <Add />}
                  <b>Residential Address</b>
                </IconButton>
              </StyledExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Grid container spacing={1}>
                  <Grid item xs={4}>
                    <FormControlLabel
                        id={id + '_chineseAddress_radioLabel2'}
                        disabled={comDisabled}
                        label="Chinese Address"
                        checked={resChiAddress}
                        value={Enum.PATIENT_ADDRESS_CHINESE_LANGUAGE}
                        onChange={e => this.handleChangeAddress(e, Enum.PATIENT_RESIDENTIAL_ADDRESS_TYPE, 'addressLanguageCd')}
                        control={
                        <CIMSCheckBox color="primary"
                            id={id + '_chineseAddress_radio2'}
                            classes={{ root: classes.radioBtn }}
                        />
                      }
                    />
                    <FormControlLabel
                        id={id + '_saveAsAbove_radioLabel'}
                        disabled={comDisabled}
                        label="Same As Above"
                        checked={this.props.contactInfoSaveAbove}
                        onChange={(...arg) => this.copyAddressFromCorToRes(...arg)}
                        control={
                        <CIMSCheckBox
                            color="primary"
                            id={id + '_saveAsAbove_radio'}
                            classes={{ root: classes.radioBtn }}
                        />
                      }
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <CIMSButton
                        id="btn_res_address_lookup"
                        onClick={() => {
                          this.props.openCommonCircularDialog();
                          this.onClickSearchBtn(Enum.PATIENT_RESIDENTIAL_ADDRESS_TYPE, registerCodeList);
                        }}
                        disabled={comDisabled}
                    >
                      Address Lookup
                    </CIMSButton>
                  </Grid>
                  <Grid item xs={4}></Grid>
                  {
                    addressInputs.map(item => {
                      return (
                        <Grid item xs={4} key={item.name}>
                          <DelayInput
                              id={id + '_res_' + item.name}
                              fullWidth
                              inputProps={{ maxLength: item.maxLength }}
                              disabled={comDisabled}
                              value={res_address ? res_address[item.name] : ''}
                              onChange={e => this.handleChangeAddress(e.target.value, Enum.PATIENT_RESIDENTIAL_ADDRESS_TYPE, item.name)}
                              name={item.name}
                            // labelText={item.label}
                              label={item.label}
                            // InputLabelProps={{ shrink: true }}
                              variant="outlined"
                          />
                        </Grid>
                      );
                    })
                  }
                  <Grid item xs={4}>
                    <SelectFieldValidator
                        id={id + '_res_districtCd'}
                        options={
                        registerCodeList &&
                        registerCodeList.district &&
                        registerCodeList.district.map((item) => (
                          { value: item.code, label: item.engDesc }))}
                        value={res_address ? res_address.districtCd : ''}
                        onChange={e => this.handleChangeAddress(e.value, Enum.PATIENT_RESIDENTIAL_ADDRESS_TYPE, RegFieldName.CONTACT_RESIDENTIAL_DISTRICT)}
                      // labelText={fieldLabel.CONTACT_RESIDENTIAL_DISTRICT}
                        TextFieldProps={{
                        variant: 'outlined',
                        label: fieldLabel.CONTACT_RESIDENTIAL_DISTRICT
                        // InputLabelProps: { shrink: true }
                      }}
                        isDisabled={comDisabled}
                        addNullOption
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <SelectFieldValidator
                        id={id + '_res_subDistrictCd'}
                        options={
                        registerCodeList &&
                        registerCodeList.sub_district &&
                        registerCodeList.sub_district.map((item) => (
                          { value: item.code, label: item.engDesc }))}
                        value={res_address ? res_address.subDistrictCd : ''}
                        onChange={e => this.handleChangeAddress(e.value, Enum.PATIENT_RESIDENTIAL_ADDRESS_TYPE, RegFieldName.CONTACT_RESIDENTIAL_SUB_DISTRICT)}
                      // labelText={fieldLabel.CONTACT_RESIDENTIAL_SUB_DISTRICT}
                        TextFieldProps={{
                        variant: 'outlined',
                        label: fieldLabel.CONTACT_RESIDENTIAL_SUB_DISTRICT
                        // InputLabelProps: { shrink: true }
                      }}
                        isDisabled={comDisabled}
                        addNullOption
                    />
                  </Grid>
                </Grid>
              </ExpansionPanelDetails>
            </StyledExpansionPanel>
            <StyledExpansionPanel square expanded={this.state.expanded === 'panel3'} onChange={() => this.handleExpanChange('panel3')}>
              <StyledExpansionPanelSummary
                  aria-controls="panel3d-content"
                  id={id + 'panel3d-header'}
              >
                <IconButton
                    id={id + '_postalBox_btn'}
                    className={classes.close_icon}
                    color={'primary'}
                    fontSize="small"
                >
                  {this.state.expanded === 'panel3' ? <Remove /> : <Add />}
                  <b>PostalBox</b>
                </IconButton>
              </StyledExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Grid container spacing={1}>
                  <Grid item xs={4}>
                    <DelayInput
                        id={id + '_postOfficeBoxNo'}
                        disabled={comDisabled}
                        inputProps={{ maxLength: 20 }}
                        label={<>{fieldLabel.CONTACT_POSTOFFICE_BOXNO}{patientContactInfo.communicationMeansCd.indexOf(Enum.CONTACT_MEAN_POSTALMAIL) > -1 ? <RequiredIcon /> : null}</>}
                        variant="outlined"
                        msgPosition="bottom"
                        value={patientContactInfo.postOfficeBoxNo}
                        onChange={e => this.handleChangeInfo(e.target.value, 'postOfficeBoxNo')}
                        name={RegFieldName.CONTACT_POSTOFFICE_BOXNO}
                        validators={postOfficeValidator}
                        errorMessages={postOfficeErrMsg}
                        ref="postOfficeBoxNo"
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <DelayInput
                        id={id + '_postOfficeName'}
                        disabled={comDisabled}
                        inputProps={{ maxLength: 100 }} label={<>{fieldLabel.CONTACT_POSTOFFICE_NAME}{patientContactInfo.communicationMeansCd.indexOf(Enum.CONTACT_MEAN_POSTALMAIL) > -1 ? <RequiredIcon /> : null}</>}
                        variant="outlined"
                        msgPosition="bottom"
                        value={patientContactInfo.postOfficeName}
                        onChange={e => this.handleChangeInfo(e.target.value, 'postOfficeName')}
                        name={RegFieldName.CONTACT_POSTOFFICE_NAME}
                        validators={postOfficeValidator}
                        errorMessages={postOfficeErrMsg}
                        ref="postOfficeName"
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <DelayInput
                        id={id + '_postOfficeRegion'}
                        disabled={comDisabled}
                        inputProps={{ maxLength: 50 }} label={<>{fieldLabel.CONTACT_POSTOFFICE_REGION}{patientContactInfo.communicationMeansCd.indexOf(Enum.CONTACT_MEAN_POSTALMAIL) > -1 ? <RequiredIcon /> : null}</>}
                        variant="outlined"
                        msgPosition="bottom"
                        value={patientContactInfo.postOfficeRegion}
                        onChange={e => this.handleChangeInfo(e.target.value, 'postOfficeRegion')}
                        name={RegFieldName.CONTACT_POSTOFFICE_REGION}
                        validators={postOfficeValidator}
                        errorMessages={postOfficeErrMsg}
                        ref="postOfficeRegion"
                    />
                  </Grid>
                </Grid>
              </ExpansionPanelDetails>
            </StyledExpansionPanel>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

const StyledExpansionPanel = withStyles({
  root: {
    width: '100%',
    '&$expanded': {
      margin: '5px 0px'
    }
  },
  expanded: {}
})(ExpansionPanel);

const StyledExpansionPanelSummary = withStyles({
  root: {
    minHeight: 30,
    '&$expanded': {
      minHeight: 30
    }
  },
  expanded: {},
  content: {
    margin: 0,
    '&$expanded': {
      margin: 0
    }
  }
})(ExpansionPanelSummary);

const style = () => ({
  root: {
    paddingTop: 10
  },
  grid: {
    justifyContent: 'center'
  },
  paddingRightGrid: {
    paddingBottom: 10
  },
  close_icon: {
    padding: 0,
    marginRight: 10,
    borderRadius: '0%'
  },
  add_icon: {
    verticalAlign: 'bottom'
  },
  expansionPanel: {
    width: '100%'
  },
  radioGroup: {
    justifyContent: 'flex-end'
  }
});
function mapStateToProps(state) {
  return {
    registerCodeList: state.registration.codeList,
    contactInfoSaveAbove: state.registration.contactInfoSaveAbove,
    patientById: state.registration.patientById,
    phoneList: state.registration.phoneList,
    addressList: state.registration.addressList,
    patientContactInfo: state.registration.patientContactInfo,
    countryList: state.patient.countryList || []
  };
}
const dispatchProps = {
  updateState,
  openCommonCircularDialog,
  closeCommonCircularDialog
};
export default connect(mapStateToProps, dispatchProps)(withStyles(style)(ContactInformation));
