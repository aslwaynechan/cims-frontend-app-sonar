import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import CIMSInputLabel from '../../../components/InputLabel/CIMSInputLabel';
import CIMSTextField from '../../../components/TextField/CIMSTextField';
import SickLeaveDateRange from './component/sickLeaveDateRange';
import SelectFieldValidator from '../../../components/FormValidator/SelectFieldValidator';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import CommonMessage from '../../../constants/commonMessage';
import CommonRegex from '../../../constants/commonRegex';
import { resetAll } from '../../../store/actions/certificate/sickLeave/sickLeaveAction';
import { deleteSubTabsByOtherWay, updateCurTab } from '../../../store/actions/mainFrame/mainFrameAction';
import accessRightEnum from '../../../enums/accessRightEnum';
import EditModeMiddleware from '../../compontent/editModeMiddleware';
import * as CommonUtilities from '../../../utilities/commonUtilities';
import moment from 'moment';
import CIMSButtonGroup from '../../../components/Buttons/CIMSButtonGroup';
import { openCommonMessage } from '../../../store/actions/message/messageAction';

const styles = () => ({
    marginGrid: {
        margin: '0px 20px 5px'
    },
    errorFieldNameText: {
        fontSize: '16px',
        wordBreak: 'break-word',
        color: '#fd0000'
    }
});

class newSickLeave extends Component {
    state = {
        isEdit: false,
        errorMessageList: [],
        showAreaTextError: {
            sufferFrom: false
        }
    };

    componentDidMount() {
        let tempLeaveInfo = { ...this.props.newLeaveInfo };
        let tempSection = CommonUtilities.matchSection(moment(), 'H');
        let tempPeriod = 1;
        if (tempSection === 'PM') {
            tempPeriod = 0.5;
        }
        tempLeaveInfo.dateRange.section = tempSection;
        tempLeaveInfo.dateRange.period = tempPeriod;
        this.props.handleOnChange({ newLeaveInfo: tempLeaveInfo });
        this.props.updateCurTab('F032', this.doClose);
    }

    componentWillUnmount() {
        this.props.resetAll();
    }

    doClose = (callback) => {
        if (this.state.isEdit) {
            this.props.openCommonMessage({
                msgCode: '110018',
                btnActions: {
                    btn1Click: () => {
                        callback(true);
                    }
                }
            });
        }
        else {
            callback(true);
        }
    }

    handleOnChange = (changes) => {
        this.props.handleOnChange(changes);
        this.setState({ isEdit: true });
    }

    updateLeaveInfo = (value, name) => {
        let leaveInfo = { ...this.props.newLeaveInfo };
        const reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
        if (name === 'to' || name === 'sufferFrom' || name === 'remark') {
            if (reg.test(value)) {
                return;
            }
        }
        leaveInfo[name] = value;
        this.handleOnChange({ newLeaveInfo: leaveInfo });
        this.setState({ isEdit: true });
    }

    updateCopyPage = (value) => {
        this.handleOnChange({ copyPage: value });
        this.setState({ isEdit: true });
    }

    fillErrorBeforePrint = (leaveInfo) => {
        let sufferFrom = leaveInfo.sufferFrom;
        let tempErrorMsgList = this.state.errorMessageList;

        if (sufferFrom === '') {
            let idx = tempErrorMsgList.findIndex(item => item.fieldName === 'Suffer From');
            if (idx === -1) {
                tempErrorMsgList.push({ fieldName: 'Suffer From', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
                this.setState({ showAreaTextError: { sufferFrom: true } });
            }
        }
        return tempErrorMsgList;
    }

    handleSufferFromChange = (value) => {
        let tempErrorMsgList = [];
        let { errorMessageList } = this.state;
        if (value !== '') {
            errorMessageList.forEach(error => {
                if (error.fieldName !== 'Suffer From') {
                    tempErrorMsgList.push(error);
                }
            });
            this.setState({
                errorMessageList: tempErrorMsgList,
                showAreaTextError: {
                    sufferFrom: false
                }
            });
        }
        this.updateLeaveInfo(value.toUpperCase(), 'sufferFrom');
        this.setState({ isEdit: true });
    }

    handleSubmit = () => {
        this.refs.sickLeaveForm.submit();
    }

    preparePrint = () => {
        let leaveInfo = this.props.newLeaveInfo;
        let errorMsgList = this.fillErrorBeforePrint(leaveInfo);
        if (errorMsgList.length > 0) {
            this.setState({ errorMessageList: errorMsgList });
            return;
        }

        if (this.props.handlingPrint) {
            return;
        }

        let params = {
            opType: 'SP',
            toValue: leaveInfo.to,
            diseaseName: leaveInfo.sufferFrom,
            dateFrom: moment(leaveInfo.dateRange.leaveFrom).format('DD-MM-YYYY'),
            dateTo: moment(leaveInfo.dateRange.leaveTo).format('DD-MM-YYYY'),
            dateFromFlag: leaveInfo.dateRange.section === '' ? 'AM' : leaveInfo.dateRange.section,
            dateToFlag: leaveInfo.dateRange.leaveToSection,
            dayNum: leaveInfo.dateRange.period
        };
        this.props.handlePrint(params);
        this.setState({ isEdit: false });
    }

    handleCancel = () => {
        this.props.deleteSubTabsByOtherWay({
            editModeTabs: this.props.editModeTabs,
            name: accessRightEnum.sickLeaveCertificate
        });
    }

    dateRangeValidatorListener = (isValid, message, name) => {
        let { errorMessageList } = this.state;
        let tempErrorMsgList = [];
        if (!isValid) {
            tempErrorMsgList = errorMessageList;
            let index = tempErrorMsgList.findIndex(error => error.fieldName === name);
            if (index === -1) {
                tempErrorMsgList.push({
                    fieldName: name,
                    errMsg: message
                });
            }
            this.setState({ errorMessageList: tempErrorMsgList });
        }
        else {
            errorMessageList.forEach(error => {
                if (error.fieldName !== name) {
                    tempErrorMsgList.push(error);
                }
            });
            this.setState({ errorMessageList: tempErrorMsgList });
        }
    }

    render() {
        const { classes } = this.props;
        const { errorMessageList, showAreaTextError } = this.state;
        let leaveInfo = this.props.newLeaveInfo;
        let allowCopyList = this.props.allowCopyList;
        //leaveInfo.dateRange.section = CommonUtilities.matchSection(moment(), 'H');
        return (
            <ValidatorForm
                id={this.props.id + '_form'}
                ref={'sickLeaveForm'}
                onSubmit={this.preparePrint}
            >
                <Grid container spacing={2}>
                    <Grid item container xs={12} >
                        <Grid item xs={1}><CIMSInputLabel>To:</CIMSInputLabel></Grid>
                        <Grid item xs={11}>
                            <CIMSTextField
                                id={this.props.id + '_txtTo'}
                                value={leaveInfo.to}
                                onChange={e => this.updateLeaveInfo(e.target.value, 'to')}
                                inputProps={{
                                    maxLength: 34
                                }}
                            />
                        </Grid>
                    </Grid>
                    <Grid item container xs={12} >
                        <Grid item xs={1}><CIMSInputLabel>Suffer From:<span style={{ color: 'red' }}>*</span></CIMSInputLabel></Grid>
                        <Grid item xs={11}>
                            <CIMSTextField
                                id={this.props.id + '_sufferFrom'}
                                value={leaveInfo.sufferFrom}
                                onChange={e => this.handleSufferFromChange(e.target.value)}
                                inputProps={{
                                    maxLength: 22
                                }}
                                error={showAreaTextError.sufferFrom}
                            />
                        </Grid>
                    </Grid>
                    <Grid item container xs={12}>
                        <SickLeaveDateRange
                            id={'sickLeaveNewSickLeaveDateRange'}
                            data={leaveInfo.dateRange}
                            onChange={e => { this.updateLeaveInfo(e, 'dateRange'); }}
                            validatorListener={this.dateRangeValidatorListener}
                            resizeUnit={this.props.resizeUnit}
                        />
                    </Grid>
                    <Grid item container xs={12} >
                        <Grid item xs={1}><CIMSInputLabel>Remark:</CIMSInputLabel></Grid>
                        <Grid item xs={11}>
                            <CIMSTextField
                                id={this.props.id + '_reamrk'}
                                value={leaveInfo.remark}
                                onChange={e => this.updateLeaveInfo(e.target.value, 'remark')}
                                inputProps={{
                                    maxLength: 50
                                }}
                            />
                        </Grid>
                    </Grid>
                    {
                        errorMessageList.length > 0 ?
                            <Grid item container xs={12}>
                                <Grid item xs={1}></Grid>
                                <Grid item xs={11} >
                                    {
                                        errorMessageList && errorMessageList.length > 0 ?
                                            <Grid item container >
                                                {
                                                    errorMessageList.map((item, i) => (
                                                        <Grid item container key={i} justify="flex-start" id={`newRefferalLetterCert${item.fieldName.replace(' ', '')}ErrorMessage`}>
                                                            <Typography className={classes.errorFieldNameText}>{`${item.fieldName}: ${item.errMsg}`}</Typography>
                                                        </Grid>
                                                    ))
                                                }
                                            </Grid> : null
                                    }
                                </Grid>
                            </Grid>
                            : null
                    }
                    <Grid item container xs={12} >
                        <Grid item xs={1}><CIMSInputLabel>NO. of Copy:</CIMSInputLabel></Grid>
                        <Grid item xs={11}>
                            <Grid container direction={'row'}>
                                <Grid style={{ width: 150 }}>
                                    <SelectFieldValidator
                                        id={this.props.id + '_selectCopyPage'}
                                        value={this.props.copyPage}
                                        options={allowCopyList && allowCopyList.map(item => ({ value: item.value, label: item.desc }))}
                                        onChange={e => this.updateCopyPage(e.value)}
                                    //direction={'top'}
                                    //menuPlacement={'top'}
                                    />
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <CIMSButtonGroup
                        buttonConfig={
                            [
                                {
                                    id: this.props.id + '_btnSaveAndPrint',
                                    name: 'Save & Print',
                                    // style:{ margin: '0 8px' },
                                    onClick: this.handleSubmit
                                },
                                {
                                    id: this.props.id + '_btnCancel',
                                    name: 'Cancel',
                                    onClick: this.handleCancel
                                }
                            ]
                        }
                    />
                    <EditModeMiddleware
                        componentName={accessRightEnum.sickLeaveCertificate}
                        when={this.state.isEdit}
                        saveFunction={this.handleSubmit}
                    />
                </Grid>
            </ValidatorForm>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        newLeaveInfo: state.sickLeave.newLeaveInfo,
        handlingPrint: state.sickLeave.handlingPrint,
        editModeTabs: state.mainFrame.editModeTabs
    };
};

const mapDispatchToProps = {
    resetAll,
    deleteSubTabsByOtherWay,
    updateCurTab,
    openCommonMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(newSickLeave));