import React, { Component } from 'react';
import { Grid, Radio, FormControlLabel, IconButton, RadioGroup } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import DateFieldValidator from '../../../../components/FormValidator/DateFieldValidator';
import ValidatorForm from '../../../../components/FormValidator/ValidatorForm';
import CIMSInputLabel from '../../../../components/InputLabel/CIMSInputLabel';
import TextFieldValidator from '../../../../components/FormValidator/TextFieldValidator';
import { Add, Remove } from '@material-ui/icons';
import moment from 'moment';
import ValidatorEnum from '../../../../enums/validatorEnum';
import CommonMessage from '../../../../constants/commonMessage';

const CustomRadio = withStyles({
    root: {
        padding: 5
    }
})(Radio);

const CustomFormControlLabel = withStyles({
    root: {
        marginLeft: 'unset',
        marginRight: 10
    }
})(FormControlLabel);

const styles = () => ({
    periodContainerRoot: {
        padding: '0px 0px 0px 16px',
        alignItems: 'center'
    },
    itemRoot: {
        marginRight: 3
    },
    iconButtonRoot: {
        borderRadius: '0%',
        padding: '0px',
        margin: '0px,3px'
    }
});

class SickLeaveDateRange extends Component {

    handleChange = (changes) => {
        this.props.onChange(changes);
    }

    addDayPeriod = (period, date) => {
        this.refs.period.validateCurrent();
        if (!this.refs.period.isValid()) {
            return;
        }
        let tempPeriod = Number(period) - 1;
        let newDate = moment();
        // if (period < 1 || period === null) {
        //     return;
        // }
        //let tempPeriod = period - 1;
        if (tempPeriod >= 0.5) {
            newDate = moment(date).add(tempPeriod, 'days');
        }
        else {
            newDate = date;
        }
        return newDate;
    }

    updateDateRange = (value, name) => {
        let rangeData = { ...this.props.data };
        rangeData[name] = value;

        this.handleChange(rangeData);
    }

    handleDateChange = (value, name) => {
        let rangeData = { ...this.props.data };
        let day = 0;
        let tempPer = 0;
        if (name === 'leaveFrom') {
            if (moment(rangeData.leaveTo).isBefore(moment(value))) {
                rangeData.period = '';
                rangeData.leaveTo = value;
            }
            else {
                day = moment(rangeData.leaveTo).diff(value, 'day');
            }

        }
        if (name === 'leaveTo') {
            day = value.diff(rangeData.leaveFrom, 'day');
        }


        rangeData[name] = value;

        tempPer = day + 1;
        if (rangeData.section === 'PM') {
            tempPer = tempPer - 0.5;
        }
        rangeData['period'] = tempPer;
        this.handleChange(rangeData);
    }

    handIconBtnClick = (e, name) => {
        let tempPeriod = Number(this.props.data.period);
        let { leaveFrom } = this.props.data;
        this.refs.period.validateCurrent();
        if (!this.refs.period.isValid()) {
            return;
        }
        else {
            if (name === 'Add') {
                tempPeriod = tempPeriod + 1;
            }
            else if (name === 'Minus') {
                if (tempPeriod > 1) {
                    tempPeriod = tempPeriod - 1;
                }
            }
        }


        if (tempPeriod < 0) {
            tempPeriod = 0;
        }

        let tempLeaveTo = this.addDayPeriod(tempPeriod, leaveFrom);
        let tempRangeData = {
            ...this.props.data,
            period: tempPeriod,
            leaveTo: tempLeaveTo
        };
        this.handleChange(tempRangeData);
    }

    handlePeriodChange = (value) => {
        //eslint-disable-next-line
        let { leaveFrom, leaveTo, section } = this.props.data;
        // let tempLeaveToSection = 'AM';
        // let tempVal = Number(value);
        // let pointIdx = value.indexOf('.');
        // // if(section==='AM'){
        // //     if(pointIdx>0){
        // //         tempLeaveToSection='PM';
        // //     }
        // // }
        // // else if(section==='PM'){
        // //     if(pointIdx===-1){
        // //         temp
        // //     }
        // // }
        // if (section === 'PM') {
        //     if (pointIdx >= 0) {
        //         tempLeaveToSection = 'PM';
        //     }
        //     else if (pointIdx === -1) {
        //         //value = Number(value) + 1;
        //         tempVal = tempVal + 1;
        //     }
        // }
        // let tempLeaveTo = this.addDayPeriod(tempVal, leaveFrom);
        let newProps = this.handleLeaveToSection(leaveFrom, section, value);
        let tempRangeData = {
            ...this.props.data,
            period: value,
            leaveTo: newProps.leaveTo,
            leaveToSection: newProps.leaveToSection
        };
        this.handleChange(tempRangeData);
    }

    periodValidatorListener = (isValid, message, name) => {
        this.props.validatorListener(isValid, message, name);
    }

    handleSectionChange = (value) => {
        let rangeData = { ...this.props.data };
        let tempPeriod = Number(rangeData.period);
        //let tempPeriod=rangeData.period;

        if (value === 'AM') {
            rangeData.period = tempPeriod + 0.5;
        }
        else if (value === 'PM') {
            rangeData.period = tempPeriod - 0.5;
        }

        rangeData.section = value;

        this.handleChange(rangeData);
    }

    handleLeaveToSection = (leaveFrom, leaveFromSection, period) => {
        let tempLeaveToSection = 'AM';
        let tempVal = Number(period);
        let pointIdx = period.indexOf('.');

        if (leaveFromSection === 'PM') {
            if (pointIdx >= 0) {
                tempLeaveToSection = 'PM';
            }
            else if (pointIdx === -1) {
                //value = Number(value) + 1;
                tempVal = tempVal + 1;
            }
        }

        let tempLeaveTo = this.addDayPeriod(tempVal, leaveFrom);

        return { leaveToSection: tempLeaveToSection, leaveTo: tempLeaveTo };
    }

    render() {
        const { classes, resizeUnit } = this.props;
        let data = this.props.data;

        return (
            <Grid style={{ width: '100%' }}>
                {/* <Grid container>
                    <Grid container item xs={2}></Grid>
                        <Grid container item xs={10}  ><CIMSInputLabel>(For half-day leave)</CIMSInputLabel></Grid>
                    </Grid> */}
                <Grid container style={{ paddingBottom: 18 * resizeUnit }}>
                    <Grid container item xs={1}><CIMSInputLabel>Leave From:</CIMSInputLabel></Grid>
                    <Grid container item xs={11} direction={'row'} >
                        <Grid item style={{ width: 180 }}>
                            <DateFieldValidator
                                id={this.props.id + '_FromDate'}
                                value={data.leaveFrom}
                                minDate="1900-01-01"
                                onChange={(e) => { this.handleDateChange(e || moment(), 'leaveFrom'); }}
                            />
                        </Grid>

                        <Grid container item xs={3} direction={'row'} className={classes.periodContainerRoot}>
                            <CIMSInputLabel
                                style={{ fontWeight: 'normal' }}
                                children={'Period'}
                            />
                            <Grid item>
                                <IconButton
                                    className={classes.iconButtonRoot}
                                    color={'primary'}
                                    onClick={e => { this.handIconBtnClick(e, 'Add'); }}
                                >
                                    <Add color="primary" />
                                </IconButton>
                            </Grid>
                            <Grid item style={{ width: 80 }}>
                                <TextFieldValidator
                                    id={this.props.id + '_txtPeriod'}
                                    /* eslint-disable */
                                    InputProps={{
                                        className: 'input_field'
                                    }}
                                    inputProps={{
                                        maxLength: 4
                                    }}
                                    /* eslint-enable */
                                    value={data.period}
                                    onChange={e => { this.handlePeriodChange(e.target.value); }}
                                    validators={[ValidatorEnum.period]}
                                    errorMessages={[CommonMessage.VALIDATION_NOTE_INVALID_PREIOD()]}
                                    validatorListener={(...arg) => this.periodValidatorListener(...arg, 'Period')}
                                    //msgPosition="bottom"
                                    notShowMsg
                                    validByBlur
                                    name={'Period'}
                                    ref={'period'}
                                />
                            </Grid>
                            <Grid item>
                                <IconButton
                                    className={classes.iconButtonRoot}
                                    color={'primary'}
                                    onClick={e => { this.handIconBtnClick(e, 'Minus'); }}
                                >
                                    <Remove color={'primary'} />
                                </IconButton>
                                <CIMSInputLabel
                                    style={{ fontWeight: 'normal' }}
                                    children={'day(s)'}
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={4}>
                            <RadioGroup
                                id={this.props.id + '_sectionRadioGroup'}
                                row
                                value={data.section}
                                onChange={e => { this.handleSectionChange(e.target.value); }}
                            >
                                <Grid item xs={2}><CustomFormControlLabel value="AM" control={<CustomRadio color={'primary'} />} label="AM" /></Grid>
                                <Grid item xs={2}><CustomFormControlLabel value="PM" control={<CustomRadio color={'primary'} />} label="PM" /></Grid>
                            </RadioGroup>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid container style={{ paddingTop: 18 * resizeUnit }}>
                    <Grid item xs={1}><CIMSInputLabel>Leave To:</CIMSInputLabel></Grid>
                    <Grid item xs={11}>
                        <Grid item style={{ width: 180 }}>
                            <DateFieldValidator
                                id={this.props.id + '_ToDate'}
                                value={data.leaveTo}
                                minDate={data.leaveFrom}
                                onChange={(e) => { this.handleDateChange(e || moment(), 'leaveTo'); }}
                            />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(SickLeaveDateRange);