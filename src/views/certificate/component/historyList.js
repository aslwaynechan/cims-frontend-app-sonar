import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import SelectFieldValidator from '../../../components/FormValidator/SelectFieldValidator';
import DateFieldValidator from '../../../components/FormValidator/DateFieldValidator';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import CommonMessage from '../../../constants/commonMessage';
import ValidatorEnum from '../../../enums/validatorEnum';
import RequiredIcon from '../../../components/InputLabel/RequiredIcon';
import * as CommonUtil from '../../../utilities/commonUtilities';
import CIMSList from '../../../components/List/CIMSList';

const sysRatio = CommonUtil.getSystemRatio();
const unit = CommonUtil.getResizeUnit(sysRatio);

const drawerWidth = 480;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        backgroundColor: theme.palette.dialogBackground,
        padding: theme.spacing(1),
        borderRadius: theme.spacing(1) / 2
    },
    hide: {
        display: 'none'
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap'
    },
    drawerPaper: {
        zIndex: 1,
        position: 'relative',
        border: 'unset',
        overflow: 'hidden'
    },
    drawerOpen: {
        width: drawerWidth
    },
    drawerClose: {
        overflowX: 'hidden',
        width: 0
    },
    toolbar: {
        padding: theme.spacing(0, 1)
    },
    title: {
        color: 'white'
    },
    verticalTitle: {
        transition: theme.transitions.create(['transform'], {
            delay: theme.transitions.duration.leavingScreen
        }),
        '-webkit-writing-mode': 'vertical-rl',
        transform: 'rotate(180deg)'
    },
    iconButton: {
        padding: 6
    },
    formContainer: {
        padding: theme.spacing(2)
    },
    list: {
        height: 520 * unit,
        width: '100%',
        overflowY: 'auto'
    },
    listItem: {
        borderTop: `1px solid ${theme.palette.grey[300]}`,
        display: 'flex',
        flexDirection: 'column'
    }
}));

export default function HistoryList(props) {
    const classes = useStyles();
    const {
        id,
        serviceCd,
        dateFrom,
        dateTo,
        serviceList,
        onChange,
        onBlur,
        onListItemClick,
        open,
        disabled,
        selectedIndex,
        data,
        renderChild
    } = props;
    let comId = `${id}_attendanceCert_historyList`;

    const handleOnChange = (value, name) => {
        onChange && onChange(value, name);
    };

    const handleOnBlur = (value, name) => {
        onBlur && onBlur(value, name);
    };

    const handleListItemClick = (value, rowData) => {
        onListItemClick && onListItemClick(value, rowData);
    };

    return (
        <Grid container className={classes.root}>
            <Grid
                item
                container
                className={classes.toolbar}
                direction={open ? 'row' : 'column-reverse'}
                justify={open ? 'space-between' : 'flex-end'}
            >
                <Typography
                    variant="h6"
                    className={clsx(classes.title, {
                        [classes.verticalTitle]: !open
                    })}
                >History</Typography>
                <IconButton
                    className={classes.iconButton}
                    onClick={() => handleOnChange(!open, 'open')}
                >
                    {open ? <ChevronLeftIcon htmlColor="white" /> : <MenuIcon htmlColor="white" />}
                </IconButton>
            </Grid>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open
                })}
                classes={{
                    paper: clsx(classes.drawerPaper)
                }}
            >
                <ValidatorForm>
                    <Grid container spacing={2} className={classes.formContainer}>
                        <Grid item container>
                            <SelectFieldValidator
                                id={`${comId}_serviceCd`}
                                addAllOption
                                value={serviceCd}
                                options={serviceList && serviceList.map((item) => (
                                    { value: item.serviceCd, label: `${item.serviceCd} - ${item.serviceName}` }
                                ))}
                                onChange={e => handleOnChange(e.value, 'serviceCd')}
                                validators={[ValidatorEnum.required]}
                                errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                                TextFieldProps={{
                                    label: <>Service<RequiredIcon /></>
                                }}
                                isDisabled={disabled}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <DateFieldValidator
                                id={`${comId}_dateFrom`}
                                label={<>Date From<RequiredIcon /></>}
                                disabled={disabled}
                                onChange={e => handleOnChange(e, 'dateFrom')}
                                onBlur={e => handleOnBlur(e, 'dateFrom')}
                                onAccept={e => handleOnBlur(e, 'dateFrom')}
                                value={dateFrom}
                                isRequired
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <DateFieldValidator
                                id={`${comId}_dateTo`}
                                label={<>Date To<RequiredIcon /></>}
                                disabled={disabled}
                                onChange={e => handleOnChange(e, 'dateTo')}
                                onBlur={e => handleOnBlur(e, 'dateTo')}
                                onAccept={e => handleOnBlur(e, 'dateTo')}
                                value={dateTo}
                                isRequired
                            />
                        </Grid>
                        <Grid item container>
                            <CIMSList
                                id={`${comId}_list`}
                                data={data}
                                ListItemProps={{
                                    button: true
                                }}
                                classes={{
                                    list: classes.list,
                                    listItem: classes.listItem
                                }}
                                selectedIndex={selectedIndex}
                                onListItemClick={(event, index) => handleListItemClick(index, data[index])}
                                renderChild={renderChild}
                            />
                        </Grid>
                    </Grid>
                </ValidatorForm>
            </Drawer>
        </Grid>
    );
}
