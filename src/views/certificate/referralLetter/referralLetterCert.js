import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Tabs, Tab, Typography } from '@material-ui/core';
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import moment from 'moment';
import NewReferralLetter from './newReferralLetter';
import {
    updateField,
    getReferralLetter
} from '../../../store/actions/certificate/referralLetter/referralLetterAction';
import * as CommonUtilities from '../../../utilities/commonUtilities';

const styles = () => ({
    root: {
        height: '100%',
        display: 'flex',
        flexFlow: 'column'
    },
    tabs: {
        minHeight: 'unset'
    },
    tab: {
        minWidth: 72,
        minHeight: 20,
        padding: 0
    },
    tabTypography: {
        fontSize: 16,
        textTransform: 'none'
    },
    container: {
        // flex: 1,
        // paddingTop: 5,
        // boxSizing: 'border-box',
        borderTop: '1px solid #ccc'
        // overflow: 'auto'
    },
    NewReferralLetterGrid: {
        height: '100%'
    }
});

const sysRatio = CommonUtilities.getSystemRatio();
const unit = CommonUtilities.getResizeUnit(sysRatio);

const customTheme = (theme) => createMuiTheme({
    ...theme,
    spacing: (18 * unit)
});

class ReferralLetterCert extends Component {
    state = {
        tabValue: 0,
        attendanceTo: '',
        attendanceDate: moment(),
        attendanceSection: 'AM',
        attendanceSectionFrom: moment(),
        attendanceSectionTo: moment(),
        attendanceFor: 'Allied Health',
        attendanceForOthers: '',
        attendanceRemark: ''
    }

    componentDidMount() {
        this.props.ensureDidMount();
    }

    changeTabValue = (event, value) => {
        this.setState({ tabValue: value });
    }

    handleOnChange = (changes) => {
        this.props.updateField(changes);
    }

    handlePrint = (params) => {
        let copyPage = this.props.copyPage;
        params.patientKey = this.props.patientInfo.patientKey;
        this.props.updateField({ handlingPrint: true });
        this.props.getReferralLetter(
            params,
            (printSuccess) => {
                this.props.updateField({ handlingPrint: false });
                if (printSuccess) {
                    console.log('Print Success');
                }
            },
            copyPage);
    }

    render() {
        const { classes } = this.props;
        return (
            <MuiThemeProvider theme={customTheme}>
                <Grid container>
                    <Tabs
                        value={this.state.tabValue}
                        onChange={this.changeTabValue}
                        indicatorColor={'primary'}
                        className={classes.tabs}
                    >
                        <Tab id={'referralLetterCert_tabNew'} className={classes.tab} label={<Typography className={classes.tabTypography}>New</Typography>} />
                        <Tab id={'referralLetterCert_tabHistory'} className={classes.tab} disabled label={<Typography className={classes.tabTypography}>History</Typography>} />
                    </Tabs>
                    <Grid item container xs={12} className={classes.container}>
                        <Grid item xs={12} hidden={this.state.tabValue !== 0} style={{ margin: '0px 20px 0px 20px' }}>
                            <NewReferralLetter
                                id={'referralLetterCertNewReferralLetter'}
                                allowCopyList={this.props.allowCopyList}
                                handleOnChange={this.handleOnChange}
                                copyPage={this.props.copyPage}
                                handlePrint={this.handlePrint}
                            />
                        </Grid>
                        <Grid item xs={12} hidden={this.state.tabValue !== 1}>
                            <Grid>
                                History
                        </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </MuiThemeProvider>
        );
    }
}

const stateToProps = (state) => {
    return {
        patientInfo: state.patient.patientInfo,
        reportData: state.attendanceCert.reportData,
        allowCopyList: state.referralLetter.allowCopyList,
        copyPage: state.referralLetter.copyPage
    };
};

const dispatchToProps = {
    updateField,
    getReferralLetter
};

export default connect(stateToProps, dispatchToProps)(withStyles(styles)(ReferralLetterCert));