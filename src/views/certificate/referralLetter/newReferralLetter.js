import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import CIMSInputLabel from '../../../components/InputLabel/CIMSInputLabel';
import CIMSMultiTextField from '../../../components/TextField/CIMSMultiTextField';
import CIMSTextField from '../../../components/TextField/CIMSTextField';
import SelectFieldValidator from '../../../components/FormValidator/SelectFieldValidator';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import CommonMessage from '../../../constants/commonMessage';
import CommonRegex from '../../../constants/commonRegex';
import AppointmentTypeGroup from './component/appointmentTypeGroup';
import ReferToGroup from './component/referToGroup';
import moment from 'moment';
import {
    resetAll
} from '../../../store/actions/certificate/referralLetter/referralLetterAction';
import { deleteSubTabsByOtherWay, updateCurTab } from '../../../store/actions/mainFrame/mainFrameAction';
import accessRightEnum from '../../../enums/accessRightEnum';
import EditModeMiddleware from '../../compontent/editModeMiddleware';
import { listHospitalClinic } from '../../../store/actions/common/commonAction';
import CIMSButtonGroup from '../../../components/Buttons/CIMSButtonGroup';
import { openCommonMessage } from '../../../store/actions/message/messageAction';
import Enum from '../../../enums/enum';


const styles = () => ({
    root: {
        height: '100%'
    },
    marginGrid: {
        display: 'flex',
        margin: '0px 20px 5px'
    },
    errorFieldNameText: {
        fontSize: '16px',
        wordBreak: 'break-word',
        color: '#fd0000'
    }
});


class ReferralLetter extends Component {

    state = {
        isEdit: false,
        errorMessageList: [],
        showInputError: {
            group: false,
            hospitalAndClinic: false,
            specialty: false
            //exemptionReason: false
        }
    };

    componentDidMount() {
        this.props.listHospitalClinic();
        this.props.updateCurTab('F033', this.doClose);
    }

    componentWillUnmount() {
        this.props.resetAll();
    }

    doClose = (callback) => {
        if (this.state.isEdit) {
            this.props.openCommonMessage({
                msgCode: '110018',
                btnActions: {
                    btn1Click: () => {
                        callback(true);
                    }
                }
            });
        }
        else {
            callback(true);
        }
    }

    handleOnChange = (changes) => {
        this.props.handleOnChange(changes);
        this.setState({ isEdit: true });
    }

    updateLetterInfo = (value, name) => {
        let letterInfo = { ...this.props.newLetterInfo };
        const reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
        if (name === 'to' || name === 'problem' || name === 'result') {
            if (reg.test(value)) {
                return;
            }
        }

        letterInfo[name] = value;
        this.handleOnChange({ newLetterInfo: letterInfo });
        this.setState({ isEdit: true });
    }

    updateCopyPage = (value) => {
        this.handleOnChange({ copyPage: value });
        this.setState({ isEdit: true });
    }

    fillErrorBeforePrint = (letterInfo) => {
        let referTo = letterInfo.referTo;
        let tempErrorMsgList = [];
        let tempShowInputError = this.state.showInputError;

        if (referTo.groupCd === null) {
            tempErrorMsgList.push({ fieldName: 'Group', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            tempShowInputError.group = true;
        }

        if (referTo.hosptialClinicName === null) {
            tempErrorMsgList.push({ fieldName: 'Hospital / Clinic', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            tempShowInputError.hospitalAndClinic = true;
        }

        if (referTo.specialty === null && referTo.groupCd === 'HA') {
            tempErrorMsgList.push({ fieldName: 'Specitalty', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            tempShowInputError.specialty = true;
        }

        this.setState({ showInputError: tempShowInputError });
        return tempErrorMsgList;
    }

    preparePrint = () => {
        let letterInfo = this.props.newLetterInfo;
        let errorMsgList = this.fillErrorBeforePrint(letterInfo);

        if (errorMsgList.length > 0) {
            this.setState({ errorMessageList: errorMsgList });
            return;
        }

        if (this.props.handlingPrint) {
            return;
        }

        let referToClinic = letterInfo.referTo.hosptialClinicName;

        if (letterInfo.referTo.groupCd === 'HA') {
            let specialty = this.props.hospitalClinicList.specialtyDtos.find(item => item.specialtyId === letterInfo.referTo.specialty);
            referToClinic = referToClinic + `, ${specialty.specialtyName}`;
        }

        let params = {
            opType: 'SP',
            appointmentType: letterInfo.appointmentType,
            problem: letterInfo.problem,
            results: letterInfo.result,
            serviceClinic: referToClinic,
            date: moment().format(Enum.DATE_FORMAT_EYMD_VALUE).toString(),
            to: letterInfo.to

        };
        this.props.handlePrint(params);
        this.setState({ isEdit: false });
    }

    handleRefferToChange = (referTo) => {
        let { groupCd, hosptialClinicName, specialty } = referTo;
        let { errorMessageList } = this.state;
        let tempErrorList = [];
        if (errorMessageList.length > 0) {
            let groupErrObj = errorMessageList.find(item => item.fieldName === 'Group');
            let hospitalClinicErrObj = errorMessageList.find(item => item.fieldName === 'Hospital / Clinic');
            let specialtyErrObj = errorMessageList.find(item => item.fieldName === 'Specitalty');
            if (groupCd === null) {
                if (groupErrObj) {
                    tempErrorList.push(groupErrObj);
                }
            }

            if (hosptialClinicName === null) {
                if (hospitalClinicErrObj) {
                    tempErrorList.push(hospitalClinicErrObj);
                }
            }

            if (specialty === null && groupCd === 'HA') {
                if (specialtyErrObj) {
                    tempErrorList.push(specialtyErrObj);
                }
            }

            this.setState({ errorMessageList: tempErrorList });
        }
        this.updateLetterInfo(referTo, 'referTo');
        this.setState({ isEdit: true });
    }

    handleCancel = () => {
        this.props.deleteSubTabsByOtherWay({
            editModeTabs: this.props.editModeTabs,
            name: accessRightEnum.referralLetter
        });
    }

    updateErrorField = (name) => {
        let { showInputError } = this.state;

        showInputError[name] = false;

        this.setState({ showInputError, isEdit: true });
    }

    render() {
        const { classes } = this.props;
        let { errorMessageList } = this.state;
        let letterInfo = this.props.newLetterInfo;
        let allowCopyList = this.props.allowCopyList;

        return (
            <Grid container spacing={2}>
                <Grid item container xs={12} >
                    <Grid item xs={1}><CIMSInputLabel>Refer To:<span style={{ color: 'red' }}>*</span></CIMSInputLabel></Grid>
                    <Grid item xs={6}>
                        <ReferToGroup
                            id={this.props.id + '_referToGroup'}
                            referTo={letterInfo.referTo}
                            onChange={(value) => { this.handleRefferToChange(value); }}
                            hospitalClinicList={this.props.hospitalClinicList}
                            error={this.state.showInputError}
                            errorFieldOnChange={this.updateErrorField}
                        />
                    </Grid>
                </Grid>
                <Grid item container xs={12} >
                    <Grid item xs={1}><CIMSInputLabel>To:</CIMSInputLabel></Grid>
                    <Grid item xs={3}>
                        <CIMSTextField
                            id={this.props.id + '_txtTo'}
                            value={letterInfo.to}
                            onChange={e => this.updateLetterInfo(e.target.value, 'to')}
                            inputProps={{
                                maxLength: 34
                            }}
                        />
                    </Grid>
                    <Grid item xs={1} />
                    <Grid item xs={2}><CIMSInputLabel>Appointment Type:</CIMSInputLabel></Grid>
                    <Grid item xs={5}>
                        <AppointmentTypeGroup
                            //id={this.props.id}
                            appointmentType={letterInfo.appointmentType}
                            onChange={(value) => { this.updateLetterInfo(value, 'appointmentType'); }}
                        />
                    </Grid>

                </Grid>
                <Grid item container xs={12} >
                    <Grid item xs={1}><CIMSInputLabel>Problem:</CIMSInputLabel></Grid>
                    <Grid item xs={11}>
                        <CIMSMultiTextField
                            rows="2"
                            id={this.props.id + '_problem'}
                            value={letterInfo.problem}
                            inputProps={{
                                maxLength: 500
                            }}
                            style={{ height: 90 }}
                            onChange={e => this.updateLetterInfo(e.target.value, 'problem')}
                        />
                    </Grid>
                </Grid>
                <Grid item container xs={12} >
                    <Grid item xs={1}><CIMSInputLabel>Result:</CIMSInputLabel></Grid>
                    <Grid item xs={11}>
                        <CIMSMultiTextField
                            rows="2"
                            id={this.props.id + '_result'}
                            value={letterInfo.result}
                            inputProps={{
                                maxLength: 500
                            }}
                            style={{ height: 90 }}
                            onChange={e => this.updateLetterInfo(e.target.value, 'result')}
                        />
                    </Grid>
                </Grid>
                {
                    errorMessageList.length > 0 ?
                        <Grid item container xs={12}>
                            <Grid item xs={1}></Grid>
                            <Grid item xs={11} >
                                {
                                    errorMessageList && errorMessageList.length > 0 ?
                                        <Grid item container >
                                            {
                                                errorMessageList.map((item, i) => (
                                                    <Grid item container key={i} justify="flex-start" id={`newRefferalLetterCert${item.fieldName.replace(' ', '')}ErrorMessage`}>
                                                        <Typography className={classes.errorFieldNameText}>{`${item.fieldName}: ${item.errMsg}`}</Typography>
                                                    </Grid>
                                                ))
                                            }
                                        </Grid> : null
                                }
                            </Grid>
                        </Grid>
                        : null
                }

                <Grid item container xs={12} >
                    <Grid item xs={1}><CIMSInputLabel>NO. of Copy:</CIMSInputLabel></Grid>
                    <Grid item xs={11}>

                        <ValidatorForm
                            id={this.props.id + '_form'}
                            ref={'form'}
                            onSubmit={() => { }}
                        >
                            <Grid container direction={'row'}>
                                <Grid style={{ width: 150 }}>
                                    <SelectFieldValidator
                                        id={this.props.id + '_selectCopyPage'}
                                        value={this.props.copyPage}
                                        options={allowCopyList && allowCopyList.map(item => ({ value: item.value, label: item.desc }))}
                                        onChange={e => this.updateCopyPage(e.value)}
                                    />
                                </Grid>
                            </Grid>
                        </ValidatorForm>
                    </Grid>
                </Grid>
                <CIMSButtonGroup
                    buttonConfig={
                        [
                            {
                                id: this.props.id + '_btnSaveAndPrint',
                                name: 'Save & Print',
                                // style:{ margin: '0 8px' },
                                onClick: this.preparePrint
                            },
                            {
                                id: this.props.id + '_btnCancel',
                                name: 'Cancel',
                                onClick: this.handleCancel
                            }
                        ]
                    }
                />
                <EditModeMiddleware componentName={accessRightEnum.referralLetter} when={this.state.isEdit} />
            </Grid>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        newLetterInfo: state.referralLetter.newLetterInfo,
        handlingPrint: state.referralLetter.handlingPrint,
        editModeTabs: state.mainFrame.editModeTabs,
        hospitalClinicList: state.common.hospitalAndClinicList,
        referToOpts: state.referralLetter.referToOpts,
        specialtyList: state.referralLetter.specialtyList
    };
};

const mapDispatchToProps = {
    resetAll,
    deleteSubTabsByOtherWay,
    listHospitalClinic,
    updateCurTab,
    openCommonMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ReferralLetter));