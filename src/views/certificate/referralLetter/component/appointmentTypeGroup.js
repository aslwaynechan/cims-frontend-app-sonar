import React, { Component } from 'react';
import { Grid, RadioGroup, Radio, FormControlLabel } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
//import CIMSTextField from '../../../../components/TextField/CIMSTextField';

const CustomRadio = withStyles({
    root: {
        padding: 5
    }
})(Radio);

const CustomFormControlLabel = withStyles({
    root: {
        marginLeft: 'unset'
    }
})(FormControlLabel);

class AppointmentTypeGroup extends Component {

    handleAppointmentTypeChange = (value) => {
        this.props.onChange(value);
    }
    render() {
        const appointmentList = [
            { label: 'Normal', value: 'Normal Appointment' },
            { label: 'Urgent', value: 'Urgent Appointment' }
        ];

        return (
            <RadioGroup
                id={this.props.id + '_appointmentTypeGroup'}
                row
                value={this.props.appointmentType}
                onChange={e => { this.handleAppointmentTypeChange(e.target.value); }}
            >
                {
                    appointmentList.map(item => (
                        <Grid item xs={2} key={item.label}>
                            <CustomFormControlLabel
                                value={item.value}
                                control={<CustomRadio color={'primary'} />}
                                label={item.label}
                            />
                        </Grid>
                    ))
                }
            </RadioGroup>
        );
    }
}

export default AppointmentTypeGroup;
