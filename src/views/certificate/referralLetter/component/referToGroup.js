import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import ValidatorForm from '../../../../components/FormValidator/ValidatorForm';
import SelectFieldValidator from '../../../../components/FormValidator/SelectFieldValidator';
import CIMSInputLabel from '../../../../components/InputLabel/CIMSInputLabel';

class ReferToGroup extends Component {


    state = {
        referToOpts: [],
        specialtyList: []
    }
    handleReferToDetialChange = (changes) => {
        this.props.onChange(changes);
    }
    // handleReferGroupChange = (value, name) => {
    //     let tempReferTo = { ...this.props.referTo };
    //     tempReferTo[name] = value;
    //     this.props.onChange(tempReferTo);
    // }

    handleReferToGroupChange = (value) => {
        let tempReferToOpts = [];
        let tempSepcialList = [];
        //let tempReferTo={...this.props.referTo};
        this.props.hospitalClinicList.hospitalClinicDtos.forEach(item => {
            if (item.groupCd === value) {
                tempReferToOpts.push(item);
            }
        });
        if (value === 'HA') {
            tempSepcialList = this.props.hospitalClinicList.specialtyDtos;
        }
        this.setState({
            referToOpts: tempReferToOpts,
            specialtyList: tempSepcialList
        });
        let tempReferTo = {
            ...this.props.referTo,
            groupCd: value,
            hosptialClinicName: null,
            specialty: null
        };
        this.handleReferToDetialChange(tempReferTo);
        this.props.errorFieldOnChange('group');
        //this.handleReferGroupChange(value, 'groupCd');

    }

    handleHospitalClinicNameChange = (value) => {
        let tempReferTo = {
            ...this.props.referTo,
            hosptialClinicName: value,
            specialty: null
        };
        this.handleReferToDetialChange(tempReferTo);
        this.props.errorFieldOnChange('hospitalAndClinic');
    }

    handleSpecialtyChange = (value) => {
        let tempReferTo = {
            ...this.props.referTo,
            specialty: value
        };
        this.handleReferToDetialChange(tempReferTo);
        this.props.errorFieldOnChange('specialty');
    }

    prepareGroupList = (hopsitalList) => {
        if (!hopsitalList || !hopsitalList.groupDtos) {
            return;
        }
        let groupList = [];
        let haOpts = hopsitalList.groupDtos.find(item => item.groupCd === 'HA');
        let dhOpts = hopsitalList.groupDtos.find(item => item.groupCd === 'DH');
        let privateOpts = hopsitalList.groupDtos.find(item => item.groupCd === 'Private');
        let othersOpts = hopsitalList.groupDtos.find(item => item.groupCd === 'Others');
        if (haOpts) {
            groupList.push(haOpts);
        }
        if (dhOpts) {
            groupList.push(dhOpts);
        }
        if (privateOpts) {
            groupList.push(privateOpts);
        }
        if (othersOpts) {
            groupList.push(othersOpts);
        }

        return groupList;
    }

    render() {
        let referTo = this.props.referTo;
        let groupList = this.prepareGroupList(this.props.hospitalClinicList);
        return (

            <ValidatorForm
                id={this.props.id + '_form'}
                ref={'form'}
                onSubmit={() => { }}
            >
                <Grid container item direction={'row'}>
                    <Grid container >
                        <Grid item style={{ width: 160 }} >
                            <Grid item ><CIMSInputLabel style={{ fontWeight: 'normal' }}>Group:</CIMSInputLabel></Grid>
                            <Grid item>
                                <SelectFieldValidator
                                    id={this.props.id + '_GroupSelectedField'}
                                    options={groupList && groupList.map(item => (
                                        { value: item.groupCd, label: item.groupName }
                                    ))}
                                    value={referTo.groupCd}
                                    onChange={e => this.handleReferToGroupChange(e.value)}
                                    TextFieldProps={{
                                        error: this.props.error.group
                                    }}
                                />
                            </Grid>
                        </Grid>
                        <Grid item style={{ flex: 1, marginLeft: 25, width: 1 }} >
                            <Grid item ><CIMSInputLabel style={{ fontWeight: 'normal' }}>Hospital / Clinic:</CIMSInputLabel></Grid>
                            <Grid item>
                                <SelectFieldValidator
                                    //label={'Clinic'}
                                    id={this.props.id + '_HospitalOrClinicSelectedField'}
                                    options={this.state.referToOpts &&
                                        this.state.referToOpts.map((item) => (
                                            { value: item.name, label: item.name }))}
                                    value={referTo.hosptialClinicName}
                                    onChange={e => this.handleHospitalClinicNameChange(e.value)}
                                    TextFieldProps={{
                                        error: this.props.error.hospitalAndClinic
                                    }}
                                />
                            </Grid>
                        </Grid>
                        <Grid item style={{ flex: 1, marginLeft: 25, width: 1 }} >
                            <Grid item ><CIMSInputLabel style={{ fontWeight: 'normal' }}>Specialty:</CIMSInputLabel></Grid>
                            <Grid item>
                                <SelectFieldValidator
                                    //label={'Clinic'}
                                    id={this.props.id + '_SpecialtySelectedField'}
                                    options={this.state.specialtyList &&
                                        this.state.specialtyList.map((item) => (
                                            { value: item.specialtyId, label: item.specialtyName }))}
                                    value={referTo.specialty}
                                    onChange={e => this.handleSpecialtyChange(e.value)}
                                    isDisabled={referTo.groupCd !== 'HA'}
                                    TextFieldProps={{
                                        error: this.props.error.specialty
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </ValidatorForm>

        );
    }
}

export default ReferToGroup;