import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import AttendanceCertNew from './attendanceCertNew';
import moment from 'moment';
import CommonRegex from '../../../constants/commonRegex';
import * as CommonUtilities from '../../../utilities/commonUtilities';
import * as CertificateUtil from '../../../utilities/certificateUtilities';
import * as CaseNoUtil from '../../../utilities/caseNoUtilities';
import {
    getAttendanceCert,
    updateField,
    listAttendanceCertificates,
    updateAttendanceCertificate
} from '../../../store/actions/certificate/attendanceCertificate/attendanceCertAction';
import { deleteSubTabsByOtherWay } from '../../../store/actions/mainFrame/mainFrameAction';
import { openCommonMessage } from '../../../store/actions/message/messageAction';
import HistoryList from '../component/historyList';
import Enum from '../../../enums/enum';
import CIMSButtonGroup from '../../../components/Buttons/CIMSButtonGroup';
import accessRightEnum from '../../../enums/accessRightEnum';

const styles = theme => ({
    container: {
        height: '100%',
        padding: theme.spacing(1) / 2
    }
});

class AttendanceCert extends Component {
    constructor(props) {
        super(props);
        this.state = {
            attendanceTo: '',
            attendanceDate: '',
            attendanceSection: '',
            attendanceSectionFrom: '',
            attendanceSectionTo: '',
            attendanceFor: '',
            attendanceForOthers: '',
            attendanceRemark: '',
            copyNo: 1,
            allowCopyList: [1, 2, 3, 4, 5],
            history: {
                open: true,
                serviceCd: this.props.service && this.props.service.serviceCd,
                dateFrom: moment().subtract(1, 'years'),
                dateTo: moment(),
                selectedIndex: ''
            }
        };
        this.state = Object.assign({}, this.state, this.initState());
    }

    componentDidMount() {
        this.props.ensureDidMount();
        this.updateAttendanceHistoryList();
    }

    initState = () => {
        const { appointmentInfo } = this.props;
        const attendanceTime = appointmentInfo && appointmentInfo.attnTime;
        const _attendanceTime = attendanceTime ? moment(attendanceTime) : moment();

        return {
            attendanceTo: 'Whom it may concern',
            attendanceDate: moment(),
            attendanceSection: CertificateUtil.getAttendanceCertSessionValue(CommonUtilities.matchSection(moment(), 'H', false)),
            attendanceSectionFrom: _attendanceTime,
            attendanceSectionTo: _attendanceTime.isAfter(moment()) ? _attendanceTime : moment(),
            attendanceFor: '',
            attendanceForOthers: '',
            attendanceRemark: '',
            copyNo: 1
        };
    }

    updateAttendanceHistoryList = () => {
        const { patientInfo } = this.props;
        const { dateFrom, dateTo, serviceCd } = this.state.history;
        this.props.listAttendanceCertificates({
            patientKey: patientInfo && patientInfo.patientKey,
            serviceCd: serviceCd === '*All' ? '' : serviceCd,
            from: dateFrom.format(Enum.DATE_FORMAT_EYMD_VALUE),
            to: dateTo.format(Enum.DATE_FORMAT_EYMD_VALUE)
        });
    }

    handleOnChange = (value, name) => {
        if (name === 'attendanceTo' || name === 'attendanceRemark' || name === 'attendanceForOthers') {
            const reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
            if (reg.test(value)) {
                return;
            }
        }
        this.setState({ [name]: value });
    }

    handlePrint = (params) => {
        this.props.updateField({ handlingPrint: true });
        let copyNo = this.state.copyNo;
        this.props.getAttendanceCert(
            params,
            copyNo,
            (printSuccess) => {
                this.props.updateField({ handlingPrint: false });
                this.updateAttendanceHistoryList();
                if (printSuccess) {
                    // eslint-disable-next-line
                    window.alert('Print Success');
                }
            });
    }

    handleHistoryChange = (value, name) => {
        let { history } = this.state;
        history[name] = value;
        this.setState({ history }, () => {
            if (name !== 'dateFrom' && name !== 'dateTo') {
                this.updateAttendanceHistoryList();
            }
        });
    }

    handleHistoryBlur = (value, name) => {
        let { history } = this.state;
        if (name === 'dateFrom' || name === 'dateTo') {
            if (name === 'dateFrom' && moment(value).isAfter(history.dateTo)) {
                history.dateTo = value;
            } else if (name === 'dateTo' && moment(value).isBefore(history.dateFrom)) {
                history.dateFrom = value;
            }
            this.setState({ history }, () => {
                this.updateAttendanceHistoryList();
            });
        }
    }

    handleHistoryListItemClick = (value, rowData) => {
        let { history } = this.state;
        history.selectedIndex = value;
        this.setState({ history });
        if (rowData) {
            this.setState({
                attendanceTo: rowData.certTo || 'Whom it may concern',
                attendanceDate: moment(rowData.attenDate),
                attendanceSection: rowData.attenSessionCd,
                attendanceSectionFrom: rowData.attenStartTime ? CommonUtilities.getTimeMoment(rowData.attenStartTime) : moment(),
                attendanceSectionTo: rowData.attenEndTime ? CommonUtilities.getTimeMoment(rowData.attenEndTime) : moment(),
                attendanceFor: rowData.attenForCd,
                attendanceForOthers: rowData.attenForOther,
                attendanceRemark: rowData.remark,
                copyNo: rowData.noOfCopy
            });
        } else {
            this.setState(this.initState());
        }
    }

    handleCopyToNew = () => {
        let { history } = this.state;
        history.selectedIndex = '';
        this.setState({ history });

        const initObj = this.initState();
        initObj.attendanceFor = this.state.attendanceFor;
        initObj.attendanceForOthers = this.state.attendanceForOthers;
        initObj.attendanceSectionFrom = this.state.attendanceSectionFrom;
        initObj.attendanceSectionTo = this.state.attendanceSectionTo;
        this.setState(initObj);
    }

    handleReprint = () => {
        if (this.props.handlingPrint) {
            return;
        }
        if(this.attendanceCertNewRef.refs && this.attendanceCertNewRef.refs.attendanceCertNewForm){
            const asyncValid = this.attendanceCertNewRef.refs.attendanceCertNewForm.isFormValid(false);
            asyncValid.then(result => {
                if (result) {
                    const {
                        attendanceDate,
                        attendanceSection,
                        attendanceSectionFrom,
                        attendanceSectionTo,
                        attendanceFor,
                        attendanceForOthers,
                        attendanceRemark,
                        attendanceTo
                    } = this.state;
                    const { patientInfo, clinic } = this.props;
                    let params = {
                        opType: 'RP',
                        attDate: attendanceDate.format(),
                        attTime: attendanceSection === 'R' ?
                            `${attendanceSectionFrom.format('HH:mm')}-${attendanceSectionTo.format('HH:mm')}` : '',
                        attendanceFor: attendanceFor === 'O' ? attendanceForOthers : CertificateUtil.getAttendanceCertForLabel(attendanceFor),
                        patientKey: patientInfo && patientInfo.patientKey,
                        period: attendanceSection === 'R' ? '' : CertificateUtil.getAttendanceCertSessionLabel(attendanceSection),
                        remark: attendanceRemark,
                        toValue: attendanceTo,
                        clinicCd: clinic && clinic.clinicCd
                    };
                    this.handlePrint(params);
                }
            });
        }
    }

    handleDelete = () => {
        const { selectedIndex } = this.state.history;
        const { attendanceCertList } = this.props;
        if (selectedIndex && attendanceCertList){
            this.props.openCommonMessage({
                msgCode: '110601',
                btnActions: {
                    btn1Click: () => {
                        const attn = attendanceCertList[parseInt(selectedIndex)];
                        const callBack = () => {
                            this.updateAttendanceHistoryList();
                        };
                        this.props.updateAttendanceCertificate({
                            id: attn.id,
                            statusCd: 'D'
                        }, callBack);
                    }
                }
            });
        }
    }

    handleClose = () => {
        this.props.deleteSubTabsByOtherWay({
            editModeTabs: this.props.editModeTabs,
            name: accessRightEnum.attendanceCert
        });
    }

    render() {
        const { classes, serviceList, attendanceCertList } = this.props;
        //this.matchCurrentSection();
        const {
            attendanceTo,
            attendanceDate,
            attendanceSection,
            attendanceSectionFrom,
            attendanceSectionTo,
            attendanceFor,
            attendanceForOthers,
            attendanceRemark,
            copyNo,
            allowCopyList,
            history,
            encounterInfo
        } = this.state;
        const isSelected = history.selectedIndex;
        return (
            <Grid
                container
                alignItems="flex-start"
                className={classes.container}
                wrap="nowrap"
                spacing={4}
            >
                <Grid item xs>
                    <HistoryList
                        {...history}
                        data={attendanceCertList}
                        serviceList={serviceList}
                        onChange={this.handleHistoryChange}
                        onBlur={this.handleHistoryBlur}
                        onListItemClick={this.handleHistoryListItemClick}
                        renderChild={(item, index) => {
                            const service = CommonUtilities.getServiceByCd(serviceList, item.serviceCd);
                            return (
                                <Grid container key={index}>
                                    <Grid item container justify="space-between">
                                        <Typography variant="body1">{service && service.serviceName}</Typography>
                                        <Typography variant="body1">{item.createdDtm && moment(item.createdDtm).format(Enum.DATE_FORMAT_24_HOUR)}</Typography>
                                    </Grid>
                                    <Grid item container justify="space-between">
                                        <Typography variant="body1">{CaseNoUtil.getFormatCaseNo(item.caseNo)}</Typography>
                                        <Typography variant="body1">{item.createdBy}</Typography>
                                    </Grid>
                                    <Grid item container justify="space-between">
                                        {
                                            item.encounterTypeCd && item.subEncounterTypeCd ?
                                                <Typography variant="body1">{`${item.encounterTypeCd} - ${item.subEncounterTypeCd}`}</Typography> : null
                                        }

                                    </Grid>
                                </Grid>
                            );
                        }}
                    />
                </Grid>
                <Grid item container>
                    <AttendanceCertNew
                        innerRef={ref => this.attendanceCertNewRef = ref}
                        attendanceTo={attendanceTo}
                        attendanceDate={attendanceDate}
                        attendanceSection={attendanceSection}
                        attendanceSectionFrom={attendanceSectionFrom}
                        attendanceSectionTo={attendanceSectionTo}
                        attendanceFor={attendanceFor}
                        attendanceForOthers={attendanceForOthers}
                        attendanceRemark={attendanceRemark}
                        copyNo={copyNo}
                        allowCopyList={allowCopyList}
                        handleOnChange={(value, name) => this.handleOnChange(value, name)}
                        handlePrint={this.handlePrint}
                        isSelected={isSelected}
                        handleClose={this.handleClose}
                    />
                </Grid>
                {
                    isSelected ?
                        <CIMSButtonGroup
                            buttonConfig={
                                [
                                    {
                                        id: 'attendanceCert_btnCopyToNew',
                                        name: 'Copy to New',
                                        onClick: this.handleCopyToNew
                                    },
                                    {
                                        id: 'attendanceCert_btnReprint',
                                        name: 'Reprint',
                                        onClick: this.handleReprint
                                    },
                                    {
                                        id: 'attendanceCert_btnDelete',
                                        name: 'Delete',
                                        onClick: this.handleDelete
                                    },
                                    {
                                        id: 'attendanceCert_btnClose',
                                        name: 'Close',
                                        onClick: this.handleClose
                                    }
                                ]
                            }
                        /> : null
                }
            </Grid>
        );
    }
}

const stateToProps = (state) => {
    return {
        //allowCopyList: state.yellowFever.allowCopyList,
        //copyPage: state.yellowFever.copyPage
        handingPrint: state.attendanceCert.handlingPrint,
        serviceList: state.common.serviceList,
        patientInfo: state.patient.patientInfo,
        service: state.login.service,
        attendanceCertList: state.attendanceCert.attendanceCertList,
        appointmentInfo: state.patient.appointmentInfo,
        clinic: state.login.clinic,
        editModeTabs: state.mainFrame.editModeTabs,
        encounterInfo: state.patient.encounterInfo
    };
};

const dispatchToProps = {
    // updateField,
    // saveAndPrintYellowFeverLetter,
    getAttendanceCert,
    updateField,
    listAttendanceCertificates,
    deleteSubTabsByOtherWay,
    openCommonMessage,
    updateAttendanceCertificate
};

export default connect(stateToProps, dispatchToProps)(withStyles(styles)(AttendanceCert));