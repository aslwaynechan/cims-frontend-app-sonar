import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import SelectFieldValidator from '../../../components/FormValidator/SelectFieldValidator';
import TextFieldValidator from '../../../components/FormValidator/TextFieldValidator';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import CIMSTextField from '../../../components/TextField/CIMSTextField';
import AttendanceReasonRadio from './component/attendanceReasonRadio';
import AttendanceDate from './component/attendanceDate';
import { deleteSubTabsByOtherWay, updateCurTab } from '../../../store/actions/mainFrame/mainFrameAction';
import accessRightEnum from '../../../enums/accessRightEnum';
import EditModeMiddleware from '../../compontent/editModeMiddleware';
import CIMSButtonGroup from '../../../components/Buttons/CIMSButtonGroup';
import { openCommonMessage } from '../../../store/actions/message/messageAction';
import RequiredIcon from '../../../components/InputLabel/RequiredIcon';
import ValidatorEnum from '../../../enums/validatorEnum';
import CommonMessage from '../../../constants/commonMessage';
import * as CertificateUtil from '../../../utilities/certificateUtilities';
import moment from 'moment';

const styles = () => ({
    root: {
        paddingTop: 30
    }
});

class AttendanceCertNew extends Component {
    state = {
        edit: false
    }

    componentDidMount() {
        this.props.updateCurTab('F031', this.doClose);
    }

    doClose = (callback) => {
        if (this.state.edit) {
            this.props.openCommonMessage({
                msgCode: '110018',
                btnActions: {
                    btn1Click: () => {
                        callback(true);
                    }
                }
            });
        }
        else {
            callback(true);
        }
    }

    handleOnChange = (value, name) => {
        if (name === 'attendanceSectionFrom' && moment(this.props.attendanceSectionTo).isBefore(value)) {
            this.props.handleOnChange(value, 'attendanceSectionTo');
        } else if (name === 'attendanceSectionTo' && moment(this.props.attendanceSectionFrom).isAfter(value)) {
            this.props.handleOnChange(value, 'attendanceSectionFrom');
        }
        this.props.handleOnChange(value, name);
        this.setState({ edit: true });
    }

    preparePrint = () => {
        if (this.props.handlingPrint) {
            return;
        }
        const {
            attendanceDate,
            attendanceSection,
            attendanceSectionFrom,
            attendanceSectionTo,
            attendanceFor,
            attendanceForOthers,
            patientInfo,
            attendanceRemark,
            attendanceTo,
            caseNoInfo,
            encounterInfo,
            service,
            clinic,
            copyNo
        } = this.props;

        let params = {
            opType: 'SP',
            attDate: attendanceDate.format(),
            attTime: attendanceSection === 'R' ?
                `${attendanceSectionFrom.format('HH:mm')}-${attendanceSectionTo.format('HH:mm')}` : '',
            attendanceFor: attendanceFor === 'O' ? attendanceForOthers : CertificateUtil.getAttendanceCertForLabel(attendanceFor),
            patientKey: patientInfo && patientInfo.patientKey,
            period: attendanceSection === 'R' ? '' : CertificateUtil.getAttendanceCertSessionLabel(attendanceSection),
            remark: attendanceRemark,
            toValue: attendanceTo,
            clinicCd: clinic && clinic.clinicCd,
            attendanceCertificateDto: {
                attenDate: attendanceDate.format(),
                attenEndTime: attendanceSection === 'R' ? attendanceSectionTo.format('HH:mm') : '',
                attenForCd: attendanceFor,
                attenForOther: attendanceForOthers,
                attenSessionCd: attendanceSection,
                attenStartTime: attendanceSection === 'R' ? attendanceSectionFrom.format('HH:mm') : '',
                caseNo: caseNoInfo && caseNoInfo.caseNo,
                certTo: attendanceTo,
                clinicCd: clinic && clinic.clinicCd,
                encounterId: encounterInfo && encounterInfo.encounterId,
                noOfCopy: copyNo,
                patientKey: patientInfo && patientInfo.patientKey,
                remark: attendanceRemark,
                serviceCd: service && service.serviceCd
            }
        };
        this.props.handlePrint(params);
        this.setState({ edit: false });
    }

    render() {
        //eslint-disable-next-line
        const { classes } = this.props;
        const {
            attendanceTo,
            attendanceDate,
            attendanceSection,
            attendanceSectionFrom,
            attendanceSectionTo,
            attendanceFor,
            attendanceForOthers,
            attendanceRemark,
            copyNo,
            allowCopyList,
            isSelected
        } = this.props;
        return (
            <ValidatorForm
                id="attendanceCertNew_form"
                ref={'attendanceCertNewForm'}
                onSubmit={this.preparePrint}
            >
                <Grid container spacing={2} className={classes.root}>
                    <Grid item container xs={12}>
                        <TextFieldValidator
                            id="attendanceCertNew_to"
                            value={attendanceTo}
                            onChange={e => this.handleOnChange(e.target.value, 'attendanceTo')}
                            inputProps={{
                                maxLength: 34
                            }}
                            onBlur={e => {
                                if (!e.target.value) {
                                    this.handleOnChange('Whom it may concern', 'attendanceTo');
                                }
                            }}
                            label={<>To<RequiredIcon /></>}
                            validators={[ValidatorEnum.required]}
                            errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                        />
                    </Grid>
                    <Grid item container xs={12}>
                        <AttendanceDate
                            date={attendanceDate}
                            section={attendanceSection}
                            from={attendanceSectionFrom}
                            to={attendanceSectionTo}
                            value={attendanceSection && (attendanceSection === 'R' ? attendanceSectionFrom && attendanceSectionTo : true) ? '1' : ''}
                            handleChange={(value, name) => this.handleOnChange(value, name)}
                            validators={[ValidatorEnum.required]}
                            errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                        />
                    </Grid>
                    <Grid item container xs={12}>
                        <AttendanceReasonRadio
                            value={attendanceFor && (attendanceFor === 'Others' ? attendanceForOthers : true) ? '1' : ''}
                            forValue={attendanceFor}
                            otherValue={attendanceForOthers}
                            handleChange={(value, name) => this.handleOnChange(value, name)}
                            validators={[ValidatorEnum.required]}
                            errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                        />
                    </Grid>
                    <Grid item container xs={12}>
                        <CIMSTextField
                            id="attendanceCertNew_remark"
                            value={attendanceRemark}
                            onChange={e => this.handleOnChange(e.target.value, 'attendanceRemark')}
                            inputProps={{
                                maxLength: 50
                            }}
                            label="Remark"
                        />
                    </Grid>
                    <Grid item container xs={2}>
                        <SelectFieldValidator
                            id="attendanceCertNew_noOfCopy"
                            value={copyNo}
                            options={allowCopyList && allowCopyList.map(item => ({ value: item, label: item }))}
                            onChange={e => this.handleOnChange(e.value, 'copyNo')}
                            TextFieldProps={{
                                label: <>No. of Copy<RequiredIcon /></>
                            }}
                            validators={[ValidatorEnum.required]}
                            errorMessages={[CommonMessage.VALIDATION_NOTE_REQUIRED()]}
                        />
                    </Grid>
                    {
                        !isSelected ?
                            <CIMSButtonGroup
                                buttonConfig={
                                    [
                                        {
                                            id: 'attendanceCertNew_btnSaveAndPrint',
                                            name: 'Save & Print',
                                            type: 'submit'
                                        },
                                        {
                                            id: 'attendanceCertNew_btnCancel',
                                            name: 'Cancel',
                                            onClick: () => { this.props.handleClose(); }
                                        }
                                    ]
                                }
                            /> : null
                    }
                    <EditModeMiddleware componentName={accessRightEnum.attendanceCert} when={this.state.edit} />
                </Grid>
            </ValidatorForm>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        patientInfo: state.patient.patientInfo,
        caseNoInfo: state.patient.caseNoInfo,
        encounterInfo: state.patient.encounterInfo,
        reportData: state.attendanceCert.reportData,
        editModeTabs: state.mainFrame.editModeTabs,
        handlingPrint: state.attendanceCert.handlingPrint,
        clinic: state.login.clinic,
        service: state.login.service
    };
};

const mapDispatchToProps = {
    //getAttendanceCert,
    deleteSubTabsByOtherWay,
    updateCurTab,
    openCommonMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AttendanceCertNew));