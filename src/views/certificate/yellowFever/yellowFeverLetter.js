import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Tabs, Tab, Typography } from '@material-ui/core';
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import NewYellowFever from './newYellowFever';
import {
    updateField,
    saveAndPrintYellowFeverLetter
} from '../../../store/actions/certificate/yellowFever/yellowFeverAction';
import * as CommonUtilities from '../../../utilities/commonUtilities';

const styles = () => ({
    tabs: {
        minHeight: 'unset'
    },
    tab: {
        minWidth: 72,
        minHeight: 20,
        padding: 0
    },
    tabTypography: {
        fontSize: 16,
        textTransform: 'none'
    },
    container: {
        borderTop: '1px solid #ccc'
    }
});

const sysRatio = CommonUtilities.getSystemRatio();
const unit = CommonUtilities.getResizeUnit(sysRatio);

const customTheme = (theme) => createMuiTheme({
    ...theme,
    spacing: (18 * unit)
});

class YellowFeverLetter extends Component {
    state = {
        tabValue: 0
    }

    componentDidMount() {
        this.props.ensureDidMount();
    }

    changeTabValue = (event, value) => {
        this.setState({ tabValue: value });
    }

    handleOnChange = (changes) => {
        this.props.updateField(changes);
    }

    handlePrint = (params) => {
        let copyPage = this.props.copyPage;
        //params.patientKey = this.props.patientInfo.patientKey;
        this.props.updateField({ handlingPrint: true });
        this.props.saveAndPrintYellowFeverLetter(
            params,
            (printSuccess) => {
                this.props.updateField({ handlingPrint: false });
                if (printSuccess) {
                    console.log('Print Success');
                }
            },
            copyPage);
    }

    render() {
        const { classes } = this.props;
        return (
            <MuiThemeProvider theme={customTheme}>
                <Grid container>
                    <Tabs
                        value={this.state.tabValue}
                        onChange={this.changeTabValue}
                        indicatorColor={'primary'}
                        className={classes.tabs}
                    >
                        <Tab id={'yellowFever_tabNew'} className={classes.tab} label={<Typography className={classes.tabTypography}>New</Typography>} />
                        <Tab id={'yellowFever_tabHistory'} className={classes.tab} disabled label={<Typography className={classes.tabTypography}>History</Typography>} />
                    </Tabs>
                    <Grid item container xs={12} className={classes.container}>
                        <Grid item xs={12} hidden={this.state.tabValue !== 0} style={{ margin: '25px 20px' }}>
                            {/* <NewSickLeave
                            id={'sickLeaveCertNewSickLeave'}
                            allowCopyList={this.props.allowCopyList}
                            handleOnChange={this.handleOnChange}
                            copyPage={this.props.copyPage}
                            handlePrint={this.handlePrint}
                        /> */}
                            <NewYellowFever
                                id={'yellowFeverNewYellowFever'}
                                allowCopyList={this.props.allowCopyList}
                                copyPage={this.props.copyPage}
                                handleOnChange={this.handleOnChange}
                                handlePrint={this.handlePrint}
                            />
                        </Grid>
                        <Grid item xs={12} hidden={this.state.tabValue !== 1}>
                            <Grid>
                                History
                        </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </MuiThemeProvider>
        );
    }
}

const stateToProps = (state) => {
    return {
        allowCopyList: state.yellowFever.allowCopyList,
        copyPage: state.yellowFever.copyPage
    };
};

const dispatchToProps = {
    updateField,
    saveAndPrintYellowFeverLetter
};

export default connect(stateToProps, dispatchToProps)(withStyles(styles)(YellowFeverLetter));