import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import CIMSInputLabel from '../../../../components/InputLabel/CIMSInputLabel';
import CIMSTextField from '../../../../components/TextField/CIMSTextField';
import RequiredIcon from '../../../../components/InputLabel/RequiredIcon';
import CIMSSelect from '../../../../components/Select/CIMSSelect';
class PassportGroup extends Component {
    updatePassportInfo = (value, name) => {
        this.props.passportGroupOnChange(value, name);
    }
    render() {
        let { passportInfo } = this.props;
        return (
            <Grid container item>
                <Grid container >
                    <Grid item xs={1}><CIMSInputLabel>Issued Country:<RequiredIcon /></CIMSInputLabel></Grid>
                    <Grid item xs={3}>
                        <CIMSSelect
                            id={`${this.props.id}_passportSelectField`}
                            name={'issuedCountry'}
                            onChange={e => this.updatePassportInfo(e.value, 'issuedCountry')}
                            //error={this.props.error.issuedCountry}
                            //upperCase
                            value={passportInfo.issuedCountry}
                            options={passportInfo.passportList && passportInfo.passportList.map(item => ({ value: item, label: item }))}
                            TextFieldProps={{
                                error: this.props.error.issuedCountry
                            }}
                        />

                    </Grid>

                    <Grid item style={{ margin: '0px 50px 0px 35px' }} ><CIMSInputLabel>Passport Number:<RequiredIcon /></CIMSInputLabel></Grid>
                    <Grid item xs={3}>
                        <CIMSTextField
                            id={`${this.props.id}_passportNumberTextField`}
                            type={'cred'}
                            name={'passportNumber'}
                            onChange={e => this.updatePassportInfo(e.target.value, e.target.name)}
                            error={this.props.error.passportNumber}
                            value={passportInfo.passportNumber}
                            inputProps={{
                                maxLength: 30
                            }}
                        />
                    </Grid>

                </Grid>
            </Grid>
        );
    }
}


export default PassportGroup;