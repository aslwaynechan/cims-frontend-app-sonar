import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import CIMSInputLabel from '../../../components/InputLabel/CIMSInputLabel';
import CIMSMultiTextField from '../../../components/TextField/CIMSMultiTextField';
import EditModeMiddleware from '../../compontent/editModeMiddleware';
import CIMSSelect from '../../../components/Select/CIMSSelect';
import PassportGroup from './component/passportGroup';
import RequiredIcon from '../../../components/InputLabel/RequiredIcon';
import {
    resetAll
} from '../../../store/actions/certificate/yellowFever/yellowFeverAction';
import CommonMessage from '../../../constants/commonMessage';
import {
    openCommonCircular,
    closeCommonCircular
} from '../../../store/actions/common/commonAction';
import AccessRightEnum from '../../../enums/accessRightEnum';
import {
    deleteSubTabsByOtherWay,
    updateCurTab
} from '../../../store/actions/mainFrame/mainFrameAction';
import {
    listPassport
} from '../../../store/actions/patient/patientAction';
import CommonRegex from '../../../constants/commonRegex';
import accessRightEnum from '../../../enums/accessRightEnum';
import { openCommonMessage } from '../../../store/actions/message/messageAction';
import CIMSButtonGroup from '../../../components/Buttons/CIMSButtonGroup';


const styles = () => ({
    marginGrid: {
        margin: '0px 20px 25px'
    },
    errorFieldNameText: {
        fontSize: '16px',
        wordBreak: 'break-word',
        color: '#fd0000'
    }
});

class NewYellowFever extends Component {
    state = {
        isEdit: false,
        errorMessageList: [],
        //showSufferFromError:
        showInputError: {
            nationality: false,
            issuedCountry: false,
            passportNumber: false,
            exemptionReason: false
        }
    };

    componentDidMount() {
        this.props.listPassport();
        this.props.updateCurTab('F030', this.doClose);
    }
    componentWillUnmount() {
        this.props.resetAll();
    }

    handleOnChange = (changes) => {
        this.props.handleOnChange(changes);
        this.setState({ isEdit: true });
    }

    updateLetterInfo = (value, name) => {
        let letterInfo = { ...this.props.newYellowFeverLetter };
        //let name = e.target.name;
        //let value = e.target.value;
        let { errorMessageList, showInputError } = this.state;
        let tempErrorList = [];
        const reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
        if (name === 'exemptionReason') {
            if (reg.test(value)) {
                return;
            }
        }

        if (name === 'nationality' || name === 'issuedCountry' || name === 'passportNumber') {
            value = value.toUpperCase();
        }

        letterInfo[name] = value;
        this.handleOnChange({ newYellowFeverInfo: letterInfo });

        if (errorMessageList.length > 0) {
            let nationalityErrField = errorMessageList.find(item => item.fieldName === 'Nationality');
            let issuedCountryErrField = errorMessageList.find(item => item.fieldName === 'Issued Country');
            let passportNumberErrField = errorMessageList.find(item => item.fieldName === 'PassportNumber');
            let exemptionReasonErrField = errorMessageList.find(item => item.fieldName === 'ExemptionReason');
            if (letterInfo.nationality === '' && name !== 'nationality') {
                if (nationalityErrField) {
                    tempErrorList.push(nationalityErrField);
                    //showInputError.nationality = true;
                }
            }

            if (letterInfo.issuedCountry === '') {
                if (issuedCountryErrField) {
                    tempErrorList.push(issuedCountryErrField);
                    //showInputError.issuedCountry = true;
                }

            }

            if (letterInfo.passportNumber === '') {
                if (passportNumberErrField) {
                    tempErrorList.push(passportNumberErrField);
                    //showInputError.passportNumber = ;
                }
            }

            if (letterInfo.exemptionReason === '') {
                if (exemptionReasonErrField) {
                    tempErrorList.push(exemptionReasonErrField);
                    //showInputError.exemptionReason = false;
                }
            }

            showInputError[name] = false;

            this.setState({
                errorMessageList: tempErrorList,
                showInputError: showInputError
            });
        }

        this.setState({ isEdit: true });

    }

    updateCopyPage = (value) => {
        this.handleOnChange({ copyPage: value });

        this.setState({ isEdit: true });
    }

    fillErrorBeforePrint = (letterInfo) => {
        //let referTo = letterInfo.referTo;
        //let tempErrorMsgList = [];
        let { errorMessageList } = this.state;
        let tempshowInputError = this.state.showInputError;
        let errField = null;

        if (letterInfo.nationality === '') {
            errField = errorMessageList.find(item => item.fieldName === 'Nationality');
            if (errField === undefined) {
                errorMessageList.push({ fieldName: 'Nationality', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            }
            tempshowInputError.nationality = true;
        }

        if (letterInfo.issuedCountry === '') {
            errField = errorMessageList.find(item => item.fieldName === 'Issued Country');
            if (errField === undefined) {
                errorMessageList.push({ fieldName: 'Issued Country', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            }
            //tempErrorMsgList.push({ fieldName: 'Issued Country', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            tempshowInputError.issuedCountry = true;
        }

        if (letterInfo.passportNumber === '') {
            errField = errorMessageList.find(item => item.fieldName === 'PassportNumber');
            if (errField === undefined) {
                errorMessageList.push({ fieldName: 'PassportNumber', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            }
            //tempErrorMsgList.push({ fieldName: 'PassportNumber', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            tempshowInputError.passportNumber = true;
        }

        if (letterInfo.exemptionReason === '') {
            errField = errorMessageList.find(item => item.fieldName === 'ExemptionReason');
            if (errField === undefined) {
                errorMessageList.push({ fieldName: 'ExemptionReason', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            }
            //tempErrorMsgList.push({ fieldName: 'ExemptionReason', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            tempshowInputError.exemptionReason = true;
        }

        this.setState({ showInputError: tempshowInputError });
        return errorMessageList;
    }

    validatorListener = (isValid, message, name) => {
        let { errorMessageList } = this.state;
        if (!isValid) {
            let errField = errorMessageList.find(item => item.fieldName === name);
            if (errField === undefined) {
                errorMessageList.push({ fieldName: name, errMsg: message });
            }
        }
        this.setState(errorMessageList);
    }



    preparePrint = () => {
        //this.props.closeCommonCircular();
        let letterInfo = this.props.newYellowFeverLetter;
        let patientInfo = this.props.patientInfo;
        let errorMsgList = this.fillErrorBeforePrint(letterInfo);

        if (errorMsgList.length > 0) {
            this.setState({ errorMessageList: errorMsgList });
            return;
        }

        if (this.props.handlingPrint) {
            return;
        }


        let params = {
            opType: 'SP',
            patientKey: patientInfo.patientKey,
            nationality: letterInfo.nationality,
            issuedCountry: `${letterInfo.issuedCountry} PASSPORT NUMBER`,
            passportNumber: letterInfo.passportNumber,
            exemptionReason: letterInfo.exemptionReason
        };
        this.props.handlePrint(params);

        this.setState({ isEdit: false });
    }

    handleSubmitError = () => {
        this.props.closeCommonCircular();
    }

    handleCancel = () => {
        this.props.deleteSubTabsByOtherWay({
            editModeTabs: this.props.editModeTabs,
            name: AccessRightEnum.yellowFeverLetter
        });
    }


    doClose = (callback) => {
        if (this.state.isEdit) {
            this.props.openCommonMessage({
                msgCode: '110018',
                btnActions: {
                    btn1Click: () => {
                        callback(true);
                    }
                }
            });
        }
        else {
            callback(true);
        }
    }

    render() {
        const { classes, allowCopyList, nationalityList, passportList } = this.props;
        let { errorMessageList, showInputError } = this.state;
        return (
            // <MuiThemeProvider theme={customTheme}>

            <Grid container spacing={2}>
                <Grid item container xs={12} >
                    <Grid item xs={1}><CIMSInputLabel>Nationality:<RequiredIcon /></CIMSInputLabel></Grid>
                    <Grid item xs={3}>
                        <CIMSSelect
                            id={`${this.props.id}_nationalitySelectField`}
                            name={'nationality'}
                            onChange={e => this.updateLetterInfo(e.value, 'nationality')}
                            //error={showInputError.nationality}
                            value={this.props.newYellowFeverLetter.nationality}
                            options={nationalityList && nationalityList.map(item => ({ value: item, label: item }))}
                            TextFieldProps={{
                                error: showInputError.nationality
                            }}
                        />
                    </Grid>
                </Grid>

                <Grid item container xs={12} >
                    <PassportGroup
                        id={`${this.props.id}_passprotGroup`}
                        passportGroupOnChange={this.updateLetterInfo}
                        error={{ issuedCountry: showInputError.issuedCountry, passportNumber: showInputError.passportNumber }}
                        passportInfo={{
                            issuedCountry: this.props.newYellowFeverLetter.issuedCountry,
                            passportNumber: this.props.newYellowFeverLetter.passportNumber,
                            passportList: passportList
                        }}
                    />
                </Grid>

                <Grid item container xs={12} >
                    <Grid item xs={1}><CIMSInputLabel>Exemption Reason:<RequiredIcon /></CIMSInputLabel></Grid>
                    <Grid item xs={11}>
                        {/* <CIMSTextField
                            id={`${this.props.id}_documentNoTextField`}
                        /> */}
                        <CIMSMultiTextField
                            id={`${this.props.id}_exemptionReasonTextField`}
                            rows="5"
                            name={'exemptionReason'}
                            onChange={e => this.updateLetterInfo(e.target.value, e.target.name)}
                            error={showInputError.exemptionReason}
                            value={this.props.newYellowFeverLetter.exemptionReason}
                            inputProps={{
                                maxLength: 1000
                            }}
                        />
                    </Grid>
                </Grid>
                {
                    errorMessageList.length > 0 ?
                        <Grid item container xs={12}>
                            <Grid item xs={1}></Grid>
                            <Grid item xs={11} >
                                {
                                    errorMessageList && errorMessageList.length > 0 ?
                                        <Grid item container >
                                            {
                                                errorMessageList.map((item, i) => (
                                                    <Grid item container key={i} justify="flex-start" id={`newRefferalLetterCert${item.fieldName.replace(' ', '')}ErrorMessage`}>
                                                        <Typography className={classes.errorFieldNameText}>{`${item.fieldName}: ${item.errMsg}`}</Typography>
                                                    </Grid>
                                                ))
                                            }
                                        </Grid> : null
                                }
                            </Grid>
                        </Grid>
                        : null
                }

                <Grid item container xs={12} >
                    <Grid item xs={1}><CIMSInputLabel>NO. of Copy:</CIMSInputLabel></Grid>
                    <Grid item xs={11}>


                        <Grid container direction={'row'}>
                            <Grid style={{ width: 150 }}>
                                <CIMSSelect
                                    id={`${this.props.id}_selectCopyPage`}
                                    value={this.props.copyPage}
                                    options={allowCopyList && allowCopyList.map(item => ({ value: item.value, label: item.desc }))}
                                    onChange={e => this.updateCopyPage(e.value)}
                                    autoComplete={'off'}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <CIMSButtonGroup
                    buttonConfig={
                        [
                            {
                                id: this.props.id + '_btnSaveAndPrint',
                                name: 'Save & Print',
                                // style:{ margin: '0 8px' },
                                onClick: this.preparePrint
                            },
                            {
                                id: this.props.id + '_btnCancel',
                                name: 'Cancel',
                                onClick: this.handleCancel
                            }
                        ]
                    }
                />
                <EditModeMiddleware componentName={accessRightEnum.yellowFeverLetter} when={this.state.isEdit} />
            </Grid>
            // </MuiThemeProvider >
        );
    }
}

const mapStateToProps = (state) => {
    return {
        newYellowFeverLetter: state.yellowFever.newYellowFeverInfo,
        patientInfo: state.patient.patientInfo,
        // newLeaveInfo: state.sickLeave.newLeaveInfo,
        handlingPrint: state.yellowFever.handlingPrint,
        editModeTabs: state.mainFrame.editModeTabs,
        nationalityList: state.patient.nationalityList,
        passportList: state.patient.passportList,
        canCloseTab: state.mainFrame.canCloseTab
    };
};

const mapDispatchToProps = {
    resetAll,
    openCommonCircular,
    closeCommonCircular,
    deleteSubTabsByOtherWay,
    listPassport,
    openCommonMessage,
    updateCurTab
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(NewYellowFever));