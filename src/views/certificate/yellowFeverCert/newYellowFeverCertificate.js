import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Typography, FormControl } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import CIMSInputLabel from '../../../components/InputLabel/CIMSInputLabel';
import CIMSTextField from '../../../components/TextField/CIMSTextField';
import { deleteSubTabsByOtherWay, updateCurTab } from '../../../store/actions/mainFrame/mainFrameAction';
import accessRightEnum from '../../../enums/accessRightEnum';
import CIMSButtonGroup from '../../../components/Buttons/CIMSButtonGroup';
import CIMSSelect from '../../../components/Select/CIMSSelect';
import CommonMessage from '../../../constants/commonMessage';
import RequiredIcon from '../../../components/InputLabel/RequiredIcon';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import CommonRegex from '../../../constants/commonRegex';
import TextFieldValidator from '../../../components/FormValidator/TextFieldValidator';
import EditModeMiddleware from '../../compontent/editModeMiddleware';
import moment from 'moment';
import DateFieldValidator from '../../../components/FormValidator/DateFieldValidator';
import ValidatorEnum from '../../../enums/validatorEnum';
import { openCommonMessage } from '../../../store/actions/message/messageAction';

const styles = () => ({
    root: {
        paddingTop: 10,
        width: '90%'
    },
    grid: {
        paddingTop: 4,
        paddingBottom: 4,
        //padding: 20,
        justifyContent: 'center'
    },
    form_input: {
        width: '100%'
    },
    errorFieldNameText: {
        fontSize: '16px',
        wordBreak: 'break-word',
        color: '#fd0000'
    }
});

function formatNumber(n) {
    n = n.toString();
    return n[1] ? n : '0' + n;
}

function formatTime(number, format) {
    let time = new Date(number);
    let newArr = [];
    let formatArr = ['Y', 'M', 'D', 'h', 'm', 's'];
    newArr.push(time.getFullYear());
    newArr.push(formatNumber(time.getMonth() + 1));
    newArr.push(formatNumber(time.getDate()));

    newArr.push(formatNumber(time.getHours()));
    newArr.push(formatNumber(time.getMinutes()));
    newArr.push(formatNumber(time.getSeconds()));

    for (let i in newArr) {
        format = format.replace(formatArr[i], newArr[i]);
    }
    return format;
}
class NewYellowFeverCertificate extends Component {
    state = {
        name: this.props.patientInfo.engSurname + ' ' + this.props.patientInfo.engGivename,
        gender: this.props.patientInfo.genderCd,
        dob: formatTime(new Date(this.props.patientInfo.dob).getTime(), 'D/M/Y'),
        nationality: this.props.patientInfo.nationality,
        otherDocumentNo: this.props.patientInfo.otherDocNo,
        issueDate: new Date(),
        doctorName: '',
        doctorPost: '',
        manufacture: '',
        batcheNo: '',
        validDate: new Date(new Date().getTime() + 24 * 60 * 60 * 1000 * 10),
        errorMessageList: [],
        showAreaTextError: {
            name: false,
            otherDocumentNo: false,
            nationality: false,
            issueDate: false,
            doctorName: false,
            doctorPost: false,
            manufacture: false,
            batcheNo: false,
            validDate: false,
            certNo: false
        },
        edit: false,
        certNo: ''

    };

    componentDidMount = () => {
        this.props.updateCurTab('F049', this.doClose);
    }


    doClose = (callback) => {
        if (this.state.edit) {
            this.props.openCommonMessage({
                msgCode: '110018',
                btnActions: {
                    btn1Click: () => {
                        callback(true);
                    }
                }
            });
        }
        else {
            callback(true);
        }
    }

    handleOnChange = (changes) => {
        this.props.handleOnChange(changes);
    }


    updateCopyPage = (value) => {
        this.handleOnChange({ copyPage: value });
    }

    handleUpdate = (value, name) => {
        if (name != 'issueDate' && name != 'validDate') {
            const reg = new RegExp(CommonRegex.VALIDATION_REGEX_INCLUDE_CHINESE);
            if (reg.test(value)) {
                return;
            }
        }
        if (name == 'issueDate') {
            this.setState({
                issueDate: value,
                edit: true
            });
            return;
        }
        if (name == 'validDate') {
            this.setState({
                validDate: value,
                edit: true
            });
            return;
        }
        let stateData = { ...this.state };
        stateData[name] = value;
        this.setState({
            ...stateData,
            edit: true
        }, () => {
            let { errorMessageList, showAreaTextError } = this.state;

            if (errorMessageList.length > 0) {
                if (name == 'name') {
                    let index = errorMessageList.findIndex(error => error.fieldName === 'Name');
                    if (index != -1) {
                        errorMessageList.splice(index, 1);
                    }
                }
                if (name == 'nationality') {
                    let index = errorMessageList.findIndex(error => error.fieldName === 'Nationality');
                    if (index != -1) {
                        errorMessageList.splice(index, 1);
                    }
                }
                if (name == 'issueDate') {
                    let index = errorMessageList.findIndex(error => error.fieldName === 'Issue Date');
                    if (index != -1) {
                        errorMessageList.splice(index, 1);
                    }
                }
                if (name == 'doctorName') {
                    let index = errorMessageList.findIndex(error => error.fieldName === 'Doctors Name');
                    if (index != -1) {
                        errorMessageList.splice(index, 1);
                    }
                }
                if (name == 'doctorPost') {
                    let index = errorMessageList.findIndex(error => error.fieldName === 'Doctor Post');
                    if (index != -1) {
                        errorMessageList.splice(index, 1);
                    }
                }
                if (name == 'manufacture') {
                    let index = errorMessageList.findIndex(error => error.fieldName === 'Manufacturer');
                    if (index != -1) {
                        errorMessageList.splice(index, 1);
                    }
                }
                if (name == 'batcheNo') {
                    let index = errorMessageList.findIndex(error => error.fieldName === 'Batch No');
                    if (index != -1) {
                        errorMessageList.splice(index, 1);
                    }
                }
                if (name == 'validDate') {
                    let index = errorMessageList.findIndex(error => error.fieldName === 'Valid Date Of Certificate');
                    if (index != -1) {
                        errorMessageList.splice(index, 1);
                    }

                }
                if (name == 'certNo') {
                    let index = errorMessageList.findIndex(error => error.fieldName === 'Cert No');
                    if (index != -1) {
                        errorMessageList.splice(index, 1);
                    }

                }
                this.setState({
                    showAreaTextError: showAreaTextError,
                    errorMessageList
                });
            }
            showAreaTextError[name] = false;
        });


    }

    handleOnBlur = (value, name) => {
        if(!value){
            if (name === 'issueDate'){
                this.setState({
                    [name]: moment(),
                    edit: true
                });
            } else if (name === 'validDate'){
                this.setState({
                    [name]: value || moment(new Date().getTime() + 24 * 60 * 60 * 1000 * 10),
                    edit: true
                });
            }
        }
    }

    isEmpty = (obj) => {
        if (typeof obj == 'undefined' || obj == null || obj == '') {
            return true;
        } else {
            return false;
        }
    }

    handleSubmit = () => {
        this.refs.newYellowFeverForm.submit();
    }

    preparePrint = () => {
        let { name, nationality, doctorName, doctorPost, manufacture, batcheNo, errorMessageList, showAreaTextError, certNo } = this.state;

        let tempErrorMessageList = [];
        errorMessageList = [];
        if (this.isEmpty(name)) {
            tempErrorMessageList.push({ fieldName: 'Name', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            showAreaTextError.name = true;
        }
        if (this.isEmpty(nationality)) {
            tempErrorMessageList.push({ fieldName: 'Nationality', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            showAreaTextError.nationality = true;
        }
        if (this.isEmpty(doctorName)) {
            tempErrorMessageList.push({ fieldName: 'Doctors Name', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            showAreaTextError.doctorName = true;
        }
        if (this.isEmpty(doctorPost)) {
            tempErrorMessageList.push({ fieldName: 'Doctor Post', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            showAreaTextError.doctorPost = true;
        }
        if (this.isEmpty(manufacture)) {
            tempErrorMessageList.push({ fieldName: 'Manufacturer', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            showAreaTextError.manufacture = true;
        }
        if (this.isEmpty(batcheNo)) {
            tempErrorMessageList.push({ fieldName: 'Batch No', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            showAreaTextError.batcheNo = true;
        }
        if (this.isEmpty(certNo)) {
            tempErrorMessageList.push({ fieldName: 'Cert No', errMsg: CommonMessage.VALIDATION_NOTE_REQUIRED() });
            showAreaTextError.certNo = true;
        }
        this.setState({
            showAreaTextError: showAreaTextError,
            errorMessageList: tempErrorMessageList
        }, () => {
            if (this.state.errorMessageList.length > 0) {
                return;
            }
            if (this.props.handlingPrint) {
                return;
            }
            let params = {
                patientName: this.state.name,
                batch: this.state.batcheNo,
                certificateValid: formatTime(new Date(this.state.validDate).getTime(), 'D/M/Y'),
                date: this.state.issueDate,
                doctorPosition: this.state.doctorPost,
                doctorSign: this.state.doctorName,
                identification: this.state.otherDocumentNo,
                manufacturer: this.state.manufacture,
                nationality: this.state.nationality,
                patientKey: this.props.patientInfo.patientKey,
                certNo: this.state.certNo
            };
            this.props.handlePrint(params);
        });


    }

    handleCancel = () => {
        this.props.deleteSubTabsByOtherWay({
            editModeTabs: this.props.editModeTabs,
            name: accessRightEnum.yellowFeverCert
        });
    }



    render() {
        const { classes, nationalityList } = this.props;
        const {
            errorMessageList,
            showAreaTextError,
            name,
            nationality,
            otherDocumentNo,
            issueDate,
            doctorName,
            doctorPost,
            manufacture,
            batcheNo,
            validDate, certNo } = this.state;
        return (
            <ValidatorForm
                id={`${this.props.id}_name`}
                ref={'newYellowFeverForm'}
                onSubmit={this.preparePrint}
            >
                <Grid container spacing={2} style={{ justifyContent: 'center' }}>
                    <Grid item container className={classes.root} spacing={2} >
                        <Grid item container xs={6} className={classes.grid} >
                            <FormControl className={classes.form_input}>
                                <Grid item ><CIMSInputLabel>Name:<RequiredIcon /></CIMSInputLabel></Grid>
                                <Grid item >
                                    <TextFieldValidator
                                        id={`${this.props.id}_name`}
                                        value={name}
                                        onChange={e => this.handleUpdate(e.target.value, 'name')}
                                        validByBlur
                                        upperCase
                                        onlyOneSpace
                                        variant="outlined"
                                        inputProps={{ maxLength: 40 }}
                                        msgPosition="bottom"
                                        validators={[
                                            ValidatorEnum.required,
                                            ValidatorEnum.isSpecialEnglish
                                        ]}
                                        errorMessages={[
                                            CommonMessage.VALIDATION_NOTE_REQUIRED(),
                                            CommonMessage.VALIDATION_NOTE_SPECIAL_ENGLISH()
                                        ]}
                                        warning={[
                                            ValidatorEnum.isEnglishWarningChar
                                        ]}
                                        warningMessages={[
                                            CommonMessage.WARNING_NOTE_SPECIAL_ENGLISH()
                                        ]}
                                        error={showAreaTextError.name}
                                    />
                                </Grid>
                            </FormControl>
                        </Grid>

                        <Grid item container xs={6} className={classes.grid} >

                            <FormControl className={classes.form_input}>
                                <Grid item ><CIMSInputLabel>Nationality:<RequiredIcon /></CIMSInputLabel></Grid>
                                <Grid item >
                                    <CIMSSelect
                                        id={`${this.props.id}_nationalitySelectField`}
                                        name={'nationality'}
                                        onChange={e => this.handleUpdate(e.value, 'nationality')}
                                        options={nationalityList.map((item) => (
                                            { value: item, label: item }))
                                        }
                                        value={nationality}
                                        TextFieldProps={{
                                            error: showAreaTextError.nationality
                                        }}
                                    />
                                </Grid>
                            </FormControl>
                        </Grid>



                        <Grid item container xs={6} className={classes.grid} >
                            <FormControl className={classes.form_input}>
                                <Grid item ><CIMSInputLabel>Doctor's Name:<RequiredIcon /></CIMSInputLabel></Grid>
                                <Grid item >
                                    <TextFieldValidator
                                        id={`${this.props.id}_doctorName`}
                                        value={doctorName}
                                        onChange={e => this.handleUpdate(e.target.value, 'doctorName')}
                                        validByBlur
                                        onlyOneSpace
                                        upperCase
                                        variant="outlined"
                                        inputProps={{ maxLength: 40 }}
                                        msgPosition="bottom"
                                        validators={[
                                            ValidatorEnum.required,
                                            ValidatorEnum.isSpecialEnglish
                                        ]}
                                        errorMessages={[
                                            CommonMessage.VALIDATION_NOTE_REQUIRED(),
                                            CommonMessage.VALIDATION_NOTE_SPECIAL_ENGLISH()
                                        ]}
                                        warning={[
                                            ValidatorEnum.isEnglishWarningChar
                                        ]}
                                        warningMessages={[
                                            CommonMessage.WARNING_NOTE_SPECIAL_ENGLISH()
                                        ]}
                                        error={showAreaTextError.doctorName}
                                    />
                                </Grid>
                            </FormControl>
                        </Grid>
                        <Grid item container xs={6} className={classes.grid} >
                            <FormControl className={classes.form_input}>
                                <Grid item ><CIMSInputLabel>Doctor's Post:<RequiredIcon /></CIMSInputLabel></Grid>
                                <Grid item >
                                    <CIMSTextField
                                        id={`${this.props.id}_doctorPost`}
                                        value={doctorPost}
                                        onChange={e => this.handleUpdate(e.target.value, 'doctorPost')}
                                        inputProps={{
                                            maxLength: 60
                                        }}
                                        error={showAreaTextError.doctorPost}
                                    />
                                </Grid>
                            </FormControl>
                        </Grid>


                        <Grid item container xs={6} className={classes.grid} >
                            <FormControl className={classes.form_input}>
                                <Grid item ><CIMSInputLabel>Manufacturer:<RequiredIcon /></CIMSInputLabel></Grid>
                                <Grid item >
                                    <CIMSTextField
                                        id={`${this.props.id}_manufacture`}
                                        value={manufacture}
                                        onChange={e => this.handleUpdate(e.target.value, 'manufacture')}
                                        inputProps={{
                                            maxLength: 40
                                        }}
                                        error={showAreaTextError.manufacture}
                                    />
                                </Grid>

                            </FormControl>
                        </Grid>
                        <Grid item container xs={6} className={classes.grid} >
                            <FormControl className={classes.form_input}>
                                <Grid item ><CIMSInputLabel>Batch No:<RequiredIcon /></CIMSInputLabel></Grid>
                                <Grid item >
                                    <CIMSTextField
                                        id={`${this.props.id}_batcheNo`}
                                        value={batcheNo}
                                        onChange={e => this.handleUpdate(e.target.value, 'batcheNo')}
                                        inputProps={{
                                            maxLength: 40
                                        }}
                                        error={showAreaTextError.batcheNo}
                                    />
                                </Grid>
                            </FormControl>
                        </Grid>

                        <Grid item container xs={6} className={classes.grid} >
                            <FormControl className={classes.form_input}>
                                <Grid item ><CIMSInputLabel>Issue Date:<RequiredIcon /></CIMSInputLabel></Grid>
                                <Grid item >
                                    <DateFieldValidator
                                        id={`${this.props.id}_issueDate`}
                                        value={issueDate}
                                        maxDate={validDate}
                                        onChange={(date) => { this.handleUpdate(date, 'issueDate'); }}
                                        onBlur={date => { this.handleOnBlur(date, 'issueDate'); }}
                                    />
                                </Grid>
                            </FormControl>

                        </Grid>

                        <Grid item container xs={6} className={classes.grid} >
                            <FormControl className={classes.form_input}>
                                <Grid item ><CIMSInputLabel>Valid Date Of Certificate:<RequiredIcon /></CIMSInputLabel></Grid>
                                <Grid item >
                                    <DateFieldValidator
                                        id={`${this.props.id}_validDate`}
                                        value={validDate}
                                        minDate={issueDate}
                                        onChange={(date) => { this.handleUpdate(date, 'validDate'); }}
                                        onBlur={date => { this.handleOnBlur(date, 'validDate'); }}
                                    />
                                </Grid>
                            </FormControl>
                        </Grid>

                        <Grid item container xs={6} className={classes.grid} >
                            <FormControl className={classes.form_input}>
                                <Grid item ><CIMSInputLabel>Other Doc. Number:</CIMSInputLabel></Grid>
                                <Grid item >
                                    <CIMSTextField
                                        id={`${this.props.id}_otherDocumentNo`}
                                        value={otherDocumentNo}
                                        onChange={e => this.handleUpdate(e.target.value, 'otherDocumentNo')}
                                        inputProps={{
                                            maxLength: 30
                                        }}
                                        error={showAreaTextError.otherDocumentNo}
                                    />
                                </Grid>
                            </FormControl>
                        </Grid>

                        <Grid item container xs={6} className={classes.grid} >
                            <FormControl className={classes.form_input}>
                                <Grid item ><CIMSInputLabel>Cert No:<RequiredIcon /></CIMSInputLabel></Grid>
                                <Grid item >
                                    <CIMSTextField
                                        id={`${this.props.id}_Cert No`}
                                        value={certNo}
                                        onChange={e => this.handleUpdate(e.target.value, 'certNo')}
                                        inputProps={{
                                            maxLength: 30
                                        }}
                                        error={showAreaTextError.certNo}
                                    />
                                </Grid>
                            </FormControl>
                        </Grid>

                    </Grid>

                    {
                        errorMessageList.length > 0 ?
                            <Grid item container xs={12}>
                                <Grid item xs={1}></Grid>
                                <Grid item xs={11} >
                                    {
                                        errorMessageList && errorMessageList.length > 0 ?
                                            <Grid item container >
                                                {
                                                    errorMessageList.map((item, i) => (
                                                        <Grid item container key={i} justify="flex-start" id={`newRefferalLetterCert${item.fieldName.replace(' ', '')}ErrorMessage`}>
                                                            <Typography className={classes.errorFieldNameText}>{`${item.fieldName}: ${item.errMsg}`}</Typography>
                                                        </Grid>
                                                    ))
                                                }
                                            </Grid> : null
                                    }
                                </Grid>
                            </Grid>
                            : null
                    }

                    <EditModeMiddleware componentName={accessRightEnum.yellowFeverCert} when={this.state.edit} />
                    <CIMSButtonGroup
                        buttonConfig={
                            [
                                {
                                    id: this.props.id + '_btnSaveAndPrint',
                                    name: 'Save & Print',
                                    onClick: this.handleSubmit
                                },
                                {
                                    id: this.props.id + '_btnCancel',
                                    name: 'Cancel',
                                    onClick: this.handleCancel
                                }
                            ]
                        }
                    />
                </Grid >
            </ValidatorForm>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        handlingPrint: state.yellowFeverCert.handlingPrint,
        editModeTabs: state.mainFrame.editModeTabs,
        nationalityList: state.patient.nationalityList || [],
        patientInfo: state.patient.patientInfo
    };
};

const mapDispatchToProps = {
    deleteSubTabsByOtherWay,
    updateCurTab,
    openCommonMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(NewYellowFeverCertificate));