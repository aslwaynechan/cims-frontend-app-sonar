import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
    Grid
} from '@material-ui/core';
import CIMSLoadingButton from '../../../components/Buttons/CIMSLoadingButton';
import CIMSTable from '../../../components/Table/CIMSTable';
import ValidatorForm from '../../../components/FormValidator/ValidatorForm';
import SelectFieldValidator from '../../../components/FormValidator/SelectFieldValidator';
import TextFieldValidator from '../../../components/FormValidator/TextFieldValidator';
import { getCodeList } from '../../../store/actions/patient/patientAction';
import { codeList } from '../../../constants/codeList';
import * as patientSpecFuncType from '../../../store/actions/patient/patientSpecFunc/patientSpecFuncActionType';
import { skipTab } from '../../../store/actions/mainFrame/mainFrameAction';
import {
    resetLinkPatient,
    searchPatientPrecisely,
    confirmAnonymousPatient
} from '../../../store/actions/patient/patientSpecFunc/patientSpecFuncAction';
import AccessRightEnum from '../../../enums/accessRightEnum';
import HKIDInput from '../../compontent/hkidInput';
import { openCommonMessage } from '../../../store/actions/message/messageAction';
import * as CommonUtilities from '../../../utilities/commonUtilities';
import CIMSPromptDialog from '../../../components/Dialog/CIMSPromptDialog';
import Enum from '../../../enums/enum';
import moment from 'moment';

const styles = () => ({
    customTableHeadCell: {
        backgroundColor: '#b8bcb9',
        fontSize: '13px'
    },
    customTableBodyCell: {
        fontSize: '13px'
    },
    marginTop20: {
        marginTop: 6
    },
    padding12: {
        paddingLeft: 12,
        paddingRight: 12
    }
});

class LinkPatient extends Component {

    constructor(props) {
        super(props);

        let params = [
            codeList.document_type
        ];
        this.props.getCodeList(params, patientSpecFuncType.PUT_CODE_LIST);
    }

    state = {
        selectedItems: [],
        tableRows: [
            { name: 'engSurname', label: 'Surname', width: 100 },
            { name: 'engGivename', label: 'Given Name', width: 100 },
            { name: 'nameChi', label: 'Chinese name', width: 100 },
            { name: 'hkid', label: 'HKID', width: 60 },
            { name: 'docType', label: 'Document Type', width: 90 },
            { name: 'otherDocNo', label: 'Document Number', width: 100 },
            { name: 'dob', label: 'Date of Birth', width: 100 },
            { name: 'gender', label: 'Sex', width: 80 }
        ],
        tableOptions: {
            rowExpand: true,
            rowHover: true,
            headCellStyle: this.props.classes.customTableHeadCell,
            bodyCellStyle: this.props.classes.customTableBodyCell,
            rowsPerPage: 10,
            rowsPerPageOptions: [10, 15, 20],
            onSelectIdName: 'patientKey',
            onSelectedRow: (rowId, rowData, selectedData) => {
                this.selectTableItem(selectedData);
            }
        }
    }

    componentDidUpdate() {
        if (this.props.linkPatientStatus === 'success') {
            this.props.handleClose();
            this.props.openCommonMessage({
                msgCode: '110035',
                params: [{ name: 'PATIENT_CALL', value: CommonUtilities.getPatientCall() }]
            });
            this.props.getPatientQueue(this.props.searchParameter);
        }
    }

    componentWillUnmount() {
        this.props.resetLinkPatient();
    }

    selectTableItem = (selected) => {
        let selectedItem = [];
        if (selected && selected.length > 0)
            selectedItem.push(selected[0].patientKey);
        this.setState({ selectedItems: selectedItem });
    }

    handleChange = (name, value) => {
        if (name === 'phoneNo') {
            value = value.replace(/\D/g, '');
        }
        this.props.handleChange(name, value);
    }

    handleSearch = () => {
        const linkPara = this.props.linkParameter;
        const params = {
            countryNo: '',
            documentNo: linkPara.hkidOrDoc.replace('(', '').replace(')', ''),
            documentType: linkPara.docTypeCd,
            givenName: linkPara.engGivename,
            surname: linkPara.engSurname,
            phoneNo: linkPara.phoneNo
        };
        this.props.searchPatientPrecisely(params);
        this.tableRef.clearSelected();
        this.setState({ selectedItems: [] });
    }

    handleConfirm = () => {
        if (this.props.linkPatientList.length > 0 && this.state.selectedItems.length === 1) {
            const params = {
                patientKey: this.state.selectedItems[0],
                anonymousPatientKey: this.props.linkParameter.patientKey
            };
            this.props.confirmAnonymousPatient(params);
        } else {
            if (this.props.tabs.findIndex((item) => item.name === AccessRightEnum.registration) !== -1) {
                this.props.openCommonMessage({
                    msgCode: '110036',
                    params: [{ name: 'PATIENT_CALL', value: CommonUtilities.getPatientCall() }]
                });
            } else {
                const linkPara = this.props.linkParameter;
                const params = {
                    hkidOrDocNum: linkPara.hkidOrDoc,
                    docTypeCd: linkPara.docTypeCd,
                    engSurname: linkPara.engSurname,
                    engGivename: linkPara.engGivename,
                    phoneNo: linkPara.phoneNo
                };
                this.props.skipTab(AccessRightEnum.registration, params);
                this.props.handleClose();
            }
        }
    }

    handleClose = () => {
        this.props.handleClose();
    }

    loadPatientList = (originData) => {
        const { codeList } = this.props;

        let resultList = [];
        if (originData && originData.length > 0) {
            originData.forEach(d => {
                let curDocType = codeList.doc_type.find(item => item.code === d.docTypeCd);
                let curGender = '';
                switch (d.genderCd) {
                    case Enum.GENDER_MALE_VALUE: {
                        curGender = 'Male';
                        break;
                    }
                    case Enum.GENDER_FEMALE_VALUE: {
                        curGender = 'Female';
                        break;
                    }
                    case Enum.GENDER_UNKNOWN_VALUE: {
                        curGender = 'Unkonwn';
                        break;
                    }
                    default: {
                        curGender = 'Unkonwn';
                        break;
                    }
                }
                let curPatient = {
                    ...d,
                    docType: curDocType ? curDocType.engDesc : null,
                    dob: moment(d.dob).format(Enum.DATE_FORMAT_EDMY_VALUE),
                    gender: curGender
                };

                resultList.push(curPatient);
            });
        }
        return resultList;
    }

    render() {
        const { classes, open, linkParameter } = this.props;
        const id = 'linkPatient';
        const linkPatientList = this.loadPatientList(this.props.linkPatientList);
        return (
            <CIMSPromptDialog
                open={open}
                id={id}
                dialogTitle={`Search for ${CommonUtilities.getPatientCall()}`}
                dialogContentText={
                    <ValidatorForm ref="form" onSubmit={() => { }} id={id + '_form'}>
                        <Grid container>
                            <Grid item container xs={10} spacing={1}>
                                <Grid item xs={6}>
                                    <HKIDInput
                                        label={linkParameter.docTypeCd === 'ID' ? 'HKID' : 'Document Number'}
                                        isHKID={linkParameter.docTypeCd === 'ID'}
                                        id={id + '_hkidOrDoc'}
                                        value={linkParameter.hkidOrDoc}
                                        onChange={e => this.handleChange('hkidOrDoc', e.target.value)}
                                        onBlur={e => this.handleChange('hkidOrDoc', e.target.value)}
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <SelectFieldValidator
                                        TextFieldProps={{
                                            label: 'Document Type'
                                        }}
                                        id={id + '_docTypeCd'}
                                        options={this.props.codeList &&
                                            this.props.codeList.doc_type &&
                                            this.props.codeList.doc_type.map(item => ({ label: item.engDesc, value: item.code }))}
                                        value={linkParameter.docTypeCd}
                                        onChange={e => this.handleChange('docTypeCd', e.value)}
                                        addNullOption
                                    />
                                </Grid>
                                <Grid item xs={4}>
                                    <TextFieldValidator
                                        label="Surname"
                                        onlyOneSpace
                                        id={id + '_engSurname'}
                                        upperCase
                                        fullWidth
                                        inputProps={{ maxLength: 40 }}
                                        value={linkParameter.engSurname}
                                        onChange={e => this.handleChange('engSurname', e.target.value)}
                                        onBlur={e => this.handleChange('engSurname', e.target.value)}
                                    />
                                </Grid>
                                <Grid item xs={4}>
                                    <TextFieldValidator
                                        label="Given Name"
                                        onlyOneSpace
                                        id={id + '_engGivename'}
                                        upperCase
                                        fullWidth
                                        inputProps={{ maxLength: 40 }}
                                        value={linkParameter.engGivename}
                                        onChange={e => this.handleChange('engGivename', e.target.value)}
                                        onBlur={e => this.handleChange('engGivename', e.target.value)}
                                    />
                                </Grid>
                                <Grid item xs={4}>
                                    <TextFieldValidator
                                        label="Phone"
                                        id={id + '_phoneNo'}
                                        fullWidth
                                        inputProps={{ maxLength: 15 }}
                                        value={linkParameter.phoneNo}
                                        onChange={e => this.handleChange('phoneNo', e.target.value)}
                                    />
                                </Grid>
                            </Grid>
                            <Grid item container xs={2} alignItems="center" justify="center">
                                <CIMSLoadingButton
                                    onClick={this.handleSearch}
                                    id={id + '_search'}
                                >Search</CIMSLoadingButton>
                            </Grid>
                        </Grid>
                        <Grid container className={classes.marginTop20}>
                            <CIMSTable
                                id={id + '_table'}
                                innerRef={ref => this.tableRef = ref}
                                rows={this.state.tableRows}
                                options={this.state.tableOptions}
                                data={linkPatientList}
                                tableStyles={{
                                    height: 420
                                }}
                            />
                        </Grid>
                    </ValidatorForm>

                }
                buttonConfig={
                    [
                        {
                            id: id + '_confirm',
                            name: linkPatientList.length > 0 && this.state.selectedItems.length === 1 ? 'Confirm' : 'New Registration',
                            onClick: this.handleConfirm
                        },
                        {
                            id: id + '_cancel',
                            name: 'Cancel',
                            onClick: this.handleClose
                        }
                    ]
                }
            />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        codeList: state.patientSpecFunc.codeList,
        linkPatientList: state.patientSpecFunc.linkPatientList,
        linkPatientStatus: state.patientSpecFunc.linkPatientStatus,
        searchParameter: state.patientSpecFunc.searchParameter,
        tabs: state.mainFrame.tabs
    };
};

const mapDispatchToProps = {
    resetLinkPatient,
    getCodeList,
    skipTab,
    searchPatientPrecisely,
    confirmAnonymousPatient,
    openCommonMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(LinkPatient));