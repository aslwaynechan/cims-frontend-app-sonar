import React from 'react';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import SelectFieldValidator from '../../../components/FormValidator/SelectFieldValidator';
import DateFieldValidator from '../../../components/FormValidator/DateFieldValidator';

const MarginGrid = withStyles({
    root: {
        marginBottom: 15
    }
})(Grid);

const FilterPatient = (props) => {
    const handleOnChange = (name, value) => {
        if (props.onChange) {
            props.onChange(name, value);
        }
    };

    const {
        searchParameter,
        subEncounterTypeList,
        encounterTypeList,
        statusList,
        filterCondition,
        onBlur
    } = props;
    return (
        <>
            <MarginGrid item>
                <DateFieldValidator
                    labelText="From:"
                    isRequired
                    value={searchParameter && searchParameter.dateFrom}
                    id={'fromtDate'}
                    onChange={(e) => handleOnChange('dateFrom', e)}
                    onBlur={e => onBlur('dateFrom', e)}
                    onAccept={e => onBlur('dateFrom', e)}
                />
            </MarginGrid>
            <MarginGrid item>
                <DateFieldValidator
                    labelText="To:"
                    isRequired
                    value={searchParameter && searchParameter.dateTo}
                    id={'toDate'}
                    onChange={(e) => handleOnChange('dateTo', e)}
                    onBlur={e => onBlur('dateTo', e)}
                    onAccept={e => onBlur('dateTo', e)}
                />
            </MarginGrid>
            <MarginGrid item>
                <SelectFieldValidator
                    id={'attnStatusCd'}
                    labelText={'Status'}
                    name={'status'}
                    placeholder={'*All'}
                    onChange={(e) => handleOnChange('attnStatusCd', e.value)}
                    value={filterCondition && filterCondition.attnStatusCd}
                    options={[
                        { value: '', label: '*All' },
                        ...statusList &&
                        statusList.map((item) => (
                            { value: item.value, label: item.label }
                        ))
                    ]}
                />
            </MarginGrid>
            <MarginGrid item>
                <SelectFieldValidator
                    labelText="Encounter"
                    options={[
                        { value: '', label: '*All' },
                        ...encounterTypeList &&
                        encounterTypeList.map((item) => (
                            { value: item.encounterTypeCd, label: item.shortName }
                        ))
                    ]}
                    id={'encounterTypeCd'}
                    placeholder={'*All'}
                    name={'encounterType'}
                    onChange={(e) => handleOnChange('encounterTypeCd', e.value)}
                    value={filterCondition && filterCondition.encounterTypeCd}
                />
            </MarginGrid>
            <MarginGrid item>
                <SelectFieldValidator
                    id={'subEncounterTypeCd'}
                    labelText="Sub-encounter"
                    options={[
                        { value: '', label: '*All' },
                        ...subEncounterTypeList &&
                        subEncounterTypeList.map((item) => (
                            { value: item.subEncounterTypeCd, label: item.shortName }
                        ))
                    ]}
                    placeholder={'*All'}
                    name={'subEncounterType'}
                    onChange={(e) => handleOnChange('subEncounterTypeCd', e.value)}
                    value={filterCondition && filterCondition.subEncounterTypeCd}
                />
            </MarginGrid>
        </>
    );
};

export default FilterPatient;